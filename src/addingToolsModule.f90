!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module addingTools

! The module 'addingTools' assumes that the Fourier coefficients of the reflection and tranmission
! of individual atmospheric layers are known.
! It provides subroutines to calculate the reflectance, internal radiation field and UDsumLocal_fc
! at the interfaces between the layers. UDsumLocal_fc is needed to calculate the derivative when
! corrections for a spherical shell atmosphere are applied
! subroutine adding can be used to replace subroutine ordersScat in the Labos module

! Author: Johan F. de Haan, KNMI KS/AK
! Version number: 1.0  

  use errorHandlingModule
  use dataStructures
  use mathTools,       only : LU_decomposition, solve_lin_system_LU_based

! set default to private, i.e. everything is private to the module, except
! that what is made explicitly public
  private
  public adding, fillsurface

  type optPartAtm ! optical properties of a (partial) atmosphere
    real(8), pointer   :: R(:,:)   => null() ! (dimSV_fc*numutot,dimSV_fc*numutot) reflection: illumination at top
    real(8), pointer   :: T(:,:)   => null() ! (dimSV_fc*numutot,dimSV_fc*numutot) diffuse transmission: illumination at top
    real(8), pointer   :: U(:,:)   => null() ! (dimSV_fc*numutot,dimSV_fc*numutot) upward   radiation last adding step: illum top
    real(8), pointer   :: D(:,:)   => null() ! (dimSV_fc*numutot,dimSV_fc*numutot) downward radiation last adding step: illum top
    real(8), pointer   :: Rst(:,:) => null() ! (dimSV_fc*numutot,dimSV_fc*numutot) reflection: illumination at bottom
    real(8), pointer   :: Tst(:,:) => null() ! (dimSV_fc*numutot,dimSV_fc*numutot) diffuse transmission: illumination at bottom
    real(8), pointer   :: Ust(:,:) => null() ! (dimSV_fc*numutot,dimSV_fc*numutot) upward   radiation last adding step: illum bot
    real(8), pointer   :: Dst(:,:) => null() ! (dimSV_fc*numutot,dimSV_fc*numutot) downward radiation last adding step: illum bot
    real(8), pointer   :: tpl(:)   => null() ! (numutot) TU (vdHulst) integrated diffuse transmission: illumination at top
    real(8), pointer   :: tplst(:) => null() ! (numutot) TU (vdHulst) integrated diffuse transmission: illumination at bottom
    real(8)            :: s                  ! URU (vdHulst) spherical albedo: illumination at top
    real(8)            :: sst                ! UR*U (vdHulst) spherical albedo: illumination at bottom
  end type optPartAtm

  contains

    subroutine adding(errS, optPropRTMGridS, iFourier, controlS, startLevel, endLevel,  atten, &
                      dimSV_fc, nmutot, nGauss, RT_fc, geometryS, UDsumLocal_fc, UD_fc,        &
                      iwave, wavelMRS, RRS_RingS, createLUTS)
  
      ! returns UDsumLocal_fc and the internal fields UD_fc, but now
      ! calculated using the adding method instead of orders of scattering
  
      implicit none
  
      type(errorType),          intent(inout) :: errS
      type(optPropRTMGridType), intent(in)    :: optPropRTMGridS
      type(controlType),        intent(in)    :: controlS 
      integer,                  intent(in)    :: iFourier, startLevel, endLevel
      integer,                  intent(in)    :: dimSV_fc, nmutot, nGauss
      real(8),                  intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(RTType),             intent(in)    :: RT_fc(0:endLevel)
      type(geometryType),       intent(in)    :: geometryS
  
      ! UDsumLocal_fc is used for pseudo-spherical correction ( used for d2R/dkabs/dz )
      type(UDLocalType),        intent(inout) :: UDsumLocal_fc(0:endLevel)
  
      ! internal field all orders of scattering
      type(UDType),             intent(inout) :: UD_fc(0:endLevel)
      
      integer, optional,             intent(in)    :: iwave
      type(wavelHRType), optional,   intent(in)    :: wavelMRS   ! instr - high resolution wavelength grid
      type(RRS_RingType), optional,  intent(in)    :: RRS_RingS
      type(createLUTType), optional, intent(inout) :: createLUTS     ! for a specific iFourier and iband

  
      ! local
  
      ! partial atmospheres above a black surface for the upper layers 
      type(optPartAtm) :: partAtmUpper(startLevel:endLevel)
      ! after adding a Lambertian surface
      type(optPartAtm) :: partAtmUpperSurf(startLevel:endLevel)
      ! partial atmospheres above the reflecting ground surface for the lower layers
      type(optPartAtm) :: partAtmLower(startLevel:endLevel)
  
      ! Claim memory for values pertaining to the interfaces, layers and partial atmospheres
      call allocatePartAtm(errS, startLevel, endLevel, dimSV_fc, nmutot, partAtmUpper, partAtmLower, partAtmUpperSurf)
      if (errorCheck(errS)) return
  
      ! LUT is only created if useSphericalCorr is .true.
      if ( controlS%useSphericalCorr ) then
        if (present(createLUTS)) then
          if ( createLUTS%useChandraFormula ) then
            call addingFromTopDownLUT_Chandra(errS, geometryS, optPropRTMGridS, iFourier,            &
                                              startLevel, endLevel, atten, RT_fc, dimSV_fc,          &
                                              nmutot, nGauss, controlS%thresholdMul, partAtmUpper,   &
                                              partAtmUpperSurf, iwave, wavelMRS, RRS_RingS, createLUTS)
          else

            call addingFromTopDownLUT(errS, geometryS, optPropRTMGridS, iFourier,            &
                                      startLevel, endLevel, atten, RT_fc, dimSV_fc,          &
                                      nmutot, nGauss, controlS%thresholdMul, partAtmUpper,   &
                                      partAtmUpperSurf, iwave, wavelMRS, RRS_RingS, createLUTS)
          end if ! createLUTS%useChandraFormula
          if (errorCheck(errS)) return

    ! JdH add code to calculate the internal field in the complete atmosphere

        else
          ! not creating LUT
          ! addingFromTopDown provides the reflection, transmission and internal field for partial
          ! atmospheres
          call addingFromTopDown(errS, geometryS, iFourier, startLevel, endLevel, atten, RT_fc, dimSV_fc, &
                                 nmutot, nGauss, controlS%thresholdMul, partAtmUpper)

          if (errorCheck(errS)) return

          ! calculate the internal field in the complete atmosphere
          call calcUD_FromTopDown(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss,       &
                                 partAtmUpper, UD_fc)
          if (errorCheck(errS)) return
          ! calcUDsumLocal is used to correct the derivatives for a spherical atmosphere
          call calcUDsumLocal(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss,           & 
                              RT_fc, UD_fc, UDsumLocal_fc)
          if (errorCheck(errS)) return

        end if
        if (errorCheck(errS)) return
        call calcUD_FromTopDown(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss,       &
                                partAtmUpper, UD_fc)
        if (errorCheck(errS)) return
        call calcUDsumLocal(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss,           & 
                            RT_fc, UD_fc, UDsumLocal_fc)
        if (errorCheck(errS)) return
  
      else
       ! start at the surface and add homogeneous layers to the top of the atmosphere
        call addingFromSurfaceUp(errS, geometryS, iFourier, startLevel, endLevel, &
                                 atten, RT_fc, dimSV_fc, nmutot, nGauss,          &
                                 controlS%thresholdMul, partAtmLower)
        if (errorCheck(errS)) return
        ! calculate the internal field in the complete atmosphere
        call calcUD_FromSurfaceUp(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss,  &
                                  partAtmLower, UD_fc)
        if (errorCheck(errS)) return
  
      end if ! controlS%useSphericalCorr
      
      ! clean up
      call deallocatePartAtm(errS, startLevel, endLevel, partAtmUpper, partAtmLower, partAtmUpperSurf)
      if (errorCheck(errS)) return
  
    end subroutine adding
  
  
    subroutine allocatePartAtm(errS, startLevel, endLevel, dimSV_fc, nmutot, partAtmUpper, partAtmLower, partAtmUpperSurf)
  
      ! allocate memory for the partial atmospheres
  
      implicit none
  
      type(errorType), intent(inout) :: errS
      integer, intent(in) :: startLevel, endLevel, dimSV_fc, nmutot
      type(optPartAtm)    :: partAtmUpper(startlevel:endLevel)
      type(optPartAtm)    :: partAtmLower(startlevel:endLevel)
      type(optPartAtm)    :: partAtmUpperSurf(startlevel:endLevel)
  
      ! local
      integer(4) :: ilevel
      integer(4) :: allocStatus
      integer(4) :: sumAllocStatus
  
      allocStatus    = 0
      sumAllocStatus = 0
  
      do ilevel = startlevel, endLevel
        allocate (partAtmUpper(ilevel)%R  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpper(ilevel)%T  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpper(ilevel)%U  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpper(ilevel)%D  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpper(ilevel)%Rst(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpper(ilevel)%Tst(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpper(ilevel)%Ust(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpper(ilevel)%Dst(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
      end do
  
      do ilevel = startlevel, endLevel
        allocate (partAtmLower(ilevel)%R  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmLower(ilevel)%T  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmLower(ilevel)%U  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmLower(ilevel)%D  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmLower(ilevel)%Rst(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmLower(ilevel)%Tst(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmLower(ilevel)%Ust(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmLower(ilevel)%Dst(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
      end do

      do ilevel = startlevel, endLevel
        allocate (partAtmUpperSurf(ilevel)%R  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpperSurf(ilevel)%T  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpperSurf(ilevel)%U  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpperSurf(ilevel)%D  (dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpperSurf(ilevel)%Rst(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpperSurf(ilevel)%Tst(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpperSurf(ilevel)%Ust(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpperSurf(ilevel)%Dst(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
      end do

      do ilevel = startlevel, endLevel
        allocate (partAtmUpper(ilevel)%tpl  (nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpper(ilevel)%tplst  (nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmLower(ilevel)%tpl  (nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmLower(ilevel)%tplst  (nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpperSurf(ilevel)%tpl  (nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate (partAtmUpperSurf(ilevel)%tplst  (nmutot), STAT=allocStatus  )
        sumAllocStatus = sumAllocStatus + allocStatus
      end do
  
      if (sumAllocStatus /= 0) Then 
        print *, 'allocatePartAtm in module addingTools has nonzero sumAllocStatus.'
        call mystop(errS, 'allocatePartAtm in addingTools module has nonzero sumAllocStatus.')
        if (errorCheck(errS)) return
      end if
  
      ! initialize
  
      do ilevel = startLevel, endLevel
        partAtmUpper(ilevel)%R    = 0.0d0
        partAtmUpper(ilevel)%T    = 0.0d0
        partAtmUpper(ilevel)%U    = 0.0d0
        partAtmUpper(ilevel)%D    = 0.0d0
        partAtmUpper(ilevel)%Rst  = 0.0d0
        partAtmUpper(ilevel)%Tst  = 0.0d0
        partAtmUpper(ilevel)%Ust  = 0.0d0
        partAtmUpper(ilevel)%Dst  = 0.0d0
        partAtmUpper(ilevel)%tpl  = 0.0d0
        partAtmUpper(ilevel)%s    = 0.0d0
        partAtmUpper(ilevel)%tplst= 0.0d0
        partAtmUpper(ilevel)%sst  = 0.0d0
      end do
  
      do ilevel = startLevel, endLevel
        partAtmLower(ilevel)%R    = 0.0d0
        partAtmLower(ilevel)%T    = 0.0d0
        partAtmLower(ilevel)%U    = 0.0d0
        partAtmLower(ilevel)%D    = 0.0d0
        partAtmLower(ilevel)%Rst  = 0.0d0
        partAtmLower(ilevel)%Tst  = 0.0d0
        partAtmLower(ilevel)%Ust  = 0.0d0
        partAtmLower(ilevel)%Dst  = 0.0d0
        partAtmLower(ilevel)%tpl  = 0.0d0
        partAtmLower(ilevel)%s    = 0.0d0
        partAtmLower(ilevel)%tplst= 0.0d0
        partAtmLower(ilevel)%sst  = 0.0d0
      end do

      do ilevel = startLevel, endLevel
        partAtmUpperSurf(ilevel)%R    = 0.0d0
        partAtmUpperSurf(ilevel)%T    = 0.0d0
        partAtmUpperSurf(ilevel)%U    = 0.0d0
        partAtmUpperSurf(ilevel)%D    = 0.0d0
        partAtmUpperSurf(ilevel)%Rst  = 0.0d0
        partAtmUpperSurf(ilevel)%Tst  = 0.0d0
        partAtmUpperSurf(ilevel)%Ust  = 0.0d0
        partAtmUpperSurf(ilevel)%Dst  = 0.0d0
        partAtmUpperSurf(ilevel)%tpl  = 0.0d0
        partAtmUpperSurf(ilevel)%s    = 0.0d0
        partAtmUpperSurf(ilevel)%tplst= 0.0d0
        partAtmUpperSurf(ilevel)%sst  = 0.0d0
      end do

    end subroutine allocatePartAtm
  
  
    subroutine deallocatePartAtm(errS, startLevel, endLevel, partAtmUpper, partAtmLower, partAtmUpperSurf)
  
      ! allocate memory for the partial atmospheres
  
      implicit none
  
      type(errorType), intent(inout) :: errS
      integer,    intent(in) :: startLevel, endLevel
      type(optPartAtm)       :: partAtmUpper(startLevel:endLevel)
      type(optPartAtm)       :: partAtmLower(startLevel:endLevel)
      type(optPartAtm)       :: partAtmUpperSurf(startLevel:endLevel)
  
      ! local
      integer :: ilevel
  
      do ilevel = startLevel, endLevel
        deallocate (partAtmUpper(ilevel)%R )
        deallocate (partAtmUpper(ilevel)%T )
        deallocate (partAtmUpper(ilevel)%U )
        deallocate (partAtmUpper(ilevel)%D )
        deallocate (partAtmUpper(ilevel)%Rst )
        deallocate (partAtmUpper(ilevel)%Tst )
        deallocate (partAtmUpper(ilevel)%Ust )
        deallocate (partAtmUpper(ilevel)%Dst )
      end do
  
      do ilevel = startLevel, endLevel
        deallocate (partAtmLower(ilevel)%R )
        deallocate (partAtmLower(ilevel)%T )
        deallocate (partAtmLower(ilevel)%U )
        deallocate (partAtmLower(ilevel)%D )
        deallocate (partAtmLower(ilevel)%Rst )
        deallocate (partAtmLower(ilevel)%Tst )
        deallocate (partAtmLower(ilevel)%Ust )
        deallocate (partAtmLower(ilevel)%Dst )
      end do

      do ilevel = startLevel, endLevel
        deallocate (partAtmUpperSurf(ilevel)%R )
        deallocate (partAtmUpperSurf(ilevel)%T )
        deallocate (partAtmUpperSurf(ilevel)%U )
        deallocate (partAtmUpperSurf(ilevel)%D )
        deallocate (partAtmUpperSurf(ilevel)%Rst )
        deallocate (partAtmUpperSurf(ilevel)%Tst )
        deallocate (partAtmUpperSurf(ilevel)%Ust )
        deallocate (partAtmUpperSurf(ilevel)%Dst )
      end do

      do ilevel = startLevel, endLevel
        deallocate (partAtmUpper(ilevel)%tpl )
        deallocate (partAtmUpper(ilevel)%tplst )
        deallocate (partAtmLower(ilevel)%tpl )
        deallocate (partAtmLower(ilevel)%tplst )
        deallocate (partAtmUpperSurf(ilevel)%tpl )
        deallocate (partAtmUpperSurf(ilevel)%tplst )
      end do
  
    end subroutine deallocatePartAtm
  
  
    subroutine addLayerToBottom  (errS, geometryS, iFourier, dimSV_fc, nmutot,  &
                                  nGauss, thresholdMul,                         &
                                  Etop, Rtop, Ttop, Rsttop, Tsttop,             &
                                  Ebot, Rbot, Tbot, Rstbot, Tstbot,             &
                                  Rcom, Tcom, U, D,                             &
                                  Rstcom, Tstcom, Ust, Dst,                     &
                                  tplcom, scom, tplstcom, sstcom)
  
      ! Add a homogeneous bottom layer with the properties 
      ! Ebot, Rbot, Tbot, Rstbot, and Tstbot
      ! to the inhomogeneous top layer with the properties
      ! Etop, Rtop, Ttop, Rsttop, and Tsttop.
      ! the properties of the combined layers are returned in
      ! Ecom, Rcom, Tcom, Rstcom, and Tstcom
      ! The scheme is more complicated than the one in 'addLayerToTop'
      ! because the top layer is inhomogeneous.
  
      implicit none
  
      type(errorType),                                   intent(inout) :: errS
      type(geometryType),                                  intent(in)  :: geometryS
      integer,                                             intent(in)  :: iFourier, dimSV_fc, nmutot, nGauss
      real(8),                                             intent(in)  :: thresholdMul
      real(8), dimension(dimSV_fc*nmutot),                 intent(in)  :: Ebot, Etop
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot), intent(in)  :: Rbot, Tbot, Rstbot, Tstbot
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot), intent(in)  :: Rtop, Ttop, Rsttop, Tsttop
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot), intent(out) :: Rcom, Tcom, U, D
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot), intent(out) :: Rstcom, Tstcom, Ust, Dst
      real(8), dimension(nmutot), optional,                intent(out) :: tplcom, tplstcom
      real(8), optional,                                   intent(out) :: scom, sstcom
  
      ! local
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot)              :: Q, Qst
      integer:: imu, imu0, i, i0
  
      Qst     = Qseries(errS, dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Rbot, Rsttop)
      Dst     = Tstbot + semul(dimSV_fc*nmutot, Qst, Ebot) &
                       + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Qst, Tstbot)
      Ust     = semul(dimSV_fc*nmutot, Rsttop, Ebot) &
              + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Rsttop, Dst)
      Rstcom  = Rstbot + esmul(dimSV_fc*nmutot, Ebot, Ust) &
                       + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Tbot, Ust)
      Tstcom  = esmul(dimSV_fc*nmutot, Etop, Dst) &
              + semul(dimSV_fc*nmutot, Tsttop, Ebot) &
              +  smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Tsttop, Dst)
      Q       = transposeSignChange4(dimSV_fc, nmutot, Qst)
      D       = Ttop + semul(dimSV_fc*nmutot, Q, Etop) &
                     +  smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Q, Ttop) 
      U       = semul(dimSV_fc*nmutot, Rbot, Etop) &
              + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Rbot, D)
      Rcom    = Rtop + esmul(dimSV_fc*nmutot, Etop, U) &
                     +  smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Tsttop, U)
      Tcom    = esmul(dimSV_fc*nmutot, Ebot, D) + semul(dimSV_fc*nmutot, Tbot, Etop) &
              + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Tbot, D)

      if ( present(scom) ) then
        if ( present(sstcom) ) then
          scom   = 0.0d0
          sstcom = 0.0d0
          if ( iFourier == 0 ) then
            do imu = 1, nGauss
              do imu0 = 1, ngauss
                i  = dimSV_fc * (imu - 1)  + 1
                i0 = dimSV_fc * (imu0 - 1) + 1
                scom   = scom   + geometryS%w(imu) * geometryS%w(imu0) * Rcom(i,i0)
                sstcom = sstcom + geometryS%w(imu) * geometryS%w(imu0) * Rstcom(i,i0)
              end do
            end do
          end if ! iFourier
        end if
      end if
  
      if ( present(tplcom) ) then
        if ( present(tplstcom) ) then
          if ( iFourier > 0 ) then
            do imu = 1, nmutot
              tplcom(imu)   = 0.0d0
              tplstcom(imu) = 0.0d0
            end do
          else
            do imu = 1, nmutot
              tplcom(imu)   = 0.0d0
              tplstcom(imu) = 0.0d0
              do imu0 = 1, ngauss
                i  = dimSV_fc * (imu  - 1) + 1
                i0 = dimSV_fc * (imu0 - 1) + 1
                tplcom(imu)   = tplcom(imu)   + geometryS%w(imu0) * Tcom(i,i0)
              end do
              tplcom(imu)   = tplcom(imu)   / geometryS%w(imu) + Ebot(imu) * Etop(imu) 
            end do
          end if ! iFourier
        end if
      end if
  
    end subroutine addLayerToBottom
  
  
    function transposeSignChange3(dimSV_fc, nmutot, x)
      ! take the transpose and
      ! add sign changes for the elements (1,3), (2,3), (4,3), (3,1), (3,2), and (3,4)
      ! when dimSV_fc = 3 or 4
  
      integer,                                             intent(in)  :: dimSV_fc, nmutot
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot), intent(in)  :: x
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot)              :: transposeSignChange3
  
      ! local
      integer:: imu, imu0, iSV, jSV, ind, ind0
  
      if ( dimSV_fc > 2 ) then
  
        transposeSignChange3 = transpose(X)
        do imu0 = 1, nmutot
          do jSV = 3, 3
           ind0 = jSV + (imu0 - 1) * dimSV_fc
            do imu = 1 , nmutot
              do iSV = 1, dimSV_fc
                ind = iSV + (imu - 1) * dimSV_fc
                  transposeSignChange3(ind,ind0) =  - x(ind, ind0)
              end do
            end do
          end do
        end do
        do imu0 = 1, nmutot
          do jSV = 1, dimSV_fc
           ind0 = jSV + (imu0 - 1) * dimSV_fc
            do imu = 1 , nmutot
              do iSV = 3, 3
                ind = iSV + (imu - 1) * dimSV_fc
                  transposeSignChange3(ind,ind0) =  - x(ind, ind0)
              end do
            end do
          end do
        end do
  
      else
  
        transposeSignChange3 = transpose(x)
  
      end if
  
    end function transposeSignChange3
  
  
    function transform_top_bottom(dimSV_fc, nmutot, x)
  
      ! Transform supermatrix X from values for illumination at the top
      ! to values for illumination at the bottom for homogeneous layers.
  
      implicit none
  
      integer, intent(in) :: dimSV_fc, nmutot
      real(8), intent(in) :: x(dimSV_fc * nmutot, dimSV_fc * nmutot)
      real(8)             :: transform_top_bottom(dimSV_fc * nmutot, dimSV_fc * nmutot)
  
      ! local
      integer :: imu, imu0, iSV, jSV, ind, ind0, start
  
      transform_top_bottom = x
  
      if ( dimSV_fc < 3 ) return   ! do nothing
  
      do imu0 = 1, nmutot
        start = dimSV_fc * (imu0 - 1)
        do jSV = 3, dimSV_fc
          ind0 = start + jSV
          do ind = 1, dimSV_fc * nmutot
            transform_top_bottom(ind,ind0) = -x(ind,ind0)
          end do ! ind0
        end do ! iSV
      end do ! imu0
  
      do ind0 = 1, dimSV_fc * nmutot
        do imu = 1, nmutot
          start = dimSV_fc * (imu - 1)
          do iSV = 3, dimSV_fc
            ind = start + iSV
            transform_top_bottom(ind,ind0) = -transform_top_bottom(ind,ind0)
          end do ! iSV
        end do ! imu
      end do ! ind0
  
    end function transform_top_bottom
  
  
    function transposeSignChange4(dimSV_fc, nmutot, x)
      ! take the transpose and
      ! add sign changes for the elements (1,4), (2,4), (3,4), (4,1), (4,2), and (4,3)
      ! when dimSV_fc = 4
  
      integer,                                             intent(in)  :: dimSV_fc, nmutot
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot), intent(in)  :: x
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot)              :: transposeSignChange4
  
      ! local
      integer:: imu, imu0, iSV, jSV, ind, ind0
  
      if ( dimSV_fc == 4 ) then
  
        transposeSignChange4 = transpose(x)
        do imu0 = 1, nmutot
          do jSV = 4, dimSV_fc
           ind0 = jSV + (imu0 - 1) * dimSV_fc
            do imu = 1 , nmutot
              do iSV = 1, dimSV_fc
                ind = iSV + (imu - 1) * dimSV_fc
                  transposeSignChange4(ind,ind0) = - x(ind, ind0)
              end do
            end do
          end do
        end do
        do imu0 = 1, nmutot
          do jSV = 1, dimSV_fc
           ind0 = jSV + (imu0 - 1) * dimSV_fc
            do imu = 1 , nmutot
              do iSV = 4, dimSV_fc
                ind = iSV + (imu - 1) * dimSV_fc
                  transposeSignChange4(ind,ind0) = - x(ind, ind0)
              end do
            end do
          end do
        end do
  
      else
  
        transposeSignChange4 = transpose(x)
  
      end if
  
    end function transposeSignChange4
  
  
    subroutine addLayerToTop  (errS, geometryS, iFourier, dimSV_fc,     &
                               nmutot, nGauss, thresholdMul,            &
                               Etop, Ebot, Rtop, Ttop, Rsttop, Tsttop,  &
                               Rbot, Tbot, Rcom, Tcom, U, D,            &
                               tplcom, scom, tplstcom, sstcom)
    
      ! add a homogeneous layer to the top of a partial atmosphere
      ! note that the transmission is not calculated because the Lambertian
      ! cloud or ground surface does not transmit light. The light incident
      ! on the surface is calculated from the internal field after completion
      ! of the atmosphere.
    
      implicit none
    
      type(errorType),                                     intent(inout) :: errS
      type(geometryType),                                  intent(in)  :: geometryS
      integer,                                             intent(in)  :: iFourier, dimSV_fc, nmutot, nGauss
      real(8),                                             intent(in)  :: thresholdMul
      real(8), dimension(dimSV_fc*nmutot),                 intent(in)  :: Etop, Ebot
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot), intent(in)  :: Rtop, Ttop, Rsttop, Tsttop
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot), intent(in)  :: Rbot, Tbot
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot), intent(out) :: Rcom, Tcom, U, D
      real(8), dimension(dimSV_fc*nmutot), optional,       intent(out) :: tplcom, tplstcom
      real(8), optional,                                   intent(out) :: scom, sstcom
    
      ! local
      
      real(8), dimension(dimSV_fc*nmutot,dimSV_fc*nmutot)              :: Q
      integer  :: imu, imu0, i, i0
      
      Q       = Qseries(errS, dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Rsttop, Rbot)
      D       = Ttop + semul(dimSV_fc*nmutot, Q, Etop) &
                     + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Q, Ttop) 
      U       = semul(dimSV_fc*nmutot, Rbot, Etop) &
              + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Rbot, D)
      Rcom    = Rtop + esmul(dimSV_fc*nmutot, Etop, U) &
                     +  smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Tsttop, U)
      Tcom    = esmul(dimSV_fc*nmutot, Ebot, D) + semul(dimSV_fc*nmutot, Tbot, Etop) &
              + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Tbot, D)

      if ( present(scom) ) then
        if ( present(sstcom) ) then
          scom   = 0.0d0
          sstcom = 0.0d0
          if ( iFourier == 0 ) then
            do imu = 1, nGauss
              do imu0 = 1, ngauss
                i  = dimSV_fc * (imu - 1)  + 1
                i0 = dimSV_fc * (imu0 - 1) + 1
                scom   = scom   + geometryS%w(imu) * geometryS%w(imu0) * Rcom(i,i0)
              end do
            end do
          end if ! iFourier
        end if
      end if

      if ( present(tplcom) ) then
        if ( present(tplstcom) ) then
          if ( iFourier > 0 ) then
            do imu = 1, nmutot
              tplcom(imu)   = 0.0d0
              tplstcom(imu) = 0.0d0
            end do
          else
            do imu = 1, nmutot
              tplcom(imu)   = 0.0d0
              tplstcom(imu) = 0.0d0
              do imu0 = 1, ngauss
                i  = dimSV_fc * (imu  - 1) + 1
                i0 = dimSV_fc * (imu0 - 1) + 1
                tplcom(imu)   = tplcom(imu)   + geometryS%w(imu0) * Tcom(i,i0)
              end do
              tplcom(imu)   = tplcom(imu)   / geometryS%w(imu) + Ebot(imu) * Etop(imu) 
            end do
          end if ! iFourier
        end if
      end if

    end subroutine addLayerToTop


    subroutine addingFromTopDown(errS, geometryS, iFourier, startLevel, endLevel, atten, RT_fc, &
                                 dimSV_fc, nmutot, nGauss, thresholdMul, partAtmUpper)

      ! call this subroutine if we do not need to create a LUT
      ! adding from top down is needed for pseudo-spherical correction

      implicit none

      type(errorType),    intent(inout) :: errS
      type(geometryType), intent(in)    :: geometryS
      integer,            intent(in)    :: iFourier, startLevel, endLevel
      integer,            intent(in)    :: dimSV_fc, nmutot, nGauss
      real(8),            intent(in)    :: thresholdMul
      real(8),            intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(RTType),       intent(in)    :: RT_fc(0:endLevel)
      type(optPartAtm),   intent(inout) :: partAtmUpper(startLevel:endLevel)

      !local
      integer :: ilevel, imu, imu0, iSV, ind
      real(8) :: Etop(dimSV_fc*nmutot), Ebot(dimSV_fc*nmutot)
      logical, parameter :: verbose = .false.  ! write to intermediate file

      ! top layer is homogeneous
      partAtmUpper(endLevel)%R    = RT_fc(endLevel)%R
      partAtmUpper(endLevel)%Rst  = transform_top_bottom(dimSV_fc, nmutot, RT_fc(endLevel)%R)
      partAtmUpper(endLevel)%T    = RT_fc(endLevel)%T
      partAtmUpper(endLevel)%Tst  = transform_top_bottom(dimSV_fc, nmutot, RT_fc(endLevel)%T)
      ! initialize
      partAtmUpper(endLevel)%U    = RT_fc(endLevel)%R
      partAtmUpper(endLevel)%D    = 0.0d0                 
      partAtmUpper(endLevel)%Ust  = 0.0d0
      partAtmUpper(endLevel)%Dst  = RT_fc(endLevel)%T

      if (verbose) then                 ! write results to intermediate file
        write(intermediateFileUnit, *) 'Adding layer to the bottom of the partial atmosphere'
        write(intermediateFileUnit,'(2(A20, I4))') 'level = ', endLevel
        write(intermediateFileUnit,*) 'Rtop'
        do imu = 1, nmutot
          write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(endLevel)%R(imu,imu0), imu0 = 1, nmutot)
        end do
        write(intermediateFileUnit,*) 'Rsttop'
        do imu = 1, nmutot
          write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(endLevel)%Rst(imu,imu0), imu0 = 1, nmutot)
        end do
        write(19,*) 'Ttop'
        do imu = 1, nmutot
          write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(endLevel)%T(imu,imu0), imu0 = 1, nmutot)
        end do
        write(intermediateFileUnit,*) 'Tsttop'
        do imu = 1, nmutot
          write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(endLevel)%Tst(imu,imu0), imu0 = 1, nmutot)
        end do
      end if ! verbose

      do ilevel = endLevel - 1 , startlevel, -1
        ! fill Etop and Ebot for adding the next layer
        ! make an exception for adding the Lambertian cloud/surface
        ! for which the transmission is zero
        if ( ilevel == startlevel ) then
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              Etop(ind) = atten(imu, endLevel, ilevel)
              Ebot(ind) = 0.0d0
            end do ! iSV
          end do ! imu
        else
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              Etop(ind) = atten(imu, endLevel, ilevel    )
              Ebot(ind) = atten(imu, ilevel  , ilevel - 1)
            end do ! iSV
          end do ! imu
        end if

        call addLayerToBottom (errS, geometryS, iFourier, dimSV_fc, nmutot, nGauss, thresholdMul,  &
                               Etop, partAtmUpper(ilevel+1)%R, partAtmUpper(ilevel+1)%T, & 
                               partAtmUpper(ilevel+1)%Rst, partAtmUpper(ilevel+1)%Tst,   &
                               Ebot, RT_fc(ilevel)%R, RT_fc(ilevel)%T,                   &
                               RT_fc(ilevel)%Rst, RT_fc(ilevel)%Tst,                     &
                               partAtmUpper(ilevel)%R, partAtmUpper(ilevel)%T,           & 
                               partAtmUpper(ilevel)%U, partAtmUpper(ilevel)%D,           & 
                               partAtmUpper(ilevel)%Rst, partAtmUpper(ilevel)%Tst,       &
                               partAtmUpper(ilevel)%Ust, partAtmUpper(ilevel)%Dst)

        if (verbose) then                 ! write results to file fort.19
          write(intermediateFileUnit, *) 'Adding layer to the bottom of the partial atmosphere'
          write(intermediateFileUnit,'(2(A20, I4))') 'level = ', ilevel
          write(intermediateFileUnit,*) 'Rtop'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%R(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Rsttop'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Rst(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Ttop'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%T(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Tsttop'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Tst(imu,imu0), imu0 = 1, nmutot)
          end do
        end if

      end do ! ilevel loop

    end subroutine addingFromTopDown


    subroutine addingFromTopDownLUT_Chandra(errS, geometryS, optPropRTMGridS, iFourier,   &
                                 startLevel, endLevel, atten, RT_fc, dimSV_fc, &
                                 nmutot, nGauss, thresholdMul, partAtmUpper,   &
                                 partAtmUpperSurf, iwave, wavelMRS, RRS_RingS, createLUTS)

      ! Adding from top down is needed for pseudo-spherical correction

      ! This subroutine is used to make a LUT where the Chandrasekhar expression is used
      ! to account for the surface albedo. That means that R0 (refl for black surface), td and s*
      ! are calculated and stored in createLUTS. Values are calculated for pressure levels (altitudes)
      ! corresponding to the interval boundaries specified in SECTION ATMOSPHERIC_INTERVALS in the
      ! configuration file.

      ! Correction for RRS is not possible as the Chandrasekhar expression holds for
      ! monochromatic light
    
      implicit none
    
      type(errorType),          intent(inout) :: errS
      type(geometryType),       intent(in)    :: geometryS
      type(optPropRTMGridType), intent(in)    :: optPropRTMGridS
      integer,                  intent(in)    :: iFourier, startLevel, endLevel
      integer,                  intent(in)    :: dimSV_fc, nmutot, nGauss
      real(8),                  intent(in)    :: thresholdMul
      real(8),                  intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(RTType),             intent(in)    :: RT_fc(0:endLevel)
      type(optPartAtm),         intent(inout) :: partAtmUpper(startLevel:endLevel)
      type(optPartAtm),         intent(inout) :: partAtmUpperSurf(startLevel:endLevel)
      integer,             optional, intent(in)    :: iwave
      type(wavelHRType),   optional, intent(in)    :: wavelMRS       ! instr - high resolution wavelength grid
      type(RRS_RingType),  optional, intent(in)    :: RRS_RingS
      type(createLUTType), optional, intent(inout) :: createLUTS     ! for a specific iFourier and iband

      !local
      integer :: ilevel, jlevel, imu, imu0, iSV, jSV, ind, i, i0, ialbedo, iPressureLUT
      real(8) :: Etop(dimSV_fc*nmutot), Ebot(dimSV_fc*nmutot)
      real(8) :: Rsurf(dimSV_fc*nmutot,dimSV_fc*nmutot), Tsurf(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Rstsurf(dimSV_fc*nmutot,dimSV_fc*nmutot), Tstsurf(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Rcom(dimSV_fc*nmutot,dimSV_fc*nmutot), Tcom(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Rstcom(dimSV_fc*nmutot,dimSV_fc*nmutot), Tstcom(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Ucom(dimSV_fc*nmutot,dimSV_fc*nmutot), Dcom(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Ustcom(dimSV_fc*nmutot,dimSV_fc*nmutot), Dstcom(dimSV_fc*nmutot,dimSV_fc*nmutot)

      logical, parameter :: verbose = .false.  ! write to intermediate file
      logical, parameter :: verboseLUT = .true.  ! write to intermediate file

      ! bottom layer is homogeneous
      partAtmUpper(endLevel)%R    = RT_fc(endLevel)%R
      partAtmUpper(endLevel)%Rst  = transform_top_bottom(dimSV_fc, nmutot, RT_fc(endLevel)%R)
      partAtmUpper(endLevel)%T    = RT_fc(endLevel)%T
      partAtmUpper(endLevel)%Tst  = transform_top_bottom(dimSV_fc, nmutot, RT_fc(endLevel)%T)
      ! initialize
      partAtmUpper(endLevel)%U    = RT_fc(endLevel)%R
      partAtmUpper(endLevel)%D    = 0.0d0                 
      partAtmUpper(endLevel)%Ust  = 0.0d0
      partAtmUpper(endLevel)%Dst  = RT_fc(endLevel)%T
        
      do ilevel = endLevel - 1 , startlevel, -1
        ! fill Etop and Ebot for adding the next layer
        ! make an exception for adding the Lambertian cloud/surface
        ! for which the transmission is zero
        if ( ilevel == startlevel ) then
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              Etop(ind) = atten(imu, endLevel, ilevel)
              Ebot(ind) = 0.0d0
            end do ! iSV
          end do ! imu
        else
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              Etop(ind) = atten(imu, endLevel, ilevel    )
              Ebot(ind) = atten(imu, ilevel  , ilevel - 1)
            end do ! iSV
          end do ! imu
        end if

        call addLayerToBottom (errS,  geometryS, iFourier, dimSV_fc, nmutot,             &
                               nGauss, thresholdMul,                                     &
                               Etop, partAtmUpper(ilevel+1)%R, partAtmUpper(ilevel+1)%T, & 
                               partAtmUpper(ilevel+1)%Rst, partAtmUpper(ilevel+1)%Tst,   &
                               Ebot, RT_fc(ilevel)%R, RT_fc(ilevel)%T,                   &
                               RT_fc(ilevel)%Rst, RT_fc(ilevel)%Tst,                     &
                               partAtmUpper(ilevel)%R, partAtmUpper(ilevel)%T,           & 
                               partAtmUpper(ilevel)%U, partAtmUpper(ilevel)%D,           & 
                               partAtmUpper(ilevel)%Rst, partAtmUpper(ilevel)%Tst,       &
                               partAtmUpper(ilevel)%Ust, partAtmUpper(ilevel)%Dst,       &
                               partAtmUpper(ilevel)%tpl, partAtmUpper(ilevel)%s,         &
                               partAtmUpper(ilevel)%tplst, partAtmUpper(ilevel)%sst)

        if (errorCheck(errS)) return
      end do ! ilevel

      if (verbose) then                 ! write results to intermediateFileUnit
        write(intermediateFileUnit, *) 'Adding layer to the bottom of the partial atmosphere'
        do ilevel = startlevel, endlevel
          write(intermediateFileUnit,'(2(A20, I4))') 'level = ', ilevel
          write(intermediateFileUnit,*) 'R'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%R(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Rst'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Rst(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'T'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%T(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Tst'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Tst(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'U'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%U(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Ust'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Ust(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'D'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%D(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Dst'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Dst(imu,imu0), imu0 = 1, nmutot)
          end do
        end do ! ilevel
      end if ! verbose
 
      iPressureLUT = -1
      do ilevel = startlevel, endlevel - 1
        ! store results only for interval pressure levels where RTMweight = 0.0
        if ( abs(optPropRTMGridS%RTMweight(ilevel)) < 1.0d-10 ) then
          iPressureLUT = iPressureLUT + 1
          if ( iFourier == 0 ) then   ! only for azimuth independent part
            createLUTS%s_star(iPressureLUT, iwave) = partAtmUpper(ilevel)%sst
            do imu = 1, nmutot
              createLUTS%td(imu,iPressureLUT, iwave) = partAtmUpper(ilevel)%tpl(imu)
              createLUTS%expt(imu,iPressureLUT, iwave) = atten(imu, endLevel, ilevel)
              do imu0 = 1, nmutot
                i  = dimSV_fc * (imu  - 1) + 1
                i0 = dimSV_fc * (imu0 - 1) + 1
                 ! remove weights for R0
                createLUTS%R0(imu,imu0,iPressureLUT, iwave) = &
                  partAtmUpper(ilevel)%R(i,i0) / geometryS%w(imu0) / geometryS%w(imu)
              end do
            end do
          else
            ! store values for R0 (black surface) in createLUTS for iFourier > 0
            createLUTS%s_star(iPressureLUT, iwave)  = 0.0d0
            do imu = 1, nmutot
              createLUTS%td(imu,iPressureLUT, iwave)   = 0.0d0
              createLUTS%expt(imu,iPressureLUT, iwave) = 0.0d0
              do imu0 = 1, nmutot
                i  = dimSV_fc * (imu  - 1) + 1
                i0 = dimSV_fc * (imu0 - 1) + 1
                 ! remove weights for R0
                createLUTS%R0(imu,imu0,iPressureLUT, iwave) = &
                  partAtmUpper(ilevel)%R(i,i0) / geometryS%w(imu0) / geometryS%w(imu)
              end do
            end do
          end if !iFourier == 0

          if (verboseLUT) then
            ! write entries for LUT to intermediate output, but only for the first wavelength
            write(intermediateFileUnit,*) ' iFourier =', iFourier, ' ilevel = ', ilevel, &
                                          ' iPressureLUT = ', iPressureLUT
            write(intermediateFileUnit,'(A,20F12.8)') ' sstar = ', createLUTS%s_star(iPressureLUT, iwave)
            write(intermediateFileUnit,'(A,20F12.8)') ' expt  = ', &
                                                    (createLUTS%expt(imu,iPressureLUT, iwave), imu=1,nmutot)
            write(intermediateFileUnit,'(A,20F12.8)') ' td    = ', &
                                                    (createLUTS%td(imu,iPressureLUT, iwave), imu=1,nmutot)
            write(intermediateFileUnit,*) ' R0'
            do imu0 = 1, nmutot
              write(intermediateFileUnit,'(20F12.8)') &
                                        (createLUTS%R0(imu,imu0, iPressureLUT, iwave), imu=1,nmutot)
            end do ! imu0
          end if ! verboseLUT

        end if ! optPropRTMGridS%RTMweight(ilevel) < 1.0d-10
      end do ! ilevel
    
    end subroutine addingFromTopDownLUT_Chandra
  
  
    subroutine addingFromTopDownLUT(errS, geometryS, optPropRTMGridS, iFourier,   &
                                 startLevel, endLevel, atten, RT_fc, dimSV_fc, &
                                 nmutot, nGauss, thresholdMul, partAtmUpper,   &
                                 partAtmUpperSurf, iwave, wavelMRS, RRS_RingS, createLUTS)

      ! adding from top down is needed for pseudo-spherical correction
    
      implicit none
    
      type(errorType),          intent(inout) :: errS
      type(geometryType),       intent(in)    :: geometryS
      type(optPropRTMGridType), intent(in)    :: optPropRTMGridS
      integer,                  intent(in)    :: iFourier, startLevel, endLevel
      integer,                  intent(in)    :: dimSV_fc, nmutot, nGauss
      real(8),                  intent(in)    :: thresholdMul
      real(8),                  intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(RTType),             intent(in)    :: RT_fc(0:endLevel)
      type(optPartAtm),         intent(inout) :: partAtmUpper(startLevel:endLevel)
      type(optPartAtm),         intent(inout) :: partAtmUpperSurf(startLevel:endLevel)
      integer,             optional, intent(in)    :: iwave
      type(wavelHRType),   optional, intent(in)    :: wavelMRS       ! instr - high resolution wavelength grid
      type(RRS_RingType),  optional, intent(in)    :: RRS_RingS
      type(createLUTType), optional, intent(inout) :: createLUTS     ! for a specific iFourier and iband

      !local
      integer :: ilevel, jlevel, imu, imu0, iSV, jSV, ind, i, i0, ialbedo, iPressureLUT
      real(8) :: Etop(dimSV_fc*nmutot), Ebot(dimSV_fc*nmutot)
      real(8) :: Rsurf(dimSV_fc*nmutot,dimSV_fc*nmutot), Tsurf(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Rstsurf(dimSV_fc*nmutot,dimSV_fc*nmutot), Tstsurf(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Rcom(dimSV_fc*nmutot,dimSV_fc*nmutot), Tcom(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Rstcom(dimSV_fc*nmutot,dimSV_fc*nmutot), Tstcom(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Ucom(dimSV_fc*nmutot,dimSV_fc*nmutot), Dcom(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Ustcom(dimSV_fc*nmutot,dimSV_fc*nmutot), Dstcom(dimSV_fc*nmutot,dimSV_fc*nmutot)

      type (UDlutType) ::  UD_LUT_fc(0:endLevel)
      integer :: allocStatus, deallocStatus
      integer :: sumAllocStatus, sumdeallocStatus

      logical, parameter :: verbose = .false.  ! write to intermediate file
      logical, parameter :: verboseLUT = .false.

      ! preparation: allocate arrays in UD_LUT_fc if useRRS is true and if a LUT needs to be created 
      if ( RRS_RingS%useRRS ) then
        ! allocate arrays for UD_LUT_fc
        allocStatus = 0
        sumAllocStatus = 0
        do ilevel = 0, endlevel
          if (.not. associated (UD_LUT_fc(ilevel)%E) ) then
            allocate( UD_LUT_fc(ilevel)%E(dimSV_fc*nmutot), STAT = allocStatus)
            sumAllocStatus = sumAllocStatus + allocStatus
          end if
          if (.not. associated (UD_LUT_fc(ilevel)%D) ) then
            allocate( UD_LUT_fc(ilevel)%D(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT = allocStatus)
            sumAllocStatus = sumAllocStatus + allocStatus
          end if
          if (.not. associated (UD_LUT_fc(ilevel)%U) ) then
            allocate( UD_LUT_fc(ilevel)%U(dimSV_fc*nmutot,dimSV_fc*nmutot), STAT = allocStatus)
            sumAllocStatus = sumAllocStatus + allocStatus
          end if
        end do ! ilevel

        if (sumAllocStatus /= 0) then 
            call mystop(errS, 'addingToolsModule: addingFromTopDown has nonzero sumAllocStatus.')
            if (errorCheck(errS)) return
        end if

      end if ! RRS_RingS%useRRS

      ! bottom layer is homogeneous
      partAtmUpper(endLevel)%R    = RT_fc(endLevel)%R
      partAtmUpper(endLevel)%Rst  = transform_top_bottom(dimSV_fc, nmutot, RT_fc(endLevel)%R)
      partAtmUpper(endLevel)%T    = RT_fc(endLevel)%T
      partAtmUpper(endLevel)%Tst  = transform_top_bottom(dimSV_fc, nmutot, RT_fc(endLevel)%T)
      ! initialize
      partAtmUpper(endLevel)%U    = RT_fc(endLevel)%R
      partAtmUpper(endLevel)%D    = 0.0d0                 
      partAtmUpper(endLevel)%Ust  = 0.0d0
      partAtmUpper(endLevel)%Dst  = RT_fc(endLevel)%T
        
      do ilevel = endLevel - 1 , startlevel, -1
        ! fill Etop and Ebot for adding the next layer
        ! make an exception for adding the Lambertian cloud/surface
        ! for which the transmission is zero
        if ( ilevel == startlevel ) then
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              Etop(ind) = atten(imu, endLevel, ilevel)
              Ebot(ind) = 0.0d0
            end do ! iSV
          end do ! imu
        else
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              Etop(ind) = atten(imu, endLevel, ilevel    )
              Ebot(ind) = atten(imu, ilevel  , ilevel - 1)
            end do ! iSV
          end do ! imu
        end if

        call addLayerToBottom (errS,  geometryS, iFourier, dimSV_fc, nmutot,             &
                               nGauss, thresholdMul,                                     &
                               Etop, partAtmUpper(ilevel+1)%R, partAtmUpper(ilevel+1)%T, & 
                               partAtmUpper(ilevel+1)%Rst, partAtmUpper(ilevel+1)%Tst,   &
                               Ebot, RT_fc(ilevel)%R, RT_fc(ilevel)%T,                   &
                               RT_fc(ilevel)%Rst, RT_fc(ilevel)%Tst,                     &
                               partAtmUpper(ilevel)%R, partAtmUpper(ilevel)%T,           & 
                               partAtmUpper(ilevel)%U, partAtmUpper(ilevel)%D,           & 
                               partAtmUpper(ilevel)%Rst, partAtmUpper(ilevel)%Tst,       &
                               partAtmUpper(ilevel)%Ust, partAtmUpper(ilevel)%Dst,       &
                               partAtmUpper(ilevel)%tpl, partAtmUpper(ilevel)%s,         &
                               partAtmUpper(ilevel)%tplst, partAtmUpper(ilevel)%sst)

        if (errorCheck(errS)) return
      end do ! ilevel

      if (verbose) then                 ! write results to intermediateFileUnit
        write(intermediateFileUnit, *) 'Adding layer to the bottom of the partial atmosphere'
        do ilevel = startlevel, endlevel
          write(intermediateFileUnit,'(2(A20, I4))') 'level = ', ilevel
          write(intermediateFileUnit,*) 'R'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%R(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Rst'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Rst(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'T'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%T(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Tst'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Tst(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'U'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%U(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Ust'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Ust(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'D'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%D(imu,imu0), imu0 = 1, nmutot)
          end do
          write(intermediateFileUnit,*) 'Dst'
          do imu = 1, nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmUpper(ilevel)%Dst(imu,imu0), imu0 = 1, nmutot)
          end do
        end do ! ilevel
      end if ! verbose
 
      iPressureLUT = -1
      do ilevel = startlevel, endlevel - 1
        ! store results only for interval pressure levels
        if ( optPropRTMGridS%RTMweight(ilevel) < 1.0d-10 ) then
          iPressureLUT = iPressureLUT + 1
          ! use the adding method to add a Lambertian surface below the atmosphere; only for iFourier == 0
          if ( iFourier == 0 ) then   ! only for azimuth independent part
            do imu = 1, nmutot
              do iSV = 1, dimSV_fc
                ind = iSV + (imu - 1) * dimSV_fc
                Etop(ind) = atten(imu, endLevel, ilevel)
                Ebot(ind) = 0.0d0
              end do ! iSV
            end do ! imu

            do ialbedo = 1, createLUTS%nsurfAlb
              call fillsurface(errS, iFourier, dimSV_fc, nmutot, createLUTS%surfAlb(ialbedo), &
                               geometryS, Rsurf, Tsurf, Rstsurf, Tstsurf)
              call addLayerToBottom (errS,  geometryS, iFourier, dimSV_fc, nmutot,             &
                                     nGauss, thresholdMul,                                     &
                                     Etop, partAtmUpper(ilevel)%R, partAtmUpper(ilevel)%T,     & 
                                     partAtmUpper(ilevel)%Rst, partAtmUpper(ilevel)%Tst,       &
                                     Ebot, Rsurf, Tsurf,                                       &
                                     Rstsurf, Tstsurf,                                         &
                                     Rcom, Tcom, Ucom, Dcom,                                   & 
                                     Rstcom, Tstcom, Ustcom, Dstcom)
              ! store values in partAtmUpper
              partAtmUpperSurf(ilevel)%R(:,:)   = Rcom(:,:)
              partAtmUpperSurf(ilevel)%T(:,:)   = Tcom(:,:)
              partAtmUpperSurf(ilevel)%Rst(:,:) = Rstcom(:,:)
              partAtmUpperSurf(ilevel)%Tst(:,:) = Tstcom(:,:)
              partAtmUpperSurf(ilevel)%U(:,:)   = Ucom(:,:)
              partAtmUpperSurf(ilevel)%D(:,:)   = Dcom(:,:)
              partAtmUpperSurf(ilevel)%Ust(:,:) = Ustcom(:,:)
              partAtmUpperSurf(ilevel)%Dst(:,:) = Dstcom(:,:)

              ! internal radiation field is only needed for rotational Raman scattering
              if ( RRS_RingS%useRRS ) then
                call calcUD_LUT_FromTopDown(errS, ilevel, endLevel, atten, dimSV_fc, nmutot, nGauss,  &
                                            partAtmUpperSurf, UD_LUT_fc)
                do jlevel = startlevel, endlevel 
                  ! copy attenuation
                  do imu = 1, nmutot
                    do iSV = 1, dimSV_fc
                      i = iSV + (imu - 1) * dimSV_fc
                      createLUTS%E(i,jlevel,iPressureLUT, iwave) = UD_LUT_fc(jlevel)%E(i)
                    end do ! imu
                  end do ! iSV
                  ! store internal field in createLUTS
                  do imu = 1, nmutot
                    do iSV = 1, dimSV_fc
                      i  = iSV + (imu - 1) * dimSV_fc
                      do imu0 = 1, nmutot
                        do jSV = 1, dimSV_fc
                          i0 = iSV + (imu0 - 1) * dimSV_fc
                          createLUTS%D(i,i0,jlevel,ialbedo,iPressureLUT, iwave) = UD_LUT_fc(jlevel)%D(i,i0)
                          createLUTS%U(i,i0,jlevel,ialbedo,iPressureLUT, iwave) = UD_LUT_fc(jlevel)%U(i,i0)
! JdH
                          if ( iwave == 1 ) &
                          write(intermediateFileUnit,'(6I3, 2E12.4)') i,i0,jlevel,ialbedo,iPressureLUT, iwave, &
                                                        createLUTS%U(i,i0,jlevel,ialbedo,iPressureLUT, iwave), & 
                                                        createLUTS%D(i,i0,jlevel,ialbedo,iPressureLUT, iwave)
                        end do ! jSV
                      end do ! imu0
                    end do ! iSV
                  end do ! imu
                end do ! jlevel
              end if ! RRS_RingS%useRRS

              ! store reflectance in createLUTS
              do imu = 1, nmutot
                do imu0 = 1, nmutot
                  i  = dimSV_fc * (imu  - 1) + 1
                  i0 = dimSV_fc * (imu0 - 1) + 1
                  createLUTS%R(imu,imu0,ialbedo,iPressureLUT, iwave) = Rcom(i,i0)
                end do ! imu0
              end do ! imu
            end do ! ialbedo
          else
            ! iFourier > 0 => no Lambertian surface added; the values are the same for each surface albedo

            ! internal radiation field is only needed for rotational Raman scattering
            if ( RRS_RingS%useRRS ) then
              call calcUD_LUT_FromTopDown(errS, ilevel, endLevel, atten, dimSV_fc, nmutot, nGauss,  &
                                          partAtmUpper, UD_LUT_fc)

              do jlevel = startlevel, endlevel 
                ! copy attenuation
                do imu = 1, nmutot
                  do iSV = 1, dimSV_fc
                    i = iSV + (imu - 1) * dimSV_fc
                    createLUTS%E(i,jlevel,iPressureLUT, iwave) = UD_LUT_fc(jlevel)%E(i)
                  end do ! imu
                end do ! iSV
                ! store internal field in createLUTS - the same for all albedo values
                do ialbedo = 1, createLUTS%nsurfAlb
                  do imu = 1, nmutot
                    do iSV = 1, dimSV_fc
                      i  = iSV + (imu - 1) * dimSV_fc
                      do imu0 = 1, nmutot
                        do jSV = 1, dimSV_fc
                          i0 = iSV + (imu0 - 1) * dimSV_fc
                          createLUTS%D(i,i0,jlevel,ialbedo,iPressureLUT, iwave) = UD_LUT_fc(jlevel)%D(i,i0)
                          createLUTS%U(i,i0,jlevel,ialbedo,iPressureLUT, iwave) = UD_LUT_fc(jlevel)%U(i,i0)
! JdH        
!                         write(*,'(6I3, 2E12.4)') i,i0,jlevel,ialbedo,iPressureLUT, iwave,  &
!                                     createLUTS%U(i,i0,jlevel,ialbedo,iPressureLUT, iwave), & 
!                                     createLUTS%D(i,i0,jlevel,ialbedo,iPressureLUT, iwave)
                        end do ! jSV
                      end do ! imu0
                    end do ! iSV
                  end do ! imu
                end do ! ialbedo
              end do ! Jlevel
            end if ! RRS_RingS%useRRS

            ! store reflectance in createLUTS (the same for all surface albedo values)
            do ialbedo = 1, createLUTS%nsurfAlb
              do imu = 1, nmutot
                i = dimSV_fc * (imu - 1) + 1
                do imu0 = 1, nmutot
                  i0 = dimSV_fc * (imu0 - 1) + 1
                  createLUTS%R(imu,imu0,ialbedo,iPressureLUT, iwave) = partAtmUpper(ilevel)%R(i,i0)
                end do ! imu0
              end do ! imu
            end do ! ialbedo

          end if ! iFourier == 0
        end if ! optPropRTMGridS%RTMweight(ilevel) < 1.0d-10
      end do ! ilevel

      if (verboseLUT) then                 ! write results to intermediate file
        if ( iwave == 1) then
          write(intermediateFileUnit, *) 'internal radiation field: addingFromTopDown in addingToolsModule'
         ! do iPressureLUT = 1, createLUTS%npressure
          do iPressureLUT = 1, 2
           ! do ialbedo = 1, createLUTS%nsurfAlb
            do ialbedo = 1, 2
              do ilevel = 0, endLevel
                !write(intermediateFileUnit,*) ' iFourier =', iFourier,' ilevel = ', ilevel,          &
                !                                           ' ialbedo =, ialbedo, ' iPressureLUT =', iPressureLUT, &
                !                                           ' iwave =', iwave
                write(intermediateFileUnit,*) ' iFourier =', iFourier, ' ilevel = ', ilevel
                write(intermediateFileUnit,*) ' ialbedo  =', ialbedo, ' iPressureLUT = ', iPressureLUT
                write(intermediateFileUnit,*) 'E, D and U', ' iwave =', iwave
                do i = 1, dimSV_fc*nmutot
                  write(intermediateFileUnit,'(300E15.7)') createLUTS%E(i,ilevel,iPressureLUT, iwave),                    &
                                        (createLUTS%D(i,i0,ilevel,ialbedo,iPressureLUT, iwave), i0 = 1, dimSV_fc*nmutot), &
                                        (createLUTS%U(i,i0,ilevel,ialbedo,iPressureLUT, iwave), i0 = 1, dimSV_fc*nmutot)
                end do ! i
              end do ! ilevel
            end do ! ialbedo
          end do ! iPressureLUT
        end if ! iwave == 1
      end if ! verboseLUT


      ! clean up
      ! deallocate arrays in UD_LUT_fc if useRRS is true and if a LUT is to be created 
      if ( RRS_RingS%useRRS ) then
        ! deallocate arrays for UD_LUT_fc
        deallocStatus = 0
        sumdeallocStatus = 0
        do ilevel = 0, endlevel
          if (associated (UD_LUT_fc(ilevel)%E) ) then
            deallocate( UD_LUT_fc(ilevel)%E, STAT = deallocStatus)
            sumdeallocStatus = sumdeallocStatus + deallocStatus
          end if
          if (associated (UD_LUT_fc(ilevel)%D) ) then
            deallocate( UD_LUT_fc(ilevel)%D, STAT = deallocStatus)
            sumdeallocStatus = sumdeallocStatus + deallocStatus
          end if
          if (associated (UD_LUT_fc(ilevel)%U) ) then
            deallocate( UD_LUT_fc(ilevel)%U, STAT = deallocStatus)
            sumdeallocStatus = sumdeallocStatus + deallocStatus
          end if
        end do ! ilevel

        if (sumdeallocStatus /= 0) then 
            call mystop(errS, 'addingToolsModule: addingFromTopDown has nonzero sumdeallocStatus.')
            if (errorCheck(errS)) return
        end if

      end if ! RRS_RingS%useRRS
    
    end subroutine addingFromTopDownLUT


    subroutine addingFromSurfaceUp(errS, geometryS, iFourier, startLevel, endLevel, &
                                   atten, RT_fc, dimSV_fc, nmutot, nGauss, &
                                   thresholdMul, partAtmLower)
    
      implicit none
    
      type(errorType),  intent(inout)   :: errS
      type(geometryType), intent(in)    :: geometryS
      integer,            intent(in)    :: iFourier, startLevel, endLevel
      integer,            intent(in)    :: dimSV_fc, nmutot, nGauss
      real(8),            intent(in)    :: thresholdMul
      real(8),            intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(RTType),       intent(in)    :: RT_fc(0:endLevel)
      type(optPartAtm), intent(inout)   :: partAtmLower(startLevel:endLevel)
    
      ! local
      integer     :: ilevel, imu, imu0, iSV, ind
      real(8)     :: Etop(dimSV_fc*nmutot), Ebot(dimSV_fc*nmutot)
    
      logical, parameter :: verbose = .false.
    
      ! add layers to the top of the parial atmosphere
      ! beginning at the ground (startlayer = 0)
      
      partAtmLower(startLevel)%R   = RT_fc(startLevel)%R
      partAtmLower(startLevel)%Rst = transform_top_bottom(dimSV_fc, nmutot, RT_fc(startLevel)%R)
      partAtmLower(startLevel)%T   = RT_fc(startLevel)%T
      partAtmLower(startLevel)%Tst = transform_top_bottom(dimSV_fc, nmutot, RT_fc(startLevel)%T)
      ! initialize
      partAtmLower(startLevel)%U    = RT_fc(startLevel)%R
      partAtmLower(startLevel)%D    = 0.0d0                 
      partAtmLower(startLevel)%Ust  = 0.0d0
      partAtmLower(startLevel)%Dst  = 0.0d0
      
      if (verbose) then                 ! write results to file fort.19
          write(intermediateFileUnit, *) 'Adding layer to the top of the partial atmosphere'
          write(intermediateFileUnit,'(2(A20, I4))') 'level = ', startLevel
          write(intermediateFileUnit,*) 'Rcom'
          do imu = 1, dimSV_fc*nmutot
            write(intermediateFileUnit,'(20F12.8)') (partAtmLower(startLevel)%R(imu,imu0), imu0 = 1, dimSV_fc*nmutot)
          end do
      end if
    
      do ilevel = startLevel + 1, endLevel
      
        ! fill Etop and Ebot for adding the next layer
        do imu = 1, nmutot
          do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            Etop(ind) = atten(imu, iLevel, ilevel - 1)
            Ebot(ind) = atten(imu, ilevel - 1  , startLevel)
          end do ! iSV
        end do ! imu

        
        call addLayerToTop  (errS, geometryS, iFourier, dimSV_fc, nmutot, nGauss, thresholdMul, &
                             Etop, Ebot, RT_fc(ilevel)%R, RT_fc(ilevel)%T,           &
                             RT_fc(ilevel)%Rst, RT_fc(ilevel)%Tst,                   &
                             partAtmLower(ilevel-1)%R, partAtmLower(ilevel-1)%T,     &
                             partAtmLower(ilevel  )%R, partAtmLower(ilevel  )%T,     & 
                             partAtmLower(ilevel  )%U, partAtmLower(ilevel  )%D,     &
                             partAtmLower(ilevel  )%tpl, partAtmLower(ilevel  )%s,   &
                             partAtmLower(ilevel  )%tplst, partAtmLower(ilevel  )%sst)
        if (errorCheck(errS)) return
    
        if (verbose) then                 ! write results to intermediate file
            write(intermediateFileUnit, *) 'Adding layer to the top of the partial atmosphere'
            write(intermediateFileUnit,'(2(A20, I4))') 'level = ', ilevel
            write(intermediateFileUnit,*) 'Rcom'
            do imu = 1, dimSV_fc*nmutot
              write(intermediateFileUnit,'(20F12.8)') (partAtmLower(ilevel)%R(imu,imu0), imu0 = 1, dimSV_fc*nmutot)
            end do
        end if
      
      end do ! ilevel loop
    
    end subroutine addingFromSurfaceUp


    subroutine fillsurface(errS, iFourier, dimSV_fc, nmutot, albedo, geometryS, R, T, Rst, Tst)

    ! The surface is a Lambertian reflector with an albedo 'albedo'
    ! As no light is transmitted by the surface: E and T vanish

    ! Values for additional directions corresponding to different axes
    ! are filled also, but are not used.

      implicit none

      type(errorType),    intent(inout) :: errS
      integer,            intent(in)    :: iFourier
      integer,            intent(in)    :: dimSV_fc
      integer,            intent(in)    :: nmutot 
      real(8),            intent(in)    :: albedo
      type(geometryType), intent(in)    :: geometryS
      real(8),            intent(out)   :: R  (dimSV_fc*nmutot, dimSV_fc*nmutot)
      real(8),            intent(out)   :: Rst(dimSV_fc*nmutot, dimSV_fc*nmutot)
      real(8),            intent(out)   :: T  (dimSV_fc*nmutot, dimSV_fc*nmutot)
      real(8),            intent(out)   :: Tst(dimSV_fc*nmutot, dimSV_fc*nmutot)

      ! local
      integer :: imu, imu0, ind, ind0

      R   = 0.0d0
      T   = 0.0d0
      Rst = 0.0d0
      Tst = 0.0d0

      ! Lambertian surface contributes only for  iFourier = 0
      ! only the (1,1) elements is filled because the Lambertian surface depolarizes
      if (iFourier == 0) then
        do imu0 = 1, nmutot
          ind0 = 1 + (imu0 - 1) * dimSV_fc    
          do imu = 1, nmutot
            ind = 1 + (imu - 1) * dimSV_fc 
            R(ind, ind0) = geometryS%w(imu) * albedo * geometryS%w(imu0)
          end do ! imu
        end do ! imu0
      end if ! iFourier == 0

    end subroutine fillsurface

    subroutine calcUD_FromTopDown(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss,  &
                                  partAtmUpper, UD_fc)
    
      implicit none
    
      type(errorType),  intent(inout) :: errS
      integer,          intent(in)    :: dimSV_fc, startLevel, endLevel
      integer,          intent(in)    :: nmutot, nGauss
      real(8),          intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(optPartAtm), intent(in)    :: partAtmUpper(startLevel:endLevel)
      type(UDType),     intent(inout) :: UD_fc(0:endLevel)
    
      ! local
      integer :: ilevel
      integer :: imu, imu0
      integer :: ind, ind0, iSV
      logical, parameter  :: verbose = .false.
    
      ! Use principle of invariance to calculate the internal radiation field
      ! from the internal radiation fields in a partial atmospheres
      ! Let D(k), U(k), Dst(k) and Ust(k) be the internal radiation fields at level k when 
      ! the total atmosphere contains the levels endlevel, endlevel-1, ..., k-1 ,
      ! where the levels endlevel and k-1 form the boundaries of the atmosphere.
      ! These fields are stored in partAtmUpper(k). The radiation fields in the complete
      ! atmosphere at level k are denoted by U_compl(k) and D_compl(k).
      ! The attenuation from level k-1 to level k is given by atten(k-1,k).
      !
      ! The recurrence relations for  U_compl(k) and D_compl(k) are:
      !
      ! D_compl(k) = D(k)                        downward field in the partial atmosphere at k
      !            + Ust(k) U_compl(k-1)         downward radiation due to illumination of level k-1 by U_compl(k-1)
      ! U_compl(k) = U(k)                        upward field in the partial atmosphere at k
      !            + atten(k-1, k) U_compl(k-1)  attenuated upward field due to illumination of level k-1 by U_compl(k-1)
      !            + Dst(k) U_compl(k-1)         scattered light arriving at k traveling upwards due
      !                                          to illumination of level k-1 by U_compl(k-1)
      ! The relations give the internal radiation field at k in terms of the radiation field at k-1.
      ! Hence we have to start the recurrence at the surface
    
      ! initialize to zero
      do ilevel = 0, startLevel
        UD_fc(ilevel)%E = 0.0d0
        UD_fc(ilevel)%U = 0.0d0
        UD_fc(ilevel)%D = 0.0d0
      end do
    
      ! copy attenuation to UD
      do ilevel = startLevel, endLevel
        do imu = 1, nmutot
          do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            UD_fc(ilevel)%E(ind) = atten(imu,endLevel,ilevel)
          end do ! imu
        end do ! iSV
      end do
    
      ! initialize for surface 
      do imu0 = 1, 2
        ind0 = 1 + (imu0 -1) * dimSV_fc
        do ind = 1, dimSV_fc * nmutot
          UD_fc(startLevel)%D(ind,imu0) = partAtmUpper(startLevel)%D(ind,dimSV_fc*nGauss+ind0) 
          UD_fc(startLevel)%U(ind,imu0) = partAtmUpper(startLevel)%U(ind,dimSV_fc*nGauss+ind0) 
        end do ! ind
      end do ! imu0
    
      ! loop for the levels above the Lambertian surface
    
      do ilevel = startLevel + 1, endLevel
        do imu0 = 1, 2
          ind0 = 1 + (imu0 - 1) * dimSV_fc
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              UD_fc(ilevel)%D(ind,imu0) =                                     &
                dot_product(partAtmUpper(ilevel)%Ust(ind,1:dimSV_fc*nGauss),  &
                            UD_fc(ilevel-1)%U(1:dimSV_fc*nGauss,imu0) )       &
                + partAtmUpper(ilevel)%D(ind, dimSV_fc*nGauss+ind0)
              UD_fc(ilevel)%U(ind,imu0) =                                     &
                dot_product(partAtmUpper(ilevel)%Dst(ind,1:dimSV_fc*nGauss),  &
                            UD_fc(ilevel-1)%U(1:dimSV_fc*nGauss,imu0) )       &
                + atten(imu, ilevel-1, ilevel) * UD_fc(ilevel-1)%U(ind,imu0)  &
                + partAtmUpper(ilevel)%U(ind, dimSV_fc*nGauss+ind0)
            end do ! iSV
          end do ! imu
        end do ! imu0
      end do ! ilevel
    
      if (verbose) then                 ! write results to intermediate file
        write(intermediateFileUnit, *) 'Calculating internal radiation field: calcUD_FromTopDown'
        do ilevel = 0, endLevel
          write(intermediateFileUnit,'(2(A20, I4))') 'level = ', ilevel
          write(intermediateFileUnit,*) 'E, D and U'
          do imu = 1, dimSV_fc*nmutot
            write(intermediateFileUnit,'(5E15.7)')   UD_fc(ilevel)%E(imu), &
                                                    (UD_fc(ilevel)%D(imu,imu0), imu0 = 1, 2), &
                                                    (UD_fc(ilevel)%U(imu,imu0), imu0 = 1, 2)
          end do
        end do ! ilevel
      end if
    
    end subroutine calcUD_FromTopDown
    

    subroutine calcUD_LUT_FromTopDown(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss,  &
                                  partAtmUpper, UD_LUT_fc)
    
      implicit none
    
      type(errorType),  intent(inout) :: errS
      integer,          intent(in)    :: dimSV_fc, startLevel, endLevel
      integer,          intent(in)    :: nmutot, nGauss
      real(8),          intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(optPartAtm), intent(in)    :: partAtmUpper(startLevel:endLevel)
      type(UDlutType),  intent(inout) :: UD_LUT_fc(0:endLevel)
    
      ! local
      integer :: ilevel
      integer :: imu, imu0
      integer :: ind, ind0, iSV, iSV0
      logical, parameter  :: verbose = .true.
!      logical, parameter  :: verbose = .false.
    
      ! Use principle of invariance to calculate the internal radiation field
      ! from the internal radiation fields in a partial atmospheres
      ! Let D(k), U(k), Dst(k) and Ust(k) be the internal radiation fields at level k when 
      ! the total atmosphere contains the levels endlevel, endlevel-1, ..., k-1 ,
      ! where the levels endlevel and k-1 form the boundaries of the atmosphere.
      ! These fields are stored in partAtmUpper(k). The radiation fields in the complete
      ! atmosphere at level k are denoted by U_compl(k) and D_compl(k).
      ! The attenuation from level k-1 to level k is given by atten(k-1,k).
      !
      ! The recurrence relations for  U_compl(k) and D_compl(k) are:
      !
      ! D_compl(k) = D(k)                        downward field in the partial atmosphere at k
      !            + Ust(k) U_compl(k-1)         downward radiation due to illumination of level k-1 by U_compl(k-1)
      ! U_compl(k) = U(k)                        upward field in the partial atmosphere at k
      !            + atten(k-1, k) U_compl(k-1)  attenuated upward field due to illumination of level k-1 by U_compl(k-1)
      !            + Dst(k) U_compl(k-1)         scattered light arriving at k traveling upwards due
      !                                          to illumination of level k-1 by U_compl(k-1)
      ! The relations give the internal radiation field at k in terms of the radiation field at k-1.
      ! Hence we have to start the recurrence at the surface

! JdH Debug
      if (verbose) then                 ! write results to intermediate file
        write(intermediateFileUnit, *) 'Enter calculation of internal radiation field for LUT'
        write(intermediateFileUnit,*)  'partial atmospheres: D and U'
        do ilevel = startLevel, endLevel
          write(intermediateFileUnit,'(2(A20, I4))') 'level = ', ilevel
!          do imu = 1, dimSV_fc*nmutot
          do imu = 1, 1
            write(intermediateFileUnit,'(300E15.7)') (partAtmUpper(ilevel)%D(imu,imu0), imu0 = 1, dimSV_fc*nmutot), &
                                                     (partAtmUpper(ilevel)%U(imu,imu0), imu0 = 1, dimSV_fc*nmutot)
          end do
        end do ! ilevel
      end if ! verbose
    
      ! initialize to zero
      do ilevel = 0, startLevel
        UD_LUT_fc(ilevel)%E = 0.0d0
        UD_LUT_fc(ilevel)%U = 0.0d0
        UD_LUT_fc(ilevel)%D = 0.0d0
      end do
    
      ! copy attenuation to UD
      do ilevel = startLevel, endLevel
        do imu = 1, nmutot
          do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            UD_LUT_fc(ilevel)%E(ind) = atten(imu,endLevel,ilevel)
          end do ! imu
        end do ! iSV
      end do
    
      ! initialize for surface 
      do ind0 = 1, dimSV_fc * nmutot
        do ind = 1, dimSV_fc * nmutot
          UD_LUT_fc(startLevel)%D(ind,ind0) = partAtmUpper(startLevel)%D(ind, ind0) 
          UD_LUT_fc(startLevel)%U(ind,ind0) = partAtmUpper(startLevel)%U(ind, ind0) 
        end do ! ind
      end do ! imu0
    
      ! loop for the levels above the Lambertian surface
    
      do ilevel = startLevel + 1, endLevel
        do imu0 = 1, nmutot
          do iSV0 = 1, dimSV_fc
            ind0 = iSV0 + (imu0 - 1) * dimSV_fc
            do imu = 1, nmutot
              do iSV = 1, dimSV_fc
                ind = iSV + (imu - 1) * dimSV_fc
                UD_LUT_fc(ilevel)%D(ind,imu0) =                                 &
                  dot_product(partAtmUpper(ilevel)%Ust(ind,1:dimSV_fc*nGauss),  &
                              UD_LUT_fc(ilevel-1)%U(1:dimSV_fc*nGauss,ind0) )   &
                  + partAtmUpper(ilevel)%D(ind, ind0)
                UD_LUT_fc(ilevel)%U(ind,imu0) =                                     &
                  dot_product(partAtmUpper(ilevel)%Dst(ind,1:dimSV_fc*nGauss),      &
                              UD_LUT_fc(ilevel-1)%U(1:dimSV_fc*nGauss,ind0) )       &
                  + atten(imu, ilevel-1, ilevel) * UD_LUT_fc(ilevel-1)%U(ind,ind0)  &
                  + partAtmUpper(ilevel)%U(ind, ind0)
              end do ! iSV
            end do ! imu
          end do ! iSV0
        end do ! imu0
      end do ! ilevel
    
      if (verbose) then                 ! write results to intermediate file
        write(intermediateFileUnit, *) 'Calculating internal radiation field: calcUD_FromTopDown'
        do ilevel = startLevel, endLevel
          write(intermediateFileUnit,'(2(A20, I4))') 'level = ', ilevel
          write(intermediateFileUnit,*) 'E, D_part, D and U'
!          do imu = 1, dimSV_fc*nmutot
          do imu = 1, 1
            write(intermediateFileUnit,'(300E15.7)') UD_LUT_fc(ilevel)%E(imu),                &
                                  (partAtmUpper(ilevel)%D(imu,imu0), imu0 = 1, dimSV_fc*nmutot), &
                                  (UD_LUT_fc(ilevel)%D(imu,imu0), imu0 = 1, dimSV_fc*nmutot), &
                                  (UD_LUT_fc(ilevel)%U(imu,imu0), imu0 = 1, dimSV_fc*nmutot)
          end do
        end do ! ilevel
      end if
    
    end subroutine calcUD_LUT_FromTopDown
        
    subroutine calcUDsumLocal(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss, & 
                              RT_fc, UD_fc, UDsumLocal_fc)
    
      ! Calculate the local internal radiation field (summed over all orders of scattering)
      ! which is needed to correct the derivatives for a spherical geometry.
    
      implicit none
    
      type(errorType), intent(inout) :: errS
      integer,          intent(in)    :: startLevel, endLevel
      integer,          intent(in)    :: dimSV_fc, nmutot, nGauss
      real(8),          intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(RTType),     intent(in)    :: RT_fc(0:endLevel)
      type(UDType),     intent(in)    :: UD_fc(0:endLevel)
      type(UDLocalType),intent(inout) :: UDsumLocal_fc(0:endLevel)
    
      ! local
      integer  :: ilevel
      integer  :: imu, imu0
      integer  :: ind, ind0, iSV
    
      ! The name originates from Labos. Only the upward radation is needed.
      ! It follows from
      ! U_local(k) = R(k) E(K) + R(k) D(k) + T*(k) U(k-1)
      ! and U_local(0) = U(0)
      ! where U and D are the internal radiation fields in the complete atmosphere: UD_fc
      ! E(k) is the attenuation from the top of the atmosphere to level k
      ! R(k) is the reflectance of layer k, i.e. the layer bounded by the levels k-1 and k
      ! T*(k) is the diffuse transmission of layer k for illumination from below
      ! As layer k is homogeneous we have T*)k) = T(k)
    
      ! D_local is not needed: set is to zero
      do ilevel = 0, endLevel
        UDsumLocal_fc(ilevel)%D = 0.0            
      end do
    
      do ilevel = 0, startLevel-1
        UDsumLocal_fc(ilevel)%U = 0.0            
      end do
    
      UDsumLocal_fc(startLevel)%U = UD_fc(startLevel)%U
    
      do ilevel = startlevel + 1, endlevel
        do imu0 = 1, 2
          ind0 = 1 + (imu0 - 1) * dimSV_fc
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            UDsumLocal_fc(ilevel)%U(ind,imu0) =                         &
              dot_product(RT_fc(ilevel)%R(ind,1:dimSV_fc*nGauss),       &
                          UD_fc(ilevel)%D(1:dimSV_fc*nGauss,imu0) )     &
            + dot_product(RT_fc(ilevel)%Tst(ind,1:dimSV_fc*nGauss),     &
                          UD_fc(ilevel-1)%U(1:dimSV_fc*nGauss,imu0) )   &
            + RT_fc(ilevel)%R(ind,dimSV_fc*nGauss+ind0) * atten(nGauss+imu0, endLevel, ilevel)
            end do ! iSV
          end do ! imu
        end do ! imu0
      end do ! ilevel loop
    
    end subroutine calcUDsumLocal
    
    
    subroutine calcUD_FromSurfaceUp(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss,  &
                                    partAtmLower, UD_fc)
    
      ! Use principle of invariance to calculate the internal radiation field
      ! from the internal radiation fields in a partial atmospheres
      ! D_compl(k) = D(k,k+1) E(k+1) + E(k,k+1) D_compl(k+1) + D(k,k+1) D_compl(k+1)
      ! U_compl(k) = U(k,k+1) E(k+1) + U(k,k+1) D_compl(k+1)
      ! where E(k+1) is he attenuation from the top of the atmosphere to level k+1
    
      implicit none
    
      type(errorType), intent(inout) :: errS
      integer,          intent(in)    :: startLevel, endLevel
      integer,          intent(in)    :: dimSV_fc, nmutot, nGauss
      real(8),          intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(optPartAtm), intent(in)    :: partAtmLower(startLevel:endLevel)
      type(UDType),     intent(inout) :: UD_fc(0:endLevel)
    
      ! local
      integer :: ilevel
      integer :: imu, imu0, iSV
      integer :: ind, ind0
      logical, parameter  :: verbose = .false.
    
      ! copy attenuation to UD
      do ilevel = startLevel, endLevel
        do imu = 1, nmutot
          do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            UD_fc(ilevel)%E(ind) = atten(imu,endLevel,ilevel)
          end do ! imu
        end do ! iSV
      end do
    
      ! start at the top of the atmosphere
    
      UD_fc(endLevel)%E = 1.0d0
      UD_fc(endLevel)%D = 0.0d0
      do imu0 = 1, 2
        ind0 = 1 + (imu0 - 1) * dimSV_fc
        do imu = 1, nmutot
          do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            UD_fc(endLevel)%U(ind,imu0) = partAtmLower(endLevel)%R(ind,dimSV_fc*nGauss+ind0)
          end do ! iSV
        end do ! imu
      end do ! imu0
    
      ! loop for the levels below endlevel
      
      do ilevel = endlevel - 1, startlevel, -1
        do imu0 = 1, 2
          ind0 = 1 + (imu0 - 1) * dimSV_fc
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              UD_fc(ilevel)%D(ind,imu0) =                                      &
                  dot_product(partAtmLower(ilevel+1)%D(ind,1:dimSV_fc*nGauss), &
                              UD_fc(ilevel+1)%D(1:dimSV_fc*nGauss,imu0) )      &
                + atten(imu, ilevel, ilevel+1) * UD_fc(ilevel+1)%D(ind,imu0)   &
                + partAtmLower(ilevel+1)%D(ind,dimSV_fc*nGauss+ind0)           &
                * atten(nGauss+imu0, endlevel, ilevel+1)
              UD_fc(ilevel)%U(ind,imu0) =                                      &
                  dot_product(partAtmLower(ilevel+1)%U(ind,1:dimSV_fc*nGauss), &
                              UD_fc(ilevel+1)%D(1:dimSV_fc*nGauss,imu0) )      &
                + partAtmLower(ilevel+1)%U(ind,dimSV_fc*nGauss+ind0)           &
                * atten(nGauss+imu0, endlevel, ilevel+1)
            end do ! iSV
          end do ! imu
        end do ! imu0
      end do ! ilevel loop
    
      ! set remaining values to zero
      do ilevel = 0, startLevel - 1
        UD_fc(ilevel)%E = 0.0d0
        UD_fc(ilevel)%U = 0.0d0
        UD_fc(ilevel)%D = 0.0d0
      end do
    
      if (verbose) then                 ! write results to intermediate file
        write(intermediateFileUnit, *) 'Calculating internal radiation field: calcUD_FromSurfaceUp'
        do ilevel = 0, endLevel
          write(intermediateFileUnit,'(2(A20, I4))') 'level = ', ilevel
          write(intermediateFileUnit,*) 'E, D and U'
          do imu = 1, dimSV_fc*nmutot
            write(intermediateFileUnit,'(5E15.7)')   UD_fc(ilevel)%E(imu), &
                                                    (UD_fc(ilevel)%D(imu,imu0), imu0 = 1, 2), &
                                                    (UD_fc(ilevel)%U(imu,imu0), imu0 = 1, 2)
          end do
        end do ! ilevel
      end if
    
    end subroutine calcUD_FromSurfaceUp
  
  
    function smul(nmutot, nGauss, thresholdMul, a, b)
  
    ! supermatrix multiplication
  
      implicit none
      integer,                           intent(in)  :: nmutot, nGauss
      real(8),                           intent(in)  :: thresholdMul
      real(8), dimension(nmutot,nmutot), intent(in)  :: a, b
      real(8), dimension(nmutot,nmutot)              :: smul
  
      integer :: k
      real(8) :: Tra, Trb, Trab
  
      ! initial output to zero
      smul = 0.0d0
  
      ! calculate traces of the input matrices
      Tra = 0.0d0
      Trb = 0.0d0
      do k = 1, nGauss
        Tra = Tra + a(k,k)     
        Trb = Trb + b(k,k)                
      end do
      Trab = abs(Tra * Trb)
  
      ! fill all values
      if (Trab >  thresholdMul) then
        smul = MATMUL(a(:, 1:nGauss), b(1:nGauss, :))
      end if
  
    end function smul
  
  
    function esmul(nmutot, e, a)
  
    ! diagonal times supermatrix
  
      implicit none
  
      integer,                           intent(in) :: nmutot
      real(8), dimension(nmutot),        intent(in) :: e
      real(8), dimension(nmutot,nmutot), intent(in) :: a
      real(8), dimension(nmutot,nmutot)             :: esmul
  
      integer :: imu0
  
      do imu0 = 1, nmutot
          esmul(:, imu0) = e(:)*a(:, imu0)
      end do
  
      return
  
    end function esmul
  
  
    function semul(nmutot, a, e)
  
    ! supermatrix times diagonal
  
      implicit none
  
      integer,                           intent(in) :: nmutot
      real(8), dimension(nmutot),        intent(in) :: e
      real(8), dimension(nmutot,nmutot), intent(in) :: a
      real(8), dimension(nmutot,nmutot)             :: semul
  
      integer :: imu
  
      do imu = 1, nmutot
          semul(imu, :) = a(imu, :)*e(:)
      end do
  
      return
  
    end function semul
  
  
    function Qseries(errS, nmutot, nGauss, thresholdMul, a, b)
  
    ! evaluate repeated reflections: a*b + a*b*a*b + .......
    ! by using LU decoposition to find the inverse
    ! see Appendix F in "Transfer of polarized light in
    ! Planetary atmospheres", J.W. Hovenier, C.V.M. van
    ! der Mee, and H. Domke. 2004, Kluwer Academic Pobl.
    ! pp. 258
  
      implicit none
  
      type(errorType), intent(inout) :: errS
      integer,                           intent(in)  :: nmutot, nGauss
      real(8), dimension(nmutot,nmutot), intent(in)  :: a, b
      real(8),                           intent(in)  :: thresholdMul
      real(8), dimension(nmutot,nmutot)              :: Qseries, ab
  
      ! local
      integer     :: i, j, k
      integer     :: indx(nGauss)     ! index that keeps count of changes due to pivots
      integer     :: status_LUdecomp  ! status of LU decomposition
      real(8)     :: d                ! odd(-1) or even(+1) number of row interchanges
      real(8)     :: col(nGauss)
      real(8)     :: Trab
  
      real(8), parameter :: ThresholdQ = 1.0d-3
  
      real(8), dimension(          nGauss,          nGauss) :: ab_gg, one_minus_ab_gg
      real(8), dimension(          nGauss,          nGauss) :: one, inverse
      real(8), dimension( nmutot - nGauss,          nGauss) :: ab_ag, tmp
      real(8), dimension(          nGauss, nmutot - nGauss) :: ab_ga
      real(8), dimension( nmutot - nGauss, nmutot - nGauss) :: ab_aa
  
      logical, parameter :: verbose = .false.
  
  
      ab =  smul(nmutot, nGauss, thresholdMul, a, b)
  
      ! calculate trace
      Trab = 0.0d0
      do k = 1, nGauss
        Trab = Trab + ab(k,k)     
      end do
  
      if (verbose) then
        write(19,*) 'ab'
        do i = 1, nmutot
          write(19,'(10E20.7)')  (ab(i,j), j = 1, nmutot)
        end do
      end if
  
      ! one term of the Q series suffices if Trace(ab) < ThresholdQ
      if ( abs(Trab) < ThresholdQ ) then
        Qseries = ab
        return
      end if
  
      ! more terms are needed: determine the inverse
      ! identify the varous sections      
      ab_gg = ab(        1:nGauss,        1:nGauss )
      ab_ag = ab(nGauss +1:nmutot,        1:nGauss )
      ab_ga = ab(        1:nGauss,nGauss +1:nmutot)   
      ab_aa = ab(nGauss +1:nmutot,nGauss +1:nmutot)   
  
      ! find the inverse of 1 - ab_gg
  
      one = 0.0d0
      do k = 1, nGauss
        one(k,k) = 1.0d0
      end do
      one_minus_ab_gg = one - ab_gg
  
      ! one_minus_ab_gg will be overwritten by its LU decomposed array
      call LU_decomposition(errS,  one_minus_ab_gg, indx, d, status_LUdecomp)
      if (errorCheck(errS)) return
  
      do k = 1, nGauss
        col(1:nGauss) = one(1:nGauss, k)
        call solve_lin_system_LU_based(errS, one_minus_ab_gg, indx, col)
        if (errorCheck(errS)) return
        inverse(1:nGauss, k) = col(1:nGauss)
      end do
  
      Qseries(1:nGauss,1:nGauss ) = inverse - one
  
      Qseries(1:nGauss, nGauss +1:nmutot) = MATMUL(inverse, ab_ga)
  
      tmp = MATMUL(ab_ag, inverse)
      Qseries(nGauss +1:nmutot, 1:nGauss ) = tmp
  
      Qseries(nGauss +1:nmutot, nGauss +1:nmutot) = MATMUL(tmp, ab_ga) &
                                                   + ab_aa
      if (verbose) then
        write(19,*) 'Qseries'
        do i = 1, nmutot
          write(19,'(10E20.7)')  (Qseries(i,j), j = 1, nmutot)
        end do
      end if
  
    end function Qseries

end module addingTools
