!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

  module verifyConfigFileModule

    ! This module contains subroutines to read and echo the configuration file

  use dataStructures
  use mathTools,                only: verfy_elem_differ

  ! default is private
  private 
  public  :: verifyConfigFile

  real(8), parameter   :: PI = 3.141592653589793d0
  real(8), parameter   :: DUToCm2 = 2.68668d16

  contains

    subroutine verifyConfigFile(errS, operational,        &
               maxFourierTermLUT,                         &
               numSpectrBands, nTrace, ncolumn,           &
               wavelInstrRadSimS, wavelInstrRadRetrS,     &
               cloudAerosolRTMgridSimS,                   &
               cloudAerosolRTMgridRetrS,                  &
               createLUTSimS, createLUTRetrS,             &
               XsecHRLUTSimS, XsecHRLUTRetrS,             &
               gasPTSimS, gasPTRetrS,                     &
               geometrySimS, geometryRetrS,               &
               traceGasSimS, traceGasRetrS,               &
               mieAerSimS, mieAerRetrS,                   &
               mieCldSimS, mieCldRetrS,                   &
               surfaceSimS, surfaceRetrS,                 &
               LambCloudRetrS, LambAerRetrS,              &
               controlSimS, controlRetrS,                 &
               RRS_RingSimS, RRS_RingRetrS,               &
               calibErrorReflS, polCorrectionRetrS,       &
               columnSimS, columnRetrS,                   &
               weakAbsRetrS, retrS, earthRadianceSimS)

     ! verifies the settings in the configuration file

       implicit none

       type(errorType),    intent(inout) :: errS
       integer,            intent(inout) :: operational       ! 1 = yes; 0 = no
       integer,            intent(inout) :: maxFourierTermLUT
       integer,            intent(inout) :: numSpectrBands
       integer,            intent(inout) :: nTrace
       integer,            intent(inout) :: ncolumn

       type(wavelInstrType),     pointer :: wavelInstrRadSimS(:), wavelInstrRadRetrS(:)
       type(cloudAerosolRTMgridType)     :: cloudAerosolRTMgridSimS
       type(cloudAerosolRTMgridType)     :: cloudAerosolRTMgridRetrS
       type(createLUTType)               :: createLUTSimS(maxFourierTermLUT, numSpectrBands)
       type(createLUTType)               :: createLUTRetrS(maxFourierTermLUT, numSpectrBands)
       type(XsecLUTType),        pointer :: XsecHRLUTSimS(:,:), XsecHRLUTRetrS(:,:)
       type(gasPTType)                   :: gasPTSimS, gasPTRetrS
       type(geometryType)                :: geometrySimS, geometryRetrS
       type(traceGasType),       pointer :: traceGasSimS(:), traceGasRetrS(:)
       type(mieScatType)                 :: mieAerSimS(maxNumMieModels), mieAerRetrS(maxNumMieModels)
       type(mieScatType)                 :: mieCldSimS(maxNumMieModels), mieCldRetrS(maxNumMieModels)
       type(LambertianType),     pointer :: surfaceSimS(:), surfaceRetrS(:)
       type(LambertianType),     pointer :: LambCloudRetrS(:), LambAerRetrS(:)
       type(controlType)                 :: controlSimS, controlRetrS
       type(RRS_RingType),       pointer :: RRS_RingSimS(:)
       type(RRS_RingType),       pointer :: RRS_RingRetrS(:)
       type(calibrReflType),     pointer :: calibErrorReflS(:)
       type(polCorrectionType),  pointer :: polCorrectionRetrS(:)
       type(columnType),         pointer :: columnSimS(:)
       type(columnType),         pointer :: columnRetrS(:)
       type(weakAbsType),        pointer :: weakAbsRetrS(:)
       type(retrType)                    :: retrS
       type(earthRadianceType),  pointer :: earthRadianceSimS(:)


       ! local
       integer  :: iband, iTrace, jTrace, ipair, iwave, imodel, icolumn, ipressure, nwavel
       real(8)  :: startWavel, endWavel
       integer  :: iinterval
       real(8)  :: tau

       ! local variables to check that pressure-temperature grid covers the atmosphere
       real(8)  :: pressureAtTopSim, pressureAtTopRetr
       real(8)  :: pressureAtBotSim, pressureAtBotRetr

       ! local variables for DOMINO
       logical  :: NO2_present, trop_NO2_present, strat_NO2_present
       integer  :: lenghtSimNO2, lenghtSimStratNO2, lenghtSimTropNO2
       integer  :: lenghtRetrNO2, lenghtRetrStratNO2, lenghtRetrTropNO2
       real(8)  :: pressureNO2, pressureStratNO2, pressureTropNO2

       ! set local variables as shorthand
       pressureAtTopSim  = cloudAerosolRTMgridSimS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%ninterval)
       pressureAtTopRetr = cloudAerosolRTMgridRetrS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%ninterval)
       pressureAtBotSim  = cloudAerosolRTMgridSimS%intervalBoundsAP_P(0)
       pressureAtBotRetr = cloudAerosolRTMgridRetrS%intervalBoundsAP_P(0)

       ! the temperatures specified at the nodes must cover the atmosphere
       if ( gasPTSimS%pressureNodes(gasPTSimS%npressureNodes) > pressureAtTopSim ) then
         call logDebug('ERROR in configuration file: temperature does not cover the atmosphere')
         call logDebug('temperature for simulation does not cover the top of the atmosphere')
         write(errS%temp,'(A,F12.6)') 'lowest pressure read = ', gasPTSimS%pressureNodes(gasPTSimS%npressureNodes)
         call errorAddLine(errS, errS%temp)
         write(errS%temp,'(A,F12.6)') 'pressure at TOA      = ', pressureAtTopSim
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped because temperature does not cover the atmosphere')         
         if (errorCheck(errS)) return
       end if
       if ( gasPTRetrS%pressureNodes(gasPTRetrS%npressureNodes) > pressureAtTopRetr ) then
         call logDebug('ERROR in configuration file: temperature does not cover the atmosphere')
         call logDebug('temperature for retrieval does not cover the top of the atmosphere')
         write(errS%temp,'(A,F12.6)') 'lowest pressure read = ', gasPTRetrS%pressureNodes(gasPTRetrS%npressureNodes)
         call errorAddLine(errS, errS%temp)
         write(errS%temp,'(A,F12.6)') 'pressure at TOA      = ', pressureAtTopRetr
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped because temperature does not cover the atmosphere')         
         if (errorCheck(errS)) return
       end if
       if ( gasPTSimS%pressureNodes(0) + 1.0 < pressureAtBotSim ) then
         call logDebug('ERROR in configuration file: temperature does not cover the atmosphere')
         call logDebug('temperature for simulation does not cover the surface')
         write(errS%temp,'(A,F12.6)') 'highest pressure read = ', gasPTSimS%pressureNodes(0)
         call errorAddLine(errS, errS%temp)
         write(errS%temp,'(A,F12.6)') 'pressure at surface   = ', pressureAtBotSim
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped because temperature does not cover the atmosphere')         
         if (errorCheck(errS)) return
       end if
       if ( gasPTRetrS%pressureNodes(0) + 1.0 < pressureAtBotRetr ) then
         call logDebug('ERROR in configuration file: temperature does not cover the atmosphere')
         call logDebug('temperature for retrieval does not cover the surface')
         write(errS%temp,'(A,F12.6)') 'highest pressure read = ', gasPTRetrS%pressureNodes(0)
         call errorAddLine(errS, errS%temp)
         write(errS%temp,'(A,F12.6)') 'pressure at surface   = ', pressureAtBotRetr
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped because temperature does not cover the atmosphere')         
         if (errorCheck(errS)) return
       end if

       ! give error when DISMAS or DOAS is used for line absorbing species
       if ( controlSimS%method == 1 .OR.  controlRetrS%method == 1 .OR. controlRetrS%method == 2) then
         do iTrace = 1, nTrace
           select case ( trim(traceGasRetrS(iTrace)%nameTraceGas) )
             case ('H2O', 'NH3', 'CO', 'O2', 'CH4', 'CO2')
               call logDebug('ERROR: line absorbing species is combined with DISMAS(method=1)')
               call logDebug('or DOAS (method=2)')
               call logDebug('absorbing gas: '// trim(traceGasRetrS(iTrace)%nameTraceGas))
               call logDebug('line absorbing species are: H2O, NH3, CO, O2, CH4, CO2')
               call mystop(errS, 'stopped because DISMAS can not be used for line absorbing species')
               if (errorCheck(errS)) return
           end select
         end do ! iTrace
       end if ! controlSimS%method > 0

       if ( controlRetrS%method == 3 .or. controlRetrS%method == 4 ) then
         ! check flags for classical DOAS and DOMINO
         do iband = 1, numSpectrBands 
           if ( (.not. weakAbsRetrS(iband)%calculateAMF) .and. (.not. weakAbsRetrS(iband)%useReferenceTempRetr) ) then
             write(errS%temp,*)
             call errorAddLine(errS, errS%temp)
             call logDebug('ERROR: in configuration file: specification for classical DOAS is incorrect')
             call logDebug('reference temperature is not used and air mass factor is not calculated')
             call logDebug('hence, the altitude averaged absorption cross section can not be calculated')
             call logDebug('change flags in the configuration file in section GENERAL')
             call logDebug('subsection specifications_DOAS_DISMAS')
             call logDebug('keywords: useReferenceTempRetr and calculateAMFRetr')
             call logDebug('one of these keywords must be set to 1')
             call mystop(errS, 'stopped because of specification error in configuration file')
             if (errorCheck(errS)) return
           end if
         end do ! iband
       end if

       ! give warning or error when DOAS slant column is used for line absorbing species
       if ( controlRetrS%method == 3 .or. controlRetrS%method == 4 ) then
         do iTrace = 1, nTrace
           select case ( trim(traceGasRetrS(iTrace)%nameTraceGas) )
             case ('H2O')
               call logDebug('WARNING: line absorbing species is combined with DOAS or DOMINO)')
               call logDebug('absorbing gas: '// trim(traceGasRetrS(iTrace)%nameTraceGas))
               call logDebug('line absorbing species are: H2O, NH3, CO, O2, CH4, CO2')
               call logDebug('results will be incorrect unless the absorption is very weak')
               call logDebug('the absorbing gas must not affect the reflectance')
               call logDebug('by more than a few percent')
             case ('CO', 'O2', 'CH4', 'CO2', 'NH3')
               call logDebug('ERROR: line absorbing species is combined with DOAS (method=3)')
               call logDebug('absorbing gas: '// trim(traceGasRetrS(iTrace)%nameTraceGas))
               call logDebug('line absorbing species are: H2O, NH3, CO, O2, CH4, CO2')
               call logDebug('use OE lbl (method = 0)')
               call mystop(errS, 'stopped because DOAS (method 3) can not be used for  NH3, CO, O2, CH4, or CO2')
               if (errorCheck(errS)) return
           end select
         end do ! iTrace
       end if ! controlRetrS%method == 3 .or. controlRetrS%method == 4

       ! do not continue when method < 4 and NO2, strat_NO2, and trop_NO2 are present
       ! as this combination is only useful for DOMINO
       if ( controlRetrS%method < 4 ) then
         NO2_present       = .false.
         trop_NO2_present  = .false.
         strat_NO2_present = .false.
         do iTrace = 1, nTrace
           select case ( trim(traceGasRetrS(iTrace)%nameTraceGas) )
             case ('NO2')
               NO2_present       = .true.
             case ('trop_NO2')
               trop_NO2_present  = .true.
             case ('strat_NO2')
               strat_NO2_present = .true.
           end select
         end do ! iTrace

         if ( NO2_present .and. trop_NO2_present .and. strat_NO2_present) then
               call logDebug('ERROR: configuration file is meant for DOMINO')
               call logDebug('but DOMINO is not selected as retrieval method')
               call logDebug('three NO2 species are present: NO2, trop_NO2, and strat_NO2')
               call logDebug('this can only be used for DOMINO')
               call mystop(errS, 'stopped because DOMINO is not used as retrieval method')
               if (errorCheck(errS)) return
         end if ! NO2, trop_NO2, and strat_NO2 are all present
       end if ! controlRetrS%method < 4

       ! set conditions for DOMINO

       if ( controlRetrS%method == 4 ) then
         NO2_present       = .false.
         trop_NO2_present  = .false.
         strat_NO2_present = .false.

         ! demand that the slant column of (total) NO2 is fitted and that NO2 is present
         do iTrace = 1, nTrace
           select case ( trim(traceGasRetrS(iTrace)%nameTraceGas) )
             case ('NO2')
               NO2_present = .true.
               if ( .not. traceGasRetrS(iTrace)%fitColumnTrace ) then
                 call logDebug('ERROR: NO2 (total column) occurs as trace gas but is not fitted')
                 call logDebug('slant column of NO2 (total column) must be fitted when DOMINO is selected')
                 call mystop(errS, 'stopped because slant column of NO2 is not fitted')
                 if (errorCheck(errS)) return
               end if ! .not. traceGasRetrS(iTrace)%fitColumnTrace
           end select
         end do ! iTrace

         ! demand that tropospheric NO2 occurs as trace gas and its slant column is not fitted
         ! as we need the airmass factor for tropospheric NO2
         ! the column should be very small for the simulation (only total column should contribute),
         ! but not neccesarily small for the retrieval (so that we can test whether the column affects the AMF)
         do iTrace = 1, nTrace
           select case ( trim(traceGasRetrS(iTrace)%nameTraceGas) )
             case ('trop_NO2')
               trop_NO2_present = .true.
               if ( traceGasRetrS(iTrace)%fitColumnTrace ) then
                 call logDebug('ERROR: trop_NO2 occurs as trace gas and is fitted')
                 call logDebug('trop_NO2 must not be fitted when DOMINO is selected')
                 call mystop(errS, 'stopped because trop_NO2 is fitted')
                 if (errorCheck(errS)) return
               end if ! .not. traceGasRetrS(iTrace)%fitColumnTrace
               if ( traceGasSimS(iTrace)%column > 1.0d10 ) then
                 call logDebug('ERROR: trop_NO2 column for simulation is > 1.0E10')
                 call logDebug('all NO2 for simulation must be provided by NO2')
                 call logDebug('trop_NO2 must not contribute when DOMINO is selected')
                 call mystop(errS, 'stopped because trop_NO2 > 1.0E10 for simulation')
                 if (errorCheck(errS)) return
               end if ! traceGasSimS(iTrace)%column > 1.0d10
           end select
         end do ! iTrace

         ! demand that stratospheric NO2 occurs as trace gas and its slant column is not fitted
         ! because we need to specify the stratispheric NO2 column
         ! the column should be very small for the simulation (only total column should contribute),
         ! but not small for the retrieval
         do iTrace = 1, nTrace
           select case ( trim(traceGasRetrS(iTrace)%nameTraceGas) )
             case ('strat_NO2')
               strat_NO2_present = .true.
               if ( traceGasRetrS(iTrace)%fitColumnTrace ) then
                 call logDebug('ERROR: strat_NO2 occurs as trace gas and is fitted')
                 call logDebug('strat_NO2 must not be fitted when DOMINO is selected')
                 call mystop(errS, 'stopped because strat_NO2 is fitted')
                 if (errorCheck(errS)) return
               end if ! .not. traceGasRetrS(iTrace)%fitColumnTrace
               if ( traceGasSimS(iTrace)%column > 1.0d10 ) then
                 call logDebug('ERROR: strat_NO2 column for simulation is > 1.0E10')
                 call logDebug('all NO2 for simulation must be provided by NO2')
                 call logDebug('strat_NO2 must not contribute when DOMINO is selected')
                 call mystop(errS, 'stopped because strat_NO2 > 1.0E10 for simulation')
                 if (errorCheck(errS)) return
               end if ! traceGasSimS(iTrace)%column > 1.0d10
           end select
         end do ! iTrace

         if ( controlSimS%copyNO2profileRetrToSim ) then
           ! additional checks are needed when the tropospheric and stratospheric NO2 profiles
           ! specified for retrieval are added and then copied to the NO2 profile for simulation

           if ( NO2_present .and. trop_NO2_present .and. strat_NO2_present) then
             ! demand that the length of the profile arrays is the same and that
             ! the pressure levels are the same for all three NO2 species
             do iTrace = 1, nTrace
               select case ( trim(traceGasSimS(iTrace)%nameTraceGas) )
                 case ('NO2')
                   lenghtSimNO2 = traceGasSimS(iTrace)%nalt
                 case('strat_NO2')
                   lenghtSimStratNO2 = traceGasSimS(iTrace)%nalt
                 case('trop_NO2')
                   lenghtSimTropNO2 = traceGasSimS(iTrace)%nalt
               end select
             end do ! iTrace

             if ( (lenghtSimNO2 /= lenghtSimStratNO2) .or. (lenghtSimNO2 /= lenghtSimTropNO2) ) then
               call logDebug('ERROR: lenght of NO2, trop NO2 and strat NO2 profiles are not equal for simulation')
               call logDebug('these three lenghts must be the same when DOMINO is selected')
               call mystop(errS, 'stopped because lenghts of NO2 vmr profiles are not the same')
               if (errorCheck(errS)) return
             end if ! check lengths are the same

             do iTrace = 1, nTrace
               select case ( trim(traceGasRetrS(iTrace)%nameTraceGas) )
                 case ('NO2')
                   lenghtRetrNO2 = traceGasRetrS(iTrace)%nalt
                 case('strat_NO2')
                   lenghtRetrStratNO2 = traceGasRetrS(iTrace)%nalt
                 case('trop_NO2')
                   lenghtRetrTropNO2 = traceGasRetrS(iTrace)%nalt
               end select
             end do ! iTrace

             if ( (lenghtRetrNO2 /= lenghtRetrStratNO2) .or. (lenghtRetrNO2 /= lenghtRetrTropNO2) ) then
               call logDebug('ERROR: lenght of NO2, trop NO2 and strat NO2 profiles are not equal for retrieval')
               call logDebug('these three lenghts must be the same when DOMINO is selected')
               call mystop(errS, 'stopped because lenghts of NO2 vmr profiles are not the same')
               if (errorCheck(errS)) return
             end if ! check lengths are the same

             if ( (lenghtRetrNO2 /= lenghtSimNO2) ) then
               call logDebug('ERROR: lenght of NO2 profiles are not equal for retrieval and simulation')
               call logDebug('these lenghts must be the same when DOMINO is selected')
               call mystop(errS, 'stopped because lenghts of NO2 vmr profiles are not the same')
               if (errorCheck(errS)) return
             end if ! check lengths are the same

             do ipressure = 0, lenghtSimNO2
               do iTrace = 1, nTrace
                 select case ( trim(traceGasSimS(iTrace)%nameTraceGas) )
                   case ('NO2')
                     pressureNO2      = traceGasSimS(iTrace)%pressure(ipressure)
                   case('strat_NO2')
                     pressureStratNO2 = traceGasSimS(iTrace)%pressure(ipressure)
                   case('trop_NO2')
                     pressureTropNO2  = traceGasSimS(iTrace)%pressure(ipressure)
                 end select
               end do ! iTrace
               if ( (pressureNO2 /= pressureStratNO2) .or. (pressureNO2 /= pressureTropNO2) ) then
                 call logDebug('ERROR: pressures of NO2, trop NO2 and strat NO2 profiles are not equal for simulation')
                 call logDebug('these three pressures must be the same when DOMINO is selected')
                 call logDebugI('pressures index = ', ipressure)
                 call mystop(errS, 'stopped because pressures of NO2 vmr profiles are not the same')
                 if (errorCheck(errS)) return
               end if ! check pressures are the same

             end do ! iPressure 

             do ipressure = 0, lenghtRetrNO2
               do iTrace = 1, nTrace
                 select case ( trim(traceGasRetrS(iTrace)%nameTraceGas) )
                   case ('NO2')
                     pressureNO2      = traceGasRetrS(iTrace)%pressure(ipressure)
                   case('strat_NO2')
                     pressureStratNO2 = traceGasRetrS(iTrace)%pressure(ipressure)
                   case('trop_NO2')
                     pressureTropNO2  = traceGasRetrS(iTrace)%pressure(ipressure)
                 end select
               end do ! iTrace
               if ( (pressureNO2 /= pressureStratNO2) .or. (pressureNO2 /= pressureTropNO2) ) then
                 call logDebug('ERROR: pressures of NO2, trop NO2 and strat NO2 profiles are not equal for retrieval')
                 call logDebug('these three pressures must be the same when DOMINO is selected')
                 call logDebugI('pressures index = ', ipressure)
                 call mystop(errS, 'stopped because pressures of NO2 vmr profiles are not the same')
                 if (errorCheck(errS)) return
               end if ! check pressures are the same

             end do ! iPressure 

             ! overwrite vmr for NO2 (sim and retr) by the sum of trop NO2 (retr) and strat NO2 (retr)
             ! this gives freedom to scale trop NO2 and strat NO2 independently
             ! do this also for the columns
             do iTrace = 1, nTrace
               select case ( trim(traceGasRetrS(iTrace)%nameTraceGas) )
                 case ('NO2')
                   traceGasSimS(iTrace)%vmr(:)      = 0.0d0
                   traceGasSimS(iTrace)%numDens(:)  = 0.0d0
                   traceGasRetrS(iTrace)%vmr(:)     = 0.0d0
                   traceGasRetrS(iTrace)%numDens(:) = 0.0d0
                   traceGasSimS(iTrace)%columnAP    = 0.0d0
                   traceGasSimS(iTrace)%column      = 0.0d0
                   traceGasRetrS(iTrace)%columnAP   = 0.0d0
                   traceGasRetrS(iTrace)%column     = 0.0d0
                   do jTrace = 1, nTrace
                     if ( trim(traceGasSimS(jTrace)%nameTraceGas) == 'strat_NO2' .or.  &
                          trim(traceGasSimS(jTrace)%nameTraceGas) == 'trop_NO2' ) then
                       traceGasSimS(iTrace)%vmr(:)    = traceGasSimS(iTrace)%vmr(:)     + traceGasRetrS(jTrace)%vmr(:)
                       traceGasSimS(iTrace)%numDens(:)= traceGasSimS(iTrace)%numdens(:) + traceGasRetrS(jTrace)%numDens(:)
                       traceGasSimS(iTrace)%columnAP  = traceGasSimS(iTrace)%columnAP   + traceGasRetrS(jTrace)%columnAP
                       traceGasSimS(iTrace)%column    = traceGasSimS(iTrace)%column     + traceGasRetrS(jTrace)%column
                     end if  ! strat or trop NO2
                   end do ! jTrace
                   do jTrace = 1, nTrace
                     if ( trim(traceGasRetrS(jTrace)%nameTraceGas) == 'strat_NO2' .or.  &
                          trim(traceGasRetrS(jTrace)%nameTraceGas) == 'trop_NO2' ) then
                       traceGasRetrS(iTrace)%vmr(:)    = traceGasRetrS(iTrace)%vmr(:)     + traceGasRetrS(jTrace)%vmr(:)
                       traceGasRetrS(iTrace)%numDens(:)= traceGasRetrS(iTrace)%numdens(:) + traceGasRetrS(jTrace)%numDens(:)
                       traceGasRetrS(iTrace)%columnAP  = traceGasRetrS(iTrace)%columnAP   + traceGasRetrS(jTrace)%columnAP
                       traceGasRetrS(iTrace)%column    = traceGasRetrS(iTrace)%column     + traceGasRetrS(jTrace)%column
                     end if  ! strat or trop NO2
                   end do ! jTrace
               end select
             end do ! iTrace
           else
             call logDebug('ERROR: one or more of NO2, trop_NO2, strat_NO2 is missing')
             call logDebug('these three must be present when DOMINO is selected')
             call mystop(errS, 'stopped because a gas is missing')
             if (errorCheck(errS)) return
           end if ! NO2_present .and. trop_NO2_present .and. strat_NO2_present

         end if ! copyNO2profileRetrToSim

         if ( numSpectrBands > 1) then
           call logDebug('ERROR: DOMINO uses one spectral band')
           call logDebug('in the configuration file more than one spectral band is specified')
           call logDebug('in section GENERAL, in subsection overall')
           call mystop(errS, 'stopped because more than one spectral band is specified')
           if (errorCheck(errS)) return
         end if ! numSpectrBands > 1

       end if ! controlRetrS%method == 4

       ! test that we account for linear polarization when usePolScrambler is .false.
       if (controlSimS%dimSV < 3) then
         do iband = 1, numSpectrBands
            if ( .not. earthRadianceSimS(iband)%usePolScrambler ) then
              call logDebug('ERROR: polarization scrambler is not used')
              call logDebug('while the dimension of the Stokes vector, dimSV, is less than 3')
              call logDebug('set dimSV to 3 or 4, or set usePolScrambler to 1')
              call mystop(errS, 'stopped because settings are inconsistent')
              if (errorCheck(errS)) return
            end if ! usePolScrambler
         end do ! iband
       end if ! controlSimS%dimSV < 3

       ! check boundaries for the subcolumns
       if ( ncolumn > 0 ) then
         do icolumn = 1, ncolumn
           ! set lower boundary for the first subcolumn to the surface altitude for all column cases
           ! and check that its value is smaller than the upper boundary for the first subcolumn
           columnSimS (icolumn)%altBound(0) = surfaceSimS(1)%altitude
           columnRetrS(icolumn)%altBound(0) = surfaceRetrS(1)%altitude
           if ( columnSimS(icolumn)%altBound(0) >= columnSimS(icolumn)%altBound(1) ) then
             call logDebug('ERROR in configuration file: top of first subcolumn <  surface altitude')
             call logDebug('check values for subcolumns and surface altitude')
             call logDebugI('column case = ', icolumn)
             call mystop(errS, 'stopped because of incorrect first subcolumn')
             if (errorCheck(errS)) return
           end if
         end do ! icolumn
       end if ! ncolumn > 0

       ! check that for ozone profile retrieval using DISMAS linear interpolation is used
       ! for the ozone profile
       do iTrace = 1, nTrace
         if ( traceGasRetrS(iTrace)%fitProfile .and.                        &
              ( trim( traceGasRetrS(iTrace)%nameTraceGas ) == 'O3' ) .and.  &
              controlRetrS%method == 1 ) then   ! DISMAS is used   
           if ( .not. traceGasRetrS(iTrace)%useLinInterp ) then
             call logDebug('ERROR in configurationFile: linear interpolation is not used ')
             call logDebug('for ozone profile retrieval combined with DISMAS ')
             call logDebug('set useLinInterpRetr to 1 in the O3 subsection ')
             call mystop(errS, 'stopped because linear interpolation should be used')
             if (errorCheck(errS)) return
           end if
         end if
       end do ! itrace

       ! check that DOAS is not used for profile retrieval
       do iTrace = 1, nTrace
         if ( ( controlRetrS%method >= 2 ) .and. traceGasRetrS(iTrace)%fitProfile ) then
           call logDebug('ERROR in configurationFile: DOAS and profile retrieval are selected ')
           call mystop(errS, 'stopped because profile retrieval is not possile with DOAS')
           if (errorCheck(errS)) return
         end if
       end do

       ! fitting temperature profile is not implemented for DOAS / DISMAS
       if ( ( controlRetrS%method > 0 ) .and. gasPTRetrS%fitTemperatureProfile ) then
         call logDebug('fitting temperature profile is not implemented for DOAS / DISMAS ')
         call logDebug('use retrievalMethod = 0, i.e. line-by-line OE for temperature fits')
         call mystop(errS, 'stopped because temperature profile can not be fitted when DOAS / DISMAS is used')
         if (errorCheck(errS)) return
       end if

       ! fitting temperature offset is not implemented for DOAS / DISMAS
       if ( ( controlRetrS%method > 0 ) .and. gasPTRetrS%fitTemperatureOffset ) then
         call logDebug('fitting temperature offset is not implemented for DOAS / DISMAS')
         call logDebug('use retrievalMethod = 0, i.e. line-by-line OE for temperature fits')
         call mystop(errS, 'stopped because temperature offset can not be fitted when DOAS / DISMAS is used')
         if (errorCheck(errS)) return
       end if

       ! wavelength for the air mass factor should lie in the fit window
       if ( controlRetrS%method == 3 .or. controlRetrS%method == 4 ) then
         do iband = 1, numSpectrBands
           if ( weakAbsRetrS(iband)%calculateAMF ) then 
             if ( weakAbsRetrS(iband)%wavelengthAMF < wavelInstrRadRetrS(iband)%startWavel - 0.01 ) then
               call logDebug("ERROR in configuration file")
               call logDebug("wavelengths for AMF MUST lie inside fit window")
               call logDebug("in section: "//"GENERAL")
               call logDebug("in subsection: "//"specifications_DOAS_DISMAS")
               call logDebug("keyword: "//"wavelengthAMFRetr")
               call logDebugI("for spectral band number: ", iband)
               call logDebugD("lower bound fit window          : ", wavelInstrRadRetrS(iband)%startWavel)
               call logDebugD("read value of wavelengthAMFRetr : ", weakAbsRetrS(iband)%wavelengthAMF)
               call mystop(errS, 'stopped because wavelength for AMF lies outside fit window')
               if (errorCheck(errS)) return
             end if
             if ( weakAbsRetrS(iband)%wavelengthAMF > wavelInstrRadRetrS(iband)%endWavel + 0.01 ) then
               call logDebug("ERROR in configuration file")
               call logDebug("wavelengths for AMF MUST lie inside fit window")
               call logDebug("in section: "//"GENERAL")
               call logDebug("in subsection: "//"specifications_DOAS_DISMAS")
               call logDebug("keyword: "//"wavelengthAMFRetr")
               call logDebugI("for spectral band number: ", iband)
               call logDebugD("upper bound fit window          : ", wavelInstrRadRetrS(iband)%endWavel)
               call logDebugD("read value of wavelengthAMFRetr : ", weakAbsRetrS(iband)%wavelengthAMF)
               call mystop(errS, 'stopped because wavelength for AMF lies outside fit window')
               if (errorCheck(errS)) return
             end if
           end if !  weakAbsRetrS(iband)%calculateAMF
         end do ! iband
       end if ! controlRetrS%method == 3

       ! check that a Lambertian cloud is used when the cloud albedo is fitted
       if ( LambCloudRetrS(1)%fitAlbedo .and. ( .not. cloudAerosolRTMgridRetrS%useLambertianCloud ) ) then
         call logDebug("ERROR in configuration file")
         call logDebug("fitting the cloud albedo can only be done if a Lambertian cloud is used")
         call logDebug("change the type of cloud in SECTION CLOUD")
         call mystop(errS, 'stopped because cloud albedo can not be fitted - no Lambertian cloud')
         if (errorCheck(errS)) return
       end if

       ! check that Lambertian aerosol is used when the aerosol albedo is fitted
       if ( LambAerRetrS(1)%fitAlbedo .and. ( .not. cloudAerosolRTMgridRetrS%useLambertianAerosol ) ) then
         call logDebug("ERROR in configuration file")
         call logDebug("fitting the aerosol albedo can only be done if a Lambertian aerosol is used")
         call logDebug("change the type of aerosol in SECTION aerosol")
         call mystop(errS, 'stopped because aerosol albedo can not be fitted - no Lambertian aerosol')
         if (errorCheck(errS)) return
       end if

       ! check that a scattering cloud is used when the optical thickness for clouds is fitted
       if ( cloudAerosolRTMgridRetrS%useLambertianCloud .and. cloudAerosolRTMgridRetrS%fitCldTau ) then
         call logDebug("ERROR in configuration file")
         call logDebug("fitting the cloud optical thickness")
         call logDebug("can only be done if a scattering cloud is used")
         call logDebug("change the type of cloud in SECTION CLOUD")
         call mystop(errS, 'stopped because the optical thickness can not be fitted - no scattering cloud')
         if (errorCheck(errS)) return
       end if

       ! check that aerosol parameters are not fitted when there is no aerosol present, i.e. 'none' is used
       if ( .not. cloudAerosolRTMgridRetrS%aerosolPresent ) then
         if ( cloudAerosolRTMgridRetrS%fitAlbedoLambAerAllBands .or. &
              cloudAerosolRTMgridRetrS%fitAerTau                .or. &
              cloudAerosolRTMgridRetrS%fitAerSSA                .or. &
              cloudAerosolRTMgridRetrS%fitAerAC ) then
           call logDebug("ERROR in configuration file")
           call logDebug("when there are no aerosols present")
           call logDebug("aerosol parameters can not be fitted")
           call logDebug("set fitting of aerosol parameters to zero in configuration file")
           call mystop(errS, 'stopped because one or more aerosol parameters are fitted')
           if (errorCheck(errS)) return
         end if
       end if ! aerosolPresent

       ! check that cloud parameters are not fitted when there is no cloud present, i.e. 'none' is used
       if ( .not. cloudAerosolRTMgridRetrS%cloudPresent ) then
         if ( cloudAerosolRTMgridRetrS%fitAlbedoLambCldAllBands .or. &
              cloudAerosolRTMgridRetrS%fitCldTau                .or. &
              cloudAerosolRTMgridRetrS%fitLnCldTau              .or. &
              cloudAerosolRTMgridRetrS%fitCldAC )   then
           call logDebug("ERROR in configuration file")
           call logDebug("when there is no cloud present")
           call logDebug("cloud parameters can not be fitted")
           call logDebug("set fitting of cloud parameters to zero in configuration file")
           call mystop(errS, 'stopped because one or more cloud parameters are fitted')
           if (errorCheck(errS)) return
         end if
       end if ! aerosolPresent

       ! check that scattering aerosol is used when the optical thickness for aerosol is fitted
       if ( cloudAerosolRTMgridRetrS%useLambertianAerosol .and. cloudAerosolRTMgridRetrS%fitAerTau ) then
         call logDebug("ERROR in configuration file")
         call logDebug("fitting the aerosol optical thickness")
         call logDebug("can only be done if scattering aerosol is used")
         call logDebug("change the type of aerosol in SECTION AEROSOL")
         call mystop(errS, 'stopped because the optical thickness can not be fitted - no scattering aerosol')
         if (errorCheck(errS)) return
       end if

       ! check that a scattering cloud is used when the Angstrom coefficient for clouds is fitted
       if ( cloudAerosolRTMgridRetrS%useLambertianCloud .and. cloudAerosolRTMgridRetrS%fitCldAC ) then
         call logDebug("ERROR in configuration file")
         call logDebug("The cloud Angstrom coefficient can only be")
         call logDebug("fitted if a scattering cloud is used")
         call logDebug("change the cloud type in SECTION CLOUD")
         call mystop(errS, 'stopped because the Angstrom coefficient can not be fitted - no scattering cloud')
         if (errorCheck(errS)) return
       end if

       ! check that HG scattering aerosol is used when the Angstrom coeffiient for aerosol is fitted
       if ( cloudAerosolRTMgridRetrS%useLambertianAerosol .and. cloudAerosolRTMgridRetrS%fitAerAC ) then
         call logDebug("ERROR in configuration file")
         call logDebug("The aerosol Angstrom coefficient can only be")
         call logDebug("fitted if a Henyey-Greenstein scattering aerosol is used")
         call logDebug("change the aerosol type in SECTION AEROSOL")
         call mystop(errS, 'stopped because the Angstrom coefficient can not be fitted - no scattering aerosol')
         if (errorCheck(errS)) return
       end if

       ! check that HG scattering aerosol is used when the single scattering albedo for aerosol is fitted
       if ( cloudAerosolRTMgridRetrS%useLambertianAerosol .and. cloudAerosolRTMgridRetrS%fitAerSSA ) then
         call logDebug("ERROR in configuration file")
         call logDebug("The aerosol single scattering albedo can only be")
         call logDebug("fitted if a Henyey-Greenstein scattering aerosol is used")
         call logDebug("change the aerosol type in SECTION AEROSOL")
         call mystop(errS, 'stopped because the aerosol SSA can not be fitted - no scattering aerosol')
         if (errorCheck(errS)) return
       end if

       ! check that HG scattering aerosol is used when the Angstrom coeffiient for aerosol is fitted
       if ( cloudAerosolRTMgridRetrS%useMieScatAer .and. cloudAerosolRTMgridRetrS%fitAerAC ) then
         call logDebug("ERROR in configuration file")
         call logDebug("The aerosol Angstrom coefficient can only be")
         call logDebug("fitted if Henyey-Greenstein scattering is used")
         call logDebug("not for Mie scattering")
         call logDebug("change the aerosol type in SECTION AEROSOL")
         call mystop(errS, 'stopped because the Angstrom coefficient can not be fitted')
         if (errorCheck(errS)) return
       end if

       ! check that HG scattering aerosol is used when the single scattering albedo for aerosol is fitted
       if ( cloudAerosolRTMgridRetrS%useMieScatAer .and. cloudAerosolRTMgridRetrS%fitAerSSA ) then
         call logDebug("ERROR in configuration file")
         call logDebug("The aerosol single scattering albedo can only be")
         call logDebug("fitted if Henyey-Greenstein scattering is used")
         call logDebug("not for Mie scattering")
         call logDebug("change the aerosol type in SECTION AEROSOL")
         call mystop(errS, 'stopped because the aerosol SSA can not be fitted')
         if (errorCheck(errS)) return
       end if

       ! check the wavelength range for Mie scattering by aerosol for simulation
       if ( cloudAerosolRTMgridSimS%useMieScatAer ) then
         do iband = 1, numSpectrBands
           do imodel = 1, maxNumMieModels
             ! test whether imodel occurs
             if ( associated( mieAerSimS(imodel)%wavel ) ) then
               nwavel = mieAerSimS(imodel)%nwavel
               if ( ( wavelInstrRadSimS(iband)%startWavel < mieAerSimS(imodel)%wavel(1)      ) .or. &
                    ( wavelInstrRadSimS(iband)%endWavel   > mieAerSimS(imodel)%wavel(nwavel) ) ) then
                 call logDebug('ERROR in Mie aerosol file used for simulation')
                 call logDebug('file does not cover the wavelength range required')
                 call logDebugI('error occurs for spectral band number    : ', iband)
                 call logDebugI('error occurs for aerosol Mie model number: ', imodel)
                 call logDebug('file name is: '//  trim(mieAerSimS(imodel)%fileNameExpCoef))
                 call mystop(errS, 'stopped because wavelength range in file incorrect')
                 if (errorCheck(errS)) return
               end if
             end if ! associated
           end do ! imodel
         end do ! iband
       end if ! cloudAerosolRTMgridSimS%useMieScatAer

       ! check the wavelength range for Mie scattering by aerosol for retrieval
       if ( cloudAerosolRTMgridRetrS%useMieScatAer ) then
         do iband = 1, numSpectrBands
           do imodel = 1, maxNumMieModels
             ! test whether imodel occurs
             if ( associated( mieAerRetrS(imodel)%wavel ) ) then
               nwavel = mieAerRetrS(imodel)%nwavel
               if ( ( wavelInstrRadRetrS(iband)%startWavel < mieAerRetrS(imodel)%wavel(1)      ) .or. &
                    ( wavelInstrRadRetrS(iband)%endWavel   > mieAerRetrS(imodel)%wavel(nwavel) ) ) then
                 call logDebug('ERROR in Mie aerosol file used for retrieval')
                 call logDebug('file does not cover the wavelength range required')
                 call logDebugI('error occurs for spectral band number    : ', iband)
                 call logDebugI('error occurs for aerosol Mie model number: ', imodel)
                 call logDebug('file name is: '//  trim(mieAerSimS(imodel)%fileNameExpCoef))
                 call mystop(errS, 'stopped because wavelength range in file incorrect')
                 if (errorCheck(errS)) return
               end if
             end if ! associated
           end do ! imodel
         end do ! iband
       end if ! cloudAerosolRTMgridSimS%useMieScatAer

       ! check the wavelength range for Mie scattering by cloud for simulation
       if ( cloudAerosolRTMgridSimS%useMieScatCld ) then
         do iband = 1, numSpectrBands
           do imodel = 1, maxNumMieModels
             ! test whether imodel occurs
             if ( associated( mieCldSimS(imodel)%wavel ) ) then
               nwavel = mieCldSimS(imodel)%nwavel
               if ( ( wavelInstrRadSimS(iband)%startWavel < mieCldSimS(imodel)%wavel(1)      ) .or. &
                    ( wavelInstrRadSimS(iband)%endWavel   > mieCldSimS(imodel)%wavel(nwavel) ) ) then
                 call logDebug('ERROR in Mie aerosol file used for simulation')
                 call logDebug('file does not cover the wavelength range required')
                 call logDebugI('error occurs for spectral band number  : ', iband)
                 call logDebugI('error occurs for cloud Mie model number: ', imodel)
                 call logDebug('file name is: '//  trim(mieCldSimS(imodel)%fileNameExpCoef))
                 call mystop(errS, 'stopped because wavelength range in file incorrect')
                 if (errorCheck(errS)) return
               end if
             end if ! associated
           end do ! imodel
         end do ! iband
       end if ! cloudAerosolRTMgridSimS%useMieScatCld

       ! check the wavelength range for Mie scattering by cloud for retrieval
       if ( cloudAerosolRTMgridRetrS%useMieScatCld ) then
         do iband = 1, numSpectrBands
           do imodel = 1, maxNumMieModels
             ! test whether imodel occurs
             if ( associated( mieCldRetrS(imodel)%wavel ) ) then
               nwavel = mieCldRetrS(imodel)%nwavel
               if ( ( wavelInstrRadRetrS(iband)%startWavel < mieCldRetrS(imodel)%wavel(1)      ) .or. &
                    ( wavelInstrRadRetrS(iband)%endWavel   > mieCldRetrS(imodel)%wavel(nwavel) ) ) then
                 call logDebug('ERROR in Mie aerosol file used for retrieval')
                 call logDebug('file does not cover the wavelength range required')
                 call logDebugI('error occurs for spectral band number  : ', iband)
                 call logDebugI('error occurs for cloud Mie model number: ', imodel)
                 call logDebug('file name is: '// trim(mieCldSimS(imodel)%fileNameExpCoef))
                 call mystop(errS, 'stopped because wavelength range in file incorrect')
                 if (errorCheck(errS)) return
               end if
             end if ! associated
           end do ! imodel
         end do ! iband
       end if ! cloudAerosolRTMgridSimS%useMieScatCld

       ! check that for each interval the number of gaussian division points is specified
       if ( cloudAerosolRTMgridSimS%nGaussAlt /= cloudAerosolRTMgridSimS%ninterval )  then
         call logDebug("ERROR in configuration file")
         call logDebug("for each interval the number of gauss points should be specified")
         call logDebug("in section: "//trim('RADIATIVE_TRANSFER'))
         call logDebug("in subsection: "//trim('numDivPointsAlt'))
         call logDebug("for key: "//trim('numDivPointsAltSim'))
         call logDebugI("ninterval        = ", cloudAerosolRTMgridSimS%ninterval)
         call logDebugI("nGaussAlt        = ", cloudAerosolRTMgridSimS%nGaussAlt)
         call mystop(errS, 'stopped because gaussian division points are not specified for each interval')
         if (errorCheck(errS)) return
       end if

       if ( cloudAerosolRTMgridRetrS%nGaussAlt /= cloudAerosolRTMgridRetrS%ninterval )  then
         call logDebug("ERROR in configuration file")
         call logDebug("for each interval the number of gauss points should be specified")
         call logDebug("in section: "//trim('RADIATIVE_TRANSFER'))
         call logDebug("in subsection: "//trim('numDivPointsAlt'))
         call logDebug("for key: "//trim('numDivPointsAltRetr'))
         call logDebugI("ninterval        = ", cloudAerosolRTMgridRetrS%ninterval)
         call logDebugI("nGaussAlt        = ", cloudAerosolRTMgridRetrS%nGaussAlt)
         call mystop(errS, 'stopped because gaussian division points are not specified for each interval')
         if (errorCheck(errS)) return
       end if

       ! Check that the number of Gaussian division points for integration over the altitude is
       ! at least 1.5 times the optical thickness of cloud plus aerosol for the relevant interval
       if ( .not. controlSimS%useReflectanceFromFile ) then
         do iinterval = 1, cloudAerosolRTMgridSimS%ninterval
           tau = 0.0d0
           if ( cloudAerosolRTMgridSimS%useMieScatCld ) tau =  tau + cloudAerosolRTMgridSimS%intervalCldTau(iinterval)
           if ( cloudAerosolRTMgridSimS%useHGScatCld  ) tau =  tau + cloudAerosolRTMgridSimS%intervalCldTau(iinterval)
           if ( cloudAerosolRTMgridSimS%useMieScatAer ) tau =  tau + cloudAerosolRTMgridSimS%intervalAerTau(iinterval)
           if ( cloudAerosolRTMgridSimS%useHGScatAer  ) tau =  tau + cloudAerosolRTMgridSimS%intervalAerTau(iinterval)
           if ( floor(tau * 1.5d0 ) > cloudAerosolRTMgridSimS%intervalnGauss(iinterval) ) then
             call logDebug("WARNING: possibly not enough Gaussian division points specified")
             call logDebug("in section: "//trim('RADIATIVE_TRANSFER'))
             call logDebug("in subsection: "//trim('numDivPointsAlt'))
             call logDebug("for key: "//trim('numDivPointsAltSim'))
             call logDebugI("for atmospheric interval:", iinterval)
             write(errS%temp,'(A,F10.3)') "cloud + aerosol optical thickness = ", tau
             call errorAddLine(errS, errS%temp)
             write(errS%temp,'(A,I4)')    "number of division points is = ", cloudAerosolRTMgridSimS%intervalnGauss(iinterval)
             call errorAddLine(errS, errS%temp)
             call logDebug("for accurate integration the number of division points")
             call logDebug("should be larger than 1.5 * optical thickness (aer + cld)")
             call logDebug("warning originated from subroutine verifyConfigFile")
             call logDebug("in module verifyConfigFileModule")
           end if
         end do
       end if

       if ( .not. controlSimS%simulationOnly ) then
         do iinterval = 1, cloudAerosolRTMgridRetrS%ninterval
           tau = 0.0d0
           if ( cloudAerosolRTMgridRetrS%useMieScatCld ) tau =  tau + cloudAerosolRTMgridRetrS%intervalCldTau(iinterval)
           if ( cloudAerosolRTMgridRetrS%useHGScatCld  ) tau =  tau + cloudAerosolRTMgridRetrS%intervalCldTau(iinterval)
           if ( cloudAerosolRTMgridRetrS%useMieScatAer ) tau =  tau + cloudAerosolRTMgridRetrS%intervalAerTau(iinterval)
           if ( cloudAerosolRTMgridRetrS%useHGScatAer  ) tau =  tau + cloudAerosolRTMgridRetrS%intervalAerTau(iinterval)
           if ( floor(tau * 1.5d0 ) > cloudAerosolRTMgridRetrS%intervalnGauss(iinterval) ) then
             call logDebug("WARNING: possibly not enough Gaussian division points specified")
             call logDebug("in section: "//trim('RADIATIVE_TRANSFER'))
             call logDebug("in subsection: "//trim('numDivPointsAlt'))
             call logDebug("for key: "//trim('numDivPointsAltRetr'))
             call logDebugI("for atmospheric interval:", iinterval)
             write(errS%temp,'(A,F10.3)') "cloud + aerosol optical thickness = ", tau
             call errorAddLine(errS, errS%temp)
             write(errS%temp,'(A,I4)') "number of division points is      = ", cloudAerosolRTMgridRetrS%intervalnGauss(iinterval)
             call errorAddLine(errS, errS%temp)
             call logDebug("for accurate integration the number of division points")
             call logDebug("should be larger than 1.5 * optical thickness (aer + cld)")
             call logDebug("warning originated from subroutine verifyConfigFile")
             call logDebug("in module verifyConfigFileModule")
           end if
         end do
       end if

       ! better use adding instead of labos when we deal optically thick cloud/aerosol
       if ( .not. controlSimS%useReflectanceFromFile ) then
         tau = 0.0d0
         do iinterval = 1, cloudAerosolRTMgridSimS%ninterval
           if ( cloudAerosolRTMgridSimS%useMieScatCld ) tau =  tau + cloudAerosolRTMgridSimS%intervalCldTau(iinterval)
           if ( cloudAerosolRTMgridSimS%useHGScatCld  ) tau =  tau + cloudAerosolRTMgridSimS%intervalCldTau(iinterval)
           if ( cloudAerosolRTMgridSimS%useMieScatAer ) tau =  tau + cloudAerosolRTMgridSimS%intervalAerTau(iinterval)
           if ( cloudAerosolRTMgridSimS%useHGScatAer  ) tau =  tau + cloudAerosolRTMgridSimS%intervalAerTau(iinterval)
         end do
         if ( .not. controlSimS%useAdding ) then
           if ( tau > 4 ) then
             call logDebug("WARNING: Labos is not suited for optically thick scattering layers")
             call logDebug("better use adding ")
             call logDebug("in section: "//trim('RADIATIVE_TRANSFER'))
             call logDebug("in subsection: "//trim('RTM_Sim_Retr'))
             call logDebug("use key: "//trim('useAddingSim'))
             call logDebug("or set numOrdersMaxSim to a high value")
             write(errS%temp,'(A,F10.3)') "cloud + aerosol optical thickness = ", tau
             call errorAddLine(errS, errS%temp)
           end if
         end if
       end if

       ! better use adding instead of labos when we deal optically thick cloud/aerosol
       if ( .not. controlSimS%simulationOnly ) then
         tau = 0.0d0
         do iinterval = 1, cloudAerosolRTMgridRetrS%ninterval
           if ( cloudAerosolRTMgridRetrS%useMieScatCld ) tau =  tau + cloudAerosolRTMgridRetrS%intervalCldTau(iinterval)
           if ( cloudAerosolRTMgridRetrS%useHGScatCld  ) tau =  tau + cloudAerosolRTMgridRetrS%intervalCldTau(iinterval)
           if ( cloudAerosolRTMgridRetrS%useMieScatAer ) tau =  tau + cloudAerosolRTMgridRetrS%intervalAerTau(iinterval)
           if ( cloudAerosolRTMgridRetrS%useHGScatAer  ) tau =  tau + cloudAerosolRTMgridRetrS%intervalAerTau(iinterval)
         end do
         if ( .not. controlRetrS%useAdding ) then
           if ( tau > 4 ) then
             call logDebug("WARNING: Labos is not suited for optically thick scattering layers")
             call logDebug("better use adding ")
             call logDebug("in section: "//trim('RADIATIVE_TRANSFER'))
             call logDebug("in subsection: "//trim('RTM_Sim_Retr'))
             call logDebug("use key: "//trim('useAddingRetr'))
             call logDebug("or set numOrdersMaxRetr to a high value")
             write(errS%temp,'(A,F10.3)') "cloud + aerosol optical thickness = ", tau
             call errorAddLine(errS, errS%temp)
           end if
         end if
       end if

       ! check that the fit interval is not the lowest interval nor the uppermost interval
       ! because a cloud / aerosl layer has to be able to move upward and downward
       if ( cloudAerosolRTMgridRetrS%numIntervalFit <= 1 ) then
         call logDebug("ERROR in configuration file")
         call logDebug("the fit interval should not be the lowest interval")
         call logDebug("numIntervalFit must be at least 2 so that cloud / aerosol can move")
         call logDebug("in section: "//trim('GENERAL'))
         call logDebug("in subsection: "//trim('specifyFitting'))
         call logDebug("for key: "//trim('numIntervalFit'))
         call logDebugI("numIntervalFit        = ", cloudAerosolRTMgridRetrS%numIntervalFit)
         call mystop(errS, 'stopped because numIntervalFit is too small')
         if (errorCheck(errS)) return
       end if

       if ( cloudAerosolRTMgridRetrS%numIntervalFit > cloudAerosolRTMgridRetrS%ninterval - 1 ) then
         call logDebug("ERROR in configuration file")
         call logDebug("the fit interval should not be the uppermost interval or above")
         call logDebug("in section: "//trim('GENERAL'))
         call logDebug("in subsection: "//trim('specifyFitting'))
         call logDebug("for key: "//trim('numIntervalFit'))
         call logDebugI("ninterval (uppermost) = ", cloudAerosolRTMgridRetrS%ninterval)
         call logDebugI("numIntervalFit        = ", cloudAerosolRTMgridRetrS%numIntervalFit)
         call mystop(errS, 'stopped because numIntervalFit is too large')
         if (errorCheck(errS)) return
       end if

       ! test whether the vmr of the trace gases cover the atmosphere
       do iTrace = 1, nTrace
#ifndef TROPNLL2DP
         if ( traceGasSimS(iTrace)%pressure(traceGasSimS(iTrace)%nalt) > pressureAtTopSim ) then
           call logDebug('ERROR in configuration file: vmr of trace gas does not cover the atmosphere')
           call logDebug('vmr of trace gas for simulation does not cover the top of the atmosphere')
           call logDebug('extend the range for trace gas: '//trim(traceGasSimS(iTrace)%nameTraceGas))
           call mystop(errS, 'stopped because trace gas specification does not cover the atmosphere')         
           if (errorCheck(errS)) return
         end if
#endif
         if ( traceGasRetrS(iTrace)%pressure(traceGasRetrS(iTrace)%nalt) > pressureAtTopRetr ) then
           call logDebug('ERROR in configuration file: vmr of trace gas does not cover the atmosphere')
           call logDebug('vmr of trace gas for retrieval does not cover the top of the atmosphere')
           call logDebug('extend the range for trace gas: '//trim(traceGasRetrS(iTrace)%nameTraceGas))
           call mystop(errS, 'stopped because trace gas specification does not cover the atmosphere')         
           if (errorCheck(errS)) return
         end if
#ifndef TROPNLL2DP
         if ( traceGasSimS(iTrace)%pressure(0) + 1.0d0 < pressureAtBotSim ) then
           call logDebug('ERROR in configuration file: vmr of trace gas does not cover the atmosphere')
           call logDebug('vmr of trace gas for simulation does not cover the surface')
           call logDebug('extend the range for trace gas: '//trim(traceGasSimS(iTrace)%nameTraceGas))
           call logDebug('the vmr should be specified for a pressure within 5 hPa of the surface')
           write(errS%temp,'(A,F12.5)') 'pressure at surface =            ', pressureAtBotSim
           call errorAddLine(errS, errS%temp)
           write(errS%temp,'(A,F12.5)') 'highest pressure for trace gas = ', traceGasSimS(iTrace)%pressure(0)
           call errorAddLine(errS, errS%temp)
           call mystop(errS, 'stopped because trace gas specification does not cover the surface')         
           if (errorCheck(errS)) return
         end if
#endif
         if ( traceGasRetrS(iTrace)%pressure(0) + 1.0d0 < pressureAtBotRetr ) then
           call logDebug('ERROR in configuration file: vmr of trace gas does not cover the atmosphere')
           call logDebug('vmr of trace gas for retrieval does not cover the surface')
           call logDebug('extend the range for trace gas: '//trim(traceGasRetrS(iTrace)%nameTraceGas))
           call logDebug('the vmr should be specified for a pressure within 5 hPa of the surface')
           write(errS%temp,'(A,F12.5)') 'pressure at surface =            ', pressureAtBotRetr
           call errorAddLine(errS, errS%temp)
           write(errS%temp,'(A,F12.5)') 'highest pressure for trace gas = ', traceGasRetrS(iTrace)%pressure(0)
           call errorAddLine(errS, errS%temp)
           call mystop(errS, 'stopped because trace gas specification does not cover the surface')         
           if (errorCheck(errS)) return
         end if
       end do ! iTrace

      ! check that the pressure-temperature grid covers the vmr grid to avoid (spline) extrapolation
       do iTrace = 1, nTrace
#ifndef TROPNLL2DP
         if ( traceGasSimS(iTrace)%pressure(traceGasSimS(iTrace)%nalt) < gasPTSimS%pressureNodes(gasPTSimS%npressureNodes) ) then
           call logDebug('ERROR in configuration file: ')
           call logDebug('PT grid does not cover the vmr grid for simulation')
           call logDebug('at the top of the atmosphere for trace gas: '//trim(traceGasSimS(iTrace)%nameTraceGas))
           call logDebug('extend the pressure range where the temperature is specified')
           call mystop(errS, 'stopped because temperature specification does not cover the vmr range')         
           if (errorCheck(errS)) return
         end if
#endif
         if (traceGasRetrS(iTrace)%pressure(traceGasRetrS(iTrace)%nalt)<gasPTRetrS%pressureNodes(gasPTRetrS%npressureNodes)) then
           call logDebug('ERROR in configuration file: ')
           call logDebug('PT grid does not cover the vmr grid for retrieval')
           call logDebug('at the top of the atmosphere for trace gas: '//trim(traceGasSimS(iTrace)%nameTraceGas))
           call logDebug('extend the pressure range where the temperature is specified')
           call mystop(errS, 'stopped because temperature specification does not cover the vmr range')         
           if (errorCheck(errS)) return
         end if
#ifndef TROPNLL2DP
         if ( traceGasSimS(iTrace)%pressure(0) > gasPTSimS%pressureNodes(0) ) then
           call logDebug('ERROR in configuration file: ')
           call logDebug('PT grid does not cover the vmr grid for simulation')
           call logDebug('at high pressures for trace gas: '//trim(traceGasSimS(iTrace)%nameTraceGas))
           call logDebug('extend the pressure range where the temperature is specified')
           call mystop(errS, 'stopped because temperature specification does not cover the vmr range')         
           if (errorCheck(errS)) return
         end if
#endif
         if ( traceGasRetrS(iTrace)%pressure(0) > gasPTRetrS%pressureNodes(0) ) then
           call logDebug('ERROR in configuration file: ')
           call logDebug('PT grid does not cover the vmr grid for retrieval')
           call logDebug('at high pressures for trace gas: '//trim(traceGasSimS(iTrace)%nameTraceGas))
           call logDebug('extend the pressure range where the temperature is specified')
           call mystop(errS, 'stopped because temperature specification does not cover the vmr range')         
           if (errorCheck(errS)) return
         end if
       end do ! iTrace

      ! if a Lambertian surface is used for cloud or aerosol in the retrieval the altitude
      !  of the lower level of the fit interval can not be determined
      
      if ( cloudAerosolRTMgridRetrS%fitIntervalBot .and. cloudAerosolRTMgridRetrS%useLambertianCloud) then
        call logDebug('ERROR in the configuration file')
        call logDebug('A lambertian cloud is used in the retrieval')
        call logDebug('AND the altitude of the lower boundary has to be fitted')
        call logDebug('set fitIntervalDP or fitIntervalTop to .true. for a Lambertian cloud')
        call mystop(errS, 'stopped: do not use a Lambertian surface if the lower boundary is to be fitted')
        if (errorCheck(errS)) return
      end if

      ! stop if there is no absorbing trace gas and DISMAS or DOAS is specified as retrieval method
      if ( controlRetrS%method > 0 ) then  ! DISMAS or DOAS is specified
        if ( nTrace == 0 ) then
          call logDebug('DISMAS or DOAS can not be used if there is no absorbing trace gas')
          call mystop(errS, 'no absorbing trace gas specified: incompatible with DISMAS/DOAS: stopped')
          if (errorCheck(errS)) return
        end if
      end if

      ! check that wavelengths for calibration errors lie inside the spectral fit window
      do iband = 1, numSpectrBands 
        if ( minval( calibErrorReflS(iband)%wavel(:) ) < wavelInstrRadSimS(iband)%startWavel - 0.01 ) then
          call logDebug("ERROR in configuration file")
          call logDebug("wavelengths for calibration errors MUST lie inside fit window")
          call logDebug("in section: "//"INSTRUMENT")
          call logDebug("in subsection: "//"calibrationErrorRefl")
          call logDebug("keyword: "//"wavelength")
          call logDebugI("for spectral band number: ", iband)
          call logDebugD("lower bound fit window      : ", wavelInstrRadSimS(iband)%startWavel)
          call logDebugD("minumum of read wavelengths : ", minval( calibErrorReflS(iband)%wavel(:) ))
          call mystop(errS, 'stopped because wavelengths for calibration errors lie outside fit window')
          if (errorCheck(errS)) return
        end if
        if ( maxval( calibErrorReflS(iband)%wavel(:) ) > wavelInstrRadSimS(iband)%endWavel + 0.01 ) then
          call logDebug("ERROR in configuration file")
          call logDebug("wavelengths for for calibration errors MUST lie inside fit window")
          call logDebug("in section: "//"INSTRUMENT")
          call logDebug("in subsection: "//"calibrationErrorRefl")
          call logDebug("keyword: "//"wavelength")
          call logDebugI("for spectral band number: ", iband)
          call logDebugD("upper bound fit window      : ", wavelInstrRadSimS(iband)%endWavel)
          call logDebugD("maximum of read wavelengths : ", maxval( calibErrorReflS(iband)%wavel(:) ))
          call mystop(errS, 'stopped because wavelengths for calibration errors lie outside fit window')
          if (errorCheck(errS)) return
        end if
      end do ! iband

      ! check that wavelengths for calibration errors do not lie in excluded parts of the fit window
      do iband = 1, numSpectrBands
        do ipair = 1, wavelInstrRadSimS(iband)%nExclude
          ! wavelengths for excluded interval
          startWavel = wavelInstrRadSimS(iband)%excludeStart(ipair)
          endWavel   = wavelInstrRadSimS(iband)%excludeEnd(ipair)
          do iwave = 1, calibErrorReflS(iband)%nwavel
            if ( ( calibErrorReflS(iband)%wavel(iwave) > startWavel - 0.001 ) .and. &
                 ( calibErrorReflS(iband)%wavel(iwave) < endWavel   + 0.001 ) ) then
              call logDebug("ERROR in configuration file")
              call logDebug("wavelength for calibration errors MUST be outside excluded interval")
              call logDebug("in section: "//"INSTRUMENT")
              call logDebug("in subsection: "//"calibrationErrorRefl")
              call logDebug("keyword: "//"wavelength")
              call logDebugI("for spectral band number: ", iband)
              call logDebugI("for excluded interval number: ", ipair)
              write(errS%temp,'(A,2F10.3)') "excluded interval: ", startWavel, endWavel
              call errorAddLine(errS, errS%temp)
              call logDebugD("wavelength calibration error: ", calibErrorReflS(iband)%wavel(iwave))
              call mystop(errS, 'stopped because wavelength for calibration error lies in excluded interval')
              if (errorCheck(errS)) return
            end if
          end do ! iwave
        end do ! ipair
      end do ! iband

      ! check that we have proper pressure differences for the intervals for simulation
      if ( .not. controlSimS%useReflectanceFromFile ) then
        do ipressure = 1, cloudAerosolRTMgridSimS%ninterval
          if ( cloudAerosolRTMgridSimS%intervalBoundsAP_P(ipressure) / maxPressureRatio >   &
               cloudAerosolRTMgridSimS%intervalBoundsAP_P(ipressure - 1) ) then
            call logDebug("ERROR in input: pressures for the top of the intervals MUST decrease")
            call logDebug("and p_top / p_bot MUST be SMALLER than maxPressureRatio")
            call logDebug("for the first interval p_bot is the surface pressure")
            call logDebug("in section "//trim("AEROSOL_CLOUD"))
            call logDebug("in subsection "//trim("interval_top_pressures"))
            call logDebug("for key "//trim("topPressureSim"))
            call logDebugI("for pressure index top = ", ipressure)
            write(errS%temp,'(A,1X,F12.5)') 'maxPressureRatio: ', maxPressureRatio
            call errorAddLine(errS, errS%temp)
            write(errS%temp,'(A,1X,20F12.5)') 'topPressureSim: ', cloudAerosolRTMgridSimS%intervalBoundsAP_P
            call errorAddLine(errS, errS%temp)
            call mystop(errS, 'stopped because pressures for the top of the intervals do not conform')
            if (errorCheck(errS)) return
          end if
        end do
      end if

      ! check that we have proper pressure differences for the intervals for retrieval
      do ipressure = 1, cloudAerosolRTMgridRetrS%ninterval
        if ( cloudAerosolRTMgridRetrS%intervalBoundsAP_P(ipressure) / maxPressureRatio >   &
             cloudAerosolRTMgridRetrS%intervalBoundsAP_P(ipressure - 1) ) then
          call logDebug("ERROR in input: pressures for the top of the intervals MUST decrease")
          call logDebug("and p_top / p_bot MUST be SMALLER than maxPressureRatio")
          call logDebug("for the first interval p_bot is the surface pressure")
          call logDebug("in section "//trim("AEROSOL_CLOUD"))
          call logDebug("in subsection "//trim("interval_top_pressures"))
          call logDebug("for key "//trim("topPressureRetr"))
          call logDebugI("for pressure index top = ", ipressure)
          write(errS%temp,'(A,1X,F12.5)') 'maxPressureRatio: ', maxPressureRatio
          call errorAddLine(errS, errS%temp)
          write(errS%temp,'(A,1X,20F12.5)') 'topPressureRetr: ', cloudAerosolRTMgridRetrS%intervalBoundsAP_P
          call errorAddLine(errS, errS%temp)
          call mystop(errS, 'stopped because pressures for the top do not conform')
          if (errorCheck(errS)) return
        end if
      end do

      ! DISMAS and DOAS can not be used when FWHM < minFWHM because DISMAS
      ! needs calculations for wavelengths where the differential absorption
      ! optical thickness vanishes.
      if ( (.not. controlSimS%useReflectanceFromFile) .and. (controlSimS%method > 0 ) ) then
        if ( controlSimS%ignoreSlit ) then
          call logDebug("ERROR in configuration file")
          call logDebug("DISMAS can not be used when ignoreSlitSim = 1")
          call logDebug("use simulationMethod = 0")
          call logDebug("in section: "//"GENERAL")
          call logDebug("in subsection: "//"method")
          call mystop(errS, 'stopped because DISMAS is selected when ignoreSlitSim = 1')
          if (errorCheck(errS)) return
        end if ! test minFWHM
      end if

      ! DISMAS and DOAS can not be used when ignoreSlit is true because they
      ! needs calculations for wavelengths where the differential absorption
      ! optical thickness vanishes.
      if ( controlSimS%method > 0 ) then
          if ( controlSimS%ignoreSlit ) then
            call logDebug("ERROR in configuration file")
            call logDebug("DOAS and DISMAS can not be used when")
            call logDebug("ignoreSlitSim = 1")
            call logDebug("use retrievalMethod = 0")
            call logDebug("in section: "//"GENERAL")
            call logDebug("in subsection: "//"method")
            call mystop(errS, 'stopped because DOAS or DISMAS is selected when ignoreSlitSim = 1')
            if (errorCheck(errS)) return
          end if ! test minFWHM
      end if

      ! monochromatic mode can not be used if radiance - irradiance data are read from file
      if ( controlSimS%useReflectanceFromFile ) then
        if ( controlRetrS%ignoreSlit .and. .not. controlRetrS%useEffXsec_OE ) then
          call logDebug("WARNING in configuration file ignore slit is used")
          call logDebug("and no effective cross sections are used in retrieval")
          call logDebug("while data is read from file")
          call logDebug("in section: "//"GENERAL")
          call logDebug("in subsection: "//"overall")
          call logDebug("this is probably inaccurate")
        end if ! test minFWHM
      end if

      ! monochromatic mode can not be used if RRS is used - simulation
      do iband = 1, numSpectrBands
        if ( RRS_RingSimS(iband)%useRRS .or. RRS_RingSimS(iband)%approximateRRS) then
          if ( controlSimS%ignoreSlit ) then
            call logDebug("WARNING in configuration file ignore slit is used")
            call logDebug("when rotational Raman scattering is used for simulation")
            call logDebug("this may be inaccurate")
          end if
        end if ! useRRS
      end do ! iband

      ! monochromatic mode can not be used if RRS is used - retrieval
      do iband = 1, numSpectrBands
        if ( RRS_RingRetrS(iband)%useRRS .or. RRS_RingRetrS(iband)%approximateRRS) then
          if ( controlRetrS%ignoreSlit ) then
            call logDebug("WARNING in configuration file ignore slit is used")
            call logDebug("when rotational Raman scattering is used for retrieval")
            call logDebug("this may be inaccurate")
          end if ! test minFWHM
        end if ! useRRS
      end do ! iband
      
      ! The cloud fraction can not be fitted if there is no cloud or
      ! if the the a priori cloud optical thickness is zero.
      if ( cloudAerosolRTMgridRetrS%fitCldAerFractionAllBands ) then
        if ( cloudAerosolRTMgridRetrS%fractionCldAerType ) then
          ! cloud fraction is specified
          if ( cloudAerosolRTMgridRetrS%cloudPresent ) then
            if ( .not. cloudAerosolRTMgridRetrS%useLambertianCloud ) then
              ! scattering cloud is used
              if ( sum( cloudAerosolRTMgridRetrS%intervalCldTau(:) ) < 0.01d0 ) then
                call logDebug("ERROR in configuration file")
                call logDebug("fitting of cloud fraction is specified")
                call logDebug("and a scattering cloud is specified")
                call logDebug("while the a priori cloud optical thickness")
                call logDebug("is less than 0.01 in total")
                call logDebug("increase a priori cloud optical thickness")
                call logDebug("or set fitting flag to 0")
                call mystop(errS, 'stopped because the cloud optical thickness is too small')
                if (errorCheck(errS)) return
              end if
            end if ! .not. cloudAerosolRTMgridRetrS%useLambertianCloud
          else
            call logDebug("ERROR in configuration file")
            call logDebug("fitting of cloud fraction is specified")
            call logDebug("while there is no cloud present")
            call logDebug("specify cloud properties or set fitting flag to 0")
            call mystop(errS, 'stopped because there is not cloud present')
            if (errorCheck(errS)) return
          end if ! cloudPresent
        else
          ! aerosol fraction is specified

          ! to be completed
        end if ! fractionCldAerType
      else
      ! cloud fraction is not fitted; minCldFraction is defined in dataStructures.f90
        if ( cloudAerosolRTMgridRetrS%cldAerfractionAllBandsAP <= minCldFraction ) &
             cloudAerosolRTMgridRetrS%cloudPresent = .false.
      end if ! fitCldAerFraction

      if ( cloudAerosolRTMgridSimS%cldAerfractionAllBandsAP <= minCldFraction ) &
             cloudAerosolRTMgridSimS%cloudPresent = .false.

      if ( controlRetrS%singleScatteringOnly ) then
        call logDebug("WARNING - Single scattering or no scattering is")
        call logDebug("          selected for retrieval")
        call logDebug("          At least some derivatives are incorrect")
        call logDebug("          Expect no convergence or poor convergence")
      end if

      do iTrace = 1, nTrace
        if ( traceGasSimS(iTrace)%useO3Climatology .or. traceGasRetrS(iTrace)%useO3Climatology ) then
          call logDebug("WARNING - TOMS V8 ozone climatology is used")
          call logDebug("          but interpolation has not fully been tested")
        end if ! useO3Climatology
      end do ! iTrace

      ! internal field is written only for special cases
      if ( controlSimS%writeInternalFieldUpHR .or. controlSimS%writeInternalFieldUp .or. &
           controlSimS%writeInternalFieldDownHR .or. controlSimS%writeInternalFieldDown ) then

        if ( controlSimS%useReflectanceFromFile ) then

          if ( controlRetrS%singleScatteringOnly .or. ( controlRetrS%method /= 0 ) ) then
            call logDebug( 'ERROR in configuration file: internal field can not be written.')
            call logDebug( 'Internal field output is selected in subsection additional')
            call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can only')
            call logDebug( 'be written if method = 0 in subsection method in SECTION GENERAL.')
            call logDebug('Further, atmosphericScattering = 2 is required for the internal field')
            call logDebug( 'see subsection RTM_Sim_Retr in SECTION RADIATIVE_TRANSFER')
            call mystop(errS, 'stopped because of error in configuration file')
            if (errorCheck(errS)) return
          end if

          ! test for surface emission for retrieval
          do iband = 1, numSpectrBands
            if ( any( surfaceRetrS(iband)%emissionAP(:) > 0.0d0 ) ) then
              call logDebug( 'ERROR in configuration file: internal field can not be written.')
              call logDebug( 'Internal field output is selected in subsection additional')
              call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can not')
              call logDebug( 'deal with the surface emission specified for retrieval')
              call logDebugI( 'in spectral band: ', iband)
              call mystop(errS, 'stopped because internal field can not deal with surface emission')
              if (errorCheck(errS)) return
            end if
          end do ! iband

          ! test for rotational Raman scattering for retrieval
          do iband = 1, numSpectrBands
            if ( RRS_RingRetrS(iband)%useRRS .or. RRS_RingRetrS(iband)%fitDiffRingSpec .or.      &
                 RRS_RingRetrS(iband)%fitRingSpec .or. RRS_RingRetrS(iband)%approximateRRS .or. &
                 RRS_RingRetrS(iband)%addDiffRingSpec .or. RRS_RingRetrS(iband)%addRingSpec  ) then
              call logDebug( 'ERROR in configuration file: internal field can not be written.')
              call logDebug( 'Internal field output is selected in subsection additional')
              call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can not')
              call logDebug( 'deal with the rotational Raman scattering specified for retrieval')
              call logDebugI( 'in spectral band: ', iband)
              call logDebug('set useRRS, approximateRRS, fitDiffRingSpec, fitRingSpec,  ')
              call logDebug('addDiffRingSpec, andaddRingSpec all to zero')
              call mystop(errS, 'stopped because internal field can not deal with RRS')
              if (errorCheck(errS)) return
            end if
          end do ! iband

        else ! useReflectanceFromFile

          if ( controlSimS%simulationOnly ) then

            if ( controlSimS%singleScatteringOnly .or. ( controlSimS%method /= 0 )  ) then
              call logDebug( 'ERROR in configuration file: internal field can not be written.')
              call logDebug( 'Internal field output is selected in subsection additional')
              call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can only')
              call logDebug( 'be written if method = 0 in subsection method in SECTION GENERAL.')
              call logDebug('Further, atmosphericScattering = 2 is required for the internal field')
              call logDebug( 'see subsection RTM_Sim_Retr in SECTION RADIATIVE_TRANSFER')
              call mystop(errS, 'stopped because of error in configuration file')
              if (errorCheck(errS)) return
            end if

            ! test for surface emission for simulation
            do iband = 1, numSpectrBands
              if ( any( surfaceSimS(iband)%emission(:)  > 0.0d0 ) ) then
                call logDebug( 'ERROR in configuration file: internal field can not be written.')
                call logDebug( 'Internal field output is selected in subsection additional')
                call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can not')
                call logDebug( 'deal with the surface emission specified for simulation')
                call logDebugI( 'in spectral band: ', iband)
                call mystop(errS, 'stopped because internal field can not deal with surface emission')
                if (errorCheck(errS)) return
              end if
            end do

            ! test for rotational Raman scattering for simulation
            do iband = 1, numSpectrBands
              if (RRS_RingSimS(iband)%useRRS          .or. RRS_RingSimS(iband)%fitDiffRingSpec .or. &
                  RRS_RingSimS(iband)%fitRingSpec     .or. RRS_RingSimS(iband)%approximateRRS  .or. &
                  RRS_RingSimS(iband)%addDiffRingSpec .or. RRS_RingSimS(iband)%addRingSpec ) then
                call logDebug( 'ERROR in configuration file: internal field can not be written.')
                call logDebug( 'Internal field output is selected in subsection additional')
                call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can not')
                call logDebug( 'deal with the rotational Raman scattering specified for simulation')
                call logDebugI( 'in spectral band: ', iband)
                call logDebug('set useRRS, addDiffRingSpec, and addRingSpec all to zero')
                call mystop(errS, 'stopped because internal field can not deal with RRS')
                if (errorCheck(errS)) return
              end if
            end do ! iband

          else ! test also retrieval values

            if ( controlSimS%singleScatteringOnly .or. controlRetrS%singleScatteringOnly .or. &
                 ( controlSimS%method /= 0 ) .or. ( controlRetrS%method /= 0 ) ) then
              call logDebug( 'ERROR in configuration file: internal field can not be written.')
              call logDebug( 'Internal field output is selected in subsection additional')
              call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can only')
              call logDebug( 'be written if method = 0 in subsection method in SECTION GENERAL.')
              call logDebug('Further, atmosphericScattering = 2 is required for the internal field')
              call logDebug( 'see subsection RTM_Sim_Retr in SECTION RADIATIVE_TRANSFER')
              call mystop(errS, 'stopped because of error in configuration file')
              if (errorCheck(errS)) return
            end if

            ! test for surface emission for simulation and retrieval
            do iband = 1, numSpectrBands
              if ( any( surfaceSimS(iband)%emission(:)  > 0.0d0 ) ) then
                call logDebug( 'ERROR in configuration file: internal field can not be written.')
                call logDebug( 'Internal field output is selected in subsection additional')
                call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can not')
                call logDebug( 'deal with the surface emission specified for simulation')
                call logDebugI( 'in spectral band: ', iband)
                call mystop(errS, 'stopped because internal field can not deal with surface emission')
                if (errorCheck(errS)) return
              end if
            end do

            ! test for surface emission for retrieval
            do iband = 1, numSpectrBands
              if ( any( surfaceRetrS(iband)%emissionAP(:) > 0.0d0 ) ) then
                call logDebug( 'ERROR in configuration file: internal field can not be written.')
                call logDebug( 'Internal field output is selected in subsection additional')
                call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can not')
                call logDebug( 'deal with the surface emission specified for retrieval')
                call logDebugI( 'in spectral band: ', iband)
                call mystop(errS, 'stopped because internal field can not deal with surface emission')
                if (errorCheck(errS)) return
              end if
            end do ! iband

            ! test for rotational Raman scattering for simulation
            do iband = 1, numSpectrBands
              if (RRS_RingSimS(iband)%useRRS          .or. RRS_RingSimS(iband)%fitDiffRingSpec .or. &
                  RRS_RingSimS(iband)%fitRingSpec     .or. RRS_RingSimS(iband)%approximateRRS  .or. &
                  RRS_RingSimS(iband)%addDiffRingSpec .or. RRS_RingSimS(iband)%addRingSpec ) then
                call logDebug( 'ERROR in configuration file: internal field can not be written.')
                call logDebug( 'Internal field output is selected in subsection additional')
                call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can not')
                call logDebug( 'deal with the rotational Raman scattering specified for simulation')
                call logDebugI( 'in spectral band: ', iband)
                call logDebug('set useRRS, addDiffRingSpec, and addRingSpec all to zero')
                call mystop(errS, 'stopped because internal field can not deal with RRS')
                if (errorCheck(errS)) return
              end if
            end do ! iband

            ! test for rotational Raman scattering for retrieval
            do iband = 1, numSpectrBands
              if (RRS_RingRetrS(iband)%useRRS          .or. RRS_RingRetrS(iband)%fitDiffRingSpec .or. &
                  RRS_RingRetrS(iband)%fitRingSpec     .or. RRS_RingRetrS(iband)%approximateRRS  .or. &
                  RRS_RingRetrS(iband)%addDiffRingSpec .or. RRS_RingRetrS(iband)%addRingSpec ) then
                call logDebug( 'ERROR in configuration file: internal field can not be written.')
                call logDebug( 'Internal field output is selected in subsection additional')
                call logDebug('in SECTION ADDITIONAL_OUTPUT. However, the internal field can not')
                call logDebug( 'deal with the rotational Raman scattering specified for retrieval')
                call logDebugI( 'in spectral band: ', iband)
                call logDebug('set useRRS, fitDiffRingSpec, fitRingSpec, addDiffRingSpecAP, and ')
                call logDebug( 'addRingSpecAP all to zero')
                call mystop(errS, 'stopped because internal field can not deal with RRS')
                if (errorCheck(errS)) return
              end if
            end do ! iband

          end if ! controlSimS%simulationOnly

        end if ! useReflectanceFromFile

      end if ! write internal radiation field

      ! aerosolLayerHeight.
      if ( controlRetrS%aerosolLayerHeight ) then
        if ( controlRetrS%ignoreSlit ) then
          call logDebug( 'ERROR in configuration file:')
          call logDebug( 'the special flag aerosolLayerHeight /=0')
          call logDebug( 'and this can not be combined with')
          call logDebug( 'ignoreSlitRetr = 1')
          call logDebug( 'because line absorption for O2 is used')
          call mystop(errS, 'stopped because aerosolLayerHeight is selected when ignoreSlitRetr = 1')
          if (errorCheck(errS)) return
        end if ! ignoreSlit
        if ( controlRetrS%dimSV > 1 ) then
          call logDebug( 'ERROR in configuration file:')
          call logDebug( 'the special flag aerosolLayerHeight /=0')
          call logDebug( 'and this can not be combined with')
          call logDebug( 'dimSV > 1')
          call mystop(errS, 'stopped because aerosolLayerHeight is selected when dimSV > 1')
          if (errorCheck(errS)) return
        end if ! ignoreSlit
        do iband = 1, numSpectrBands
          if ( RRS_RingRetrS(iband)%useRRS .or. RRS_RingRetrS(iband)%approximateRRS .or. &
               RRS_RingRetrS(iband)%fitDiffRingSpec .or. RRS_RingRetrS(iband)%fitRingSpec ) then
            call logDebug( 'ERROR in configuration file:')
            call logDebug( 'the special flag aerosolLayerHeight /=0')
            call logDebug( 'and this can not be combined with')
            call logDebug( 'any use of RRS or fitting Ring spectra')
            call mystop(errS, 'stopped because aerosolLayerHeight is selected and RRS/Ring')
            if (errorCheck(errS)) return
          end if ! RRS/Ring
        end do ! iband
      end if

      ! tropospheric ozone profille retrieval
      ! ensure that the first part of the state vector is the tropospheric profile
      do iTrace = 1, nTrace
        if ( trim(traceGasRetrS(iTrace)%nameTraceGas) == 'trop_O3' ) then
          if ( traceGasRetrS(iTrace)%fitProfile ) then
            if ( iTrace > 1 ) then
              call logDebug( 'ERROR in configuration file:')
              call logDebug( 'when tropospheric ozone profile is fitted')
              call logDebug( 'it must be the first trace gas listed in the configuration file')
              call logDebugI( 'currently it is trace gas number:', iTrace)
              call logDebug( 'please move "SECTION trop_O3" upwards in config file')
              call mystop(errS, 'stopped because trop_O3 is not the first trace gas in config file')
              if (errorCheck(errS)) return
            end if ! iTrace > 1
          end if ! traceGasRetrS(iTrace)%fitProfile = .true.
        end if ! trim(traceGasRetrS(iTrace)%nameTraceGas) == 'trop_O3'
      end do ! iTrace

      ! Perform tests when writing polarazation correction files is selected in additional output
      if ( controlRetrS%polCorrectionFile ) then
        if ( controlRetrS%useReflectanceFromFile .or. &
             controlRetrS%simulationOnly         .or. &
             controlRetrS%aerosolLayerHeight     .or. &
             controlRetrS%ignoreSlit             .or. &
             (controlRetrS%dimSV /= 1)           .or. &
             (controlSimS%dimSV   < 3)           .or. &
             (retrS%maxNumIterations > 1) ) then
              write(errS%temp,*)  'ERROR in configuration file:'
              call errorAddLine(errS, errS%temp)
              write(errS%temp,*)  'polCorrectionFile is .true. but settings are incorrect'
              call errorAddLine(errS, errS%temp)
              write(errS%temp,*)  'see verifyConfigurationModule around line 1435 for settings'
              call errorAddLine(errS, errS%temp)
              call mystop(errS, 'stopped because setting are incorrect for pol. correction files')
              if (errorCheck(errS)) return
        end if
      end if ! polCorrectionFile

      ! Perform tests when the testChandrasekhar flag is set
      ! The Chandrasekar expression for the surface albedo
      !     R(A) = R(0) + A t*(mu) t(mu0) / [ 1 - A s* ]
      ! can be derived for pure monochromatic light. Here
      ! R(A) is the reflectance when the Lambertian surface albedo is A
      ! R(0) is the reflectance when the Lambertian surface albedo is 0
      ! t*(mu) is the total transmittance from the surface to TOA (black surface)
      ! t(mu0) is the total transmittance from TOA to the black surface
      ! s* is the spherical albedo for illumination at the bottom of the atmosphere
      ! Due to reciprocity we have t*(mu) = t(mu)

      ! In case of wide slit functions or when inelastic scattering is present (RRS)
      ! The expression will be an approximation and the meaning of t(mu0),t*(mu), and s*
      ! is not so well defined. However, we can assume that  t*(mu) = t(mu) still holds
      ! and that the the expression can be used as an interpolation formula. That is,
      ! we can calculate the reflectance for three values of the surface albedo, 
      ! A = 0, 0.5, and 1.0, and then calculate R(0), t(mu), and s* from these reflectances.
      if ( controlRetrS%testChandrasekhar ) then
        if ( numSpectrBands /= 3 ) then
          call logDebug( 'ERROR in configuration file:')
          call logDebug( 'when testChandrasekhar is used the number of spectral bands must be 3')
          call mystop(errS, 'stopped because the number of spectral bands must be 3')
          if (errorCheck(errS)) return
        end if
        if ( controlSimS%method /= 0 ) then
          call logDebug( 'ERROR in configuration file:')
          call logDebug( 'when testChandrasekhar is used the method must be 0')
          call mystop(errS, 'stopped because the simulationMethod is not 0 ')
          if (errorCheck(errS)) return
        end if
        if ( cloudAerosolRTMgridSimS%useAlbedoLambSurfAllBands ) then
          call logDebug( 'ERROR in configuration file:')
          call logDebug( 'when testChandrasekhar is used the surfaceTypeSim must be wavelDependent')
          call mystop(errS, 'stopped because incorrect surfaceType for simulation')
          if (errorCheck(errS)) return
        end if
        do iband = 1, numSpectrBands
          if ( surfaceSimS(iband)%nAlbedo /= 1 ) then
            call logDebug( 'ERROR in configuration file:')
            call logDebug( 'when testChandrasekhar is used the number albedo values')
            call logDebug( 'must be one for each spectral band for simulation')
            call mystop(errS, 'stopped because too many surface albedo values are specified')
            if (errorCheck(errS)) return
          end if
        end do
        if ( abs ( surfaceSimS(1)%albedo(1) ) > 1.0E-6          .or. &
             abs ( surfaceSimS(2)%albedo(1) - 0.5d0 ) > 1.0E-6  .or. &
             abs ( surfaceSimS(3)%albedo(1) - 1.0d0 ) > 1.0E-6 ) then
        call logDebug( 'ERROR in configuration file:')
        call logDebug( 'when testChandrasekhar is used the surface albedo values')
        call logDebug( 'for simulation must be 0.0, 0.5, and 1.0 for the three spectral bands')
        call mystop(errS, 'stopped because too many surface albedo values are specified')
        if (errorCheck(errS)) return
        end if
        do iband = 1, numSpectrBands
          if ( abs( surfaceSimS(iband)%emission(1) ) > 1.0d-10) then
            call logDebug( 'ERROR in configuration file:')
            call logDebug( 'when testChandrasekhar is used the surfEmission must be 0.0')
            call logDebug( 'for each spectral band for simulation')
            call mystop(errS, 'stopped because there is surface emission specified')
            if (errorCheck(errS)) return
          end if
        end do
        if ( abs(geometrySimS%SZA - geometrySimS%VZA) > 1.0d-4 ) then
            call logDebug( 'ERROR in configuration file:')
            call logDebug( 'when testChandrasekhar is used VZA and SZA must be equal')
            call mystop(errS, 'stopped because VZA differs from SZA')
            if (errorCheck(errS)) return
        end if
      end if !controlRetrS%testChandrasekhar

      ! test geometries only if simulation and retrieval are used
      ! if the geometries differ more than threshDiffGeometry (set in dataStructures file)
      ! issue a warning and continue processing
#ifndef TROPNLL2DP
       if ( .not. controlRetrS%useReflectanceFromFile ) then
         if ( abs( geometrySimS%sza - geometryRetrS%sza) > threshDiffGeometry ) &
           call logDebug('WARNING sza differs for simulation and retrieval in config file ')
         if ( abs( geometrySimS%vza - geometryRetrS%vza) > threshDiffGeometry ) &
           call logDebug('WARNING sza differs for simulation and retrieval in config file ')
         if ( abs( geometrySimS%s_azimuth - geometryRetrS%s_azimuth) > threshDiffGeometry ) &
           call logDebug('WARNING s_azimuth differs for simulation and retrieval in config file ')
         if ( abs( geometrySimS%v_azimuth - geometryRetrS%v_azimuth) > threshDiffGeometry ) &
           call logDebug('WARNING v_azimuth differs for simulation and retrieval in config file ')
       end if ! .not. controlRetrS%useReflectanceFromFile
#endif

       ! ADD code to verify settings for a LUT
       ! - make sure that pressure levels for intervals are the same for simulation and retrieval
       ! - make sure that the maximum number of iterations is 1
       ! - make sure that  globalS%controlSimS%useReflectanceFromFile is .false.
       ! - make sure that globalS%inputS%operational /= 1
       ! - make sure that surfPressureSim equals surfPressureRetr
       ! - make sure that offsets are not present
       ! - give warning when aerosol or cloud is present
       ! - make sure that correction for a spherical atmosphere is .true.


      !! test on operational version
      !if ( operational == 1 ) then
      !  if ( controlRetrS%dimSV > 1 ) then
      !    write(errS%temp,*)  'ERROR in configuration file:'
      !    call errorAddLine(errS, errS%temp)
      !    write(errS%temp,*)  'for operational use dimSVRetr should be 1'
      !    call errorAddLine(errS, errS%temp)
      !    call mystop(errS, 'stopped because dimSVRetr > 1 in config file')
      !    if (errorCheck(errS)) return
      !  end if ! controlRetrS%dimSV > 1
      !
      !  do iband = 1, numSpectrBands
      !    if ( polCorrectionRetrS(iband)%usePolarizationCorrection == 0 ) then
      !      write(errS%temp,*)  'ERROR in configuration file:'
      !      call errorAddLine(errS, errS%temp)
      !      write(errS%temp,*)  'for operational use usePolarizationCorrection should be 1'
      !      call errorAddLine(errS, errS%temp)
      !      call mystop(errS, 'stopped because usePolarizationCorrection for retrieval is not 1')
      !      if (errorCheck(errS)) return
      !    end if ! controlRetrS%dimSV > 1
      !
      !    if ( RRS_RingRetrS(iband)%useRRS /= 0 ) then
      !      write(errS%temp,*)  'ERROR in configuration file:'
      !      call errorAddLine(errS, errS%temp)
      !      write(errS%temp,*)  'for operational use useRRS should be 0'
      !      call errorAddLine(errS, errS%temp)
      !      call mystop(errS, 'stopped because useRRS for retrieval is not 0 in config file')
      !      if (errorCheck(errS)) return
      !    end if ! controlRetrS%dimSV > 1
      !
      !    if ( RRS_RingRetrS(iband)%approximateRRS /= 0 ) then
      !      write(errS%temp,*)  'ERROR in configuration file:'
      !      call errorAddLine(errS, errS%temp)
      !      write(errS%temp,*)  'for operational use approximateRRS should be 0'
      !      call errorAddLine(errS, errS%temp)
      !      call mystop(errS, 'stopped because approximateRRS for retrieval is not 0 in config file')
      !      if (errorCheck(errS)) return
      !    end if ! controlRetrS%dimSV > 1
      !
      !    if ( RRS_RingRetrS(iband)%useCabannes /= 0 ) then
      !      write(errS%temp,*)  'ERROR in configuration file:'
      !      call errorAddLine(errS, errS%temp)
      !      write(errS%temp,*)  'for operational use useCabannes should be 0'
      !      call errorAddLine(errS, errS%temp)
      !      call mystop(errS, 'stopped because useCabannes for retrieval is not 0 in config file')
      !      if (errorCheck(errS)) return
      !    end if ! controlRetrS%dimSV > 1
      !
      !  end do ! iband
      !
      !end if ! operational == 1 

      ! checks for creating and using a LUT for the absorption cross section

      ! make sure that the expansion coefficients are calculated before they are used for the non-operational code
      if ( operational == 0 ) then
        if ( controlSimS%usePolyExpXsec ) then
          do iband = 1, numSpectrBands
            do iTrace = 1, nTrace
              if ( .not. XsecHRLUTSimS(iband, iTrace)%createXsecPolyLUT ) then
                call logDebug( 'ERROR in configuration file:')
                call logDebug( 'usePolyExpXsec is .true. for simulation while createXsecPolyLUT is .false.')
                call logDebug( 'the LUT is needed to use the expansion in Legendre polynomials')
                call mystop(errS, 'stopped because calculation of XsecLUT skipped')
                if (errorCheck(errS)) return
              end if ! createXsecPolyLUT
            end do ! iTrace
          end do ! iband
        end if ! controlSimS%usePolyExpXsec
        if ( controlRetrS%usePolyExpXsec ) then
          do iband = 1, numSpectrBands
            do iTrace = 1, nTrace
              if ( .not. XsecHRLUTRetrS(iband, iTrace)%createXsecPolyLUT ) then
                call logDebug( 'ERROR in configuration file:')
                call logDebug( 'usePolyExpXsec is .true. for retrieval while createXsecPolyLUT is .false.')
                call logDebug( 'the LUT is needed to use the expansion in Legendre polynomials')
                call mystop(errS, 'stopped because calculation of XsecLUT skipped')
                if (errorCheck(errS)) return
              end if ! createXsecPolyLUT
            end do ! iTrace
          end do ! iband
        end if ! controlSimS%usePolyExpXsec
      end if ! operational

      ! The number of expansion coefficients is limited to maxNumExpansionCoeffXsec
      ! we test only values for simulation as these are identical to those for retrieval
      do iband = 1, numSpectrBands
        do iTrace = 1, nTrace
          if ( XsecHRLUTSimS(iband, iTrace)%createXsecPolyLUT ) then
            if ( XsecHRLUTSimS(iband, iTrace)%ncoeff_lnp_LUT > maxNumExpansionCoeffXsec ) then
                  call logDebug( 'ERROR in configuration file:')
                  call logDebug( 'too many expansion coefficients for the absorption')
                  call logDebug( 'cross section are specified')
                  call logDebug( 'check maxNumExpansionCoeffXsec in dataStructures.f90')
                  call mystop(errS, 'stopped because too many coefficients are specified')
                  if (errorCheck(errS)) return
            end if ! XsecHRLUTSimS%ncoeff_lnp_LUT > maxNumExpansionCoeffXsec
            if ( XsecHRLUTSimS(iband, iTrace)%ncoeff_lnT_LUT > maxNumExpansionCoeffXsec ) then
                  call logDebug( 'ERROR in configuration file:')
                  call logDebug( 'too many expansion coefficients for the absorption')
                  call logDebug( 'cross section are specified')
                  call logDebug( 'check maxNumExpansionCoeffXsec in dataStructures.f90')
                  call mystop(errS, 'stopped because too many coefficients are specified')
                  if (errorCheck(errS)) return
            end if ! XsecHRLUTSimS%ncoeff_lnT_LUT > maxNumExpansionCoeffXsec
          end if ! XsecHRLUTSimS(iband, iTrace)%createXsecPolyLUT
        end do ! iTrace
      end do ! iband

      ! make sure that the integration used to calculate the expansion coefficients
      ! is sufficiently accurate
      ! we test only values for simulation as these are identical to those for retrieval
      do iband = 1, numSpectrBands
        do iTrace = 1, nTrace
          if ( XsecHRLUTSimS(iband, iTrace)%createXsecPolyLUT ) then
            if ( XsecHRLUTSimS(iband, iTrace)%ncoeff_lnp_LUT >  XsecHRLUTSimS(iband, iTrace)%nPressure_LUT ) then
                  call logDebug( 'ERROR in configuration file:')
                  call logDebug( 'calculation of expansion coefficients for the absorption')
                  call logDebug( 'cross section are probably inaccurate')
                  call logDebug( 'increase nPressureLUT in configuration file')
                  call mystop(errS, 'stopped because calculation may be inaccurate')
                  if (errorCheck(errS)) return
            end if ! XsecHRLUTSimS%ncoeff_lnp_LUT > maxNumExpansionCoeffXsec
            if ( XsecHRLUTSimS(iband, iTrace)%ncoeff_lnT_LUT > XsecHRLUTSimS(iband, iTrace)%nTemp_LUT ) then
                  call logDebug( 'ERROR in configuration file:')
                  call logDebug( 'calculation of expansion coefficients for the absorption')
                  call logDebug( 'cross section are probably inaccurate')
                  call logDebug( 'increase nTempLUT in configuration file')
                  call mystop(errS, 'stopped because calculation may be inaccurate')
                  if (errorCheck(errS)) return
            end if ! XsecHRLUTSimS%ncoeff_lnT_LUT > maxNumExpansionCoeffXsec
          end if ! XsecHRLUTSimS(iband, iTrace)%createXsecPolyLUT
        end do ! iTrace
      end do ! iband

      ! Modify the a priori error of the cloud albedo or the surface albedo when
      ! a threshold for the cloud fraction is used. Then formally the surface albedo and
      ! the cloud albedo are fitted but the a priori variance of one of them is set to 1.0E-8
      ! so that effectively only one of them is fitted. Also, the lenght of the state vactor
      ! remains the same.
      ! The a priori errors for the cloud/surface albedo are not modified when
      ! useThresholdCloudFraction = .false.
      if ( cloudAerosolRTMgridRetrS%useThresholdCldFraction ) then 
        if ( cloudAerosolRTMgridRetrS%fitAlbedoLambCldAllBands ) then
          if ( cloudAerosolRTMgridRetrS%fitAlbedoLambSurfAllBands ) then
            call setAPvarianceSurfCldAlbedo( errS, numSpectrBands, cloudAerosolRTMgridRetrS, &
                                             LambCloudRetrS, surfaceRetrS )
          else
            call logDebug( 'ERROR in configuration file:')
            call logDebug( 'useThresholdCldFraction is .true.')
            call logDebug( 'while the surface albedo is not fitted')
            call mystop(errS, 'stopped because the surface albedo is not fitted')
            if (errorCheck(errS)) return
          end if ! fit surface albedo
        else
           call logDebug( 'ERROR in configuration file:')
           call logDebug( 'useThresholdCloudFraction is .true.')
           call logDebug( 'while the cloud albedo is not fitted')
           call mystop(errS, 'stopped because the cloud albedo is not fitted')
           if (errorCheck(errS)) return
        end if ! fit cloud albedo
      end if ! useThresholdCloudFraction

      ! check specification of signal to noise ratio for earth Radiance
      do iband = 1, numSpectrBands
        if ( earthRadianceSimS(iband)%SNS%useLAB_SNR .and. &
             earthRadianceSimS(iband)%SNS%use_S5_SNR ) then
           call logDebug( 'ERROR in configuration file:')
           call logDebug( 'in SECTION INSTRUMENT')
           call logDebug( 'in subsection SNR_radiance')
           call logDebug( 'useLAB_SNR and useS5_SNR cannot both be true')
           call mystop(errS, 'stopped because useLAB_SNR and useS5_SNR are both true')
           if (errorCheck(errS)) return
        end if
      end do
    end subroutine verifyConfigFile

    subroutine setAPvarianceSurfCldAlbedo( errS, numSpectrBands, cloudAerosolRTMgridRetrS, &
                                           LambCloudRetrS, surfaceRetrS )

      type(errorType),    intent(inout) :: errS
      integer,            intent(inout) :: numSpectrBands
      type(cloudAerosolRTMgridType)     :: cloudAerosolRTMgridRetrS
      type(LambertianType),     pointer :: surfaceRetrS(:)
      type(LambertianType),     pointer :: LambCloudRetrS(:)

      ! local
      integer  :: iband

      ! demand that the cloud fraction is wavelength independent
      ! if not, then produce an error message
      if ( cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then

        ! case where the surface albedo and the cloud albedo is the same for all wavelengths
        if ( cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands .and. &
             cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then
          if ( cloudAerosolRTMgridRetrS%cldAerfractionAllBands  >  &
               cloudAerosolRTMgridRetrS%thresholdCldFraction )  then
               cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP = 1.0E-8
          else
               cloudAerosolRTMgridRetrS%varAlbedoLambCldAllAP = 1.0E-8
          end if
        end if

        ! case where the cloud albedo is wavelength independent but the
        ! surface albedo can be wavelength dependent
        if (       cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands .and. &
             .not. cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then
          if ( cloudAerosolRTMgridRetrS%cldAerfractionAllBands  >  &
               cloudAerosolRTMgridRetrS%thresholdCldFraction )  then
            do iband = 1, numSpectrBands
               surfaceRetrS(iband)%varianceAlbedo(:) = 1.0E-8
            end do
          else
               cloudAerosolRTMgridRetrS%varAlbedoLambCldAllAP = 1.0E-8
          end if
        end if

        ! case where the cloud albedo can be wavelength dependent but the
        ! surface albedo is wavelength independent
        if ( .not. cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands .and. &
                   cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then
          if ( cloudAerosolRTMgridRetrS%cldAerfractionAllBands  >  &
               cloudAerosolRTMgridRetrS%thresholdCldFraction )  then
               cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP = 1.0E-8
          else
            do iband = 1, numSpectrBands
               LambCloudRetrS(iband)%varianceAlbedo(:) = 1.0E-8
            end do
          end if
        end if

        ! case where the cloud albedo and surface albedo can be wavelength dependent
        if ( .not. cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands .and. &
             .not. cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then
          if ( cloudAerosolRTMgridRetrS%cldAerfractionAllBands  >  &
               cloudAerosolRTMgridRetrS%thresholdCldFraction )  then
            do iband = 1, numSpectrBands
               surfaceRetrS(iband)%varianceAlbedo(:) = 1.0E-8
            end do
          else
            do iband = 1, numSpectrBands
               LambCloudRetrS(iband)%varianceAlbedo(:) = 1.0E-8
            end do
          end if
        end if


      else
        call logDebug( 'ERROR in configuration file:')
        call logDebug( 'the cloud fraction MUST be wavelength independent')
        call logDebug( 'when a threshold is used to determine whether the')
        call logDebug( 'surface albedo or the cloud albedo is fitted')
        call mystop(errS, 'stopped because cloud fraction is wavelength dependent')
        if (errorCheck(errS)) return
      end if

    end subroutine setAPvarianceSurfCldAlbedo

  end module verifyConfigFileModule

