!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

! ------------------------------------------------------------------------------
!
!
! ------------------------------------------------------------------------------

module inputModule

use dataStructures
use disamar_file

implicit none

character, parameter :: space = " ", bra = "(", ket = ")",  &
    comma = ",", squote = "'", dquote = '"', tab = achar(9)

integer, parameter :: dp = kind(1d0)
integer, parameter :: lrecl = 128

interface readf
  module procedure reads, readd
end interface

private
public :: readline, parse, reada, readf, readi

contains

! ------------------------------------------------------------------------------

subroutine readline(errS, eof, file)

    type(errorType), intent(inout) :: errS
    logical, intent(out) :: eof
    type(file_type) :: file

    character(len=255) :: str
    character :: term

    integer :: i, j

    eof = .false.

    file%parser%char = ""

    lines: do
        file%parser%more = .true.
        j = 1
        do while (file%parser%more)
            file%parser%last = j + lrecl - 1
            file%parser%line(file%parser%level) = file%parser%line(file%parser%level) + 1
            if (file_readline(file, str) /=  0) goto 900
            file%parser%char(j:file%parser%last) = str
            file%parser%last = verify(file%parser%char,space//tab,back = .true.)
            if (file%parser%lc .gt. 0 .and. file%parser%last .ge. file%parser%lc) then
              file%parser%more = (file%parser%char(file%parser%last - file%parser%lc + 1:file%parser%last) .eq. file%parser%concat)
                if (file%parser%more) then
                    j = file%parser%last-file%parser%lc + 1
                endif
            else
                file%parser%more = .false.
            endif
        end do

        do while (index(file%parser%char, tab) .gt. 0)
            i = index(file%parser%char, tab)
            file%parser%char(i:i) = space
        end do

        i = 1
        do while (file%parser%char(i:i) .eq. space .and. i .lt. file%parser%last)
            i = i + 1
        end do
        if (file%parser%char(i:i) .eq. "#") then
            cycle lines
        endif

        call parse(errS, file)
        if (errorCheck(errS)) return

        if (file%parser%nitems .eq. 0 .and. file%parser%skipbl) then
            cycle lines
        else
            exit lines
        end if

    end do lines

    return

900 continue
    if (file%parser%more .and. j .gt. 1) then
        call mystop(errS, 'unexpected end-of-file')
        if (errorCheck(errS)) return
    endif
    file%parser%char(1:file%parser%last) = ""
    file%parser%item = 0
    file%parser%nitems = -1
    eof = .true.
    return

end subroutine readline

! ------------------------------------------------------------------------------

subroutine parse(errS, file)
 
type(errorType), intent(inout) :: errS
type(file_type), intent(inout) :: file

integer :: i, state, nest
logical :: tcomma
character :: term, c

i = 0
nest = 0
state = 0
tcomma = .true.
      
file%parser%item = 0
file%parser%nitems = 0

chars: do

    i = i + 1
    if (i .gt. file%parser%last) then
        if (file%parser%nitems .gt. 0) then
            select case(state)
                case(1)
                    call mystop(errS, 'Closing quote missing')
                    if (errorCheck(errS)) return
                case(2)
                    file%parser%end(file%parser%nitems) = i - 1
            end select
        endif
        return
    endif

    c = file%parser%char(i:i)
    term = squote
    select case (state)

        case(0)
            select case(c)
                case(space,tab)
                case(bra)
                    nest = 1
                    state = 3
                case(squote,dquote)
                    file%parser%nitems = file%parser%nitems + 1
                    file%parser%loc(file%parser%nitems) = i
                    term = c
                    state = 1
                case(comma)
                    if (tcomma) then
                        file%parser%nitems = file%parser%nitems + 1
                        file%parser%loc(file%parser%nitems) = 0
                    endif
                    tcomma = .true.
                case default
                    file%parser%nitems = file%parser%nitems + 1
                    file%parser%loc(file%parser%nitems) = i
                    state = 2
            end select
    
        case(1)
            if (c .eq. term) then
                file%parser%end(file%parser%nitems) = i
                state = 4
            endif

        case(2)
            select case(c)
                case(space,tab)
                    file%parser%end(file%parser%nitems) = i - 1
                    state = 0
                    tcomma = .false.
                case(comma)
                    file%parser%end(file%parser%nitems) = i-1
                    state = 0
                    tcomma = .true.
            end select

        case(3)
            select case(c)
                case(bra)
                    nest = nest + 1
                case(ket)
                    nest = nest - 1
                    if (nest .eq. 0) then
                        state = 0
                    endif
            end select

        case(4)
            select case(c)
                case(space,tab)
                    tcomma = .false.
                    state = 0
                case(comma)
                    tcomma = .true.
                    state = 0
                case(bra)
                    tcomma = .false.
                    state = 3
                    nest = 1
            end select
      
    end select
    
end do chars

end subroutine parse

! ------------------------------------------------------------------------------

subroutine reada(errS, file, str)

    type(errorType), intent(inout) :: errS
    type(file_type) :: file
    character(len=*), intent(inout) :: str
    integer :: i

    if (file%parser%clear) str = ""
    if (file%parser%item .ge. file%parser%nitems) return

    file%parser%item = file%parser%item + 1
    if (file%parser%loc(file%parser%item) .eq. 0) return

    i = file%parser%loc(file%parser%item)
    if (file%parser%char(i:i) .eq. squote .or. file%parser%char(i:i) .eq. dquote) then
        str = file%parser%char(i+1:file%parser%end(file%parser%item) - 1)
    else
        str = file%parser%char(i:file%parser%end(file%parser%item))
    endif

end subroutine reada

! ------------------------------------------------------------------------------

subroutine readd(errS, file, a, factor)

    type(errorType), intent(inout) :: errS
    type(file_type) :: file
    double precision, intent(inout) :: a
    double precision, intent(in), optional :: factor

    character(len=50) :: str

    if (file%parser%clear) a = 0d0
    if (file%parser%item .ge. file%parser%nitems) return

    str = ""
    call reada(errS, file, str)
    if (errorCheck(errS)) return
    if (str ==  "") return
    read (unit=str, fmt=*, err=999) a
! JdH Debug
! write(*,*) string
! end Debug
    if (present(factor)) then
        a = a / factor
    endif
    return

999 continue
    call mystop(errS, 'invalid double precision value')
    if (errorCheck(errS)) return

end subroutine readd

! ------------------------------------------------------------------------------

subroutine reads(errS, file, a, factor)

    type(errorType), intent(inout) :: errS
    type(file_type) :: file
    real, intent(inout) :: a
    real, intent(in), optional :: factor

    double precision :: aa

    if (present(factor)) then
        call readd(errS, file, aa, real(factor,dp))
        if (errorCheck(errS)) return
    else
      call readd(errS, file, aa)
      if (errorCheck(errS)) return
    endif
    a = aa

end subroutine reads

! ------------------------------------------------------------------------------

subroutine readi(errS, file, i)

    type(errorType), intent(inout) :: errS
    type(file_type) :: file
    integer, intent(inout) :: i

    character(len=50) :: str

    if (file%parser%clear) i = 0
    if (file%parser%item .ge. file%parser%nitems) return

    str = ""
    call reada(errS, file, str)
    if (errorCheck(errS)) return
    if (str ==  "") return
    read(unit=str, fmt=*, err=999) i
    return

999 continue
    call mystop(errS, 'invalid integer')
    if (errorCheck(errS)) return

end subroutine readi

! ------------------------------------------------------------------------------

end module inputModule
