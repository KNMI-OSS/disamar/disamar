!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

! **********************************************************************
! This module is part of DISAMAR
! The module classic_doasModule contains subroutines to
! use the classic DOAS (differential optical absorption spectroscopy) method
! to retrieve columns of trace gases by fitting the slant column
! and calculating separately the air mass factor for one wavelength.
!
! Author: Johan de Haan, November 2012
!
! **********************************************************************


module classic_doasModule

  use dataStructures
  use mathTools,                only: spline, splint, splintLin, svdfit, fpoly, svdcmp, &
                                      verfy_elem_differ, getSmoothAndDiffXsec, polyInt, slitfunction
  use radianceIrradianceModule, only: fillAltPresGridRTM, integrateSlitFunctionIrr
  use propAtmosphereModule,     only: getOptPropAtm, getAbsorptionXsec, update_altitudes_PTz_grid, &
                                      update_T_z_other_grids, update_ndens_vmr
  use LabosModule,              only: layerBasedOrdersScattering
  use readModule,               only: getHRSolarIrradiance
  use optimalEstmationModule,   only: calculateNewState
  use doasModule,               only: interpolateXsecRTMaltGrid, calcTemperatureNdensRTMgrid, convoluteWeakAbsSlitS, &
                                      calculateAltAveragedXsecHR, calcReflAndAMFRTM, finishDOAS
  use writeModule,              only: writeAltResolvedAMF
  use asciiHDFtools
    
  ! default is private
  private 
  public  :: classicDOASfit, fillCodesFitParamClDOAS

  contains

    subroutine classicDOASfit(errS, staticS,                                                                 &
                              numSpectrBands, nTrace, wavelInstrS, wavelHRSimS,  wavelMRRetrS, wavelHRRetrS, &
                              geometryS, XsecHRS, gasPTSimS, gasPTRetrS, traceGasSimS, traceGasRetrS,        &
                              solarIrrS, earthRadianceS, surfaceS, LambCloudS,                               &
                              mieAerS, cldAerFractionS, weakAbsRetrS, controlSimS, controlRetrS,             &
                              retrS, cloudAerosolRTMgridSimS, cloudAerosolRTMgridRetrS,                      &
                              RRS_RingSimS, RRS_RingRetrS, optPropRTMGridSimS, optPropRTMGridRetrS,          &
                              diagnosticS, reflDerivHRSimS)

      ! This subroutine performa a classical DOAS fit where the air mass factor is wavelength
      ! denpendent and is calculated on line.

      implicit none

      type(errorType),               intent(inout) :: errS
      type(staticType),              intent(in)    :: staticS
      integer,                       intent(in)    :: numSpectrBands                  ! number wavelength band
      integer,                       intent(in)    :: nTrace                          ! number of trace gases
      type(wavelInstrType),          intent(in)    :: wavelInstrS(numSpectrBands)     ! wavelength grid for retrieval
      type(wavelHRType),             intent(in)    :: wavelHRSimS(numSpectrBands)     ! high resolution wavelength grid sim
      type(wavelHRType),             intent(in)    :: wavelMRRetrS(numSpectrBands)    ! high/instr resolution wavelength grid retr
      type(wavelHRType),             intent(in)    :: wavelHRRetrS(numSpectrBands)    ! high resolution wavelength grid retr
      type(geometryType),            intent(in)    :: geometryS                       ! info geometry
      type(XsecType),                intent(inout) :: XsecHRS(numSpectrBands, nTrace) ! absorption cross section
      type(gasPTType),               intent(inout) :: gasPTSimS                       ! pressure and temperature
      type(gasPTType),               intent(inout) :: gasPTRetrS                      ! pressure and temperature
      type(traceGasType),            intent(inout) :: traceGasSimS(nTrace)            ! atmospheric trace gas properties
      type(traceGasType),            intent(inout) :: traceGasRetrS(nTrace)           ! atmospheric trace gas properties
      type(SolarIrrType),            intent(inout) :: solarIrrS(numSpectrBands)       ! solar irradiance
      type(earthRadianceType),       intent(inout) :: earthRadianceS(numSpectrBands)  ! earth radiance; info slit function
      type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)        ! surface properties
      type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)      ! Lambertian cloud properties
      type(mieScatType),             intent(inout) :: mieAerS(maxNumMieModels)        ! specification expansion coefficients
      type(cldAerFractionType),      intent(inout) :: cldAerFractionS(numSpectrBands) ! cloud / aerosol fraction
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands)    ! DOAS results
      type(controlType),             intent(inout) :: controlSimS                     ! control parameters for simulation
      type(controlType),             intent(inout) :: controlRetrS                    ! control parameters for retrieval
      type(retrType),                intent(inout) :: retrS                           ! retrieval structure
      type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridSimS         ! cloud-aerosol properties simulation
      type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridRetrS        ! cloud-aerosol properties retrieval
      type(RRS_RingType),            intent(inout) :: RRS_RingSimS(numSpectrBands)    ! Ring spectra for simulation
      type(RRS_RingType),            intent(inout) :: RRS_RingRetrS(numSpectrBands)   ! Ring spectra for retrieval
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridSimS              ! optical properties on RTM grid simulation
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridRetrS             ! optical properties on RTM grid retrieval
      type(diagnosticType),          intent(inout) :: diagnosticS                     ! diagnostic information
      type(reflDerivType),           intent(in)    :: reflDerivHRSimS(numSpectrBands) ! contains amf for simulation

      ! local
      integer           :: iband, iTrace, ialt
      integer           :: iteration

      type(wavelHRType) :: wavelRTMS(numSpectrBands)         ! wavelength for calculating the air mass factor

      type(XsecType)    :: XsecRTMS(numSpectrBands,nTrace)   ! absorption cross sections at the wavelength for the air mass factor
                                                             ! for the altitude grid specified in the configuration file, i.e.
                                                             ! based on the pressure grid where the vmr is specified

      ! Absorption cross sections on the high resolution wavelength grid and RTM altitude grid
      type(XsecType) :: XsecHRRTMaltS(numSpectrBands,nTrace)        ! same as XsecHRS, but 
                                                                    ! interpolated to the RTM altitude grid

      ! START of the calculations

      ! fill altitude grid for radiative transfer calculalations
      ! this altitude grid uses Gaussian division points for the intervals
      ! and can also be used to integrate over the altitude
      ! the altitude resolved air mass factors are calculated on this grid
      ! and we use it to calculate the monochromatic air mass factor for the
      ! entire atmosphere for each trace gas. In addition it is used to calculate
      ! the effective absorption cross section for each trace gas when the
      ! temperature is not fitted but the temperature profile is assumed to be known

      call fillAltPresGridRTM(errS, cloudAerosolRTMgridRetrS, optPropRTMGridRetrS)
      if (errorCheck(errS)) return

      ! calculate the number density and temperature for the RTM grid used later for the
      ! calculation of the air mass factor
      ! note: use an isothermal atmosphere only for the cross section, not for the number density
      call calcTemperatureNdensRTMgrid(errS, nTrace, gasPTRetrS, traceGasRetrS, optPropRTMGridRetrS)
      if (errorCheck(errS)) return

      ! NOTE that it ia assumed that the cloud altitude can not change (not a fit parameter)
      ! and therefor the RTM grid remains constant (not correct for O2-O2 but O2-O2 needs
      ! special treatment due to its pressure dependence which is not accounted for at present)

      ! claim memory and initialize weakAbsRetrS using geometrical air mass factors, XsecInstrS, and  XsecInstrRTMaltS
      ! initialize the number of wavelengths for the radiative transfer calculations using the value from the configuration file
      ! if differential cross sections are used the number of wavelengths and the array sizes will be updated.

      call initializeClassicDOAS(errS, numSpectrBands, nTrace, geometryS, traceGasRetrS, optPropRTMGridRetrS, XsecHRS, &
                                 weakAbsRetrS, wavelInstrS, wavelRTMS, retrS, XsecRTMS, XsecHRRTMaltS)
      if (errorCheck(errS)) return

      ! get solar irradiance on HR wavelength retrieval grid and convolute with the slit function
      ! this is used for the I0 effect for the effective cross section
      ! also fill Ring spectra using the slit function for the earth radiance

      do iband = 1, numSpectrBands
        call getHRSolarIrradiance(errS, wavelMRRetrS(iband), solarIrrS(iband), RRS_RingRetrS(iband), wavelHRRetrS(iband))
        if (errorCheck(errS)) return
        call integrateSlitFunctionIrr(errS, controlRetrS, wavelHRRetrS(iband), wavelInstrS(iband), solarIrrS(iband), &
             earthRadianceS(iband), RRS_RingRetrS(iband))
        if (errorCheck(errS)) return
      end do

      ! Get the absorption coefficients on HR wavelength grid at the retrieval altitude grid.
      ! This is the altitude grid specified in the configuration file which can differ for different trace gases.
      ! If the temperature is fitted, the absorption cross section and its temperature
      ! derivative at the reference temperature are stored in XsecHRS, otherwise the temperature
      ! derivative is set to zero and the absorption cross section for the local temperature
      ! and pressure is stored in XsecHRS

      do iband = 1, numSpectrBands
        do iTrace = 1, nTrace
          call getAbsorptionXsec(errS, staticS, controlRetrS, weakAbsRetrS(iband), wavelHRRetrS(iband), gasPTRetrS, &
                                 traceGasRetrS(iTrace), XsecHRS(iband,iTrace))
          if (errorCheck(errS)) return

          call interpolateXsecRTMaltGrid(errS, traceGasRetrS(iTrace), optPropRTMGridRetrS, XsecHRS(iband,iTrace), &
                                         XsecHRRTMaltS(iband,iTrace))
          if (errorCheck(errS)) return
        end do ! iTrace
      end do ! iband

      ! calculate the altitude averaged absorption cross section on the high resolution wavelength grid
      ! note that this cross section depends on the temperature and number density profile, but not
      ! on the column itself
      do iband = 1, numSpectrBands
        if ( weakAbsRetrS(iband)%calculateAMF ) then
          wavelRTMS(iband)%wavel(1) = weakAbsRetrS(iband)%wavelengthAMF
          call setXsecRTMclassicDOAS(errS, nTrace, wavelHRRetrS(iband), XsecHRS(iband,:), wavelRTMS(iband), XsecRTMS(iband,:))
          if (errorCheck(errS)) return

          ! calculate the altitude resolved amf using radiative transfer
          ! the calculation of the amf is based on the vertical column, profile, surface and
          ! cloud properties specified in the configuration file and is not affected by the slant column fit
          call calcReflAndAMFRTM(errS, nTrace, wavelRTMS(iband), XsecRTMS(iband,:), geometryS,  &
                                 RRS_RingRetrS(iband), controlRetrS, cloudAerosolRTMgridRetrS,  &
                                 gasPTRetrS, traceGasRetrS, surfaceS(iband), LambCloudS(iband), &
                                 mieAerS, mieAerS, cldAerFractionS(iband), OptPropRTMGridRetrS, &
                                 weakAbsRetrS(iband)%reflRTM, weakAbsRetrS(iband)%amfAltRTM,    &
                                 weakAbsRetrS(iband)%radianceCldFraction)
          if (errorCheck(errS)) return
          ! put values for the amf calculated for one wavelength into the amf for the high resolution grid
          do ialt = 0,  weakAbsRetrS(iband)%RTMnlayer
            weakAbsRetrS(iband)%amfAltHR(ialt,:) = weakAbsRetrS(iband)%amfAltRTM(ialt,1)
          end do ! ialt
          call calculate_amf_classicDOAS(errS, nTrace, XsecHRS(iband,:), OptPropRTMGridRetrS, wavelRTMS(iband), &
                                         traceGasRetrS, weakAbsRetrS(iband))
          if (errorCheck(errS)) return
 
          call calculateAltAveragedXsecHR(errS, nTrace, OptPropRTMGridRetrS, wavelHRRetrS(iband), XsecHRRTMaltS(iband,:), &
                                          weakAbsRetrS(iband))
          if (errorCheck(errS)) return
        else
          if ( weakAbsRetrS(iband)%useReferenceTempRetr ) then
            ! altitude average cross section does not depend on the air mass factor if a reference temperature is used
            call calculateAltAveragedXsecHR(errS, nTrace, OptPropRTMGridRetrS, wavelHRRetrS(iband), XsecHRRTMaltS(iband,:), &
                                            weakAbsRetrS(iband))
            if (errorCheck(errS)) return
          else
            write(errS%temp,*)
            call errorAddLine(errS, errS%temp)
            call logDebug('ERROR: in classic_doasModule in subroutine classicDOASfit')
            call logDebug('reference temperature is not used and air mass factor is not calculated')
            call logDebug('the altitude averaged absorption cross section can not be calculated')
            call logDebug('change flags in the configuration file in section GENERAL')
            call logDebug('subsection specifications_DOAS_DISMAS')
            call logDebug('keywords: useReferenceTempRetr and/or calculateAMFRetr')
            call mystop(errS, 'stopped because of specification error in configuration file')
            if (errorCheck(errS)) return
          end if ! weakAbsRetrS(iband)%useReferenceTempRetr
        end if !  weakAbsRetrS(iband)%calculateAMF

      end do ! iband

      ! convolute with slit function, set wavelength where AMF is to be calculated, and
      ! fill the absorption cross sections for the RTM calculation at the AMF wavelength 
      do iband = 1, numSpectrBands
        call convoluteWeakAbsSlitS(errS, nTrace, controlRetrS, wavelHRRetrS(iband), traceGasRetrS, solarIrrS(iband),  &
                                   earthRadianceS(iband), wavelInstrS(iband), weakAbsRetrS(iband))
        if (errorCheck(errS)) return
        do iTrace = 1, nTrace
          call getSmoothAndDiffXsec(errS, weakAbsRetrS(iband)%degreePoly, wavelInstrS(iband)%nwavel,  &
                                    wavelInstrS(iband)%wavel,                                   &
                                    weakAbsRetrS(iband)%XsecEffInstr(:,iTrace),                 &
                                    weakAbsRetrS(iband)%XsecEffSmoothInstr(:,iTrace),           &
                                    weakAbsRetrS(iband)%XsecEffDiffInstr(:,iTrace) )
          if (errorCheck(errS)) return
        end do ! iTrace
      end do ! iband

      call fillAPrioriClassicDOAS(errS, numSpectrBands, weakAbsRetrS, RRS_RingRetrS, &
                                  gasPTSimS, gasPTRetrS, retrS)
      if (errorCheck(errS)) return

      ! initialize
      retrS%isConverged = .false.
      ! set initial (starting) values
      retrS%x               = retrS%xa
      retrS%xPrev           = retrS%xa
      retrS%dx              = 0.0d0
      retrS%SInv_lnvmr      = 0.0d0
      retrS%SInv_vmr        = 0.0d0
      retrS%SInv_ndens      = 0.0d0

      do iteration = 1, retrS%maxNumIterations

        call logDebugI('iteration DOAS = ', iteration)
        retrS%numIterations = iteration

        ! the reflectance and the derivatives are calculated from the polynomials, the geometrical amf, and
        ! the vertical column
        call calcReflAndDerivClassicDOAS (errS, numSpectrBands, nTrace, controlRetrS, traceGasRetrS,  &
                                          wavelInstrS, weakAbsRetrS, RRS_RingRetrS, retrS)
        if (errorCheck(errS)) return

        ! update dR
        retrS%dR = retrS%reflMeas - retrS%refl

        ! fill initial residue
        if ( iteration == 1 ) retrS%dR_initial = retrS%dR

        ! calculate new state vector and set convergence flag retrS%isConverged
        call calculateNewState(errS, iteration, retrS)
        if (errorCheck(errS)) return

        ! update fit parameters
        call updateFitParametersClassicDOAS(errS, numSpectrBands, nTrace, traceGasRetrS, retrS, weakAbsRetrS, &
                                            RRS_RingRetrS, gasPTRetrS, surfaceS, cloudAerosolRTMgridRetrS, LambCloudS)
        if (errorCheck(errS)) return

        call updateCovFitParamClDOAS(errS, numSpectrBands, retrS, diagnosticS, weakAbsRetrS,  &
                                     RRS_RingRetrS, gasPTRetrS)

        ! update values in the retrieval structure
        retrS%reflPrev = retrS%refl
        retrS%xPrev    = retrS%x

        if ( retrS%isConverged ) exit  ! exit iteration loop

      end do ! iteration

      call calculateDiagnosticsClassicDOAS(errS, retrS, diagnosticS)
      if (errorCheck(errS)) return

      call updateCovFitParamClDOAS(errS, numSpectrBands, retrS, diagnosticS, weakAbsRetrS, RRS_RingRetrS, gasPTRetrS)
      if (errorCheck(errS)) return

      call print_resultsClassicDOAS(errS, numSpectrBands, nTrace, geometryS, wavelInstrS, traceGasSimS, &
                                     traceGasRetrS, OptPropRTMGridRetrS, weakAbsRetrS,                  &
                                     RRS_RingSimS, RRS_RingRetrS, retrS, diagnosticS,                   &
                                     controlSimS, controlRetrS, wavelHRSimS, cloudAerosolRTMgridSimS,   & ! The last two lines are used
                                     optPropRTMGridSimS, reflDerivHRSimS)                                 ! for printing the amf

      call print_classicDOAS_asciiHDF(errS, numSpectrBands, nTrace, geometryS, gasPTSimS,  gasPTRetrS,  &
                                      traceGasSimS, traceGasRetrS, optPropRTMGridRetrS,  XsecHRS,       &
                                      weakAbsRetrS, RRS_RingSimS, RRS_RingRetrS, retrS, diagnosticS,    &
                                      controlRetrS, surfaceS, LambCloudS)
      if (errorCheck(errS)) return

      ! clean up

      call finishDOAS(errS, numSpectrBands, nTrace, weakAbsRetrS, wavelRTMS,  XsecRTMS, XsecHRRTMaltS)
      if (errorCheck(errS)) return

    end subroutine classicDOASfit


    subroutine fillCodesFitParamClDOAS(errS, numSpectrBands, nTrace, traceGasS, weakAbsRetrS, RRS_RingS, gasPTS, retrS)


      ! fillCodesFitParamClDOAS is called in the beginning of the main program DISAMAR (errS, around line 278)
      ! Fill the array retrS%codeFitParameters with values for the parameters that are to be fitted
      ! using classical DOAS

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands               ! number of spectral bands
      integer,                       intent(in)    :: nTrace                       ! number of trace gases
      type(traceGasType),            intent(in)    :: traceGasS(nTrace)            ! properties of the trace gases
      type(weakAbsType),             intent(in)    :: weakAbsRetrS(numSpectrBands) ! DOAS results
      type(RRS_RingType),            intent(in)    :: RRS_RingS(numSpectrBands)    ! Ring spectra
      type(gasPTType),               intent(in)    :: gasPTS                       ! pressure and temperature
      type(retrType),                intent(inout) :: retrS                        ! retrieval parameters

      ! local
      integer :: counter, iband, index, istate, iTrace

      logical, parameter :: verbose = .false.

      ! reset maxfitparameters for classical DOAS to
      ! 2 * nTrace to be able to fit the colums of the trace gases and the temperature,
      ! the polynomial coefficients for the different spectral windows, and Ring spectra for each band
      retrS%maxfitparameters = 2 * nTrace &                           ! columns and effective temperatures of the trace gases
                             + sum(weakAbsRetrS(:)%degreePoly + 1) &  ! polynomial coefficient for each spectal band
                             + numSpectrBands                         ! Ring coefficient for each spectral band

      ! claim memory for the arrays that are used to identify the state vector element
      allocate( retrS%codeFitParameters(retrS%maxfitparameters) )          ! used
      allocate( retrS%codeSpecBand(retrS%maxfitparameters) )               ! used
      allocate( retrS%codeTraceGas(retrS%maxfitparameters) )               ! used
      allocate( retrS%codeAltitude(retrS%maxfitparameters) )               ! not used
      allocate( retrS%codeIndexSurfAlb(retrS%maxfitparameters) )           ! not used
      allocate( retrS%codeIndexSurfEmission(retrS%maxfitparameters) )      ! not used
      allocate( retrS%codeIndexMulOffset(retrS%maxfitparameters) )         ! not used
      allocate( retrS%codeIndexStraylight(retrS%maxfitparameters) )        ! not used
      allocate( retrS%codelnPolyCoef(retrS%maxfitparameters) )             ! used

      ! initialize
      counter                    = 0
      retrS%codeSpecBand         = 0
      retrS%codeTraceGas         = 0
      retrS%codeAltitude         = 0
      retrS%codeIndexSurfAlb     = 0
      retrS%codeIndexSurfEmission= 0
      retrS%codeIndexMulOffset   = 0
      retrS%codeIndexStraylight  = 0
      retrS%codelnPolyCoef       = 0

      do iTrace = 1, nTrace
        if ( traceGasS(iTrace)%fitColumnTrace .OR.  &
             traceGasS(itrace)%nameTraceGas == 'O2-O2') then
          counter = counter + 1
          retrS%codeFitParameters(counter) = 'slantColumn'
          retrS%codeTraceGas     (counter) = iTrace  
          retrS%codeSpecBand     (counter) = 1        ! the column should not change with spectral band
        end if
      end do

      do iband = 1, numSpectrBands
        do index = 0,  weakAbsRetrS(iband)%degreePoly
          counter = counter + 1
          retrS%codelnPolyCoef   (counter) = index
          retrS%codeFitParameters(counter) = 'lnpolyCoef'
          retrS%codeSpecBand     (counter) = iband
        end do
      end do

      if ( gasPTS%fitTemperatureOffset ) then
        counter = counter + 1
        retrS%codeFitParameters(counter) = 'offsetTemp'     
      end if

      do iband = 1, numSpectrBands
        if ( RRS_RingS(iband)%fitDiffRingSpec ) then
          counter = counter + 1
          retrS%codeFitParameters   (counter) = 'diffRingCoef'
          retrS%codeSpecBand        (counter) = iband
        end if
        if ( RRS_RingS(iband)%fitRingSpec ) then
          counter = counter + 1
          retrS%codeFitParameters   (counter) = 'RingCoef'
          retrS%codeSpecBand        (counter) = iband
        end if
      end do ! iband

      retrS%nstate = counter

      if ( verbose ) then
        write(intermediateFileUnit,'(A)') 'codeFitParameters  #TraceGas  #lnpolyCoef   #spectralBand'
        do istate = 1, retrS%nstate
          write(intermediateFileUnit,'(A15,3I10)') retrS%codeFitParameters(istate),       &
                 retrS%codeTraceGas(istate), retrS%codelnPolyCoef(istate), retrS%codeSpecBand(istate)
        end do
      end if

    end subroutine fillCodesFitParamClDOAS


    subroutine updateFitParametersClassicDOAS(errS, numSpectrBands, nTrace, traceGasS, retrS, weakAbsRetrS, &
                                       RRS_RingS, gasPTS, surfaceS, cloudAerosolRTMgridS, LambCloudS)

      ! Write the newly calculated values of the state vector into the structures weakAbsRetrS and gasPTS

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands               ! number of spectral bands
      integer,                       intent(in)    :: nTrace                       ! number of trace gases
      type(traceGasType),            intent(inout) :: traceGasS(nTrace)            ! properties of the trace gases
      type(retrType),                intent(inout) :: retrS                        ! retrieval parameters
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands) ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)    ! Ring spectra
      type(gasPTType),               intent(inout) :: gasPTS                       ! pressure and temperature
      type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)     ! surface properties
      type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS         ! cloud-aerosol properties
      type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)   ! wavelength dependent cloud properties

      ! local
      logical :: altitude_update_required
      integer :: istate, iband, index, iTrace

      real(8), parameter :: DUToCm2 = 2.68668d16

      ! initialize
      altitude_update_required = .false.

      ! loop over al the state vector elements and update the relevant values
      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case( 'slantColumn' )

            iband  = retrS%codeSpecBand(istate)
            iTrace = retrS%codeTraceGas(istate)
            weakAbsRetrS(iband)%slantColumn(iTrace) = retrS%x(istate) * DUToCm2

          case('offsetTemp')

            gasPTS%temperatureOffset = retrS%x(istate)
            if( gasPTS%temperatureOffset >  50.0d0 ) gasPTS%temperatureOffset =  50.0d0
            if( gasPTS%temperatureOffset < -50.0d0 ) gasPTS%temperatureOffset = -50.0d0
            ! change the a-priori temperatures with the offset
            gasPTS%temperature(:) = gasPTS%temperatureAP(:) + gasPTS%temperatureOffset
            altitude_update_required = .true.

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)
            RRS_RingS(iband)%ringCoeff  = retrS%x(istate)

          case('RingCoef')

            iband = retrS%codeSpecBand(istate)
            RRS_RingS(iband)%ringCoeff  = retrS%x(istate)

          case('lnpolyCoef')

            iband = retrS%codeSpecBand(istate)
            index = retrS%codelnPolyCoef(istate)
            weakAbsRetrS(iband)%lnpolyCoef(index) = retrS%x(istate)

          case default
            call logDebug('in subroutine updateFitParametersDOAS in doasModule')
            call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
            if (errorCheck(errS)) return
        end select

      end do ! istate

      if ( altitude_update_required )  then
        ! update the altitudes and the temperature in the traceGasS structure
        call update_altitudes_PTz_grid(errS, surfaceS(1), gasPTS)
        if (errorCheck(errS)) return
        call update_T_z_other_grids(errS, numSpectrBands, nTrace, surfaceS, gasPTS, traceGasS, &
                                    cloudAerosolRTMgridS, LambCloudS)
        if (errorCheck(errS)) return
        call update_ndens_vmr(errS, nTrace, gasPTS, traceGasS )
        if (errorCheck(errS)) return
      end if

    end subroutine updateFitParametersClassicDOAS


    subroutine updateCovFitParamClDOAS(errS, numSpectrBands, retrS, diagnosticS, weakAbsRetrS, RRS_RingS, gasPTS)

      ! The a-posteriori errors are calculated after the solution has converged, or at least the
      ! iteration loop has ended. When this diagnostic information is available, one can update
      ! the errors in traceGasS, surfaceS, and cloudAerosolRTMgridS.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands               ! number of spectral bands
      type(retrType),                intent(in)    :: retrS                        ! retrieval parameters
      type(diagnosticType),          intent(in)    :: diagnosticS                  ! diagnostic parameters
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands) ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)    ! Ring spectra
      type(gasPTType),               intent(inout) :: gasPTS                       ! pressure and temperature

      ! local
      integer :: istate, jstate, iindex, jindex, iband, jband, iTrace, jTrace

      real(8), parameter :: DUToCm2 = 2.68668d16

      ! loop over al the state vector elements and update the relevant values

      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case('slantColumn')

            iband  = retrS%codeSpecBand(istate)
            iTrace = retrS%codeTraceGas(istate)
            do jstate= 1, retrS%nstate
              select case (retrS%codeFitParameters(jstate))
                case('slantColumn')
                  jband  = retrS%codeSpecBand(jstate)
                  jTrace = retrS%codeTraceGas(jstate)
                  if ( (jTrace == iTrace) .and. (iband == jband) ) then
                     weakAbsRetrS(iband)%covSlantColumn(iTrace) = diagnosticS%S_lnvmr(istate,jstate) * DUToCm2**2
                  end if
              end select
            end do 

          case('offsetTemp')

            gasPTS%varianceTempOffset = diagnosticS%S_lnvmr(istate,istate)

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)
            RRS_RingS(iband)%varRingCoeff  = diagnosticS%S_lnvmr(istate,istate)

          case('RingCoef')

            iband = retrS%codeSpecBand(istate)
            RRS_RingS(iband)%varRingCoeff  = diagnosticS%S_lnvmr(istate,istate)

          case('lnpolyCoef')

            iband  = retrS%codeSpecBand(istate)
            iindex = retrS%codelnPolyCoef(istate)
            do jstate= 1, retrS%nstate
              jband  = retrS%codeSpecBand(jstate)
              jindex = retrS%codelnPolyCoef(jstate)
              if ( (iband == jband) .and. (iindex == jindex ) ) then
                select case (retrS%codeFitParameters(jstate))
                  case('lnpolyCoef')
                    weakAbsRetrS(iband)%covlnPolyCoef(iindex) = diagnosticS%S_lnvmr(istate,jstate)
                end select
              end if
            end do 

          case default

            call logDebug('in subroutine updateCovFitParametersDOAS in doasModule')
            call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
            if (errorCheck(errS)) return

        end select

      end do ! istate

    end subroutine updateCovFitParamClDOAS


    subroutine fillAPrioriClassicDOAS(errS, numSpectrBands, weakAbsRetrS, RRS_RingS, &
                                      gasPTSimS, gasPTRetrS, retrS)

      ! fill the a-priori state vector and the a-priori covariance matrix
      ! fitting of the trace gas is done for the logarithm of the volume mixing ratio,
      ! but a-priori covariance matrices for the volume mixing ratio and the number density
      ! are also given.
                          
      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands                  ! number of spectral bands
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands)    ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)       ! Ring spectra and Ring parameters
      type(gasPTType),               intent(in)    :: gasPTSimS                       ! pressure and temperature
      type(gasPTType),               intent(inout) :: gasPTRetrS                      ! pressure and temperature
      type(retrType),                intent(inout) :: retrS                           ! retrieval parameters

      ! local
      integer :: istate, jstate
      integer :: iband, iTrace, iPoly

      real(8), parameter :: DUToCm2 = 2.68668d16

      ! control output
      logical, parameter :: verbose = .false.

      retrS%Sa_lnvmr = 0.0d0
      retrS%Sa_vmr   = 0.0d0
      retrS%Sa_ndens = 0.0d0

      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case('slantColumn')

            iTrace = retrS%codeTraceGas(iState)
            iband  = retrS%codeSpecBand(istate)
            retrS%xa(istate)     = weakAbsRetrS(iband)%slantColumnAP(iTrace) / DUToCm2
            retrS%x(istate)      = weakAbsRetrS(iband)%slantColumn  (iTrace) / DUToCm2
            retrS%x_true(istate) = 0.0d0 ! true slant column is not known

            retrS%Sa_lnvmr(istate, istate) = weakAbsRetrS(iband)%covSlantColumnAP(iTrace) / DUToCm2 / DUToCm2 
            retrS%Sa_vmr  (istate, istate) = weakAbsRetrS(iband)%covSlantColumnAP(iTrace) / DUToCm2 / DUToCm2
            retrS%Sa_ndens(istate, istate) = weakAbsRetrS(iband)%covSlantColumnAP(iTrace) / DUToCm2 / DUToCm2

          case('offsetTemp')

            retrS%xa(istate)               = gasPTRetrS%temperatureOffsetAP
            retrS%x_true(istate)           = gasPTSimS%temperatureOffset
            retrS%Sa_lnvmr(istate, istate) = gasPTRetrS%varianceTempOffsetAP
            retrS%Sa_vmr  (istate, istate) = gasPTRetrS%varianceTempOffsetAP
            retrS%Sa_ndens(istate, istate) = gasPTRetrS%varianceTempOffsetAP

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)
            retrS%xa(istate)               = RRS_RingS(iband)%ringCoeffAP
            retrS%x_true(istate)           = 0.0d0                           ! in simulation no Ring spectrum
            retrS%Sa_lnvmr(istate, istate) = RRS_RingS(iband)%varRingCoeffAP
            retrS%Sa_vmr  (istate, istate) = RRS_RingS(iband)%varRingCoeffAP
            retrS%Sa_ndens(istate, istate) = RRS_RingS(iband)%varRingCoeffAP

          case('RingCoef')

            retrS%xa(istate)               = RRS_RingS(iband)%ringCoeffAP
            retrS%x_true(istate)           = 0.0d0                           ! in simulation no Ring spectrum
            retrS%Sa_lnvmr(istate, istate) = RRS_RingS(iband)%varRingCoeffAP
            retrS%Sa_vmr  (istate, istate) = RRS_RingS(iband)%varRingCoeffAP
            retrS%Sa_ndens(istate, istate) = RRS_RingS(iband)%varRingCoeffAP

          case('lnpolyCoef')

            iband = retrS%codeSpecBand(istate)
            iPoly = retrS%codelnPolyCoef(istate)
            retrS%xa(istate) = weakAbsRetrS(iband)%lnPolyCoefAP(iPoly)
            retrS%x(istate)  = weakAbsRetrS(iband)%lnPolyCoefAP(iPoly)
            retrS%x_true(istate) = 0.0d0   ! we do not know the true coefficients of the polynomial calculate them?
            retrS%Sa_lnvmr(istate, istate) = weakAbsRetrS(iband)%covlnPolyCoefAP(iPoly)
            retrS%Sa_vmr  (istate, istate) = weakAbsRetrS(iband)%covlnPolyCoefAP(iPoly)
            retrS%Sa_ndens(istate, istate) = weakAbsRetrS(iband)%covlnPolyCoefAP(iPoly)

          case default

            call logDebug('in subroutine fillAPrioriDOAS in doasModule')
            call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
            if (errorCheck(errS)) return

        end select

      end do ! istate

      ! write to intermediate file if verbose is .true.
      if ( verbose ) then
        write(intermediateFileUnit,*) ' code   xa  and  Sa_lnvmr from fillAPriori'
        do istate = 1, retrS%nstate
          write(intermediateFileUnit,'(A15,101F12.6)') retrS%codeFitParameters(istate), retrS%xa(istate), &
                                      (retrS%Sa_lnvmr(istate, jstate), jstate = 1, retrS%nstate)
        end do
      end if

    end subroutine fillAPrioriClassicDOAS


    subroutine calculateDiagnosticsClassicDOAS(errS, retrS, diagnosticS)

      ! calculate the error covariance matrix, averaging kernel, gain, and chi2

      implicit none

      type(errorType), intent(inout) :: errS
      type(retrType),          intent(in)    :: retrS
      type(diagnosticType),    intent(inout) :: diagnosticS

      ! local
      real(8) :: Kwhite(retrS%nwavelRetr,retrS%nState)
      real(8) :: sqrtSa(retrS%nState,retrS%nState), sqrtInvSa(retrS%nState,retrS%nState) 
      real(8) :: SaInv(retrS%nState,retrS%nState)
      real(8) :: Awhite(retrS%nState,retrS%nState), Atrans(retrS%nState,retrS%nState)
      real(8) :: Swhite(retrS%nState,retrS%nState), Strans(retrS%nState,retrS%nState)

      ! matrices for svd
      real(8) :: w(retrS%nState)
      real(8) :: ua(retrS%nState,retrS%nState), uaT(retrS%nState,retrS%nState)
      real(8) :: u(retrS%nwavelRetr,retrS%nState), uT(retrS%nState,retrS%nwavelRetr)
      real(8) :: v(  retrS%nState,retrS%nState), vT(retrS%nState,  retrS%nState)

      real(8) :: diagSaInv(retrS%nState, retrS%nState)
      real(8) :: diagsqrtSa(retrS%nState, retrS%nState)
      real(8) :: diagsqrtInvSa(retrS%nState, retrS%nState)

      ! various
      integer :: istate

      ! calculate sqrtSe (assuming a diagonal matrix)
      ! and their inverse values

      diagSaInv     = 0.0d0
      diagsqrtSa    = 0.0d0
      diagsqrtInvSa = 0.0d0

      ! lnvmr

      ua = retrS%Sa_lnvmr
      ! svdcmp overwrites first array (input) with u
      call svdcmp(errS, ua, w, v, retrS%nState)
      if (errorCheck(errS)) return
      uaT = transpose(ua)

      do istate = 1, retrS%nState
          diagSaInv(istate,istate)     = 1.0d0 / w(istate)
          diagsqrtSa(istate,istate)    = sqrt(w(istate))
          diagsqrtInvSa(istate,istate) = 1.0d0 / diagsqrtSa(istate,istate)
      end do

      sqrtInvSa = matmul(v, matmul(diagsqrtInvSa, uaT) )
      sqrtSa    = matmul(v, matmul(diagsqrtSa   , uaT) )
      SaInv     = matmul(v, matmul(diagSaInv    , uaT) )

      ! calculate dxwhite, dRwhite, and Kwhite
      Kwhite   = matmul(retrS%sqrtInvSe,matmul(retrS%K_lnvmr,sqrtSa))

      ! find the singular value decomposition for Kwhite = U W VT

      u = Kwhite
      ! svdcmp overwrites first array (input) with u
      call svdcmp(errS, u, w, v, retrS%nState)
      if (errorCheck(errS)) return
      vT = transpose(v)
      uT = transpose(u)
           
      Strans    = 0.0d0
      Atrans    = 0.0d0
      do istate = 1, retrS%nState
          Strans(istate,istate)    = 1.0d0 / (1.0d0 + w(istate)**2)
          Atrans(istate,istate)    = w(istate)**2 * Strans(istate,istate)
      end do

      Swhite    = matmul(v,matmul(Strans,vT))
      Awhite    = matmul(v,matmul(Atrans,vT))

      diagnosticS%S_lnvmr      = matmul(sqrtSa,matmul(Swhite,sqrtSa))
      diagnosticS%A_lnvmr      = matmul(sqrtSa,matmul(Awhite,sqrtInvSa))
      diagnosticS%G_lnvmr      = matmul(matmul(diagnosticS%S_lnvmr,transpose(retrS%K_lnvmr)), retrS%InvSe)
      diagnosticS%Snoise_lnvmr = matmul(matmul(diagnosticS%G_lnvmr,retrS%Se),transpose(diagnosticS%G_lnvmr))

      diagnosticS%infoContent_lnvmr = 0.5d0 * sum( log(1.0d0 + w(:)**2) )

      diagnosticS%DFS = 0.0d0
      do istate = 1, retrS%nstate
        diagnosticS%DFS = diagnosticS%DFS + diagnosticS%A_lnvmr(istate,istate)
      end do

      ! calculate smoothed state vector, retrS%x_smoothed

      diagnosticS%x_smoothed = retrS%xa + matmul(diagnosticS%A_lnvmr, retrS%x_true - retrS%xa)

    end subroutine calculateDiagnosticsClassicDOAS


    subroutine initializeClassicDOAS(errS, numSpectrBands, nTrace, geometryS, traceGasS, optPropRTMGridS, XsecHRS,  &
                                     weakAbsRetrS, wavelInstrS, wavelRTMS, retrS, XsecRTMS, XsecHRRTMaltS)


      ! Purpose of this subroutime is to claim memory for the arrays in the structures
      ! wavelRTMS, XsecRTMS, and XsecHRRTMaltS.
      ! These are initial value and the size of the arrays may be updated later after the number of wavelengths
      ! corresponding to zero value in the total differential absorption is known. Then the arrays in
      ! wavelRTMS, and XsecRTMS may get updated dimensions.
      ! Futher, the air mass factors in weakAbsRetrS are initialized with geometrical air mass factors
      
      implicit none

      type(errorType), intent(inout) :: errS
      integer,                  intent(in)    :: numSpectrBands                           ! number of spectral bands
      integer,                  intent(in)    :: nTrace                                   ! number of trace gases
      type(geometryType),       intent(in)    :: geometryS                                ! info geometry
      type(traceGasType),       intent(in)    :: traceGasS(nTrace)                        ! properties of the trace gases
      type(optPropRTMGridType), intent(inout) :: optPropRTMGridS                          ! optical properties on RTM grid
      type(XsecType),           intent(in)    :: XsecHRS(numSpectrBands, nTrace)          ! absorption cross section
      type(weakAbsType),        intent(inout) :: weakAbsRetrS(numSpectrBands)             ! DOAS results
      type(wavelInstrType),     intent(in)    :: wavelInstrS(numSpectrBands)              ! wavelength grid for retrieval
      type(wavelHRType),        intent(inout) :: wavelRTMS(numSpectrBands)                ! wavelengths RTM calculations
      type(retrType),           intent(in)    :: retrS                                    ! retrieval structure
      type(XsecType),           intent(inout) :: XsecRTMS(numSpectrBands, nTrace)
      type(XsecType),           intent(inout) :: XsecHRRTMaltS(numSpectrBands,nTrace)

      ! local
      integer :: iband, iTrace, iwave, ialt, indexWavelRefl
      real(8) :: geometricalAMF, sumRefl

      geometricalAMF = 1.0d0 / geometryS%uu + 1.0d0 / geometryS%u0

      do iband = 1, numSpectrBands
        wavelRTMS(iband)%nGaussMax = 0
        wavelRTMS(iband)%nGaussMin = 0
        wavelRTMS(iband)%nwavel    = 1
      end do ! iband

      ! fill values for cross sections XsecRTMS
      do iTrace = 1, nTrace
        do iband = 1, numSpectrBands
          XsecRTMS(iband, iTrace)%XsectionFileName = XsecHRS(iband, iTrace)%XsectionFileName
          XsecRTMS(iband, iTrace)%useHITRAN        = XsecHRS(iband, iTrace)%useHITRAN
          XsecRTMS(iband, iTrace)%factorLM         = XsecHRS(iband, iTrace)%factorLM
          XsecRTMS(iband, iTrace)%gasIndex         = XsecHRS(iband, iTrace)%gasIndex
          XsecRTMS(iband, iTrace)%ISO              = XsecHRS(iband, iTrace)%ISO
          XsecRTMS(iband, iTrace)%thresholdLine    = XsecHRS(iband, iTrace)%thresholdLine
          XsecRTMS(iband, iTrace)%nwavelHR         = 1
          XsecRTMS(iband, iTrace)%nwavel           = 1
          XsecRTMS(iband, iTrace)%nalt             = traceGasS(iTrace)%nalt
        end do ! iband
      end do ! iTrace

      ! fill values for cross sections XsecHRRTMaltS
      do iTrace = 1, nTrace
        do iband = 1, numSpectrBands
          XsecHRRTMaltS(iband, iTrace)%XsectionFileName = XsecHRS(iband, iTrace)%XsectionFileName
          XsecHRRTMaltS(iband, iTrace)%useHITRAN        = XsecHRS(iband, iTrace)%useHITRAN
          XsecHRRTMaltS(iband, iTrace)%factorLM         = XsecHRS(iband, iTrace)%factorLM
          XsecHRRTMaltS(iband, iTrace)%gasIndex         = XsecHRS(iband, iTrace)%gasIndex
          XsecHRRTMaltS(iband, iTrace)%ISO              = XsecHRS(iband, iTrace)%ISO
          XsecHRRTMaltS(iband, iTrace)%thresholdLine    = XsecHRS(iband, iTrace)%thresholdLine
          XsecHRRTMaltS(iband, iTrace)%nwavelHR         = XsecHRS(iband, iTrace)%nwavelHR
          XsecHRRTMaltS(iband, iTrace)%nwavel           = XsecHRS(iband, iTrace)%nwavel
          XsecHRRTMaltS(iband, iTrace)%nalt             = optPropRTMGridS%RTMnlayer
        end do ! iband
      end do ! iTrace

      ! allocate arrays in structures
      do iband = 1, numSpectrBands
        call claimMemWeakAbsS(errS,  weakAbsRetrS(iband) )
        if (errorCheck(errS)) return
        call claimMemWavelHRS(errS,  wavelRTMS(iband) )
        if (errorCheck(errS)) return
        do iTrace = 1, nTrace
          call claimMemXsecS(errS,  XsecRTMS(iband, iTrace) )
          if (errorCheck(errS)) return
          call claimMemXsecS(errS,  XsecHRRTMaltS(iband, iTrace) )
          if (errorCheck(errS)) return
        end do ! iTrace
      end do ! iband 

      ! fill values for weakAbsRetrS
      do iband = 1, numSpectrBands
        do iTrace = 1, nTrace

          weakAbsRetrS(iband)%slantColumnAP    (iTrace) = geometricalAMF * traceGasS(iTrace)%columnAP
          weakAbsRetrS(iband)%slantColumn      (iTrace) = geometricalAMF * traceGasS(iTrace)%column
          weakAbsRetrS(iband)%covSlantColumnAP (iTrace) = geometricalAMF * traceGasS(iTrace)%covColumnAP
          weakAbsRetrS(iband)%covSlantColumn   (iTrace) = geometricalAMF * traceGasS(iTrace)%covColumn
          weakAbsRetrS(iband)%columnAP         (iTrace) = traceGasS(iTrace)%columnAP
          weakAbsRetrS(iband)%column           (iTrace) = traceGasS(iTrace)%column
          weakAbsRetrS(iband)%covColumnAP      (iTrace) = traceGasS(iTrace)%covColumnAP
          weakAbsRetrS(iband)%covColumn        (iTrace) = traceGasS(iTrace)%covColumn
          weakAbsRetrS(iband)%amfWindow        (iTrace) = geometricalAMF
        
          weakAbsRetrS(iband)%amfInstrGrid(:,iTrace)  = geometricalAMF
          weakAbsRetrS(iband)%amfHRGrid(:,iTrace)     = geometricalAMF
        end do ! iTrace

        weakAbsRetrS(iband)%covlnPolyCoefAP(:) = 100.0d0
        weakAbsRetrS(iband)%covlnPolyCoef(:)   = 100.0d0

        do iwave = 1, weakAbsRetrS(iband)%nwavelRTM
          do ialt = 0, weakAbsRetrS(iband)%RTMnlayer
            weakAbsRetrS(iband)%amfAltRTM(ialt,iwave)         = geometricalAMF
          end do
        end do

        do iwave = 1, weakAbsRetrS(iband)%nwavelInstr
          do ialt = 0, weakAbsRetrS(iband)%RTMnlayer
            weakAbsRetrS(iband)%amfAltInstr(ialt, iwave)      = geometricalAMF
          end do
        end do

        do iwave = 1, weakAbsRetrS(iband)%nwavelHR
          do ialt = 0, weakAbsRetrS(iband)%RTMnlayer
            weakAbsRetrS(iband)%amfAltHR(ialt, iwave)         = geometricalAMF
          end do
        end do

      end do ! iband

      ! initialize the first term of the polynomial (constant) with the logarithm of the average
      ! value of the reflectance for the spectral band involved
      ! note that in retrS%reflMeas the the spectral bands are not distinguished
      
      indexWavelRefl = 1
      do iband = 1, numSpectrBands
        weakAbsRetrS(iband)%lnPolyCoefAP(:) = 0.0d0
        weakAbsRetrS(iband)%lnPolyCoef(:)   = 0.0d0
        sumRefl = 0.0d0
        do iwave = 1, wavelInstrS(iband)%nwavel
          sumRefl = sumRefl + retrS%reflMeas(indexWavelRefl)
          indexWavelRefl = indexWavelRefl + 1
        end do !iwave
        weakAbsRetrS(iband)%lnPolyCoefAP(0) = log( sumRefl / (wavelInstrS(iband)%nwavel) ) 
        weakAbsRetrS(iband)%lnPolyCoef  (0) = weakAbsRetrS(iband)%lnPolyCoefAP(0)
      end do ! iband

    end subroutine initializeClassicDOAS


    subroutine setXsecRTMclassicDOAS(errS, nTrace, wavelHRS, XsecHRS, wavelRTMS, XsecRTMS)

     ! Calculate the absorption cross section at the wavelength where the AMF is to be calculated.
     ! Use interpolation on  weakAbsRetrS%XsecEffInstr, i.e. the effective absorption cross section 
     ! to get the value at the proper wavelength. Because we work with an altitude averaged
     ! absorption cross section the altitude dependence of the absorption cross section vanishes.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,               intent(in)    :: nTrace                         ! number of trace gases
      type(wavelHRType),     intent(in)    :: wavelHRS                       ! high resolution wavelength grid
      type(XsecType),        intent(in)    :: XsecHRS(nTrace)                ! aborption cross section on HR grid
      type(wavelHRType),     intent(in)    :: wavelRTMS                      ! RTM wavelength grid
      type(XsecType),        intent(inout) :: XsecRTMS(nTrace)               ! absorption cross sections

      ! local
      integer :: iTrace, iwave
      integer :: ialt, status
      real(8) :: SDabs(wavelHRS%nwavel)

      do iTrace = 1, nTrace
        do ialt = 0, XsecHRS(iTrace)%nalt
          call spline(errS, wavelHRS%wavel, XsecHRS(iTrace)%Xsec(:,ialt), SDabs, status)
          if (errorCheck(errS)) return
          do iwave = 1, wavelRTMS%nwavel
            XsecRTMS(iTrace)%Xsec(iwave,ialt) = splint(errS, wavelHRS%wavel,  XsecHRS(iTrace)%Xsec(:,ialt), &
                                                  SDabs, wavelRTMS%wavel(iwave), status)
          end do ! iwave
        end do ! ialt
      end do ! iTrace

    end subroutine setXsecRTMclassicDOAS


    subroutine calcReflAndDerivClassicDOAS (errS, numSpectrBands, nTrace, controlRetrS, traceGasRetrS, &
                                            wavelInstrS, weakAbsRetrS, RRS_RingS, retrS)

      ! calculate reflectance and derivatives on the instrument wavelength grid

      implicit none

      type(errorType),       intent(inout) :: errS
      integer,               intent(in)    :: numSpectrBands, nTrace        ! number wavelength bands and trace gases
      type(controlType),     intent(inout) :: controlRetrS                  ! control parameters for retrieval
      type(traceGasType),    intent(in)    :: traceGasRetrS(nTrace)         ! properties of the trace gases
      type(wavelInstrType),  intent(in)    :: wavelInstrS(numSpectrBands)   ! wavelength grid for retrieval
      type(weakAbsType),     intent(inout) :: weakAbsRetrS(numSpectrBands)  ! DOAS results
      type(RRS_RingType),    intent(inout) :: RRS_RingS(numSpectrBands)     ! Ring spectra
      type(retrType),        intent(inout) :: retrS                         ! retrieval structure

      ! local
      integer :: iband, iTrace
      integer :: ipoly, iwave, istate
      integer :: indexWavelRefl, indexWavelDeriv(retrS%nstate)
      real(8) :: scaling
      real(8) :: wStart, wEnd

      ! polynomial values on instrument grid
      real(8), allocatable :: polynomial(:)           ! (wavelInstrS(iband)%nwavel)
      real(8), allocatable :: lnpolynomial(:)         ! (wavelInstrS(iband)%nwavel)

      ! variables for fitting the polynomial to the reflectance (sun-normalized radiance)
      real(8), allocatable  :: wavelScaledInstr(:)    ! (wavelInstrS(iband)%nwavel)

      real(8), parameter :: DUToCm2 = 2.68668d16

      indexWavelRefl      = 1
      indexWavelDeriv(:)  = 1
      retrS%K_lnvmr(:,:)  = 0.0d0

      do iband = 1, numSpectrBands

        allocate( lnpolynomial     (wavelInstrS(iband)%nwavel) )
        allocate( polynomial       (wavelInstrS(iband)%nwavel) )
        allocate( wavelScaledInstr (wavelInstrS(iband)%nwavel) )

        ! calculate scaled wavelengths on instrument grid
        wStart = wavelInstrS(iband)%wavel(1)
        wEnd   = wavelInstrS(iband)%wavel( wavelInstrS(iband)%nwavel )

        wavelScaledInstr(:) = 2.0d0 * ( wavelInstrS(iband)%wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0

        ! One might be tempted to calculate the smooth reflectance from the values calculated by the
        ! call to calcReflAndAMFRTM. However, this interferes with optimal estimation. The smooth reflectance 
        ! must be calculated from the current polynomial coefficients.

        ! calculate polynomial
        lnpolynomial(:) =  weakAbsRetrS(iband)%lnpolyCoef(0)
        do iPoly = 1, weakAbsRetrS(iband)%degreePoly
          lnpolynomial(:) = lnpolynomial(:) +  &
                            weakAbsRetrS(iband)%lnpolyCoef(iPoly) * wavelScaledInstr(:) ** iPoly
        end do ! iPoly

        polynomial(:) = exp( lnpolynomial(:) )
        weakAbsRetrS(iband)%polynomial(:) = polynomial(:)

        ! calculate the reflectance using the DOAS expression
        ! note that the amf is the geometrical amf and that
        ! weakAbsRetrS(iband)%amfInstrGrid (iwave,i) * weakAbsRetrS(iband)%column(i)
        ! is the slant column for trace gas i for each value of iwave
        do iwave = 1, wavelInstrS(iband)%nwavel
          retrS%refl(indexWavelRefl) = polynomial(iwave) * &
             exp( - sum( weakAbsRetrS(iband)%slantColumn(:) * weakAbsRetrS(iband)%XsecEffInstr(iwave,:) ) )
          if( controlRetrS%method == 4 ) then ! for DOMINO exclude contributions by strat_NO2 and trop_NO2
            retrS%refl(indexWavelRefl) = polynomial(iwave)
            do iTrace = 1, nTrace
              if ( trim(traceGasRetrS(iTrace)%nameTraceGas) =='strat_NO2' .or. &
                   trim(traceGasRetrS(iTrace)%nameTraceGas) =='trop_NO2' ) then
                ! do nothing
              else
                retrS%refl(indexWavelRefl) = retrS%refl(indexWavelRefl) * &
                   exp(-weakAbsRetrS(iband)%slantColumn(iTrace) * weakAbsRetrS(iband)%XsecEffInstr(iwave,iTrace))
              end if
            end do ! iTrace
          end if
          ! add Ring spectrum
          if ( RRS_RingS(iband)%fitRingSpec ) then
            retrS%refl(indexWavelRefl) = retrS%refl(indexWavelRefl)  &
                                       + RRS_RingS(iband)%ringCoeff * RRS_RingS(iband)%Ring(iwave)
          end if
          if ( RRS_RingS(iband)%fitDiffRingSpec ) then
            ! this form for adding a Ring spectrum is the baseline for TROPOMI ATBD
            ! note that the derivative has to be changed also
            retrS%refl(indexWavelRefl) = retrS%refl(indexWavelRefl)  * &
                                   (1.0d0 + RRS_RingS(iband)%ringCoeff * RRS_RingS(iband)%diffRing(iwave))
            !retrS%refl(indexWavelRefl) = retrS%refl(indexWavelRefl)   &
            !                           + RRS_RingS(iband)%ringCoeff * RRS_RingS(iband)%diffRing(iwave)
          end if

          indexWavelRefl = indexWavelRefl + 1

        end do ! iwave
        
        ! calclate the derivatives of the reflectance on the instrument grid w.r.t. the state vector elements 
        ! store the results in retrS%K_lnvmr(nwavelRetr,nState)

        do istate = 1, retrS%nstate

          select case (retrS%codeFitParameters(istate))

            case( 'slantColumn' )

              iTrace = retrS%codeTraceGas(istate)

              do iwave = 1, wavelInstrS(iband)%nwavel
                retrS%K_lnvmr(indexWavelDeriv(istate),istate) = - retrS%refl(indexWavelDeriv(istate)) * DUToCm2 &
                                                              * weakAbsRetrS(iband)%XsecEffInstr(iwave,iTrace)
                indexWavelDeriv(istate) = indexWavelDeriv(istate) + 1
              end do ! iwave

            case('RingCoef')

              if ( iband == retrS%codeSpecBand(istate) ) then
                do iwave = 1, wavelInstrS(iband)%nwavel
                  retrS%K_lnvmr(indexWavelDeriv(istate),istate) = RRS_RingS(iband)%Ring(iwave)
                  indexWavelDeriv(istate) = indexWavelDeriv(istate) + 1
                end do ! iwave
              end if

            case('diffRingCoef')

              if ( iband == retrS%codeSpecBand(istate) ) then
                ! check that the derivative w.r.t. the Ring coefficient correponds
                ! to the expression for the reflectance given above
                do iwave = 1, wavelInstrS(iband)%nwavel
                  ! retrS%K_lnvmr(indexWavelDeriv(istate),istate) = RRS_RingS(iband)%diffRing(iwave)
                  retrS%K_lnvmr(indexWavelDeriv(istate),istate) = &
                              retrS%refl(indexWavelRefl) * RRS_RingS(iband)%diffRing(iwave)
                  indexWavelDeriv(istate) = indexWavelDeriv(istate) + 1
                end do ! iwave
              end if

            case('offsetTemp')

              call logDebug('temperature fit for DOAS not yet implemented')
              call mystop(errS, 'stopped because temperature fit is not yet implemented')
              if (errorCheck(errS)) return

            case('lnpolyCoef')

              ! dR/dci = d[ exp(c0 + c1*ls + c2*ls**2 + ...) * exp(-M*N*sig) ] / dci
              !        =    exp(c0 + c1*ls + c2*ls**2 + ...) * exp(-M*N*sig) * ls**i
              !        =    R * ls**i
              ! where 'ls' is the scaled wavelength 

              ipoly = retrS%codelnPolyCoef(istate)
              if ( iband == retrS%codeSpecBand(istate) ) then

                do iwave = 1, wavelInstrS(iband)%nwavel
                  scaling = wavelScaledInstr(iwave)
                  if ( ipoly == 0 ) then
                    retrS%K_lnvmr(indexWavelDeriv(istate),istate) = &
                                retrS%refl(indexWavelDeriv(istate))
                  else
                    retrS%K_lnvmr(indexWavelDeriv(istate),istate) = scaling**ipoly &
                                                                  * retrS%refl(indexWavelDeriv(istate))
                  end if  
                  indexWavelDeriv(istate) = indexWavelDeriv(istate) + 1
                end do ! iwave

              end if ! iband == retrS%codeSpecBand(istate)

            case default
              call logDebug('in subroutine calcReflAndDerivClassical' )
              call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
              call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
              if (errorCheck(errS)) return
          end select

        end do ! istate

        deallocate( lnpolynomial )
        deallocate( polynomial )
        deallocate( wavelScaledInstr )

      end do ! iband

    end subroutine calcReflAndDerivClassicDOAS


    subroutine calculate_amf_classicDOAS(errS, nTrace, XsecHRS, optPropRTMGridS, wavelRTMS, &
                                         traceGasRetrS, weakAbsS)

      ! calculate the air mass factor at the specified wavelength

      implicit none

      type(errorType),          intent(inout) :: errS
      integer,                  intent(in)    :: nTrace                ! number of trace gases
      type(XsecType),           intent(in)    :: XsecHRS(nTrace)       ! absorption cross section    
      type(optPropRTMGridType), intent(in)    :: optPropRTMGridS       ! properties on RTM grid
      type(wavelHRType),        intent(in)    :: wavelRTMS             ! RTM wavelength grid
      type(traceGasType),       intent(inout) :: traceGasRetrS(nTrace) ! atmospheric trace gas properties
      type(weakAbsType),        intent(inout) :: weakAbsS              ! DOAS results

      ! local

      integer :: ialt, iwave, iTrace
      real(8) :: column, amfwindow, Teff, s1, s2, s3
      real(8) :: tempCorrection

      logical, parameter :: verbose = .true.

      weakAbsS%amfWindow(:)      = 0.0d0
      weakAbsS%Teff_minus_T0(:)  = 0.0d0
      do iTrace = 1, nTrace
        do iwave = 1, wavelRTMS%nwavel ! note that the amf is calculated for one wavelength
          column    = 0.0d0
          amfwindow = 0.0d0
          Teff      = 0.0d0
          do ialt = 0, weakAbsS%RTMnlayer
            ! ndensGas in molecules/cm3; altitudes (weight) in km; therefore the factor 1.0d5
            s1 = optPropRTMGridS%ndensGas(ialt,itrace) * optPropRTMGridS%RTMweight(ialt) * 1.0d5
            s2 = s1 * weakAbsS%amfAltRTM(ialt, iwave)
            s3 = s2 * optPropRTMGridS%RTMtemperature(ialt)
            column    = column + s1
            amfwindow = amfwindow + s2
            Teff      = Teff + s3
          end do ! ialt
          weakAbsS%column(iTrace)        = column
          weakAbsS%amfWindow(iTrace)     = amfwindow / column
          weakAbsS%Teff_minus_T0(iTrace) = Teff / column / weakAbsS%amfWindow(iTrace)
          ! subtract the reference temperature T0
          weakAbsS%Teff_minus_T0(iTrace) = weakAbsS%Teff_minus_T0(iTrace) - weakAbsS%referenceTempRetr
        end do ! iwave

        ! NO2 is special and needs a temperature correction if a reference temperature is used
        if ( trim(traceGasRetrS(iTrace)%nameTraceGas) =='NO2'      .or. &
             trim(traceGasRetrS(iTrace)%nameTraceGas) =='trop_NO2' .or. &
             trim(traceGasRetrS(iTrace)%nameTraceGas) =='strat_NO2') then
          if ( weakAbsS%useReferenceTempRetr ) then
            ! perform temperature correction when an isothermal atmosphere is used
            tempCorrection = XsecHRS(iTrace)%corrFactorAMF * weakAbsS%Teff_minus_T0(iTrace)
            weakAbsS%amfWindowCorrT(iTrace) = weakAbsS%amfWindow(iTrace) * (1.0d0 + tempCorrection )
          end if ! weakAbsRetrS(iband)%useReferenceTempRetr

          if ( verbose ) then
            write(intermediateFileUnit,*)
            write(intermediateFileUnit,'(A)') trim(traceGasRetrS(iTrace)%nameTraceGas)
            write(intermediateFileUnit,'(A, ES12.4)') 'column                   = ', weakAbsS%column(iTrace)
            write(intermediateFileUnit,'(A)') 'reference temperature (Tref) and temperature correction used'
            write(intermediateFileUnit,'(A)') 'note that correction factor c = dsigmaDiff/dT / sigmaDiff depends on Tref'
            write(intermediateFileUnit,'(A, F12.8)')  'reference temperature    = ', weakAbsS%referenceTempRetr
            write(intermediateFileUnit,'(A, F12.8)')  'effective temperature    = ', &
                      weakAbsS%Teff_minus_T0(iTrace) + weakAbsS%referenceTempRetr
            write(intermediateFileUnit,'(A, F12.8)') 'correction factor c      = ', XsecHRS(iTrace)%corrFactorAMF
            write(intermediateFileUnit,'(A, F12.8)') 'AMF not corrected        = ', weakAbsS%amfWindow(iTrace)
            write(intermediateFileUnit,'(A, F12.8)') 'AMF corrected for temp   = ', &
                      weakAbsS%amfWindowCorrT(iTrace)
            write(intermediateFileUnit,*)
          end if ! verbose
        else
          weakAbsS%amfWindowCorrT(iTrace) = weakAbsS%amfWindow(iTrace) ! no correction for this trace gas
        end if ! species is NO2
      end do !iTrace

      if ( verbose ) then
        write(intermediateFileUnit, *)
        write(intermediateFileUnit, *) 'output from calculate_amf_classicDOAS in classic_doasModule'
        do iTrace = 1, nTrace
          write(intermediateFileUnit, *) 
          do iwave = 1, wavelRTMS%nwavel ! note that the amf is calculated for one wavelength
            write(intermediateFileUnit, *) '    altitude    pressure   temperature     ndens        amf       weight'
            do ialt = 0, weakAbsS%RTMnlayer
              write(intermediateFileUnit, '(3F12.4, ES15.5, 2F12.8)') optPropRTMGridS%RTMaltitude(ialt),      &
                                                                      optPropRTMGridS%RTMpressure(ialt),      &
                                                                      optPropRTMGridS%RTMtemperature(ialt),   &
                                                                      optPropRTMGridS%ndensGas(ialt, iTrace), &
                                                                      weakAbsS%amfAltRTM(ialt, iwave),        &
                                                                      optPropRTMGridS%RTMweight(ialt)
            end do ! ialt
          end do ! iwave
        end do !iTrace
      end if 

    end subroutine calculate_amf_classicDOAS


    subroutine print_resultsClassicDOAS(errS, numSpectrBands, nTrace, geometryS, wavelInstrS, traceGasSimS, &
                                        traceGasRetrS, optPropRTMGridRetrS, weakAbsRetrS,                   &
                                        RRS_RingSimS,  RRS_RingRetrS, retrS, diagnosticS,                   &
                                        controlSimS, controlRetrS, wavelHRSimS, cloudAerosolRTMgridSimS,    & ! The last two lines are
                                        optPropRTMGridSimS, reflDerivHRSimS)                                   !  used printing the amf

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands                  ! number wavelength band
      integer,                       intent(in)    :: nTrace                          ! number of trace gases
      type(geometryType),            intent(in)    :: geometryS                                ! info geometry
      type(wavelInstrType),          intent(in)    :: wavelInstrS(numSpectrBands)     ! wavelength grid for retrieval
      type(traceGasType),            intent(inout) :: traceGasSimS(nTrace)            ! atmospheric trace gas properties
      type(traceGasType),            intent(inout) :: traceGasRetrS(nTrace)           ! atmospheric trace gas properties
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridSimS              ! optical properties on RTM grid simulation
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridRetrS             ! optical properties on RTM grid retrieval
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands)    ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingSimS(numSpectrBands)    ! Ring spectra for simulation
      type(RRS_RingType),            intent(inout) :: RRS_RingRetrS(numSpectrBands)   ! Ring spectra for retrieval
      type(retrType),                intent(inout) :: retrS                           ! retrieval structure
      type(diagnosticType),          intent(inout) :: diagnosticS                     ! diagnostic information
      type(controlType),             intent(in)    :: controlSimS                     ! control data for simulation
      type(controlType),             intent(in)    :: controlRetrS                    ! control data for retrieval
      type(wavelHRType),             intent(in)    :: wavelHRSimS(numSpectrBands)     ! high resolution wavelength grid simulation
      type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridSimS         ! cld/aer properties sim
      type(reflDerivType),           intent(in)    :: reflDerivHRSimS(numSpectrBands) ! contains amf for simulation


      ! local
      integer :: iwave, istate, jstate, iband, ipoly, iTrace, jTrace, ialt
    
      real(8) :: factor, strat_column, strat_slant_column, trop_slant_column, trop_AMF, strat_AMF
      real(8) :: retr_trop_column, trop_column, bias_trop_column
      real(8) :: precision_strat_slant_column, precision_trop_slant_column
  
      real(8), parameter :: DUToCm2 = 2.68668d16

      if ( controlSimS%writeAltResolvedAMF .and. (.not. controlSimS%useReflectanceFromFile) ) then
        do iband = 1, numSpectrBands
          write(addtionalOutputUnit,*)
          write(addtionalOutputUnit,*) 'AMF FOR SIMULATION'
          call writeAltResolvedAMF(errS, iband, wavelHRSimS(iband), cloudAerosolRTMgridSimS, &
                                   optPropRTMGridSimS, reflDerivHRSimS(iband))
          if (errorCheck(errS)) return
        end do
      end if

      weakAbsRetrS(:)%geometricalAMF = 1.0d0 / geometryS%uu + 1.0d0 / geometryS%u0

      write(outputFileUnit,*)
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'RESULTS OF OPTIMAL ESTIMATION - DOAS or DOMINO'
      write(outputFileUnit,*)
      write(outputFileUnit,'(A, 2F10.5)') 'DFS      = ', diagnosticS%DFS
      write(outputFileUnit,*)
      write(outputFileUnit,'(A, 2F10.5)') 'information content = ', diagnosticS%infoContent_lnvmr

      write(outputFileUnit,*)
      write(outputFileUnit,*) 'ACCURACY OF THE FITTED SUN-NORMALIZED RADIANCE (residue)'
      write(outputFileUnit,*)
  
      if ( retrS%isConverged ) then
        write(outputFileUnit,'(A,2F12.5,I4)') ' solution has converged: xConvThreshold xConv  nIter = ', &
                        retrS%xConvThreshold, retrS%xConv, retrS%numIterations 
      else
        write(outputFileUnit,'(A,2E15.5,I4)') ' solution has NOT converged: xConvThreshold xConv nIter = ', &
                        retrS%xConvThreshold, retrS%xConv, retrS%numIterations
      end if

      write(outputFileUnit,*)
      write(outputFileUnit,'(A)') 'Here reflectance is used to denote the sun-normalized radiance'
      write(outputFileUnit,'(A)') &
        '         wavel     actual S/N     measured refl      fitted refl     rel diff(%) rel diff initial(%)'
      do iwave = 1, retrS%nwavelRetr
        write(outputFileUnit,'(A, f12.4, F12.2, 2F18.8, 2F15.6)')  'data',              &
                                        retrS%wavelRetr(iwave),                         &
                                        retrS%refl(iwave)/retrS%reflNoiseError(iwave),  &
                                        retrS%reflMeas(iwave), retrS%refl(iwave),       &
                              100.0d0 * retrS%dR(iwave)/retrS%reflMeas(iwave),          &
                              100.0d0 * retrS%dR_initial(iwave)/retrS%reflMeas(iwave)
      end do

      do istate = 1, retrS%nstate
  
        select case (retrS%codeFitParameters(istate))
  
  
          case('slantColumn')
  
            iTrace = retrS%codeTraceGas(istate)
            iband  = retrS%codeSpecBand(istate)
            if ( traceGasRetrS(iTrace)%unitFlag ) then
              factor = 1.0 / DUToCm2
            else
              factor = 1.0d0
            end if

            write(outputFileUnit,*)
            write(outputFileUnit,*) traceGasRetrS(iTrace)%nameTraceGas
            write(outputFileUnit,'(A, I4)') 'result for spectral band number = ', iband
            write(outputFileUnit,*)
            write(outputFileUnit,'(A,2ES12.4)')                                                      &
               'a-priori and RETRIEVED SLANT COLUMN                                             = ', &
               factor * weakAbsRetrS(iband)%slantColumnAP(iTrace),                                   &
               factor * weakAbsRetrS(iband)%slantColumn  (iTrace)
            write(outputFileUnit,'(A,2ES12.4,F12.4)')                                                &
               'a-priori precision, a-posteriori precision, and rel precision (%) SLANT COLUMN  = ', &
               factor * sqrt(weakAbsRetrS(iband)%covSlantColumnAP(iTrace) ),                         &
               factor * sqrt(weakAbsRetrS(iband)%covSlantColumn  (iTrace) ),                         &
               100.0d0* sqrt(weakAbsRetrS(iband)%covSlantColumn(iTrace)) / weakAbsRetrS(iband)%slantColumn(iTrace)
                                                                 

            if ( weakAbsRetrS(iband)%calculateAMF ) then
              ! fill retrieved vertical column based on the calculated amf and the fitted slant column
              weakAbsRetrS(iband)%column(iTrace) = weakAbsRetrS(iband)%slantColumn(iTrace) &
                                                 / weakAbsRetrS(iband)%amfWindow(iTrace)
              weakAbsRetrS(iband)%covColumn(iTrace)  = weakAbsRetrS(iband)%covSlantColumn  (iTrace)   &
                                                     / weakAbsRetrS(iband)%amfWindow(iTrace)**2                  
              ! NO2 is special and needs a temperature correction if a reference temperature is used
              if ( trim(traceGasRetrS(iTrace)%nameTraceGas) =='NO2'      .or. &
                   trim(traceGasRetrS(iTrace)%nameTraceGas) =='trop_NO2' .or. &
                   trim(traceGasRetrS(iTrace)%nameTraceGas) =='strat_NO2') then
                if ( weakAbsRetrS(iband)%useReferenceTempRetr ) then
                  ! perform temperature correction when an isothermal atmosphere is used
                  weakAbsRetrS(iband)%column(iTrace) = weakAbsRetrS(iband)%slantColumn(iTrace) &
                                                     / weakAbsRetrS(iband)%amfWindowCorrT(iTrace)
                  weakAbsRetrS(iband)%covColumn(iTrace)  = weakAbsRetrS(iband)%covSlantColumn  (iTrace)   &
                                                         / weakAbsRetrS(iband)%amfWindowCorrT(iTrace)**2
                else
                  weakAbsRetrS(iband)%covColumn(iTrace)  = weakAbsRetrS(iband)%covSlantColumn  (iTrace)   &
                                                         / weakAbsRetrS(iband)%amfWindow(iTrace)**2                  
                end if ! weakAbsRetrS(iband)%useReferenceTempRetr
              end if ! species is NO2

              write(outputFileUnit,*)
              write(outputFileUnit,'(A,4ES10.2,F12.3)') 'a-priori true and RETRIEVED VERTICAL COLUMN and bias(abs and %) = ', &
                                                                        factor *  weakAbsRetrS(iband)%columnAP(iTrace), &
                                                                                 factor * traceGasSimS (iTrace)%column, &
                                                                          factor *  weakAbsRetrS(iband)%column(iTrace), &
                                         factor *  (weakAbsRetrS(iband)%column(iTrace) - traceGasSimS(iTrace)%column),  &
              100*( weakAbsRetrS(iband)%column(iTrace) - traceGasSimS(iTrace)%column ) / traceGasSimS(iTrace)%column
              write(outputFileUnit,'(A,2ES12.4,F12.3)')  'a-priori. a-posteriori, and rel. error (%) VERTICAL COLUMN = ', &
                                                                factor * sqrt( weakAbsRetrS(iband)%covColumnAP(iTrace) ), &
                                                                factor * sqrt( weakAbsRetrS(iband)%covColumn  (iTrace) ), &
                     ! for classic DOAS and DOMINO the relative error in the slant column is the relative error in
                     ! the vertical column
                     100.0d0* sqrt(weakAbsRetrS(iband)%covSlantColumn(iTrace)) / weakAbsRetrS(iband)%slantColumn(iTrace)
            end if ! weakAbsRetrS(iband)%calculateAMF

            ! results for DOMINO
            if( controlRetrS%method == 4 ) then
              do jTrace = 1, nTrace
                if ( trim(traceGasRetrS(jTrace)%nameTraceGas) =='strat_NO2') then
                    strat_column =  factor * traceGasRetrS(jTrace)%column
                    strat_AMF = weakAbsRetrS(iband)%amfWindowCorrT(jTrace)
                    strat_slant_column = strat_AMF * strat_column
                   ! strat_slant_column = geometricalAMF * strat_column
                    precision_strat_slant_column = factor * traceGasRetrS(jTrace)%relErrorColumnAP &
                                                 * strat_slant_column / 100.0d0  ! absolute; not in percent
                end if
                if ( trim(traceGasRetrS(jTrace)%nameTraceGas) =='trop_NO2') then
                    trop_AMF = weakAbsRetrS(iband)%amfWindowCorrT(jTrace)
                    trop_column = factor * traceGasRetrS(jTrace)%column
                end if
              end do ! jTrace
              trop_slant_column = factor * weakAbsRetrS(iband)%slantColumn(iTrace) - strat_slant_column
              retr_trop_column = trop_slant_column / trop_AMF
              bias_trop_column = 100.0d0 * (retr_trop_column - trop_column) / trop_column
              precision_trop_slant_column = factor * sqrt(weakAbsRetrS(iband)%covSlantColumn(iTrace)  &
                                                          +  precision_strat_slant_column**2 )
              if ( trim(traceGasRetrS(iTrace)%nameTraceGas) =='NO2') then
                write(outputFileUnit,*)
                write(outputFileUnit,'(A)') 'DOMINO results'
                write(outputFileUnit,'(A, ES12.4)') 'total slant column =                                   ', &
                                            factor * weakAbsRetrS(iband)%slantColumn(iTrace)
                write(outputFileUnit,'(A, ES12.4)') 'stratospheric vertical column =                        ', &
                                                    strat_column
                write(outputFileUnit,'(A, ES12.4)') 'stratospheric slant column =                           ', &
                                                    strat_slant_column
                write(outputFileUnit,'(A, ES12.4)') 'precision stratospheric slant column (absolute) =      ', &
                                                     precision_strat_slant_column
                write(outputFileUnit,'(A, ES12.4)') 'retrieved trop slant column  =                         ', &
                                            trop_slant_column
                write(outputFileUnit,'(A, ES12.4)') 'retrieved tropospheric vertical column =               ', &
                                            retr_trop_column
                write(outputFileUnit,'(A, F12.4)') 'bias for retrieved tropospheric vertical column (%) =  ', &
                                            bias_trop_column
                write(outputFileUnit,'(A, ES12.4)') 'precision tropospheric vertical column =               ', &
                     precision_trop_slant_column / trop_AMF
                write(outputFileUnit,'(A, F11.3)') 'precision tropospheric vertical column in percent =     ', &
                  100.0d0 * precision_trop_slant_column / trop_slant_column
              end if
            end if ! controlRetrS%method == 4

          case('nodeTemp')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A,2F10.3)') 'true and RETRIEVED TEMPERATURE       = '

          case('offsetTemp')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A,2F10.3)') 'true and RETRIEVED TEMPERATURE       = '

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)
            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.8)')   'a-priori true and retrieved DIFFERENTAL RING COEFFICIENT       = ', &
                 RRS_RingRetrS(iband)%ringCoeffAP, RRS_RingSimS(iband)%ringCoeff, RRS_RingRetrS(iband)%ringCoeff
            write(outputFileUnit,'(A, 2F12.8)')   'a-priori and a-posteriori error differential Ring coefficient  = ', &
                sqrt(RRS_RingRetrS(iband)%varRingCoeffAP), sqrt(RRS_RingRetrS(iband)%varRingCoeff)

          case('RingCoef')

            iband = retrS%codeSpecBand(istate)
            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved RING COEFFICIENT                   = ', &
                 RRS_RingRetrS(iband)%ringCoeffAP, RRS_RingSimS(iband)%ringCoeff, RRS_RingRetrS(iband)%ringCoeff
            write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error Ring coefficient               = ', &
                sqrt(RRS_RingRetrS(iband)%varRingCoeffAP), sqrt(RRS_RingRetrS(iband)%varRingCoeff)

          case('lnpolyCoef')
  
            iband = retrS%codeSpecBand(istate)
            ipoly = retrS%codelnPolyCoef(istate)

            if ( ipoly == 0) then
  
              write(outputFileUnit,*)
              write(outputFileUnit,'(A,50F10.5)') 'a-priori coefficients for ln(P)                                = ', &
                                                   weakAbsRetrS(iband)%lnpolyCoefAP(:)
              write(outputFileUnit,'(A,50F10.5)') 'RETRIEVED COEFFICIENTS FOR ln(P)                               = ', &
                                                   weakAbsRetrS(iband)%lnpolyCoef(:)
              write(outputFileUnit,'(A,50F10.5)') 'a-priori error coefficients (1 std dev)                        = ', &
                  ( sqrt( weakAbsRetrS(iband)%covlnPolyCoefAP(ipoly) ), ipoly = 0, weakAbsRetrS(iband)%degreePoly)
              write(outputFileUnit,'(A,50F10.5)') 'a-posteriori error coefficients (1 std dev)                    = ', &
                  ( sqrt( weakAbsRetrS(iband)%covlnPolyCoef(ipoly) ), ipoly = 0, weakAbsRetrS(iband)%degreePoly)
            end if

  
          case default
            call logDebug('in subroutine print_resultsDOAS in doasModule' )
            call logDebug('incorrect value for retrS%codeFitParameters(counter) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return
        end select
  
      end do ! istate

      ! write air mass factors for fit windows
      if( weakAbsRetrS(iband)%calculateAMF ) then
        write(outputFileUnit,*)
        write(outputFileUnit,*) 'AMF is calculated at a wavelength specified in the configuration file'
        write(outputFileUnit,*) 'using parameters specified in the configuration file for retrieval'
        write(outputFileUnit,*)
        write(outputFileUnit,'(A)') &
                'gas                   geomAMF              AMF      radianceCloudFraction   at wavelength (nm)'
        do iband = 1, numSpectrBands
          do iTrace = 1, nTrace
            if ( trim(traceGasRetrS(iTrace)%nameTraceGas) =='NO2'      .or. &
                 trim(traceGasRetrS(iTrace)%nameTraceGas) =='trop_NO2' .or. &
                 trim(traceGasRetrS(iTrace)%nameTraceGas) =='strat_NO2') then
              if ( weakAbsRetrS(iband)%useReferenceTempRetr ) then
                write(outputFileUnit,'(A10,4F20.8)') traceGasRetrS(iTrace)%nameTraceGas,              &
                      weakAbsRetrS(iband)%geometricalAMF, weakAbsRetrS(iband)%amfWindowCorrT(iTrace), &
                      weakAbsRetrS(iband)%radianceCldFraction, weakAbsRetrS(iband)%wavelengthAMF
              else
                write(outputFileUnit,'(A10,4F20.8)') traceGasRetrS(iTrace)%nameTraceGas,        &
                      weakAbsRetrS(iband)%geometricalAMF, weakAbsRetrS(iband)%amfWindow(iTrace), &
                      weakAbsRetrS(iband)%radianceCldFraction, weakAbsRetrS(iband)%wavelengthAMF
              end if ! %useReferenceTempRetr
            else
              write(outputFileUnit,'(A10,4F20.8)') traceGasRetrS(iTrace)%nameTraceGas,        &
                    weakAbsRetrS(iband)%geometricalAMF, weakAbsRetrS(iband)%amfWindow(iTrace), &
                    weakAbsRetrS(iband)%radianceCldFraction, weakAbsRetrS(iband)%wavelengthAMF
            end if ! NO2
          end do ! iTrace
        end do ! iband

        do iband = 1, numSpectrBands
          write(outputFileUnit,*)
          write(outputFileUnit,'(A, F10.3)') 'altitude resolved amf for the wavelength (nm): ', &
                                              weakAbsRetrS(iband)%wavelengthAMF
          write(outputFileUnit,'(A)') 'altitude (km)    pressure (hPa)    amf (-)     weights(km-1)'
          do ialt = 0, weakAbsRetrS(iband)%RTMnlayer
            write(outputFileUnit,'(4F14.5)') optPropRTMGridRetrS%RTMaltitude(ialt), &
                                             optPropRTMGridRetrS%RTMpressure(ialt), &
                                             weakAbsRetrS(iband)%amfAltRTM(ialt,1), &
                                             optPropRTMGridRetrS%RTMweight(ialt)
          end do ! ialt
        end do ! iband
      end if
  
      ! write polynomial and monochromatic air mass factors on instrument wavelength grid
      write(outputFileUnit,*)
      do iband = 1, numSpectrBands
        write(outputFileUnit,*)
        write(outputFileUnit,'(A,I4)') 'results for spectral band number: ', iband
        write(outputFileUnit,'(A)')  '   wavelength        polynomial'
        do iwave = 1, wavelInstrS(iband)%nwavel
          write(outputFileUnit,'(2F15.8)') wavelInstrS(iband)%wavel(iwave), weakAbsRetrS(iband)%polynomial(iwave)
        end do ! iwave

       ! write altitude averaged absorption cross sections on instrument wavelength grid
        write(outputFileUnit,*)
        do iTrace = 1, nTrace
          write(outputFileUnit,*)
          write(outputFileUnit,*) traceGasRetrS(iTrace)%nameTraceGas
          write(outputFileUnit,'(A)') 'wavelength(nm)      Xsec_eff   Xsec_eff_smooth  Xsec_eff_diff'
          do iwave = 1, wavelInstrS(iband)%nwavel
            write(outputFileUnit,'(F15.8,3ES15.6)')  wavelInstrS(iband)%wavel(iwave),                       &
                                                     weakAbsRetrS(iband)%XsecEffInstr(iwave, iTrace),       &
                                                     weakAbsRetrS(iband)%XsecEffSmoothInstr(iwave, iTrace), &
                                                     weakAbsRetrS(iband)%XsecEffDiffInstr(iwave, iTrace)
                                                     
          end do ! iwave
        end do ! iTrace
      end do ! iband
  
      ! write full a-priori errror covariance matrix ordered according to the results printed above
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'full a-priori covariance matrix Sa'
      do istate = 1, retrS%nstate
        write(outputFileUnit,'(A, 500E17.8)') retrS%codeFitParameters(istate), &
                          (retrS%Sa_lnvmr(istate,jstate), jstate = 1, retrS%nstate)
  
      end do
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'full a-posteriori covariance matrix S '
      do istate = 1, retrS%nstate
        write(outputFileUnit,'(A, 500E17.8)') retrS%codeFitParameters(istate), &
                        (diagnosticS%S_lnvmr(istate,jstate), jstate = 1, retrS%nstate)
      end do
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'noise part of the a-posteriori covariance matrix S '
      do istate = 1, retrS%nstate
        write(outputFileUnit,'(A, 500E17.8)') retrS%codeFitParameters(istate),  &
                        (diagnosticS%Snoise_lnvmr(istate,jstate), jstate = 1, retrS%nstate)
      end do
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'correlation coefficients (r**2) matrix for S'
      do istate = 1, retrS%nstate
        write(outputFileUnit,'(A, 50E17.8)') retrS%codeFitParameters(istate), (diagnosticS%S_lnvmr(istate,jstate) &
        / sqrt( diagnosticS%S_lnvmr(istate,istate) * diagnosticS%S_lnvmr(jstate,jstate) ) , jstate = 1, retrS%nstate)
      end do

      ! write gain for slant column only for one spectral band
      if ( numSpectrBands == 1 ) then
        write(outputFileUnit,*) 'transposed of the gain matrix for slant column'
        write(outputFileUnit,'(20X, 10A15)') (retrS%codeFitParameters(istate), istate = 1, retrS%nstate)
        do iwave = 1, retrS%nwavelRetr
          write(outputFileUnit,'(F15.8,10ES15.6)') wavelInstrS(1)%wavel(iwave),                       &
                   (DUToCm2*diagnosticS%G_lnvmr(istate,iwave), istate = 1, retrS%nstate)
        end do
        if( weakAbsRetrS(1)%calculateAMF ) then
          if (controlRetrS%method /= 4 ) then
            iTrace = retrS%codeTraceGas(istate)
            write(outputFileUnit,*)
            write(outputFileUnit,*) 'transposed of the gain matrix for vertical column'
            do iwave = 1, retrS%nwavelRetr
              write(outputFileUnit,'(F15.8,10ES15.6)') wavelInstrS(1)%wavel(iwave),                       &
              (DUToCm2*diagnosticS%G_lnvmr(iTrace,iwave)/weakAbsRetrS(1)%amfWindow(iTrace) , iTrace = 1, nTrace)
            end do
          else
            iTrace = retrS%codeTraceGas(istate)
            write(outputFileUnit,*)
            write(outputFileUnit,*) 'transposed of the gain matrix for total vertical column of NO2'
            do iwave = 1, retrS%nwavelRetr
              write(outputFileUnit,'(F15.8,10ES15.6)') wavelInstrS(1)%wavel(iwave),                       &
                                        DUToCm2*diagnosticS%G_lnvmr(1,iwave)/weakAbsRetrS(1)%amfWindow(1)
            end do
          end if ! controlRetrS%method /= 4 
        end if ! weakAbsRetrS(iband)%calculateAMF
      end if ! numSpectrBands == 1

    end subroutine print_resultsClassicDOAS


    subroutine print_classicDOAS_asciiHDF(errS, numSpectrBands, nTrace, geometryS, gasPTSimS, gasPTRetrS,  &
                                          traceGasSimS, traceGasRetrS, optPropRTMGridRetrS, XsecHRS,       &
                                          weakAbsRetrS, RRS_RingSimS, RRS_RingRetrS, retrS, diagnosticS,   &
                                          controlRetrS, surfaceSimS, LambCloudS)

      implicit none

      type(errorType),               intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands                  ! number wavelength band
      integer,                       intent(in)    :: nTrace                          ! number of trace gases
      type(geometryType),            intent(in)    :: geometryS                       ! info geometry
      type(gasPTType),               intent(inout) :: gasPTSimS                       ! pressure and temperature
      type(gasPTType),               intent(inout) :: gasPTRetrS                      ! pressure and temperature
      type(traceGasType),            intent(inout) :: traceGasSimS(nTrace)            ! atmospheric trace gas properties
      type(traceGasType),            intent(inout) :: traceGasRetrS(nTrace)           ! atmospheric trace gas properties
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridRetrS             ! optical properties on RTM grid retrieval
      type(XsecType),                intent(inout) :: XsecHRS(numSpectrBands, nTrace) ! absorption cross section
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands)    ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingSimS(numSpectrBands)    ! Ring spectra for simulation
      type(RRS_RingType),            intent(inout) :: RRS_RingRetrS(numSpectrBands)   ! Ring spectra for retrieval
      type(retrType),                intent(inout) :: retrS                           ! retrieval structure
      type(diagnosticType),          intent(inout) :: diagnosticS                     ! diagnostic information
      type(controlType),             intent(in)    :: controlRetrS                    ! control data for retrieval
      type(LambertianType),          intent(inout) :: surfaceSimS(numSpectrBands)     ! surface properties
      type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)      ! Lambertian cloud properties

      ! local
      integer :: istate, jstate, iband, ipoly, iTrace
      integer :: counterTrace
      integer :: i, itrop, istrat  ! indices for NO2, tropospheric NO2, and stratospheric NO2
      real(8) :: slantColumnNO2(1), slantColumnTropNO2(1), slantColumnStratNO2(1)
      real(8) :: precisionSlantColumnNO2(1)
      real(8) :: precisionSlantColumnStratNO2(1), precisionSlantColumnTropNO2(1)
      real(8) :: columnNO2(1), columnTropNO2(1), columnStratNO2(1), retrColumnTropNO2(1)  ! vertical columns
      real(8) :: tropAMF(1), stratAMF(1), biasRetrTropColumn(1), biasRetrTropColumnPercent(1)
      real(8) :: precisionRetrTropColumn(1), precisionRetrTropColumnPercent(1)

      real(8)               :: x_real(1)
      integer               :: x_int(1)
  
      real(8), parameter :: DUToCm2 = 2.68668d16

      character(LEN=50) :: legend(3), legend_precision(3)
      character(LEN=50) :: legendPoly(2), legend_precisionPoly(2)
      character(LEN=50) :: nameTraceGas(nTrace), unit(nTrace)
      character(LEN=50) :: nameTraceGas_state_vector(retrS%nstate)
      character(LEN=50) :: unit_state_vector(retrS%nstate)
      character(LEN=50) :: unit_covariance(retrS%nstate)

      character(LEN=50) :: groupName, name, keyword
      character(LEN=50) :: keywordList1, keywordList2, keywordList3, keywordList4
      character(LEN=50) :: keyword1
      character(LEN=50) :: string_1

      real(8)    :: factor
      real(8)    :: wavelengths(retrS%nstate)
      real(8)    :: correlation(retrS%nstate,retrS%nstate)
      real(8)    :: column(3, nTrace), precisionColumn(3, nTrace)
      real(8)    :: allBands(3), precisionAllBands(3)
      real(8)    :: slantColumnAP(nTrace),  precisionSlantColumnAP(nTrace), percentprecisionSlantColumnAP(nTrace)
      real(8)    :: slantColumn(nTrace), precisionSlantColumn(nTrace), percentprecisionSlantColumn(nTrace)
      real(8)    :: wavelengthAMF(numSpectrBands), radianceCloudFraction(numSpectrBands)
      real(8)    :: AMF(nTrace, numSpectrBands), AMF_correctedForTemperature(nTrace, numSpectrBands)
      real(8)    :: altitude(0:optPropRTMGridRetrS%RTMnlayer), pressure(0:optPropRTMGridRetrS%RTMnlayer)
      real(8)    :: temperature(0:optPropRTMGridRetrS%RTMnlayer), weight(0:optPropRTMGridRetrS%RTMnlayer)
      real(8)    :: altResolvedAMF(0:optPropRTMGridRetrS%RTMnlayer, numSpectrBands)
      real(8)    :: correctionFactorTemperature(nTrace, numSpectrBands)
      real(8)    :: referenceTemperature(numSpectrBands), effectiveTemperature(nTrace, numSpectrBands)

      real(8), allocatable  :: lnpolyCoef(:,:,:)           ! (maxNumCoefficients, a-priori/retrieved, numSpectrBands)
      real(8), allocatable  :: lnpolyCoefPrecision(:,:,:)  ! (maxNumCoefficients, a-priori/retrieved, numSpectrBands)

      integer    :: maxNumCoefficients             ! maximum number of coefficients for the spectral bands
      integer    :: spectralBandNumber(numSpectrBands)

      ! initialize counters
      counterTrace  = 0

      ! fill spectral band number, wavelengthAMF, radianceCloudFraction, AMF, altResolvedAMF
      do iband = 1, numSpectrBands
        spectralBandNumber(iband) = iband
        wavelengthAMF(iband)                     = weakAbsRetrS(iband)%wavelengthAMF
        radianceCloudFraction(iband)             = weakAbsRetrS(iband)%radianceCldFraction
        AMF(:, iband)                            = weakAbsRetrS(iband)%amfWindow(:)
        AMF_correctedForTemperature(:, iband)    = weakAbsRetrS(iband)%amfWindowCorrT(:)
        altResolvedAMF(:, iband)                 = weakAbsRetrS(iband)%amfAltRTM(:,1)
        correctionFactorTemperature(:, iband)    = XsecHRS(iband,:)%corrFactorAMF
        referenceTemperature(iband)              = weakAbsRetrS(iband)%referenceTempRetr
        effectiveTemperature(:, iband)           = weakAbsRetrS(iband)%Teff_minus_T0(:) + weakAbsRetrS(iband)%referenceTempRetr
      end do
      ! fill pressure and altitude grids for altitude resolved amf and the corresponding Gaussian weights
      altitude(:)    = optPropRTMGridRetrS%RTMaltitude(:)
      pressure(:)    = optPropRTMGridRetrS%RTMpressure(:)
      temperature(:) = optPropRTMGridRetrS%RTMtemperature(:)
      weight(:)      = optPropRTMGridRetrS%RTMweight(:)

      ! fill wavelengths and units array so that it can be used as attribute

      do istate = 1, retrS%nstate
  
        select case (retrS%codeFitParameters(istate))
  
          case('slantColumn')
            iTrace = retrS%codeTraceGas(istate)
            if ( traceGasRetrS(iTrace)%unitFlag ) then
              unit_state_vector(istate) = 'Dobson_Units'
              unit_covariance(istate)   = 'Dobson_Units2'
            else
              unit_state_vector(istate) = 'molecules_per_cm2'
              unit_covariance(istate)   = 'molecules2_per_cm4'
            end if
            nameTraceGas_state_vector(istate) = traceGasRetrS(iTrace)%nameTraceGas

          case('diffRingCoef', 'RingCoef', 'lnpolyCoef')
            wavelengths(istate) = 0.0d0
            unit_state_vector(istate) = ' '
            unit_covariance(istate)   = ' '
            nameTraceGas_state_vector(istate) = ' '

          case('nodeTemp', 'offsetTemp')
            unit_state_vector(istate) = 'Kelvin'
            unit_covariance(istate)   = 'Kelvin2'
            nameTraceGas_state_vector(istate) = ' '

          case default
            call logDebug('in subroutine print_resultsDOAS_asciiHDF in doasModule')
            call logDebug('incorrect value for retrS%codeFitParameters(counter) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return
        end select
  
      end do ! istate

      ! fill correlation coefficient matrix for S_lnvmr
      do jstate = 1, retrS%nstate
        do istate = 1, retrS%nstate
          correlation(istate,jstate) = diagnosticS%S_lnvmr(istate,jstate) &
            / sqrt( diagnosticS%S_lnvmr(istate,istate) * diagnosticS%S_lnvmr(jstate,jstate) )
        end do ! istate
      end do ! jstate

      ! fill legend and legend precision
      legend(1) = 'true'
      legend(2) = 'a_priori'
      legend(3) = 'retrieved'
      legend_precision(1) = 'a_priori_precision'
      legend_precision(2) = 'precision'
      legend_precision(3) = 'bias'

      legendPoly(1)           = 'a_priori'
      legendPoly(2)           = 'retrieved'
      legend_precisionPoly(1) = 'a_priori_precision'
      legend_precisionPoly(2) = 'precision'

      ! find the maximum number of polynomial coefficients
      maxNumCoefficients = maxval( weakAbsRetrS(:)%degreePoly )

      allocate ( lnpolyCoef         (0:maxNumCoefficients, 2, numSpectrBands) )
      allocate ( lnpolyCoefPrecision(0:maxNumCoefficients, 2, numSpectrBands) )
      lnpolyCoef          = 0.0d0
      lnpolyCoefPrecision = 0.0d0

      call initializeAsciiHDF
        call beginAttributes
          keyword = 'version_number_DISAMAR'
          call addAttribute(errS, keyword, DISAMAR_version_number)
          if (errorCheck(errS)) return
          keyword = 'solar_zenith_angle_retrieval'
          call addAttribute(errS, keyword, geometryS%sza)
          if (errorCheck(errS)) return
          keyword = 'viewing_zenith_angle_retrieval'
          call addAttribute(errS, keyword, geometryS%vza)
          if (errorCheck(errS)) return
          keyword = 'azimuth_difference_retrieval'
          call addAttribute(errS, keyword, geometryS%dphi)
          if (errorCheck(errS)) return
          keyword = 'number_spectral_bands'
          call addAttribute(errS, keyword, numSpectrBands)
          if (errorCheck(errS)) return
          keyword = 'number of iterations'
          call addAttribute(errS, keyword, retrS%numIterations + 1)
          if (errorCheck(errS)) return
          keyword = 'solution has converged'
          if ( retrS%isConverged ) then
            call addAttribute(errS, keyword, .true.)
            if (errorCheck(errS)) return
          else
            call addAttribute(errS, keyword, .false.)
            if (errorCheck(errS)) return
          end if
          keyword = 'stateVectorConvThreshold'
          call addAttribute(errS, keyword, retrS%xConvThreshold)
          if (errorCheck(errS)) return
          keyword = 'stateVectorConv'
          call addAttribute(errS, keyword, retrS%xConv)
          if (errorCheck(errS)) return
          keyword = 'chi2_reflectance'
          call addAttribute(errS, keyword, retrS%chi2R)
          if (errorCheck(errS)) return
          keyword = 'chi2_state_vector'
          call addAttribute(errS, keyword, retrS%chi2x)
          if (errorCheck(errS)) return
          keyword = 'chi2'
          call addAttribute(errS, keyword, retrS%chi2)
          if (errorCheck(errS)) return
          keyword = 'rmse'
          call addAttribute(errS, keyword, retrS%rmse)
          if (errorCheck(errS)) return
          keyword = 'DFS'
          call addAttribute(errS, keyword, diagnosticS%DFS)
          if (errorCheck(errS)) return
        call endAttributes

      groupName = 'sun_normalized_radiance'
      call beginGroup(errS, groupName)
      if (errorCheck(errS)) return
        keyword1 = 'unit'
        string_1 = 'nm'
        name = 'wavelengths'
        call writeReal(errS, name, x1D=retrS%wavelRetr, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = ' '
        name = 'SNR_reference_spectrum'
        call writeReal(errS, name, x1D=retrS%SNrefspec, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = ' '
        name = 'SNR_measured_spectrum'
        call writeReal(errS, name, x1D=retrS%reflMeas/retrS%reflNoiseError, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'per_sr'
        name = 'measured_spectrum'
        call writeReal(errS, name, x1D=retrS%reflMeas, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'per_sr'
        name = 'fitted_spectrum'
        call writeReal(errS, name, x1D=retrS%refl, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'per_sr'
        name = 'assumed_noise'
        call writeReal(errS, name, x1D=retrS%reflNoiseError, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'percent'
        name = 'initial_residue'
        call writeReal(errS, name, x1D=1.0D2*retrS%dR_initial(:)/retrS%reflMeas(:), keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'percent'
        name = 'final_residue'
        call writeReal(errS, name, x1D=1.0D2*retrS%dR(:)/retrS%reflMeas(:), keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
      call endGroup

      groupName = 'parameters'
      call beginGroup(errS, groupName)
      if (errorCheck(errS)) return

      do istate = 1, retrS%nstate
  
        select case (retrS%codeFitParameters(istate))
  
          case('slantColumn')
  
            counterTrace = counterTrace + 1
            iTrace = retrS%codeTraceGas(istate)
            iband  = retrS%codeSpecBand(istate)
            nameTraceGas(iTrace) = traceGasRetrS(iTrace)%nameTraceGas
            
            if ( traceGasRetrS(iTrace)%unitFlag ) then
              factor = 1.0d0 / DUToCm2
              unit(iTrace)  = 'Dobson Units'
            else
              factor = 1.0d0
              unit(iTrace)  = 'molecules_per_cm2'
            end if

            slantColumnAP                (iTrace) = factor * weakAbsRetrS(iband)%slantColumnAP(iTrace)
            precisionSlantColumnAP       (iTrace) = factor * sqrt(weakAbsRetrS(iband)%covSlantColumnAP(iTrace) )
            percentPrecisionSlantColumnAP(iTrace) = 100.0d0 *  precisionSlantColumnAP(iTrace) / slantColumnAP(iTrace)
            slantColumn                  (iTrace) = factor * weakAbsRetrS(iband)%slantColumn(iTrace)
            precisionSlantColumn         (iTrace) = factor * sqrt(weakAbsRetrS(iband)%covSlantColumn(iTrace) )
            percentPrecisionSlantColumn  (iTrace) = 100.0d0 * precisionSlantColumn(iTrace) / slantColumn(iTrace)

            if( counterTrace == nTrace ) then
              name = 'slant_columnAP'
              keywordList1 = 'name_trace_gas'
              keywordList2 = 'unit'
              call writeReal(errS, name, x1D = slantColumnAP, &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit)
              if (errorCheck(errS)) return
              name = 'precision_slant_columnAP'
              call writeReal(errS, name, x1D = precisionSlantColumnAP, &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit)
              if (errorCheck(errS)) return
              name = 'precision_slant_columnAP_percent'
              call writeReal(errS, name, x1D = percentPrecisionSlantColumnAP, &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit)
              if (errorCheck(errS)) return
              name = 'slant_column'
              keywordList1 = 'name_trace_gas'
              keywordList2 = 'unit'
              call writeReal(errS, name, x1D = slantColumn, &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit)
              if (errorCheck(errS)) return
              name = 'precision_slant_column'
              call writeReal(errS, name, x1D = precisionSlantColumn, &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit)
              if (errorCheck(errS)) return
              name = 'precision_slant_column_percent'
              call writeReal(errS, name, x1D = percentPrecisionSlantColumn, &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit)
              if (errorCheck(errS)) return
            end if ! counterTrace == nTrace

            if( weakAbsRetrS(iband)%calculateAMF ) then
              ! fill retrieved vertical column based on the calculated amf and the fitted slant column
              weakAbsRetrS(iband)%column(iTrace) = weakAbsRetrS(iband)%slantColumn(iTrace) &
                                                 / weakAbsRetrS(iband)%amfWindow(iTrace)
              ! NO2 is special
              if ( trim(traceGasRetrS(iTrace)%nameTraceGas) =='NO2'      .or. &
                   trim(traceGasRetrS(iTrace)%nameTraceGas) =='trop_NO2' .or. &
                   trim(traceGasRetrS(iTrace)%nameTraceGas) =='strat_NO2') then
                weakAbsRetrS(iband)%column(iTrace) = weakAbsRetrS(iband)%slantColumn(iTrace) &
                                                   / weakAbsRetrS(iband)%amfWindowCorrT(iTrace)
              end if
              column(1,iTrace)    = factor * traceGasSimS(iTrace)%column      ! true column
              column(2,iTrace)    = factor * traceGasRetrS(iTrace)%columnAP   ! a-priori column
              column(3,iTrace)    = weakAbsRetrS(iband)%column(iTrace)        ! retrieved column

              precisionColumn(1,iTrace) = factor * sqrt(traceGasRetrS(iTrace)%covColumnAP)      ! a-priori precision
              precisionColumn(2,iTrace) = factor * sqrt(weakAbsRetrS(iband)%covColumn(iTrace))  ! a-posteriori precision
              precisionColumn(3,iTrace) = column(3,iTrace) - column(1,iTrace)                   ! bias

              if( counterTrace == nTrace ) then
                name = 'total_columns'
                keywordList1 = 'name_trace_gas'
                keywordList2 = 'unit'
                keywordList3 = 'legend'
                call writeReal(errS, name, x2D = column, &
                               keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                               keywordList2 = keywordList2, stringList_2 = unit,         &
                               keywordList3 = keywordList3, stringList_3 = legend )
                if (errorCheck(errS)) return
                name = 'precision_bias_columns'
                call writeReal(errS, name, x2D = precisionColumn, &
                               keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                               keywordList2 = keywordList2, stringList_2 = unit,         &
                               keywordList3 = keywordList3, stringList_3 = legend_precision )
                if (errorCheck(errS)) return
              end if ! counterTrace == nTrace
            end if ! weakAbsRetrS(iband)%calculateAMF

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)

            allBands(1) = RRS_RingSimS(iband)%ringCoeff
            allBands(2) = RRS_RingRetrS(iband)%ringCoeffAP
            allBands(3) = RRS_RingRetrS(iband)%ringCoeff

            precisionAllBands(1) = sqrt(RRS_RingRetrS(iband)%varRingCoeffAP)   ! a-priori prescision
            precisionAllBands(2) = sqrt(RRS_RingRetrS(iband)%varRingCoeff)     ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'diffRingCoef'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return

          case('RingCoef')

            iband = retrS%codeSpecBand(istate)

            allBands(1) = RRS_RingSimS(iband)%ringCoeff
            allBands(2) = RRS_RingRetrS(iband)%ringCoeffAP
            allBands(3) = RRS_RingRetrS(iband)%ringCoeff

            precisionAllBands(1) = sqrt(RRS_RingRetrS(iband)%varRingCoeffAP)   ! a-priori prescision
            precisionAllBands(2) = sqrt(RRS_RingRetrS(iband)%varRingCoeff)     ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'RingCoef'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return

          case('nodeTemp')

          case('offsetTemp')

            allBands(1) = gasPTSimS%temperatureOffset
            allBands(2) = gasPTRetrS%temperatureOffsetAP
            allBands(3) = gasPTRetrS%temperatureOffset

            precisionAllBands(1) = sqrt(gasPTRetrS%varianceTempOffsetAP) ! a-priori precision
            precisionAllBands(2) = sqrt(gasPTRetrS%varianceTempOffset)   ! precision
            precisionAllBands(3) = allBands(3) - allBands(1)             ! bias

            name         = 'temperature_offset'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_temperature_offset'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('lnpolyCoef')

            iband = retrS%codeSpecBand(istate)
            ipoly = retrS%codelnPolyCoef(istate)

            lnpolyCoef(ipoly, 1, iband) = weakAbsRetrS(iband)%lnpolyCoefAP(ipoly)
            lnpolyCoef(ipoly, 2, iband) = weakAbsRetrS(iband)%lnpolyCoef  (ipoly)

            lnpolyCoefPrecision(ipoly, 1, iband) = sqrt( weakAbsRetrS(iband)%covlnPolyCoefAP(ipoly) )
            lnpolyCoefPrecision(ipoly, 2, iband) = sqrt( weakAbsRetrS(iband)%covlnPolyCoef  (ipoly) )

            if ( (ipoly == weakAbsRetrS(iband)%degreePoly) .and. (iband == numSpectrBands) ) then
  
              name = 'ln_polynomial_coefficients'
              keyword1     = 'unit'
              keywordList1 = 'legendPoly'
              keywordList2 = 'spectral_band_number'
              call writeReal(errS, name, x3D = lnpolyCoef, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = legendPoly, &
                             keywordList2 = keywordList2, intList_2    = spectralBandNumber)
              if (errorCheck(errS)) return
              

              name = 'precision_ln_poly_coef'
              keyword1     = 'unit'
              keywordList1 = 'legendPoly'
              keywordList2 = 'spectral_band_number'
              call writeReal(errS, name, x3D = lnpolyCoefPrecision, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = legendPoly, &
                             keywordList2 = keywordList2, intList_2    = spectralBandNumber)
              if (errorCheck(errS)) return

            end if
            
  
          case default

            call logDebug('in subroutine print_classicDOAS_asciiHDF in classic_doasModule' )
            call logDebug('incorrect value for retrS%codeFitParameters(counter) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return

        end select
  
      end do ! istate


      if( any( weakAbsRetrS(:)%calculateAMF) ) then
         do iTrace = 1, nTrace
           nameTraceGas(iTrace) = traceGasRetrS(iTrace)%nameTraceGas
         end do
         name = 'wavelengthAMF'
         call writeReal(errS, name, x1D = wavelengthAMF)
         if (errorCheck(errS)) return
         name = 'radiance_cloud_fraction'
         call writeReal(errS, name, x1D = radianceCloudFraction)
         if (errorCheck(errS)) return
         name = 'airMassFactor'
         keywordList1 = 'name_trace_gas'
         call writeReal(errS, name, x2D = AMF, &
                        keywordList1 = keywordList1, stringList_1 = nameTraceGas)
         if (errorCheck(errS)) return

         if ( weakAbsRetrS(iband)%useReferenceTempRetr ) then
           name = 'airMassFactor_corrected_for_temperature'
           keywordList1 = 'name_trace_gas'
           call writeReal(errS, name, x2D = AMF_correctedForTemperature, &
                          keywordList1 = keywordList1, stringList_1 = nameTraceGas)
           if (errorCheck(errS)) return
           name = 'reference_temperature'
           call writeReal(errS, name, x1D = referenceTemperature)
           if (errorCheck(errS)) return
           name = 'temperature_on_altitude_resolved_AMF_grid'
           call writeReal(errS, name, x1D = temperature)
           if (errorCheck(errS)) return
           name = 'effective_temperature'
           call writeReal(errS, name, x2D = effectiveTemperature, &
                          keywordList1 = keywordList1, stringList_1 = nameTraceGas)
           if (errorCheck(errS)) return
           name = 'temperature_correction_factor_AMF'
           call writeReal(errS, name, x2D = correctionFactorTemperature, &
                          keywordList1 = keywordList1, stringList_1 = nameTraceGas)
           if (errorCheck(errS)) return
           name = 'temperature_on_altitude_resolved_AMF_grid'
         end if !  weakAbsRetrS(iband)%useReferenceTempRetr

         name = 'pressureGridAltResolvedAMF_hPa'
         call writeReal(errS, name, x1D = pressure)
         if (errorCheck(errS)) return
         name = 'altitudeGridAltResolvedAMF_km'
         call writeReal(errS, name, x1D = altitude)
         if (errorCheck(errS)) return
         name = 'gaussianWeightsAltResolvedAMF_per_km'
         call writeReal(errS, name, x1D = weight)
         if (errorCheck(errS)) return
         name = 'altResolvedAMF'
         call writeReal(errS, name, x2D = altResolvedAMF)
         if (errorCheck(errS)) return
      end if

      keywordList1 = 'state_vector_elements'
      keywordList4 = 'nameTraceGas'
      name = 'correlation_coefficients'
      call writeReal(errS, name, x2D = correlation, &
                     keywordList1 = keywordList1, stringList_1 = retrS%codeFitParameters(1:retrS%nstate), &
                     keywordList4 = keywordList4, stringList_4 = nameTraceGas_state_vector)
      if (errorCheck(errS)) return

      call endGroup

      if( controlRetrS%method == 4 ) then

        do iTrace = 1, nTrace
          if ( trim(traceGasRetrS(iTrace)%nameTraceGas) =='NO2')       i      = iTrace
          if ( trim(traceGasRetrS(iTrace)%nameTraceGas) =='strat_NO2') istrat = iTrace
          if ( trim(traceGasRetrS(iTrace)%nameTraceGas) =='trop_NO2')  itrop  = iTrace
        end do

        groupName = 'DOMINO'
        call beginGroup(errS, groupName)
        if (errorCheck(errS)) return

        name = 'wavelengthAMF'
        call writeReal(errS, name, x1D = wavelengthAMF)
        if (errorCheck(errS)) return

        name = 'slant column NO2'
        slantColumnNO2(1) = factor * weakAbsRetrS(1)%slantColumn(i)
        call writeReal(errS, name, x1D = slantColumnNO2)
        if (errorCheck(errS)) return

        name = 'precision slant column NO2'
        precisionSlantColumnNO2(1) = factor * sqrt(weakAbsRetrS(iband)%covSlantColumn(i))
        call writeReal(errS, name, x1D = precisionSlantColumnNO2)
        if (errorCheck(errS)) return

        name = 'strat vertical column'
        columnStratNO2(1) = factor * traceGasRetrS(istrat)%column
        call writeReal(errS, name, x1D = columnStratNO2)
        if (errorCheck(errS)) return

        name = 'strat AMF'
        stratAMF(1) = weakAbsRetrS(1)%amfWindowCorrT(istrat)
        call writeReal(errS, name, x1D = stratAMF)
        if (errorCheck(errS)) return

        name = 'strat slant column'
        slantColumnStratNO2(1) = stratAMF(1) * factor * traceGasRetrS(istrat)%column
        call writeReal(errS, name, x1D = slantColumnStratNO2)
        if (errorCheck(errS)) return

        name = 'precision strat slant column'
        precisionSlantColumnStratNO2(1) = factor * traceGasRetrS(istrat)%relErrorColumnAP &
                                        * slantColumnStratNO2(1) / 100.0d0
        call writeReal(errS, name, x1D = precisionSlantColumnStratNO2)
        if (errorCheck(errS)) return

        name = 'trop slant column'
        slantColumnTropNO2(1) = slantColumnNO2(1) - slantColumnStratNO2(1)
        call writeReal(errS, name, x1D = slantColumnTropNO2)
        if (errorCheck(errS)) return

        name = 'trop AMF'
        tropAMF(1) = weakAbsRetrS(1)%amfWindowCorrT(itrop)
        call writeReal(errS, name, x1D = tropAMF)
        if (errorCheck(errS)) return

        name = 'retrieved trop column'
        retrColumnTropNO2(1) = slantColumnTropNO2(1) / tropAMF(1)
        call writeReal(errS, name, x1D = retrColumnTropNO2)
        if (errorCheck(errS)) return

        name = 'bias retrieved trop column'
        biasRetrTropColumn(1) = retrColumnTropNO2(1) - factor * traceGasRetrS(itrop)%column
        call writeReal(errS, name, x1D = biasRetrTropColumn)
        if (errorCheck(errS)) return

        name = 'bias retrieved trop column percent'
        biasRetrTropColumnPercent(1) = 100.0d0 * biasRetrTropColumn(1) / (factor * traceGasRetrS(itrop)%column)
        call writeReal(errS, name, x1D = biasRetrTropColumnPercent)
        if (errorCheck(errS)) return

        name = 'precision retrieved trop column'
        precisionRetrTropColumn(1) = &
                 sqrt(precisionSlantColumnNO2(1)**2 + precisionSlantColumnStratNO2(1)) / tropAMF(1)
        call writeReal(errS, name, x1D = precisionRetrTropColumn)
        if (errorCheck(errS)) return

        name = 'precision retrieved trop column percent'
        precisionRetrTropColumnPercent(1) = 100.0d0 * precisionRetrTropColumn(1) / retrColumnTropNO2(1)
        call writeReal(errS, name, x1D = precisionRetrTropColumnPercent)
        if (errorCheck(errS)) return

        call endGroup
      end if ! controlRetrS%method == 4

      groupName = 'specifications_sim'
      call beginGroup(errS, groupName)
      if (errorCheck(errS)) return

        name = 'solar_zenith_angle'
        x_real(1)    = geometryS%sza
        keyword1     = 'unit'
        string_1     = 'degrees'
        call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return

        name = 'solar_azimuth_angle'
        x_real(1)    = geometryS%s_azimuth
        keyword1     = 'unit'
        string_1     = 'degrees'
        call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return

        name = 'viewing_nadir_angle'
        x_real(1)    = geometryS%vza
        keyword1     = 'unit'
        string_1     = 'degrees'
        call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return

        name = 'viewing_azimuth_angle'
        x_real(1)    = geometryS%v_azimuth
        keyword1     = 'unit'
        string_1     = 'degrees'
        call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return

        name = 'azimuth_difference_used_in_RTM'
        x_real(1)    = geometryS%dphi
        keyword1     = 'unit'
        string_1     = 'degrees'
        call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return

        name = 'surface_pressure'
        x_real(1)    = surfaceSimS(1)%pressure
        keyword1     = 'unit'
        string_1     = 'hPa'
        call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return

      call endGroup

      call endAsciiHDF(errS)
      if (errorCheck(errS)) return

      deallocate(lnpolyCoef)
      deallocate(lnpolyCoefPrecision)

    end subroutine print_classicDOAS_asciiHDF

end module classic_doasModule

