!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module DISAMAR_log

    use iso_c_binding

    implicit none

    integer, parameter :: LOG_TRACE   = 0
    integer, parameter :: LOG_DEBUG   = 1
    integer, parameter :: LOG_INFO    = 2
    integer, parameter :: LOG_WARNING = 3
    integer, parameter :: LOG_ERROR   = 4

    integer :: disamar_log_level = LOG_INFO
    integer :: depth = 0
    character(len = 132) :: ws = ' '
    logical :: fort = .true.
    type(c_funptr) :: callback_logger_pointer

    interface
        subroutine disamar_logger_func(message, level)
            character*(*), intent(in) :: message
            integer, intent(in) :: level
        end subroutine
        subroutine c_disamar_logger_func(c_message, c_level)
            use iso_c_binding
            type(c_ptr), value :: c_message
            integer(c_int), value :: c_level
        end subroutine
    end interface

    save

    contains

    ! -------------------------------------------------------------------------------

    function c_disamar_set_log_level(c_level) bind(C)

        implicit none

        integer(c_int) :: c_disamar_set_log_level
        integer(c_int), value :: c_level

        call enter('c_disamar_set_log_level')

        c_disamar_set_log_level = disamar_set_log_level(c_level)

99999   continue
        call exit('c_disamar_set_log_level')

    end function c_disamar_set_log_level

    ! -------------------------------------------------------------------------------

    function disamar_set_log_level(level)

        integer, intent(in) :: level
        integer :: disamar_set_log_level
        call enter('disamar_set_log_level')

        disamar_set_log_level = 0 ! STATUS_S_SUCCESS

        if (level < LOG_TRACE .or. level > LOG_ERROR) then
            disamar_set_log_level = 16 ! STATUS_E_VALUE_ERROR
            goto 99999
        end if

        disamar_log_level = level

99999   continue
        call exit('disamar_set_log_level')

    end function disamar_set_log_level

    ! -------------------------------------------------------------------------------

    function c_disamar_set_callback_logger(callback_logger) bind(C)

        implicit none

        integer(c_int) :: c_disamar_set_callback_logger
        type(c_funptr), value :: callback_logger

        call enter('c_disamar_set_callback_logger')
        c_disamar_set_callback_logger = 0 ! STATUS_S_SUCCESS

        callback_logger_pointer = callback_logger
        fort = .false.

99999   continue
        call exit('c_disamar_set_callback_logger')

    end function c_disamar_set_callback_logger

    ! -------------------------------------------------------------------------------

    subroutine disamar_set_callback_logger(callback_logger)

        procedure(disamar_logger_func) :: callback_logger

        call enter('disamar_set_callback_logger')

        callback_logger_pointer = c_funloc(callback_logger)

99999   continue
        call exit('disamar_set_callback_logger')

    end subroutine disamar_set_callback_logger

    ! -------------------------------------------------------------------------------

    subroutine disamar_logger(message, level)

        character*(*), intent(in) :: message
        integer, intent(in) :: level

        character(c_char), pointer :: f_message(:)
        integer :: i
        integer :: status
        integer(c_int) :: c_level
        procedure(disamar_logger_func), pointer :: callback_logger
        procedure(c_disamar_logger_func), pointer :: c_callback_logger
        type(c_ptr) :: c_message

        ! check if callback function is available

        if (c_associated(callback_logger_pointer)) then
            if (fort) then
                call c_f_procpointer(callback_logger_pointer, callback_logger)
                call callback_logger(message, level)
            else
                call c_f_procpointer(callback_logger_pointer, c_callback_logger)
                allocate(f_message(len(trim(message))+1))
                do i = 1, len(trim(message))
                    f_message(i) = message(i:i)
                end do
                f_message(len(trim(message))+1) = c_null_char
                c_message = c_loc(f_message(1))
                c_level = level
                call c_callback_logger(c_message, c_level)
                deallocate(f_message)
            end if
            goto 99999
        end if

        ! if no callback function use default logging

        if (level .lt. disamar_log_level) goto 99999

        if (level .eq. LOG_TRACE) then
            write(*,*) '[T] ' // trim(message)
        else if (level .eq. LOG_DEBUG) then
            write(*,*) '[D] ' // trim(message)
        else if (level .eq. LOG_INFO) then
            write(*,*) '[I] ' // trim(message)
        else if (level .eq. LOG_WARNING) then
            write(*,*) '[W] ' // trim(message)
        else if (level .eq. LOG_ERROR) then
            write(*,*) '[E] ' // trim(message)
        else
            write(*,*) trim(message)
        end if

99999   continue

    end subroutine disamar_logger

    ! -------------------------------------------------------------------------------

    subroutine enter(name)

        character*(*), intent(in) :: name

        integer :: indent
        indent = depth * 2
        call disamar_logger(ws(1:indent) // 'enter ' // trim(name), LOG_TRACE)
        depth = depth + 1

    end subroutine enter

    !  -------------------------------------------------------------------------------

    subroutine exit(name)

        character*(*), intent(in) :: name

        integer :: indent
        depth = depth - 1
        indent = depth * 2
        call disamar_logger(ws(1:indent) // 'exit  ' // trim(name), LOG_TRACE)

    end subroutine exit

    !  -------------------------------------------------------------------------------

    subroutine fortranlog(msg, len, lev)

        character*(*) msg
        integer len, lev

        call disamar_logger(msg, lev)

    end subroutine fortranlog

    !  -------------------------------------------------------------------------------

    subroutine disamar_array_hash(var, label)
        implicit none
        real(8), intent(in) :: var(:)
        character*(*), intent(in) :: label

        integer :: N
        integer(8), allocatable, dimension(:) :: int_var
        integer(8) :: a, b, i, hash
        character*(200) :: msg

        integer(8), parameter :: mod_adler=65521

        a = 1
        b = 0
        hash = 0

        N = size(var, 1)
        allocate(int_var(N))
        int_var = 0

        int_var = TRANSFER(var, int_var, N)

        do i= 1, N
            a = MOD(a + int_var(i), mod_adler)
            b = MOD(b+a, mod_adler)
        end do

        deallocate(int_var)

        hash = ior(b * 65536, a)

        write(msg, '(A,A,A,Z16.16,A,I8)') 'Hash for ', label, ': ', hash, ', length: ', N
        call disamar_logger(msg, LOG_DEBUG)

    end subroutine disamar_array_hash

    !  -------------------------------------------------------------------------------

end module DISAMAR_log
