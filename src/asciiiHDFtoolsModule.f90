!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module asciiHDFtools

  ! This module contains tools to write output in a special format
  ! hat can be transformed into a HDF5 file using python software.

  ! The following routines are available
  ! call initializeAsciiHDF    - required for initialization
  ! call endAsciiHDF           - required for the last line
  ! call beginGroup(errS, name)      - name should start with a letter, can contain letters, numbers, '-', and '_'
  !                              but it can not contain blank spaces
  ! call endGroup              - the number of calls to endGroup and beginGroup should be the same
  ! call beginAttributes       - required if attributes are used - should start directly after beginArray or beginGroup
  ! call endAttributes         - the number of call to beginAttributes and endAttributes should be the same
  ! call addAttribute(errS, keyword, value) - keyword has the same restrictions as name
  !                                   - value can be a Boolean, integer, real(4), real(8), or string

  use dataStructures

  interface addAttribute
    module procedure addAttributeChar, addAttributeInt, addAttributeReal4, addAttributeReal8, addAttributeB
  end interface

  interface addAttributeList
    module procedure addAttributeListChar, addAttributeListInt, addAttributeListReal4, addAttributeListReal8
  end interface

  ! default is private
  private 
  public  :: initializeAsciiHDF, endAsciiHDF
  public  :: beginGroup, endGroup
  public  :: beginAttributes, endAttributes, addAttribute, addAttributeList
  public  :: writeString, writeInt, writeReal

  integer :: beginGroupCounter, endGroupCounter
  integer :: beginAttributeCounter, endAttributeCounter

  contains

    subroutine initializeAsciiHDF

      implicit none

      beginGroupCounter = 1
      endGroupCounter   = 1
      beginAttributeCounter = 1
      endAttributeCounter   = 1

      write(asciiHDFoutputUnit,'(A)')      'BeginGroup(/)'

    end subroutine initializeAsciiHDF


    subroutine endAsciiHDF(errS)

      implicit none
      type(errorType), intent(inout) :: errS

      if ( beginGroupCounter /= endGroupCounter ) then
        call logDebug('ERROR in writing to AsciiHDF')
        call logDebug('number of calls to beginGroup and endGroup differs')
        call mystop(errS, 'stopped because beginGroupCounter and endGroupCounter differ')
        if (errorCheck(errS)) return
      end if

      if ( beginAttributeCounter /= endAttributeCounter ) then
        call logDebug('ERROR in writing to AsciiHDF')
        call logDebug('number of calls to beginAttributes and endAttributes differs')
        call mystop(errS, 'stopped because beginAttributeCounter and endAttributeCounter differ')
        if (errorCheck(errS)) return
      end if

      write(asciiHDFoutputUnit,'(A)')      'EndGroup'

    end subroutine endAsciiHDF


    subroutine beginGroup(errS, name)

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: name     

      write(asciiHDFoutputUnit,*)
      write(asciiHDFoutputUnit,'(3A)')      'BeginGroup(',trim(name),')'
      beginGroupCounter = beginGroupCounter + 1

    end subroutine beginGroup


    subroutine endGroup

      implicit none

      write(asciiHDFoutputUnit,'(A)')      'EndGroup'
      endGroupCounter = endGroupCounter + 1

    end subroutine endGroup


    subroutine beginAttributes

      implicit none

      write(asciiHDFoutputUnit,'(A)')   'BeginAttributes'
      beginAttributeCounter = beginAttributeCounter + 1

    end subroutine beginAttributes


    subroutine addAttributeChar(errS, keyword, value)

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: keyword     
      character(LEN=50), intent(in) :: value     

      write(asciiHDFoutputUnit,'(4A)')  trim(keyword), ' = "', trim(value), '"'

    end subroutine addAttributeChar


    subroutine addAttributeInt(errS, keyword, value)

      ! Integer value

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: keyword     
      integer,           intent(in) :: value     

      write(asciiHDFoutputUnit,'(2A, I10)')  trim(keyword), ' = ', value

    end subroutine addAttributeInt


    subroutine addAttributeReal4(errS, keyword, value)

      ! Single precision value

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: keyword     
      real(4),           intent(in) :: value     

      write(asciiHDFoutputUnit,'(2A, E16.7)')  trim(keyword), ' = ', value

    end subroutine addAttributeReal4


    subroutine addAttributeReal8(errS, keyword, value)

      ! Double precision value

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: keyword     
      real(8),           intent(in) :: value 

      write(asciiHDFoutputUnit,'(2A, E25.15E3)')  trim(keyword), ' = ', value

    end subroutine addAttributeReal8


    subroutine addAttributeB(errS, keyword, value)

      ! Boolean or logical value

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: keyword     
      logical,           intent(in) :: value     

      character(LEN=4), parameter :: str_true  = 'true'
      character(LEN=5), parameter :: str_false = 'false'

      if ( value ) then
        write(asciiHDFoutputUnit,'(3A)')  trim(keyword), ' = ', str_true
      else
        write(asciiHDFoutputUnit,'(3A)')  trim(keyword), ' = ', str_false
      end if

    end subroutine addAttributeB


    subroutine addAttributeListChar(errS, keyword, array)

      ! List of strings

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: keyword     
      character(LEN=50), intent(in) :: array(:)

      ! local
      integer :: length, i

      length = size(array)

      if ( length > 1 ) then
        write(asciiHDFoutputUnit,'(3A, 500(A1,A,A1,A2),A1,A,A1)') trim(keyword), ' = ', &
        '[', ( '"',trim(array(i)),'"',', ' ,i = 1, length - 1), '"',trim(array(length)),'"', ']'
      else
        write(asciiHDFoutputUnit,'(7A)')  trim(keyword), ' = ', '[', '"',trim(array(length)),'"', ']'
      end if

    end subroutine addAttributeListChar


    subroutine addAttributeListInt(errS, keyword, array)

      ! List of integers

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: keyword     
      integer,           intent(in) :: array(:)

      ! local
      integer :: length, i

      length = size(array)

      if ( length > 1 ) then
        write(asciiHDFoutputUnit,'(3A, 500(I10,A2),I10, A)')  trim(keyword), ' = ', &
          '[', (array(i),', ',i = 1, length - 1), array(length), ']'
      else
        write(asciiHDFoutputUnit,'(3A, I10, A)')  trim(keyword), ' = ', '[', array(length), ']'
      end if

    end subroutine addAttributeListInt


    subroutine addAttributeListReal4(errS, keyword, array)

      ! List of single precision reals

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: keyword     
      real(4),           intent(in) :: array(:)

      ! local
      integer :: length, i

      length = size(array)

      if ( length > 1 ) then
        write(asciiHDFoutputUnit,'(3A, 500(E16.7,A2),E16.7, A)') trim(keyword), ' = ', '[', &
                       ( array(i),', ',i = 1, length - 1), array(length), ']'
      else
        write(asciiHDFoutputUnit,'(3A, E16.7, A)')   trim(keyword), ' = ', '[', array(length), ']'
      end if

    end subroutine addAttributeListReal4


    subroutine addAttributeListReal8(errS, keyword, array)

      ! List of double precision reals

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=50), intent(in) :: keyword     
      real(8),           intent(in) :: array(:)

      ! local
      integer :: length, i

      length = size(array)
      if ( length > 1 ) then
        write(asciiHDFoutputUnit,'(3A, 500(E25.16E3,A2),E25.16E3, A)') trim(keyword), ' = ',  '[', &
            ( array(i),', ',i = 1, length - 1 ), array(length), ']'
      else
        write(asciiHDFoutputUnit,'(3A, E25.16E3, A)') trim(keyword), ' = ', '[', array(length), ']'
      end if

    end subroutine addAttributeListReal8


    subroutine endAttributes

      implicit none

      write(asciiHDFoutputUnit,'(A)')   'EndAttributes'
      endAttributeCounter = endAttributeCounter + 1

    end subroutine endAttributes


    subroutine insertAttributes(errS, keyword1, string_1, int_1, real4_1, real8_1, logical_1, &
                                keyword2, string_2, int_2, real4_2, real8_2, logical_2, &
                                keyword3, string_3, int_3, real4_3, real8_3, logical_3, &
                                keyword4, string_4, int_4, real4_4, real8_4, logical_4, &
                                keywordList1, stringList_1, intList_1, real4List_1, real8List_1, logicalList_1, &
                                keywordList2, stringList_2, intList_2, real4List_2, real8List_2, logicalList_2, &
                                keywordList3, stringList_3, intList_3, real4List_3, real8List_3, logicalList_3, &
                                keywordList4, stringList_4, intList_4, real4List_4, real8List_4, logicalList_4)

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN= 50), intent(in) :: keyword1, keyword2, keyword3, keyword4
      character(LEN= 50), intent(in) :: string_1, string_2, string_3, string_4
      integer,            intent(in) :: int_1, int_2, int_3, int_4
      real(4),            intent(in) :: real4_1, real4_2, real4_3, real4_4
      real(8),            intent(in) :: real8_1, real8_2, real8_3, real8_4
      logical,            intent(in) :: logical_1, logical_2, logical_3, logical_4
      character(LEN= 50), intent(in) :: keywordList1, keywordList2, keywordList3, keywordList4
      character(LEN= 50), intent(in) :: stringList_1(:), stringList_2(:), stringList_3(:), stringList_4(:)
      integer,            intent(in) :: intList_1(:), intList_2(:), intList_3(:), intList_4(:)
      real(4),            intent(in) :: real4List_1(:), real4List_2(:), real4List_3(:), real4List_4(:)
      real(8),            intent(in) :: real8List_1(:), real8List_2(:), real8List_3(:), real8List_4(:)
      real(8),            intent(in) :: logicalList_1(:), logicalList_2(:), logicalList_3(:), logicalList_4(:)

      optional :: keyword1, keyword2, keyword3, keyword4
      optional :: string_1, string_2, string_3, string_4
      optional :: int_1, int_2, int_3, int_4
      optional :: real4_1, real4_2, real4_3, real4_4
      optional :: real8_1, real8_2, real8_3, real8_4
      optional :: logical_1, logical_2, logical_3, logical_4
      optional :: keywordList1, keywordList2, keywordList3, keywordList4
      optional :: stringList_1, stringList_2, stringList_3, stringList_4
      optional :: intList_1, intList_2, intList_3, intList_4
      optional :: real4List_1, real4List_2, real4List_3, real4List_4
      optional :: real8List_1, real8List_2, real8List_3, real8List_4
      optional :: logicalList_1, logicalList_2, logicalList_3, logicalList_4

      if ( present(keyword1)     .or. present(keyword2)     .or. present(keyword3)     .or. &
           present(keyword4)     .or. present(keywordList1) .or. present(keywordList2) .or. &
           present(keywordList3) .or. present(keywordList4) ) then

        call beginAttributes

          if ( present(keyword1) .and. present(string_1 ) ) call addAttribute(errS, keyword1, string_1 )
                                                            if (errorCheck(errS)) return
          if ( present(keyword1) .and. present(int_1    ) ) call addAttribute(errS, keyword1, int_1    )
                                                            if (errorCheck(errS)) return
          if ( present(keyword1) .and. present(real4_1  ) ) call addAttribute(errS, keyword1, real4_1  )
                                                            if (errorCheck(errS)) return
          if ( present(keyword1) .and. present(real8_1  ) ) call addAttribute(errS, keyword1, real8_1  )
                                                            if (errorCheck(errS)) return
          if ( present(keyword1) .and. present(logical_1) ) call addAttribute(errS, keyword1, logical_1)
                                                            if (errorCheck(errS)) return
          if ( present(keyword2) .and. present(string_2 ) ) call addAttribute(errS, keyword2, string_2 )
                                                            if (errorCheck(errS)) return
          if ( present(keyword2) .and. present(int_2    ) ) call addAttribute(errS, keyword2, int_2    )
                                                            if (errorCheck(errS)) return
          if ( present(keyword2) .and. present(real4_2  ) ) call addAttribute(errS, keyword2, real4_2  )
                                                            if (errorCheck(errS)) return
          if ( present(keyword2) .and. present(real8_2  ) ) call addAttribute(errS, keyword2, real8_2  )
                                                            if (errorCheck(errS)) return
          if ( present(keyword2) .and. present(logical_2) ) call addAttribute(errS, keyword2, logical_2)
                                                            if (errorCheck(errS)) return
          if ( present(keyword3) .and. present(string_3 ) ) call addAttribute(errS, keyword3, string_3 )
                                                            if (errorCheck(errS)) return
          if ( present(keyword3) .and. present(int_3    ) ) call addAttribute(errS, keyword3, int_3    )
                                                            if (errorCheck(errS)) return
          if ( present(keyword3) .and. present(real4_3  ) ) call addAttribute(errS, keyword3, real4_3  )
                                                            if (errorCheck(errS)) return
          if ( present(keyword3) .and. present(real8_3  ) ) call addAttribute(errS, keyword3, real8_3  )
                                                            if (errorCheck(errS)) return
          if ( present(keyword3) .and. present(logical_3) ) call addAttribute(errS, keyword3, logical_3)
                                                            if (errorCheck(errS)) return
          if ( present(keyword4) .and. present(string_4 ) ) call addAttribute(errS, keyword4, string_4 )
                                                            if (errorCheck(errS)) return
          if ( present(keyword4) .and. present(int_4    ) ) call addAttribute(errS, keyword4, int_4    )
                                                            if (errorCheck(errS)) return
          if ( present(keyword4) .and. present(real4_4  ) ) call addAttribute(errS, keyword4, real4_4  )
                                                            if (errorCheck(errS)) return
          if ( present(keyword4) .and. present(real8_4  ) ) call addAttribute(errS, keyword4, real8_4  )
                                                            if (errorCheck(errS)) return
          if ( present(keyword4) .and. present(logical_4) ) call addAttribute(errS, keyword4, logical_4)
                                                            if (errorCheck(errS)) return
          if ( present(keywordList1) .and. present(stringList_1 ) ) call addAttributeList(errS, keywordList1, stringList_1 )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList1) .and. present(intList_1    ) ) call addAttributeList(errS, keywordList1, intList_1    )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList1) .and. present(real4List_1  ) ) call addAttributeList(errS, keywordList1, real4List_1  )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList1) .and. present(real8List_1  ) ) call addAttributeList(errS, keywordList1, real8List_1  )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList1) .and. present(logicalList_1) ) call addAttributeList(errS, keywordList1, logicalList_1)
                                                       if (errorCheck(errS)) return
          if ( present(keywordList2) .and. present(stringList_2 ) ) call addAttributeList(errS, keywordList2, stringList_2 )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList2) .and. present(intList_2    ) ) call addAttributeList(errS, keywordList2, intList_2    )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList2) .and. present(real4List_2  ) ) call addAttributeList(errS, keywordList2, real4List_2  )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList2) .and. present(real8List_2  ) ) call addAttributeList(errS, keywordList2, real8List_2  )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList2) .and. present(logicalList_2) ) call addAttributeList(errS, keywordList2, logicalList_2)
                                                       if (errorCheck(errS)) return
          if ( present(keywordList3) .and. present(stringList_3 ) ) call addAttributeList(errS, keywordList3, stringList_3 )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList3) .and. present(intList_3    ) ) call addAttributeList(errS, keywordList3, intList_3    )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList3) .and. present(real4List_3  ) ) call addAttributeList(errS, keywordList3, real4List_3  )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList3) .and. present(real8List_3  ) ) call addAttributeList(errS, keywordList3, real8List_3  )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList3) .and. present(logicalList_3) ) call addAttributeList(errS, keywordList3, logicalList_3)
                                                       if (errorCheck(errS)) return
          if ( present(keywordList4) .and. present(stringList_4 ) ) call addAttributeList(errS, keywordList4, stringList_4 )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList4) .and. present(intList_4    ) ) call addAttributeList(errS, keywordList4, intList_4    )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList4) .and. present(real4List_4  ) ) call addAttributeList(errS, keywordList4, real4List_4  )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList4) .and. present(real8List_4  ) ) call addAttributeList(errS, keywordList4, real8List_4  )
                                                                    if (errorCheck(errS)) return
          if ( present(keywordList4) .and. present(logicalList_4) ) call addAttributeList(errS, keywordList4, logicalList_4)
                                                       if (errorCheck(errS)) return

        call endAttributes

      end if

    end subroutine insertAttributes


    subroutine writeString(errS, name, x, x1D, x2D, x3D, x_long,                         &
                           keyword1, string_1, int_1, real4_1, real8_1, logical_1, &
                           keyword2, string_2, int_2, real4_2, real8_2, logical_2, &
                           keyword3, string_3, int_3, real4_3, real8_3, logical_3, &
                           keyword4, string_4, int_4, real4_4, real8_4, logical_4, &
                           keywordList1, stringList_1, intList_1, real4List_1, real8List_1, logicalList_1, &
                           keywordList2, stringList_2, intList_2, real4List_2, real8List_2, logicalList_2, &
                           keywordList3, stringList_3, intList_3, real4List_3, real8List_3, logicalList_3, &
                           keywordList4, stringList_4, intList_4, real4List_4, real8List_4, logicalList_4)

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN= 50), intent(in) :: name
      character(LEN= 50), intent(in) :: x, x1D(:), x2D(:,:), x3D(:,:,:)
      character(LEN=400), intent(in) :: x_long
      character(LEN= 50), intent(in) :: keyword1, keyword2, keyword3, keyword4
      character(LEN= 50), intent(in) :: string_1, string_2, string_3, string_4
      integer,            intent(in) :: int_1, int_2, int_3, int_4
      real(4),            intent(in) :: real4_1, real4_2, real4_3, real4_4
      real(8),            intent(in) :: real8_1, real8_2, real8_3, real8_4
      logical,            intent(in) :: logical_1, logical_2, logical_3, logical_4
      character(LEN= 50), intent(in) :: keywordList1,      keywordList2,     keywordList3,     keywordList4
      character(LEN= 50), intent(in) :: stringList_1(:),   stringList_2(:),  stringList_3(:),  stringList_4(:)
      integer,            intent(in) :: intList_1(:),         intList_2(:),     intList_3(:),     intList_4(:)
      real(4),            intent(in) :: real4List_1(:),     real4List_2(:),   real4List_3(:),   real4List_4(:)
      real(8),            intent(in) :: real8List_1(:),     real8List_2(:),   real8List_3(:),   real8List_4(:)
      real(8),            intent(in) :: logicalList_1(:), logicalList_2(:), logicalList_3(:), logicalList_4(:)

      optional :: x, x1D, x2D, x3D
      optional :: x_long
      optional :: keyword1, keyword2, keyword3, keyword4
      optional :: string_1, string_2, string_3, string_4
      optional :: int_1, int_2, int_3, int_4
      optional :: real4_1, real4_2, real4_3, real4_4
      optional :: real8_1, real8_2, real8_3, real8_4
      optional :: logical_1, logical_2, logical_3, logical_4
      optional :: keywordList1, keywordList2, keywordList3, keywordList4
      optional :: stringList_1, stringList_2, stringList_3, stringList_4
      optional :: intList_1, intList_2, intList_3, intList_4
      optional :: real4List_1, real4List_2, real4List_3, real4List_4
      optional :: real8List_1, real8List_2, real8List_3, real8List_4
      optional :: logicalList_1, logicalList_2, logicalList_3, logicalList_4

      ! local
      integer  :: i, j, k
      integer  :: shape2DArray(2), shape3DArray(3)

      write(asciiHDFoutputUnit,'(3A)')         'BeginArray(', trim(name),', string)'

      call insertAttributes(errS, keyword1, string_1, int_1, real4_1, real8_1, logical_1, &
                            keyword2, string_2, int_2, real4_2, real8_2, logical_2, &
                            keyword3, string_3, int_3, real4_3, real8_3, logical_3, &
                            keyword4, string_4, int_4, real4_4, real8_4, logical_4, &
                            keywordList1, stringList_1, intList_1, real4List_1, real8List_1, logicalList_1, &
                            keywordList2, stringList_2, intList_2, real4List_2, real8List_2, logicalList_2, &
                            keywordList3, stringList_3, intList_3, real4List_3, real8List_3, logicalList_3, &
                            keywordList4, stringList_4, intList_4, real4List_4, real8List_4, logicalList_4)
      if (errorCheck(errS)) return

      write(asciiHDFoutputUnit,'(A)')          'Order = Fortran'
      if ( present(x) )  then
        write(asciiHDFoutputUnit,'(A)')        'NumDimensions = 1'
        write(asciiHDFoutputUnit,'(A)')        'Size = 1'
        write(asciiHDFoutputUnit,'(A)')        trim(x)
      end if
      if ( present(x1D) )  then
        write(asciiHDFoutputUnit,'(A)')        'NumDimensions = 1'
        write(asciiHDFoutputUnit,'(A,I10)')    'Size = ', shape(x1D)
        do i = 1, size(x1D)
          write(asciiHDFoutputUnit,'(A)') trim(x1D(i))
        end do
      end if
      if ( present(x2D) )  then
        write(asciiHDFoutputUnit,'(A)')        'NumDimensions = 2'
        write(asciiHDFoutputUnit,'(A,2I10)')   'Size = ', shape(x2D)
        shape2DArray = shape(x2D)
        do j = 1, shape2DArray(2)
          do i = 1, shape2DArray(1)
            write(asciiHDFoutputUnit,'(A)')  trim(x2D(i,j))
          end do
        end do
      end if
      if ( present(x3D) )  then
        write(asciiHDFoutputUnit,'(A)')        'NumDimensions = 3'
        write(asciiHDFoutputUnit,'(A,3I10)')   'Size = ', shape(x3D)
        shape3DArray = shape(x3D)
        do k = 1, shape3DArray(3)
          do j = 1, shape3DArray(2)
            do i = 1, shape3DArray(1)
              write(asciiHDFoutputUnit,'(A)')  trim(x3D(i,j,k))
            end do
          end do
        end do
      end if
      if ( present(x_long) )  then
        write(asciiHDFoutputUnit,'(A)')        'NumDimensions = 1'
        write(asciiHDFoutputUnit,'(A)')        'Size = 1'
        write(asciiHDFoutputUnit,'(A)')        trim(x_long)
      end if

      write(asciiHDFoutputUnit,'(A)')          'EndArray'

    end subroutine writeString


    subroutine writeInt(errS, name, x, x1D, x2D, x3D, &
                        keyword1, string_1, int_1, real4_1, real8_1, logical_1, &
                        keyword2, string_2, int_2, real4_2, real8_2, logical_2, &
                        keyword3, string_3, int_3, real4_3, real8_3, logical_3, &
                        keyword4, string_4, int_4, real4_4, real8_4, logical_4, &
                        keywordList1, stringList_1, intList_1, real4List_1, real8List_1, logicalList_1, &
                        keywordList2, stringList_2, intList_2, real4List_2, real8List_2, logicalList_2, &
                        keywordList3, stringList_3, intList_3, real4List_3, real8List_3, logicalList_3, &
                        keywordList4, stringList_4, intList_4, real4List_4, real8List_4, logicalList_4)

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN= 50), intent(in) :: name
      integer,            intent(in) :: x, x1D(:), x2D(:,:), x3D(:,:,:)
      character(LEN= 50), intent(in) :: keyword1, keyword2, keyword3, keyword4
      character(LEN= 50), intent(in) :: string_1, string_2, string_3, string_4
      integer,            intent(in) :: int_1, int_2, int_3, int_4
      real(4),            intent(in) :: real4_1, real4_2, real4_3, real4_4
      real(8),            intent(in) :: real8_1, real8_2, real8_3, real8_4
      logical,            intent(in) :: logical_1, logical_2, logical_3, logical_4
      character(LEN= 50), intent(in) :: keywordList1,      keywordList2,     keywordList3,     keywordList4
      character(LEN= 50), intent(in) :: stringList_1(:),   stringList_2(:),  stringList_3(:),  stringList_4(:)
      integer,            intent(in) :: intList_1(:),         intList_2(:),     intList_3(:),     intList_4(:)
      real(4),            intent(in) :: real4List_1(:),     real4List_2(:),   real4List_3(:),   real4List_4(:)
      real(8),            intent(in) :: real8List_1(:),     real8List_2(:),   real8List_3(:),   real8List_4(:)
      real(8),            intent(in) :: logicalList_1(:), logicalList_2(:), logicalList_3(:), logicalList_4(:)

      optional :: x, x1D, x2D, x3D
      optional :: keyword1, keyword2, keyword3, keyword4
      optional :: string_1, string_2, string_3, string_4
      optional :: int_1, int_2, int_3, int_4
      optional :: real4_1, real4_2, real4_3, real4_4
      optional :: real8_1, real8_2, real8_3, real8_4
      optional :: logical_1, logical_2, logical_3, logical_4
      optional :: keywordList1, keywordList2, keywordList3, keywordList4
      optional :: stringList_1, stringList_2, stringList_3, stringList_4
      optional :: intList_1, intList_2, intList_3, intList_4
      optional :: real4List_1, real4List_2, real4List_3, real4List_4
      optional :: real8List_1, real8List_2, real8List_3, real8List_4
      optional :: logicalList_1, logicalList_2, logicalList_3, logicalList_4

      write(asciiHDFoutputUnit,'(3A)')         'BeginArray(', trim(name),', int32)'

      call insertAttributes(errS, keyword1, string_1, int_1, real4_1, real8_1, logical_1, &
                            keyword2, string_2, int_2, real4_2, real8_2, logical_2, &
                            keyword3, string_3, int_3, real4_3, real8_3, logical_3, &
                            keyword4, string_4, int_4, real4_4, real8_4, logical_4, &
                            keywordList1, stringList_1, intList_1, real4List_1, real8List_1, logicalList_1, &
                            keywordList2, stringList_2, intList_2, real4List_2, real8List_2, logicalList_2, &
                            keywordList3, stringList_3, intList_3, real4List_3, real8List_3, logicalList_3, &
                            keywordList4, stringList_4, intList_4, real4List_4, real8List_4, logicalList_4)
      if (errorCheck(errS)) return

      write(asciiHDFoutputUnit,'(A)')          'Order = Fortran'
      if ( present(x) )  then
        write(asciiHDFoutputUnit,'(A)')        'NumDimensions = 1'
        write(asciiHDFoutputUnit,'(A)')        'Size = 1'
        write(asciiHDFoutputUnit,'(I10)')      x
      end if
      if ( present(x1D) )  then
        write(asciiHDFoutputUnit,'(A)')        'NumDimensions = 1'
        write(asciiHDFoutputUnit,'(A,I10)')    'Size = ', shape(x1D)
        write(asciiHDFoutputUnit,'(5000I10)')  x1D
      end if
      if ( present(x2D) )  then
        write(asciiHDFoutputUnit,'(A)')        'NumDimensions = 2'
        write(asciiHDFoutputUnit,'(A,2I10)')   'Size = ', shape(x2D)
        write(asciiHDFoutputUnit,'(20000I10)') x2D
      end if
      if ( present(x3D) )  then
        write(asciiHDFoutputUnit,'(A)')        'NumDimensions = 3'
        write(asciiHDFoutputUnit,'(A,3I10)')   'Size = ', shape(x3D)
        write(asciiHDFoutputUnit,'(2000000I10)') x3D
      end if

      write(asciiHDFoutputUnit,'(A)')          'EndArray'

    end subroutine writeInt


    subroutine writeReal(errS, name, x, x1D, x2D, x3D, &
                         keyword1, string_1, int_1, real4_1, real8_1, logical_1, &
                         keyword2, string_2, int_2, real4_2, real8_2, logical_2, &
                         keyword3, string_3, int_3, real4_3, real8_3, logical_3, &
                         keyword4, string_4, int_4, real4_4, real8_4, logical_4, &
                         keywordList1, stringList_1, intList_1, real4List_1, real8List_1, logicalList_1, &
                         keywordList2, stringList_2, intList_2, real4List_2, real8List_2, logicalList_2, &
                         keywordList3, stringList_3, intList_3, real4List_3, real8List_3, logicalList_3, &
                         keywordList4, stringList_4, intList_4, real4List_4, real8List_4, logicalList_4)

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN= 50), intent(in) :: name     
      real(8),            intent(in) :: x, x1D(:), x2D(:,:), x3D(:,:,:)
      character(LEN= 50), intent(in) :: keyword1, keyword2, keyword3, keyword4
      character(LEN= 50), intent(in) :: string_1, string_2, string_3, string_4
      integer,            intent(in) :: int_1, int_2, int_3, int_4
      real(4),            intent(in) :: real4_1, real4_2, real4_3, real4_4
      real(8),            intent(in) :: real8_1, real8_2, real8_3, real8_4
      logical,            intent(in) :: logical_1, logical_2, logical_3, logical_4
      character(LEN= 50), intent(in) :: keywordList1,         keywordList2,     keywordList3,    keywordList4
      character(LEN= 50), intent(in) :: stringList_1(:),   stringList_2(:),  stringList_3(:),  stringList_4(:)
      integer,            intent(in) :: intList_1(:),         intList_2(:),     intList_3(:),     intList_4(:)
      real(4),            intent(in) :: real4List_1(:),     real4List_2(:),   real4List_3(:),   real4List_4(:)
      real(8),            intent(in) :: real8List_1(:),     real8List_2(:),   real8List_3(:),   real8List_4(:)
      real(8),            intent(in) :: logicalList_1(:), logicalList_2(:), logicalList_3(:), logicalList_4(:)

      optional :: x, x1D, x2D, x3D
      optional :: keyword1, keyword2, keyword3, keyword4
      optional :: string_1, string_2, string_3, string_4
      optional :: int_1, int_2, int_3, int_4
      optional :: real4_1, real4_2, real4_3, real4_4
      optional :: real8_1, real8_2, real8_3, real8_4
      optional :: logical_1, logical_2, logical_3, logical_4
      optional :: keywordList1, keywordList2, keywordList3, keywordList4
      optional :: stringList_1, stringList_2, stringList_3, stringList_4
      optional :: intList_1, intList_2, intList_3, intList_4
      optional :: real4List_1, real4List_2, real4List_3, real4List_4
      optional :: real8List_1, real8List_2, real8List_3, real8List_4
      optional :: logicalList_1, logicalList_2, logicalList_3, logicalList_4

      write(asciiHDFoutputUnit,'(3A)')           'BeginArray(', trim(name),', float64)'

      call insertAttributes(errS, keyword1, string_1, int_1, real4_1, real8_1, logical_1, &
                            keyword2, string_2, int_2, real4_2, real8_2, logical_2, &
                            keyword3, string_3, int_3, real4_3, real8_3, logical_3, &
                            keyword4, string_4, int_4, real4_4, real8_4, logical_4, &
                            keywordList1, stringList_1, intList_1, real4List_1, real8List_1, logicalList_1, &
                            keywordList2, stringList_2, intList_2, real4List_2, real8List_2, logicalList_2, &
                            keywordList3, stringList_3, intList_3, real4List_3, real8List_3, logicalList_3, &
                            keywordList4, stringList_4, intList_4, real4List_4, real8List_4, logicalList_4)
      if (errorCheck(errS)) return


      write(asciiHDFoutputUnit,'(A)')            'Order = Fortran'
      if ( present(x) )  then
        write(asciiHDFoutputUnit,'(A)')          'NumDimensions = 1'
        write(asciiHDFoutputUnit,'(A)')          'Size = 1'
        write(asciiHDFoutputUnit,'(E25.16E3)')     x
      end if
      if ( present(x1D) )  then
        write(asciiHDFoutputUnit,'(A)')          'NumDimensions = 1'
        write(asciiHDFoutputUnit,'(A,I10)')      'Size = ', shape(x1D)
        write(asciiHDFoutputUnit,'(5000E25.16E3)') x1D
      end if
      if ( present(x2D) )  then
        write(asciiHDFoutputUnit,'(A)')          'NumDimensions = 2'
        write(asciiHDFoutputUnit,'(A,2I10)')     'Size = ', shape(x2D)
        write(asciiHDFoutputUnit,'(20000E25.16E3)') x2D
      end if
      if ( present(x3D) )  then
        write(asciiHDFoutputUnit,'(A)')          'NumDimensions = 3'
        write(asciiHDFoutputUnit,'(A,3I10)')     'Size = ', shape(x3D)
        write(asciiHDFoutputUnit,'(2000000E25.16E3)') x3D
      end if

      write(asciiHDFoutputUnit,'(A)')            'EndArray'

    end subroutine writeReal


end module asciiHDFtools
