!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module FourierCoefficientsModule

  ! This module contains methods to collect the Fourier coefficients (and associated data)
  ! and write them out to an asciiHDF file (which can easily be transformed into HDF-5 
  ! using some python code).
  
  ! The default behaviour is to NOT create the output file, to actually do nothing.
  ! To collect the Fourier coefficients set the parameter "writeFourierCoefficients" to .true.

  ! The following routines are available
  ! MSinitFourierFile    - Initializes the output file, writes out generic metadata and 
  !                        allocates the buffers.
  ! MScloseFourierFile   - Write out the data to the output file, deallocate the memory and
  !                        close the output file.
  ! MSaddDataFourier     - add a Fourier coefficient (for R, and both wf_interface arrays).
  ! MSaddDataK           - add a data point to both extinction coefficient arrays.

  use dataStructures
  
  implicit none
    
  private
  
  public :: MSinitFourierFile, MScloseFourierFile
  public :: MSaddDataFourier, MSaddDataK
  
  
  integer, parameter :: outputFourierCoefficientsUnit   = 50
  character(LEN= 50), parameter :: FourierCoefficientsFileName = 'disamar_fc.asciiHDF'
  logical, parameter :: writeFourierCoefficients = .true.

  real(8), pointer :: refl(:,:) => null()
  real(8), pointer :: wf_interface_k_abs_fc(:, :) => null()
  real(8), pointer :: wf_interface_k_sca_fc(:, :) => null()
  real(8), pointer :: k_sca(:) => null()
  real(8), pointer :: k_abs(:) => null()
  
  contains
    subroutine MSinitFourierFile(errS, wavelMRS, geometryS, cloudAerosolRTMgridS, optPropRTMGridS)
      implicit none
      
    type(errorType), intent(inout) :: errS
      type(wavelHRType),intent(in) :: wavelMRS        ! instr - high resolution wavelength grid
      type(geometryType),intent(in) :: geometryS       ! information on geometry
      type(cloudAerosolRTMgridType), intent(in) :: cloudAerosolRTMgridS      ! cloud-aerosol properties
      type(optPropRTMGridType), intent(in) :: optPropRTMGridS           ! optical properties on RTM grid
      
      integer :: allocStatus
      integer :: sumAllocStatus
      
      if (writeFourierCoefficients) then
! Open output file
        open(UNIT=outputFourierCoefficientsUnit, ACTION='WRITE', STATUS='REPLACE', FILE=FourierCoefficientsFileName)

! write an ASCIIHDF file (easily converted to HDF-5).
        write(outputFourierCoefficientsUnit,'(A)')      'BeginGroup(/)'
        write(outputFourierCoefficientsUnit,'(A)')      'BeginAttributes'

! Some metadata attributes.
        write(outputFourierCoefficientsUnit,'(3A)') 'DISAMAR_version_number = "', DISAMAR_version_number,'"'
        write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'solar_zenith_angle = ', geometryS%sza
        write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'viewing_zenith_angle = ', geometryS%vza
        write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'mu0 = ', geometryS%u0
        write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'mu = ', geometryS%uu
        if (cloudAerosolRTMgridS%useAlbedoLambCldAllBands) then
          write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'albedo = ', cloudAerosolRTMgridS%albedoLambCldAllBands
        else
          write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'albedo = ', -1.0D0
        end if
        write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'cloud_temperature = ', &
          optPropRTMGridS%RTMtemperature(cloudAerosolRTMgridS%RTMnlevelCloud)
        write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'scale_height = ', &
          8.31446D0 * optPropRTMGridS%RTMtemperature(cloudAerosolRTMgridS%RTMnlevelCloud) / (28.964D0 * 9.81D0)
        write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'cloud_altitude = ', &
          cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit)
        write(outputFourierCoefficientsUnit,'(A,E25.15E3)') 'cloud_pressure = ', &
          cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit)
        write(outputFourierCoefficientsUnit,'(A)')      'EndAttributes'

! Write out the wavelenth array
        write(outputFourierCoefficientsUnit,'(A)')      'BeginArray(wavelengths)'
        write(outputFourierCoefficientsUnit,'(A)')      'BeginAttributes'
        write(outputFourierCoefficientsUnit,'(A)')      'long_name = "wavelength"'
        write(outputFourierCoefficientsUnit,'(A)')      'units = "nm"'
        write(outputFourierCoefficientsUnit,'(A)')      'EndAttributes'
        write(outputFourierCoefficientsUnit,'(A)')      'Order = Fortran'
        write(outputFourierCoefficientsUnit,'(A)')      'NumDimensions = 1'
        write(outputFourierCoefficientsUnit,'(A,I10)')      'Size = ', shape(wavelMRS%wavel)
        write(outputFourierCoefficientsUnit,'(50000E25.16E3)') wavelMRS%wavel
        write(outputFourierCoefficientsUnit,'(A)')      'EndArray'

! Write out the Gaussian weights (for integration over the slit function).
        write(outputFourierCoefficientsUnit,'(A)')      'BeginArray(weight)'
        write(outputFourierCoefficientsUnit,'(A)')      'BeginAttributes'
        write(outputFourierCoefficientsUnit,'(A)')      'long_name = "Gaussian weight for integration"'
        write(outputFourierCoefficientsUnit,'(A)')      'units = "1"'
        write(outputFourierCoefficientsUnit,'(A)')      'EndAttributes'
        write(outputFourierCoefficientsUnit,'(A)')      'Order = Fortran'
        write(outputFourierCoefficientsUnit,'(A)')      'NumDimensions = 1'
        write(outputFourierCoefficientsUnit,'(A,I10)')      'Size = ', shape(wavelMRS%weight)
        write(outputFourierCoefficientsUnit,'(50000E25.16E3)') wavelMRS%weight
        write(outputFourierCoefficientsUnit,'(A)')      'EndArray'
        
! Allocate data arrays (sizes are now known)
        sumAllocStatus = 0
        
        if ( .not. associated ( refl ) ) then
          allocate( refl(wavelMRS%nwavel, 3), STAT = allocStatus )
          sumAllocStatus = sumAllocStatus + allocStatus
        end if
        
        if ( .not. associated ( wf_interface_k_abs_fc ) ) then
          allocate( wf_interface_k_abs_fc(wavelMRS%nwavel, 3), STAT = allocStatus )
          sumAllocStatus = sumAllocStatus + allocStatus
        end if
        
        if ( .not. associated ( wf_interface_k_sca_fc ) ) then
          allocate( wf_interface_k_sca_fc(wavelMRS%nwavel, 3), STAT = allocStatus )
          sumAllocStatus = sumAllocStatus + allocStatus
        end if
        
        if ( .not. associated ( k_sca ) ) then
          allocate( k_sca(wavelMRS%nwavel), STAT = allocStatus )
          sumAllocStatus = sumAllocStatus + allocStatus
        end if
        
        if ( .not. associated ( k_abs ) ) then
          allocate( k_abs(wavelMRS%nwavel), STAT = allocStatus )
          sumAllocStatus = sumAllocStatus + allocStatus
        end if
        
        if ( sumAllocStatus /= 0 ) then
          call mystop(errS, 'MSinitFourierFile has nonzero sumAllocStatus. alloc Failed')
          if (errorCheck(errS)) return
        end if
      end if
    end subroutine MSinitFourierFile
    
    subroutine MScloseFourierFile(errS)
      implicit none
      type(errorType), intent(inout) :: errS
      
      integer :: sumDeallocStatus
      integer :: deallocStatus
      
      if (writeFourierCoefficients) then 
        if ( associated ( refl ) ) then
          write(outputFourierCoefficientsUnit,'(A)')      'BeginArray(refl)'
          write(outputFourierCoefficientsUnit,'(A)')      'BeginAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'long_name = "R"'
          write(outputFourierCoefficientsUnit,'(A)')      'title = "R"'
          write(outputFourierCoefficientsUnit,'(A)')      'dimensions = "(wavelength, Fourier)"'
          write(outputFourierCoefficientsUnit,'(A)')      'EndAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'Order = Fortran'
          write(outputFourierCoefficientsUnit,'(A)')      'NumDimensions = 2'
          write(outputFourierCoefficientsUnit,'(A,2I10)')      'Size = ', shape(refl)
          write(outputFourierCoefficientsUnit,'(50000E25.16E3)') refl
          write(outputFourierCoefficientsUnit,'(A)')      'EndArray'
        end if
        
        if ( associated ( wf_interface_k_abs_fc ) ) then
          write(outputFourierCoefficientsUnit,'(A)')      'BeginArray(wf_interface_k_abs_fc)'
          write(outputFourierCoefficientsUnit,'(A)')      'BeginAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'long_name = "d2R/dk_abs dz"'
          write(outputFourierCoefficientsUnit,'(A)')      &
          'title = "partial derivative of R with respect to volume absorption coefficient and altitude"'
          write(outputFourierCoefficientsUnit,'(A)')      'dimensions = "(wavelength, Fourier)"'
          write(outputFourierCoefficientsUnit,'(A)')      'EndAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'Order = Fortran'
          write(outputFourierCoefficientsUnit,'(A)')      'NumDimensions = 2'
          write(outputFourierCoefficientsUnit,'(A,2I10)')      'Size = ', shape(wf_interface_k_abs_fc)
          write(outputFourierCoefficientsUnit,'(50000E25.16E3)') wf_interface_k_abs_fc
          write(outputFourierCoefficientsUnit,'(A)')      'EndArray'
        end if
        
        if ( associated ( wf_interface_k_sca_fc ) ) then
          write(outputFourierCoefficientsUnit,'(A)')      'BeginArray(wf_interface_k_sca_fc)'
          write(outputFourierCoefficientsUnit,'(A)')      'BeginAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'long_name = "d2R/dk_sca dz"'
          write(outputFourierCoefficientsUnit,'(A)')      &
          'title = "partial derivative of R with respect to volume scattering coefficient and altitude"'
          write(outputFourierCoefficientsUnit,'(A)')      'dimensions = "(wavelength, Fourier)"'
          write(outputFourierCoefficientsUnit,'(A)')      'EndAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'Order = Fortran'
          write(outputFourierCoefficientsUnit,'(A)')      'NumDimensions = 2'
          write(outputFourierCoefficientsUnit,'(A,2I10)')      'Size = ', shape(wf_interface_k_sca_fc)
          write(outputFourierCoefficientsUnit,'(50000E25.16E3)') wf_interface_k_sca_fc
          write(outputFourierCoefficientsUnit,'(A)')      'EndArray'
        end if
        
        if ( associated ( k_sca ) ) then
          write(outputFourierCoefficientsUnit,'(A)')      'BeginArray(k_sca)'
          write(outputFourierCoefficientsUnit,'(A)')      'BeginAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'long_name = "extinction due to scattering"'
          write(outputFourierCoefficientsUnit,'(A)')      'units = "cm-1"'
          write(outputFourierCoefficientsUnit,'(A)')      'EndAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'Order = Fortran'
          write(outputFourierCoefficientsUnit,'(A)')      'NumDimensions = 1'
          write(outputFourierCoefficientsUnit,'(A,I10)')      'Size = ', shape(k_sca)
          write(outputFourierCoefficientsUnit,'(50000E25.16E3)') k_sca
          write(outputFourierCoefficientsUnit,'(A)')      'EndArray'
        end if
        
        if ( associated ( k_abs ) ) then
          write(outputFourierCoefficientsUnit,'(A)')      'BeginArray(k_abs)'
          write(outputFourierCoefficientsUnit,'(A)')      'BeginAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'long_name = "extinction due to absorption"'
          write(outputFourierCoefficientsUnit,'(A)')      'units = "cm-1"'
          write(outputFourierCoefficientsUnit,'(A)')      'EndAttributes'
          write(outputFourierCoefficientsUnit,'(A)')      'Order = Fortran'
          write(outputFourierCoefficientsUnit,'(A)')      'NumDimensions = 1'
          write(outputFourierCoefficientsUnit,'(A,I10)')      'Size = ', shape(k_abs)
          write(outputFourierCoefficientsUnit,'(50000E25.16E3)') k_abs
          write(outputFourierCoefficientsUnit,'(A)')      'EndArray'
        end if
        
        write(outputFourierCoefficientsUnit,'(A)') 'EndGroup'
        close(outputFourierCoefficientsUnit)

        sumDeallocStatus = 0
        if ( associated ( refl ) ) then
          deallocate( refl, STAT = deallocStatus )
          sumDeallocStatus = sumDeallocStatus + deallocStatus
        end if
        
        if ( associated ( wf_interface_k_abs_fc ) ) then
          deallocate( wf_interface_k_abs_fc, STAT = deallocStatus )
          sumDeallocStatus = sumDeallocStatus + deallocStatus
        end if
        
        if ( associated ( wf_interface_k_sca_fc ) ) then
          deallocate( wf_interface_k_sca_fc, STAT = deallocStatus )
          sumDeallocStatus = sumDeallocStatus + deallocStatus
        end if
        
        if ( associated ( k_sca ) ) then
          deallocate( k_sca, STAT = deallocStatus )
          sumDeallocStatus = sumDeallocStatus + deallocStatus
        end if
        
        if ( associated ( k_abs ) ) then
          deallocate( k_abs, STAT = deallocStatus )
          sumDeallocStatus = sumDeallocStatus + deallocStatus
        end if
        
        if ( sumDeallocStatus /= 0 ) then
          call mystop(errS, 'MScloseFourierFile has nonzero sumDeallocStatus. dealloc Failed')
          if (errorCheck(errS)) return
        end if
      end if
    end subroutine MScloseFourierFile
    
    subroutine MSaddDataFourier(errS, iWavel, iFourier, R, wf_interface_k_abs, wf_interface_k_sca)
      implicit none
    type(errorType), intent(inout) :: errS
      integer, intent(in) :: iWavel, iFourier
      real(8), intent(in) :: R, wf_interface_k_abs, wf_interface_k_sca
      
      if (writeFourierCoefficients) then 
        if ( associated ( refl ) ) then
          refl(iWavel, iFourier) = R
        end if
        
        if ( associated ( wf_interface_k_abs_fc ) ) then
          wf_interface_k_abs_fc(iWavel, iFourier) = wf_interface_k_abs
        end if
        
        if ( associated ( wf_interface_k_sca_fc ) ) then
          wf_interface_k_sca_fc(iWavel, iFourier) = wf_interface_k_sca
        end if
      end if
    end subroutine MSaddDataFourier
    
    subroutine MSaddDataK(errS, iWavel, k_abs_d, k_sca_d)
      implicit none
    type(errorType), intent(inout) :: errS
      integer, intent(in) :: iWavel
      real(8), intent(in) :: k_abs_d, k_sca_d
      
      if (writeFourierCoefficients) then 
        if ( associated ( k_abs ) ) then
          k_abs(iWavel) = k_abs_d
        end if
        
        if ( associated ( k_sca ) ) then
          k_sca(iWavel) = k_sca_d
        end if
      end if
    end subroutine MSaddDataK
end module FourierCoefficientsModule

