!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module optimalEstmationModule

  ! This module contains subroutines to for optimal estimation

  use dataStructures
  use mathTools,            only: GaussDivPoints, splintLin, spline, splint, polyInt, svdcmp, svbksb
  use propAtmosphereModule, only: update_altitudes_PTz_grid, update_T_z_other_grids, update_ndens_vmr, &
                                  calculateTotalColumn

  ! default is private
  private 
  public  :: fillCodesFitParameters, updateFitParameters, updateCovFitParameters
  public  :: fillAPriori, calculateNewState, calculateDiagnostics
  public  :: evaluateSpectralFeatures, calculate_Se

  contains


  subroutine fillCodesFitParameters(errS, numSpectrBands, nTrace, controlS, traceGasS, surfaceS, LambCloudS,     &
                                    cldAerFractionS, gasPTS, mulOffsetS, strayLightS, RRS_RingS, &
                                    cloudAerosolRTMgridS, retrS)


    ! Fill the logical array fitAtmStateVector with values for the parameters that are to be fitted.
    ! It contains .true. if that parameters is to be fitted. The array is filled in the following order
    ! number of values                                code
    ! sum(traceGasRetrS(:)%nalt) + nTrace           nodeTrace     : fit nodes used for the trace gas profiles
    ! nTrace                                        columnTrace   : fit total column of trace gases
    ! gasPTS%nalt+1                                 nodeTemp      : fit temperature profile
    ! 1 value                                       offsetTemp    : fit temperature offset
    ! 1 value                                       surfPressure  : fit surface pressure
    ! sum(surfaceS(:)%nwavelAlbedo)                 surfAlbedo    : fit surface albedo at nodes for each wavelength band
    ! sum(surfaceS(:)%nwavelEmission)               surfEmission  : fit surface emission at nodes for each wavelength band
    ! sum(LambCloudS(:)%nwavelAlbedo)               LambCldAlbedo : fit Lambertian cloud albedo at nodes for each wavelength band
    ! sum(cldAerFractionS(:)%nwavelCldAerFraction)         : fit cloud/aerosol fraction (wavelength dependent)
    ! sum(earthRadMulOffsetRetrS(:)%nwavel)  mulOffset     : fit multiplicative offset for each wavelength band
    ! sum(earthRadStrayLightRetrS(:)%nwavel) straylight    : fit straylight for each wavelength band
    ! numSpectrBands                         RingCoeff     : fit Ring spectrum for each spectral band
    ! 1 value                                cloudFraction : fit cloud fraction (wavelength independent)
    ! 1 value                                aerosolTau    : fit aerosol optical thickness
    ! 1 value                                cloudTau      : fit cloud optical thickness (or Lambertian albedo)
    ! 1 value                                intervalDP    : fit top pressure of the special interval with cloud/aerosol
    !                                                      ! keeping the pressure difference P(bot) - P(top) fixed
    ! 1 value                                intervalTop   : fit top of the special interval containing cloud/aerosol
    ! 1 value                                intervalBot   : fit bottom of the special interval containing cloud/aerosol

    ! The associated arrays in the retrieval structure
    !   codeFitParameters       contains the strings used to identify fit parameters
    !   codeSpecBand            contains the number of the wavelength band (used for surface albedo and stray light)
    !   codeTraceGas            contains the trace gas number
    !   codeAltitude            contains the altitude index (used for the nodes of the profiles)
    !   codeIndexSurfAlb        contains the wavelength index for the surface albedo
    !   codeIndexSurfEmission   contains the wavelength index for the surface emission
    !   codeIndexLambCldAlb     contains the wavelength index for the Lambertian cloud albedo
    !   codeIndexCloudFraction  contains the wavelength index for the cloud fraction
    !   codeIndexMulOffset      contains the polynomial coefficient index for the multiplicative offset
    !   codeIndexStraylight     contains the polynomial coefficient index for the straylight
    ! these arrays can be used to identify the state vector elements, e.g. if for state vector element
    !   x(index) we can print codeFitParameters(index) and get nodeTrace
    !                         codeTraceGas             and get the trace gas number
    !                         codeAltitude             and get the altitude index

    ! After the call to fillCodesFitParameters the number of state vector elements is known.
    ! Therefore, the length of the arrays fitAtmStateVector and codeFitParameters is
    ! the maximum number of parameters that can be fitted, and not the number of state vector elements.

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                       intent(in)    :: numSpectrBands                  ! number of spectral bands
    integer,                       intent(in)    :: nTrace                          ! number of trace gases
    type(controlType),             intent(in)    :: controlS                        ! control parameters
    type(traceGasType),            intent(inout) :: traceGasS(nTrace)               ! properties of the trace gases
    type(LambertianType),          intent(in)    :: surfaceS(numSpectrBands)        ! properties of the surface
    type(LambertianType),          intent(in)    :: LambCloudS(numSpectrBands)      ! Lambertian cloud properties
    type(cldAerFractionType),      intent(in)    :: cldAerFractionS(numSpectrBands) ! cloud/aerosol fraction (wavel dep)
    type(gasPTType),               intent(in)    :: gasPTS                          ! pressure and temperature
    type(mulOffsetType),           intent(in)    :: mulOffsetS(numSpectrBands)      ! multiplicative offset
    type(straylightType),          intent(in)    :: strayLightS(numSpectrBands)     ! stray light
    type(RRS_RingType),            intent(in)    :: RRS_RingS(numSpectrBands)       ! Ring spectrum
    type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS            ! aerosol and cloud properties
    type(retrType),                intent(inout) :: retrS                           ! retrieval parameters

    ! local
    integer :: counter, counter_Sa_not_diag, iband, index, istate, iTrace, ialt
    integer :: startValue, endValue, ncolumnfit

    integer :: allocStatus
    integer :: sumAllocStatus

    logical, parameter :: verbose = .false.

    allocStatus    = 0
    sumAllocStatus = 0

    ! claim memory for the arrays

    allocate( retrS%codeFitParameters(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codeSpecBand(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codeTraceGas(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codeAltitude(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codeIndexSurfAlb(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codeIndexSurfEmission(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codeIndexLambCldAlb(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codeIndexCloudFraction(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codeIndexMulOffset(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codeIndexStraylight(retrS%maxfitparameters), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%codelnPolyCoef(retrS%maxfitparameters), STAT = allocStatus )  ! not used here but for DOAS
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then 
      call logDebug('FATAL ERROR: allocation failed')
      call logDebug('for codes that define the state vector')
      call logDebug('in subroutine fillCodesFitParameters')
      call logDebug('in module optimalEstmationModule')
      call mystop(errS, 'stopped becaue allocation failed')
      if (errorCheck(errS)) return
    end if

    ! initialize
    counter                       = 0
    counter_Sa_not_diag           = 0 
    retrS%codeSpecBand            = 0
    retrS%codeTraceGas            = 0
    retrS%codeAltitude            = 0
    retrS%codeFitParameters       = '  '
    retrS%codeIndexSurfAlb        = 0
    retrS%codeIndexSurfEmission   = 0
    retrS%codeIndexLambCldAlb     = 0
    retrS%codeIndexCloudFraction  = 0
    retrS%codeIndexMulOffset      = 0
    retrS%codeIndexStraylight     = 0

    ! initialization
    do iTrace = 1, nTrace
      if ( traceGasS(iTrace)%fitProfile )  then
        startValue = 1 + counter
        endValue   = startValue + traceGasS(iTrace)%nalt
        if ( trim(traceGasS(iTrace)%nameTraceGas) == 'trop_O3' ) then
          counter = endvalue - 2
        else
          counter    = endValue
        end if
        traceGasS(iTrace)%nprof = counter - 1 ! profile index starts at 0 , not 1
        counter_Sa_not_diag = counter
        retrS%codeFitParameters(startValue:endValue) = 'nodeTrace'     
        retrS%codeTraceGas(startValue:endValue)      = iTrace
        do ialt = 0, traceGasS(iTrace)%nalt
          retrS%codeAltitude(startValue+ialt)        = ialt
        end do ! ialt
      end if ! traceGasS(iTrace)%fitProfile
    end do ! iTrace

    if ( cloudAerosolRTMgridS%useAlbedoLambSurfAllBands ) then

      if ( cloudAerosolRTMgridS%fitAlbedoLambSurfAllBands ) then
        counter = counter + 1
        retrS%codeFitParameters(counter) = 'surfAlbedo'
        retrS%codeSpecBand(counter)      = 1
      end if

    else

      do iband = 1, numSpectrBands
        if ( surfaceS(iband)%fitAlbedo ) then
          do index = 1,  surfaceS(iband)%nwavelAlbedo
            retrS%codeIndexSurfAlb    (counter+index) = index
            retrS%codeFitParameters   (counter+index) = 'surfAlbedo'
            retrS%codeSpecBand        (counter+index) = iband
          end do
          counter = counter + surfaceS(iband)%nwavelAlbedo

          if (surfaceS(iband)%ncorrelationRow > 1) counter_Sa_not_diag = counter
        end if
      end do

    end if

    ncolumnfit = 0
    do iTrace = 1, nTrace
      if ( traceGasS(iTrace)%fitColumnTrace ) then
        counter = counter + 1
        ncolumnfit = ncolumnfit + 1
        retrS%codeFitParameters(counter)    = 'columnTrace'
        retrS%codeTraceGas(counter)         = iTrace  
      end if
    end do
    retrS%ncolumnfit = ncolumnfit

    if ( gasPTS%fitTemperatureProfile ) then
      startValue = 1 + counter
      endValue   = startValue +gasPTS%npressureNodes
      counter    = endValue
      retrS%codeFitParameters(startValue:endValue) = 'nodeTemp'
      do ialt = 0, gasPTS%npressureNodes
        retrS%codeAltitude(startValue+ialt)        = ialt
      end do
    end if

    if ( gasPTS%fitTemperatureOffset ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'offsetTemp'     
    end if

    if ( surfaceS(1)%fitSurfacePressure ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'surfPressure'     
    end if

    if ( cloudAerosolRTMgridS%useAlbedoLambCldAllBands ) then

      if ( cloudAerosolRTMgridS%fitAlbedoLambCldAllBands ) then
        counter = counter + 1
        retrS%codeFitParameters(counter) = 'LambCldAlbedo'
        retrS%codeSpecBand(counter)      = 1
      end if

    else

      do iband = 1, numSpectrBands
        if ( LambCloudS(iband)%fitAlbedo ) then
          do index = 1,  LambCloudS(iband)%nwavelAlbedo
            retrS%codeIndexLambCldAlb (counter+index) = index
            retrS%codeFitParameters   (counter+index) = 'LambCldAlbedo'
            retrS%codeSpecBand        (counter+index) = iband
          end do
          counter = counter + LambCloudS(iband)%nwavelAlbedo
        end if
      end do

    end if

    do iband = 1, numSpectrBands
      if ( surfaceS(iband)%fitEmission ) then
        do index = 1,  surfaceS(iband)%nwavelEmission
          retrS%codeIndexSurfEmission(counter+index) = index
          retrS%codeFitParameters    (counter+index) = 'surfEmission'
          retrS%codeSpecBand         (counter+index) = iband
        end do
        counter = counter + surfaceS(iband)%nwavelEmission
      end if
    end do

    do iband = 1, numSpectrBands
      if ( mulOffsetS(iband)%fitMulOffset ) then
        do index = 1,  mulOffsetS(iband)%nwavel
          retrS%codeIndexmulOffset (counter+index) = index
          retrS%codeFitParameters  (counter+index) = 'mulOffset'
          retrS%codeSpecBand       (counter+index) = iband
        end do
        counter = counter + mulOffsetS(iband)%nwavel
      end if
    end do

    do iband = 1, numSpectrBands
      if ( strayLightS(iband)%fitStrayLight ) then
        do index = 1,  strayLightS(iband)%nwavel
          retrS%codeIndexStraylight (counter+index) = index
          retrS%codeFitParameters   (counter+index) = 'straylight'
          retrS%codeSpecBand        (counter+index) = iband
        end do
        counter = counter + strayLightS(iband)%nwavel
      end if
    end do

    do iband = 1, numSpectrBands
      if ( RRS_RingS(iband)%fitDiffRingSpec ) then
        counter = counter + 1
        retrS%codeFitParameters   (counter) = 'diffRingCoef'
        retrS%codeSpecBand        (counter) = iband
      end if
      if ( RRS_RingS(iband)%fitRingSpec ) then
        counter = counter + 1
        retrS%codeFitParameters   (counter) = 'RingCoef'
        retrS%codeSpecBand        (counter) = iband
      end if
    end do ! iband

    if ( cloudAerosolRTMgridS%fitAerTau ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'aerosolTau'     
    end if

    if ( cloudAerosolRTMgridS%fitAerSSA ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'aerosolSSA'     
    end if

    if ( cloudAerosolRTMgridS%fitAerAC ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'aerosolAC'     
    end if

    if ( cloudAerosolRTMgridS%fitCldTau ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'cloudTau'     
    end if

    if ( cloudAerosolRTMgridS%fitCldAC ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'cloudAC'     
    end if

    if ( cloudAerosolRTMgridS%useCldAerFractionAllBands ) then

      if ( cloudAerosolRTMgridS%fitCldAerFractionAllBands ) then
        counter = counter + 1
        retrS%codeFitParameters(counter) = 'cloudFraction'
        retrS%codeSpecBand(counter)      = 1
      end if

    else

      do iband = 1, numSpectrBands
        if ( cldAerFractionS(iband)%fitCldAerFraction ) then
          do index = 1,  cldAerFractionS(iband)%nwavelCldAerFraction
            retrS%codeIndexCloudFraction (counter+index) = index
            retrS%codeFitParameters      (counter+index) = 'cloudFraction'
            retrS%codeSpecBand           (counter+index) = iband
          end do
          counter = counter + cldAerFractionS(iband)%nwavelCldAerFraction
        end if
      end do

    end if

    if ( cloudAerosolRTMgridS%fitIntervalDP ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'intervalDP'
    end if

    if ( cloudAerosolRTMgridS%fitIntervalTop ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'intervalTop'     
    end if

    if ( cloudAerosolRTMgridS%fitIntervalBot ) then
      counter = counter + 1
      retrS%codeFitParameters(counter) = 'intervalBot'     
    end if

    retrS%nstate       = counter
    retrS%nSa_not_diag = counter_Sa_not_diag

    if ( (counter == 0) .and. (.not. controlS%simulationOnly)) then
      call logDebug('ERROR in configuration file')
      call logDebug('no parameters are fitted in retrieval')
      call logDebug('convergence is not possible')
      call logDebug('you might want to use simulationOnly = 1')
      call logDebug('in subsection overall in SECTION GENERAL ')
      call logDebug('calculation is stopped')
      call mystop(errS, 'stopped because no parameter is fitted')
      if (errorCheck(errS)) return
    end if

    if ( verbose ) then
      write(intermediateFileUnit,'(A)') 'codeFitParameters  #TraceGas  #altitude indexSurfAlb  ' // &
                                        'indexMulOffset   indexStrayLight   spectralBand'
      do istate = 1, retrS%nstate
        write(intermediateFileUnit,'(A15,3I10,3I16)') retrS%codeFitParameters(istate),           &
                             retrS%codeTraceGas(istate), retrS%codeAltitude(istate),             &
                             retrS%codeIndexSurfAlb(istate), retrS%codeIndexStraylight(istate),  &
                             retrS%codeSpecBand(istate)
      end do
    end if

  end subroutine fillCodesFitParameters


  subroutine calculate_Se (errS, numSpectrBands, wavelInstrS, calibErrorReflS, retrS)

    ! calculate the error covariance matrix for the reflectance (sun-normalized radiance)
    ! including calibration errors

    type(errorType),           intent(inout) :: errS
    integer,                   intent(in)    :: numSpectrBands                  ! number of spectral bands
    type(wavelInstrType),      intent(in)    :: wavelInstrS(numSpectrBands)     ! wavelength grid for (simulated) measurements
    type(calibrReflType),      intent(in)    :: calibErrorReflS(numSpectrBands) ! (numSpectrBands) calibration error reflectance
    type(retrType),            intent(inout) :: retrS                           ! retrieval parameters

    integer   :: iband , jband, iwave, jwave, i, j
    real(8)   :: absDist                            ! wavelength distance for correlation in Se
    logical, parameter :: verbose = .false.

    ! matrices for svd
    real(8) :: w(retrS%nwavelRetr)
    real(8) :: ue(retrS%nwavelRetr,retrS%nwavelRetr), ueT(retrS%nwavelRetr,retrS%nwavelRetr)
    real(8) :: v(retrS%nwavelRetr,retrS%nwavelRetr)

    real(8) :: diagInvSe(retrS%nwavelRetr, retrS%nwavelRetr)
    real(8) :: diagsqrtSe(retrS%nwavelRetr, retrS%nwavelRetr)
    real(8) :: diagsqrtInvSe(retrS%nwavelRetr, retrS%nwavelRetr)

    ! use the correlation length to fill the non-diagonal part
    ! the correlation length is given for a spectral band
    ! first the multiplicative part
    j = 1  ! wavelength counter
    retrS%Se = 0.0d0  
    do jband = 1, numSpectrBands
      do jwave = 1, wavelInstrS(jband)%nwavel
        i = 1  ! wavelength counter
        do iband = 1, numSpectrBands
          do iwave = 1, wavelInstrS(iband)%nwavel
            ! ignore correlation between different spectral bands
            if ( iband == jband ) then
              absDist = abs(  wavelInstrS(jband)%wavel(jwave) - wavelInstrS(iband)%wavel(iwave) )
              retrS%Se(i, j) = exp(- absDist / calibErrorReflS(iband)%corrLengthMul )  &
                             * retrS%reflCalibErrorMul(i) * retrS%reflCalibErrorMul(j)
            end if ! iband == jband
            i = i + 1
          end do ! iwave
        end do ! iband
        j = j + 1
      end do ! jwave
    end do ! jband 

    ! then add the additive part
    j = 1  ! wavelength counter
    do jband = 1, numSpectrBands
      do jwave = 1, wavelInstrS(jband)%nwavel
        i = 1  ! wavelength counter
        do iband = 1, numSpectrBands
          do iwave = 1, wavelInstrS(iband)%nwavel
            ! ignore correlation between different spectral bands
            if ( iband == jband ) then
              absDist = abs(  wavelInstrS(jband)%wavel(jwave) - wavelInstrS(iband)%wavel(iwave) )
              retrS%Se(i, j) = retrS%Se(i, j) + exp(- absDist / calibErrorReflS(iband)%corrLengthAdd )  &
                             * retrS%reflCalibErrorAdd(i) * retrS%reflCalibErrorAdd(j)
            end if ! iband == jband
            i = i + 1
          end do ! iwave
        end do ! iband
        j = j + 1
      end do ! jwave
    end do ! jband 

    ! then add the noise part to the diagonal
    do iwave = 1, retrS%nwavelRetr
      retrS%Se(iwave, iwave) = retrS%Se(iwave, iwave) + retrS%reflNoiseError(iwave)**2
! JdH Debug  make noise error constant
!      retrS%Se(iwave, iwave) = retrS%Se(iwave, iwave) + retrS%reflNoiseError(1)**2
    end do

    ! calculate square root and inverse of Se
    diagInvSe     = 0.0d0
    diagsqrtSe    = 0.0d0
    diagsqrtInvSe = 0.0d0
    ue = retrS%Se
    ! svdcmp overwrites first array (input) with u
    call svdcmp(errS, ue, w, v, retrS%nwavelRetr)
    if (errorCheck(errS)) return
    ueT = transpose(ue)

    do iwave = 1, retrS%nwavelRetr
      diagsqrtSe(iwave,iwave)    = sqrt(w(iwave))
      if ( abs( w(iwave) ) > 1.0D-15 ) then
        diagInvSe(iwave,iwave)     = 1.0d0 / w(iwave)
        diagsqrtInvSe(iwave,iwave) = 1.0d0 / diagsqrtSe(iwave,iwave)
      else
        diagInvSe(iwave,iwave)     = 0.0d0
        diagsqrtInvSe(iwave,iwave) = 0.0d0
      end if
    end do

    retrS%sqrtInvSe = matmul(v, matmul(diagsqrtInvSe, ueT) )
    retrS%sqrtSe    = matmul(v, matmul(diagsqrtSe   , ueT) )
    retrS%InvSe     = matmul(v, matmul(diagInvSe    , ueT) )

    if ( verbose ) then
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'output from calculate_Se'
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'additive calibration error'
      write(intermediateFileUnit,'(200E14.5)') (retrS%reflCalibErrorAdd(iwave), iwave = 1, retrS%nwavelRetr )
      write(intermediateFileUnit,*) 'multiplicative calibration error'
      write(intermediateFileUnit,'(200E14.5)') (retrS%reflCalibErrorMul(iwave), iwave = 1, retrS%nwavelRetr )
      write(intermediateFileUnit,*) 'singular eigenvalues w'
      write(intermediateFileUnit,'(200E14.5)') (w(iwave), iwave = 1, retrS%nwavelRetr )
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'Se'
      do iwave = 1, retrS%nwavelRetr
        write(intermediateFileUnit,'(200E14.5)') (retrS%Se(iwave,jwave), jwave = 1, retrS%nwavelRetr )
      end do
      write(intermediateFileUnit,*) 'sqrtSe'
      do iwave = 1, retrS%nwavelRetr
        write(intermediateFileUnit,'(200E14.5)') (retrS%sqrtSe(iwave,jwave), jwave = 1, retrS%nwavelRetr )
      end do
      write(intermediateFileUnit,*) 'InvSe'
      do iwave = 1, retrS%nwavelRetr
        write(intermediateFileUnit,'(200E14.5)') (retrS%InvSe(iwave,jwave), jwave = 1, retrS%nwavelRetr )
      end do
      write(intermediateFileUnit,*) 'sqrtInvSe'
      do iwave = 1, retrS%nwavelRetr
        write(intermediateFileUnit,'(200E14.5)') (retrS%sqrtInvSe(iwave,jwave), jwave = 1, retrS%nwavelRetr )
      end do
      write(intermediateFileUnit,*) 'SeInvSe'
      v = matmul(retrS%Se, retrS%InvSe)
      do iwave = 1, retrS%nwavelRetr
        write(intermediateFileUnit,'(200E14.5)') (v(iwave,jwave), jwave = 1, retrS%nwavelRetr )
      end do
    end if

  end subroutine calculate_Se


  subroutine updateFitParameters(errS, numSpectrBands, nTrace, retrS, gasPTS, traceGasS, surfaceS, &
                                 LambCloudS, mieAerS, mieCldS, cldAerFractionS, mulOffsetS,  &
                                 strayLightS, RRS_RingS, cloudAerosolRTMgridS, controlS)

    ! Write the newly calculated values of the state vector into the structures
    ! traceGasS, surfaceS, strayLightS and cloudAerosolRTMgridS, that describe the atmosphere
    ! stray light and surface properties, including those of cloud and aerosol. Radiative transfer
    ! calculations use the values in these structures as input and not the state
    ! vector values.
    ! 
    ! If boundaries are exceeded (for the surface albedo or cloud albedo when using Lambertian
    ! surfaces, they are set to the boundary values. Further, the state vector elements are also
    ! set to the boundary values as large values are not meaning full and may make the convergence test
    ! useless.

    implicit none

    type(errorType),               intent(inout) :: errS
    integer,                       intent(in)    :: numSpectrBands                  ! number of spectral bands
    integer,                       intent(in)    :: nTrace                          ! number of trace gases
    type(retrType),                intent(inout) :: retrS                           ! retrieval parameters
    type(gasPTType),               intent(inout) :: gasPTS                          ! pressure and temperature profiles
    type(traceGasType),            intent(inout) :: traceGasS(nTrace)               ! properties of the trace gases
    type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)        ! properties of the surface
    type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)      ! Lambertian cloud prop (wavel dep)
    type(mieScatType),             intent(inout) :: mieAerS(maxNumMieModels)        ! Mie scattering properties aerosol
    type(mieScatType),             intent(inout) :: mieCldS(maxNumMieModels)        ! Mie scattering properties cloud
    type(cldAerFractionType),      intent(inout) :: cldAerFractionS(numSpectrBands) ! cloud/aerosol fraction (wavel dep)
    type(mulOffsetType),           intent(inout) :: mulOffsetS(numSpectrBands)      ! multiplicative offset
    type(straylightType),          intent(inout) :: strayLightS(numSpectrBands)     ! stray light
    type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)       ! Ring parameters
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS            ! aerosol and cloud properties
    type(controlType),             intent(inout) :: controlS                        ! control structure

    ! local
    logical :: altitude_update_T, altitude_update_P
    integer :: istate, iband, imodel, index, iTrace, ialt, iTracePrev
    real(8) :: column, columnAboveCloud, maxColumn
    real(8) :: cloudOpticalThickness, cldAerFraction, aerosolSSA, aerosolOpticalThickness
    real(8) :: aerosolAC, cloudAC
    real(8) :: LambCldAlbedo, surfAlbedo, surfAlbedoMax, surfEmission
    real(8) :: altitudeTop, altitudeBot, altitudeSurf
    real(8) :: pressureTop, pressureBot, pressureSurf
    real(8) :: pressureDifference

    real(8), parameter :: DUToCm2 = 2.68668d16

    ! parameters for spline interpolation to calculate the updated pressure
    reaL(8) :: lnpressure(0:gasPTS%npressure)  ! interpolate on the logarithm of the pressure
    reaL(8) :: SDaltitude(0:gasPTS%npressure)  ! spline derivatives for the altitude
    integer :: status

    ! initialize
    altitude_update_T = .false.
    index             = 0
    column            = 0.0d0
    columnAboveCloud  = 0.0d0

    ! If the surface albedo is too large the cloud albedo can not be fitted.
    ! Determine here the largest value of the surface albedo

    surfAlbedoMax = 0.0d0
    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case('surfAlbedo')

          surfAlbedo = retrS%x(istate)
          if ( surfAlbedo > surfAlbedoMax ) surfAlbedoMax = surfAlbedo

      end select

    end do ! istate

    if ( surfAlbedoMax > surfaceAlbBlockUpdateCldAlb ) then
      LambCloudS(:)%updateAlbedo = .false.
    else
      LambCloudS(:)%updateAlbedo = .true.
    end if


    ! loop over al the state vector elements and update the relevant values
    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case( 'nodeTrace' )

          ! natural logarithm of the volume mixing ratio is fitted
          iTrace = retrS%codeTraceGas(istate)
          ialt   = retrS%codeAltitude(istate)
          traceGasS(iTrace)%vmr(ialt) = exp( retrS%x(istate) )
          traceGasS(iTrace)%numDens(ialt) = &
              1.0d-6 * traceGasS(iTrace)%vmr(ialt) * traceGasS(iTrace)%numDensAir(ialt)
          ! set very wide values for the boundaries
          retrS%x_upperBound(istate) = 1.0D10
          retrS%x_lowerBound(istate) = -1.0D10

        case( 'columnTrace' )

          iTrace = retrS%codeTraceGas(istate)
          ! the column itself is fitted
          traceGasS(iTrace)%column = retrS%x(istate)

          maxColumn = 1.D100 ! initialize to a large value; unit is molecules cm-2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'O3' )        maxColumn = maxColO3     * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'trop_O3' )   maxColumn = maxColO3     * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'strat_O3' )  maxColumn = maxColO3     * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'NO2' )       maxColumn = maxColNO2    * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'trop_NO2' )  maxColumn = maxColNO2    * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'strat_NO2' ) maxColumn = maxColNO2    * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'SO2' )       maxColumn = maxColSO2    * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'HCHO' )      maxColumn = maxColHCHO   * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'BrO' )       maxColumn = maxColBrO    * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'CHOCHO' )    maxColumn = maxColCHOCHO * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'H2O' )       maxColumn = maxColH2O    * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'NH3' )       maxColumn = maxColNH3    * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'CO2' )       maxColumn = maxColCO2    * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'CH4' )       maxColumn = maxColCH4    * DUToCm2
          if ( trim(traceGasS(iTrace)%nameTraceGas) == 'CO' )        maxColumn = maxColCO     * DUToCm2

          if ( traceGasS(iTrace)%column > maxColumn ) traceGasS(iTrace)%column = maxColumn
          if ( traceGasS(iTrace)%column < maxColumn * 1.0d-6 ) traceGasS(iTrace)%column = maxColumn * 1.0d-6
          retrS%x(istate) = traceGasS(iTrace)%column 

          ! as the column has changed, update the profile by scaling the profile
          traceGasS(iTrace)%numDens = traceGasS(iTrace)%numDensAP &
              * traceGasS(iTrace)%column / traceGasS(iTrace)%columnAP
          traceGasS(iTrace)%vmr     = traceGasS(iTrace)%vmrAP     &
              * traceGasS(iTrace)%column / traceGasS(iTrace)%columnAP

          retrS%x_upperBound(istate) = maxColumn
          retrS%x_lowerBound(istate) = - maxColumn / 10.0d0

        case('nodeTemp')

          ialt   = retrS%codeAltitude(istate)

          gasPTS%temperatureNodes(ialt) = retrS%x(istate)

          if( gasPTS%temperatureNodes(ialt) < minTemperature ) gasPTS%temperatureNodes(ialt) = minTemperature
          if( gasPTS%temperatureNodes(ialt) > maxTemperature ) gasPTS%temperatureNodes(ialt) = maxTemperature
          retrS%x(istate) = gasPTS%temperatureNodes(ialt)
          altitude_update_T = .true.

          retrS%x_upperBound(istate) = maxTemperature
          retrS%x_lowerBound(istate) = minTemperature

        case('offsetTemp')

          gasPTS%temperatureOffset = retrS%x(istate)

          if( gasPTS%temperatureOffset >  maxTempOffset ) gasPTS%temperatureOffset =  maxTempOffset
          if( gasPTS%temperatureOffset < -maxTempOffset ) gasPTS%temperatureOffset = -maxTempOffset
          retrS%x(istate) = gasPTS%temperatureOffset

          ! change the temperatures profile using the offset
          gasPTS%temperatureNodes(:) = gasPTS%temperatureNodesAP(:) + gasPTS%temperatureOffset
          altitude_update_T = .true.

          retrS%x_upperBound(istate) = 100.0d0
          retrS%x_lowerBound(istate) = - 100.0d0

        case('surfPressure')

          pressureSurf = retrS%x(istate)

          if ( pressureSurf > maxPresSurface ) pressureSurf = maxPresSurface
          if ( pressureSurf < minPresSurface ) pressureSurf = minPresSurface

          ! top pressure of the first interval
          pressureTop = cloudAerosolRTMgridS%intervalBounds_P(1)
          
          ! demand that the pressure ratio of the first interval is less than maxPressureRatio
          if ( pressureTop / pressureSurf > maxPressureRatio ) then
            pressureSurf = pressureTop / maxPressureRatio
          end if

          surfaceS(:)%pressure                     = pressureSurf
          cloudAerosolRTMgridS%intervalBounds_P(0) = pressureSurf

          ! use spline interpolation to update the altitudes
          lnpressure  = log(gasPTS%pressure)
          call spline(errS, lnpressure, gasPTS%alt, SDaltitude, status)
          if (errorCheck(errS)) return
          altitudeSurf = splint(errS, lnpressure, gasPTS%alt, SDaltitude, log(pressureSurf), status)

          cloudAerosolRTMgridS%intervalBounds(0) = altitudeSurf
          surfaceS(:)%altitude = altitudeSurf

          ! update state vector because it has to correspond to the atmospheric parameters
          retrS%x(istate) = pressureSurf

          retrS%x_upperBound(istate) = maxPresSurface
          retrS%x_lowerBound(istate) = minPresSurface

        case('surfAlbedo')

          surfAlbedo = retrS%x(istate)

          ! test on limits for surface albedo except when the AAI or ozone profile is calculated
          if ( .not. controlS%allowNegativeSurfAlbedo ) then
            if ( surfAlbedo > maxSurfAlbedo ) surfAlbedo = maxSurfAlbedo
            if ( surfAlbedo < minSurfAlbedo ) surfAlbedo = minSurfAlbedo
          end if
          retrS%x(istate) = surfAlbedo

          if ( cloudAerosolRTMgridS%useAlbedoLambSurfAllBands ) then
            cloudAerosolRTMgridS%albedoLambSurfAllBands = surfAlbedo
          else
            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexSurfAlb(istate)
            surfaceS(iband)%albedo(index) = surfAlbedo
          end if ! cloudAerosolRTMgridS%fitAlbedoLambSurfAllBands

          retrS%x_upperBound(istate) = maxSurfAlbedo
          retrS%x_lowerBound(istate) = minSurfAlbedo

        case('surfEmission')

          surfEmission = retrS%x(istate)
          if ( surfEmission > maxSurfEmission ) surfEmission = maxSurfEmission
          if ( surfEmission < minSurfEmission ) surfEmission = minSurfEmission
          retrS%x(istate) = surfEmission

          iband = retrS%codeSpecBand(istate)
          index = retrS%codeIndexSurfEmission(istate)
          surfaceS(iband)%emission(index) = surfEmission

          retrS%x_upperBound(istate) = maxSurfEmission
          retrS%x_lowerBound(istate) = minSurfEmission

        case('LambCldAlbedo')

          iband = retrS%codeSpecBand(istate)

          if ( cloudAerosolRTMgridS%useAlbedoLambCldAllBands ) then

            if ( LambCloudS(iband)%updateAlbedo ) then

              LambCldAlbedo = retrS%x(istate)

              if ( LambCldAlbedo > maxCldAlbedo ) LambCldAlbedo = maxCldAlbedo
              if ( LambCldAlbedo < minCldAlbedo ) LambCldAlbedo = minCldAlbedo
              cloudAerosolRTMgridS%albedoLambCldAllBands = LambCldAlbedo
              retrS%x(istate) = LambCldAlbedo

            else

              retrS%x(istate) = cloudAerosolRTMgridS%albedoLambCldAllBandsAP   ! a priori value

            end if ! LambCloudS(iband)%updateAlbedo

          else
            
            index = retrS%codeIndexLambCldAlb(istate)

            if ( LambCloudS(iband)%updateAlbedo ) then

              LambCldAlbedo = retrS%x(istate)

              if ( LambCldAlbedo > maxCldAlbedo ) LambCldAlbedo = maxCldAlbedo
              if ( LambCldAlbedo < minCldAlbedo ) LambCldAlbedo = minCldAlbedo 
              LambCloudS(iband)%albedo(index) = LambCldAlbedo
              retrS%x(istate) = LambCldAlbedo

            else

              LambCloudS(iband)%albedo(index) = LambCloudS(iband)%albedoAP(index)  ! a priori value

            end if ! LambCloudS(iband)%updateAlbedo

          end if ! cloudAerosolRTMgridS%useAlbedoLambCldAllBands 

          retrS%x_upperBound(istate) = maxCldAlbedo
          retrS%x_lowerBound(istate) = minCldAlbedo

        case('mulOffset')

          ! polynomial coefficients are fitted
          iband = retrS%codeSpecBand(istate)
          index = retrS%codeIndexMulOffset(istate)
          mulOffsetS(iband)%percentOffsetPrev(index) = mulOffsetS(iband)%percentOffset(index)
          mulOffsetS(iband)%percentOffset(index)     = retrS%x(istate)

          retrS%x_upperBound(istate) = 100.0d0
          retrS%x_lowerBound(istate) = -100.0d0

        case('straylight')

          ! polynomial coefficients are fitted
          iband = retrS%codeSpecBand(istate)
          index = retrS%codeIndexStraylight(istate)
          strayLightS(iband)%percentStrayLightPrev(index) = strayLightS(iband)%percentStrayLight(index)
          ! It is assumed that the percentage stray light does not depend on the cloud fraction
          ! JdH: might need to be revised
          strayLightS(iband)%percentStrayLight(index)     = retrS%x(istate)

          retrS%x_upperBound(istate) = 100.0d0
          retrS%x_lowerBound(istate) = -100.0d0

        case('diffRingCoef')

          iband = retrS%codeSpecBand(istate)
          RRS_RingS(iband)%ringCoeff = retrS%x(istate)

          retrS%x_upperBound(istate) = 100.0d0
          retrS%x_lowerBound(istate) = -100.0d0

        case('RingCoef')

          iband = retrS%codeSpecBand(istate)
          RRS_RingS(iband)%ringCoeff = retrS%x(istate)

          retrS%x_upperBound(istate) = 100.0d0
          retrS%x_lowerBound(istate) = -100.0d0

        case('aerosolTau')

          aerosolOpticalThickness = retrS%x(istate)

          ! test boundaries and adjust values if required
          if ( aerosolOpticalThickness > maxOptThicknessAer ) aerosolOpticalThickness = maxOptThicknessAer
          if ( aerosolOpticalThickness < minOptThicknessAer ) aerosolOpticalThickness = minOptThicknessAer
          cloudAerosolRTMgridS%intervalAerTau(cloudAerosolRTMgridS%numIntervalFit) = aerosolOpticalThickness
          retrS%x(istate) = aerosolOpticalThickness

          if ( cloudAerosolRTMgridS%useMieScatAer ) then
            ! update the optical thickness for the components
            do imodel = 1, maxNumMieModels
              if ( mieAerS(imodel)%tauAP(cloudAerosolRTMgridS%numIntervalFit) > 1.0d-10 ) then
                mieAerS(imodel)%tau(cloudAerosolRTMgridS%numIntervalFit) =  &
                   mieAerS(imodel)%tauAP(cloudAerosolRTMgridS%numIntervalFit) &
                 * retrS%x(istate) / retrS%xa(istate)
              else
                ! a priori value for this component is very small
                ! if there is only one component then retrS%xa(istate) is also very small
                ! but if there is only one component, we should update it
                if ( retrS%xa(istate)  > 1.01d-10 ) then
                  ! more than one component
                  mieAerS(imodel)%tau(cloudAerosolRTMgridS%numIntervalFit) = 0.0d0
                else
                  ! assume these is just one one component
                  mieAerS(imodel)%tau(cloudAerosolRTMgridS%numIntervalFit) = retrS%x(istate)
                end if
              end if
            end do
          end if

          retrS%x_upperBound(istate) = maxOptThicknessAer
          retrS%x_lowerBound(istate) = minOptThicknessAer

        case('aerosolSSA')

          ! single scattering albedo itself is fitted, not the natural logarithm

          aerosolSSA = retrS%x(istate)

          if ( aerosolSSA > maxSSA ) aerosolSSA = maxSSA
          if ( aerosolSSA < minSSA ) aerosolSSA = minSSA
          cloudAerosolRTMgridS%intervalAerSSA(cloudAerosolRTMgridS%numIntervalFit) = aerosolSSA
          retrS%x(istate) = aerosolSSA

          retrS%x_upperBound(istate) = maxSSA
          retrS%x_lowerBound(istate) = minSSA

        case('aerosolAC')

          ! angstrom coefficient itself is fitted, not the natural logarithm

          aerosolAC = retrS%x(istate)

          if ( aerosolAC > maxAngstromCoef ) aerosolAC = maxAngstromCoef
          if ( aerosolAC < minAngstromCoef ) aerosolAC = minAngstromCoef
          cloudAerosolRTMgridS%intervalAerAC(cloudAerosolRTMgridS%numIntervalFit) = aerosolAC
          retrS%x(istate) = aerosolAC

          retrS%x_upperBound(istate) = maxAngstromCoef
          retrS%x_lowerBound(istate) = minAngstromCoef

        case('cloudTau')

          if ( cloudAerosolRTMgridS%fitLnCldTau ) then

            ! natural logarithm of the cloud optical thickness is fitted

            cloudOpticalThickness = exp( retrS%x(istate) )
            if ( cloudOpticalThickness > maxOptThicknessCld )   cloudOpticalThickness = maxOptThicknessCld
            if ( cloudOpticalThickness < 1.0d-5 )   cloudOpticalThickness = 1.0d-5
            cloudAerosolRTMgridS%intervalCldTau(cloudAerosolRTMgridS%numIntervalFit) = cloudOpticalThickness

            retrS%x(istate) = log( cloudOpticalThickness )

            if ( cloudAerosolRTMgridS%useMieScatCld ) then
              ! update the optical thickness for the components
              do imodel = 1, maxNumMieModels
                mieCldS(imodel)%tau(cloudAerosolRTMgridS%numIntervalFit) =  &
                   exp( retrS%x(istate) - retrS%xPrev(istate) )             &
                 * mieCldS(imodel)%tau(cloudAerosolRTMgridS%numIntervalFit)
              end do
            end if

            retrS%x_upperBound(istate) = log( maxOptThicknessCld )
            retrS%x_lowerBound(istate) = log( 1.0d-5 )
            
          else

            ! cloud optical thickness itself is fitted

            cloudOpticalThickness = retrS%x(istate)
            if ( cloudOpticalThickness > maxOptThicknessCld )   cloudOpticalThickness = maxOptThicknessCld
            if ( cloudOpticalThickness < minOptThicknessCld )   cloudOpticalThickness = minOptThicknessCld
            cloudAerosolRTMgridS%intervalCldTau(cloudAerosolRTMgridS%numIntervalFit) = cloudOpticalThickness
            retrS%x(istate) = cloudOpticalThickness

            if ( cloudAerosolRTMgridS%useMieScatCld ) then
              ! update the optical thickness for the components
              do imodel = 1, maxNumMieModels
                if ( mieCldS(imodel)%tauAP(cloudAerosolRTMgridS%numIntervalFit) > 1.0d-10 ) then
                  mieCldS(imodel)%tau(cloudAerosolRTMgridS%numIntervalFit) =  &
                     mieCldS(imodel)%tauAP(cloudAerosolRTMgridS%numIntervalFit) &
                   * retrS%x(istate) / retrS%xa(istate)
                else
                  ! a priori value for this component is very small
                  ! if there is only one component then retrS%xa(istate) is also very small
                  ! but if there is only one component, we should update it
                  if ( retrS%xa(istate)  > 1.01d-10 ) then
                    ! more than one component
                    mieCldS(imodel)%tau(cloudAerosolRTMgridS%numIntervalFit) = 0.0d0
                  else
                    ! assume these is just one one component
                    mieCldS(imodel)%tau(cloudAerosolRTMgridS%numIntervalFit) = retrS%x(istate)
                  end if
                end if
              end do
            end if

            retrS%x_upperBound(istate) = maxOptThicknessCld
            retrS%x_lowerBound(istate) = 1.0d-5

          end if ! cloudAerosolRTMgridS%fitLnCldTau

        case('cloudAC')

          ! angstrom coefficient itself is fitted, not the natural logarithm

          cloudAC = retrS%x(istate)

          if ( cloudAC > maxAngstromCoef ) cloudAC = maxAngstromCoef
          if ( cloudAC < minAngstromCoef ) cloudAC = minAngstromCoef
          cloudAerosolRTMgridS%intervalCldAC(cloudAerosolRTMgridS%numIntervalFit) = cloudAC
          retrS%x(istate) = cloudAC

          retrS%x_upperBound(istate) = maxAngstromCoef
          retrS%x_lowerBound(istate) = minAngstromCoef

        case('cloudFraction')

          iband = retrS%codeSpecBand(istate)

          cldAerFraction = retrS%x(istate)
          if ( cldAerFraction < minCldFraction ) cldAerFraction = minCldFraction
          if ( cldAerFraction > maxCldFraction ) cldAerFraction = maxCldFraction

          if ( cloudAerosolRTMgridS%useCldAerFractionAllBands ) then
            cloudAerosolRTMgridS%CldAerFractionAllBands = cldAerFraction
          else
            index = retrS%codeIndexCloudFraction(istate)
            cldAerFractionS(iband)%cldAerFraction(index) = cldAerFraction
          end if

          retrS%x(istate) = cldAerFraction

          retrS%x_upperBound(istate) = maxCldFraction
          retrS%x_lowerBound(istate) = minCldFraction

        case('intervalDP')

          altitude_update_P = .false.

          altitudeTop = retrS%x(istate)

          ! limit altitude
          if ( altitudeTop > maxAltTopFitInterval ) altitudeTop = maxAltTopFitInterval

          ! If the estimate for the new top altitude is less than the lower boundary of the layer below is
          ! set the top altitude to 200 m above the lower boundary of the layer below is. The main reason for doing
          ! this is that spline interpolation used to determine the pressure should not become extrapolation.
          ! After this further tests are used to estimate new pressure levels.
          if ( altitudeTop < cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit - 2) ) &
            altitudeTop = cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit - 2) + 0.2d0

          ! use spline interpolation to update the pressure at the top
          lnpressure  = log(gasPTS%pressure)
          call spline(errS, gasPTS%alt, lnpressure, SDaltitude, status)
          if (errorCheck(errS)) return
          pressureTop = exp( splint(errS, gasPTS%alt, lnpressure, SDaltitude, altitudeTop, status) )

          pressureDifference = cloudAerosolRTMgridS%intervalBoundsAP_P(cloudAerosolRTMgridS%numIntervalFit - 1) &
                             - cloudAerosolRTMgridS%intervalBoundsAP_P(cloudAerosolRTMgridS%numIntervalFit)
          pressureBot        = pressureTop + pressureDifference

          ! demand that the pressure ratio for the interval above the fit interval < maxPressureRatio
          if ( cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit+1) / pressureTop  &
                                                                                   > maxPressureRatio ) then
            pressureTop = cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit+1) / maxPressureRatio
            altitude_update_P = .true.
          end if

          ! demand that the pressure ratio for the interval below the fit interval < maxPressureRatio
          if ( pressureBot / cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit-2) &
                                                                                  > maxPressureRatio ) then
            pressureBot = cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit-2) &
                        * maxPressureRatio
            pressureTop = pressureBot - pressureDifference
            altitude_update_P = .true.
          end if

          ! use spline interpolation to update the altitudes
          call spline(errS, lnpressure, gasPTS%alt, SDaltitude, status)
          if (errorCheck(errS)) return
          if ( altitude_update_P ) then
            altitudeTop = splint(errS, lnpressure, gasPTS%alt, SDaltitude, log(pressureTop), status)
            altitudeBot = splint(errS, lnpressure, gasPTS%alt, SDaltitude, log(pressureBot), status)
          else
            altitudeBot = splint(errS, lnpressure, gasPTS%alt, SDaltitude, log(pressureBot), status)
          end if

          ! store updated values
          cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit)       = altitudeTop
          cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit - 1)   = altitudeBot
          cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit)     = pressureTop
          cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit - 1) = pressureBot
          LambCloudS(:)%altitude = altitudeTop
          LambCloudS(:)%pressure = pressureTop

          ! update state vector because it has to correspond to the atmospheric parameters
          retrS%x(istate)   = altitudeTop

          retrS%x_upperBound(istate) = maxAltTopFitInterval
          retrS%x_lowerBound(istate) = 0.0d0

        case('intervalTop')

          altitude_update_P = .false.

          altitudeTop = retrS%x(istate)

          ! limit altitude
          if ( altitudeTop > maxAltTopFitInterval ) altitudeTop = maxAltTopFitInterval

          ! use spline interpolation to update the pressure at the top
          lnpressure  = log(gasPTS%pressure)
          call spline(errS, gasPTS%alt, lnpressure, SDaltitude, status)
          if (errorCheck(errS)) return
          pressureTop = exp( splint(errS, gasPTS%alt, lnpressure, SDaltitude, altitudeTop, status) )

          ! demand that the pressure ratio for the interval above the fit interval < maxPressureRatio
          if ( cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit+1) / pressureTop &
                                                                                  > maxPressureRatio ) then
            pressureTop = cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit+1) &
                        / maxPressureRatio
            altitude_update_P = .true.
          end if

          ! get the pressure at the bottom of the interval
          pressureBot = cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit-1)

          ! demand that the pressure ratio of the interval is less than maxPressureRatio
          if ( pressureTop / pressureBot > maxPressureRatio ) then
            pressureTop = pressureBot * maxPressureRatio
            altitude_update_P = .true.
          end if

          ! use spline interpolation to update the altitudes
          if ( altitude_update_P ) then
            lnpressure  = log(gasPTS%pressure)
            call spline(errS, lnpressure, gasPTS%alt, SDaltitude, status)
            if (errorCheck(errS)) return
            altitudeTop = splint(errS, lnpressure, gasPTS%alt, SDaltitude, log(pressureTop), status)
          end if

          cloudAerosolRTMgridS%intervalBounds  (cloudAerosolRTMgridS%numIntervalFit) = altitudeTop
          cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit) = pressureTop
          LambCloudS(:)%altitude = altitudeTop
          LambCloudS(:)%pressure = pressureTop

          ! update state vector because it has to correspond to the atmospheric parameters
          retrS%x(istate) = altitudeTop

          retrS%x_upperBound(istate) = maxAltTopFitInterval
          retrS%x_lowerBound(istate) = 0.0d0

        case('intervalBot')

          altitude_update_P = .false.

          altitudeBot = retrS%x(istate)

          ! demand that the bottom altitude does not change more than one a-priori standard deviation
          ! if ( abs(altitudeBot - altitudeBotPrev) > stdDevBot ) then
          !   altitudeBot = altitudeBotPrev + sign(stdDevBot, altitudeBot - altitudeBotPrev )
          ! end if

          ! use spline interpolation to update the pressure at the bottom of the interval
          lnpressure  = log(gasPTS%pressure)
          call spline(errS, gasPTS%alt, lnpressure, SDaltitude, status)
          if (errorCheck(errS)) return
          pressureBot = exp( splint(errS, gasPTS%alt, lnpressure, SDaltitude, altitudeBot, status) )

          ! get the pressure at the top of the interval
          pressureTop = cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit)

          ! demand that the pressure ratio is not larger than maxPressureRatio
          if ( pressureTop / pressureBot  > maxPressureRatio ) then
            pressureBot = pressureTop * maxPressureRatio
            altitude_update_P = .true.
          end if

          ! demand that the pressure ratio for the interval below the fit inteval is less than maxPressureRatio
          if ( pressureBot / cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit-2) &
                                                                                       > maxPressureRatio ) then
            pressureBot = cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit-2) &
                        * maxPressureRatio
            altitude_update_P = .true.
          end if

          ! use spline interpolation to update the altitudes
          if ( altitude_update_P ) then
            lnpressure  = log(gasPTS%pressure)
            call spline(errS, lnpressure, gasPTS%alt, SDaltitude, status)
            if (errorCheck(errS)) return
            altitudeBot = splint(errS, lnpressure, gasPTS%alt, SDaltitude, log(pressureBot), status)
          end if

          cloudAerosolRTMgridS%intervalBounds  (cloudAerosolRTMgridS%numIntervalFit-1) = altitudeBot
          cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit-1) = pressureBot

          ! update state vector because it has to correspond to the atmospheric parameters
          retrS%x(istate) = altitudeBot

          retrS%x_upperBound(istate) = maxAltTopFitInterval
          retrS%x_lowerBound(istate) = 0.0d0

        case default
          call logDebug('in subroutine updateFitParameters in optimalEstimationModule')
          call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
          call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
          if (errorCheck(errS)) return
      end select

    end do ! istate

    if ( altitude_update_T )  then
      ! update the altitudes and the temperature in the traceGasS structure
      call update_altitudes_PTz_grid(errS, surfaceS(1), gasPTS)
      if (errorCheck(errS)) return
      call update_T_z_other_grids(errS, numSpectrBands, nTrace, surfaceS, gasPTS, traceGasS, &
                                  cloudAerosolRTMgridS, LambCloudS)
      if (errorCheck(errS)) return
      call update_ndens_vmr(errS, nTrace, gasPTS, traceGasS )
      if (errorCheck(errS)) return
    end if

    ! update the total column of the trace gas if the profile has changed
    ! or when the surface pressure has changed
    ! loop over the state vector elements
    ! calculate the column when istate differs from istatePrev
    ! otherwise we repeat the calculation 10 - 70 times
    ! it is assumed that the state vector elements for the profile of one trace gas
    ! are successive elements of the state vector.

    iTracePrev = 0  ! initialization

    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case( 'nodeTrace' )

          iTrace = retrS%codeTraceGas(istate)

          if ( iTrace /= iTracePrev )  then
            call calculateTotalColumn(errS, gasPTS, traceGasS(iTrace), cloudAerosolRTMgridS, &
                                      column, columnAboveCloud)
            if (errorCheck(errS)) return
            traceGasS(iTrace)%column           = column
            traceGasS(iTrace)%columnAboveCloud = columnAboveCloud
            iTracePrev = iTrace
          end if ! iTrace /= iTracePrev

        case( 'surfPressure' )

          do iTrace = 1, nTrace
            call calculateTotalColumn(errS, gasPTS, traceGasS(iTrace), cloudAerosolRTMgridS, &
                                      column, columnAboveCloud)
            if (errorCheck(errS)) return
            traceGasS(iTrace)%column           = column
            traceGasS(iTrace)%columnAboveCloud = columnAboveCloud
          end do

      end select

    end do ! istate

    ! special altitudes/pressures - only if the profile is fitted
    ! values at the surface, at the cloud and at the tropopause are calculated using interpolation
    do iTrace = 1, nTrace
      if ( traceGasS(iTrace)%fitProfile ) then
        traceGasS(iTrace)%vmrAtSurface    = splintLin(errS, traceGasS(iTrace)%pressure, traceGasS(iTrace)%vmr, &
                                                   surfaceS(1)%pressure, status)
        traceGasS(iTrace)%vmrAtCloud      = splintLin(errS, traceGasS(iTrace)%pressure, traceGasS(iTrace)%vmr, &
                                                   LambCloudS(1)%pressure, status)
        traceGasS(iTrace)%vmrAtTropopause = splintLin(errS, traceGasS(iTrace)%pressure, traceGasS(iTrace)%vmr, &
                                                      gasPTS%tropopausePressure, status)
      else
        traceGasS(iTrace)%vmrAtSurface    = 0.0d0
        traceGasS(iTrace)%vmrAtCloud      = 0.0d0
        traceGasS(iTrace)%vmrAtTropopause = 0.0d0
      end if 
    end do ! iTrace


  end subroutine updateFitParameters


  subroutine updateCovFitParameters(errS, numSpectrBands, nTrace, retrS, diagnosticS, traceGasS, surfaceS, &
                                    LambCloudS, cldAerFractionS, gasPTS, mulOffsetS, strayLightS,          &
                                    RRS_RingS, cloudAerosolRTMgridS)

    ! The a-posteriori errors are calculated after the solution has converged, or at least the
    ! iteration loop has ended. When this diagnostic information is available, one can update
    ! the errors in traceGasS, surfaceS, LambCloudS, cloudFractionS, gasPTS, strayLightS and cloudAerosolRTMgridS.

    ! May 14, 2008: Errors removed: S_vmr contains the error covariance for surface albedo,
    ! aerosol and cloud optical thickness, and cloud fraction, not the error for the logarithm
    ! of these quantities, which was the case in older versions of the code.

    implicit none

    type(errorType),               intent(inout) :: errS
    integer,                       intent(in)    :: numSpectrBands                  ! number of spectral bands
    integer,                       intent(in)    :: nTrace                          ! number of trace gases
    type(retrType),                intent(in)    :: retrS                           ! retrieval parameters
    type(diagnosticType),          intent(in)    :: diagnosticS                     ! diagnostic parameters
    type(traceGasType),            intent(inout) :: traceGasS(nTrace)               ! properties of the trace gases
    type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)        ! properties of the surface
    type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)      ! Lambertian cloud properties
    type(cldAerFractionType),      intent(inout) :: cldAerFractionS(numSpectrBands) ! cloud / aerosol fraction
    type(gasPTType),               intent(inout) :: gasPTS                          ! pressure and temperature profiles
    type(mulOffsetType),           intent(inout) :: mulOffsetS(numSpectrBands)      ! multiplicative offset
    type(straylightType),          intent(inout) :: strayLightS(numSpectrBands)     ! stray light
    type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)       ! Ring parameters
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS            ! aerosol and cloud properties

    ! local
    integer :: istate, jstate, ialt, jalt, iband, index, iTrace, jTrace
    integer :: status

    real(8) :: diag_cov_vmr(0:traceGasS(1)%nalt) ! if trace gas profile is fitted it is the first gas

    ! initialize (co)variances to their a-priori values
    do iTrace = 1, nTrace
      traceGasS(iTrace)%covVmr                      = traceGasS(iTrace)%covVmrAP
      traceGasS(iTrace)%covNumDens                  = traceGasS(iTrace)%covNumDensAP
    end do
    gasPTS%varianceTempOffset                       = gasPTS%varianceTempOffsetAP
    do iband = 1, numSpectrBands
      if ( associated ( surfaceS(iband)%varianceAlbedo ) ) &
      surfaceS(iband)%varianceAlbedo                = surfaceS(iband)%varianceAlbedoAP
    end do
    do iband = 1, numSpectrBands
      if ( associated ( surfaceS(iband)%varianceEmission ) ) &
      surfaceS(iband)%varianceEmission              = surfaceS(iband)%varianceEmissionAP
    end do
    do iband = 1, numSpectrBands
      if ( associated ( LambCloudS(iband)%varianceAlbedo ) ) &
      LambCloudS(iband)%varianceAlbedo              = LambCloudS(iband)%varianceAlbedoAP
    end do
    do iband = 1, numSpectrBands
      if ( associated ( cldAerFractionS(iband)%varianceCldAerFraction ) ) &
      cldAerFractionS(iband)%varianceCldAerFraction = cldAerFractionS(iband)%varianceCldAerFractionAP
    end do
    do iband = 1, numSpectrBands
      if ( associated ( mulOffsetS(iband)%varMulOffset ) ) &
      mulOffsetS(iband)%varMulOffset                =  mulOffsetS(iband)%varMulOffsetAP
    end do
    do iband = 1, numSpectrBands
      if ( associated ( strayLightS(iband)%varStrayLight ) ) &
      strayLightS(iband)%varStrayLight              =  strayLightS(iband)%varStrayLightAP
    end do
    cloudAerosolRTMgridS%varAerTau                  = cloudAerosolRTMgridS%varAerTauAP
    cloudAerosolRTMgridS%varCldAerFractionAllBands  = cloudAerosolRTMgridS%varCldAerFractionAllBandsAP
    cloudAerosolRTMgridS%varCldTau                  = cloudAerosolRTMgridS%varCldTauAP
    cloudAerosolRTMgridS%varIntervalBounds          = cloudAerosolRTMgridS%varIntervalBoundsAP
    cloudAerosolRTMgridS%varIntervalBounds_P        = cloudAerosolRTMgridS%varIntervalBoundsAP_P
    cloudAerosolRTMgridS%varAlbedoLambSurfAll       = cloudAerosolRTMgridS%varAlbedoLambSurfAllAP
    cloudAerosolRTMgridS%varAlbedoLambCldAll        = cloudAerosolRTMgridS%varAlbedoLambCldAllAP
    cloudAerosolRTMgridS%varAlbedoLambAerAll        = cloudAerosolRTMgridS%varAlbedoLambAerAllAP

    ! loop over al the state vector elements and update the relevant values

    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case('nodeTrace')

          iTrace = retrS%codeTraceGas(istate)
          ialt   = retrS%codeAltitude(iState)
          do jstate= 1, retrS%nstate
            select case (retrS%codeFitParameters(jstate))
              case('nodeTrace')
                jTrace = retrS%codeTraceGas(jstate)
                jalt   = retrS%codeAltitude(jstate)
                if ( iTrace == jTrace ) then
                  traceGasS(iTrace)%covVmr(ialt,jalt)     = diagnosticS%S_vmr  (istate,jstate)
                  traceGasS(iTrace)%covNumDens(ialt,jalt) = diagnosticS%S_ndens(istate,jstate)
                end if
            end select
          end do 

        case('columnTrace')

          iTrace = retrS%codeTraceGas(iState)
          traceGasS(iTrace)%covColumn = diagnosticS%S_ndens(istate,istate)

        case('nodeTemp')

          ialt   = retrS%codeAltitude(istate)
          do jstate= 1, retrS%nstate
            select case (retrS%codeFitParameters(jstate))
              case('nodeTemp')
                jalt   = retrS%codeAltitude(jstate)
                gasPTS%covTempNodes(ialt,jalt) = diagnosticS%S_vmr(istate,jstate)
            end select
          end do 

        case('offsetTemp')

          gasPTS%varianceTempOffset = diagnosticS%S_ndens(istate,istate)

        case('surfPressure')

          surfaceS(:)%varPressure = diagnosticS%S_vmr(istate,istate)

        case('surfAlbedo')

          if ( cloudAerosolRTMgridS%useAlbedoLambSurfAllBands ) then
            cloudAerosolRTMgridS%varAlbedoLambSurfAll = diagnosticS%S_vmr(istate,istate)
          else
            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexSurfAlb(istate)
            surfaceS(iband)%varianceAlbedo(index) = diagnosticS%S_vmr(istate,istate)
          end if 

        case('surfEmission')

            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexSurfEmission(istate)
            surfaceS(iband)%varianceEmission(index) = diagnosticS%S_vmr(istate,istate)

        case('LambCldAlbedo')

          iband = retrS%codeSpecBand(istate)

          if ( cloudAerosolRTMgridS%useAlbedoLambCldAllBands ) then
            if ( LambCloudS(iband)%updateAlbedo ) then
              cloudAerosolRTMgridS%varAlbedoLambCldAll = diagnosticS%S_vmr(istate,istate)
            else
              cloudAerosolRTMgridS%varAlbedoLambCldAll = 0.0d0  ! not fitted
            end if 
          else
            index = retrS%codeIndexLambCldAlb(istate)
            if ( LambCloudS(iband)%updateAlbedo ) then
              LambCloudS(iband)%varianceAlbedo(index) = diagnosticS%S_vmr(istate,istate)
            else
              LambCloudS(iband)%varianceAlbedo(index) = 0.0d0
            end if
          end if

        case('mulOffset')

          iband = retrS%codeSpecBand(istate)
          index = retrS%codeIndexMulOffset(istate)
          mulOffsetS(iband)%varMulOffset(index) = diagnosticS%S_vmr(istate,istate)

        case('straylight')

          iband = retrS%codeSpecBand(istate)
          index = retrS%codeIndexStraylight(istate)
          strayLightS(iband)%varStrayLight(index) = diagnosticS%S_vmr(istate,istate)

        case('diffRingCoef')

          iband = retrS%codeSpecBand(istate)
          RRS_RingS(iband)%varRingCoeff = diagnosticS%S_vmr  (istate, istate)

        case('RingCoef')

          iband = retrS%codeSpecBand(istate)
          RRS_RingS(iband)%varRingCoeff = diagnosticS%S_vmr  (istate, istate)

        case('aerosolTau')

          cloudAerosolRTMgridS%varAerTau = diagnosticS%S_vmr(istate,istate)

        case('aerosolSSA')

          cloudAerosolRTMgridS%varAerSSA = diagnosticS%S_vmr(istate,istate)

        case('aerosolAC')

          cloudAerosolRTMgridS%varAerAC = diagnosticS%S_vmr(istate,istate)

        case('cloudTau')

          cloudAerosolRTMgridS%varCldTau = diagnosticS%S_vmr(istate,istate)

        case('cloudAC')

          cloudAerosolRTMgridS%varCldAC = diagnosticS%S_vmr(istate,istate)

        case('cloudFraction')

          iband = retrS%codeSpecBand(istate)

          if ( cloudAerosolRTMgridS%useCldAerFractionAllBands ) then
            cloudAerosolRTMgridS%varCldAerFractionAllBands = diagnosticS%S_vmr(istate,istate)
          else
            index = retrS%codeIndexCloudFraction(istate)
            cldAerFractionS(iband)%varianceCldAerFraction(index) = diagnosticS%S_vmr(istate,istate)
          end if

        case('intervalDP')

           cloudAerosolRTMgridS%varIntervalBounds(cloudAerosolRTMgridS%numIntervalFit) = &
                diagnosticS%S_vmr(istate,istate)
           ! relative change in variance is the same for pressure and altitude
           cloudAerosolRTMgridS%varIntervalBounds_P(cloudAerosolRTMgridS%numIntervalFit) =     &
                cloudAerosolRTMgridS%varIntervalBounds    (cloudAerosolRTMgridS%numIntervalFit) &
              * cloudAerosolRTMgridS%varIntervalBoundsAP_P(cloudAerosolRTMgridS%numIntervalFit) &
              / cloudAerosolRTMgridS%varIntervalBoundsAP  (cloudAerosolRTMgridS%numIntervalFit)

        case('intervalTop')

           cloudAerosolRTMgridS%varIntervalBounds(cloudAerosolRTMgridS%numIntervalFit) = &
                diagnosticS%S_vmr(istate,istate)
           ! relative change in variance is the same for pressure and altitude
           cloudAerosolRTMgridS%varIntervalBounds_P(cloudAerosolRTMgridS%numIntervalFit) =     &
                cloudAerosolRTMgridS%varIntervalBounds    (cloudAerosolRTMgridS%numIntervalFit) &
              * cloudAerosolRTMgridS%varIntervalBoundsAP_P(cloudAerosolRTMgridS%numIntervalFit) &
              / cloudAerosolRTMgridS%varIntervalBoundsAP  (cloudAerosolRTMgridS%numIntervalFit)

        case('intervalBot')

            cloudAerosolRTMgridS%varIntervalBounds(cloudAerosolRTMgridS%numIntervalFit-1) =  &
                diagnosticS%S_vmr(istate,istate)
           ! relative change in variance is the same for pressure and altitude
           cloudAerosolRTMgridS%varIntervalBounds_P(cloudAerosolRTMgridS%numIntervalFit-1) =      &
                cloudAerosolRTMgridS%varIntervalBounds    (cloudAerosolRTMgridS%numIntervalFit-1) &
              * cloudAerosolRTMgridS%varIntervalBoundsAP_P(cloudAerosolRTMgridS%numIntervalFit-1) &
              / cloudAerosolRTMgridS%varIntervalBoundsAP  (cloudAerosolRTMgridS%numIntervalFit-1)

        case default

          call logDebug('in subroutine updateCovFitParameters in optimalEstimationModule')
          call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
          call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
          if (errorCheck(errS)) return

      end select

    end do ! istate

    ! special altitudes/pressures - only if profile is fitted
    ! for the surface and the cloud the first spectral band is used
    do iTrace = 1, nTrace
      if ( traceGasS(iTrace)%fitProfile .and. iTrace == 1) then
        do ialt = 0, traceGasS(1)%nalt
          diag_cov_vmr(ialt) = traceGasS(iTrace)%covVmr(ialt, ialt)
        end do
        traceGasS(iTrace)%vmrAtSurface_precision    = splintLin(errS, traceGasS(iTrace)%pressure, diag_cov_vmr, &
                                                                surfaceS(1)%pressure, status)
        traceGasS(iTrace)%vmrAtCloud_precision      = splintLin(errS, traceGasS(iTrace)%pressure, diag_cov_vmr, &
                                                                LambCloudS(1)%pressure, status)
        traceGasS(iTrace)%vmrAtTropopause_precision = splintLin(errS, traceGasS(iTrace)%pressure, diag_cov_vmr, &
                                                                gasPTS%tropopausePressure, status)
        traceGasS(iTrace)%vmrAtSurface_precision    = sqrt(traceGasS(iTrace)%vmrAtSurface_precision)
        traceGasS(iTrace)%vmrAtCloud_precision      = sqrt(traceGasS(iTrace)%vmrAtCloud_precision)
        traceGasS(iTrace)%vmrAtTropopause_precision = sqrt(traceGasS(iTrace)%vmrAtSurface_precision)
      else
        traceGasS(iTrace)%vmrAtSurface_precision    = 0.0d0
        traceGasS(iTrace)%vmrAtCloud_precision      = 0.0d0
        traceGasS(iTrace)%vmrAtTropopause_precision = 0.0d0
        traceGasS(iTrace)%vmrAtSurface_precision    = 0.0d0
        traceGasS(iTrace)%vmrAtCloud_precision      = 0.0d0
        traceGasS(iTrace)%vmrAtTropopause_precision = 0.0d0
      end if 
    end do ! iTrace


  end subroutine updateCovFitParameters


  subroutine fillAPriori(errS, numSpectrBands, nTrace, traceGasSimS, traceGasRetrS, surfaceSimS, surfaceRetrS, &
                         LambCloudSimS, LambCloudRetrS, cldAerFractionSimS, cldAerFractionRetrS,               &
                         gasPTSimS, gasPTRetrS, mulOffsetSimS, mulOffsetRetrS, strayLightSimS,                 &
                         strayLightRetrS, RRS_RingS, cloudAerosolRTMgridSimS, cloudAerosolRTMgridRetrS, retrS)

    ! fill the a-priori state vector and the a-priori covariance matrix
    ! fitting of the trace gas is done for the logarithm of the volume mixing ratio,
    ! but a-priori covariance matrices for the volume mixing ratio and the number density
    ! are also given.
                        
    implicit none

    type(errorType),               intent(inout) :: errS
    integer,                       intent(in)    :: numSpectrBands                      ! number of spectral bands
    integer,                       intent(in)    :: nTrace                              ! number of trace gases
    type(traceGasType),            intent(inout) :: traceGasSimS(nTrace)                ! trace gas properties simulation
    type(traceGasType),            intent(inout) :: traceGasRetrS(nTrace)               ! trace gas properties retrieval
    type(LambertianType),          intent(inout) :: surfaceSimS(numSpectrBands)         ! surface properties simulation
    type(LambertianType),          intent(inout) :: surfaceRetrS(numSpectrBands)        ! surface properties retrieval
    type(LambertianType),          intent(inout) :: LambCloudSimS(numSpectrBands)       ! cloud properties simulation
    type(LambertianType),          intent(inout) :: LambCloudRetrS(numSpectrBands)      ! cloud properties retrieval
    type(cldAerFractionType),      intent(inout) :: cldAerFractionSimS(numSpectrBands)  ! cloud / aerosol fraction sim
    type(cldAerFractionType),      intent(inout) :: cldAerFractionRetrS(numSpectrBands) ! cloud / aerosol fraction retr
    type(gasPTType),               intent(inout) :: gasPTSimS                           ! P T profiles for simulation
    type(gasPTType),               intent(inout) :: gasPTRetrS                          ! P T profiles for retrieval
    type(mulOffsetType),           intent(inout) :: mulOffsetSimS(numSpectrBands)       ! multiplicative offset
    type(mulOffsetType),           intent(inout) :: mulOffsetRetrS(numSpectrBands)      ! multiplicative offset
    type(straylightType),          intent(inout) :: strayLightSimS(numSpectrBands)      ! stray light for simulation
    type(straylightType),          intent(inout) :: strayLightRetrS(numSpectrBands)     ! stray light for retrieval
    type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)           ! Ring spectra
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridSimS             ! aerosol and cloud properties sim
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridRetrS            ! aerosol and cloud properties retr
    type(retrType),                intent(inout) :: retrS                               ! retrieval parameters

    ! local
    integer :: ialt, jalt
    integer :: istate, jstate
    integer :: iband, iTrace, jTrace, index, iwave, jwave

    ! parameters for interpolating the true profile to the retrieval grid
    real(8) :: tau, surfaceAlbedo, surfaceAlbedoMax, surfaceEmission, cloudAlbedo, aerSSA
    real(8) :: aerosolAC, cloudAC, cloudFraction, strayLight, mulOffset

    ! control output
    logical, parameter :: verbose = .false.

    retrS%Sa_lnvmr = 0.0d0
    retrS%Sa_vmr   = 0.0d0
    retrS%Sa_ndens = 0.0d0

    ! If the a prior surface albedo is too large the cloud albedo can not be fitted.
    ! Determine here the maximum of the surface albedo

    surfaceAlbedoMax = 0.0d0

    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case('surfAlbedo')

          if ( cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then
            surfaceAlbedo = cloudAerosolRTMgridRetrS%albedoLambSurfAllBandsAP
          else
            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexSurfAlb(istate)
            surfaceAlbedo = surfaceRetrS(iband)%albedoAP(index)
          end if
          if ( surfaceAlbedo > surfaceAlbedoMax ) surfaceAlbedoMax = surfaceAlbedo

      end select

    end do ! istate

    if ( surfaceAlbedoMax > surfaceAlbBlockUpdateCldAlb ) then
      LambCloudRetrS(:)%updateAlbedo = .false.
    else
      LambCloudRetrS(:)%updateAlbedo = .true.
    end if

    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case('nodeTrace')

          iTrace = retrS%codeTraceGas(istate)
          ialt   = retrS%codeAltitude(istate)
          ! ln(volumeMixingRatio) is fitted
          retrS%xa(istate)     = log( traceGasRetrS(iTrace)%vmrAP(ialt) )
          retrS%x_true(istate) = log( traceGasSimS (iTrace)%vmr  (ialt) )

          ! note that vmr is in ppmv => factor 1.0d-12 when the covariance for the number density is needed
          ! note that gasAtnRetrS%vmrCovTraceAP is the covariance for the volume mixing ratio
          ! for the logarithm we use dln(x) = dx / x
          do jstate = 1, retrS%nstate
            select case (retrS%codeFitParameters(jstate))
              case('nodeTrace')
                jTrace = retrS%codeTraceGas(jstate)
                jalt   = retrS%codeAltitude(jstate)
                if ( iTrace == jTrace ) then
                retrS%Sa_lnvmr(istate, jstate) = traceGasRetrS(iTrace)%covVmrAP(ialt, jalt) &
                                               / traceGasRetrS(iTrace)%vmrAP(ialt)          &
                                               / traceGasRetrS(iTrace)%vmrAP(jalt)
                retrS%Sa_vmr  (istate, jstate) = traceGasRetrS(iTrace)%covVmrAP(ialt, jalt)
                retrS%Sa_ndens(istate, jstate) = traceGasRetrS(iTrace)%covVmrAP(ialt, jalt) &
                                               * traceGasRetrS(iTrace)%numDensAir(ialt)     &
                                               * traceGasRetrS(iTrace)%numDensAir(jalt) * 1.0d-12
                end if
            end select
          end do ! jstate

          ! set initial value to a-priori value
          traceGasRetrS(iTrace)%vmr(ialt) = traceGasRetrS(iTrace)%vmrAP(ialt)

        case('columnTrace')

          iTrace = retrS%codeTraceGas(iState)

          ! fit the column itself not the logarithm of the column
          retrS%xa(istate)     = traceGasRetrS(iTrace)%columnAP
          retrS%x_true(istate) = traceGasSimS(iTrace)%column

          retrS%Sa_lnvmr(istate, istate)  = traceGasRetrS(iTrace)%covColumnAP
          retrS%Sa_vmr  (istate, istate)  = traceGasRetrS(iTrace)%covColumnAP
          retrS%Sa_ndens(istate, istate)  = traceGasRetrS(iTrace)%covColumnAP

          ! set initial value to a-priori value
          traceGasRetrS(iTrace)%column = traceGasRetrS(iTrace)%columnAP

        case('nodeTemp')

          ialt   = retrS%codeAltitude(istate)
          retrS%xa(istate)               = gasPTRetrS%temperatureNodesAP(ialt)
          retrS%x_true(istate)           = gasPTRetrS%temperature_trueNodes(ialt)

          do jstate = 1, retrS%nstate
            select case (retrS%codeFitParameters(jstate))
              case('nodeTemp')
                jalt   = retrS%codeAltitude(jstate)
                retrS%Sa_lnvmr(istate, jstate) = gasPTRetrS%covTempNodesAP(ialt, jalt)
                retrS%Sa_vmr  (istate, jstate) = gasPTRetrS%covTempNodesAP(ialt, jalt)
                retrS%Sa_ndens(istate, jstate) = gasPTRetrS%covTempNodesAP(ialt, jalt)
            end select
          end do ! jstate

          ! set initial value to a-priori value
          gasPTRetrS%temperatureNodes(ialt) = gasPTRetrS%temperatureNodesAP(ialt)

        case('offsetTemp')

          retrS%xa(istate)               = gasPTRetrS%temperatureOffsetAP
          retrS%x_true(istate)           = gasPTSimS%temperatureOffset
          retrS%Sa_lnvmr(istate, istate) = gasPTRetrS%varianceTempOffsetAP
          retrS%Sa_vmr  (istate, istate) = gasPTRetrS%varianceTempOffsetAP
          retrS%Sa_ndens(istate, istate) = gasPTRetrS%varianceTempOffsetAP

          ! set initial value to a-priori value
          gasPTRetrS%temperatureOffset = gasPTRetrS%temperatureOffsetAP

        case('surfPressure')

          retrS%xa(istate)               = surfaceRetrS(1)%pressureAP
          retrS%x_true(istate)           = surfaceSimS (1)%pressure
          retrS%Sa_lnvmr(istate, istate) = surfaceRetrS(1)%varPressureAP
          retrS%Sa_vmr  (istate, istate) = surfaceRetrS(1)%varPressureAP
          retrS%Sa_ndens(istate, istate) = surfaceRetrS(1)%varPressureAP

        case('surfAlbedo')

          if ( cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then

            retrS%xa(istate) = cloudAerosolRTMgridRetrS%albedoLambSurfAllBandsAP
            ! set initial value equal to a priori value
            cloudAerosolRTMgridRetrS%albedoLambSurfAllBands = &
                 cloudAerosolRTMgridRetrS%albedoLambSurfAllBandsAP
            if ( cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP > 0.0d0 ) then
              retrS%Sa_lnvmr(istate, istate) = cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP
              retrS%Sa_vmr  (istate, istate) = cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP
              retrS%Sa_ndens(istate, istate) = cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP
            else
              write(errS%temp,*) 'ERROR: a-priori variance of surface albedo = ', &
                         cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP
              call errorAddLine(errS, errS%temp)
              call logDebug('it should be positive because the inverse of Sa must be positive and finite')
              call mystop(errS, 'stopped due to incorrect a-priori variance for surface albedo')
              if (errorCheck(errS)) return
            end if ! cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP > 0.0d0

          else
          
            iband = retrS%codeSpecBand(istate)
            iwave = retrS%codeIndexSurfAlb(istate)
            retrS%xa(istate) = surfaceRetrS(iband)%albedoAP(iwave)

            if ( surfaceRetrS(iband)%varianceAlbedoAP(iwave) > 0.0d0 ) then
              retrS%Sa_lnvmr(istate, istate) = surfaceRetrS(iband)%varianceAlbedoAP(iwave)
              retrS%Sa_vmr  (istate, istate) = surfaceRetrS(iband)%varianceAlbedoAP(iwave)
              retrS%Sa_ndens(istate, istate) = surfaceRetrS(iband)%varianceAlbedoAP(iwave)
            else
              write(errS%temp,*) 'ERROR: a-priori variance of surface albedo = ', &
                         surfaceRetrS(iband)%varianceAlbedoAP(iwave)
              call errorAddLine(errS, errS%temp)
              call logDebug('it should be positive because the inverse of Sa must be positive and finite')
              write(errS%temp,*) 'iband and index are:', iband, iwave
              call errorAddLine(errS, errS%temp)
              call mystop(errS, 'stopped due to incorrect a-priori variance for surface albedo')
              if (errorCheck(errS)) return
            end if

            ! set initial value to a-priori value
            surfaceRetrS(iband)%albedo(iwave)  = surfaceRetrS(iband)%albedoAP(iwave)

            ! if a correlation matrix is specified fill non-diagonal values
            if ( associated( surfaceRetrS(iband)%correlationAP ) ) then
              do jstate = 1, retrS%nstate
                select case (retrS%codeFitParameters(jstate))
                  case('surfAlbedo')
                    jwave = retrS%codeIndexSurfAlb(jstate)
                    retrS%Sa_lnvmr(istate, jstate) = surfaceRetrS(iband)%correlationAP(iwave, jwave)      &
                                                   * sqrt(   surfaceRetrS(iband)%varianceAlbedoAP(iwave)  &
                                                   * surfaceRetrS(iband)%varianceAlbedoAP(jwave) )
                    retrS%Sa_vmr  (istate, jstate) = retrS%Sa_lnvmr(istate, jstate)
                    retrS%Sa_ndens(istate, jstate) = retrS%Sa_lnvmr(istate, jstate)
                end select
              end do ! jstate
            end if ! associated

          end if ! cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands

          ! evaluate the 'true' surface albedo
          if ( cloudAerosolRTMgridSimS%useAlbedoLambSurfAllBands ) then
            retrS%x_true(istate) = cloudAerosolRTMgridSimS%albedoLambSurfAllBands
          else
            iband = retrS%codeSpecBand(istate)
            iwave = retrS%codeIndexSurfAlb(istate)
            select case ( surfaceSimS(iband)%nwavelAlbedo )
              case(1) ! no wavelength dependence surface albedo 
                surfaceAlbedo = surfaceSimS(iband)%albedo(1)
              case(2:100) ! interpolation or extrapolation
                surfaceAlbedo =  polyInt(errS, surfaceSimS(iband)%wavelAlbedo, surfaceSimS(iband)%albedo, &
                                         surfaceRetrS(iband)%wavelAlbedo(iwave))
              case default
                call mystop(errS, 'incorrect number of surface albedo values in fillAPriori')
                if (errorCheck(errS)) return
            end select
            retrS%x_true(istate) = surfaceAlbedo
          end if

        case('surfEmission')

          iband = retrS%codeSpecBand(istate)
          iwave = retrS%codeIndexSurfEmission(istate)
          retrS%xa(istate) = surfaceRetrS(iband)%emissionAP(iwave)

          if ( surfaceRetrS(iband)%varianceEmissionAP(iwave) > 0.0d0 ) then
            retrS%Sa_lnvmr(istate, istate) = surfaceRetrS(iband)%varianceEmissionAP(iwave)
            retrS%Sa_vmr  (istate, istate) = surfaceRetrS(iband)%varianceEmissionAP(iwave)
            retrS%Sa_ndens(istate, istate) = surfaceRetrS(iband)%varianceEmissionAP(iwave)
          else
            write(errS%temp,*) 'ERROR: a-priori variance of surface emission = ', &
                       surfaceRetrS(iband)%varianceEmissionAP(index)
            call errorAddLine(errS, errS%temp)
            call logDebug('it should be positive because the inverse of Sa must be positive and finite')
            write(errS%temp,*) 'iband and index are:', iband, index
            call errorAddLine(errS, errS%temp)
            call mystop(errS, 'stopped due to incorrect a-priori variance for surface emission')
            if (errorCheck(errS)) return
          end if

          ! set initial value to a-priori value
          surfaceRetrS(iband)%emission(iwave)  = surfaceRetrS(iband)%emissionAP(iwave)

          ! evaluate the 'true' surface emission
          iband = retrS%codeSpecBand(istate)
          iwave = retrS%codeIndexSurfEmission(istate)
          select case ( surfaceSimS(iband)%nwavelEmission )
            case(1) ! no wavelength dependence surface emission
              surfaceEmission = surfaceSimS(iband)%emission(1)
            case(2:100) ! interpolation or extrapolation
              surfaceEmission =  polyInt(errS, surfaceSimS(iband)%wavelEmission, surfaceSimS(iband)%emission, &
                                         surfaceRetrS(iband)%wavelEmission(iwave))
            case default
              call mystop(errS, 'incorrect number of surface emission values in fillAPriori')
              if (errorCheck(errS)) return
          end select
          retrS%x_true(istate) = surfaceEmission

        case('LambCldAlbedo')

          if ( cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then

            retrS%xa(istate) = cloudAerosolRTMgridRetrS%albedoLambCldAllBandsAP

            if ( cloudAerosolRTMgridRetrS%varAlbedoLambCldAllAP > 0.0d0 ) then
              retrS%Sa_lnvmr(istate, istate) = cloudAerosolRTMgridRetrS%varAlbedoLambCldAllAP
              retrS%Sa_vmr  (istate, istate) = cloudAerosolRTMgridRetrS%varAlbedoLambCldAllAP
              retrS%Sa_ndens(istate, istate) = cloudAerosolRTMgridRetrS%varAlbedoLambCldAllAP
            else
              write(errS%temp,*) 'ERROR: a-priori variance of Lambertian cloud albedo = ', &
                         cloudAerosolRTMgridRetrS%varAlbedoLambCldAllAP
              call errorAddLine(errS, errS%temp)
              call logDebug('it should be positive because the inverse of Sa must be positive and finite')
              call mystop(errS, 'stopped due to incorrect a-priori variance for Lambertian cloud albedo')
              if (errorCheck(errS)) return
            end if

            retrS%x_true(istate) = cloudAerosolRTMgridSimS%albedoLambCldAllBands

            ! set initial value to a-priori value
            cloudAerosolRTMgridRetrS%albedoLambCldAllBands  = cloudAerosolRTMgridRetrS%albedoLambCldAllBandsAP

          else

            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexLambCldAlb(istate)

            retrS%xa(istate) = LambCloudRetrS(iband)%albedoAP(index)

            if ( LambCloudRetrS(iband)%varianceAlbedoAP(index) > 0.0d0 ) then
              retrS%Sa_lnvmr(istate, istate) = LambCloudRetrS(iband)%varianceAlbedoAP(index)
              retrS%Sa_vmr  (istate, istate) = LambCloudRetrS(iband)%varianceAlbedoAP(index)
              retrS%Sa_ndens(istate, istate) = LambCloudRetrS(iband)%varianceAlbedoAP(index)
            else
              write(errS%temp,*) 'ERROR: a-priori variance of Lambertian cloud albedo = ', &
                         LambCloudRetrS(iband)%varianceAlbedoAP(index)
              call errorAddLine(errS, errS%temp)
              call logDebug('it should be positive because the inverse of Sa must be positive and finite')
              write(errS%temp,*) 'iband and index are:', iband, index
              call errorAddLine(errS, errS%temp)
              call mystop(errS, 'stopped due to incorrect a-priori variance for Lambertian cloud albedo')
              if (errorCheck(errS)) return
            end if

            ! evaluate the 'true' cloud albedo at the wavelength nodes used in the retrieval
            if ( cloudAerosolRTMgridSimS%useLambertianCloud ) then
              select case ( LambCloudSimS(iband)%nwavelAlbedo )
                case(1) ! no wavelength dependence cloud albedo 
                  cloudAlbedo = LambCloudSimS(iband)%albedo(1)
                case(2:100) ! polynomial interpolation or extrapolation
                  cloudAlbedo =  polyInt(errS, LambCloudSimS(iband)%wavelAlbedo, LambCloudSimS(iband)%albedo, &
                                         LambCloudRetrS(iband)%wavelAlbedo(index))
                case default
                  call mystop(errS, 'incorrect number of Lambertian cloud albedo values in fillAPriori')
                  if (errorCheck(errS)) return
              end select
            end if !  cloudAerosolRTMgridSimS%useLambertianCloud 
            retrS%x_true(istate) = cloudAlbedo

            ! set initial value to a-priori value
            LambCloudRetrS(iband)%albedo(index)  = LambCloudRetrS(iband)%albedoAP(index)

          end if

        case('mulOffset')

          iband = retrS%codeSpecBand(istate)
          index = retrS%codeIndexMulOffset(istate)
          retrS%xa(istate) = mulOffsetRetrS(iband)%percentOffsetAP(index)
          retrS%Sa_lnvmr(istate, istate) = mulOffsetRetrS(iband)%varMulOffsetAP(index)
          retrS%Sa_vmr  (istate, istate) = mulOffsetRetrS(iband)%varMulOffsetAP(index)
          retrS%Sa_ndens(istate, istate) = mulOffsetRetrS(iband)%varMulOffsetAP(index)

          ! evaluate the 'true' stray light at the wavelength nodes used in the retrieval
          select case ( mulOffsetSimS(iband)%nwavel )
            case(1) ! no wavelength dependence multiplicative offset
              mulOffset = mulOffsetSimS(iband)%percentOffset(1)
            case(2:100) ! polynomial interpolation or extrapolation
              mulOffset =  polyInt(errS, mulOffsetSimS(iband)%wavel, mulOffsetSimS(iband)%percentOffset, &
                                   mulOffsetRetrS(iband)%wavel(index))
            case default
              call mystop(errS, 'incorrect number of multiplicative offset values in fillAPriori')
              if (errorCheck(errS)) return
          end select
          retrS%x_true(istate) = mulOffset

          ! set initial value to a-priori value
          mulOffsetRetrS(iband)%percentOffset(index) = mulOffsetRetrS(iband)%percentOffsetAP(index)

        case('straylight')

          iband = retrS%codeSpecBand(istate)
          index = retrS%codeIndexStraylight(istate)
          retrS%xa(istate) = strayLightRetrS(iband)%percentStrayLightAP(index)
          retrS%Sa_lnvmr(istate, istate) = strayLightRetrS(iband)%varStrayLightAP(index)
          retrS%Sa_vmr  (istate, istate) = strayLightRetrS(iband)%varStrayLightAP(index)
          retrS%Sa_ndens(istate, istate) = strayLightRetrS(iband)%varStrayLightAP(index)

          ! evaluate the 'true' stray light at the wavelength nodes used in the retrieval
          select case ( strayLightSimS(iband)%nwavel )
            case(1) ! no wavelength dependence stray light
              strayLight = strayLightSimS(iband)%percentStrayLight(1)
            case(2:100) ! polynomial interpolation or extrapolation
              strayLight =  polyInt(errS, strayLightSimS(iband)%wavel, strayLightSimS(iband)%percentStrayLight, &
                                    strayLightRetrS(iband)%wavel(index))
            case default
              call mystop(errS, 'incorrect number of stray light values in fillAPriori')
              if (errorCheck(errS)) return
          end select
          retrS%x_true(istate) = strayLight

          ! set initial value to a-priori value
          strayLightRetrS(iband)%percentStrayLight(index) = strayLightRetrS(iband)%percentStrayLightAP(index)

        case('diffRingCoef')

          iband = retrS%codeSpecBand(istate)
          retrS%xa(istate)               = RRS_RingS(iband)%ringCoeffAP
          retrS%x_true(istate)           = 0.0d0                           ! in simulation no Ring spectrum
          retrS%Sa_lnvmr(istate, istate) = RRS_RingS(iband)%varRingCoeffAP
          retrS%Sa_vmr  (istate, istate) = RRS_RingS(iband)%varRingCoeffAP
          retrS%Sa_ndens(istate, istate) = RRS_RingS(iband)%varRingCoeffAP

        case('RingCoef')

          iband = retrS%codeSpecBand(istate)
          retrS%xa(istate)               = RRS_RingS(iband)%ringCoeffAP
          retrS%x_true(istate)           = 0.0d0                           ! in simulation no Ring spectrum
          retrS%Sa_lnvmr(istate, istate) = RRS_RingS(iband)%varRingCoeffAP
          retrS%Sa_vmr  (istate, istate) = RRS_RingS(iband)%varRingCoeffAP
          retrS%Sa_ndens(istate, istate) = RRS_RingS(iband)%varRingCoeffAP

        case('aerosolTau')

          retrS%xa(istate) = cloudAerosolRTMgridRetrS%intervalAerTauAP(cloudAerosolRTMgridRetrS%numIntervalFit)

          if ( cloudAerosolRTMgridRetrS%varAerTauAP  > 0.0d0 ) then
            retrS%Sa_lnvmr(istate, istate) = cloudAerosolRTMgridRetrS%varAerTauAP
            retrS%Sa_vmr  (istate, istate) = cloudAerosolRTMgridRetrS%varAerTauAP
            retrS%Sa_ndens(istate, istate) = cloudAerosolRTMgridRetrS%varAerTauAP
          else
            write(errS%temp,*) 'ERROR: a-priori variance of optical thickness for aerosol = ', &
                       cloudAerosolRTMgridRetrS%varAerTauAP 
            call errorAddLine(errS, errS%temp)
            call logDebug('it should be positive because the inverse of Sa must be positive and finite')
            call mystop(errS, 'stopped due to incorrect a-priori variance for the optical thickness for aerosol')
            if (errorCheck(errS)) return
          end if

          ! avoid array bound errors
          if ( size(cloudAerosolRTMgridSimS%intervalAerTau) >= &
               cloudAerosolRTMgridSimS%numIntervalFit ) then
            retrS%x_true(istate) = cloudAerosolRTMgridSimS%intervalAerTau(cloudAerosolRTMgridSimS%numIntervalFit)
          else
            retrS%x_true(istate) = 0.0d0
          end if

          ! set initial value to a-priori value
          cloudAerosolRTMgridRetrS%intervalAerTau(cloudAerosolRTMgridRetrS%numIntervalFit) &
             = cloudAerosolRTMgridRetrS%intervalAerTauAP(cloudAerosolRTMgridRetrS%numIntervalFit)

        case('aerosolSSA')

          aerSSA = cloudAerosolRTMgridRetrS%intervalAerSSAAP(cloudAerosolRTMgridRetrS%numIntervalFit)
          retrS%xa(istate) = aerSSA

          if ( cloudAerosolRTMgridRetrS%varAerSSAAP  > 0.0d0 ) then
            retrS%Sa_lnvmr(istate, istate) = cloudAerosolRTMgridRetrS%varAerSSAAP
            retrS%Sa_vmr  (istate, istate) = cloudAerosolRTMgridRetrS%varAerSSAAP
            retrS%Sa_ndens(istate, istate) = cloudAerosolRTMgridRetrS%varAerSSAAP
          else
            write(errS%temp,*) 'ERROR: a-priori variance of single scattering albedo for aerosol = ', &
                       cloudAerosolRTMgridRetrS%varAerSSAAP 
            call errorAddLine(errS, errS%temp)
            call logDebug('it should be positive because the inverse of Sa must be positive and finite')
            call mystop(errS, 'stopped due to incorrect a-priori variance for the single scattering albedo of the aerosol')
            if (errorCheck(errS)) return
          end if
          
          ! avoid array bound errors
          if ( size(cloudAerosolRTMgridSimS%intervalAerSSA) >= &
               cloudAerosolRTMgridSimS%numIntervalFit ) then
            aerSSA = cloudAerosolRTMgridSimS%intervalAerSSA(cloudAerosolRTMgridSimS%numIntervalFit)
          else
            aerSSA = 0.0d0
          end if
          retrS%x_true(istate) = aerSSA

          ! set initial value to a-priori value
          cloudAerosolRTMgridRetrS%intervalAerSSA(cloudAerosolRTMgridRetrS%numIntervalFit) &
             = cloudAerosolRTMgridRetrS%intervalAerSSAAP(cloudAerosolRTMgridRetrS%numIntervalFit)

        case('aerosolAC')

          aerosolAC = cloudAerosolRTMgridRetrS%intervalAerACAP(cloudAerosolRTMgridRetrS%numIntervalFit)
          retrS%xa(istate) = aerosolAC

          if ( cloudAerosolRTMgridRetrS%varAerACAP  > 0.0d0 ) then
            retrS%Sa_lnvmr(istate, istate) = cloudAerosolRTMgridRetrS%varAerACAP
            retrS%Sa_vmr  (istate, istate) = cloudAerosolRTMgridRetrS%varAerACAP
            retrS%Sa_ndens(istate, istate) = cloudAerosolRTMgridRetrS%varAerACAP
          else
            write(errS%temp,*) 'ERROR: a-priori variance of angstrom coefficient for aerosol = ', &
                       cloudAerosolRTMgridRetrS%varAerACAP 
            call errorAddLine(errS, errS%temp)
            call logDebug('it should be positive because the inverse of Sa must be positive and finite')
            call mystop(errS, 'stopped due to incorrect a-priori variance for the angstrom coefficient of the aerosol')
            if (errorCheck(errS)) return
          end if

          ! avoid array bound errors
          if ( size(cloudAerosolRTMgridSimS%intervalAerAC) >= &
               cloudAerosolRTMgridSimS%numIntervalFit ) then
            aerosolAC = cloudAerosolRTMgridSimS%intervalAerAC(cloudAerosolRTMgridRetrS%numIntervalFit)
          else
            aerosolAC = 0.0d0
          end if
          retrS%x_true(istate) = aerosolAC

          ! set initial value to a-priori value
          cloudAerosolRTMgridRetrS%intervalAerAC(cloudAerosolRTMgridRetrS%numIntervalFit) &
             = cloudAerosolRTMgridRetrS%intervalAerACAP(cloudAerosolRTMgridRetrS%numIntervalFit)

        case('cloudTau')
          
          if ( cloudAerosolRTMgridRetrS%fitLnCldTau ) then

            ! natural logarithm of the aerosol optical thickness at the reference wavelength is fitted
            tau = cloudAerosolRTMgridRetrS%intervalCldTauAP(cloudAerosolRTMgridRetrS%numIntervalFit)
            if ( tau > 0.0d0 ) then
              retrS%xa(istate) = log( tau )
            else
              call logDebugD('ERROR: a-priori cloud optical thickness = ', tau)
              call logDebug('it should be positive because the logarithm is fitted')
              call mystop(errS, 'stopped due to incorrect a-priori cloud optical thickness')
              if (errorCheck(errS)) return
            end if

            ! avoid array bound errors
            if ( size(cloudAerosolRTMgridSimS%intervalCldTau) >= &
                 cloudAerosolRTMgridSimS%numIntervalFit ) then
              tau = cloudAerosolRTMgridSimS%intervalCldTau(cloudAerosolRTMgridSimS%numIntervalFit)
            else
              tau = 0.0d0
            end if

            if ( tau > 0.0d0 ) then
              retrS%x_true(istate) = log( tau )
            else
              call logDebugD('WARNING: true cloud optical thickness = ', tau)
              call logDebug('this value is unreliable')
              call logDebug('perhaps because different interval are used for simulation and retrieval')
            end if

            if ( cloudAerosolRTMgridRetrS%varCldTauAP > 0.0d0 ) then
              retrS%Sa_lnvmr(istate, istate) =  cloudAerosolRTMgridRetrS%varCldTauAP &
                     / cloudAerosolRTMgridRetrS%intervalCldTauAP(cloudAerosolRTMgridRetrS%numIntervalFit)**2
            else
              write(errS%temp,*) 'ERROR: a-priori variance cloud optical thickness = ', &
                          cloudAerosolRTMgridRetrS%varCldTauAP
              call errorAddLine(errS, errS%temp)
              call logDebug('it should be positive because the inverse of Sa must be positive and finite')
              call mystop(errS, 'stopped due to incorrect a-priori variance of the cloud optical thickness')
              if (errorCheck(errS)) return
            end if

          else

            retrS%xa(istate)     = cloudAerosolRTMgridRetrS%intervalCldTauAP(cloudAerosolRTMgridRetrS%numIntervalFit)

            ! avoid array bound errors
            if ( size(cloudAerosolRTMgridSimS%intervalCldTau) >= &
                 cloudAerosolRTMgridSimS%numIntervalFit ) then
              tau = cloudAerosolRTMgridSimS%intervalCldTau(cloudAerosolRTMgridSimS%numIntervalFit)
            else
              tau = 0.0d0
            end if
            retrS%x_true(istate) = tau

            if ( cloudAerosolRTMgridRetrS%varCldTauAP > 0.0d0 ) then
              retrS%Sa_lnvmr(istate, istate) =  cloudAerosolRTMgridRetrS%varCldTauAP
            else
              write(errS%temp,*) 'ERROR: a-priori variance of the cloud optical thickness = ', &
                          cloudAerosolRTMgridRetrS%varCldTauAP
              call errorAddLine(errS, errS%temp)
              call logDebug('it should be positive because the inverse of Sa must be positive and finite')
              call mystop(errS, 'stopped due to incorrect a-priori variance for the cloud optical thickness')
              if (errorCheck(errS)) return
            end if

          end if ! cloudAerosolRTMgridRetrS%fitLnCldTau

          retrS%Sa_vmr  (istate, istate) = cloudAerosolRTMgridRetrS%varCldTauAP
          retrS%Sa_ndens(istate, istate) = cloudAerosolRTMgridRetrS%varCldTauAP

          ! set initial value to a-priori value
          cloudAerosolRTMgridRetrS%intervalCldTau(cloudAerosolRTMgridRetrS%numIntervalFit) &
             = cloudAerosolRTMgridRetrS%intervalCldTauAP(cloudAerosolRTMgridRetrS%numIntervalFit)

        case('cloudAC')

          cloudAC = cloudAerosolRTMgridRetrS%intervalCldACAP(cloudAerosolRTMgridRetrS%numIntervalFit)
          retrS%xa(istate) = cloudAC

          if ( cloudAerosolRTMgridRetrS%varCldACAP  > 0.0d0 ) then
            retrS%Sa_lnvmr(istate, istate) = cloudAerosolRTMgridRetrS%varCldACAP
            retrS%Sa_vmr  (istate, istate) = cloudAerosolRTMgridRetrS%varCldACAP
            retrS%Sa_ndens(istate, istate) = cloudAerosolRTMgridRetrS%varCldACAP
          else
            write(errS%temp,*) 'ERROR: a-priori variance of angstrom coefficient for cloud = ', &
                       cloudAerosolRTMgridRetrS%varCldACAP 
            call errorAddLine(errS, errS%temp)
            call logDebug('it should be positive because the inverse of Sa must be positive and finite')
            call mystop(errS, 'stopped due to incorrect a-priori variance for the angstrom coefficient of the cloud')
            if (errorCheck(errS)) return
          end if

          ! avoid array bound errors
          if ( size(cloudAerosolRTMgridSimS%intervalCldAC) >= &
               cloudAerosolRTMgridSimS%numIntervalFit ) then
            cloudAC = cloudAerosolRTMgridSimS%intervalCldAC(cloudAerosolRTMgridRetrS%numIntervalFit)
          else
            cloudAC = 0.0d0
          end if
          retrS%x_true(istate) = cloudAC

          ! set initial value to a-priori value
          cloudAerosolRTMgridRetrS%intervalCldAC(cloudAerosolRTMgridRetrS%numIntervalFit) &
             = cloudAerosolRTMgridRetrS%intervalCldACAP(cloudAerosolRTMgridRetrS%numIntervalFit)

        case('cloudFraction')

          if ( cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then

            retrS%xa(istate)     = cloudAerosolRTMgridRetrS%cldAerFractionAllBandsAP
            retrS%x_true(istate) = cloudAerosolRTMgridSimS %cldAerFractionAllBandsAP

            if ( cloudAerosolRTMgridRetrS%varCldAerFractionAllBandsAP > 0.0d0 ) then
              retrS%Sa_lnvmr(istate, istate) = cloudAerosolRTMgridRetrS%varCldAerFractionAllBandsAP
            else
              write(errS%temp,*) 'ERROR: a-priori variance cloud/aerosol fraction = ', &
                         cloudAerosolRTMgridRetrS%varCldAerFractionAllBandsAP
              call errorAddLine(errS, errS%temp)
              call logDebug('it should be positive because the inverse of Sa must be positive and finite')
              call mystop(errS, 'stopped due to incorrect a-priori variance of the cloud / aerosol fraction')
              if (errorCheck(errS)) return
            end if

            retrS%Sa_vmr  (istate, istate) = cloudAerosolRTMgridRetrS%varCldAerFractionAllBandsAP
            retrS%Sa_ndens(istate, istate) = cloudAerosolRTMgridRetrS%varCldAerFractionAllBandsAP

            ! set initial value to a-priori value
            cloudAerosolRTMgridRetrS%cldAerFractionAllBands &
             = cloudAerosolRTMgridRetrS%cldAerFractionAllBandsAP

          else
            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexCloudFraction(istate)
            retrS%xa(istate) = cldAerFractionRetrS(iband)%cldAerFractionAP(index)

            if ( cldAerFractionRetrS(iband)%varianceCldAerFractionAP(index) > 0.0d0 ) then
              retrS%Sa_lnvmr(istate, istate) = cldAerFractionRetrS(iband)%varianceCldAerFractionAP(index)
              retrS%Sa_vmr  (istate, istate) = cldAerFractionRetrS(iband)%varianceCldAerFractionAP(index)
              retrS%Sa_ndens(istate, istate) = cldAerFractionRetrS(iband)%varianceCldAerFractionAP(index)
            else
              write(errS%temp,*) 'ERROR: a-priori variance of cloud fraction = ', &
                         cldAerFractionRetrS(iband)%varianceCldAerFractionAP(index)
              call errorAddLine(errS, errS%temp)
              call logDebug('it should be positive because the inverse of Sa must be positive and finite')
              write(errS%temp,*) 'iband and index are:', iband, index
              call errorAddLine(errS, errS%temp)
              call mystop(errS, 'stopped due to incorrect a-priori variance for cloud fraction')
              if (errorCheck(errS)) return
            end if

            ! evaluate the 'true' cloud fraction at the wavelengths used in the retrieval
            select case ( cldAerFractionRetrS(iband)%nwavelCldAerFraction )
              case(1) ! no wavelength dependence cloud fraction
                cloudFraction = cldAerFractionSimS(iband)%cldAerFraction(1)
              case(2:100) ! polynomial interpolation or extrapolation
                cloudFraction =  polyInt(errS, cldAerFractionSimS(iband)%wavelCldAerFraction, &
                                         cldAerFractionSimS(iband)%cldAerFraction,      &
                                         cldAerFractionRetrS(iband)%wavelCldAerFraction(index))
              case default
                call mystop(errS, 'incorrect number of cloud / aerosol fraction values in fillAPriori')
                if (errorCheck(errS)) return
            end select
            retrS%x_true(istate) = cloudFraction

            ! set initial value to a-priori value
            cldAerFractionRetrS(iband)%cldAerFraction(index)  = cldAerFractionRetrS(iband)%cldAerFractionAP(index)

          end if

        case('intervalDP')

          retrS%xa(istate)  = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit)
          retrS%Sa_lnvmr(istate, istate) =  &
             cloudAerosolRTMgridRetrS%varIntervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit)
          retrS%Sa_vmr  (istate, istate) = retrS%Sa_lnvmr(istate, istate)
          retrS%Sa_ndens(istate, istate) = retrS%Sa_lnvmr(istate, istate)

          retrS%x_true(istate) = cloudAerosolRTMgridSimS%intervalBounds(cloudAerosolRTMgridSimS%numIntervalFit)

          ! set initial value to a-priori value
          cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit) &
             = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit)
          cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit - 1) &
             = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit - 1)

          ! set altitude and pressure for Lambertian cloud
          LambCloudRetrS(:)%altitude = &
                   cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit) 
          LambCloudRetrS(:)%pressure = &
                   cloudAerosolRTMgridRetrS%intervalBounds_P(cloudAerosolRTMgridRetrS%numIntervalFit) 

        case('intervalTop')

          retrS%xa(istate)  = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit)
          retrS%Sa_lnvmr(istate, istate) =  &
             cloudAerosolRTMgridRetrS%varIntervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit)
          retrS%Sa_vmr  (istate, istate) = retrS%Sa_lnvmr(istate, istate)
          retrS%Sa_ndens(istate, istate) = retrS%Sa_lnvmr(istate, istate)

          retrS%x_true(istate) = cloudAerosolRTMgridSimS%intervalBounds(cloudAerosolRTMgridSimS%numIntervalFit)

          ! set initial value to a-priori value
          cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit) &
             = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit)

          ! set altitude and pressure for Lambertian cloud
          LambCloudRetrS(:)%altitude = &
                   cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit) 
          LambCloudRetrS(:)%pressure = &
                   cloudAerosolRTMgridRetrS%intervalBounds_P(cloudAerosolRTMgridRetrS%numIntervalFit) 

        case('intervalBot')

          retrS%xa(istate)  = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit-1)
          retrS%Sa_lnvmr(istate, istate) =  &
             cloudAerosolRTMgridRetrS%varIntervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit-1)
          retrS%Sa_vmr  (istate, istate) = retrS%Sa_lnvmr(istate, istate)
          retrS%Sa_ndens(istate, istate) = retrS%Sa_lnvmr(istate, istate)

          retrS%x_true(istate) = cloudAerosolRTMgridSimS%intervalBounds(cloudAerosolRTMgridSimS%numIntervalFit - 1)

          ! set initial value to a-priori value
          cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit - 1) &
             = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit - 1)

        case default
          call logDebug('in subroutine fillAPriori in optimalEstimationModule')
          call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
          call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
          if (errorCheck(errS)) return

      end select

    end do ! istate

    ! write to intermediate file if verbose is .true.
    if ( verbose ) then
      write(intermediateFileUnit,*) ' code   xa  and  Sa_lnvmr from fillAPriori'
      do istate = 1, retrS%nstate
        write(intermediateFileUnit,'(A15,101F12.6)') retrS%codeFitParameters(istate), retrS%xa(istate), &
                                    (retrS%Sa_lnvmr(istate, jstate), jstate = 1, retrS%nstate)
      end do
    end if

  end subroutine fillAPriori


  subroutine calculateNewState(errS, iteration, retrS)

    ! A modified form of Gauss-Newton iteration is used and the change in the state vector
    ! in the transformed space is limited by changing the effective signal to noise

    implicit none

    type(errorType), intent(inout) :: errS
    integer,         intent(in)    :: iteration
    type(retrType),  intent(inout) :: retrS

    ! local
    real(8) :: dxwhite(retrS%nState), dxtrans(retrS%nState), dxtransNew(retrS%nState)
    real(8) :: dRwhite(retrS%nwavelRetr), dRtrans(retrS%nState) 
    real(8) :: Kwhite(retrS%nwavelRetr,retrS%nState)
    real(8) :: SInvwhite(retrS%nState,retrS%nState), SInvtrans(retrS%nState,retrS%nState)
    real(8) :: sqrtSa(retrS%nState,retrS%nState), sqrtInvSa(retrS%nState,retrS%nState) 
    real(8) :: SaInv(retrS%nState,retrS%nState)

    ! matrices for svd
    real(8) :: w(retrS%nState)
    real(8) :: u(retrS%nwavelRetr,retrS%nState), uT(retrS%nState,retrS%nwavelRetr)
    real(8) :: v(retrS%nState,retrS%nState), vT(retrS%nState,retrS%nState)

    ! limit change in state vector
    real(8) :: changeTransfState, maxChange
    real(8) :: factor, totFactor, dfs
    integer :: ifactor

    ! various
    integer :: iwave, istate

    ! control printing to intermediate file
    logical, parameter :: verbose = .false.
    logical, parameter :: writeReflection = .false.


    call invert_Sa(errS, retrS, retrS%Sa_lnvmr, SaInv, sqrtSa, sqrtInvSa)

    ! calculate dxwhite, dRwhite, and Kwhite
    dxwhite  = matmul(sqrtInvSa,retrS%dx)
    dRwhite  = matmul(retrS%sqrtInvSe,retrS%dR)
    Kwhite   = matmul(retrS%sqrtInvSe,matmul(retrS%K_lnvmr,sqrtSa))

    ! find the singular value decomposition for Kwhite = U W VT

    u = Kwhite
    ! svdcmp overwrites first array (input) with u
    call svdcmp(errS, u, w, v, retrS%nState)
    if (errorCheck(errS)) return
    vT = transpose(v)
    uT = transpose(u)

    ! calculate the transformed values of dR and dx
    dRtrans =  matmul(uT,dRwhite)
    dxtrans =  matmul(vT,dxwhite)

    ! calculate new state vector in the tranformed space
    dxtransNew = w * (dRtrans + w * dxtrans) /(w**2 + 1.0d0)

    dfs = 0.0d0
    do istate = 1, retrS%nState
      dfs = dfs + w(istate)**2 / ( w(istate)**2 + 1 )
    end do

    ! allow a maximal change equal to retrS%MaxChangeTransfState
    ! reduce S/N only if the change is larger than retrS%MaxChangeTransfState AND maxVal(abs(dxtrans))
    changeTransfState  = maxVal( abs(dxtransNew - dxtrans) )
    maxChange = max( retrS%MaxChangeTransfState, maxVal(abs(dxtrans)) )
    ! interoduce the factor 1.01d0 for rounding errors
    totFactor = 1.00d0  ! initial value
    factor    = 0.75d0  ! initial value
    retrS%SNRnormal = .true.
    if ( changeTransfState > 1.01d0 * maxChange ) then
      retrS%SNRnormal = .false.
      do ifactor = 1, 10
        totFactor  = factor * totFactor
        dRtrans    = factor * dRtrans
        w          = factor * w
        dxtransNew = w * (dRtrans + w * dxtrans) /(w**2 + 1.0d0)
        changeTransfState  = maxVal( abs(dxtransNew - dxtrans) )
        if (verbose) then
          write(intermediateFileUnit,'(A,2E12.4,I4)') 'changeTransfState = ', changeTransfState
        end if
        if ( changeTransfState < maxChange ) exit
      end do
    end if

    ! By setting dxtransNew(istate) to zero for istate > n 
    ! only the first n eigenvalues are used in the update of the state vector
    ! By setting n to dfs we get nearly the same solution as in optimal estimation
    ! Hence reduction of the number of eigenvalues does not seem to be a good way
    ! to stabilize the solution (make it smooth), unless we reduce the information content   
    ! do istate = 12, retrS%nState
    !   dxtransNew(istate) = 0.0d0
    ! end do

    retrS%dx = matmul(sqrtSa, matmul(v,dxtransNew))

    retrS%x  = retrS%xa + retrS%dx
        
    if (iteration > 10 ) then
      retrS%x  = 0.7d0 * retrS%x + 0.3d0 * retrS%xPrev
      retrS%dx = retrS%x - retrS%xa
    end if

    ! store results
    retrS%x_stored(:,iteration) = retrS%x(:)

    ! calculate chi**2
    retrS%chi2x = dot_product( (retrS%x - retrS%xPrev), matmul(SaInv,(retrS%x - retrS%xPrev)) )
    retrS%chi2R = dot_product( retrS%dR, matmul(retrS%InvSe,retrS%dR) )
    retrS%chi2 = retrS%chi2x + retrS%chi2R
    retrS%rmse  = sqrt(sum(retrS%dR(:)**2)/size(retrS%dR(:)))
    
    SInvtrans = 0.0d0
    do istate = 1, retrS%nState
      SInvtrans(istate,istate) = 1.0d0 + w(istate)**2
    end do
    SInvwhite  = matmul(v,matmul(SInvtrans,vT))
    retrS%SInv_lnvmr = matmul(sqrtInvSa,matmul(SInvwhite,sqrtInvSa))

    ! test convergence

    ! For ozone hole conditions the first step may be very small so that dx is
    ! small and retrS%xConv is less than the threshold. Therefore, we also test
    ! that the signal to noise is not reduced by demanding

    retrS%xConv = dot_product( (retrS%x - retrS%xPrev), matmul(retrS%SInv_lnvmr, (retrS%x - retrS%xPrev)) )

    ! divide by the number of state vector elements
    retrS%xConv = retrS%xConv / retrS%nstate
    if ( ( retrS%xConv  < retrS%xConvThreshold ) .and. retrS%SNRnormal ) then
        retrS%isConverged = .true.
    else
        retrS%isConverged = .false.
    end if

    if ( verbose ) then

      if ( writeReflection ) then
        write(intermediateFileUnit,*) 'iwave   reflMeas     refl'
        do iwave = 1, retrS%nwavelRetr
          write(intermediateFileUnit,'(I4,2ES20.10)') iwave, retrS%reflMeas(iwave), retrS%refl(iwave)
        end do
      end if

      if ( iteration == 1 ) then
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,'(46X,500A17)') (retrS%codeFitParameters(iState), istate = 1, retrS%nState)
        write(intermediateFileUnit,'(A,15X,500E17.7)')  'a-priori state vector      = ', retrS%xa
      end if
      write(intermediateFileUnit,'(A,E15.5,500E17.7)')  'chi2 and new state vector  = ', retrS%chi2, retrS%x

    end if ! verbose

  end subroutine calculateNewState


  subroutine calculateDiagnostics(errS, retrS, diagnosticS)

    ! calculate the error covariance matrix, averaging kernel, gain, and chi2

    implicit none

    type(errorType),         intent(inout) :: errS
    type(retrType),          intent(in)    :: retrS
    type(diagnosticType),    intent(inout) :: diagnosticS

    ! local
    real(8) :: Kwhite(retrS%nwavelRetr,retrS%nState)
    real(8) :: sqrtSa(retrS%nState,retrS%nState), sqrtInvSa(retrS%nState,retrS%nState) 
    real(8) :: SaInv(retrS%nState,retrS%nState)
    real(8) :: Awhite(retrS%nState,retrS%nState), Atrans(retrS%nState,retrS%nState)
    real(8) :: Swhite(retrS%nState,retrS%nState), Strans(retrS%nState,retrS%nState)
    real(8) :: SInv_trans(retrS%nState,retrS%nState), SInv_white(retrS%nState,retrS%nState)

    ! matrices for svd
    real(8) :: w(retrS%nState)
    real(8) :: u(retrS%nwavelRetr, retrS%nState), uT(retrS%nState, retrS%nwavelRetr)
    real(8) :: v(retrS%nState, retrS%nState), vT(retrS%nState, retrS%nState)
    real(8) :: Ss(retrS%nState, retrS%nState)  ! smoothing error for lnvmr
    real(8) :: Sn(retrS%nState, retrS%nState)  ! noise error for lnvmr

    ! various
    integer :: istate, jstate, iTrace, i, j

    ! lnvmr

    call invert_Sa(errS, retrS, retrS%Sa_lnvmr, SaInv, sqrtSa, sqrtInvSa)

    ! calculate dxwhite, dRwhite, and Kwhite
    Kwhite   = matmul(retrS%sqrtInvSe,matmul(retrS%K_lnvmr,sqrtSa))

    ! find the singular value decomposition for Kwhite = U W VT

    u = Kwhite
    ! svdcmp overwrites first array (input) with u
    call svdcmp(errS, u, w, v, retrS%nState)
    if (errorCheck(errS)) return
    vT = transpose(v)
    uT = transpose(u)
         
    SInv_trans  = 0.0d0
    Strans      = 0.0d0
    Atrans      = 0.0d0
    do istate = 1, retrS%nState
        SInv_trans(istate,istate) = 1.0d0 + w(istate)**2
        Strans(istate,istate)     = 1.0d0 / SInv_trans(istate,istate)
        Atrans(istate,istate)     = w(istate)**2 * Strans(istate,istate)
    end do

    SInv_white = matmul(v,matmul(SInv_trans,vT))
    Swhite     = matmul(v,matmul(Strans,vT))
    Awhite     = matmul(v,matmul(Atrans,vT))

    retrS%SInv_lnvmr         = matmul(sqrtInvSa,matmul(SInv_white,sqrtInvSa))
    diagnosticS%S_lnvmr      = matmul(sqrtSa,matmul(Swhite,sqrtSa))
    diagnosticS%A_lnvmr      = matmul(sqrtSa,matmul(Awhite,sqrtInvSa))
    diagnosticS%G_lnvmr      = matmul(matmul(diagnosticS%S_lnvmr,transpose(retrS%K_lnvmr)), retrS%InvSe)
    diagnosticS%Snoise_lnvmr = matmul(matmul(diagnosticS%G_lnvmr,retrS%Se),transpose(diagnosticS%G_lnvmr))

    diagnosticS%infoContent_lnvmr = 0.5d0 * sum( log(1.0d0 + w(:)**2) )

    ! calculate smoothed profile, retrS%x_smoothed
    ! differences between the smoothed profile and the retrieved profile are an indication
    ! for the non-linearity of the retrieval

    diagnosticS%x_smoothed = retrS%xa + matmul(diagnosticS%A_lnvmr, retrS%x_true - retrS%xa)

! JdH Debug test SaInv
    !write(intermediateFileUnit, '(A)') 'test SaInv for lnvmr'
    !SaInv = matmul(retrS%Sa_lnvmr, SaInv)
    !do istate = 1, retrS%nState
    !  write(intermediateFileUnit, '(50ES12.3)') SaInv(istate,:)
    !end do

! JdH Debug noise error using Eq. (3.30) from Rodgers (2000)
!   v = matmul(diagnosticS%A_lnvmr,diagnosticS%S_lnvmr)
!   write(intermediateFileUnit,*) 'Snoise - AS Rodgers 2000 Eq. 3.30'
!   do istate = 1, retrS%nState
!     write(intermediateFileUnit, '(30ES12.3)') (v(istate,jstate), jstate=1, retrS%nState)
!   end do
    
    ! vmr
    call invert_Sa(errS, retrS, retrS%Sa_vmr, SaInv, sqrtSa, sqrtInvSa)

    ! calculate dxwhite, dRwhite, and Kwhite
    Kwhite   = matmul(retrS%sqrtInvSe,matmul(retrS%K_vmr,sqrtSa))

    ! find the singular value decomposition for Kwhite = U W VT

    u = Kwhite

    ! svdcmp overwrites first array (input) with u
    call svdcmp(errS, u, w, v, retrS%nState)
    if (errorCheck(errS)) return
    vT = transpose(v)
    uT = transpose(u)
         
    SInv_trans  = 0.0d0
    Strans      = 0.0d0
    Atrans      = 0.0d0
    do istate = 1, retrS%nState
        SInv_trans(istate,istate) = 1.0d0 + w(istate)**2
        Strans(istate,istate)     = 1.0d0 / SInv_trans(istate,istate)
        Atrans(istate,istate)     = w(istate)**2 * Strans(istate,istate)
    end do

    SInv_white = matmul(v,matmul(SInv_trans,vT))
    Swhite     = matmul(v,matmul(Strans,vT))
    Awhite     = matmul(v,matmul(Atrans,vT))

    retrS%SInv_vmr         = matmul(sqrtInvSa,matmul(SInv_white,sqrtInvSa))
    diagnosticS%S_vmr      = matmul(sqrtSa,matmul(Swhite,sqrtSa))
    diagnosticS%A_vmr      = matmul(sqrtSa,matmul(Awhite,sqrtInvSa))
    diagnosticS%G_vmr      = matmul(matmul(diagnosticS%S_vmr,transpose(retrS%K_vmr)), retrS%InvSe)
    diagnosticS%Snoise_vmr = matmul(matmul(diagnosticS%G_vmr,retrS%Se),transpose(diagnosticS%G_vmr))

    diagnosticS%infoContent_vmr = 0.5d0 * sum( log(1.0d0 + w(:)**2) )

    ! separate storage of profile part and 'other' part
    if ( trim(retrS%codeFitParameters(1)) == 'nodeTrace' ) then
      ! profile is fitted
       do istate = 1, diagnosticS%nlevel
         do jstate = 1, diagnosticS%nlevel
           diagnosticS%S_vmr_prof(istate, jstate) = diagnosticS%S_vmr(istate, jstate)
           diagnosticS%A_vmr_prof(istate, jstate) = diagnosticS%A_vmr(istate, jstate)
         end do
       end do

      ! correlation for other fit paparameters
       do istate = diagnosticS%nlevel + 1, diagnosticS%nstate
         do jstate = diagnosticS%nlevel + 1, diagnosticS%nstate
           i = istate - diagnosticS%nlevel
           j = jstate - diagnosticS%nlevel
           diagnosticS%error_correlation_other(i,j) = diagnosticS%S_vmr(istate, jstate) / &
             sqrt( diagnosticS%S_vmr(istate, istate)*diagnosticS%S_vmr(jstate, jstate) )
         end do
       end do
    else
      ! no profile is fitted => consider all state vector elements
       do istate = 1, diagnosticS%nstate
         do jstate = 1, diagnosticS%nstate
           diagnosticS%error_correlation_other(istate,jstate) = diagnosticS%S_vmr(istate, jstate) / &
             sqrt( diagnosticS%S_vmr(istate, istate)*diagnosticS%S_vmr(jstate, jstate) )
         end do
       end do

    end if
    

! JdH Debug test SaInv
    !write(intermediateFileUnit, '(A)') 'test SaInv for vmr'
    !SaInv = matmul(retrS%Sa_vmr, SaInv)
    !do istate = 1, retrS%nState
    !  write(intermediateFileUnit, '(50ES12.3)') SaInv(istate,:)
    !end do

    ! ndens

    call invert_Sa(errS, retrS, retrS%Sa_ndens, SaInv, sqrtSa, sqrtInvSa)

    ! calculate dxwhite, dRwhite, and Kwhite
    Kwhite   = matmul(retrS%sqrtInvSe,matmul(retrS%K_ndens,sqrtSa))

    ! find the singular value decomposition for Kwhite = U W VT

    u = Kwhite
    
    ! svdcmp overwrites first array (input) with u
    call svdcmp(errS, u, w, v, retrS%nState)
    if (errorCheck(errS)) return
    vT = transpose(v)
    uT = transpose(u)
         
    SInv_trans  = 0.0d0
    Strans      = 0.0d0
    Atrans      = 0.0d0
    do istate = 1, retrS%nState
        SInv_trans(istate,istate) = 1.0d0 + w(istate)**2
        Strans(istate,istate)     = 1.0d0 / SInv_trans(istate,istate)
        Atrans(istate,istate)     = w(istate)**2 * Strans(istate,istate)
    end do

    SInv_white = matmul(v,matmul(SInv_trans,vT))
    Swhite     = matmul(v,matmul(Strans,vT))
    Awhite     = matmul(v,matmul(Atrans,vT))

    retrS%SInv_ndens         = matmul(sqrtInvSa,matmul(SInv_white,sqrtInvSa))
    diagnosticS%S_ndens      = matmul(sqrtSa,matmul(Swhite,sqrtSa))
    diagnosticS%A_ndens      = matmul(sqrtSa,matmul(Awhite,sqrtInvSa))
    diagnosticS%G_ndens      = matmul(matmul(diagnosticS%S_ndens,transpose(retrS%K_ndens)), retrS%InvSe)
    diagnosticS%Snoise_ndens = matmul(matmul(diagnosticS%G_ndens,retrS%Se),transpose(diagnosticS%G_ndens))


! JdH Debug test SaInv
!    write(intermediateFileUnit, '(A)') 'test SaInv for ndens'
!    SaInv = matmul(retrS%Sa_ndens, SaInv)
!    do istate = 1, retrS%nState
!      write(intermediateFileUnit, '(50ES12.3)') SaInv(istate,:)
!    end do

    diagnosticS%infoContent_ndens = 0.5d0 * sum( log(1.0d0 + w(:)**2) )

    diagnosticS%DFS = 0.0d0
    do istate = 1, retrS%nstate
      diagnosticS%DFS = diagnosticS%DFS + diagnosticS%A_lnvmr(istate,istate)
    end do

    diagnosticS%DFStrace = 0.0d0
 
    do istate = 1, retrS%nstate
      select case (retrS%codeFitParameters(istate))
        case( 'nodeTrace' )
          iTrace = retrS%codeTraceGas(istate)
          diagnosticS%DFStrace(iTrace) = diagnosticS%DFStrace(iTrace) + diagnosticS%A_lnvmr(istate,istate)
      end select
    end do

    do istate = 1, retrS%nstate
      select case (retrS%codeFitParameters(istate))
        case( 'columnTrace' )
          iTrace = retrS%codeTraceGas(istate)
          diagnosticS%DFStrace(iTrace) = diagnosticS%DFStrace(iTrace) + diagnosticS%A_lnvmr(istate,istate)
      end select
    end do

    diagnosticS%DFSTemperature = 0.0d0
    do istate = 1, retrS%nstate
      select case (retrS%codeFitParameters(istate))
        case( 'nodeTemp' )
          diagnosticS%DFSTemperature = diagnosticS%DFSTemperature + diagnosticS%A_lnvmr(istate,istate)
      end select
    end do

  end subroutine calculateDiagnostics


  subroutine invert_Sa(errS, retrS, Sa, SaInv, sqrtSa, sqrtInvSa)

    ! returns the inverse of the a priori covariance matrix Sa
    ! elements of Sa can have a large dynamic range but some of
    ! the elements that are very large or small are isolated on
    ! the diagonal of Sa.
    ! Hence we first look for diagonal elements, invert them
    ! and use singular value decompostion only for the non-diagonal elements

    type(errorType), intent(inout) :: errS
    type(retrType),  intent(in)    :: retrS
    real(8),         intent(in)    :: Sa(retrS%nState,retrS%nState)
    real(8),         intent(out)   :: SaInv(retrS%nState,retrS%nState)
    real(8),         intent(out)   :: sqrtSa(retrS%nState,retrS%nState)
    real(8),         intent(out)   :: sqrtInvSa(retrS%nState,retrS%nState)
    ! non-diagonal part 
    real(8)   :: SaInvPart(retrS%nSa_not_diag,retrS%nSa_not_diag)
    real(8)   :: sqrtSaPart(retrS%nSa_not_diag,retrS%nSa_not_diag)
    real(8)   :: sqrtInvSaPart(retrS%nSa_not_diag,retrS%nSa_not_diag)

    ! local matrices for svd
    real(8) :: w(retrS%nSa_not_diag)
    real(8) :: ua(retrS%nSa_not_diag, retrS%nSa_not_diag)
    real(8) :: uaT(retrS%nSa_not_diag, retrS%nSa_not_diag)
    real(8) :: v(retrS%nSa_not_diag, retrS%nSa_not_diag)

    real(8) :: diagSaInvPart(retrS%nSa_not_diag, retrS%nSa_not_diag)
    real(8) :: diagsqrtSaPart(retrS%nSa_not_diag, retrS%nSa_not_diag)
    real(8) :: diagsqrtInvSaPart(retrS%nSa_not_diag, retrS%nSa_not_diag)

    ! various
    integer :: i, j

    SaInv             = 0.0d0
    sqrtSa            = 0.0d0
    sqrtInvSa         = 0.0d0

    SaInvPart         = 0.0d0
    sqrtSaPart        = 0.0d0
    sqrtInvSaPart     = 0.0d0

    diagSaInvPart     = 0.0d0
    diagsqrtSaPart    = 0.0d0
    diagsqrtInvSaPart = 0.0d0

    if ( retrS%nSa_not_diag > 1 ) then

      ! fill ua assuming that the non-diagonal part of Sa is (1:nSa_not_diag, 1:nSa_not_diag)
      do i = 1, retrS%nSa_not_diag
        do j = 1, retrS%nSa_not_diag
          ua(i,j) = Sa(i,j)
        end do 
      end do
    ! svdcmp overwrites first array (input) with u
      call svdcmp(errS, ua, w, v, retrS%nSa_not_diag)
      if (errorCheck(errS)) return
      uaT = transpose(ua)

      do i = 1, retrS%nSa_not_diag
        ! rounding errors can make w negative for profile retrieval of O2-O2
        ! eliminate negative singular eigenvalues
        if ( w(i) > 0.0d0 ) then
           diagSaInvPart(i,i)     = 1.0d0 / w(i)
           diagsqrtSaPart(i,i)    = sqrt(w(i))
           diagsqrtInvSaPart(i,i) = 1.0d0 / sqrt(w(i))
        else
           diagSaInvPart(i,i)     = 0.0d0
           diagsqrtSaPart(i,i)    = 0.0d0
           diagsqrtInvSaPart(i,i) = 0.0d0
        end if
      end do

      sqrtInvSaPart = matmul(v, matmul(diagsqrtInvSaPart, uaT) )
      sqrtSaPart    = matmul(v, matmul(diagsqrtSaPart   , uaT) )
      SaInvPart     = matmul(v, matmul(diagSaInvPart    , uaT) )

      do i = 1, retrS%nSa_not_diag
        do j = 1, retrS%nSa_not_diag
         sqrtInvSa(i,j) = sqrtInvSaPart(i,j)
         sqrtSa(i,j)    = sqrtSaPart(i,j)   
         SaInv(i,j)     = SaInvPart(i,j)    
        end do 
      end do

    end if ! retrS%nSa_not_diag > 1

    do i = retrS%nSa_not_diag + 1, retrS%nstate
      SaInv(i,i)     = 1.0d0 / Sa(i,i)
      sqrtSa(i,i)    = sqrt(Sa(i,i))
      sqrtInvSa(i,i) = 1.0d0 / sqrt(Sa(i,i))
    end do

  end subroutine invert_Sa


  subroutine evaluateSpectralFeatures(errS, numSpectrBands, nTrace, controlS, wavelInstrS, cloudAerosolRTMgridS,  &
                                      solarIrradianceS, earthRadianceS, retrS, diagnosticS, traceGasS)

    implicit none

    ! The gain matrix that translates error in the sun-normalized radiance (also called reflectance here)
    ! is used to evaluate the errors in the retrieved parameters due to sinusoidal features in the
    ! solar irradiance, earth radiance, and reflectance.
    ! The expression G d_refl = dx is transformed into an integral over the wavelength.
    ! The sinusoidal features are defined as
    ! sine((lamda - lamda_start) * 2 * PI /period + phase * PI /180.0d0 )
    ! lamda_start is the first wavelength (index = 0) of the spectral band
    ! period is the period in nm
    ! phase is the phase in degree
    ! sinusoidal feature in the solar irradiance and earth radiance are translated into
    ! features in the reflectance so that G calculated for the reflectance can be used.
    ! results are calculated for all spectral bands, and the features are applied to one spectral band
    ! so that the effect of features in one band on retrieved parameters in another band can be
    ! evaluated.

      type(errorType), intent(inout) :: errS
    integer,                       intent(in) :: numSpectrBands
    integer,                       intent(in) :: nTrace
    type(controlType),             intent(in) :: controlS
    type(wavelInstrType),          intent(in) :: wavelInstrS(numSpectrBands)
    type(cloudAerosolRTMgridType), intent(in) :: cloudAerosolRTMgridS
    type(SolarIrrType),            intent(in) :: solarIrradianceS(numSpectrBands)
    type(EarthRadianceType),       intent(in) :: earthRadianceS(numSpectrBands)
    type(retrType),                intent(in) :: retrS
    type(diagnosticType),          intent(in) :: diagnosticS
    type(traceGasType),            intent(in) :: traceGasS(nTrace)


    ! local
    integer, parameter    :: ngauss = 4                          ! number of gauss points per spectral bin
    real(8)               :: x0(ngauss), w0(ngauss)              ! gaussian division points and weights 
                                                                 ! on (0,wavelInstrS(iband)%stepWavel)
    real(8), allocatable  :: gauss_grid(:), weight_gauss_grid(:)

    integer               :: iband                               ! spectral band index
    integer               :: gain_index_start, gain_index_end    ! used for selection of the spectral band
    integer               :: iwave                               ! index spectral bin
    integer               :: igauss                              ! index gauss point
    integer               :: index                               ! index gauss point
    integer               :: statusSpline, statusSplint
    real(8)               :: waveStart

    real(8), allocatable  :: gainAdd(:,:)                        ! gain normalized for additive features
    real(8), allocatable  :: gainMul(:,:)                        ! gain normalized for multiplicative features
    real(8), allocatable  :: SDgain(:)                           ! second derivatives for spline interpolation
    real(8), allocatable  :: gain_gauss_gridAdd(:, :)            ! gain normalized and interpolated to gauss grid
    real(8), allocatable  :: gain_gauss_gridMul(:, :)            ! gain normalized and interpolated to gauss grid

    ! specification of phases and period of cosine
    real(8), parameter    :: PI = 3.141592653589793d0
    integer   , parameter :: nphase = 7         ! number of phase for the cosine
    real(8)               :: phase(nphase)      ! phase of cosine at start wavelength given in degree
    integer               :: nperiod            ! number of periods
    real(8), allocatable  :: period(:)          ! period for the wavelength feature in nm
    real(8)               :: tmp
    real(8)               :: startPeriod        ! smallest period used e.g. one thenth of a spectral bin size      
    real(8)               :: factorIncrPeriod   ! factor with which the period increases

    integer               :: iphase, iperiod, istate, icase, ialt, iTrace
    integer   ,parameter  :: maxNumPeriod = 400

    real(8)               :: refl_mean, irr_mean, rad_mean

    real(8), allocatable  :: relErrorAdd(:,:)   !(nperiod,nphase)  relative error for a given istate and iband
    real(8), allocatable  :: relErrorMul(:,:)   !(nperiod,nphase)  relative error for a given istate and iband

    integer :: sumAllocStatus, allocStatus
    integer :: sumdeAllocStatus, deallocStatus 

    logical, parameter    :: writeNormGainMatrix = .false.

    ! return if no spectral featurs are specified in the configuration file
    if ( .not. controlS%writeSpecFeatIrr .and. .not.controlS%writeSpecFeatRad &
         .and. .not.controlS%writeSpecFeatRefl)  return

    ! assign values to the phase at the start of the window
    phase = (/0.0d0, 30.0d0, 60.0d0, 90.0d0, 120.0d0, 150.0d0, 180.0d0/)

    gain_index_start = 1
    gain_index_end   = 0

    do iband = 1, numSpectrBands

      gain_index_end   = gain_index_end + wavelInstrS(iband)%nwavel

      ! determine the number of periods
      ! start at spectral bin size and end at 18 times the spectral window
      startPeriod = wavelInstrS(iband)%stepWavel
      tmp = startPeriod
      factorIncrPeriod = 1.05d0
      index = 1
      do
        index = index + 1
        tmp = tmp * factorIncrPeriod
        if ( tmp > 18.0d0 * (wavelInstrS(iband)%endWavel - wavelInstrS(iband)%startWavel) ) exit
        if ( index + 1> maxNumPeriod ) exit ! just to make sure that do loop ends
      end do

      nperiod = index

      allocStatus    = 0
      sumAllocStatus = 0

      allocate( period(nperiod), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate ( gauss_grid( ngauss*wavelInstrS(iband)%nwavel ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate ( weight_gauss_grid( ngauss*wavelInstrS(iband)%nwavel ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      ! allocate gain
      allocate ( gainAdd(wavelInstrS(iband)%nwavel, retrS%nstate ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate ( gainMul(wavelInstrS(iband)%nwavel, retrS%nstate ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate ( SDgain (wavelInstrS(iband)%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate ( gain_gauss_gridAdd(ngauss*wavelInstrS(iband)%nwavel, retrS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate ( gain_gauss_gridMul(ngauss*wavelInstrS(iband)%nwavel, retrS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( relErrorAdd(nperiod,nphase), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( relErrorMul(nperiod,nphase), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      if ( sumAllocStatus /= 0 ) then
        call logDebug('FATAL ERROR: allocation failed')
        call logDebug('when calculating effects of spectral features')
        call logDebug('in subroutine evaluateSpectralFeatures')
        call logDebug('in module optimalEstimationModule')
        call mystop(errS, 'stopped because allocation failed')
        if (errorCheck(errS)) return
      end if

      ! fill the array with the periods
      period(1) =  startPeriod
      do iperiod = 2, nperiod
        period(iperiod)  = period(iperiod - 1) * factorIncrPeriod
      end do

      ! calculate gaussian division points and weight on (0,stepWavel)
      call gaussDivPoints(errS, 0.0d0, wavelInstrS(iband)%stepWavel, x0, w0, ngauss)    
      if (errorCheck(errS)) return

      ! fill gauss values on gauss grid
      index = 0
      do iwave = 1, wavelInstrS(iband)%nwavel
        waveStart = wavelInstrS(iband)%startWavel + (iwave - 1) * wavelInstrS(iband)%stepWavel
        do igauss = 1, ngauss
          index = index + 1
          gauss_grid(index)        = waveStart + x0(igauss)
          weight_gauss_grid(index) = w0(igauss) / wavelInstrS(iband)%stepWavel
        end do ! igauss
      end do ! iwave


      ! In order to get errors for the solar irradiance, earth radiance, and sun-normalized radiance
      ! we multiply the gain matrices with appropriate factors as these are given on the instrument
      ! grid, instead of multiplying the errors (sines) wich are given on the integration grid.

      ! G * amplitude_add * refl_mean = dx   where amplitude_add is the amplitude of the additive feature
      !                                            refl_mean is the mean reflectance in the band
      !                                            dx is the cange in the state vector
      ! G * amplitude_mul * refl      = dx   where amplitude_mul is the amplitude of the multiplicative feature
      !                                            refl is the reflectance that varies with wavelength
      !                                            dx is the cange in the state vector

      ! To get the error in percent for the state vector element we have to multiply with 100 and dived by x
      ! 100 * G * amplitude_add * refl_mean / x  = dx(%)
      ! 100 * G * amplitude_mul * refl / x       = dx(%)     

      ! gainMul and gainAdd give the multiplicative and additive error, repectively, for the 
      ! state vector element in percent for a feature with an amplitude of 1%

      ! gainMul = G * refl / x
      ! gainAdd = G * refl_mean / x

      ! The above expressions hold for the reflectance (sun-normalized radiance)
      ! for the solar irradiance and the earth radiance we have to translate the disturbances
      ! into disturbance in the reflectance. Using refl = rad / irr it is directly evendent
      ! that a multiplicative error in the radiance is the same as a multiplicative error in the reflectance.
      ! For the solar irradiance we can use a Taylor expansion and ignore higher orders, which
      ! yields a sign change. Hence we have

      ! gainMul = - G * refl / x   ! solar irradiance
      ! gainMul =   G * refl / x   ! earth radiance

      ! For the additive error in the irradiance we use refl = rad / [irr + a * irr_mean * sin(phi)]
      ! refl = [rad / irr] / [ 1 + a * sin(phi) * irr_mean / irr ] ) and using 1/(1 + eps ) = 1 - eps
      ! refl = refl * [ 1 - a * sin(phi) * irr_mean / irr ] which means that the disturbanc of the
      ! reflectance is - a * sin(phi) * refl * irr_mean / irr    instead of a * sin(phi) for the reflectance
      ! note that a is already normalized, which yields 

      ! gainAdd = - G * refl * irr_mean / irr / x     ! for solar irradiance

      ! For the additive error in the earth radiance we use refl = [rad + a * rad_mean * sin(phi)] / irr
      ! and the disturbance of the reflectance is a * sin(phi)* rad_mean / irr  which yields

      ! gainAdd = G * rad_mean / irr / x               ! for earth radiance

      refl_mean = sum( retrS%refl(gain_index_start:gain_index_end) ) / (gain_index_end - gain_index_start + 1)
      irr_mean  = sum( solarIrradianceS(iband)%solIrr(:) ) / (solarIrradianceS(iband)%nwavel )
      rad_mean  = sum( earthRadianceS(iband)%rad(1,:) ) / (earthRadianceS(iband)%nwavel )

      do icase = 1, 3

        select case (icase)

          case (1) ! solar irradiance

            if ( .not. controlS%writeSpecFeatIrr ) cycle  ! go to next value of icase

            write(addtionalOutputUnit,*)
            write(addtionalOutputUnit,*) 'FEATURES FOR SOLAR IRRADIANCE'
            write(addtionalOutputUnit,*)

            do istate = 1, retrS%nstate
              gainMul(1:wavelInstrS(iband)%nwavel, istate) = &
                    - diagnosticS%G_ndens(istate, gain_index_start:gain_index_end)       &
                      * retrS%refl(gain_index_start:gain_index_end)
              gainAdd(1:wavelInstrS(iband)%nwavel, istate) = &
                    - diagnosticS%G_ndens(istate, gain_index_start:gain_index_end)       &
                      * retrS%refl(gain_index_start:gain_index_end) * irr_mean           &
                      / solarIrradianceS(iband)%solIrr(solarIrradianceS(iband)%nwavel)
             end do ! istate

          case (2) ! earth radiance

            if ( .not. controlS%writeSpecFeatRad ) cycle  ! go to next value of icase

            write(addtionalOutputUnit,*)
            write(addtionalOutputUnit,*) 'FEATURES FOR EARTH RADIANCE'
            write(addtionalOutputUnit,*)

            do istate = 1, retrS%nstate
              gainMul(1:wavelInstrS(iband)%nwavel, istate) = &
                      diagnosticS%G_ndens(istate, gain_index_start:gain_index_end) &
                      * retrS%refl(gain_index_start:gain_index_end)
              gainAdd(1:wavelInstrS(iband)%nwavel, istate) =                                  &
                      diagnosticS%G_ndens(istate, gain_index_start:gain_index_end) * rad_mean &
                      / solarIrradianceS(iband)%solIrr(solarIrradianceS(iband)%nwavel) 
             end do ! istate

          case (3) ! reflectance or sun-normalized radiance

            if ( .not. controlS%writeSpecFeatRefl ) cycle  ! go to next value of icase

            write(addtionalOutputUnit,*)
            write(addtionalOutputUnit,*) 'FEATURES FOR SUN-NORMALIZED RADIANCE'
            write(addtionalOutputUnit,*)

            do istate = 1, retrS%nstate
              gainMul(1:wavelInstrS(iband)%nwavel, istate) = &
                      diagnosticS%G_ndens(istate, gain_index_start:gain_index_end) &
                      * retrS%refl(gain_index_start:gain_index_end)
              gainAdd(1:wavelInstrS(iband)%nwavel, istate) = &
                      diagnosticS%G_ndens(istate, gain_index_start:gain_index_end) &
                      * refl_mean
             end do ! istate

          case default
            call logDebug('ERROR: optimalEstmationModule:evaluateSpectralFeatures')
            call logDebug('incorrect value of icase')
            call mystop(errS, 'stopped due to incorrect value of icase')
            if (errorCheck(errS)) return
        end select

        do istate = 1, retrS%nstate

          select case (retrS%codeFitParameters(istate))

            case( 'nodeTrace' )

              
              ! natural logarithm of the volume mixing ratio is fitted and is stored in x
              ! while G is expected to operate on the number density of the trace gas
              ! hence, we have to divide the gain matrix by the number density to get
              ! the proper relative error 
                ialt   = retrS%codeAltitude(istate)
                iTrace = retrS%codeTraceGas(istate)
                gainMul(:,istate) = gainMul(:,istate) / traceGasS(iTrace)%numDens(ialt)
                gainAdd(:,istate) = gainAdd(:,istate) / traceGasS(iTrace)%numDens(ialt)

            case( 'columnTrace' )

              ! natural logarithm of the column, and aerosolTau is fitted
              ! therefore we neeed the exp of these parameters to calculate the relative error
              ! note that diagnosticS%G_ndens is expeted to operate on the number densitiy,
              ! the total column, the surface albedo, and the aerosol extinction coefficent, not
              ! on the logarithms of these values  
              
                gainMul(:,istate) = gainMul(:,istate) / exp( retrS%x(istate))
                gainAdd(:,istate) = gainAdd(:,istate) / exp( retrS%x(istate))

            case('cloudFraction', 'straylight', 'intervalDP', 'intervalTop', 'intervalBot', 'aerosolSSA', &
                 'aerosolAC', 'LambCldAlbedo', 'cloudAC', 'nodeTemp', 'offsetTemp', 'surfAlbedo',         &
                 'surfEmission', 'surfPressure', 'diffRingCoef', 'RingCoef', 'mulOffset', 'aerosolTau')

              ! the value itself, not the log of the value is fitted
              if ( abs( retrS%x(istate) ) > 1.0d-100 ) then 
                gainMul(:,istate) = gainMul(:,istate) / retrS%x(istate)
                gainAdd(:,istate) = gainAdd(:,istate) / retrS%x(istate)
              else
                gainMul(:,istate) = 0.0d0
                gainAdd(:,istate) = 0.0d0
              end if

            case('cloudTau')

              if ( cloudAerosolRTMgridS%fitLnCldTau ) then
                gainMul(:,istate) = gainMul(:,istate) / exp( retrS%x(istate) )
                gainAdd(:,istate) = gainAdd(:,istate) / exp( retrS%x(istate) )
              else
                if ( abs( retrS%x(istate) ) > 1.0d-100 ) then 
                  gainMul(:,istate) = gainMul(:,istate) / retrS%x(istate)
                  gainAdd(:,istate) = gainAdd(:,istate) / retrS%x(istate)
                else
                  gainMul(:,istate) = 0.0d0
                  gainAdd(:,istate) = 0.0d0
                end if
              end if

            case default
              call logDebug('in subroutine evaluateSpectralFeatures in optimalEstimationModule')
              call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
              call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
              if (errorCheck(errS)) return
          end select

        end do ! istate

        ! interpolate gain values to gaussian grid
        do istate = 1, retrS%nstate
          call spline(errS, wavelInstrS(iband)%wavel(:),gainAdd(:, istate), SDgain(:), statusSpline )
          if (errorCheck(errS)) return
          do iwave = 1, ngauss * wavelInstrS(iband)%nwavel
            gain_gauss_gridAdd(iwave,istate) = splint(errS, wavelInstrS(iband)%wavel(:),gainAdd(:,istate), &
                                               SDgain(:), gauss_grid(iwave), statusSplint)
          end do ! iwave
          call spline(errS, wavelInstrS(iband)%wavel(:),gainMul(:, istate), SDgain(:), statusSpline )
          if (errorCheck(errS)) return
          do iwave = 1, ngauss * wavelInstrS(iband)%nwavel
            gain_gauss_gridMul(iwave,istate) = splint(errS, wavelInstrS(iband)%wavel(:),gainMul(:,istate), &
                                               SDgain(:), gauss_grid(iwave), statusSplint)
          end do ! iwave
        end do ! istate

        ! write to output the normalized gain matrix
        if ( writeNormGainMatrix ) then
          write(addtionalOutputUnit,*)
          write(addtionalOutputUnit,'(A)') 'normalized gain matrix for addditive error'
          write(addtionalOutputUnit,'(A,100A17)') 'wavelength(nm)', &
                               (retrS%codeFitParameters(istate), istate = 1, retrS%nstate)
          do iwave = 1, ngauss * wavelInstrS(iband)%nwavel
            write(addtionalOutputUnit,'(F12.4, 2X,100E17.8)') gauss_grid(iwave), &
                 (gain_gauss_gridAdd(iwave, istate), istate = 1, retrS%nstate) 
          end do ! iwave

           write(addtionalOutputUnit,*)
           write(addtionalOutputUnit,'(A)') 'normalized gain matrix for multiplicative error'
           write(addtionalOutputUnit,'(A,100A17)') 'wavelength(nm)', &
                                (retrS%codeFitParameters(istate), istate = 1, retrS%nstate)
           do iwave = 1, ngauss * wavelInstrS(iband)%nwavel
             write(addtionalOutputUnit,'(F12.4, 2X,100E17.8)') gauss_grid(iwave), &
                  (gain_gauss_gridMul(iwave, istate), istate = 1, retrS%nstate) 
           end do ! iwave
        end if

        do istate = 1, retrS%nstate
          relErrorAdd = 0.0d0
          relErrorMul = 0.0d0
          do iphase = 1, nphase
            do iperiod = 1, nperiod
              do iwave = 1, ngauss * wavelInstrS(iband)%nwavel
                relErrorAdd(iperiod,iphase) = relErrorAdd(iperiod,iphase) +  weight_gauss_grid(iwave)      &
                 * gain_gauss_gridAdd(iwave, istate)                                                       &
                 * sin((gauss_grid(iwave)- gauss_grid(1)) * 2.0d0*PI /period(iperiod) + phase(iphase) * PI/180.0d0 )
                relErrorMul(iperiod,iphase) = relErrorMul(iperiod,iphase) +  weight_gauss_grid(iwave)      &
                 * gain_gauss_gridMul(iwave, istate)                                                       &
                 * sin((gauss_grid(iwave)- gauss_grid(1)) * 2.0d0*PI /period(iperiod) + phase(iphase) * PI/180.0d0 )
              end do ! iwave
            end do ! iperiod
          end do ! iphase
          ! write to output values to identify the state vector element
          write(addtionalOutputUnit,*)
          write(addtionalOutputUnit,'(A)') retrS%codeFitParameters(istate)
          write(addtionalOutputUnit,'(A,I4)') 'spectral band number = ', retrS%codeSpecBand(istate)
          if ( retrS%codeFitParameters(istate) == 'nodeTrace' ) then
          write(addtionalOutputUnit,'(A,I4,F10.3)') 'altitude number, pressue (hPA) = ', &
            retrS%codeAltitude(istate), traceGasS(retrS%codeTraceGas(istate))%pressure(retrS%codeAltitude(istate))
          else 
            write(addtionalOutputUnit,'(A,I4)') 'altitude number      = ', retrS%codeAltitude(istate)
          end if
          write(addtionalOutputUnit,'(A,I4)') 'trace gas number            = ', retrS%codeTraceGas(istate) 
          write(addtionalOutputUnit,'(A,I4)') 'surface albedo index        = ', retrS%codeIndexSurfAlb(istate) 
          write(addtionalOutputUnit,'(A,I4)') 'surface emission index      = ', retrS%codeIndexSurfEmission(istate) 
          write(addtionalOutputUnit,'(A,I4)') 'LambCld albedo index        = ', retrS%codeIndexLambCldAlb(istate) 
          write(addtionalOutputUnit,'(A,I4)') 'cloud fraction index        = ', retrS%codeIndexCloudFraction(istate) 
          write(addtionalOutputUnit,'(A,I4)') 'stray light index           = ', retrS%codeIndexStraylight(istate)
          write(addtionalOutputUnit,'(A,I4)') 'multiplicative offset index = ', retrS%codeIndexStraylight(istate)
          ! write to output the relative errors
          write(addtionalOutputUnit,*)
          write(addtionalOutputUnit,*) 'relative errors in percent for multiplicative spectral features of 1% amplitude'
          write(addtionalOutputUnit,'(A,20F15.3)') 'period\phase', (phase(iphase), iphase = 1, nphase)
          do iperiod = 1, nperiod
            write(addtionalOutputUnit,'(F12.3,20ES15.5)') period(iperiod), &
                                                  (relErrorMul(iperiod,iphase), iphase = 1, nphase)
          end do ! iperiod
          write(addtionalOutputUnit,*)
          write(addtionalOutputUnit,*) 'relative errors in percent for additive spectral features of 1% amplitude'
          write(addtionalOutputUnit,*) 'the 1% amplitude is for the first wavelength of the spectral band'
          write(addtionalOutputUnit,'(A,20F15.3)') 'period\phase', (phase(iphase), iphase = 1, nphase)
          do iperiod = 1, nperiod
            write(addtionalOutputUnit,'(F12.3,20ES15.5)') period(iperiod), &
                                                  (relErrorAdd(iperiod,iphase), iphase = 1, nphase)
          end do ! iperiod
        end do ! istate
      end do ! icase     


      deallocStatus    = 0
      sumdeAllocStatus = 0

      deallocate ( period, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate ( relErrorAdd, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate ( relErrorMul, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate ( gain_gauss_gridAdd, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate ( gain_gauss_gridMul, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate ( weight_gauss_grid, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate ( gauss_grid, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate ( gainAdd, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate ( gainMul, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate ( SDgain, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus

      if ( sumdeAllocStatus /= 0 ) then
        call logDebug('FATAL ERROR: deallocation failed')
        call logDebug('when calculating effects of spectral features')
        call logDebug('in subroutine evaluateSpectralFeatures')
        call logDebug('in module optimalEstimationModule')
        call mystop(errS, 'stopped because deallocation failed')
        if (errorCheck(errS)) return
      end if


      gain_index_start =  gain_index_start +  wavelInstrS(iband)%nwavel + 1

    end do ! iband

  end subroutine evaluateSpectralFeatures


end module optimalEstmationModule
