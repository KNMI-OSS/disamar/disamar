!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

! -----------------------------------------------------------------------------
!
! TROPOMI L2 Data Processors
!
! Copyright 2013 - 2018 KNMI, DLR, RAL, ESA
! All rights Reserved
!
! This is a generated file (created at 2018-03-02T12:30:16.380049)
!
!> \file pqf_module.f90
!! Module file for the list of processing quality flags.

!> Module for the list of processing quality flags.
!!
!! This module contains a list of all the processing quality flags, including those that
!! cannot occur in this specific algorithm. The values of these processing quality flags
!! is the same for all algorithms of S5P. Values given as a power of 2 are warning flags,
!! which means that they can co-exist with other flags.
!!
!! Note that this module must be compiled with ``-fno-range-check'', for the PQF_WARNING_MASK
!! constant.

! S = pixel contains valid retrieval data
! W = pixel contains retrieval data but should be used with care
! E = pixel does not contain retrieval data (with partial exceptions in CO).


! -----------------------------------------------------------------------------

module pqf_module

  implicit none

  ! Masks for general qualification of PQF values

  integer, parameter :: PQF_ERROR_MASK                                   = Z'000000FF' !< Mask to select only errors.
  integer, parameter :: PQF_WARNING_MASK                                 = Z'7FFFFF00' !< Mask to select only warnings
  integer, parameter :: PQF_MAX_NUMBER_OF_WARNINGS                       = 23          !< Maximum number of warning flags
  integer, parameter :: PQF_CURRENT_NUMBER_OF_WARNINGS                   = 19          !< Current number of defined warnings
  integer, parameter :: PQF_FILTER_MASK                                  = Z'000000C0' !< Maks to select filters.


! Processing failures and filter conditions
  integer, parameter :: PQF_S_SUCCESS                                    = Z'00000000'  !< No failures, output contains value. Warnings still possible.
  integer, parameter :: PQF_E_RADIANCE_MISSING                           = Z'00000001'  !< The number of spectral pixels in the radiance due to flagging is too small to perform the fitting.
  integer, parameter :: PQF_E_IRRADIANCE_MISSING                         = Z'00000002'  !< The number of spectral pixels in the irradiance due to flagging is too small to perform the fitting.
  integer, parameter :: PQF_E_INPUT_SPECTRUM_MISSING                     = Z'00000003'  !< The reflectance spectrum does not contain enough points to perform the retrieval. This is different from (ir)radiance\_missing in that the missing points may not be aligned.
  integer, parameter :: PQF_E_REFLECTANCE_RANGE_ERROR                    = Z'00000004'  !< Any of the reflectances is out of bounds ($R<0$ or $R> R_{\text{max}}$).
  integer, parameter :: PQF_E_LER_RANGE_ERROR                            = Z'00000005'  !< Lambert-equivalent reflectivity out of range error.
  integer, parameter :: PQF_E_SNR_RANGE_ERROR                            = Z'00000006'  !< Too low signal to noise to perform retrieval.
  integer, parameter :: PQF_E_SZA_RANGE_ERROR                            = Z'00000007'  !< Solar zenith angle out of range, maximum value from configuration.
  integer, parameter :: PQF_E_VZA_RANGE_ERROR                            = Z'00000008'  !< Viewing zenith angle out of range, maximum value from configuration.
  integer, parameter :: PQF_E_LUT_RANGE_ERROR                            = Z'00000009'  !< Extrapolation in lookup table (airmass factor, cloud radiances).
  integer, parameter :: PQF_E_OZONE_RANGE_ERROR                          = Z'0000000A'  !< Ozone column significantly out of range of profile climatology.
  integer, parameter :: PQF_E_WAVELENGTH_OFFSET_ERROR                    = Z'0000000B'  !< Wavelength offset exceeds maximum from configuration.
  integer, parameter :: PQF_E_INITIALIZATION_ERROR                       = Z'0000000C'  !< An error occurred during the processing of the pixel, no output was generated. The following errors raise this flag: Mismatch between irradiance and radiance wavelengths; The on-ground distance between band 1 and band 2 ground pixels exceeds a threshold set in the configuration. Derived a-priori information does not validate, no processing is possible.
  integer, parameter :: PQF_E_MEMORY_ERROR                               = Z'0000000D'  !< Memory allocation or deallocation error.
  integer, parameter :: PQF_E_ASSERTION_ERROR                            = Z'0000000E'  !< Error in algorithm detected during assertion.
  integer, parameter :: PQF_E_IO_ERROR                                   = Z'0000000F'  !< Error detected during transfer of data between algorithm and framework.
  integer, parameter :: PQF_E_NUMERICAL_ERROR                            = Z'00000010'  !< General fatal numerical error occurred during inversion.
  integer, parameter :: PQF_E_LUT_ERROR                                  = Z'00000011'  !< Error in accessing the lookup table.
  integer, parameter :: PQF_E_ISRF_ERROR                                 = Z'00000012'  !< Error detected in the input instrument spectral response function input data.
  integer, parameter :: PQF_E_CONVERGENCE_ERROR                          = Z'00000013'  !< The main algorithm did not converge.
  integer, parameter :: PQF_E_CLOUD_FILTER_CONVERGENCE_ERROR             = Z'00000014'  !< The cloud filter did not converge.
  integer, parameter :: PQF_E_MAX_ITERATION_CONVERGENCE_ERROR            = Z'00000015'  !< No convergence because retrieval exceeds maximum number of iterations. Maximum value from configuration.
  integer, parameter :: PQF_E_AOT_LOWER_BOUNDARY_CONVERGENCE_ERROR       = Z'00000016'  !< No convergence because the aerosol optical thickness crosses lower boundary twice in succession.
  integer, parameter :: PQF_E_OTHER_BOUNDARY_CONVERGENCE_ERROR           = Z'00000017'  !< No convergence because a state vector element crosses boundary twice in succession. Note that a separate failure flag is defined for non-convergence due to crossing of lower AOT boundary.
  integer, parameter :: PQF_E_GEOLOCATION_ERROR                          = Z'00000018'  !< Geolocation out of range.
  integer, parameter :: PQF_E_CH4_NOSCAT_ZERO_ERROR                      = Z'00000019'  !< The \Methane column retrieved by the non-scattering \Carbonmonoxide algorithm from the weak  band or strong band is 0.
  integer, parameter :: PQF_E_H2O_NOSCAT_ZERO_ERROR                      = Z'0000001A'  !< The \Water column retrieved by the non-scattering \Carbonmonoxide algorithm from the weak  band or strong band is 0.
  integer, parameter :: PQF_E_MAX_OPTICAL_THICKNESS_ERROR                = Z'0000001B'  !< Maximum optical thickness exceeded during iterations.
  integer, parameter :: PQF_E_AEROSOL_BOUNDARY_ERROR                     = Z'0000001C'  !< Boundary hit of aerosol parameters at last iteration.
  integer, parameter :: PQF_E_BOUNDARY_HIT_ERROR                         = Z'0000001D'  !< Fatal boundary hit during iterations.
  integer, parameter :: PQF_E_CHI2_ERROR                                 = Z'0000001E'  !< $\chi^2$ is not-a-number or larger than $10^{10}$.
  integer, parameter :: PQF_E_SVD_ERROR                                  = Z'0000001F'  !< Singular value decomposition failure.
  integer, parameter :: PQF_E_DFS_ERROR                                  = Z'00000020'  !< Degree of freedom is not-a-number.
  integer, parameter :: PQF_E_RADIATIVE_TRANSFER_ERROR                   = Z'00000021'  !< Errors occurred during the radiative transfer computations, no processing possible.
  integer, parameter :: PQF_E_OPTIMAL_ESTIMATION_ERROR                   = Z'00000022'  !< Errors occurred during the optimal estimation, processing has been terminated.
  integer, parameter :: PQF_E_PROFILE_ERROR                              = Z'00000023'  !< Flag that indicates if there were any errors during the computation of the ozone profile.
  integer, parameter :: PQF_E_CLOUD_ERROR                                = Z'00000024'  !< No cloud data.
  integer, parameter :: PQF_E_MODEL_ERROR                                = Z'00000025'  !< Forward model failure.
  integer, parameter :: PQF_E_NUMBER_OF_INPUT_DATA_POINTS_TOO_LOW_ERROR  = Z'00000026'  !< Not enough input ozone columns to calculate a tropospheric column.
  integer, parameter :: PQF_E_CLOUD_PRESSURE_SPREAD_TOO_LOW_ERROR        = Z'00000027'  !< Cloud pressure variability to low to estimate a tropospheric column.
  integer, parameter :: PQF_E_CLOUD_TOO_LOW_LEVEL_ERROR                  = Z'00000028'  !< Clouds are too low in the atmosphere to assume sufficient shielding.
  integer, parameter :: PQF_E_GENERIC_RANGE_ERROR                        = Z'00000029'  !< Generic range error.
  integer, parameter :: PQF_E_GENERIC_EXCEPTION                          = Z'0000002A'  !< Catch all generic error.
  integer, parameter :: PQF_E_INPUT_SPECTRUM_ALIGNMENT_ERROR             = Z'0000002B'  !< Input radiance and irradiance spectra are not aligned correctly.
  integer, parameter :: PQF_E_ABORT_ERROR                                = Z'0000002C'  !< Not processed because processor aborted prematurely (time out or user abort)
  integer, parameter :: PQF_E_WRONG_INPUT_TYPE_ERROR                     = Z'0000002D'  !< Wrong input type error, mismatch between expectation and received data.
  integer, parameter :: PQF_E_WAVELENGTH_CALIBRATION_ERROR               = Z'0000002E'  !< An error occurred in the wavelength calibration of this pixel
  integer, parameter :: PQF_E_COREGISTRATION_ERROR                       = Z'0000002F'  !< No colocated pixels found in a supporting band
  integer, parameter :: PQF_E_SLANT_COLUMN_DENSITY_ERROR                 = Z'00000030'  !< Slant column fit returned error, no values can be computed
  integer, parameter :: PQF_E_AIRMASS_FACTOR_ERROR                       = Z'00000031'  !< Airmass factor could not be computed
  integer, parameter :: PQF_E_VERTICAL_COLUMN_DENSITY_ERROR              = Z'00000032'  !< vertical column density could not be computed
  integer, parameter :: PQF_E_SIGNAL_TO_NOISE_RATIO_ERROR                = Z'00000033'  !< The signal to noise ratio for this spectrum is too low for processing
  integer, parameter :: PQF_E_CONFIGURATION_ERROR                        = Z'00000034'  !< Error while parsing the configuration
  integer, parameter :: PQF_E_KEY_ERROR                                  = Z'00000035'  !< Key does not exist
  integer, parameter :: PQF_E_SATURATION_ERROR                           = Z'00000036'  !< Saturation in input spectrum
  integer, parameter :: PQF_F_SOLAR_ECLIPSE_FILTER                       = Z'00000040'  !< Solar eclipse.
  integer, parameter :: PQF_F_CLOUD_FILTER                               = Z'00000041'  !< The cloud filter triggered causing the pixel to be skipped.
  integer, parameter :: PQF_F_ALTITUDE_CONSISTENCY_FILTER                = Z'00000042'  !< Too large difference between ECMWF altitude and DEM altitude value.
  integer, parameter :: PQF_F_ALTITUDE_ROUGHNESS_FILTER                  = Z'00000043'  !< Too large standard deviation of altitude in DEM.
  integer, parameter :: PQF_F_SUN_GLINT_FILTER                           = Z'00000044'  !< For pixels over water, viewing direction inside sun glint region. Definition of sun glint angle and threshold value from ATBD.
  integer, parameter :: PQF_F_MIXED_SURFACE_TYPE_FILTER                  = Z'00000045'  !< Pixel contains land and water areas (e.g. coastal pixel).
  integer, parameter :: PQF_F_SNOW_ICE_FILTER                            = Z'00000046'  !< Pixel contains snow/ice: Snow/ice flag according to dynamic input OR climatological surface albedo at VIS wavelength is larger than 0.5.
  integer, parameter :: PQF_F_AAI_FILTER                                 = Z'00000047'  !< AAI smaller than 2.0.
  integer, parameter :: PQF_F_CLOUD_FRACTION_FRESCO_FILTER               = Z'00000048'  !< Pixel contains clouds: The FRESCO effective cloud fraction is larger than threshold. Threshold value from ATBD.
  integer, parameter :: PQF_F_AAI_SCENE_ALBEDO_FILTER                    = Z'00000049'  !< Pixel contains clouds: The difference between scene albedo at 380 nm from AAI calculation and the climatologcal surface albedo exceeds threshold. Threshold value from ATBD. This test filters out clouds.
  integer, parameter :: PQF_F_SMALL_PIXEL_RADIANCE_STD_FILTER            = Z'0000004A'  !< Pixel contains clouds: Standard deviation of radiances in small-pixel column exceeds threshold. Threshold value from ATBD.
  integer, parameter :: PQF_F_CLOUD_FRACTION_VIIRS_FILTER                = Z'0000004B'  !< Pixel contains clouds: The cloud fraction from VIIRS / NPP exceeds theshold. Threshold value from ATBD.
  integer, parameter :: PQF_F_CIRRUS_REFLECTANCE_VIIRS_FILTER            = Z'0000004C'  !< Pixel contains clouds: Cirrus reflectance from VIIRS / NPP exceeds threshold. Threshold value from ATBD.
  integer, parameter :: PQF_F_CF_VIIRS_SWIR_IFOV_FILTER                  = Z'0000004D'  !< Fraction of cloudy VIIRS pixels wihtin S5P SWIR ground pixel exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_CF_VIIRS_SWIR_OFOVA_FILTER                 = Z'0000004E'  !< Fraction of cloudy VIIRS pixels wihtin S5P SWIR OFOVa exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_CF_VIIRS_SWIR_OFOVB_FILTER                 = Z'0000004F'  !< Fraction of cloudy VIIRS pixels wihtin S5P SWIR OFOVb exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_CF_VIIRS_SWIR_OFOVC_FILTER                 = Z'00000050'  !< Fraction of cloudy VIIRS pixels wihtin S5P SWIR OFOVc exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_CF_VIIRS_NIR_IFOV_FILTER                   = Z'00000051'  !< Fraction of cloudy VIIRS pixels wihtin S5P NIR ground pixel exceeds  a priori threshold from configuration.
  integer, parameter :: PQF_F_CF_VIIRS_NIR_OFOVA_FILTER                  = Z'00000052'  !< Fraction of cloudy VIIRS pixels wihtin S5P NIR OFOVa exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_CF_VIIRS_NIR_OFOVB_FILTER                  = Z'00000053'  !< Fraction of cloudy VIIRS pixels wihtin S5P NIR OFOVb exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_CF_VIIRS_NIR_OFOVC_FILTER                  = Z'00000054'  !< Fraction of cloudy VIIRS pixels wihtin S5P NIR OFOVc exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_REFL_CIRRUS_VIIRS_SWIR_FILTER              = Z'00000055'  !< Average VIIRS cirrus reflectance within SWIR ground pixel exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_REFL_CIRRUS_VIIRS_NIR_FILTER               = Z'00000056'  !< Average VIIRS cirrus reflectance within NIR ground pixel exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_DIFF_REFL_CIRRUS_VIIRS_FILTER              = Z'00000057'  !< Difference in VIIRS average cirrus reflectance between SWIR and NIR ground pixel exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_CH4_NOSCAT_RATIO_FILTER                    = Z'00000058'  !< The ratio between [\Methane]$_{\text{weak}}$ and [\Methane]$_{\text{strong}}$ is below or exceeds a priori thresholds from configuration.
  integer, parameter :: PQF_F_CH4_NOSCAT_RATIO_STD_FILTER                = Z'00000059'  !< The standard deviation of [\Methane]$_{\text{weak}}$/[\Methane]$_{\text{strong}}$ within the SWIR pixel and the 8 neighbouring pixels exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_H2O_NOSCAT_RATIO_FILTER                    = Z'0000005A'  !< The ratio between [\Water]$_{\text{weak}}$ and [\Water]$_{\text{strong}}$ is below or exceeds a priori thresholds from configuration.
  integer, parameter :: PQF_F_H2O_NOSCAT_RATIO_STD_FILTER                = Z'0000005B'  !< The standard deviation of [\Water]$_{\text{weak}}$/[\Water]$_{\text{strong}}$ within the SWIR pixel and the 8 neigbouring pixels exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_DIFF_PSURF_FRESCO_ECMWF_FILTER             = Z'0000005C'  !< Difference between the FRESCO apparent surface pressure and the ECMWF surface pressure exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_PSURF_FRESCO_STDV_FILTER                   = Z'0000005D'  !< The standard deviation of the FRESCO apparent surface pressure in the NIR pixel and the 8 surrounding pixels exceeds a priori threshold from configuration.
  integer, parameter :: PQF_F_OCEAN_FILTER                               = Z'0000005E'  !< The ground pixel is over ocean (and ocean glint retrievals are not switched on).
  integer, parameter :: PQF_F_TIME_RANGE_FILTER                          = Z'0000005F'  !< Time is out of the range that is to be processed.
  integer, parameter :: PQF_F_PIXEL_OR_SCANLINE_INDEX_FILTER             = Z'00000060'  !< Not processed because pixel index does not match general selection criteria.
  integer, parameter :: PQF_F_GEOGRAPHIC_REGION_FILTER                   = Z'00000061'  !< Pixel falls outside the specified regions of interest.

! Processing warnings
  integer, parameter :: PQF_W_INPUT_SPECTRUM_WARNING                     = Z'00000100'  !< Number of good pixels in radiance, irradiance or calculated reflectance below threshold from configuration.
  integer, parameter :: PQF_W_WAVELENGTH_CALIBRATION_WARNING             = Z'00000200'  !< Offset from wavelength fit is larger than limit set in configuration.
  integer, parameter :: PQF_W_EXTRAPOLATION_WARNING                      = Z'00000400'  !< Pressure or temperature outside cross section LUT range, other lookup table extrapolation.
  integer, parameter :: PQF_W_SUN_GLINT_WARNING                          = Z'00000800'  !< Sun glint posibility warning.
  integer, parameter :: PQF_W_SOUTH_ATLANTIC_ANOMALY_WARNING             = Z'00001000'  !< TROPOMI is inside the south Atlantic anomaly while taking these measurements.
  integer, parameter :: PQF_W_SUN_GLINT_CORRECTION                       = Z'00002000'  !< A sun glint correction has been applied.
  integer, parameter :: PQF_W_SNOW_ICE_WARNING                           = Z'00004000'  !< Snow/ice flag is set, i.e. using scene data from the cloud support product.
  integer, parameter :: PQF_W_CLOUD_WARNING                              = Z'00008000'  !< Cloud filter based on FRESCO apparent surface  pressure (VIIRS not available), cloud fraction above threshold or cloud pressure adjusted to force cloud above surface.
  integer, parameter :: PQF_W_AAI_WARNING                                = Z'00010000'  !< Possible aerosol contamination as indicated by the AAI.
  integer, parameter :: PQF_W_PIXEL_LEVEL_INPUT_DATA_MISSING             = Z'00020000'  !< Dynamic auxiliary input data (e.g.. cloud) is missing for this ground pixel. A fallback option is used.
  integer, parameter :: PQF_W_DATA_RANGE_WARNING                         = Z'00040000'  !< Carbon monoxide column tends to negative values; Water column tends to negative values; Heavy water (HDO) column tends to negative values; others.
  integer, parameter :: PQF_W_LOW_CLOUD_FRACTION_WARNING                 = Z'00080000'  !< Low cloud fraction, therefore no cloud pressure retrieved.
  integer, parameter :: PQF_W_ALTITUDE_CONSISTENCY_WARNING               = Z'00100000'  !< Difference between ECMWF surface elevation and high-resolution surface elevation exceeds threshold from configuration.
  integer, parameter :: PQF_W_SIGNAL_TO_NOISE_RATIO_WARNING              = Z'00200000'  !< Signal to noise ratio in SWIR and/or NIR band below threshold from configuration.
  integer, parameter :: PQF_W_DECONVOLUTION_WARNING                      = Z'00400000'  !< Failed deconvolution irradiance spectrum (not pixel-specific, but row-specific).
  integer, parameter :: PQF_W_SO2_VOLCANIC_ORIGIN_LIKELY_WARNING         = Z'00800000'  !< Warning for \Sulphurdioxide BL product, UTLS products: volcanic origin except for heavily polluted sites.
  integer, parameter :: PQF_W_SO2_VOLCANIC_ORIGIN_CERTAIN_WARNING        = Z'01000000'  !< Warning for \Sulphurdioxide BL product, UTLS products: volcanic origin certain.
  integer, parameter :: PQF_W_INTERPOLATION_WARNING                      = Z'02000000'  !< Warning for interpolation on partially missing data. In this case the valid available data is used, potentially leading to a bias.
  integer, parameter :: PQF_W_SATURATION_IN_SCANLINE_WARNING             = Z'04000000'  !< Saturation occurred in the same scanline, possibly causing a bias in the current retrieval

end module pqf_module

