!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module S5POperationalModule

  ! This module contains several subroutines that are used by the operational
  ! version of DISAMAR as used in the Sentinel 5P TROPOMI L2 data processors.

  ! The operational version is used as follows:
  !
  ! At the beginning of the run, the staticS data is being filled. This is done
  ! once and in a single thread.
  ! Then the loop over all the pixels to be retrieved starts. At the beginning
  ! of the loop the globalS$inputS data is filled with retrievel specific data.
  ! Then the config.in file is being read and other parts of the globalS are
  ! being filled. After reading the config.in file, some parts of the globalS
  ! are being replaced with data from staticS and globalS%inputS. This is done
  ! before the verify. During the run more data from staticS and globalS%inputS
  ! is being used, for instance solar irradiance data, and radiance/irradiance
  ! measurement data.

  use errorHandlingModule
  use dataStructures
  use staticDataModule
  use inputModule
  use readModule,               only: getHRSolarIrradiance, getHRSolarIrradianceStatic, fillOzoneNodes
  use mathTools,                only: spline, splint, splintLin, gaussDivPoints, GaussDivPoints, polyInt
  use radianceIrradianceModule, only: integrateSlitFunctionIrr
  use readIrrRadFromFileModule, only: readIrrRadPostProcess, setMRWavelengthGrid

  ! default is private
  private
  public  :: readIrrRadFromMemory, replaceDynamicData, replaceXSecLUTData, replaceHRWavelengthData

  real(8), parameter   :: PI = 3.141592653589793d0

  contains

    !  -------------------------------------------------------------------------------

      subroutine readIrrRadFromMemory(errS, globalS, numSpectrBands, controlSimS,             &
                                    wavelInstrIrrSimS, wavelHRSimS, wavelMRIrrSimS,           &
                                    wavelInstrRadSimS, wavelMRRadSimS,                        &
                                    solarIrradianceSimS, earthRadianceSimS,                   &
                                    wavelInstrIrrRetrS, wavelHRRetrS, wavelInstrRadRetrS,     &
                                    solarIrradianceRetrS, earthRadianceRetrS)

      implicit none

      type(errorType), intent(inout) :: errS
      type(globalType),        intent(inout) :: globalS
      integer,                 intent(in)    :: numSpectrBands                       ! number of spectral bands / fit windows
      type(controlType),       intent(in)    :: controlSimS                          ! control parameters (simulation)
      type(wavelInstrType),    intent(inout) :: wavelInstrIrrSimS(numSpectrBands)    ! wavelength grid for measured irradiance
      type(wavelHRType),       intent(inout) :: wavelHRSimS(numSpectrBands)          ! high resolution wavel grid (ir)radiance
      type(wavelHRType),       intent(inout) :: wavelMRIrrSimS(numSpectrBands)       ! high resolution wavel grid (ir)radiance
      type(wavelInstrType),    intent(inout) :: wavelInstrRadSimS(numSpectrBands)    ! wavelength grid for measured radiance
      type(wavelHRType),       intent(inout) :: wavelMRRadSimS(numSpectrBands)       ! wavelength grid for measured radiance
      type(solarIrrType),      intent(inout) :: solarIrradianceSimS(numSpectrBands)  ! solar irr on radiance wavelength grid
      type(earthRadianceType), intent(inout) :: earthRadianceSimS(numSpectrBands)    ! earth radiance read from file
      type(wavelInstrType),    intent(inout) :: wavelInstrIrrRetrS(numSpectrBands)   ! wavelength grid for measured irradiance
      type(wavelHRType),       intent(inout) :: wavelHRRetrS(numSpectrBands)         ! high resolution wavel grid (ir)radiance
      type(wavelInstrType),    intent(inout) :: wavelInstrRadRetrS(numSpectrBands)   ! wavelength grid for measured radiance
      type(solarIrrType),      intent(inout) :: solarIrradianceRetrS(numSpectrBands) ! solar irr on radiance wavelength grid
      type(earthRadianceType), intent(inout) :: earthRadianceRetrS(numSpectrBands)   ! earth radiance read from file

      ! local
      integer, parameter :: maxNumWavelengths = 1200
      integer, parameter :: maxNumChannels = 10

      integer            :: OpenError
      integer            :: iband, iwave, iwave_irr, iwave_rad, iTrace, index_irr, index_rad
      integer            :: ichannel, ichannel_irr, ichannel_rad
      integer            :: nchannel, nchannel_irr, nchannel_rad
      integer            :: nwavel(maxNumChannels), nwavel_irr(maxNumChannels), nwavel_rad(maxNumChannels)
      integer            :: numChannelsForFitWindow, numChannelsForFitWindow_irr, numChannelsForFitWindow_rad


      real(8)            :: wavelength    (maxNumWavelengths, maxNumChannels)
      real(8)            :: wavelength_irr(maxNumWavelengths, maxNumChannels)
      real(8)            :: wavelength_rad(maxNumWavelengths, maxNumChannels)
      real(8)            :: SNR           (maxNumWavelengths, maxNumChannels)
      real(8)            :: SNR_irr       (maxNumWavelengths, maxNumChannels)
      real(8)            :: SNR_rad       (maxNumWavelengths, maxNumChannels)
      real(8)            :: reflectance   (maxNumWavelengths, maxNumChannels)
      real(8)            :: irradiance    (maxNumWavelengths, maxNumChannels)
      real(8)            :: radiance      (maxNumWavelengths, maxNumChannels)
      integer            :: indexArray    (maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_irr(maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_rad(maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_irr_mod(maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_rad_mod(maxNumWavelengths, maxNumChannels)

      integer            :: counter, ipair, i_column
      real(8)            :: wavel, FWHM
      logical            :: addWavel
      logical            :: old_version

      integer            :: channelNumber    (numSpectrBands)  ! channel number for each fit window
      integer            :: channelNumber_irr(numSpectrBands)  ! channel number for each fit window
      integer            :: channelNumber_rad(numSpectrBands)  ! channel number for each fit window

      logical            :: eof
      logical            :: wavelength_grids_differ

      type(solarIrrType) :: solarIrradianceReadS(numSpectrBands) ! solar irr on radiance wavelength grid
                                                                 ! only used for high sampling interpolation

      character(LEN = 40)   :: identifier
      real(8)               :: value       ! dummy value

      logical, parameter    :: verbose = .false.

      call enter('readIrrRadFromMemory')

      ! initialize
      wavelength_grids_differ = .false.

      wavelength    (:,:)  = 0.0d0
      wavelength_irr(:,:)  = 0.0d0
      wavelength_rad(:,:)  = 0.0d0
      SNR(:,:)             = 0.0d0
      SNR_irr(:,:)         = 0.0d0
      SNR_rad(:,:)         = 0.0d0
      reflectance(:,:)     = 0.0d0
      irradiance(:,:)      = 0.0d0
      radiance(:,:)        = 0.0d0
      nwavel(:)            = 0
      nwavel_irr(:)        = 0
      nwavel_rad(:)        = 0

      ! initialize
      ichannel     = 1
      ichannel_irr = 1
      ichannel_rad = 1
      nchannel     = 1
      nchannel_irr = 1
      nchannel_rad = 1

      old_version = .false.

      nchannel = 1
      nchannel_irr = nchannel
      nchannel_rad = nchannel

      do ichannel = 1, nchannel
        do iwave = 1, size(globalS%inputS%irradiance)
          wavelength_irr(iwave, ichannel)  = globalS%inputS%irradianceWavelength(iwave)
          SNR_irr(iwave, ichannel)         = globalS%inputS%irradiance(iwave) / globalS%inputS%irradianceNoise(iwave)
          irradiance(iwave, ichannel)      = globalS%inputS%irradiance(iwave)
          if (wavelength_irr(iwave, ichannel) > 10) then
            nwavel_irr(ichannel) = iwave
          end if
        end do
        do iwave = 1, size(globalS%inputS%radiance)
          wavelength_rad(iwave, ichannel)  = globalS%inputS%radianceWavelength(iwave)
          SNR_rad(iwave, ichannel)         = globalS%inputS%radiance(iwave) / globalS%inputS%radianceNoise(iwave)
          radiance(iwave, ichannel)        = globalS%inputS%radiance(iwave)
          if (wavelength_rad(iwave, ichannel) > 10) then
            nwavel_rad(ichannel) = iwave
          end if
        end do
      end do

      call readIrrRadPostProcess(errS, globalS, numSpectrBands, controlSimS, old_version,       &
                                 wavelInstrIrrSimS, wavelHRSimS, wavelMRIrrSimS,                &
                                 wavelInstrRadSimS, wavelMRRadSimS,                             &
                                 solarIrradianceSimS, earthRadianceSimS,                        &
                                 wavelInstrIrrRetrS, wavelHRRetrS, wavelInstrRadRetrS,          &
                                 solarIrradianceRetrS, earthRadianceRetrS,                      &
                                 maxNumWavelengths, maxNumChannels,                             &
                                 nchannel, wavelength, SNR, reflectance, nwavel,                &
                                 nchannel_irr, wavelength_irr, SNR_irr, irradiance, nwavel_irr, &
                                 nchannel_rad, wavelength_rad, SNR_rad, radiance, nwavel_rad)
      if (errorCheck(errS)) goto 99999

99999 continue
      call exit('readIrrRadFromMemory')

    end subroutine readIrrRadFromMemory

    !  -------------------------------------------------------------------------------

    subroutine setupHRWavelengthGridIrr(errS, wavelInstrS, wavelHRS,  FWHM)

      ! We assume that we can use spline interpolation on the solar spectrum
      ! to get the solar irradiance at the high resolution (HR) wavelength grids
      ! for simulation and retrieval, i.e. the high resolution solar spectrum
      ! is not undersampled.

      ! This subroutine allocates and fills the array wavel in the structures wavelHRS

       implicit none

       type(errorType),      intent(inout) :: errS
       type(wavelInstrType), intent(in)    :: wavelInstrS
       type(wavelHRType),    intent(inout) :: wavelHRS
       real(8),              intent(in)    :: FWHM

       real(8)    :: waveStart, waveEnd
       type boundariesType
         integer          :: numBoundaries
         real(8), pointer :: boundaries(:)  => null()
       end type boundariesType

       real(8), allocatable :: intervalBoundaries(:)
       real(8)              :: maxInterval
       !  x0(i,j) are the division points (x0(i), i = 1:j) and w0 the corresponding weights
       real(8), allocatable :: x0(:,:)
       real(8), allocatable :: w0(:,:)
       real(8)              :: dw, sw, wavel, newWavel

       logical              :: addWavel
       integer              :: nwavel
       real(8), allocatable :: wavelBand(:)        ! (nwavel) full band - not accounting for excluded parts
       real(8), allocatable :: wavelBandWeight(:)  ! (nwavel) weights for full band - not accounting for excluded parts

       integer              :: iwave, nlines, iinterval, index, ipair
       integer              :: iGauss, nGauss, nGaussMax, nGaussMin

       integer              :: allocStatus
       integer              :: deallocStatus

       logical, parameter   :: verbose = .false.

       ! initialize
       waveStart = wavelInstrS%startWavel - 2.0d0 * FWHM
       waveEnd   = wavelInstrS%endWavel   + 2.0d0 * FWHM

       ! starting at waveStart we add a boundary that lies one FWHM further
       nlines = 0
       wavel = waveStart
       do
         nlines = nlines + 1
         newWavel = wavel + FWHM
         wavel = newWavel
         if ( wavel > waveEnd ) exit
       end do

       allocStatus = 0
       allocate ( intervalBoundaries(0:nlines), STAT=allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('FATAL ERROR: allocation failed')
         call logDebug('for intervalBoundaries')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       ! fill values for the array intervalBoundaries
       nlines = 0
       wavel = waveStart
       intervalBoundaries(0) = wavel
       do
         nlines = nlines + 1
         newWavel = wavel + FWHM
         wavel = newWavel
         intervalBoundaries(nlines) = wavel
         if ( wavel > waveEnd ) exit
       end do

       ! fill Gausspoints and weights arrays
       nGaussMax = wavelHRS%nGaussFWHM
       nGaussMin = wavelHRS%nGaussFWHM

       ! allocate arrays for gausspoints and weight on (0,1)
       ! they are deallocated just before leaving this subroutine

       allocStatus = 0
       allocate ( x0(nGaussMax,nGaussMax), w0(nGaussMax,nGaussMax), STAT=allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('FATAL ERROR: allocation failed')
         call logDebug('for x0 or w0')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       do iGauss = nGaussMin, nGaussMax
         call GaussDivPoints(errS, 0.0d0, 1.0d0, x0(:,iGauss), w0(:,iGauss), iGauss)
         if (errorCheck(errS)) return
       end do

       ! determine the number of wavelengths for the high resolution simulation grid
       maxInterval = 0.0d0
       do iinterval = 2, size(intervalBoundaries) - 2
         dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)
         if ( dw >  maxInterval )  maxInterval = dw
       end do

       index = 0
       do iinterval = 1, size(intervalBoundaries) - 1
         dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)      ! width of interval
         sw = intervalBoundaries(iinterval-1)                                      ! start wavelength for interval
         nGauss = max( nGaussMin, nint( nGaussMax * dw / maxInterval ) )
         if (nGauss >  nGaussMax ) nGauss =  nGaussMax
         do iGauss = 1, nGauss
           index = index + 1
         end do
       end do

       nwavel = index

       allocStatus = 0
       allocate ( wavelBand(nwavel), wavelBandWeight(nwavel), STAT=allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('FATAL ERROR: allocation failed')
         call logDebug('for wavelBand or wavelBandWeight')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       ! fill wavelength grid for the entire spectral band
       index = 1
       do iinterval = 1, size(intervalBoundaries) - 1
         dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)
         sw = intervalBoundaries(iinterval-1)
         nGauss = max(  nGaussMin, nint( nGaussMax * dw / maxInterval ) )
         if (nGauss >  nGaussMax ) nGauss = nGaussMax
         do iGauss = 1, nGauss
           wavelBand(index)       = sw + dw * x0(iGauss, nGauss)
           wavelBandWeight(index) = dw * w0(iGauss, nGauss)
           index = index + 1
         end do
       end do

       ! deal with excluded intervals

       ! copy excluded intervals and reduce them with 4 * FWHM (2 left and 2 right)
       wavelHRS%nExclude        = wavelInstrS%nExclude
       wavelHRS%excludeStart(:) = wavelInstrS%excludeStart(:) + 2 * FWHM
       wavelHRS%excludeEnd(:)   = wavelInstrS%excludeEnd(:)   - 2 * FWHM

       ! determine the lengths of the wavelength array taking into account excluded wavelengths
       if ( wavelHRS%nExclude > 0 ) then
         index = 0
         do iwave = 1, nwavel
           wavel = wavelBand(iwave)
           addWavel = .true.
           do ipair = 1, wavelHRS%nExclude
             if ( (wavel >= wavelHRS%excludeStart(ipair)) .and. (wavel <= wavelHRS%excludeEnd(ipair)) ) &
               addWavel = .false.
           end do ! ipair
           if ( addWavel ) index = index + 1
         end do ! iwave

         wavelHRS%nwavel = index

       else

         wavelHRS%nwavel = nwavel

       end if

       ! claim memory space
       call claimMemWavelHRS(errS, wavelHRS)          ! claim memory for high resolution wavelength grid

       ! fill wavelengths

       if ( wavelHRS%nExclude > 0 ) then

         index = 0
         do iwave = 1, nwavel
           wavel = wavelBand(iwave)
           addWavel = .true.
           do ipair = 1, wavelInstrS%nExclude
             if ( (wavel >= wavelHRS%excludeStart(ipair)) .and. (wavel <= wavelHRS%excludeEnd(ipair)) ) &
               addWavel = .false.
           end do ! ipair
           if ( addWavel ) then
             index = index + 1
             wavelHRS%wavel(index)  = wavelBand(iwave)
             wavelHRS%weight(index) = wavelBandWeight(iwave)
           end if
         end do ! iwave

       else

         do iwave = 1, nwavel
           wavelHRS%wavel(iwave)  = wavelBand(iwave)
           wavelHRS%weight(iwave) = wavelBandWeight(iwave)
         end do

       end if

       if ( verbose ) then

         write(intermediateFileUnit, '(A, 2F10.4)') 'wavelength range = ', waveStart, waveEnd

         write(intermediateFileUnit, *)
         write( intermediateFileUnit, *) ' interval boundaries'
         do iinterval = 0, size(intervalBoundaries) -1
           write(intermediateFileUnit, '(F15.6)') intervalBoundaries(iinterval)
         end do

         write(intermediateFileUnit, *)
         write(intermediateFileUnit,'(A)') 'wavelengths for the high resolution grid'
         do iwave = 1, wavelHRS%nwavel
           write(intermediateFileUnit,'(F12.5)') wavelHRS%wavel(iwave)
         end do

       end if ! verbose

       ! clean up
       deallocStatus  = 0
       deallocate( intervalBoundaries, x0, w0, wavelBand, wavelBandWeight, STAT=deallocStatus )
       if ( deallocStatus /= 0 ) then
         call logDebug('FATAL ERROR: deallocation failed')
         call logDebug('for intervalBoundaries, x0, w0, wavelBand, or wavelBandWeight')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because deallocation failed')
         if (errorCheck(errS)) return
       end if

    end subroutine setupHRWavelengthGridIrr

    !  -------------------------------------------------------------------------------

    subroutine set_nwavelInstr(errS, globalS)
      type(errorType), intent(inout) :: errS
      type(globalType) :: globalS

      ! set values for the number of wavelengths on the instrument grid

      integer :: iTrace, iband, icolumn

      do iTrace = 1, globalS%nTrace
        do iband = 1, globalS%numSpectrBands
          globalS%XsecHRSimS (iband,iTrace)%nwavel  = globalS%wavelInstrRadSimS(iband)%nwavel
          globalS%XsecHRRetrS(iband,iTrace)%nwavel  = globalS%wavelInstrRadRetrS(iband)%nwavel
        end do
      end do

      do icolumn = 1, globalS%ncolumn
        globalS%columnSimS (icolumn)%nwavel  = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
        globalS%columnRetrS(icolumn)%nwavel  = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
      end do ! icolumn

      do iband = 1, globalS%numSpectrBands
        globalS%solarIrradianceSimS (iband)%nwavel   = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%solarIrradianceRetrS(iband)%nwavel   = globalS%wavelInstrRadRetrS(iband)%nwavel
        globalS%earthRadianceSimS(iband)%nwavel      = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%earthRadianceRetrS(iband)%nwavel     = globalS%wavelInstrRadRetrS(iband)%nwavel
        globalS%weakAbsSimS(iband)%nwavelInstr       = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%weakAbsRetrS(iband)%nwavelInstr      = globalS%wavelInstrRadRetrS(iband)%nwavel
        globalS%RRS_RingSimS(iband)%nwavel           = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%RRS_RingRetrS(iband)%nwavel          = globalS%wavelInstrRadRetrS(iband)%nwavel
      end do
      globalS%retrS%nwavelRetr                       = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
      globalS%diagnosticS%nwavelRetr                 = globalS%retrS%nwavelRetr

    end subroutine set_nwavelInstr

    !  -------------------------------------------------------------------------------

    subroutine set_nwavelMR(errS, globalS)
      type(errorType), intent(inout) :: errS
      type(globalType) :: globalS

      integer :: iband

      do iband = 1, globalS%numSpectrBands
        globalS%solarIrradianceSimS (iband)%nwavelMR = globalS%wavelMRRadSimS (iband)%nwavel
        globalS%solarIrradianceRetrS(iband)%nwavelMR = globalS%wavelMRRadRetrS(iband)%nwavel
      end do

    end subroutine set_nwavelMR

    !  -------------------------------------------------------------------------------

    subroutine replaceXSecLUTData(errS, inputS, globalS)

        implicit none

        type(errorType),        intent(inout) :: errS
        type(inputType),        intent(inout) :: inputS
        type(globalType),       intent(inout) :: globalS

        integer                               :: iband, ispecies, sumAllocStatus, allocStatus
        integer                               :: nTemperature, nPressure, nWavel

        call enter('replaceXSecLUTData')

        if ((.not. associated(staticS%o2xsection)) .or. (.not. staticS%o2XsectionLoaded)) goto 99999

        sumAllocStatus = 0
        allocStatus = 0
        nWavel = staticS%hr_wavel%nwavel
        nPressure = staticS%o2xsection%ncoeff_lnp_LUT
        nTemperature = staticS%o2xsection%ncoeff_lnT_LUT

        call logDebug('Replacing O2 and O2-O2 cross section data')

        globalS%controlRetrS%usePolyExpXsec = .true.
        globalS%controlSimS%usePolyExpXsec = .true.

        if (associated(globalS%XsecHRLUTRetrS)) then
            do iband = 1, globalS%numSpectrBands
                do ispecies = 1, globalS%nTrace
                    if (associated(globalS%XsecHRLUTRetrS(iband, ispecies)%coeff_lnTlnp_LUT)) deallocate(globalS%XsecHRLUTRetrS(iband, ispecies)%coeff_lnTlnp_LUT)
                end do
            end do
            deallocate(globalS%XsecHRLUTRetrS)
        end if

        if (associated(globalS%XsecHRLUTSimS)) then
            do iband = 1, globalS%numSpectrBands
                do ispecies = 1, globalS%nTrace
                    if (associated(globalS%XsecHRLUTSimS(iband, ispecies)%coeff_lnTlnp_LUT)) deallocate(globalS%XsecHRLUTSimS(iband, ispecies)%coeff_lnTlnp_LUT)
                end do
            end do
            deallocate(globalS%XsecHRLUTSimS)
        end if

        sumAllocStatus = 0
        allocate(globalS%XsecHRLUTRetrS(globalS%numSpectrBands,globalS%nTrace), STAT = allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus

        allocate(globalS%XsecHRLUTSimS(globalS%numSpectrBands,globalS%nTrace), STAT = allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus

        if (sumAllocStatus /= 0) then
            call mystop(errS, 'Failed to allocate memory for cross section LUT')
            if (errorCheck(errS)) goto 99999
        end if

        if (globalS%numSpectrBands > 1) then
            call mystop(errS, 'Currently only a single band is supported by replaceXSecLUTData and the lookup table')
            globalS%processingQF = PQF_E_CONFIGURATION_ERROR
            if (errorCheck(errS)) goto 99999
        end if

        do iband = 1, globalS%numSpectrBands
            do ispecies = 1, globalS%nTrace
                nWavel = staticS%o2xsection%nwavelHR
                nPressure = staticS%o2xsection%ncoeff_lnp_LUT
                nTemperature = staticS%o2xsection%ncoeff_lnT_LUT

                allocate(globalS%XsecHRLUTRetrS(iband, ispecies)%coeff_lnTlnp_LUT(nTemperature, nPressure, nWavel), STAT = allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus

                allocate(globalS%XsecHRLUTSimS(iband, ispecies)%coeff_lnTlnp_LUT(nTemperature, nPressure, nWavel), STAT = allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus

                if (sumAllocStatus /= 0) then
                    call mystop(errS, 'Failed to allocate memory for cross section LUT')
                    if (errorCheck(errS)) goto 99999
                end if
                globalS%XsecHRLUTRetrS(iband, ispecies)%pmax_LUT = staticS%o2xsection%pmax_LUT
                globalS%XsecHRLUTRetrS(iband, ispecies)%pmin_LUT = staticS%o2xsection%pmin_LUT
                globalS%XsecHRLUTRetrS(iband, ispecies)%Tmax_LUT = staticS%o2xsection%Tmax_LUT
                globalS%XsecHRLUTRetrS(iband, ispecies)%Tmin_LUT = staticS%o2xsection%Tmin_LUT
                globalS%XsecHRLUTRetrS(iband, ispecies)%ncoeff_lnp_LUT = nPressure
                globalS%XsecHRLUTRetrS(iband, ispecies)%ncoeff_lnT_LUT = nTemperature
                globalS%XsecHRLUTRetrS(iband, ispecies)%nwavelHR = nWavel
                globalS%XsecHRLUTRetrS(iband, ispecies)%createXsecPolyLUT = .false.

                globalS%XsecHRLUTSimS(iband, ispecies)%pmax_LUT = staticS%o2xsection%pmax_LUT
                globalS%XsecHRLUTSimS(iband, ispecies)%pmin_LUT = staticS%o2xsection%pmin_LUT
                globalS%XsecHRLUTSimS(iband, ispecies)%Tmax_LUT = staticS%o2xsection%Tmax_LUT
                globalS%XsecHRLUTSimS(iband, ispecies)%Tmin_LUT = staticS%o2xsection%Tmin_LUT
                globalS%XsecHRLUTSimS(iband, ispecies)%ncoeff_lnp_LUT = nPressure
                globalS%XsecHRLUTSimS(iband, ispecies)%ncoeff_lnT_LUT = nTemperature
                globalS%XsecHRLUTSimS(iband, ispecies)%nwavelHR = nWavel
                globalS%XsecHRLUTSimS(iband, ispecies)%createXsecPolyLUT = .false.

                select case (trim(globalS%traceGasRetrS(ispecies)%nameTraceGas))
                    case('O2')
                        if (associated(staticS%o2xsection)) then
                            globalS%XsecHRLUTRetrS(iband, ispecies)%coeff_lnTlnp_LUT = staticS%o2xsection%coeff_lnTlnp_LUT
                        end if

                    case('O2-O2')
                        if (associated(staticS%o2o2xsection)) then
                            globalS%XsecHRLUTRetrS(iband, ispecies)%coeff_lnTlnp_LUT = staticS%o2o2xsection%coeff_lnTlnp_LUT
                        end if

                    case default
                        call mystop(errS, 'Currently only O2 and O2-O2 are supported by replaceXSecLUTData and the lookup table, not "' // trim(globalS%traceGasRetrS(ispecies)%nameTraceGas) // '"')
                        globalS%processingQF = PQF_E_CONFIGURATION_ERROR
                        if (errorCheck(errS)) goto 99999
                end select

                select case (trim(globalS%traceGasSimS(ispecies)%nameTraceGas))
                    case('O2')
                        globalS%XsecHRLUTSimS(iband, ispecies)%coeff_lnTlnp_LUT = staticS%o2xsection%coeff_lnTlnp_LUT
                    case('O2-O2')
                        if (associated(staticS%o2o2xsection)) then
                            globalS%XsecHRLUTSimS(iband, ispecies)%coeff_lnTlnp_LUT = staticS%o2o2xsection%coeff_lnTlnp_LUT
                        end if
                    case default
                        call mystop(errS, 'Currently only O2 and O2-O2 are supported by replaceXSecLUTData and the lookup table, not "' // trim(globalS%traceGasSimS(ispecies)%nameTraceGas) // '"')
                        globalS%processingQF = PQF_E_CONFIGURATION_ERROR
                        if (errorCheck(errS)) goto 99999
                end select
            end do
        end do
99999   continue
        call exit('replaceXSecLUTData')

    end subroutine replaceXSecLUTData


    subroutine replaceHRWavelengthData(errS, inputS, globalS)

        implicit none

        type(errorType),        intent(inout) :: errS
        type(inputType),        intent(inout) :: inputS
        type(globalType),       intent(inout) :: globalS

        integer                               :: iband, ispecies, sumAllocStatus, allocStatus
        integer                               :: nWavel

        call enter('replaceHRWavelengthData')

        if ((.not. associated(staticS%o2xsection)) .or. (.not. staticS%o2XsectionLoaded)) goto 99999

        call logDebug('Replacing High resolution wavelength grid')

        sumAllocStatus = 0
        allocStatus = 0
        nWavel = staticS%hr_wavel%nwavel

        globalS%controlRetrS%usePolyExpXsec = .true.
        globalS%controlSimS%usePolyExpXsec = .true.

        if (associated(globalS%wavelHRRetrS)) then
            do iband = 1, globalS%numSpectrBands
                globalS%wavelHRRetrS(iband)%nwavel = staticS%hr_wavel%nwavel
                if(.not. associated(globalS%wavelHRRetrS(iband)%wavel)) allocate(globalS%wavelHRRetrS(iband)%wavel(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if(.not. associated(globalS%wavelHRRetrS(iband)%weight)) allocate(globalS%wavelHRRetrS(iband)%weight(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if (sumAllocStatus /= 0) then
                    call mystop(errS, 'Failed to allocate memory for HR wavelength LUT')
                    if (errorCheck(errS)) goto 99999
                end if
                globalS%wavelHRRetrS(iband)%wavel = staticS%hr_wavel%wavel
                globalS%wavelHRRetrS(iband)%weight = staticS%hr_wavel%weights
            end do
        end if

        if (associated(globalS%wavelHRSimS)) then
            do iband = 1, globalS%numSpectrBands
                globalS%wavelHRSimS(iband)%nwavel = staticS%hr_wavel%nwavel
                if(.not. associated(globalS%wavelHRSimS(iband)%wavel)) allocate(globalS%wavelHRSimS(iband)%wavel(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if(.not. associated(globalS%wavelHRSimS(iband)%weight)) allocate(globalS%wavelHRSimS(iband)%weight(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if (sumAllocStatus /= 0) then
                    call mystop(errS, 'Failed to allocate memory for HR wavelength LUT')
                    if (errorCheck(errS)) goto 99999
                end if
                globalS%wavelHRSimS(iband)%wavel = staticS%hr_wavel%wavel
                globalS%wavelHRSimS(iband)%weight = staticS%hr_wavel%weights
            end do
        end if

        if (associated(globalS%wavelMRRadRetrS)) then
            do iband = 1, globalS%numSpectrBands
                globalS%wavelMRRadRetrS(iband)%nwavel = staticS%hr_wavel%nwavel
                if(.not. associated(globalS%wavelMRRadRetrS(iband)%wavel)) allocate(globalS%wavelMRRadRetrS(iband)%wavel(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if(.not. associated(globalS%wavelMRRadRetrS(iband)%weight)) allocate(globalS%wavelMRRadRetrS(iband)%weight(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if (sumAllocStatus /= 0) then
                    call mystop(errS, 'Failed to allocate memory for HR wavelength LUT')
                    if (errorCheck(errS)) goto 99999
                end if
                globalS%wavelMRRadRetrS(iband)%wavel = staticS%hr_wavel%wavel
                globalS%wavelMRRadRetrS(iband)%weight = staticS%hr_wavel%weights
            end do
        end if

        if (associated(globalS%wavelMRRadSimS)) then
            do iband = 1, globalS%numSpectrBands
                globalS%wavelMRRadSimS(iband)%nwavel = staticS%hr_wavel%nwavel
                if(.not. associated(globalS%wavelMRRadSimS(iband)%wavel)) allocate(globalS%wavelMRRadSimS(iband)%wavel(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if(.not. associated(globalS%wavelMRRadSimS(iband)%weight)) allocate(globalS%wavelMRRadSimS(iband)%weight(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if (sumAllocStatus /= 0) then
                    call mystop(errS, 'Failed to allocate memory for HR wavelength LUT')
                    if (errorCheck(errS)) goto 99999
                end if
                globalS%wavelMRRadSimS(iband)%wavel = staticS%hr_wavel%wavel
                globalS%wavelMRRadSimS(iband)%weight = staticS%hr_wavel%weights
            end do
        end if

        if (associated(globalS%wavelMRIrrRetrS)) then
            do iband = 1, globalS%numSpectrBands
                globalS%wavelMRIrrRetrS(iband)%nwavel = staticS%hr_wavel%nwavel
                if(.not. associated(globalS%wavelMRIrrRetrS(iband)%wavel)) allocate(globalS%wavelMRIrrRetrS(iband)%wavel(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if(.not. associated(globalS%wavelMRIrrRetrS(iband)%weight)) allocate(globalS%wavelMRIrrRetrS(iband)%weight(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if (sumAllocStatus /= 0) then
                    call mystop(errS, 'Failed to allocate memory for HR wavelength LUT')
                    if (errorCheck(errS)) goto 99999
                end if
                globalS%wavelMRIrrRetrS(iband)%wavel = staticS%hr_wavel%wavel
                globalS%wavelMRIrrRetrS(iband)%weight = staticS%hr_wavel%weights
            end do
        end if

        if (associated(globalS%wavelMRIrrSimS)) then
            do iband = 1, globalS%numSpectrBands
                globalS%wavelMRIrrSimS(iband)%nwavel = staticS%hr_wavel%nwavel
                if(.not. associated(globalS%wavelMRIrrSimS(iband)%wavel)) allocate(globalS%wavelMRIrrSimS(iband)%wavel(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if(.not. associated(globalS%wavelMRIrrSimS(iband)%weight)) allocate(globalS%wavelMRIrrSimS(iband)%weight(staticS%hr_wavel%nwavel), stat=allocStatus)
                sumAllocStatus = sumAllocStatus + allocStatus
                if (sumAllocStatus /= 0) then
                    call mystop(errS, 'Failed to allocate memory for HR wavelength LUT')
                    if (errorCheck(errS)) goto 99999
                end if
                globalS%wavelMRIrrSimS(iband)%wavel = staticS%hr_wavel%wavel
                globalS%wavelMRIrrSimS(iband)%weight = staticS%hr_wavel%weights
            end do
        end if

99999   continue
        call exit('replaceHRWavelengthData')

    end subroutine replaceHRWavelengthData

    !  -------------------------------------------------------------------------------

    subroutine replaceISRFData(errS, inputS, iband, slitFunctionSpecsS)

        type(errorType),        intent(inout) :: errS
        type(inputType),        intent(inout) :: inputS
        integer,                intent(in)    :: iband
        type(slitFunctionType), intent(inout) :: slitFunctionSpecsS

        integer     :: allocStatus
        integer     :: deallocStatus
        integer     :: sumAllocStatus
        integer     :: i, j, k

        call enter('replaceISRFData')

        if (slitFunctionSpecsS%slitIndex /= 2) goto 99999

        call logDebug('Replacing ISRF data')

        slitFunctionSpecsS%nwavelNominal = size(staticS%isrf_wavelength)
        slitFunctionSpecsS%nwavelListed = size(staticS%isrf_offset)

        if (associated(slitFunctionSpecsS%bandNumber)) deallocate(slitFunctionSpecsS%bandNumber, STAT = deallocStatus)
        if (associated(slitFunctionSpecsS%pixelNumber)) deallocate(slitFunctionSpecsS%pixelNumber, STAT = deallocStatus)
        if (associated(slitFunctionSpecsS%wavelNominal)) deallocate(slitFunctionSpecsS%wavelNominal, STAT = deallocStatus)
        if (associated(slitFunctionSpecsS%deltawavelListed)) deallocate(slitFunctionSpecsS%deltawavelListed, STAT = deallocStatus)
        if (associated(slitFunctionSpecsS%SlitFunctionTable)) deallocate(slitFunctionSpecsS%SlitFunctionTable, STAT = deallocStatus)

        sumAllocStatus = 0

        allocate(slitFunctionSpecsS%bandNumber(slitFunctionSpecsS%nwavelNominal), STAT = allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate(slitFunctionSpecsS%pixelNumber(slitFunctionSpecsS%nwavelNominal), STAT = allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate(slitFunctionSpecsS%wavelNominal(slitFunctionSpecsS%nwavelNominal), STAT = allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate(slitFunctionSpecsS%deltawavelListed(slitFunctionSpecsS%nwavelListed, slitFunctionSpecsS%nwavelNominal), STAT = allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate(slitFunctionSpecsS%SlitFunctionTable(slitFunctionSpecsS%nwavelListed, slitFunctionSpecsS%nwavelNominal), STAT = allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus

        if (sumAllocStatus /= 0) then
            call mystop(errS, 'Failed to allocate memory for ISRF')
            if (errorCheck(errS)) goto 99999
        end if

        do i = 1, slitFunctionSpecsS%nwavelNominal
            slitFunctionSpecsS%bandNumber(i) = 1
            slitFunctionSpecsS%pixelNumber(i) = inputS%pixel_index
            slitFunctionSpecsS%wavelNominal(i) = staticS%isrf_wavelength(i)
            do j = 1, slitFunctionSpecsS%nwavelListed
                slitFunctionSpecsS%deltawavelListed(j, i) = staticS%isrf_offset(j)
                if (staticS%fort) then
                    k = ((j - 1) * slitFunctionSpecsS%nwavelListed + (i - 1)) * staticS%nrows + inputS%pixel_index
                else
                    k = ((inputS%pixel_index - 1) * slitFunctionSpecsS%nwavelNominal + (i - 1)) * slitFunctionSpecsS%nwavelListed + j
                end if
                slitFunctionSpecsS%SlitFunctionTable(j, i) = staticS%isrf(k)
            end do
        end do

        slitFunctionSpecsS%slitIndex = 5

99999   continue
        call exit('replaceISRFData')

    end subroutine replaceISRFData

    !  -------------------------------------------------------------------------------

    subroutine replaceDynamicData(errS, globalS, inputS, geometryRetrS, gasPTRetrS, O3ClimRetrS, traceGasRetrS, surfaceRetrS, cloudAerosolRTMgridRetrS, nTrace)

        type(errorType),                intent(inout) :: errS
        type(globalType),               intent(inout) :: globalS
        type(inputType),                intent(inout) :: inputS
        type(geometryType),             intent(inout) :: geometryRetrS
        type(gasPTType),                intent(inout) :: gasPTRetrS
        type(O3ClimType),               intent(inout) :: O3ClimRetrS
        type(traceGasType),                   pointer :: traceGasRetrS(:)
        type(LambertianType),                 pointer :: surfaceRetrS(:)
        type(cloudAerosolRTMgridType),  intent(inout) :: cloudAerosolRTMgridRetrS
        integer,                        intent(in)    :: nTrace

        integer                 :: i, j, k
        integer                 :: ipressure
        integer                 :: allocStatus
        integer                 :: sumAllocStatus
        integer                 :: numtim, numlat, numtco3, numlev
        integer                 :: iTrace
        real(4)                 :: vmr, error_retr

        logical, parameter      :: verbose = .false.

        call enter('replaceDynamicData')

        call logDebug('Replacing data from config file with dynamic data')

        ! Replace specific GEOMETRY data

        geometryRetrS%sza       = inputS%solarZenithAngle
        geometryRetrS%s_azimuth = inputS%solarAzimuthAngle
        geometryRetrS%vza       = inputS%instrumentNadirAngle
        geometryRetrS%v_azimuth = inputS%instrumentAzimuthAngle

        geometryRetrS%dphi = modulo(180.0d0 - (geometryRetrS%v_azimuth - geometryRetrS%s_azimuth), 360.0d0)

        ! calculate direction cosines and dphi in radians
        geometryRetrS%uu      = cos(geometryRetrS%vza * PI / 180.0d0)
        geometryRetrS%u0      = cos(geometryRetrS%sza * PI / 180.0d0)
        geometryRetrS%dphiRad =     geometryRetrS%dphi* PI / 180.0d0

        ! Replace specific SURFACE data
        ! surfaceType must be waveldependent in DISAMAR Config file

        surfaceRetrS(:)%pressure   = inputS%surfacePressure
        surfaceRetrS(:)%pressureAP = inputS%surfacePressure
        surfaceRetrS(:)%altitude   = inputS%surfaceAltitude

        if (.not. allocated(inputS%surfaceAlbedo)) then
            call logDebug('FATAL ERROR: surfaceAlbedo not allocated')
            call logDebug('in subroutine replaceDynamicData')
            call mystop(errS, 'stopped')
            if (errorCheck(errS)) goto 99999
        end if
        if (.not. allocated(inputS%surfaceAlbedoWavelength)) then
            call logDebug('FATAL ERROR: surfaceAlbedoWavelength not allocated')
            call logDebug('in subroutine replaceDynamicData')
            call mystop(errS, 'stopped')
            if (errorCheck(errS)) goto 99999
        end if
        if (size(inputS%surfaceAlbedo) /= size(inputS%surfaceAlbedoWavelength)) then
            call logDebug('FATAL ERROR: surfaceAlbedo and surfaceAlbedoWavelength not same size')
            call logDebug('in subroutine replaceDynamicData')
            call mystop(errS, 'stopped')
            if (errorCheck(errS)) goto 99999
        end if
        do j = 1, size(inputS%surfaceAlbedo)
            surfaceRetrS(1)%albedo(j) = inputS%surfaceAlbedo(j)
            surfaceRetrS(1)%albedoAP(j) = inputS%surfaceAlbedo(j)
            surfaceRetrS(1)%wavelAlbedo(j) = inputS%surfaceAlbedoWavelength(j)
        end do

        if (cloudAerosolRTMgridRetrS%intervalBounds_P(1) > (inputS%surfacePressure - 100.0)) then
            cloudAerosolRTMgridRetrS%intervalBounds_P(1)   = inputS%surfacePressure - 100.0
            cloudAerosolRTMgridRetrS%intervalBoundsAP_P(1) = inputS%surfacePressure - 100.0
            cloudAerosolRTMgridRetrS%intervalBounds_P(2)   = inputS%surfacePressure - 150.0
            cloudAerosolRTMgridRetrS%intervalBoundsAP_P(2) = inputS%surfacePressure - 150.0
        end if

        ! Replace specific PRESSURE_TEMPERATURE data

        if (.not. allocated(inputS%pressure)) then
            call logDebug('FATAL ERROR: pressure not allocated')
            call logDebug('in subroutine replaceDynamicData')
            call mystop(errS, 'stopped')
            if (errorCheck(errS)) goto 99999
        end if
        if (.not. allocated(inputS%temperature)) then
            call logDebug('FATAL ERROR: temperature not allocated')
            call logDebug('in subroutine replaceDynamicData')
            call mystop(errS, 'stopped')
            if (errorCheck(errS)) goto 99999
        end if
        if (size(inputS%pressure) /= size(inputS%temperature)) then
            call logDebug('FATAL ERROR: pressure and temperature not same size')
            call logDebug('in subroutine replaceDynamicData')
            call mystop(errS, 'stopped')
            if (errorCheck(errS)) goto 99999
        end if
        gasPTRetrS%npressureNodes = size(inputS%pressure) - 1

        allocStatus    = 0
        sumAllocStatus = 0

        deallocate(gasPTRetrS%altNodes, STAT=allocStatus )
        allocate(gasPTRetrS%altNodes(0:gasPTRetrS%npressureNodes), STAT=allocStatus)
        sumallocStatus = sumallocStatus + allocStatus

        deallocate(gasPTRetrS%altNodesAP, STAT=allocStatus )
        allocate(gasPTRetrS%altNodesAP(0:gasPTRetrS%npressureNodes), STAT=allocStatus)
        sumallocStatus = sumallocStatus + allocStatus

        deallocate(gasPTRetrS%pressureNodes, STAT=allocStatus )
        allocate(gasPTRetrS%pressureNodes(0:gasPTRetrS%npressureNodes), STAT=allocStatus)
        sumallocStatus = sumallocStatus + allocStatus

        deallocate(gasPTRetrS%lnpressureNodes, STAT=allocStatus )
        allocate(gasPTRetrS%lnpressureNodes(0:gasPTRetrS%npressureNodes), STAT=allocStatus)
        sumallocStatus = sumallocStatus + allocStatus

        deallocate(gasPTRetrS%temperatureNodesAP, STAT=allocStatus )
        allocate(gasPTRetrS%temperatureNodesAP(0:gasPTRetrS%npressureNodes), STAT=allocStatus)
        sumallocStatus = sumallocStatus + allocStatus

        deallocate(gasPTRetrS%temperatureNodes, STAT=allocStatus )
        allocate(gasPTRetrS%temperatureNodes(0:gasPTRetrS%npressureNodes), STAT=allocStatus)
        sumallocStatus = sumallocStatus + allocStatus

        deallocate(gasPTRetrS%temperature_trueNodes, STAT=allocStatus )
        allocate(gasPTRetrS%temperature_trueNodes(0:gasPTRetrS%npressureNodes), STAT=allocStatus)
        sumallocStatus = sumallocStatus + allocStatus

        deallocate(gasPTRetrS%covTempNodesAP, STAT=allocStatus )
        allocate(gasPTRetrS%covTempNodesAP(0:gasPTRetrS%npressureNodes,0:gasPTRetrS%npressureNodes), &
                       STAT=allocStatus)
        sumallocStatus = sumallocStatus + allocStatus

        deallocate(gasPTRetrS%covTempNodes, STAT=allocStatus )
        allocate(gasPTRetrS%covTempNodes(0:gasPTRetrS%npressureNodes,0:gasPTRetrS%npressureNodes), &
                       STAT=allocStatus)
        sumallocStatus = sumallocStatus + allocStatus

        if (sumallocStatus /= 0) then
            call logDebug('FATAL ERROR: Allocation failed')
            call logDebug('for retrieval - gasPTRetrS type')
            call logDebug('in subroutine replaceDynamicData')
            call mystop(errS, 'stopped because allocation failed')
            if (errorCheck(errS)) goto 99999
        end if

        do ipressure = 0, gasPTRetrS%npressureNodes
            gasPTRetrS%pressureNodes(ipressure)            = inputS%pressure(ipressure+1)
            gasPTRetrS%lnpressureNodes(ipressure)          = log(inputS%pressure(ipressure+1))
            gasPTRetrS%temperatureNodesAP(ipressure)       = inputS%temperature(ipressure+1)
            gasPTRetrS%temperatureNodes(ipressure)         = inputS%temperature(ipressure+1)
            gasPTRetrS%covTempNodesAP(ipressure,ipressure) = 25.0 ! varianceTemperatureAP(ipressure)
            gasPTRetrS%covTempNodes(ipressure,ipressure)   = 25.0 ! varianceTemperatureAP(ipressure)
        end do


        ! Replace ISRF data

        inputS%numBands = 1
        do i = 1, inputS%numBands
            call replaceISRFData(errS, inputS, i, globalS%solarIrradianceSimS(i)%slitFunctionSpecsS)
            call replaceISRFData(errS, inputS, i, globalS%earthRadianceSimS(i)%slitFunctionSpecsS)
            call replaceISRFData(errS, inputS, i, globalS%solarIrradianceRetrS(i)%slitFunctionSpecsS)
            call replaceISRFData(errS, inputS, i, globalS%earthRadianceRetrS(i)%slitFunctionSpecsS)
        end do

        ! Do not replace O2 cross section data here, otherwise it is overwritten.

        ! call replaceXSecLUTData(errS, inputS, globalS)
        ! call replaceHRWavelengthData(errS, inputS, globalS)

goto 99999


        ! Replace specific trace gas data fields

        do iTrace = 1, nTrace

            if (.not. traceGasRetrS(iTrace)%fitProfile) then
                traceGasRetrS(iTrace)%pressure(0) = inputS%surfacePressure
                do i = 0, traceGasRetrS(iTrace)%nalt
                    if (traceGasRetrS(iTrace)%pressure(i) > inputS%surfacePressure) then
                        traceGasRetrS(iTrace)%pressure(i) = inputS%surfacePressure - i
                        ! Note: subtract i  to prevent equal values in the array
                    endif
                end do
                cycle
            endif

            if (inputS%numLevels2 /= (traceGasRetrS(iTrace)%nalt + 1)) then
                call logDebug("Invalid number of levels for trace gas in configuration file")
                call logDebugI("levels in config file: ", (traceGasRetrS(iTrace)%nalt + 1))
                call logDebugI("expected levels: ", inputS%numLevels2)
                call mystop(errS, 'stopped in replaceDynamicData')
                if (errorCheck(errS)) goto 99999
            endif

            do i = 1, inputS%numLevels2
                traceGasRetrS(iTrace)%pressure(i-1) = inputS%pressure2(inputS%numLevels2 - i + 1)
            end do

            if ( traceGasRetrS(iTrace)%useO3Climatology ) then

                ! Replace O3 CLIMATOLOGY data

                numtim  = staticS%climNumTim
                numlat  = staticS%climNumLat
                numlev  = staticS%climNumLev
                numtco3 = staticS%climNumTCO3

                call freeMemO3ClimS(errS, O3ClimRetrS)
                if (errorCheck(errS)) goto 99999

                O3ClimRetrS%dim_months_clim        = staticS%climNumTim
                O3ClimRetrS%dim_lat_clim           = staticS%climNumLat
                O3ClimRetrS%dim_total_column_clim  = staticS%climNumTCO3
                O3ClimRetrS%dim_pressures_clim     = staticS%climNumLev
                O3ClimRetrS%dim_pressures_vmr      = staticS%climNumLev + 1

                call claimMemO3ClimS(errS, O3ClimRetrS)
                if (errorCheck(errS)) goto 99999

                ! don't use the times from the netcdf file, these are in days, not months
                do i = 1, staticS%climNumTim
                    O3ClimRetrS%nodes_months(i) = i
                end do

                O3ClimRetrS%nodes_latitudes(:)    = staticS%climLat(1:numlat)
                O3ClimRetrS%nodes_total_column(:) = staticS%climTCO3(1:numtco3)
                O3ClimRetrS%nodes_pressures(:)    = staticS%climLev(1:numlev)

                O3ClimRetrS%clim_O3(1:numlev, 1:numtco3, 1:numlat, 1:numtim) = &
                    staticS%climO3(1:numlev, 1:numtco3, 1:numlat, 1:numtim)

                O3ClimRetrS%month = (inputS%yday / 30) + 1
                O3ClimRetrS%day_of_month = (inputS%yday - (inputS%yday / 30) * 30) + 1
                O3ClimRetrS%latitude = inputS%latitude

                call fillOzoneNodes(errS, O3ClimRetrS, traceGasRetrS(iTrace))
                if (errorCheck(errS)) goto 99999

            end if

        end do

99999   continue
        call exit('replaceDynamicData')

    end subroutine

    !  -------------------------------------------------------------------------------

end module S5POperationalModule
