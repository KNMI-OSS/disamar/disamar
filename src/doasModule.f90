!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

! **********************************************************************
! This module is part of DISAMAR
! The module doasModule contains subroutines to
! use the DOAS (differential optical absorption spectroscopy) method
! to retrieve columns of trace gases
!
! Author: Johan de Haan, July 2009
!
! **********************************************************************

! Approach:
!   The basic idea is to use as model for the reflectance R(l) = P(l) * exp[ - M * N * sig_d (l)]
!   where l is the wavelength, P(l) a low degree polynomial in the wavelength, M = th air mass factor
!   for the fit window, N it the vertical column of the trace gas, and sig_d(l) is the differential absorption
!   cross section. At those wavelengths where sig_d (l) = 0 we have R(l) = P(l) and these wavelengths, l_p,
!   are used for the radiative transfer calculations. Then we have R(l_p) = P(l_p) for several l_p, which can be
!   used to fit the polynomial. In addition we have the altitude resolved monochromatic air mass factor
!   m(z, l_p) at those wavelengths. From m(z, l_p) and the profile n(z) of the trace gas, the air mass factor
!   for the fit window can be calculated and the effective absorption cross section, sig_d (l), by assuming that
!   m(z, l_p) is a smooth function of the wavelength so that it can be calculated at any wavelengh using
!   interpolation. To avoid extrapolation the end points of the fit interval should coincide with the
!   zero values of the fit window. If there is more than one trace gas this might mean that during iterations the
!   size of the fit window can change, because the zero values of the total differential slant optical thickness
!   can vary if the column amounts of the different trace gases can vary.
! Steps:
!
! - initialization
! i1- Initialize the altitude resolved air mass factor m(z, l) using a geometrical amf.
!     Here m(z, l) is defined on the instrument wavelength grid and on the RTM altitude grid.
! i2- Set the first polynimial coefficient to the measured average reflectance and the others to zero.
!     Note that Ln(P(l)) is fitted, not P(l). Hence, <Rmeas> = exp(lnP0)  where lnP0 is the first
!     polynomial coefficient.
! i3- Use the a-priori values from the configuration file to initialize the columns for the trace gases.
!     Here the number densities, calculated from the volume mixing ratios specified in the configuration
!     file, are scaled so that the total column is the a-priori column. 
! i4- Get the solar irradiance on that high spectral resolution wavelength grid and evaluate the solar
!     irradiance on the instrument wavelength grid using integration over the slit function.
!
! Preparation
! p1- Determine the type and number of fit parameters from the information in the configuration file, e.g. the 
!     degree of the polynomial and the number of trace gases that are to be fitted.
! p2- Calculate the absorption cross section sig (l) for all of the trace gases on a high spectral resolution
!     wavelength grid (HR grid) and at the radiative transfer altitude grid (RTM altitude grid) (which will differ 
!     from the altitude grid specified in the configuration file, the retrieval altitude grid).
! p3- Calculate the monochromatic amf for the atmosphere using integration on the RTM grid, for all values
!     of the high resolution wavelength grid and the instrument wavelength grid.
! p4- Calculate the altitude-averaged absorption cross section on the high-resolution wavelength grid
!     using numerical integration for the RTM altitude grid.
! p5- Convolute the altitude-averaged absorption cross sections with the instrument slit function, weighted 
!     with the solar irradiance. This gives the effective absorption cross section on the instrument wavelength grid.
!     Optionally, use an expression for stronger absorption.
! p6- Fit a low-degree polynomial to the effective absorption cross section on the instrument wavelength grid.
! p7- Subtract the low degree polynomial to get the altitude averaged differential and smooth absorption cross sections
!     for each trace gas on the instrument wavelength grid.
! p8- Calculate the differential slant absorption optical thickness (summed over all trace gases) on the
!     instrument wavelength grid and use root finding (inverse linear interpolation) to obtaine the wavelengths
!     where the differential slant absorption optical thickness is zero.
! p9- Interpolate the smooth effective absorption cross section (on the instrument grid) to the wavelengths 
!     where the RTM calculations are performed and use those for the RTM calculations.
!
! - iteration
! 1 - Perform RTM calculations for the RTM wavelength grid, providing the monochromatic reflectance and
!     the altitude resolved air mass factor on the RTM altitude grid and the RTM wavelength grid.
! 2 - Fit the logarithm of the low degree polynomial [ln(P(l))] to the reflectance and the altitude resolved
!     air mass factors and evaluate them on the high-resolution wavelength grid. Next, calculate the
!     air mass factors for the atmosphere using integration weighted with the number density for each trace gas.
! 3 - Evaluate the air mass factor for the atmosphere on the instrument wavelength grid, again using integration
!     weighted with the number density for each trace gas.
! 4 - Perform steps p4 through p10 to get updated values for the altitude averaged cross section, the effective
!     absorption cross section, the effective differential absorption cross section, the wavelengths where
!     the slant differential absorption optical thickness vanishes, and the absorption cross sections
!     on the RTM wavelength grid (wavelengths may have changed).
! 5 - Using the newly calculated values for the polynomial coefficients, the air mass factor for the atmosphere,
!     the effective differential absorption cross sections, calculate the new reflectance.
! 6 - Calculate the derivatives with respect to the state vector elements (polynomial coefficients and total column).
! 7 - Calculate an update of the state vector, using the optimal estimation subroutine. Note that the
!     Newly calculated polynomial coefficients obtained through fitting of the reflectance are only used
!     to calculate the new reflectance, not to update the coefficients themselves. These newly calculated
!     polynomial coefficients are part of the forward model, and only used in the calculations for the
!     reflectance and derivatives. They can not be used to change the elements of the state vector as that
!     would interfere with the optimal estimation algorithm. Only the reflectance and the derivatives
!     should be used to estimate the new state vector.
! 8 - Update the atmospheric parameters using the new values for the state vector elements (essentailly only the
!     total columns). Scale the volume mixing ratio of the trace gases with the change in the columns.
! 9 - Test for convergence. If not converged go to step 1 of the iteration.

! - post processing
!   - calculate diagnostics
!   - write results
! - finish
!   clean up allocated memory


module doasModule

  use dataStructures
  use mathTools,                only: spline, splint, splintLin, svdfit, fpoly, svdcmp, &
                                      verfy_elem_differ, getSmoothAndDiffXsec, polyInt, slitfunction
  use radianceIrradianceModule, only: fillAltPresGridRTM, integrateSlitFunctionIrr, &
                                      allocateUD, deallocateUD
  use propAtmosphereModule,     only: getOptPropAtm, getAbsorptionXsec, update_altitudes_PTz_grid, &
                                      update_T_z_other_grids, update_ndens_vmr
  use LabosModule,              only: layerBasedOrdersScattering
  use readModule,               only: getHRSolarIrradiance
  use optimalEstmationModule,   only: calculateNewState
  use asciiHDFtools
    
  ! default is private
  private 
  public  :: performDOASfit, fillCodesFitParamDOAS
  public  :: interpolateXsecRTMaltGrid, calcTemperatureNdensRTMgrid
  public  :: convoluteWeakAbsSlitS, calculateAltAveragedXsecHR, calcReflAndAMFRTM, finishDOAS

  contains

    subroutine performDOASfit(errS, staticS,                                                      &
                              numSpectrBands, nTrace, wavelInstrS, wavelMRS, wavelHRS, geometryS, &
                              XsecHRS, gasPTSimS, gasPTRetrS,  traceGasSimS, traceGasRetrS,       &
                              solarIrrS, earthRadianceS, surfaceS, LambCloudS,                    &
                              mieAerS, cldAerFractionS, weakAbsRetrS, controlS,                   &
                              retrS, cloudAerosolRTMgridS, RRS_RingSimS,                          &
                              RRS_RingRetrS, optPropRTMGridS, diagnosticS)

      ! This subroutine performa a modified DOAS fit where the air mass factor is wavelength
      ! denpendent and is calculated on line using wavelengths where the slant differential
      ! absorption is zero.

      implicit none

      type(errorType),               intent(inout) :: errS
      type(staticType),              intent(in)    :: staticS
      integer,                       intent(in)    :: numSpectrBands                  ! number wavelength band
      integer,                       intent(in)    :: nTrace                          ! number of trace gases
      type(wavelInstrType),          intent(in)    :: wavelInstrS(numSpectrBands)     ! wavelength grid for retrieval
      type(wavelHRType),             intent(in)    :: wavelMRS(numSpectrBands)        ! high/instr resolution wavelength grid
      type(wavelHRType),             intent(in)    :: wavelHRS(numSpectrBands)        ! high resolution wavelength grid
      type(geometryType),            intent(in)    :: geometryS                       ! info geometry
      type(XsecType),                intent(inout) :: XsecHRS(numSpectrBands, nTrace) ! absorption cross section
      type(gasPTType),               intent(inout) :: gasPTSimS                       ! pressure and temperature
      type(gasPTType),               intent(inout) :: gasPTRetrS                      ! pressure and temperature
      type(traceGasType),            intent(inout) :: traceGasSimS(nTrace)            ! atmospheric trace gas properties
      type(traceGasType),            intent(inout) :: traceGasRetrS(nTrace)           ! atmospheric trace gas properties
      type(SolarIrrType),            intent(inout) :: solarIrrS(numSpectrBands)       ! solar irradiance
      type(earthRadianceType),       intent(inout) :: earthRadianceS(numSpectrBands)  ! earth radiance; info slit function
      type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)        ! surface properties
      type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)      ! Lambertian cloud properties
      type(mieScatType),             intent(inout) :: mieAerS(maxNumMieModels)        ! specification expansion coefficients
      type(cldAerFractionType),      intent(inout) :: cldAerFractionS(numSpectrBands) ! cloud / aerosol fraction
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands)    ! DOAS results
      type(controlType),             intent(inout) :: controlS                        ! control parameters
      type(retrType),                intent(inout) :: retrS                           ! retrieval structure
      type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS            ! cloud-aerosol properties
      type(RRS_RingType),            intent(inout) :: RRS_RingSimS(numSpectrBands)    ! Ring spectra for simulation
      type(RRS_RingType),            intent(inout) :: RRS_RingRetrS(numSpectrBands)   ! Ring spectra for retrieval
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS                 ! optical properties on RTM grid
      type(diagnosticType),          intent(inout) :: diagnosticS                     ! diagnostic information

      ! local
      integer           :: iband, iTrace
      integer           :: iteration

      type(wavelHRType) :: wavelRTMS(numSpectrBands)         ! wavelength grid for RTM calculations for DOAS

      type(XsecType)    :: XsecRTMS(numSpectrBands,nTrace)   ! absorption cross sections at the wavelengths
                                                             ! for the RTM calculations. i.e. at those wavelengths
                                                             ! where the total differential absorption is zero.
                                                             ! the altitude grid is the retrieval grid from the
                                                             ! configuration file

      ! Absorption cross sections on the high resolution wavelength grid and RTM altitude grid
      type(XsecType) :: XsecHRRTMaltS(numSpectrBands,nTrace)  ! same as XsecHRS, but 
                                                              ! interpolated to the RTM altitude grid

      ! START of the calculations

      ! fill altitude grid for radiative transfer calculalations
      ! this altitude grid uses Gaussian division points for the intervals
      ! and can also be used to integrate over the altitude
      ! the altitude resolved air mass factors are calculated on this grid
      ! and we use it to calculate the monochromatic air mass factor for the
      ! entire atmosphere for each trace gas. In addition it is used to calculate
      ! the effective absorption cross section for each trace gas when the
      ! temperature is not fitted but the temperature profile is assumed to be known

      call fillAltPresGridRTM(errS, cloudAerosolRTMgridS, OptPropRTMGridS)
      if (errorCheck(errS)) return

      ! calculate the number density and temperature on RTM altitude grid
      call calcTemperatureNdensRTMgrid(errS, nTrace, gasPTRetrS, traceGasRetrS, optPropRTMGridS)
      if (errorCheck(errS)) return

      ! NOTE that it ia assumed that the cloud altitude can not change (not a fit parameter)
      ! and therefor the RTM grid remains constant (not correct for O2-O2 but O2-O2 needs
      ! special treatment due to its pressure dependence which is not accounted for at present)

      ! claim memory and initialize weakAbsRetrS using geometrical air mass factors, XsecInstrS, and  XsecInstrRTMaltS
      ! initialize the number of wavelengths for the radiative transfer calculations using the value from the configuration file
      ! if differential cross sections are used the number of wavelengths and the array sizes will be updated.

      call initializeDOAS(errS, numSpectrBands, nTrace, geometryS, traceGasRetrS, optPropRTMGridS, XsecHRS,         &
                          weakAbsRetrS, wavelInstrS, wavelRTMS, retrS, XsecRTMS, XsecHRRTMaltS)
      if (errorCheck(errS)) return

      ! get solar irradiance on HR wavelength retrieval grid and convolute with the slit function
      ! this is used for the I0 effect for the effective cross section
      ! In addition, calculate the Ring spectra using for the convolution the earth radiance slit function
      do iband = 1, numSpectrBands
        call getHRSolarIrradiance(errS, wavelMRS(iband), solarIrrS(iband), RRS_RingRetrS(iband), wavelHRS(iband))
        if (errorCheck(errS)) return
        call integrateSlitFunctionIrr(errS, controlS, wavelHRS(iband), wavelInstrS(iband), solarIrrS(iband), &
             earthRadianceS(iband), RRS_RingRetrS(iband))
        if (errorCheck(errS)) return
      end do

      ! Get the absorption coefficients on HR wavelength grid at the retrieval altitude grid.
      ! This is the altitude grid specified in the configuration file which can differ for different trace gases.
      ! If the temperature is fitted, the absorption cross section and its temperature
      ! derivative at the reference temperature are stored in XsecHRS, otherwise the temperature
      ! derivative is set to zero and the absorption cross section for the local temperature
      ! and pressure is stored in XsecHRS

      do iband = 1, numSpectrBands
        do iTrace = 1, nTrace
          call getAbsorptionXsec(errS, staticS, controlS, weakAbsRetrS(iband), wavelHRS(iband), gasPTRetrS, &
                                 traceGasRetrS(iTrace), XsecHRS(iband,iTrace))
          if (errorCheck(errS)) return
          call interpolateXsecRTMaltGrid(errS, traceGasRetrS(iTrace), optPropRTMGridS, XsecHRS(iband,iTrace), &
                                         XsecHRRTMaltS(iband,iTrace))
          if (errorCheck(errS)) return
        end do ! iTrace
      end do ! iband

      ! calculate the altitude averaged absorption cross section on the high resolution wavelength grid
      do iband = 1, numSpectrBands
        call calculateAltAveragedXsecHR(errS, nTrace, optPropRTMGridS, wavelHRS(iband), XsecHRRTMaltS(iband,:), &
                                        weakAbsRetrS(iband))
        if (errorCheck(errS)) return
      end do ! iband

      ! convolute with slit function, set wavelength where the differential absorption is zero, and
      ! fill the absorption cross sections for the RTM calculations
      do iband = 1, numSpectrBands

        call convoluteWeakAbsSlitS(errS, nTrace, controlS, wavelHRS(iband), traceGasRetrS, solarIrrS(iband),  &
                                   earthRadianceS(iband), wavelInstrS(iband), weakAbsRetrS(iband))
        if (errorCheck(errS)) return

        do iTrace = 1, nTrace
          call getSmoothAndDiffXsec(errS, weakAbsRetrS(iband)%degreePoly, wavelInstrS(iband)%nwavel,  &
                                    wavelInstrS(iband)%wavel,                                   &
                                    weakAbsRetrS(iband)%XsecEffInstr(:,iTrace),                 &
                                    weakAbsRetrS(iband)%XsecEffSmoothInstr(:,iTrace),           &
                                    weakAbsRetrS(iband)%XsecEffDiffInstr(:,iTrace) )
          if (errorCheck(errS)) return
        end do ! iTrace


        call setWavelRTM(errS, nTrace, wavelInstrS(iband), weakAbsRetrS(iband), wavelRTMS(iband))
        if (errorCheck(errS)) return

        call setXsecRTM(errS, nTrace, wavelInstrS(iband), weakAbsRetrS(iband), wavelRTMS(iband), &
                        XsecRTMS(iband,:))
        if (errorCheck(errS)) return

      end do ! iband


      call fillAPrioriDOAS(errS, numSpectrBands, nTrace, traceGasSimS, traceGasRetrS,  &
                           weakAbsRetrS, RRS_RingRetrS, gasPTSimS, gasPTRetrS, retrS)
      if (errorCheck(errS)) return

      ! initialize
      retrS%isConverged = .false.
      ! set initial (starting) values
      retrS%x               = retrS%xa
      retrS%xPrev           = retrS%xa
      retrS%dx              = 0.0d0
      retrS%SInv_lnvmr      = 0.0d0
      retrS%SInv_vmr        = 0.0d0
      retrS%SInv_ndens      = 0.0d0

      do iteration = 1, retrS%maxNumIterations

        call logDebugI('iteration DOAS = ', iteration)
        retrS%numIterations = iteration

        ! calculate the reflectance and derivatives 
        call calculateReflAndDerivInstr(errS, numSpectrBands, nTrace, wavelInstrS, wavelRTMS,         &
                                        geometryS, XsecRTMS, gasPTRetrS, traceGasRetrS,         &
                                        surfaceS, LambCloudS, mieAerS,                          &
                                        cldAerFractionS, weakAbsRetrS, RRS_RingRetrS, controlS, &
                                        retrS, cloudAerosolRTMgridS, optPropRTMGridS)
        if (errorCheck(errS)) return

        ! Calculate the air mass factor on the instrument grid and HR grid from the values on the DOAS grid
        ! by fitting a low degree polynomial to the values calculated for the RTM wavelengths

        do iband = 1, numSpectrBands

          ! calculate the air mass factors for the atmosphere on HR and instrument grid using the altitude
          ! rsolved air mass factors calculated in 'calculateReflAndDerivInstr'
          call process_AMF(errS, nTrace, optPropRTMGridS, wavelRTMS(iband), wavelHRS(iband), wavelInstrS(iband), &
                           weakAbsRetrS(iband))
          if (errorCheck(errS)) return

          ! calculate the altitude averaged absorption cross section on the high resolution wavelength grid
          call calculateAltAveragedXsecHR(errS, nTrace, optPropRTMGridS, wavelHRS(iband), XsecHRRTMaltS(iband,:), &
                                          weakAbsRetrS(iband))
          if (errorCheck(errS)) return

          ! convolute the altitude averaged absorption cross section with the slit function to get
          ! the effective absorption cross section on the instrument wavelength grid
          call convoluteWeakAbsSlitS(errS, nTrace, controlS, wavelHRS(iband), traceGasRetrS, solarIrrS(iband),  &
                                     earthRadianceS(iband), wavelInstrS(iband), weakAbsRetrS(iband))
          if (errorCheck(errS)) return

          ! calculate the smooth and differential part of the effective absorption cross section
          call getSmoothAndDiffXsec(errS, weakAbsRetrS(iband)%degreePoly, wavelInstrS(iband)%nwavel,  &
                                    wavelInstrS(iband)%wavel,weakAbsRetrS(iband)%XsecEffInstr,  &
                                    weakAbsRetrS(iband)%XsecEffSmoothInstr,                     &
                                    weakAbsRetrS(iband)%XsecEffDiffInstr)
          if (errorCheck(errS)) return

          ! determine the wavelengths and absorption cross sections that will be used in the
          ! radiative transfer calculations, i.e. fill wavelRTMS and XsecRTMS

          call setWavelRTM(errS, nTrace, wavelInstrS(iband), weakAbsRetrS(iband), wavelRTMS(iband))
          if (errorCheck(errS)) return

          call setXsecRTM(errS, nTrace, wavelInstrS(iband), weakAbsRetrS(iband), wavelRTMS(iband), &
                          XsecRTMS(iband,:))
          if (errorCheck(errS)) return

        end do ! iband


        ! update dR
        retrS%dR      = retrS%reflMeas - retrS%refl

        ! fill initial residue
        if ( iteration == 1 ) retrS%dR_initial = retrS%dR

        ! calculate new state vector and set convergence flag retrS%isConverged
        call calculateNewState(errS, iteration, retrS)
        if (errorCheck(errS)) return

        ! update fit parameters
        call updateFitParametersDOAS(errS, numSpectrBands, nTrace, traceGasRetrS, retrS, weakAbsRetrS, &
                                     RRS_RingRetrS, gasPTRetrS, surfaceS, cloudAerosolRTMgridS, LambCloudS)
        if (errorCheck(errS)) return

        ! update values in the retrieval structure
        retrS%reflPrev = retrS%refl
        retrS%xPrev    = retrS%x

        if ( retrS%isConverged ) exit  ! exit iteration loop

      end do ! iteration

      call calculateDiagnosticsDOAS(errS, retrS, diagnosticS)
      if (errorCheck(errS)) return

      call updateCovFitParametersDOAS(errS, numSpectrBands, nTrace,  traceGasRetrS, retrS, diagnosticS, &
                                      weakAbsRetrS, RRS_RingRetrS, gasPTRetrS)
      if (errorCheck(errS)) return

      call print_resultsDOAS(errS, numSpectrBands, nTrace, wavelInstrS, traceGasSimS, traceGasRetrS,    &
                             weakAbsRetrS, RRS_RingSimS, RRS_RingRetrS, retrS, diagnosticS)
      if (errorCheck(errS)) return

      call print_resultsDOAS_asciiHDF(errS, numSpectrBands, nTrace, geometryS, gasPTSimS, gasPTRetrS,      &
                             traceGasSimS, traceGasRetrS, weakAbsRetrS, RRS_RingSimS, RRS_RingRetrS, &
                             retrS, diagnosticS)
      if (errorCheck(errS)) return

      ! clean up

      call finishDOAS(errS, numSpectrBands, nTrace, weakAbsRetrS, wavelRTMS,  XsecRTMS, XsecHRRTMaltS)
      if (errorCheck(errS)) return

    end subroutine performDOASfit


    subroutine initializeDOAS(errS, numSpectrBands, nTrace, geometryS, traceGasS, optPropRTMGridS, XsecHRS,  &
                              weakAbsRetrS, wavelInstrS, wavelRTMS, retrS, XsecRTMS, XsecHRRTMaltS)


      ! Purpose of this subroutime is to claim memory for the arrays in the structures
      ! wavelRTMS, XsecRTMS, and XsecHRRTMaltS.
      ! These are initial value and the size of the arrays may be updated later after the number of wavelengths
      ! corresponding to zero value in the total differential absorption is known. Then the arrays in
      ! wavelRTMS, and XsecRTMS may get updated dimensions.
      ! Futher, the air mass factors in weakAbsRetrS are initialized with geometrical air mass factors
      
      implicit none

      type(errorType), intent(inout) :: errS
      integer,                  intent(in)    :: numSpectrBands                           ! number of spectral bands
      integer,                  intent(in)    :: nTrace                                   ! number of trace gases
      type(geometryType),       intent(in)    :: geometryS                                ! info geometry
      type(traceGasType),       intent(in)    :: traceGasS(nTrace)                        ! properties of the trace gases
      type(optPropRTMGridType), intent(inout) :: optPropRTMGridS                          ! optical properties on RTM grid
      type(XsecType),           intent(in)    :: XsecHRS(numSpectrBands, nTrace)          ! absorption cross section
      type(weakAbsType),        intent(inout) :: weakAbsRetrS(numSpectrBands)             ! DOAS results
      type(wavelInstrType),     intent(in)    :: wavelInstrS(numSpectrBands)              ! wavelength grid for retrieval
      type(wavelHRType),        intent(inout) :: wavelRTMS(numSpectrBands)                ! wavelengths RTM calculations
      type(retrType),           intent(in)    :: retrS                                    ! retrieval structure
      type(XsecType),           intent(inout) :: XsecRTMS(numSpectrBands, nTrace)
      type(XsecType),           intent(inout) :: XsecHRRTMaltS(numSpectrBands,nTrace)

      ! local
      integer :: iband, iTrace, iwave, ialt, indexWavelRefl
      real(8) :: geometricalAMF, sumRefl

      geometricalAMF = 1.0d0 / geometryS%uu + 1.0d0 / geometryS%u0

      do iband = 1, numSpectrBands
        wavelRTMS(iband)%nGaussMax = 0
        wavelRTMS(iband)%nGaussMin = 0
        wavelRTMS(iband)%nwavel    = weakAbsRetrS(iband)%degreePoly + 1
      end do ! iband

      ! fill values for cross sections XsecRTMS
      do iTrace = 1, nTrace
        do iband = 1, numSpectrBands
          XsecRTMS(iband, iTrace)%XsectionFileName = XsecHRS(iband, iTrace)%XsectionFileName
          XsecRTMS(iband, iTrace)%useHITRAN        = XsecHRS(iband, iTrace)%useHITRAN
          XsecRTMS(iband, iTrace)%factorLM         = XsecHRS(iband, iTrace)%factorLM
          XsecRTMS(iband, iTrace)%gasIndex         = XsecHRS(iband, iTrace)%gasIndex
          XsecRTMS(iband, iTrace)%ISO              = XsecHRS(iband, iTrace)%ISO
          XsecRTMS(iband, iTrace)%thresholdLine    = XsecHRS(iband, iTrace)%thresholdLine
          XsecRTMS(iband, iTrace)%nwavelHR         = XsecHRS(iband, iTrace)%nwavelHR
          XsecRTMS(iband, iTrace)%nwavel           = weakAbsRetrS(iband)%degreePoly + 1
          XsecRTMS(iband, iTrace)%nalt             = traceGasS(iTrace)%nalt
        end do ! iband
      end do ! iTrace


      ! fill values for cross sections XsecHRRTMaltS
      do iTrace = 1, nTrace
        do iband = 1, numSpectrBands
          XsecHRRTMaltS(iband, iTrace)%XsectionFileName = XsecHRS(iband, iTrace)%XsectionFileName
          XsecHRRTMaltS(iband, iTrace)%useHITRAN        = XsecHRS(iband, iTrace)%useHITRAN
          XsecHRRTMaltS(iband, iTrace)%factorLM         = XsecHRS(iband, iTrace)%factorLM
          XsecHRRTMaltS(iband, iTrace)%gasIndex         = XsecHRS(iband, iTrace)%gasIndex
          XsecHRRTMaltS(iband, iTrace)%ISO              = XsecHRS(iband, iTrace)%ISO
          XsecHRRTMaltS(iband, iTrace)%thresholdLine    = XsecHRS(iband, iTrace)%thresholdLine
          XsecHRRTMaltS(iband, iTrace)%nwavelHR         = XsecHRS(iband, iTrace)%nwavelHR
          XsecHRRTMaltS(iband, iTrace)%nwavel           = XsecHRS(iband, iTrace)%nwavel
          XsecHRRTMaltS(iband, iTrace)%nalt             = optPropRTMGridS%RTMnlayer
        end do ! iband
      end do ! iTrace

      ! allocate arrays in structures
      do iband = 1, numSpectrBands
        call claimMemWeakAbsS(errS,  weakAbsRetrS(iband) )
        if (errorCheck(errS)) return
        call claimMemWavelHRS(errS,  wavelRTMS(iband) )
        if (errorCheck(errS)) return
        do iTrace = 1, nTrace
          call claimMemXsecS(errS,  XsecRTMS(iband, iTrace) )
          if (errorCheck(errS)) return
          call claimMemXsecS(errS,  XsecHRRTMaltS(iband, iTrace) )
          if (errorCheck(errS)) return
        end do ! iTrace
      end do ! iband 

      ! fill values for weakAbsRetrS
      do iband = 1, numSpectrBands
        do iTrace = 1, nTrace

          weakAbsRetrS(iband)%columnAP       (iTrace) = traceGasS(iTrace)%columnAP
          weakAbsRetrS(iband)%column         (iTrace) = traceGasS(iTrace)%column
          weakAbsRetrS(iband)%covColumnAP    (iTrace) = traceGasS(iTrace)%covColumnAP
          weakAbsRetrS(iband)%covColumn      (iTrace) = traceGasS(iTrace)%covColumn
          weakAbsRetrS(iband)%amfWindow      (iTrace) = geometricalAMF
        
          weakAbsRetrS(iband)%amfInstrGrid(:,iTrace)  = geometricalAMF
          weakAbsRetrS(iband)%amfHRGrid(:,iTrace)     = geometricalAMF
        end do ! iTrace

        weakAbsRetrS(iband)%covlnPolyCoefAP(:) = 100.0d0
        weakAbsRetrS(iband)%covlnPolyCoef(:)   = 100.0d0

        do iwave = 1, weakAbsRetrS(iband)%nwavelRTM
          do ialt = 0, weakAbsRetrS(iband)%RTMnlayer
            weakAbsRetrS(iband)%amfAltRTM(ialt,iwave)         = geometricalAMF
          end do
        end do

        do iwave = 1, weakAbsRetrS(iband)%nwavelInstr
          do ialt = 0, weakAbsRetrS(iband)%RTMnlayer
            weakAbsRetrS(iband)%amfAltInstr(ialt, iwave)      = geometricalAMF
          end do
        end do

        do iwave = 1, weakAbsRetrS(iband)%nwavelHR
          do ialt = 0, weakAbsRetrS(iband)%RTMnlayer
            weakAbsRetrS(iband)%amfAltHR(ialt, iwave)         = geometricalAMF
          end do
        end do

      end do ! iband

      ! initialize the first term of the polynomial (constant) with the logarithm of the average
      ! value of the reflectance for the spectral band involved
      ! note that in retrS%reflMeas the the spectral bands are not distinguished
      
      indexWavelRefl = 1
      do iband = 1, numSpectrBands
        weakAbsRetrS(iband)%lnPolyCoefAP(:) = 0.0d0
        weakAbsRetrS(iband)%lnPolyCoef(:)   = 0.0d0
        sumRefl = 0.0d0
        do iwave = 1, wavelInstrS(iband)%nwavel
          sumRefl = sumRefl + retrS%reflMeas(indexWavelRefl)
          indexWavelRefl = indexWavelRefl + 1
        end do !iwave
        weakAbsRetrS(iband)%lnPolyCoefAP(0) = log( sumRefl / (wavelInstrS(iband)%nwavel) ) 
        weakAbsRetrS(iband)%lnPolyCoef  (0) = weakAbsRetrS(iband)%lnPolyCoefAP(0)
      end do ! iband

    end subroutine initializeDOAS


    subroutine finishDOAS(errS, numSpectrBands, nTrace, weakAbsRetrS, wavelRTMS, XsecRTMS, XsecHRRTMaltS, &
                          diffXsecHRRTMaltS, smoothXsecHRRTMaltS)

      implicit none

      type(errorType), intent(inout) :: errS
      integer,           intent(in)    :: numSpectrBands                           ! number of spectral bands
      integer,           intent(in)    :: nTrace                                   ! number of trace gases
      type(weakAbsType), intent(inout) :: weakAbsRetrS(numSpectrBands)             ! DOAS results
      type(wavelHRType), intent(inout) :: wavelRTMS(numSpectrBands)                 ! wavelength grid for RTM calculations
      type(XsecType),    intent(inout) :: XsecRTMS(numSpectrBands, nTrace)         ! DOAS absorption cross sections
      type(XsecType),    intent(inout) :: XsecHRRTMaltS(numSpectrBands,nTrace)
      type(XsecType),    intent(inout) :: diffXsecHRRTMaltS(numSpectrBands,nTrace)
      type(XsecType),    intent(inout) :: smoothXsecHRRTMaltS(numSpectrBands,nTrace)

      optional :: diffXsecHRRTMaltS, smoothXsecHRRTMaltS

      ! local
      integer :: iband, iTrace

      ! clean up

      do iband = 1, numSpectrBands
        call freeMemWeakAbsS(errS,  weakAbsRetrS(iband) )
        if (errorCheck(errS)) return
        call freeMemWavelHRS(errS,  wavelRTMS(iband) )
        if (errorCheck(errS)) return
        do iTrace = 1, nTrace
          call freeMemXsecS(errS,  XsecRTMS(iband, iTrace) )
          if (errorCheck(errS)) return
          call freeMemXsecS(errS,  XsecHRRTMaltS(iband, iTrace) )
          if (errorCheck(errS)) return
          if ( present( diffXsecHRRTMaltS ) ) call freeMemXsecS(errS,  diffXsecHRRTMaltS(iband, iTrace) )
                                              if (errorCheck(errS)) return
          if ( present( smoothXsecHRRTMaltS ) ) call freeMemXsecS(errS,  smoothXsecHRRTMaltS(iband, iTrace) )
                                                if (errorCheck(errS)) return
        end do ! iTrace
      end do ! iband 

    end subroutine finishDOAS


    subroutine convoluteWeakAbsSlitS(errS, nTrace, controlS, wavelHRS, traceGasS, solarIrrS, earthRadianceS, &
                                     wavelInstrS, weakAbsRetrS)

      ! convolute the altitude averaged absorption cross sections with the instrument slit function weighted with
      ! the solar irradiance (I0 effect) using the weakAbsRetrS structure
      !
      ! input:  wavelHRS        high-resolution wavelength grid with gaussian division points so that
      !                         integration over the slit function is simple
      !         traceGasS       contains info on the altitudes where the absorption cross sections were calculated
      !         solarIrrS       solar irradiance for the I0 effect
      !         earthRadianceS  contains info on the slit function
      !         XsecHRS         cross sections on high resolution grid ( and derivative w.r.t. the temperature dXsec / dT  ) 
      !         wavelInstrS     nominal wavelengths for the spectral bins of the instrument
      !
      ! input / output: weakAbsRetrS cross sections convoluted with the slit function

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: nTrace             ! number of trace gases
      type(controlType),             intent(inout) :: controlS           ! control parameters
      type(wavelHRType),             intent(in)    :: wavelHRS           ! high resolution wavelength grid
      type(traceGasType),            intent(in)    :: traceGasS(nTrace)  ! contains altitudes of the cross section
      type(SolarIrrType),            intent(in)    :: solarIrrS          ! solar irradiance
      type(earthRadianceType),       intent(inout) :: earthRadianceS     ! earth radiance; info slit function
      type(wavelInstrType),          intent(in)    :: wavelInstrS        ! wavelength grid for retrieval
      type(weakAbsType),             intent(inout) :: weakAbsRetrS       ! DOAS results

      ! local
      real(8) :: slitfunctionValues(wavelHRS%nwavel)
      real(8) :: attenuation(wavelHRS%nwavel)
      real(8) :: wavelScaledHR(wavelHRS%nwavel)
      real(8) :: lnpolynomialHR(wavelHRS%nwavel)
      real(8) :: polynomialHR(wavelHRS%nwavel)
      real(8) :: wavelScaledInstr(wavelInstrS%nwavel)
      real(8) :: lnpolynomialInstr(wavelInstrS%nwavel)
      real(8) :: polynomialInstr(wavelInstrS%nwavel)
      real(8) :: arglog, Xsec
      real(8) :: wStart, wEnd
      integer :: i, iwave, iTrace, iPoly
      integer :: startIndex, endIndex, index

      logical, parameter :: verbose = .false.
      logical, parameter :: verboseSlitFunction = .true.

      if ( weakAbsRetrS%XsecStrongAbs .and. (controlS%method == 2) ) then

        ! expressions for strong absorption; not available for classical DOAS as the column is not known
        ! because the slant column is fitted

        ! Demand that integration over the slit function is consistent with the expression
        ! Imeas = Fmeas * P * exp[- M * N * sig_eff ] where I meas is the measured radiances,
        ! Fmeas is the measured solar irradiance, P is the DOAS polynomial, M is the wavelength dependend
        ! air mass factor, N is the total column, and sig_eff is the effective absorption cross section.

        ! calculate the wavelengths scaled to (-1,+1)
        wStart = wavelInstrS%wavel(1)
        wEnd   = wavelInstrS%wavel( wavelInstrS%nwavel )

        wavelScaledHR(:)    = 2.0d0 * ( wavelHRS%wavel(:)    - wStart ) / ( wEnd - wStart ) - 1.0d0
        wavelScaledInstr(:) = 2.0d0 * ( wavelInstrS%wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0

        ! calculate polynomial
        lnpolynomialHR(:)    =  weakAbsRetrS%lnpolyCoef(0)
        lnpolynomialInstr(:) =  weakAbsRetrS%lnpolyCoef(0)
        do iPoly = 1, weakAbsRetrS%degreePoly
          lnpolynomialHR(:)    = lnpolynomialHR(:)  &
                               + weakAbsRetrS%lnpolyCoef(iPoly) * wavelScaledHR(:)    ** iPoly
          lnpolynomialInstr(:) = lnpolynomialInstr(:)  &
                               + weakAbsRetrS%lnpolyCoef(iPoly) * wavelScaledInstr(:) ** iPoly
        end do ! iPoly

        polynomialHR(:)    = exp( lnpolynomialHR(:) )
        polynomialInstr(:) = exp( lnpolynomialInstr(:) )

        do iwave = 1, wavelInstrS%nwavel
          slitfunctionValues = slitfunction(errS, wavelHRS, iwave, wavelInstrS%nwavel, wavelInstrS%wavel,  &
                                            earthRadianceS%slitFunctionSpecsS, startIndex, endIndex)
          do index = startIndex, endIndex
            ! multiply with gaussian weights for wavelength integration
            slitfunctionValues(index) = wavelHRS%weight(index) * slitfunctionValues(index)
            ! multiply with polynomial
            slitfunctionValues(index) = polynomialHR(index) * slitfunctionValues(index)
            ! multiply with solar irradiance for I0 effect
            slitfunctionValues(index) = solarIrrS% solIrrHR(index) * slitfunctionValues(index)
          end do ! index
          do iTrace = 1, nTrace
            ! calculate attenumation: exp ( - M N sigma )
            arglog = 0.0d0
            do index = startIndex, endIndex
              attenuation(index) = exp( - weakAbsRetrS%amfHRGrid(index,iTrace) * weakAbsRetrS%column(iTrace)  &
                                 * weakAbsRetrS%XsecAvAltHR(index,iTrace) )
              arglog = arglog + attenuation(index) * slitfunctionValues(index) 
            end do ! index
            arglog = arglog / solarIrrS%solIrr(iwave) / polynomialInstr(iwave)
            weakAbsRetrS%XsecEffInstr(iwave,iTrace) = - log(arglog) / weakAbsRetrS%column(iTrace) &
                                                    / weakAbsRetrS%amfInstrGrid(iwave,iTrace)
          end do ! iTrace

        end do ! iwave

      else 

        ! normal expressions for weak absorption
        ! initialize
        slitfunctionValues = 0.0
        do iwave = 1, wavelInstrS%nwavel
          slitfunctionValues = slitfunction(errS, wavelHRS, iwave, wavelInstrS%nwavel, wavelInstrS%wavel,  &
                                            earthRadianceS%slitFunctionSpecsS, startIndex, endIndex)
          if ( verboseSlitFunction .and. iwave ==1 ) then
             write(addtionalOutputUnit,*)
             write(addtionalOutputUnit,'(A)') 'retrieval'
             write(addtionalOutputUnit,'(A)') 'wavelength    slitfunctionValues'
             do i = startIndex, endIndex
               write(addtionalOutputUnit,'(2F15.9)') wavelHRS%wavel(i), slitfunctionValues(i)
             end do
             write(addtionalOutputUnit,*)
             write(addtionalOutputUnit,'(A, F10.4)') 'mean             ', earthRadianceS%slitFunctionSpecsS%mean
             write(addtionalOutputUnit,'(A, F10.4)') 'standardDeviation', earthRadianceS%slitFunctionSpecsS%standardDeviation
             write(addtionalOutputUnit,'(A, F10.4)') 'skewness         ', earthRadianceS%slitFunctionSpecsS%skewness
             write(addtionalOutputUnit,'(A, F10.4)') 'kurtosis         ', earthRadianceS%slitFunctionSpecsS%kurtosis
             write(addtionalOutputUnit,'(A, F10.4)') 'slitIntegrated   ', earthRadianceS%slitFunctionSpecsS%slitIntegrated
          end if
          do index = startIndex, endIndex
            ! multiply with gaussian weights for wavelength integration
            slitfunctionValues(index) = wavelHRS%weight(index) * slitfunctionValues(index)
            ! multiply with solar irradiance for I0 effect
            slitfunctionValues(index) = slitfunctionValues(index) * solarIrrS%solIrrHR(index)
          end do ! index
          do iTrace = 1, nTrace
            Xsec = 0.0d0
            do index = startIndex, endIndex
              Xsec = Xsec + weakAbsRetrS%XsecAvAltHR(index,iTrace) * slitfunctionValues(index)
            end do ! index
            weakAbsRetrS%XsecEffInstr(iwave,iTrace) = Xsec / solarIrrS%solIrr(iwave)
          end do ! iTrace
        end do ! iwave

      end if !  weakAbsRetrS%XsecStrongAbs

      if ( verbose ) then

        ! write absorption cross sections to intermediate file
        do iTrace = 1, nTrace
          write(intermediateFileUnit,*)
          write(intermediateFileUnit,'(2A)')                                               &
           'absorption cross section before convolution with slit function, trace gas = ', &
           traceGasS(iTrace)%nameTraceGas
          write(intermediateFileUnit,'(A)') '   wavelength       Xsec'
          do iwave = 1, wavelHRS%nwavel
             write(intermediateFileUnit,'(F15.5, ES15.5)')  wavelHRS%wavel(iwave),                 &
                                                            weakAbsRetrS%XsecAvAltHR(iwave,iTrace)
          end do ! iwave
          write(intermediateFileUnit,*)
          write(intermediateFileUnit,'(2A)')                                               &
           'absorption cross section after convolution with slit function, trace gas = ',  &
           traceGasS(iTrace)%nameTraceGas
          write(intermediateFileUnit,'(A)') '   wavelength       Xsec'
          do iwave = 1, wavelInstrS%nwavel
             write(intermediateFileUnit,'(F15.5, ES15.5)') wavelInstrS%wavel(iwave),                &
                                                           weakAbsRetrS%XsecEffInstr(iwave,iTrace)
          end do ! iwave
        end do ! iTrace

      end if

    end subroutine convoluteWeakAbsSlitS


    subroutine interpolateXsecRTMaltGrid(errS, traceGasS, optPropRTMGridS, XsecS, XsecRTMaltS)

      ! use cubic spline interpolation to calculate the absorption cross sections and the temperature derivative
      ! at the radiative transfer altitude grid

      implicit none

      type(errorType), intent(inout) :: errS
      type(traceGasType),            intent(in)    :: traceGasS        ! contains altitudes retrieval grid
      type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS  ! contains altitudes of RTM grid
      type(XsecType),                intent(in)    :: XsecS            ! absorption cross sections on retrieval grid
      type(XsecType),                intent(inout) :: XsecRTMaltS      ! absorption cross sections on RTM grid

      ! local
      integer :: iwave, ialt
      integer :: status
      real(8) :: SDXsec(0:traceGasS%nalt)


      do iwave = 1, XsecS%nwavelHR
        call spline(errS, traceGasS%alt(:), XsecS%Xsec(iwave,:), SDXsec(:) , status)
        if (errorCheck(errS)) return
        do ialt = 0, optPropRTMGridS%RTMnlayer
          XsecRTMaltS%Xsec(iwave,ialt) = splint(errS, traceGasS%alt(:), XsecS%Xsec(iwave,:), SDXsec(:), &
                                                optPropRTMGridS%RTMaltitude(ialt), status)
        end do ! ialt
      end do ! iwave

    end subroutine interpolateXsecRTMaltGrid


    subroutine calcTemperatureNdensRTMgrid(errS, nTrace, gasPTS, traceGasS, optPropRTMGridS)

      ! calculate number density of the trace gases and the temperature on the RTM altitude grid

      type(errorType), intent(inout) :: errS
      integer,                  intent(in)    :: nTrace             ! number of trace gases
      type(gasPTType),          intent(in)    :: gasPTS             ! pressure and temperature from configuration file
      type(traceGasType),       intent(in)    :: traceGasS(nTrace)  ! trace gas properties on retrieval grid
      type(optPropRTMGridType), intent(inout) :: optPropRTMGridS    ! properties on RTM grid
      
      ! local
      integer  :: ialt, iTrace
      integer  :: status

      ! tempory arrays for interpolation
      real(8)  :: lnpressureGrid(0:gasPTS%npressure)
      real(8)  :: SDlnpressure  (0:gasPTS%npressure)
      real(8)  :: SDtemperature (0:gasPTS%npressure)

      ! tempory arrays for interpolation; here the length depends on the trace gas
      real(8), allocatable  :: lnvmrTraceGridAP(:)
      real(8), allocatable  :: SDlnvmrTraceAP(:)

      ! arrays on RTM grid (p in hPa, T in K, vmr in ppmv, ndens in mol cm-3)
      real(8)  :: pressure(0:optPropRTMGridS%RTMnlayer)
      real(8)  :: lnpressure(0:optPropRTMGridS%RTMnlayer)           ! natural logarithm of pressure
      real(8)  :: temperature(0:optPropRTMGridS%RTMnlayer)
      real(8)  :: vmrTrace(0:optPropRTMGridS%RTMnlayer,nTrace)
      real(8)  :: lnvmrTrace(0:optPropRTMGridS%RTMnlayer,nTrace)    ! natural logarithm of volume mixing ratio
      real(8)  :: numberDensityAir(0:optPropRTMGridS%RTMnlayer)

      ! calculate values for RTM layer grid

      lnpressureGrid = log(gasPTS%pressure)
      call spline(errS, gasPTS%alt, lnpressureGrid, SDlnpressure , status)
      if (errorCheck(errS)) return
      do ialt = 0, optPropRTMGridS%RTMnlayer
        lnpressure (ialt) = splint(errS, gasPTS%alt, lnpressureGrid, SDlnpressure , &
                                   optPropRTMGridS%RTMaltitude(ialt), status)
      end do
      pressure(:) = exp(lnpressure(:))

      if ( gasPTS%useLinInterp ) then
        do ialt = 0, optPropRTMGridS%RTMnlayer
          temperature(ialt) = splintLin(errS, gasPTS%alt, gasPTS%temperature, optPropRTMGridS%RTMaltitude(ialt), status)
        end do
      else
        call spline(errS, gasPTS%alt, gasPTS%temperature, SDtemperature, status)
        if (errorCheck(errS)) return
        do ialt = 0, optPropRTMGridS%RTMnlayer
          temperature(ialt) = splint(errS, gasPTS%alt, gasPTS%temperature, SDtemperature, &
                                     optPropRTMGridS%RTMaltitude(ialt), status)
        end do
      end if

      numberDensityAir(:) = pressure(:) / temperature(:) / 1.380658d-19 ! in molecules cm-3

      do iTrace = 1, nTrace

        ! allocate arrays
        allocate( lnvmrTraceGridAP(0:traceGasS(iTrace)%nalt) )
        allocate( SDlnvmrTraceAP  (0:traceGasS(iTrace)%nalt) )

        lnvmrTraceGridAP = log(traceGasS(iTrace)%vmrAP)
        if ( .not. traceGasS(iTrace)%useLinInterp ) then
          call spline(errS, traceGasS(iTrace)%alt, lnvmrTraceGridAP, SDlnvmrTraceAP, status)
          if (errorCheck(errS)) return
        end if

        ! calculate the number density on the RTM grid

        do ialt = 0, optPropRTMGridS%RTMnlayer
          if ( traceGasS(iTrace)%useLinInterp ) then
            lnvmrTrace(ialt,iTrace) = splintLin(errS, traceGasS(iTrace)%alt, lnvmrTraceGridAP, &
                                      optPropRTMGridS%RTMaltitude(ialt), status)
          else
            lnvmrTrace(ialt,iTrace) = splint(errS, traceGasS(iTrace)%alt, lnvmrTraceGridAP, SDlnvmrTraceAP, &
                                      optPropRTMGridS%RTMaltitude(ialt), status)
          end if 
          vmrTrace(ialt,iTrace) = exp(lnvmrTrace(ialt,iTrace))
          optPropRTMGridS%ndensGas(ialt,iTrace) = vmrTrace(ialt,iTrace) * 1.0d-6 * numberDensityAir(ialt)
          optPropRTMGridS%RTMtemperature(ialt)  = temperature(ialt)
          optPropRTMGridS%RTMpressure(ialt)     = pressure(ialt)
        end do

        deallocate( lnvmrTraceGridAP )
        deallocate( SDlnvmrTraceAP )

      end do ! iTrace

    end subroutine calcTemperatureNdensRTMgrid


    subroutine calculateAltAveragedXsecHR(errS, nTrace, optPropRTMGridS, wavelHRS, XsecHRRTMaltS, weakAbsS)

      ! Calculate the monochromatic altitude averaged absorption cross section weighted with the 
      ! number density of the trace gas and the altitude resolved air mass factor on the high resolution grid.
      ! the results are stored in the weakAbsS.

      ! It is assumed that the monochromatic altitude resolved air mass factors are known.

      implicit none

      type(errorType),          intent(inout) :: errS
      integer,                  intent(in)    :: nTrace                     ! number of trace gases
      type(optPropRTMGridType), intent(in)    :: optPropRTMGridS         ! contains altitude grid, weights and temperature
      type(wavelHRType),        intent(in)    :: wavelHRS                   ! instrument wavelength grid
      type(XsecType),           intent(in)    :: XsecHRRTMaltS(nTrace)      ! Xsec on HR wavelength grid and RTM alt grid
      type(weakAbsType),        intent(inout) :: weakAbsS                   ! results are stored here

      ! local

      integer :: ialt, iwave, iTrace
      real(8) :: amf, s1, s2

      ! calculate altitude averaged Xsec by integration over altitude weighted with the number density
      ! of the trace gas and the altitude resolved air mass factor.

      weakAbsS%XsecAvAltHR(:,:)    = 0.0d0

      do iTrace = 1, nTrace
        do iwave = 1, wavelHRS%nwavel
          amf = 0.0d0
          do ialt = 0, optPropRTMGridS%RTMnlayer
            s1 = optPropRTMGridS%ndensGas(ialt,itrace) * optPropRTMGridS%RTMweight(ialt) &
               * weakAbsS%amfAltHR(ialt, iwave)
            amf = amf + s1
            s2 = s1 * XsecHRRTMaltS(iTrace)%Xsec(iwave,ialt)
            weakAbsS%XsecAvAltHR(iwave,iTrace)    = weakAbsS%XsecAvAltHR(iwave,iTrace) + s2
          end do ! ialt
          ! normalize
          weakAbsS%XsecAvAltHR(iwave,iTrace)      = weakAbsS%XsecAvAltHR(iwave,iTrace) / amf
        end do ! iwave
      end do !iTrace
      
    end subroutine calculateAltAveragedXsecHR


    subroutine process_AMF(errS, nTrace, optPropRTMGridS, wavelRTMS, wavelHRS, wavelInstrS, weakAbsS)

      ! Calculate the altitude resolved air mass factor on the instrument wavelength grid and on
      ! the high resolution grid using fitting of a low degree polynomial to the values calculated
      ! on the RTM wavelength grid.
      implicit none

      type(errorType), intent(inout) :: errS
      integer,                  intent(in)    :: nTrace              ! number of trace gases
      type(optPropRTMGridType), intent(in)    :: optPropRTMGridS     ! properties on RTM grid
      type(wavelHRType),        intent(in)    :: wavelRTMS           ! RTM wavelength grid
      type(wavelHRType),        intent(in)    :: wavelHRS            ! HR wavelength grid
      type(wavelInstrType),     intent(in)    :: wavelInstrS         ! instrument wavelength grid
      type(weakAbsType),        intent(inout) :: weakAbsS            ! DOAS results

      ! local

      integer :: ialt, iwave, iTrace, iPoly
      real(8) :: column, s1, s2

      ! arrays for fitting polynomial to the altitude resolved air mass factor
      ! that is known at a few wavelengths in order to obtain values for any wavelength
      !real(8) :: sig(wavelInstrS%nwavel)                        ! wavelInstrS%nwavel, wrong dimension, causes crash in svdfit 
      real(8) :: sig(wavelRTMS%nwavel)                           ! corrected on 23 Jan. 2022  P.W.
      real(8) :: polynomialInstr(wavelInstrS%nwavel)
      real(8) :: polynomialHR(wavelHRS%nwavel)
      real(8) :: wavelScaledRTM(wavelRTMS%nwavel)               !  wavelRTMS%nwavel
      real(8) :: wavelScaledInstr(wavelInstrS%nwavel)
      real(8) :: wavelScaledHR(wavelHRS%nwavel)
      real(8) :: wStart, wEnd

      real(8) :: aa(0:weakAbsS%degreePoly )
      real(8) :: ww(0:weakAbsS%degreePoly )
      real(8) :: vv(0:weakAbsS%degreePoly, 0:weakAbsS%degreePoly)

      real(8) :: chisq  ! chi**2 of the fit


      ! calculate scaled wavelengths on instrument grid and on RTM grid
      wStart = wavelInstrS%wavel(1)
      wEnd   = wavelInstrS%wavel( wavelInstrS%nwavel )

      wavelScaledHR(:)    = 2.0d0 * ( wavelHRS   %wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0
      wavelScaledInstr(:) = 2.0d0 * ( wavelInstrS%wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0
      wavelScaledRTM(:)   = 2.0d0 * ( wavelRTMS  %wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0

      ! assume constant errors for the least squares polynomial fit
      sig(:) = 1.0d0

      ! fit the coefficients of the polynomial for all altitudes

      do ialt = 0, weakAbsS%RTMnlayer

        call svdfit(errS, wavelScaledRTM,weakAbsS%amfAltRTM(ialt,:),sig,aa,vv,ww,chisq,fpoly) 
        if (errorCheck(errS)) return

        ! evaluate polynomial on instrument and high resolution wavelength grid

        polynomialInstr(:) = aa(0)
        do iPoly = 1, weakAbsS%degreePoly
          polynomialInstr(:) = polynomialInstr(:) + aa(iPoly) * wavelScaledInstr(:) ** iPoly
        end do ! iPoly
        weakAbsS%amfAltInstr(ialt,:) = polynomialInstr(:)

        polynomialHR(:) = aa(0)
        do iPoly = 1, weakAbsS%degreePoly
          polynomialHR(:) = polynomialHR(:) + aa(iPoly) * wavelScaledHR(:) ** iPoly
        end do ! iPoly
        weakAbsS%amfAltHR(ialt,:) = polynomialHR(:)

      end do  ! ialt

      ! weakAbsS%amfAltInstr and weakAbsS%amfAltHR contain the smooth air mass factors on the
      ! instrument and high resolution wavelength, respectively.

      ! calculate monochromatic air mass factor for the trace gases on instrument grid by 
      ! integration over altitude weighted with the number density of the trace gas

      weakAbsS%amfInstrGrid(:,:)  = 0.0d0
      do iTrace = 1, nTrace
        do iwave = 1, wavelInstrS%nwavel
          column = 0.0d0
          do ialt = 0,  weakAbsS%RTMnlayer
            s1 = optPropRTMGridS%ndensGas(ialt,itrace) * optPropRTMGridS%RTMweight(ialt)
            s2 = s1 * weakAbsS%amfAltInstr(ialt, iwave)
            column = column + s1
            weakAbsS%amfInstrGrid(iwave,iTrace)  = weakAbsS%amfInstrGrid(iwave,iTrace)  + s2
          end do ! ialt
          weakAbsS%amfInstrGrid(iwave,iTrace)  = weakAbsS%amfInstrGrid(iwave,iTrace) / column
        end do ! iwave
      end do !iTrace

      weakAbsS%amfHRGrid(:,:)  = 0.0d0
      do iTrace = 1, nTrace
        do iwave = 1, wavelHRS%nwavel
          column = 0.0d0
          do ialt = 0,  weakAbsS%RTMnlayer
            s1 = optPropRTMGridS%ndensGas(ialt,itrace) * optPropRTMGridS%RTMweight(ialt)
            s2 = s1 * weakAbsS%amfAltHR(ialt, iwave)
            column = column + s1
            weakAbsS%amfHRGrid(iwave,iTrace)  = weakAbsS%amfHRGrid(iwave,iTrace)  + s2
          end do ! ialt
          weakAbsS%amfHRGrid(iwave,iTrace)  = weakAbsS%amfHRGrid(iwave,iTrace) / column
        end do ! iwave
      end do !iTrace

    end subroutine process_AMF


    subroutine setWavelRTM(errS, nTrace, wavelInstrS, weakAbsRetrS, wavelRTMS)

     ! The following steps are taken 
     ! - the total absorption is calculated on the instrument wavelength grid providing sum(k)
     ! - a low degree polynomial is fitted to sum(k) and subsequently subtracted, providing dsum(k)
     ! - wavelengths corresponding to zero values of dsum(k) are calculated and stored in wavelRTMS

      implicit none

      type(errorType), intent(inout) :: errS
      integer,               intent(in)    :: nTrace                         ! number of trace gases
      type(wavelInstrType),  intent(in)    :: wavelInstrS                    ! instrument wavelength grid
      type(weakAbsType),     intent(inout) :: weakAbsRetrS                   ! DOAS results
      type(wavelHRType),     intent(inout) :: wavelRTMS                      ! RTM wavelength grid

      ! local
      integer :: iTrace, iwave, iPoly
      real(8) :: step, wStart, wEnd, wMiddle, wInterval
      integer :: indLoc(1)

      real(8) :: totalSlantAbs    (wavelInstrS%nwavel)
      real(8) :: totalSlantDiffAbs(wavelInstrS%nwavel)

      ! arrays for fitting polynomial to total slant optical thickness
      real(8) :: sig(wavelInstrS%nwavel)
      real(8) :: polynomial(wavelInstrS%nwavel)
      real(8) :: wavelScaled(wavelInstrS%nwavel)

      real(8) :: a(0:weakAbsRetrS%degreePoly )
      real(8) :: w(0:weakAbsRetrS%degreePoly )
      real(8) :: v(0:weakAbsRetrS%degreePoly, 0:weakAbsRetrS%degreePoly)

      real(8) :: chisq  ! chi**2 of the fit

      ! parameters for finding the zeros of the total differential absorption optical thickness
      integer, parameter :: maxSignChanges = 300
      integer            :: numSignChanges
      real(8)            :: wavelSignChanges( maxSignChanges )


      ! calculate differential slant absorption optical thickness on the instrument wavelength grid

      totalSlantAbs(:) = 0.0d0

      do iTrace = 1, nTrace

          do iwave = 1, wavelInstrS%nwavel
            totalSlantAbs(iwave) = totalSlantAbs(iwave) + weakAbsRetrS%amfInstrGrid(iwave,iTrace) &
                                                        * weakAbsRetrS%column(iTrace)             &
                                                        * weakAbsRetrS%XsecEffDiffInstr(iwave,iTrace)
          end do ! iwave

      end do ! iTrace

      ! fit low degree polynomial to the total slant absorption

      wStart = wavelInstrS%wavel(1)
      wEnd   = wavelInstrS%wavel( wavelInstrS%nwavel )
      wavelScaled(:) = 2.0d0 * ( wavelInstrS%wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0
     
      ! assume constant errors for the least squares polynomial fit
      sig(:) = 1.0d0

      ! perform polynomial fit

      call svdfit(errS, wavelScaled, totalSlantAbs, sig, a, v, w, chisq, fpoly)
      if (errorCheck(errS)) return

      ! calculate total differential absorption optical thickness

      polynomial(:) = a(0)
      do iPoly = 1, weakAbsRetrS%degreePoly
        polynomial(:) = polynomial(:) + a(iPoly) * wavelScaled(:) ** iPoly
      end do ! iPoly
      totalSlantDiffAbs(:) = totalSlantAbs(:) - polynomial(:)

      ! find the wavelengths where the differential slant absorption changes sign
      wavelSignChanges = 0.0d0
      numSignChanges   = 1
      do iwave = 2, wavelInstrS%nwavel
        if ( totalSlantDiffAbs(iwave-1) * totalSlantDiffAbs(iwave) < 0.0d0 ) then
          ! use inverse linear interpolation to find the wavelength where the zero occurs
          wavelSignChanges(numSignChanges) = wavelInstrS%wavel(iwave-1)       & 
                 + ( wavelInstrS%wavel(iwave-1) - wavelInstrS%wavel(iwave) )  &
                 * totalSlantDiffAbs(iwave-1) / ( totalSlantDiffAbs(iwave) - totalSlantDiffAbs(iwave-1))

          numSignChanges = numSignChanges + 1
          if ( numSignChanges > maxSignChanges ) then
            call logDebug('ERROR: doasModule:setWavelRTM: maxSignChanges is too small')
            call logDebug('increase the dimension of the array maxSignChanges')
            call logDebug('in subroutine setWavelRTM in doasModule')
            call mystop(errS, 'stopped because array is too small')
            if (errorCheck(errS)) return
          end if

        end if
      end do ! iwave

      if ( numSignChanges <= weakAbsRetrS%degreePoly ) then
        call logDebug('ERROR in DOASMODULE subroutine setWavelRTM')
        call logDebugI('not enough sign changes for the polynomial with degree: ', weakAbsRetrS%degreePoly)
        call logDebugI('number of sign changes found = ', numSignChanges)
        call mystop(errS, 'stopped because there are not enough sign changes')
        if (errorCheck(errS)) return
      end if

      ! the wavelengths where the total differential absorption is zero are now known, but we do
      ! not have to perform radiative transfer calculations for all of them. THe minimum number
      ! required is the degree of the polynomial + 1. Now we select the appropriate wavelengths
    
      ! use select case for weakAbsRetrS%degreePoly
      ! if degreePoly = 0 select wavelength closest to the middle of the fit interval
      ! if degreePoly = 1 select smallest and largest wavelengths
      ! if degreePoly = 2 select smallest and largest wavelengths, and the wavelength closest to the middle
      ! if degreePoly > 2 divide the fit interval into degreePoly subintervals of equal length and find
      ! the wavelengths closest to the end points of the intervals
      ! check that all wavelengths are different

      ! fill wavelengths

      select case (weakAbsRetrS%degreePoly)

        case ( 0 )  ! value close to the middle of the interval

          wMiddle = 0.5d0* ( wEnd - wStart )
          indLoc = minLoc(abs( wavelSignChanges - wMiddle ) )
          wavelRTMS%wavel(1) = wavelSignChanges( indLoc(1) )  

        case ( 1:100 )

          step =  ( wEnd - wStart ) / weakAbsRetrS%degreePoly
          do iPoly = 0, weakAbsRetrS%degreePoly
            wInterval = wStart + iPoly * step

            indLoc = minLoc(abs( wavelSignChanges - wInterval ) )
            wavelRTMS%wavel(iPoly + 1) =  wavelSignChanges( indLoc(1) )
          end do

        case default

          call logDebugI('incorrect value for weakAbsRetrS%degreePoly : ', weakAbsRetrS%degreePoly)
          call logDebug('in doasModule : subroutine setWavelRTM ')
          call mystop(errS, 'stopped due to incorrect value for weakAbsRetrS%degreePoly')
          if (errorCheck(errS)) return

      end select

      ! verufy that all wavelength are different

      if ( .not. verfy_elem_differ(wavelRTMS%wavel, 0.1d0) ) then
        
        call logDebug('ERROR in doasModule : subroutine setWavelRTM ')
        call logDebug('selected wavelengths for RTM calculations are (nearly) the same  ')
        write(errS%temp,'(A, 10F12.5)') 'wavelengths : ', wavelRTMS%wavel
        call errorAddLine(errS, errS%temp)
        call mystop(errS, 'stopped due to incorrect value for weakAbsRetrS%degreePoly')
        if (errorCheck(errS)) return

      end if

      write(intermediateFileUnit,'(A,40F17.10)') 'wavelength = ', wavelRTMS%wavel

    end subroutine setWavelRTM


    subroutine setXsecRTM(errS, nTrace, wavelInstrS, weakAbsRetrS, wavelRTMS, XsecRTMS)

     ! The radiative transfer calculations for the few wavelength calculated in setWavelRTM
     ! are performed using the smooth part of the altitude averaged absorption cross section. 
     ! Here these absorption cross sections are calculated using interpolation on the
     ! values given on the instrument wavelength grid and stored in XsecRTMS. Note that
     ! the altitude dependence of the absorption cross section has vanished.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,               intent(in)    :: nTrace                         ! number of trace gases
      type(wavelInstrType),  intent(in)    :: wavelInstrS                    ! instrument wavelength grid
      type(weakAbsType),     intent(in)    :: weakAbsRetrS                   ! DOAS results
      type(wavelHRType),     intent(in)    :: wavelRTMS                      ! RTM wavelength grid
      type(XsecType),        intent(inout) :: XsecRTMS(nTrace)               ! DOAS absorption cross sections

      ! local
      integer :: iTrace, iwave
      integer :: ialt, status
      real(8) :: SDabs(wavelInstrS%nwavel)

      do iTrace = 1, nTrace
        call spline(errS, wavelInstrS%wavel, weakAbsRetrS%XsecEffSmoothInstr(:,iTrace), SDabs, status)
        if (errorCheck(errS)) return
        do iwave = 1, weakAbsRetrS%nwavelRTM
          XsecRTMS(iTrace)%Xsec(iwave,0) = splint(errS, wavelInstrS%wavel, weakAbsRetrS%XsecEffSmoothInstr(:,iTrace), &
                                                  SDabs, wavelRTMS%wavel(iwave), status)
        end do ! iwave
        do ialt = 1, XsecRTMS(iTrace)%nalt
          do iwave = 1, weakAbsRetrS%nwavelRTM
            XsecRTMS(iTrace)%Xsec(iwave,ialt) = XsecRTMS(iTrace)%Xsec(iwave,0)
          end do ! iwave
        end do ! ialt
      end do ! iTrace

    end subroutine setXsecRTM


    subroutine fillCodesFitParamDOAS(errS, numSpectrBands, nTrace, traceGasS, weakAbsRetrS, RRS_RingS, gasPTS, retrS)


      ! fillCodesFitParamDOAS is called in the beginning of the main program DISAMAR (errS, around line 234)
      ! Fill the logical array fitAtmStateVector with values for the parameters that are to be fitted
      ! using DOAS
      ! number of values                                code
      ! nTrace                                          columnTrace : fit total column of trace gases
      ! nTrace                                          tempTrace   : temperature of trace gas
      ! (weakAbsRetrS%degreePoly + 1) * numSpectrBands  lnpolyCoef  : fit polynomial coefficients for each spactral band

      ! The associated arrays in the retrieval structure that are used for DOAS
      !   codeFitParameters    contains the strings used to identify fit parameters
      !   codeSpecBand         contains the number of the wavelength band (used for surface albedo and stray light)
      !   codeTraceGas         contains the trace gas number
      !   codelnPolyCoef       contains the index of the polynomial coefficent (0, 1, 2, ..)
      ! these arrays can be used to identify the state vector elements, e.g. if for state vector element
      !   x(index) we can print codeFitParameters(index) and get nodeTrace
      !                         codeTraceGas(index)      and get the trace gas number
      !                         codelnPolyCoef(index)    and get the coefficient number

      ! After the call to fillCodesFitParamDOAS the number of state vector elements is known.
      ! Therefore, the length of the arrays fitAtmStateVector and codeFitParameters is
      ! the maximum number of parameters that can be fitted, and not the number of state
      ! vector elements.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands               ! number of spectral bands
      integer,                       intent(in)    :: nTrace                       ! number of trace gases
      type(traceGasType),            intent(in)    :: traceGasS(nTrace)            ! properties of the trace gases
      type(weakAbsType),             intent(in)    :: weakAbsRetrS(numSpectrBands) ! DOAS results
      type(RRS_RingType),            intent(in)    :: RRS_RingS(numSpectrBands)    ! Ring spectra
      type(gasPTType),               intent(in)    :: gasPTS                       ! pressure and temperature
      type(retrType),                intent(inout) :: retrS                        ! retrieval parameters

      ! local
      integer :: counter, iband, index, istate, iTrace, ialt
      integer :: startValue, endValue

      logical, parameter :: verbose = .false.

      ! reset maxfitparameters for DOAS to
      ! 2* nTrace to be able to fit the colums of the trace gases and the temperature,
      ! the polynomial coefficients for the different spectral windows, and Ring spectra for each band
      retrS%maxfitparameters = nTrace + 1 + sum(weakAbsRetrS(:)%degreePoly + 1) + numSpectrBands

      ! claim memory for the arrays
      allocate( retrS%codeFitParameters(retrS%maxfitparameters) )
      allocate( retrS%codeSpecBand(retrS%maxfitparameters) )
      allocate( retrS%codeTraceGas(retrS%maxfitparameters) )
      allocate( retrS%codeAltitude(retrS%maxfitparameters) )
      allocate( retrS%codeIndexSurfAlb(retrS%maxfitparameters) )
      allocate( retrS%codeIndexSurfEmission(retrS%maxfitparameters) )
      allocate( retrS%codeIndexStraylight(retrS%maxfitparameters) )
      allocate( retrS%codelnPolyCoef(retrS%maxfitparameters) )

      ! initialize
      counter              = 0
      retrS%codeSpecBand   = 0
      retrS%codeTraceGas   = 0
      retrS%codelnPolyCoef = 0

      ! initialization
      do iTrace = 1, nTrace
        if ( traceGasS(iTrace)%fitColumnTrace ) then
          counter = counter + 1
          retrS%codeFitParameters(counter) = 'columnTrace'
          retrS%codeTraceGas     (counter) = iTrace  
          retrS%codeSpecBand     (counter) = 1        ! the column should not change with spectral band
        end if
      end do

      do iband = 1, numSpectrBands
        do index = 0,  weakAbsRetrS(iband)%degreePoly
          counter = counter + 1
          retrS%codelnPolyCoef   (counter) = index
          retrS%codeFitParameters(counter) = 'lnpolyCoef'
          retrS%codeSpecBand     (counter) = iband
        end do
      end do

      if ( gasPTS%fitTemperatureProfile ) then
        startValue = 1 + counter
        endValue   = startValue + gasPTS%npressureNodes
        counter    = endValue
        retrS%codeFitParameters(startValue:endValue) = 'nodeTemp'
        do ialt = 0, gasPTS%npressureNodes
          retrS%codeAltitude(startValue+ialt)        = ialt
        end do
      end if

      if ( gasPTS%fitTemperatureOffset ) then
        counter = counter + 1
        retrS%codeFitParameters(counter) = 'offsetTemp'     
      end if

      do iband = 1, numSpectrBands
        if ( RRS_RingS(iband)%fitDiffRingSpec ) then
          counter = counter + 1
          retrS%codeFitParameters   (counter) = 'diffRingCoef'
          retrS%codeSpecBand        (counter) = iband
        end if
        if ( RRS_RingS(iband)%fitRingSpec ) then
          counter = counter + 1
          retrS%codeFitParameters   (counter) = 'RingCoef'
          retrS%codeSpecBand        (counter) = iband
        end if
      end do ! iband

      retrS%nstate = counter

      if ( verbose ) then
        write(intermediateFileUnit,'(A)') 'codeFitParameters  #TraceGas  #lnpolyCoef   spectralBand '
        do istate = 1, retrS%nstate
          write(intermediateFileUnit,'(A15,3I10)') retrS%codeFitParameters(istate),       &
                 retrS%codeTraceGas(istate), retrS%codelnPolyCoef(istate), retrS%codeSpecBand(istate)
        end do
      end if

    end subroutine fillCodesFitParamDOAS


    subroutine updateFitParametersDOAS(errS, numSpectrBands, nTrace, traceGasS, retrS, weakAbsRetrS, &
                                       RRS_RingS, gasPTS, surfaceS, cloudAerosolRTMgridS, LambCloudS)

      ! Write the newly calculated values of the state vector into the structures weakAbsRetrS and gasPTS

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands               ! number of spectral bands
      integer,                       intent(in)    :: nTrace                       ! number of trace gases
      type(traceGasType),            intent(inout) :: traceGasS(nTrace)            ! properties of the trace gases
      type(retrType),                intent(inout) :: retrS                        ! retrieval parameters
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands) ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)    ! Ring spectra
      type(gasPTType),               intent(inout) :: gasPTS                       ! pressure and temperature
      type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)     ! surface properties
      type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS         ! cloud-aerosol properties
      type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)   ! cloud properties

      ! local
      logical :: altitude_update_required
      integer :: istate, iband, index, iTrace, ialt

      real(8), parameter :: DUToCm2 = 2.68668d16

      ! initialize
      altitude_update_required = .false.

      ! loop over al the state vector elements and update the relevant values
      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case( 'columnTrace' )

            iTrace = retrS%codeTraceGas(istate)
            traceGasS(iTrace)%column    = abs( retrS%x(istate) ) * DUToCm2   ! column must be positive
            do iband = 1, numSpectrBands
              weakAbsRetrS(iband)%column(iTrace) = traceGasS(iTrace)%column
            end do
            ! update the profile by scaling the profile
            traceGasS(iTrace)%numDens = traceGasS(iTrace)%numDensAP &
                * traceGasS(iTrace)%column / traceGasS(iTrace)%columnAP
            traceGasS(iTrace)%vmr     = traceGasS(iTrace)%vmrAP     &
                * traceGasS(iTrace)%column / traceGasS(iTrace)%columnAP

          case('nodeTemp')

            ialt   = retrS%codeAltitude(istate)
            gasPTS%temperatureNodes(ialt) = retrS%x(istate)
            if( gasPTS%temperatureNodes(ialt) < 150.0d0 ) gasPTS%temperatureNodes(ialt) = 150.0d0
            if( gasPTS%temperatureNodes(ialt) > 400.0d0 ) gasPTS%temperatureNodes(ialt) = 400.0d0
            altitude_update_required = .true.

          case('offsetTemp')

            gasPTS%temperatureOffset = retrS%x(istate)
            if( gasPTS%temperatureOffset >  50.0d0 ) gasPTS%temperatureOffset =  50.0d0
            if( gasPTS%temperatureOffset < -50.0d0 ) gasPTS%temperatureOffset = -50.0d0
            ! change the a-priori temperatures with the offset
            gasPTS%temperature(:) = gasPTS%temperatureAP(:) + gasPTS%temperatureOffset
            altitude_update_required = .true.

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)
            RRS_RingS(iband)%ringCoeff  = retrS%x(istate)

          case('RingCoef')

            iband = retrS%codeSpecBand(istate)
            RRS_RingS(iband)%ringCoeff  = retrS%x(istate)

          case('lnpolyCoef')

            iband = retrS%codeSpecBand(istate)
            index = retrS%codelnPolyCoef(istate)
            weakAbsRetrS(iband)%lnpolyCoef(index) = retrS%x(istate)

          case default
            call logDebug('in subroutine updateFitParametersDOAS in doasModule')
            call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
            if (errorCheck(errS)) return
        end select

      end do ! istate

      if ( altitude_update_required )  then
        ! update the altitudes and the temperature in the traceGasS structure
        call update_altitudes_PTz_grid(errS, surfaceS(1), gasPTS)
        if (errorCheck(errS)) return
        call update_T_z_other_grids(errS, numSpectrBands, nTrace, surfaceS, gasPTS, traceGasS, &
                                    cloudAerosolRTMgridS, LambCloudS)
        if (errorCheck(errS)) return
        call update_ndens_vmr(errS, nTrace, gasPTS, traceGasS )
        if (errorCheck(errS)) return
      end if

    end subroutine updateFitParametersDOAS


    subroutine updateCovFitParametersDOAS(errS, numSpectrBands, nTrace, traceGasS, retrS, diagnosticS, weakAbsRetrS, &
                                          RRS_RingS, gasPTS)

      ! The a-posteriori errors are calculated after the solution has converged, or at least the
      ! iteration loop has ended. When this diagnostic information is available, one can update
      ! the errors in traceGasS, surfaceS, and cloudAerosolRTMgridS.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands               ! number of spectral bands
      integer,                       intent(in)    :: nTrace                       ! number of trace gases
      type(traceGasType),            intent(inout) :: traceGasS(nTrace)            ! properties of the trace gases
      type(retrType),                intent(in)    :: retrS                        ! retrieval parameters
      type(diagnosticType),          intent(in)    :: diagnosticS                  ! diagnostic parameters
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands) ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)    ! Ring spectra
      type(gasPTType),               intent(inout) :: gasPTS                       ! pressure and temperature

      ! local
      integer :: istate, jstate, iindex, jindex, iband, jband, iTrace, jTrace, ialt

      real(8), parameter :: DUToCm2 = 2.68668d16

      ! loop over al the state vector elements and update the relevant values

      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case('columnTrace')

            iband  = retrS%codeSpecBand(istate)
            iTrace = retrS%codeTraceGas(istate)
            do jstate= 1, retrS%nstate
              select case (retrS%codeFitParameters(jstate))
                case('columnTrace')
                  jband  = retrS%codeSpecBand(jstate)
                  jTrace = retrS%codeTraceGas(jstate)
                  if ( (jTrace == iTrace) .and. (iband == jband) ) then
                     weakAbsRetrS(iband)%covColumn(iTrace) = diagnosticS%S_lnvmr  (istate,jstate) * DUToCm2**2
                     traceGasS(iTrace)%covColumn = diagnosticS%S_lnvmr(istate,jstate) * DUToCm2**2
                  end if
              end select
            end do 

          case('nodeTemp')

            ialt   = retrS%codeAltitude(istate)
            gasPTS%covTempNodes(ialt,ialt) = diagnosticS%S_lnvmr(istate,istate)

          case('offsetTemp')

            gasPTS%varianceTempOffset = diagnosticS%S_lnvmr(istate,istate)

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)
            RRS_RingS(iband)%varRingCoeff  = diagnosticS%S_lnvmr(istate,istate)

          case('RingCoef')

            iband = retrS%codeSpecBand(istate)
            RRS_RingS(iband)%varRingCoeff  = diagnosticS%S_lnvmr(istate,istate)

          case('lnpolyCoef')

            iband  = retrS%codeSpecBand(istate)
            iindex = retrS%codelnPolyCoef(istate)
            do jstate= 1, retrS%nstate
              jband  = retrS%codeSpecBand(jstate)
              jindex = retrS%codelnPolyCoef(jstate)
              if ( (iband == jband) .and. (iindex == jindex ) ) then
                select case (retrS%codeFitParameters(jstate))
                  case('lnpolyCoef')
                    weakAbsRetrS(iband)%covlnPolyCoef(iindex) = diagnosticS%S_lnvmr(istate,jstate)
                end select
              end if
            end do 

          case default

            call logDebug('in subroutine updateCovFitParametersDOAS in doasModule')
            call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
            if (errorCheck(errS)) return

        end select

      end do ! istate

    end subroutine updateCovFitParametersDOAS


    subroutine fillAPrioriDOAS(errS, numSpectrBands, nTrace, traceGasSimS, traceGasRetrS, &
                               weakAbsRetrS, RRS_RingS, gasPTSimS, gasPTRetrS, retrS)

      ! fill the a-priori state vector and the a-priori covariance matrix
      ! fitting of the trace gas is done for the logarithm of the volume mixing ratio,
      ! but a-priori covariance matrices for the volume mixing ratio and the number density
      ! are also given.
                          
      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands                  ! number of spectral bands
      integer,                       intent(in)    :: nTrace                          ! number of trace gases
      type(traceGasType),            intent(in)    :: traceGasSimS(nTrace)            ! trace gas properties for simulation
      type(traceGasType),            intent(in)    :: traceGasRetrS(nTrace)           ! trace gas properties for retrieval
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands)    ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)       ! Ring spectra and Ring parameters
      type(gasPTType),               intent(in)    :: gasPTSimS                       ! pressure and temperature
      type(gasPTType),               intent(inout) :: gasPTRetrS                      ! pressure and temperature
      type(retrType),                intent(inout) :: retrS                           ! retrieval parameters

      ! local
      integer :: istate, jstate
      integer :: iband, iTrace, iPoly, ialt

      real(8), parameter :: DUToCm2 = 2.68668d16

      ! control output
      logical, parameter :: verbose = .false.

      retrS%Sa_lnvmr = 0.0d0
      retrS%Sa_vmr   = 0.0d0
      retrS%Sa_ndens = 0.0d0

      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case('columnTrace')

            iTrace = retrS%codeTraceGas(iState)
            iband  = retrS%codeSpecBand(istate)
            retrS%xa(istate)     = traceGasRetrS(iTrace)%columnAP / DUToCm2
            retrS%x(istate)      = traceGasRetrS(iTrace)%columnAP / DUToCm2
            retrS%x_true(istate) = traceGasSimS(iTrace)%column    / DUToCm2

            retrS%Sa_lnvmr(istate, istate) = weakAbsRetrS(iband)%covColumnAP(iTrace) / DUToCm2 / DUToCm2 
            retrS%Sa_vmr  (istate, istate) = weakAbsRetrS(iband)%covColumnAP(iTrace) / DUToCm2 / DUToCm2
            retrS%Sa_ndens(istate, istate) = weakAbsRetrS(iband)%covColumnAP(iTrace) / DUToCm2 / DUToCm2

          case('nodeTemp')

            ! no covariance introduced yet for the temperatures
            ialt   = retrS%codeAltitude(istate)
            retrS%xa(istate)               = gasPTRetrS%temperatureNodesAP(ialt)
            retrS%x_true(istate)           = gasPTSimS%temperature_trueNodes(ialt)
            retrS%Sa_lnvmr(istate, istate) = gasPTRetrS%covTempNodesAP(ialt,ialt)
            retrS%Sa_vmr  (istate, istate) = gasPTRetrS%covTempNodesAP(ialt,ialt)
            retrS%Sa_ndens(istate, istate) = gasPTRetrS%covTempNodesAP(ialt,ialt)

          case('offsetTemp')

            retrS%xa(istate)               = gasPTRetrS%temperatureOffsetAP
            retrS%x_true(istate)           = gasPTSimS%temperatureOffset
            retrS%Sa_lnvmr(istate, istate) = gasPTRetrS%varianceTempOffsetAP
            retrS%Sa_vmr  (istate, istate) = gasPTRetrS%varianceTempOffsetAP
            retrS%Sa_ndens(istate, istate) = gasPTRetrS%varianceTempOffsetAP

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)
            retrS%xa(istate)               = RRS_RingS(iband)%ringCoeffAP
            retrS%x_true(istate)           = 0.0d0                           ! in simulation no Ring spectrum
            retrS%Sa_lnvmr(istate, istate) = RRS_RingS(iband)%varRingCoeffAP
            retrS%Sa_vmr  (istate, istate) = RRS_RingS(iband)%varRingCoeffAP
            retrS%Sa_ndens(istate, istate) = RRS_RingS(iband)%varRingCoeffAP

          case('RingCoef')

            retrS%xa(istate)               = RRS_RingS(iband)%ringCoeffAP
            retrS%x_true(istate)           = 0.0d0                           ! in simulation no Ring spectrum
            retrS%Sa_lnvmr(istate, istate) = RRS_RingS(iband)%varRingCoeffAP
            retrS%Sa_vmr  (istate, istate) = RRS_RingS(iband)%varRingCoeffAP
            retrS%Sa_ndens(istate, istate) = RRS_RingS(iband)%varRingCoeffAP

          case('lnpolyCoef')

            iband = retrS%codeSpecBand(istate)
            iPoly = retrS%codelnPolyCoef(istate)
            retrS%xa(istate) = weakAbsRetrS(iband)%lnPolyCoefAP(iPoly)
            retrS%x(istate)  = weakAbsRetrS(iband)%lnPolyCoefAP(iPoly)
            retrS%x_true(istate) = 0.0d0   ! we do not know the true coefficients of the polynomial calculate them?
            retrS%Sa_lnvmr(istate, istate) = weakAbsRetrS(iband)%covlnPolyCoefAP(iPoly)
            retrS%Sa_vmr  (istate, istate) = weakAbsRetrS(iband)%covlnPolyCoefAP(iPoly)
            retrS%Sa_ndens(istate, istate) = weakAbsRetrS(iband)%covlnPolyCoefAP(iPoly)

          case default

            call logDebug('in subroutine fillAPrioriDOAS in doasModule')
            call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
            if (errorCheck(errS)) return

        end select

      end do ! istate

      ! write to intermediate file if verbose is .true.
      if ( verbose ) then
        write(intermediateFileUnit,*) ' code   xa  and  Sa_lnvmr from fillAPriori'
        do istate = 1, retrS%nstate
          write(intermediateFileUnit,'(A15,101F12.6)') retrS%codeFitParameters(istate), retrS%xa(istate), &
                                      (retrS%Sa_lnvmr(istate, jstate), jstate = 1, retrS%nstate)
        end do
      end if

    end subroutine fillAPrioriDOAS


    subroutine calcReflAndAMFRTM(errS, nTrace, wavelRTMS, XsecRTMS, geometryS, RRS_RingS,  &
                                 controlS, cloudAerosolRTMgridS, gasPTS, traceGasS,  &
                                 surfaceS, LambCloudS, mieAerS, mieCldS,             &
                                 cldAerFractionS, optPropRTMGridS, refl, amf,        &
                                 radianceCldFraction)

      ! calcReflAndAMFRTM uses the specification of the atmosphere given by
      ! gasPTS, traceGasS, surfaceS, cloudAerosolRTMgridS and the specification of the 
      ! radiative transfer calculations specified in controlS to calculate 
      ! the reflectance, R, and d2R/dkabs d2Rdkabsdz for one spectral band for a few
      ! stored in the array wavel.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: nTrace                    ! number of trace gases
      type(wavelHRType),             intent(in)    :: wavelRTMS                 ! wavelength grid for RTM calculations
      type(XsecType),                intent(in)    :: XsecRTMS(nTrace)          ! absorption cross sections
      type(geometryType),            intent(in)    :: geometryS                 ! geometrical information
      type(RRS_RingType),            intent(in)    :: RRS_RingS                 ! Raman / Ring info
      type(controlType),             intent(inout) :: controlS                  ! control parameters
      type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS      ! cloud-aerosol properties
      type(gasPTType),               intent(inout) :: gasPTS                    ! atmospheric pressure and temperature
      type(traceGasType),            intent(inout) :: traceGasS(nTrace)         ! atmospheric trace gas properties
      type(LambertianType),          intent(inout) :: surfaceS                  ! surface properties for a spectral band
      type(LambertianType),          intent(inout) :: LambCloudS                ! Lambertian cloud properties
      type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)  ! specification expansion coefficients
      type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)  ! specification expansion coefficients
      type(cldAerFractionType),      intent(in)    :: cldAerFractionS           ! cloud / aerosol fraction structure
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS           ! optical properties on RTM grid
      real(8),                       intent(out)   :: refl(wavelRTMS%nwavel)  ! sun-normalized radiance
      real(8),                       intent(out)   :: amf(0:optPropRTMGridS%RTMnlayer, wavelRTMS%nwavel) ! AMF
      real(8), optional,             intent(out)   :: radianceCldFraction

      ! local

      logical  :: cloudyPart
      integer  :: iwave, statusRTM, ilevel
      real(8)  :: babs, bsca           ! absorption and scattering optical thickness
      real(8)  :: depolarization       ! depolarization factor
      real(8), parameter  :: PI = 3.141592653589793d0

      ! Reflectance and basic derivatives returned by layerBasedOrdersScattering
      ! note that the combination LambScaCloud has a Lambertian and scattering cloud
      ! as the Lambertian surface is at the top of the fit interval the weighting functions
      ! for cloud and aerosol particles are not needed in case. Therefore, only the
      ! weighting function for the cloud albedo, and scattering and absorption by gas are
      ! required for this combination.

      real(8)    :: refl_Clear(controlS%dimSV)
      real(8)    :: refl_Cloud(controlS%dimSV)

      real(8)    :: contribRefl_Clear(controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)
      real(8)    :: contribRefl_Cloud(controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)

      type(UDType)  :: UD_Clear(0:optPropRTMGridS%RTMnlayer)
      type(UDType)  :: UD_Cloud(0:optPropRTMGridS%RTMnlayer)

      real(8)    :: wfInterfkscaGas_Clear(0:optPropRTMGridS%RTMnlayer)
      real(8)    :: wfInterfkscaGas_Cloud(0:optPropRTMGridS%RTMnlayer)

      real(8)    :: wfInterfkscaAerAbove_Clear(0:optPropRTMGridS%RTMnlayer)
      real(8)    :: wfInterfkscaAerBelow_Clear(0:optPropRTMGridS%RTMnlayer)
      real(8)    :: wfInterfkscaAerAbove_Cloud(0:optPropRTMGridS%RTMnlayer)
      real(8)    :: wfInterfkscaAerBelow_Cloud(0:optPropRTMGridS%RTMnlayer)

      real(8)    :: wfInterfkscaCldAbove_Clear(0:optPropRTMGridS%RTMnlayer)
      real(8)    :: wfInterfkscaCldBelow_Clear(0:optPropRTMGridS%RTMnlayer)
      real(8)    :: wfInterfkscaCldAbove_Cloud(0:optPropRTMGridS%RTMnlayer)
      real(8)    :: wfInterfkscaCldBelow_Cloud(0:optPropRTMGridS%RTMnlayer)

      real(8)    :: wfInterfkabs_Clear       (0:optPropRTMGridS%RTMnlayer)
      real(8)    :: wfInterfkabs_Cloud    (0:optPropRTMGridS%RTMnlayer)

      real(8)    :: wfSurfaceAlbedo_Clear
      real(8)    :: wfSurfaceAlbedo_Cloud

      real(8)    :: wfSurfaceEmission_Clear
      real(8)    :: wfSurfaceEmission_Cloud

      real(8)    :: wfCloudAlbedo

      ! weighting functions for the cloud fraction
      real(8)    :: wfCloudFraction
      real(8)    :: cf              ! cloud fraction
  
      real(8)    :: surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave

      logical, parameter :: verbose = .false.


      call allocateUD(errS, geometryS%nmutot, controlS%dimSV, optPropRTMGridS%RTMnlayer, UD_Clear, UD_Cloud)
      if (errorCheck(errS)) return

      ! loop over wavelengths

      do iwave = 1, wavelRTMS%nwavel

        ! initialize
        refl_Clear                  = 0.0d0
        refl_Cloud                  = 0.0d0

        contribRefl_Clear           = 0.0d0
        contribRefl_Cloud           = 0.0d0

        wfInterfkabs_Clear          = 0.0d0
        wfInterfkabs_Cloud          = 0.0d0

        wfInterfkscaGas_Clear       = 0.0d0
        wfInterfkscaGas_Cloud       = 0.0d0

        wfInterfkscaAerAbove_Clear  = 0.0d0
        wfInterfkscaAerBelow_Clear  = 0.0d0
        wfInterfkscaAerAbove_Cloud  = 0.0d0
        wfInterfkscaAerBelow_Cloud  = 0.0d0

        wfInterfkscaCldAbove_Clear  = 0.0d0
        wfInterfkscaCldBelow_Clear  = 0.0d0
        wfInterfkscaCldAbove_Cloud  = 0.0d0
        wfInterfkscaCldBelow_Cloud  = 0.0d0

        wfSurfaceAlbedo_Clear       = 0.0d0
        wfSurfaceAlbedo_Cloud       = 0.0d0

        wfSurfaceEmission_Clear     = 0.0d0
        wfSurfaceEmission_Cloud     = 0.0d0

        wfCloudAlbedo               = 0.0d0

        wfCloudFraction             = 0.0d0

        ! set cloud fraction
        if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
          cf = cloudAerosolRTMgridS%cldAerFractionAllBands
        else
          cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelRTMS%wavel(iwave))
        end if

        ! get optical properties of the atmosphere at this wavelength
        ! and store them in OptPropRTMGridS
        ! in addition, calculate the surface albedo at this wavelength: surfAlb_iwave

        if ( cf < 1.0d0 ) then  ! only if the cloud fraction is less than 1.0

          cloudyPart = .false.
          call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelRTMS, XsecRTMS, RRS_RingS,   &
                              controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,  &
                              mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,              &
                              surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,           &
                              depolarization)
          if (errorCheck(errS)) return

          call layerBasedOrdersScattering(errS,        &
            optPropRTMGridS,                     & 
            controlS,                            & 
            geometryS,                           &
            surfAlb_iwave,                       & 
            0,                                   &   ! index for Lambertian surface - here ground surface with index 0
            babs,                                &   ! output
            bsca,                                &   ! output
            refl_Clear,                          &   ! output 
            contribRefl_Clear,                   &   ! output
            UD_Clear,                            &   ! output
            wfInterfkscaGas_Clear,               &   ! output 
            wfInterfkscaAerAbove_Clear,          &   ! output 
            wfInterfkscaAerBelow_Clear,          &   ! output 
            wfInterfkscaCldAbove_Clear,          &   ! output 
            wfInterfkscaCldBelow_Clear,          &   ! output 
            wfInterfkabs_Clear,                  &   ! output
            wfSurfaceAlbedo_Clear,               &   ! output 
            wfSurfaceEmission_Clear,             &   ! output 
            statusRTM)
          if (errorCheck(errS)) return

        else

          refl_Clear                 = 0.0d0
          contribRefl_Clear          = 0.0d0
          wfInterfkscaGas_Clear      = 0.0d0
          wfInterfkscaAerAbove_Clear = 0.0d0
          wfInterfkscaAerBelow_Clear = 0.0d0
          wfInterfkscaCldAbove_Clear = 0.0d0
          wfInterfkscaCldBelow_Clear = 0.0d0
          wfInterfkabs_Clear         = 0.0d0
          wfSurfaceAlbedo_Clear      = 0.0d0 
          wfSurfaceEmission_Clear    = 0.0d0 

        end if  !  cf < 1.0d0

        if ( cloudAerosolRTMgridS%useLambertianCloud ) then
          ! the optical properties of the atmosphere are the same as for the cloud free part
          ! calculate them if the cloud fraction >= 1.0 as no calculations were done for the
          ! cloud free part in that case
          if (  cf >= 1.0d0 ) then
            cloudyPart = .false.
            call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelRTMS, XsecRTMS, RRS_RingS,  &
                                controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS, &
                                mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,             &
                                surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,          &
                                depolarization)
            if (errorCheck(errS)) return
          end if !  cf >= 1.0d0

          call layerBasedOrdersScattering(errS,        &
            optPropRTMGridS,                     & 
            controlS,                            &
            geometryS,                           & 
            cloudAlb_iwave,                      & 
            cloudAerosolRTMgridS%RTMnlevelCloud, &   ! index for Lambertian cloud at level RTMnlevelCloud
            babs,                                &   ! output
            bsca,                                &   ! output
            refl_Cloud,                          &   ! output 
            contribRefl_Cloud,                   &   ! output
            UD_Cloud,                            &   ! output
            wfInterfkscaGas_Cloud,               &   ! output 
            wfInterfkscaAerAbove_Cloud,          &   ! output 
            wfInterfkscaAerBelow_Cloud,          &   ! output 
            wfInterfkscaCldAbove_Cloud,          &   ! output 
            wfInterfkscaCldBelow_Cloud,          &   ! output 
            wfInterfkabs_Cloud,                  &   ! output
            wfCloudAlbedo,                       &   ! output 
            wfSurfaceEmission_Cloud,             &   ! output 
            statusRTM)
          if (errorCheck(errS)) return

        else ! cloudAerosolRTMgridS%useLambertianCloud

          ! use scattering cloud

          cloudyPart = .true.
          call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelRTMS, XsecRTMS, RRS_RingS,  &
                              controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS, &
                              mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,             &
                              surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,          &
                              depolarization)
          if (errorCheck(errS)) return

          call layerBasedOrdersScattering(errS,        &
            optPropRTMGridS,                     & 
            controlS,                            &
            geometryS,                           & 
            cloudAlb_iwave,                      & 
            0,                                   &   ! index for Lambertian surface - here ground surface with index 0
            babs,                                &   ! output
            bsca,                                &   ! output
            refl_Cloud,                          &   ! output 
            contribRefl_Cloud,                   &   ! output
            UD_Cloud,                            &   ! output
            wfInterfkscaGas_Cloud,               &   ! output 
            wfInterfkscaAerAbove_Cloud,          &   ! output 
            wfInterfkscaAerBelow_Cloud,          &   ! output 
            wfInterfkscaCldAbove_Cloud,          &   ! output 
            wfInterfkscaCldBelow_Cloud,          &   ! output 
            wfInterfkabs_Cloud,                  &   ! output
            wfSurfaceAlbedo_Cloud,               &   ! output 
            wfSurfaceEmission_Cloud,             &   ! output 
            statusRTM)
          if (errorCheck(errS)) return

        end if ! cloudAerosolRTMgridS%useLambertianCloud

        refl(iwave)  = cf * refl_Cloud(1) + (1.0d0 - cf) * refl_Clear(1)
        amf(:,iwave) = ( cf * wfInterfkabs_Cloud(:) + (1.0d0 - cf) * wfInterfkabs_Clear(:) ) / refl(iwave)

        if ( present (radianceCldFraction) )  radianceCldFraction = cf * refl_Cloud(1) / refl(iwave)

        ! transform reflectance (as defined by Van de Hulst) into sun-normalized radiance
        refl(iwave) = geometryS%u0 * refl(iwave) / PI

      end do ! iwave

      ! make air mass factor positive
      amf = -amf

      if ( verbose ) then

        write(intermediateFileUnit,*)
        write(intermediateFileUnit,'(A)') ' reflectance and derivatives at a few wavelengths used for DOAS '
        write(intermediateFileUnit,'(A)') '      wavel    refl       derivatives  ....'
        do iwave = 1, wavelRTMS%nwavel
          write(intermediateFileUnit,'(F12.6,F12.8,100E14.5)') wavelRTMS%wavel(iwave),  &
                                                               refl(iwave),             &
                                ( amf(ilevel, iwave), ilevel = 0,optPropRTMGridS%RTMnlayer )
        end do
      end if

      ! clean up
      call deallocateUD(errS, optPropRTMGridS%RTMnlayer, UD_Clear, UD_Cloud)
      if (errorCheck(errS)) return

    end subroutine calcReflAndAMFRTM


    subroutine calculateReflAndDerivInstr(errS, numSpectrBands, nTrace, wavelInstrS, wavelRTMS,     &
                                          geometryS, XsecRTMS, gasPTS, traceGasS,             &
                                          surfaceS, LambCloudS, mieAerS,                      &
                                          cldAerFractionS, weakAbsRetrS, RRS_RingS,           &
                                          controlS, retrS, cloudAerosolRTMgridS, optPropRTMGridS)

      ! calculate reflectance and derivatives on the instrument wavelength grid, including
      ! the air mass factors

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands                  ! number wavelength band
      integer,                       intent(in)    :: nTrace                          ! number of trace gases
      type(wavelInstrType),          intent(in)    :: wavelInstrS(numSpectrBands)     ! wavelength grid for retrieval
      type(wavelHRType),             intent(inout) :: wavelRTMS(numSpectrBands)       ! wavelengths for RTM calculations
      type(geometryType),            intent(in)    :: geometryS                       ! info geometry
      type(XsecType),                intent(inout) :: XsecRTMS(numSpectrBands,nTrace) ! absorption cross sections set to zero
      type(gasPTType),               intent(inout) :: gasPTS                          ! pressure and temperature
      type(traceGasType),            intent(inout) :: traceGasS(nTrace)               ! atmospheric trace gas properties
      type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)        ! surface properties
      type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)      ! Lambertian cloud properties
      type(mieScatType),             intent(inout) :: mieAerS(maxNumMieModels)        ! specification expansion coefficients
      type(cldAerFractionType),      intent(inout) :: cldAerFractionS(numSpectrBands) ! cloud fraction properties
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands)    ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)       ! Ring spectra and parameters
      type(controlType),             intent(inout) :: controlS                        ! control parameters
      type(retrType),                intent(inout) :: retrS                           ! retrieval structure
      type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS            ! cloud-aerosol properties
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS                 ! optical properties on RTM grid

      ! local
      integer :: iband, iTrace
      integer :: ipoly, iwave, istate
      integer :: indexWavelRefl, indexWavelDeriv(retrS%nstate)
      real(8) :: scaling
      real(8) :: wStart, wEnd

      ! polynomial values on instrument grid
      real(8), allocatable :: polynomial(:)           ! (wavelInstrS(iband)%nwavel)
      real(8), allocatable :: lnpolynomial(:)         ! (wavelInstrS(iband)%nwavel)

      ! variables for fitting the polynomial to the reflectance (sun-normalized radiance)
      real(8), allocatable  :: wavelScaledRTM(:)      ! (wavelRTMS(iband)%nwavel)
      real(8), allocatable  :: wavelScaledInstr(:)    ! (wavelInstrS(iband)%nwavel)

      real(8), parameter :: DUToCm2 = 2.68668d16
      real(8), parameter :: PI = 3.141592653589793d0

      indexWavelRefl      = 1
      indexWavelDeriv(:)  = 1
      retrS%K_lnvmr(:,:)  = 0.0d0

      do iband = 1, numSpectrBands

        allocate( lnpolynomial     (wavelInstrS(iband)%nwavel) )
        allocate( polynomial       (wavelInstrS(iband)%nwavel) )
        allocate( wavelScaledRTM   (wavelRTMS(iband)%nwavel) )
        allocate( wavelScaledInstr (wavelInstrS(iband)%nwavel) )

        ! Perform RTM calculations at a few wavelengths => sun-normalized radiance (refl) and the
        ! altitude resolved air mass factors. Note that the reflectance calculated here is not used,
        ! only the air mass factors are used, as is appropriate for DOAS.

        call calcReflAndAMFRTM(errS, nTrace, wavelRTMS(iband), XsecRTMS(iband,:), geometryS,         &
                               RRS_RingS(iband), controlS, cloudAerosolRTMgridS, gasPTS,       &
                               traceGasS, surfaceS(iband), LambCloudS(iband),                  &
                               mieAerS, mieAerS, cldAerFractionS(iband), optPropRTMGridS,      &
                               weakAbsRetrS(iband)%reflRTM, weakAbsRetrS(iband)%amfAltRTM )
        if (errorCheck(errS)) return

        ! calculate scaled wavelengths on instrument grid and on RTM grid
        wStart = wavelInstrS(iband)%wavel(1)
        wEnd   = wavelInstrS(iband)%wavel( wavelInstrS(iband)%nwavel )

        wavelScaledInstr(:) = 2.0d0 * ( wavelInstrS(iband)%wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0
        wavelScaledRTM(:)   = 2.0d0 * ( wavelRTMS(iband)  %wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0

        ! One might be tempted to calculate the smooth reflectance from the values calculated by the
        ! call to calcReflAndAMFRTM. However, this interferes with optimal estimation. The smooth reflectance 
        ! must be calculated from the current polynomial coefficients.

        ! calculate polynomial
        lnpolynomial(:) =  weakAbsRetrS(iband)%lnpolyCoef(0)
        do iPoly = 1, weakAbsRetrS(iband)%degreePoly
          lnpolynomial(:) = lnpolynomial(:) +  &
                            weakAbsRetrS(iband)%lnpolyCoef(iPoly) * wavelScaledInstr(:) ** iPoly
        end do ! iPoly

        polynomial(:) = exp( lnpolynomial(:) )
        weakAbsRetrS(iband)%polynomial(:) = polynomial(:)

        do iwave = 1, wavelInstrS(iband)%nwavel
          ! calculate the reflectance using the DOAS expression
          retrS%refl(indexWavelRefl) = polynomial(iwave) * &
             exp( - sum(weakAbsRetrS(iband)%amfInstrGrid (iwave,:) * weakAbsRetrS(iband)%column(:) &
                        * weakAbsRetrS(iband)%XsecEffDiffInstr(iwave,:) ) )
          ! add differential Ring spectrum
          if ( RRS_RingS(iband)%fitDiffRingSpec ) then
            retrS%refl(indexWavelRefl) = retrS%refl(indexWavelRefl)   &
                                       + RRS_RingS(iband)%ringCoeff * RRS_RingS(iband)%diffRing(iwave)
          end if
          ! add 'normal 'Ring spectrum
          if ( RRS_RingS(iband)%fitRingSpec ) then
            retrS%refl(indexWavelRefl) = retrS%refl(indexWavelRefl)  &
                                       + RRS_RingS(iband)%ringCoeff * RRS_RingS(iband)%Ring(iwave)
          end if

          indexWavelRefl = indexWavelRefl + 1

        end do
        
        ! calclate the derivatives of the reflectance on the instrument grid w.r.t. the state vector elements 
        ! store the results in retrS%K_lnvmr(nwavelRetr,nState)

        do istate = 1, retrS%nstate

          select case (retrS%codeFitParameters(istate))

            case( 'columnTrace' )

              iTrace = retrS%codeTraceGas(istate)

              do iwave = 1, wavelInstrS(iband)%nwavel
                retrS%K_lnvmr(indexWavelDeriv(istate),istate) = - retrS%refl(indexWavelDeriv(istate)) * DUToCm2 &
                * weakAbsRetrS(iband)%XsecEffDiffInstr(iwave,iTrace) * weakAbsRetrS(iband)%amfInstrGrid(iwave,iTrace)
                indexWavelDeriv(istate) = indexWavelDeriv(istate) + 1
              end do ! iwave

            case('nodeTemp')

              call logDebug('fitting of temperature profile is not possible in DOAS')
              call mystop(errS, 'stopped because temperature profile fitting is not possible for DOAS')
              if (errorCheck(errS)) return

            case('offsetTemp')

              call logDebug('temperature fit for DOAS not yet implemented')
              call mystop(errS, 'stopped because temperature fit is not yet implemented')
              if (errorCheck(errS)) return

            case('diffRingCoef')

              do iwave = 1, wavelInstrS(iband)%nwavel
                retrS%K_lnvmr(indexWavelDeriv(istate),istate) = RRS_RingS(iband)%diffRing(iwave)

                indexWavelDeriv(istate) = indexWavelDeriv(istate) + 1
              end do ! iwave

            case('RingCoef')          

              do iwave = 1, wavelInstrS(iband)%nwavel
                retrS%K_lnvmr(indexWavelDeriv(istate),istate) = RRS_RingS(iband)%Ring(iwave)

                indexWavelDeriv(istate) = indexWavelDeriv(istate) + 1
              end do ! iwave

            case('lnpolyCoef')

              ! dR/dci = d[ exp(c0 + c1*ls + c2*ls**2 + ...) * exp(-M*N*sig) ] / dci
              !        =    exp(c0 + c1*ls + c2*ls**2 + ...) * exp(-M*N*sig) * ls**i
              !        =    R * ls**i
              ! where 'ls' is the scaled wavelength 

              ipoly = retrS%codelnPolyCoef(istate)
              if ( iband == retrS%codeSpecBand(istate) ) then

                do iwave = 1, wavelInstrS(iband)%nwavel
                  scaling = wavelScaledInstr(iwave)
                  if ( ipoly == 0 ) then
                    retrS%K_lnvmr(indexWavelDeriv(istate),istate) = &
                                retrS%refl(indexWavelDeriv(istate))
                  else
                    retrS%K_lnvmr(indexWavelDeriv(istate),istate) = scaling**ipoly &
                                                                  * retrS%refl(indexWavelDeriv(istate))
                  end if  
                  indexWavelDeriv(istate) = indexWavelDeriv(istate) + 1
                end do ! iwave

              end if ! iband == retrS%codeSpecBand(istate)

            case default
              call logDebug('in subroutine calculateReflAndDerivInstr in doasModule')
              call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
              call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(istate)')
              if (errorCheck(errS)) return
          end select

        end do ! istate

        deallocate( lnpolynomial )
        deallocate( polynomial )
        deallocate( wavelScaledRTM )
        deallocate( wavelScaledInstr )

      end do ! iband

    end subroutine calculateReflAndDerivInstr


    subroutine calculateDiagnosticsDOAS(errS, retrS, diagnosticS)

      ! calculate the error covariance matrix, averaging kernel, gain, and chi2

      implicit none

      type(errorType), intent(inout) :: errS
      type(retrType),          intent(in)    :: retrS
      type(diagnosticType),    intent(inout) :: diagnosticS

      ! local
      real(8) :: Kwhite(retrS%nwavelRetr,retrS%nState)
      real(8) :: sqrtSa(retrS%nState,retrS%nState), sqrtInvSa(retrS%nState,retrS%nState) 
      real(8) :: SaInv(retrS%nState,retrS%nState)
      real(8) :: Awhite(retrS%nState,retrS%nState), Atrans(retrS%nState,retrS%nState)
      real(8) :: Swhite(retrS%nState,retrS%nState), Strans(retrS%nState,retrS%nState)

      ! matrices for svd
      real(8) :: w(retrS%nState)
      real(8) :: ua(retrS%nState,retrS%nState), uaT(retrS%nState,retrS%nState)
      real(8) :: u(retrS%nwavelRetr,retrS%nState), uT(retrS%nState,retrS%nwavelRetr)
      real(8) :: v(  retrS%nState,retrS%nState), vT(retrS%nState,  retrS%nState)

      real(8) :: diagSaInv(retrS%nState, retrS%nState)
      real(8) :: diagsqrtSa(retrS%nState, retrS%nState)
      real(8) :: diagsqrtInvSa(retrS%nState, retrS%nState)

      ! various
      integer :: istate

      ! calculate sqrtSe (assuming a diagonal matrix)
      ! and their inverse values

      diagSaInv     = 0.0d0
      diagsqrtSa    = 0.0d0
      diagsqrtInvSa = 0.0d0

      ! lnvmr

      ua = retrS%Sa_lnvmr
      ! svdcmp overwrites first array (input) with u
      call svdcmp(errS, ua, w, v, retrS%nState)
      if (errorCheck(errS)) return
      uaT = transpose(ua)

      do istate = 1, retrS%nState
          diagSaInv(istate,istate)     = 1.0d0 / w(istate)
          diagsqrtSa(istate,istate)    = sqrt(w(istate))
          diagsqrtInvSa(istate,istate) = 1.0d0 / diagsqrtSa(istate,istate)
      end do

      sqrtInvSa = matmul(v, matmul(diagsqrtInvSa, uaT) )
      sqrtSa    = matmul(v, matmul(diagsqrtSa   , uaT) )
      SaInv     = matmul(v, matmul(diagSaInv    , uaT) )

      ! calculate dxwhite, dRwhite, and Kwhite
      Kwhite   = matmul(retrS%sqrtInvSe,matmul(retrS%K_lnvmr,sqrtSa))

      ! find the singular value decomposition for Kwhite = U W VT

      u = Kwhite
      ! svdcmp overwrites first array (input) with u
      call svdcmp(errS, u, w, v, retrS%nState)
      if (errorCheck(errS)) return
      vT = transpose(v)
      uT = transpose(u)
           
      Strans    = 0.0d0
      Atrans    = 0.0d0
      do istate = 1, retrS%nState
          Strans(istate,istate)    = 1.0d0 / (1.0d0 + w(istate)**2)
          Atrans(istate,istate)    = w(istate)**2 * Strans(istate,istate)
      end do

      Swhite    = matmul(v,matmul(Strans,vT))
      Awhite    = matmul(v,matmul(Atrans,vT))

      diagnosticS%S_lnvmr      = matmul(sqrtSa,matmul(Swhite,sqrtSa))
      diagnosticS%A_lnvmr      = matmul(sqrtSa,matmul(Awhite,sqrtInvSa))
      diagnosticS%G_lnvmr      = matmul(matmul(diagnosticS%S_lnvmr,transpose(retrS%K_lnvmr)), retrS%InvSe)
      diagnosticS%Snoise_lnvmr = matmul(matmul(diagnosticS%G_lnvmr,retrS%Se),transpose(diagnosticS%G_lnvmr))

      diagnosticS%infoContent_lnvmr = 0.5d0 * sum( log(1.0d0 + w(:)**2) )

      diagnosticS%DFS = 0.0d0
      do istate = 1, retrS%nstate
        diagnosticS%DFS = diagnosticS%DFS + diagnosticS%A_lnvmr(istate,istate)
      end do

      ! calculate smoothed state vector, retrS%x_smoothed

      diagnosticS%x_smoothed = retrS%xa + matmul(diagnosticS%A_lnvmr, retrS%x_true - retrS%xa)

    end subroutine calculateDiagnosticsDOAS


    subroutine print_resultsDOAS(errS, numSpectrBands, nTrace, wavelInstrS, traceGasSimS, traceGasRetrS, &
                                 weakAbsRetrS, RRS_RingSimS, RRS_RingRetrS, retrS, diagnosticS)

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands                  ! number wavelength band
      integer,                       intent(in)    :: nTrace                          ! number of trace gases
      type(wavelInstrType),          intent(in)    :: wavelInstrS(numSpectrBands)     ! wavelength grid for retrieval
      type(traceGasType),            intent(inout) :: traceGasSimS(nTrace)            ! atmospheric trace gas properties
      type(traceGasType),            intent(inout) :: traceGasRetrS(nTrace)           ! atmospheric trace gas properties
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands)    ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingSimS(numSpectrBands)    ! Ring spectra
      type(RRS_RingType),            intent(inout) :: RRS_RingRetrS(numSpectrBands)   ! Ring spectra
      type(retrType),                intent(inout) :: retrS                           ! retrieval structure
      type(diagnosticType),          intent(inout) :: diagnosticS                     ! diagnostic information

      ! local
      integer :: iwave, istate, jstate, iband, ipoly, iTrace
    
      real(8) :: factor
  
      real(8), parameter :: DUToCm2 = 2.68668d16

      write(outputFileUnit,*)
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'RESULTS OF OPTIMAL ESTIMATION - DOAS'
      write(outputFileUnit,*)
      write(outputFileUnit,'(A, 2F10.5)') 'DFS      = ', diagnosticS%DFS
      write(outputFileUnit,*)
      write(outputFileUnit,'(A, 2F10.5)') 'information content = ', diagnosticS%infoContent_lnvmr

      write(outputFileUnit,*)
      write(outputFileUnit,*) 'ACCURACY OF THE FITTED SUN-NORMALIZED RADIANCE (residue)'
      write(outputFileUnit,*)
  
      if ( retrS%isConverged ) then
        write(outputFileUnit,'(A,2F12.5,I4)') ' solution has converged: xConvThreshold xConv  nIter = ', &
                        retrS%xConvThreshold, retrS%xConv, retrS%numIterations 
      else
        write(outputFileUnit,'(A,2E15.5,I4)') ' solution has NOT converged: xConvThreshold xConv nIter = ', &
                        retrS%xConvThreshold, retrS%xConv, retrS%numIterations
      end if

      write(outputFileUnit,*)
      write(outputFileUnit,'(A)') 'Here reflectance is used to denote the sun-normalized radiance'
      write(outputFileUnit,'(A)') &
        '         wavel     actual S/N     measured refl      fitted refl     rel diff(%) rel diff initial(%)'
      do iwave = 1, retrS%nwavelRetr
        write(outputFileUnit,'(A, f12.4, F12.2, 2F18.8, 2F15.6)')  'data',              &
                                        retrS%wavelRetr(iwave),                         &
                                        retrS%refl(iwave)/retrS%reflNoiseError(iwave),  &
                                        retrS%reflMeas(iwave), retrS%refl(iwave),       &
                              100.0d0 * retrS%dR(iwave)/retrS%reflMeas(iwave),          &
                              100.0d0 * retrS%dR_initial(iwave)/retrS%reflMeas(iwave)
      end do

      do istate = 1, retrS%nstate
  
        select case (retrS%codeFitParameters(istate))
  
  
          case('columnTrace')
  
            iTrace = retrS%codeTraceGas(istate)
            if ( traceGasRetrS(iTrace)%unitFlag ) then
              factor = 1.0 / DUToCm2
            else
              factor = 1.0d0
            end if

            write(outputFileUnit,*)
            write(outputFileUnit,*) traceGasRetrS(iTrace)%nameTraceGas
            write(outputFileUnit,'(A,3ES12.4,F9.3)') 'a-priori true and RETRIEVED TRACE GAS COLUMN and bias(%)           = ', &
                                                     factor * traceGasRetrS(iTrace)%columnAP,                                 &
                                                     factor * traceGasSimS(iTrace)%column,                                    &
                                                     factor * traceGasRetrS(iTrace)%column,                                   &
               100*( traceGasRetrS(iTrace)%column - traceGasSimS(iTrace)%column ) / traceGasSimS(iTrace)%column
            write(outputFileUnit,'(A, 3ES12.4)')     'a-priori. a-posteriori, and rel. error (%) trace gas column        = ', &
                                                     factor * sqrt(traceGasRetrS(iTrace)%covColumnAP ),                       &
                                                     factor * sqrt(traceGasRetrS(iTrace)%covColumn),                          &
                                                     100.0d0* sqrt(traceGasRetrS(iTrace)%covColumn) /                         &
                                                                 traceGasRetrS(iTrace)%column

          case('nodeTemp')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A,2F10.3)') 'true and RETRIEVED TEMPERATURE       = '

          case('offsetTemp')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A,2F10.3)') 'true and RETRIEVED TEMPERATURE       = '

          case('lnpolyCoef')
  
            iband = retrS%codeSpecBand(istate)
            ipoly = retrS%codelnPolyCoef(istate)

            if ( ipoly == 0) then
  
              write(outputFileUnit,*)
              write(outputFileUnit,'(A,50F10.5)') 'a-priori coefficients for ln(P)                                = ', &
                                                   weakAbsRetrS(iband)%lnpolyCoefAP(:)
              write(outputFileUnit,'(A,50F10.5)') 'RETRIEVED COEFFICIENTS FOR ln(P)                               = ', &
                                                   weakAbsRetrS(iband)%lnpolyCoef(:)
              write(outputFileUnit,'(A,50F10.5)') 'a-priori error coefficients (1 std dev)                        = ', &
                  ( sqrt( weakAbsRetrS(iband)%covlnPolyCoefAP(ipoly) ), ipoly = 0, weakAbsRetrS(iband)%degreePoly)
              write(outputFileUnit,'(A,50F10.5)') 'a-posteriori error coefficients (1 std dev)                    = ', &
                  ( sqrt( weakAbsRetrS(iband)%covlnPolyCoef(ipoly) ), ipoly = 0, weakAbsRetrS(iband)%degreePoly)
            end if

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)
            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.8)')   'a-priori true and retrieved DIFFERENTAL RING COEFFICIENT       = ', &
                 RRS_RingRetrS(iband)%ringCoeffAP, RRS_RingSimS(iband)%ringCoeff, RRS_RingRetrS(iband)%ringCoeff
            write(outputFileUnit,'(A, 2F12.8)')   'a-priori and a-posteriori error differential Ring coefficient  = ', &
                sqrt(RRS_RingRetrS(iband)%varRingCoeffAP), sqrt(RRS_RingRetrS(iband)%varRingCoeff)
 
          case('RingCoef')

            iband = retrS%codeSpecBand(istate)
            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved RING COEFFICIENT                   = ', &
                 RRS_RingRetrS(iband)%ringCoeffAP, RRS_RingSimS(iband)%ringCoeff, RRS_RingRetrS(iband)%ringCoeff
            write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error Ring coefficient               = ', &
                sqrt(RRS_RingRetrS(iband)%varRingCoeffAP), sqrt(RRS_RingRetrS(iband)%varRingCoeff)

  
          case default
            call logDebug('in subroutine print_resultsDOAS in doasModule' )
            call logDebug('incorrect value for retrS%codeFitParameters(counter) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return
        end select
  
      end do ! istate

      ! write polynomial and monochromatic air mass factors on instrument wavelength grid
      write(outputFileUnit,*)
      do iband = 1, numSpectrBands
        write(outputFileUnit,*)
        write(outputFileUnit,'(A,I4)') 'results for spectral band number: ', iband
        write(outputFileUnit,'(A)')  '   wavelength        polynomial'
        do iwave = 1, wavelInstrS(iband)%nwavel
          write(outputFileUnit,'(2F15.8)') wavelInstrS(iband)%wavel(iwave), weakAbsRetrS(iband)%polynomial(iwave)
        end do ! iwave
        do iTrace = 1, nTrace
          write(outputFileUnit,*)
          write(outputFileUnit,'(A,I4)') 'results for spectral band number: ', iband
          write(outputFileUnit,'(A)') 'wavelength(nm)   air mass factor  Xsec_eff    Xsec_eff_smooth   Xsec_eff_diff '
          do iwave = 1, wavelInstrS(iband)%nwavel
            write(outputFileUnit,'(2F15.8, 3ES15.6)')  wavelInstrS(iband)%wavel(iwave),                        &
                                                       weakAbsRetrS(iband)%amfInstrGrid(iwave, iTrace),        &
                                                       weakAbsRetrS(iband)%XsecEffInstr(iwave, iTrace),        &
                                                       weakAbsRetrS(iband)%XsecEffSmoothInstr(iwave, iTrace),  &
                                                       weakAbsRetrS(iband)%XsecEffDiffInstr(iwave, iTrace)
          end do ! iwave
        end do ! iTrace
      end do ! iband
      
      ! write full a-priori errror covariance matrix ordered according to the results printed above
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'full a-priori covariance matrix Sa'
      do istate = 1, retrS%nstate
        write(outputFileUnit,'(A, 500E17.8)') retrS%codeFitParameters(istate), &
                          (retrS%Sa_lnvmr(istate,jstate), jstate = 1, retrS%nstate)
  
      end do
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'full a-posteriori covariance matrix S '
      do istate = 1, retrS%nstate
        write(outputFileUnit,'(A, 500E17.8)') retrS%codeFitParameters(istate), &
                        (diagnosticS%S_lnvmr(istate,jstate), jstate = 1, retrS%nstate)
      end do
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'noise part of the a-posteriori covariance matrix S '
      do istate = 1, retrS%nstate
        write(outputFileUnit,'(A, 500E17.8)') retrS%codeFitParameters(istate),  &
                        (diagnosticS%Snoise_lnvmr(istate,jstate), jstate = 1, retrS%nstate)
      end do
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'correlation coefficients (r**2) matrix for S'
      do istate = 1, retrS%nstate
        write(outputFileUnit,'(A, 500E17.8)') retrS%codeFitParameters(istate), (diagnosticS%S_lnvmr(istate,jstate) &
        / sqrt( diagnosticS%S_lnvmr(istate,istate) * diagnosticS%S_lnvmr(jstate,jstate) ) , jstate = 1, retrS%nstate)
      end do

    end subroutine print_resultsDOAS


    subroutine print_resultsDOAS_asciiHDF(errS, numSpectrBands, nTrace, geometryS, gasPTSimS, gasPTRetrS,  &
                                          traceGasSimS, traceGasRetrS, weakAbsRetrS, RRS_RingSimS, RRS_RingRetrS, &
                                          retrS, diagnosticS)

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: numSpectrBands                  ! number wavelength band
      integer,                       intent(in)    :: nTrace                          ! number of trace gases
      type(geometryType),            intent(in)    :: geometryS                       ! info geometry
      type(gasPTType),               intent(inout) :: gasPTSimS                       ! pressure and temperature
      type(gasPTType),               intent(inout) :: gasPTRetrS                      ! pressure and temperature
      type(traceGasType),            intent(inout) :: traceGasSimS(nTrace)            ! atmospheric trace gas properties
      type(traceGasType),            intent(inout) :: traceGasRetrS(nTrace)           ! atmospheric trace gas properties
      type(weakAbsType),             intent(inout) :: weakAbsRetrS(numSpectrBands)    ! DOAS results
      type(RRS_RingType),            intent(inout) :: RRS_RingSimS(numSpectrBands)    ! Ring spectra and parameters
      type(RRS_RingType),            intent(inout) :: RRS_RingRetrS(numSpectrBands)   ! Ring spectra and parameters
      type(retrType),                intent(inout) :: retrS                           ! retrieval structure
      type(diagnosticType),          intent(inout) :: diagnosticS                     ! diagnostic information

      ! local
      integer :: istate, jstate, iband, ipoly, iTrace
      integer :: counterTrace
  
      real(8), parameter :: DUToCm2 = 2.68668d16

      character(LEN=50) :: legend(3), legend_precision(3)
      character(LEN=50) :: legendPoly(2), legend_precisionPoly(2)
      character(LEN=50) :: nameTraceGas(nTrace), unit(nTrace)
      character(LEN=50) :: nameTraceGas_state_vector(retrS%nstate)
      character(LEN=50) :: unit_state_vector(retrS%nstate)
      character(LEN=50) :: unit_covariance(retrS%nstate)

      character(LEN=50) :: groupName, name, keyword
      character(LEN=50) :: keywordList1, keywordList2, keywordList3, keywordList4
      character(LEN=50) :: keyword1
      character(LEN=50) :: string_1

      real(8)    :: factor
      real(8)    :: wavelengths(retrS%nstate)
      real(8)    :: correlation(retrS%nstate,retrS%nstate)
      real(8)    :: columns(3, nTrace), precisionColumns(3, nTrace)
      real(8)    :: allBands(3), precisionAllBands(3)

      real(8), allocatable  :: lnpolyCoef(:,:,:)           ! (maxNumCoefficients, a-priori/retrieved, numSpectrBands)
      real(8), allocatable  :: lnpolyCoefPrecision(:,:,:)  ! (maxNumCoefficients, a-priori/retrieved, numSpectrBands)

      integer    :: maxNumCoefficients             ! maximum number of coefficients for the spectral bands
      integer    :: spectralBandNumber(numSpectrBands)

      ! initialize counters
      counterTrace  = 0

      ! fill spectral band number
      do iband = 1, numSpectrBands
        spectralBandNumber(iband) = iband
      end do

      ! fill wavelengths and units array so that it can be used as attribute

      do istate = 1, retrS%nstate
  
        select case (retrS%codeFitParameters(istate))
  
          case('columnTrace')
            iTrace = retrS%codeTraceGas(istate)
            if ( traceGasRetrS(iTrace)%unitFlag ) then
              unit_state_vector(istate) = 'Dobson_Units'
              unit_covariance(istate)   = 'Dobson_Units2'
            else
              unit_state_vector(istate) = 'molecules_per_cm2'
              unit_covariance(istate)   = 'molecules2_per_cm4'
            end if
            nameTraceGas_state_vector(istate) = traceGasRetrS(iTrace)%nameTraceGas

          case('nodeTemp', 'offsetTemp')
            unit_state_vector(istate) = 'Kelvin'
            unit_covariance(istate)   = 'Kelvin2'
            nameTraceGas_state_vector(istate) = ' '

          case('lnpolyCoef')
            unit_state_vector(istate) = ' '
            unit_covariance(istate)   = ' '
            nameTraceGas_state_vector(istate) = ' '

          case default
            call logDebug('in subroutine print_resultsDOAS_asciiHDF in doasModule')
            call logDebug('incorrect value for retrS%codeFitParameters(counter) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return
        end select
  
      end do ! istate

      ! fill correlation coefficient matrix for S_lnvmr
      do jstate = 1, retrS%nstate
        do istate = 1, retrS%nstate
          correlation(istate,jstate) = diagnosticS%S_lnvmr(istate,jstate) &
            / sqrt( diagnosticS%S_lnvmr(istate,istate) * diagnosticS%S_lnvmr(jstate,jstate) )
        end do ! istate
      end do ! jstate

      ! fill legend and legend precision
      legend(1) = 'true'
      legend(2) = 'a_priori'
      legend(3) = 'retrieved'
      legend_precision(1) = 'a_priori_precision'
      legend_precision(2) = 'precision'
      legend_precision(3) = 'bias'

      legendPoly(1)           = 'a_priori'
      legendPoly(2)           = 'retrieved'
      legend_precisionPoly(1) = 'a_priori_precision'
      legend_precisionPoly(2) = 'precision'

      ! find the maximum number of polynomial coefficients
      maxNumCoefficients = maxval( weakAbsRetrS(:)%degreePoly )

      allocate ( lnpolyCoef         (0:maxNumCoefficients, 2, numSpectrBands) )
      allocate ( lnpolyCoefPrecision(0:maxNumCoefficients, 2, numSpectrBands) )
      lnpolyCoef          = 0.0d0
      lnpolyCoefPrecision = 0.0d0

      call initializeAsciiHDF
        call beginAttributes
          keyword = 'version_number_DISAMAR'
          call addAttribute(errS, keyword, DISAMAR_version_number)
          if (errorCheck(errS)) return
          keyword = 'solar_zenith_angle_retrieval'
          call addAttribute(errS, keyword, geometryS%sza)
          if (errorCheck(errS)) return
          keyword = 'viewing_zenith_angle_retrieval'
          call addAttribute(errS, keyword, geometryS%vza)
          if (errorCheck(errS)) return
          keyword = 'azimuth_difference_retrieval'
          call addAttribute(errS, keyword, geometryS%dphi)
          if (errorCheck(errS)) return
          keyword = 'number_spectral_bands'
          call addAttribute(errS, keyword, numSpectrBands)
          if (errorCheck(errS)) return
          keyword = 'number of iterations'
          call addAttribute(errS, keyword, retrS%numIterations + 1)
          if (errorCheck(errS)) return
          keyword = 'solution has converged'
          if ( retrS%isConverged ) then
            call addAttribute(errS, keyword, .true.)
            if (errorCheck(errS)) return
          else
            call addAttribute(errS, keyword, .false.)
            if (errorCheck(errS)) return
          end if
          keyword = 'stateVectorConvThreshold'
          call addAttribute(errS, keyword, retrS%xConvThreshold)
          if (errorCheck(errS)) return
          keyword = 'stateVectorConv'
          call addAttribute(errS, keyword, retrS%xConv)
          if (errorCheck(errS)) return
          keyword = 'chi2_reflectance'
          call addAttribute(errS, keyword, retrS%chi2R)
          if (errorCheck(errS)) return
          keyword = 'chi2_state_vector'
          call addAttribute(errS, keyword, retrS%chi2x)
          if (errorCheck(errS)) return
          keyword = 'chi2'
          call addAttribute(errS, keyword, retrS%chi2)
          if (errorCheck(errS)) return
          keyword = 'rmse'
          call addAttribute(errS, keyword, retrS%rmse)
          if (errorCheck(errS)) return
          keyword = 'DFS'
          call addAttribute(errS, keyword, diagnosticS%DFS)
          if (errorCheck(errS)) return
        call endAttributes

      groupName = 'sun_normalized_radiance'
      call beginGroup(errS, groupName)
      if (errorCheck(errS)) return
        keyword1 = 'unit'
        string_1 = 'nm'
        name = 'wavelengths'
        call writeReal(errS, name, x1D=retrS%wavelRetr, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = ' '
        name = 'SNR_reference_spectrum'
        call writeReal(errS, name, x1D=retrS%SNrefspec, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = ' '
        name = 'SNR_measured_spectrum'
        call writeReal(errS, name, x1D=retrS%reflMeas/retrS%reflNoiseError, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'per_sr'
        name = 'measured_spectrum'
        call writeReal(errS, name, x1D=retrS%reflMeas, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'per_sr'
        name = 'fitted_spectrum'
        call writeReal(errS, name, x1D=retrS%refl, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'per_sr'
        name = 'assumed_noise'
        call writeReal(errS, name, x1D=retrS%reflNoiseError, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'percent'
        name = 'initial_residue'
        call writeReal(errS, name, x1D=1.0D2*retrS%dR_initial(:)/retrS%reflMeas(:), keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'percent'
        name = 'final_residue'
        call writeReal(errS, name, x1D=1.0D2*retrS%dR(:)/retrS%reflMeas(:), keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
      call endGroup

      groupName = 'parameters'
      call beginGroup(errS, groupName)
      if (errorCheck(errS)) return

      do istate = 1, retrS%nstate
  
        select case (retrS%codeFitParameters(istate))
  
          case('columnTrace')
  
            counterTrace = counterTrace + 1
            iTrace = retrS%codeTraceGas(istate)

            keyword = 'unit'
            
            if ( traceGasRetrS(iTrace)%unitFlag ) then
              factor = 1.0d0 / DUToCm2
              unit(iTrace)  = 'Dobson Units'
            else
              factor = 1.0d0
              unit(iTrace)  = 'molecules_per_cm2'
            end if
            nameTraceGas(iTrace) = traceGasRetrS(iTrace)%nameTraceGas
            columns(1,iTrace)    = factor * traceGasSimS(iTrace)%column      ! true column
            columns(2,iTrace)    = factor * traceGasRetrS(iTrace)%columnAP   ! a-priori column
            columns(3,iTrace)    = factor * traceGasRetrS(iTrace)%column     ! retrieved column

            precisionColumns(1,iTrace) = factor * sqrt(traceGasRetrS(iTrace)%covColumnAP)  ! a-priori precision
            precisionColumns(2,iTrace) = factor * sqrt(traceGasRetrS(iTrace)%covColumn)    ! a-posteriori precision
            precisionColumns(3,iTrace) = columns(3,iTrace) - columns(1,iTrace)             ! bias

            if( counterTrace == nTrace ) then
              keyword1     = 'unit'
              name = 'total_columns'
              keywordList1 = 'name_trace_gas'
              keywordList2 = 'unit'
              keywordList3 = 'legend'
              call writeReal(errS, name, x2D = columns, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit,         &
                             keywordList3 = keywordList3, stringList_3 = legend )
              if (errorCheck(errS)) return
              name = 'precision_bias_columns'
              call writeReal(errS, name, x2D = precisionColumns, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit,         &
                             keywordList3 = keywordList3, stringList_3 = legend_precision )
              if (errorCheck(errS)) return
            end if

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)

            allBands(1) = RRS_RingSimS(iband)%ringCoeff
            allBands(2) = RRS_RingRetrS(iband)%ringCoeffAP
            allBands(3) = RRS_RingRetrS(iband)%ringCoeff

            precisionAllBands(1) = sqrt(RRS_RingRetrS(iband)%varRingCoeffAP)   ! a-priori prescision
            precisionAllBands(2) = sqrt(RRS_RingRetrS(iband)%varRingCoeff)     ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'diffRingCoef'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return

          case('RingCoef')

            iband = retrS%codeSpecBand(istate)

            allBands(1) = RRS_RingSimS(iband)%ringCoeff
            allBands(2) = RRS_RingRetrS(iband)%ringCoeffAP
            allBands(3) = RRS_RingRetrS(iband)%ringCoeff

            precisionAllBands(1) = sqrt(RRS_RingRetrS(iband)%varRingCoeffAP)   ! a-priori prescision
            precisionAllBands(2) = sqrt(RRS_RingRetrS(iband)%varRingCoeff)     ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'RingCoef'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return

          case('nodeTemp')

          case('offsetTemp')

            allBands(1) = gasPTSimS%temperatureOffset
            allBands(2) = gasPTRetrS%temperatureOffsetAP
            allBands(3) = gasPTRetrS%temperatureOffset

            precisionAllBands(1) = sqrt(gasPTRetrS%varianceTempOffsetAP) ! a-priori precision
            precisionAllBands(2) = sqrt(gasPTRetrS%varianceTempOffset)   ! precision
            precisionAllBands(3) = allBands(3) - allBands(1)             ! bias

            name         = 'temperature_offset'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_temperature_offset'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('lnpolyCoef')

            iband = retrS%codeSpecBand(istate)
            ipoly = retrS%codelnPolyCoef(istate)

            lnpolyCoef(ipoly, 1, iband) = weakAbsRetrS(iband)%lnpolyCoefAP(ipoly)
            lnpolyCoef(ipoly, 2, iband) = weakAbsRetrS(iband)%lnpolyCoef  (ipoly)

            lnpolyCoefPrecision(ipoly, 1, iband) = sqrt( weakAbsRetrS(iband)%covlnPolyCoefAP(ipoly) )
            lnpolyCoefPrecision(ipoly, 2, iband) = sqrt( weakAbsRetrS(iband)%covlnPolyCoef  (ipoly) )

            if ( (ipoly == weakAbsRetrS(iband)%degreePoly) .and. (iband == numSpectrBands) ) then
  
              name = 'ln_polynomial_coefficients'
              keyword1     = 'unit'
              keywordList1 = 'legendPoly'
              keywordList2 = 'spectral_band_number'
              call writeReal(errS, name, x3D = lnpolyCoef, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = legendPoly, &
                             keywordList2 = keywordList2, intList_2    = spectralBandNumber)
              if (errorCheck(errS)) return

              name = 'precision_ln_poly_coef'
              keyword1     = 'unit'
              keywordList1 = 'legendPoly'
              keywordList2 = 'spectral_band_number'
              call writeReal(errS, name, x3D = lnpolyCoefPrecision, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = legendPoly, &
                             keywordList2 = keywordList2, intList_2    = spectralBandNumber)
              if (errorCheck(errS)) return

            end if

          case default

            call logDebug('in subroutine print_resultsDOAS_asciiHDF in doasModule' )
            call logDebug('incorrect value for retrS%codeFitParameters(counter) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return

        end select
  
      end do ! istate

      keywordList1 = 'state_vector_elements'
      keywordList2 = 'unit'
      keywordList3 = 'wavelength'
      keywordList4 = 'nameTraceGas'

      name = 'correlation_coefficients'
      call writeReal(errS, name, x2D = correlation, &
                     keywordList1 = keywordList1, stringList_1 = retrS%codeFitParameters(1:retrS%nstate), &
                     keywordList3 = keywordList3, real8List_3  = wavelengths, &
                     keywordList4 = keywordList4, stringList_4 = nameTraceGas_state_vector)
      if (errorCheck(errS)) return

      call endGroup

      call endAsciiHDF(errS)
      if (errorCheck(errS)) return

      deallocate(lnpolyCoef)
      deallocate(lnpolyCoefPrecision)

    end subroutine print_resultsDOAS_asciiHDF


end module doasModule

