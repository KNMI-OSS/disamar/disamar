!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

  module readModule

    ! This module contains subroutines to read
    ! - solar irradiance
    ! - absorption cross sections
    ! - expansion coefficients for the phase function for Mie scattering
    ! - a-priori covariance matrix for the trace gas
    ! - reference radiance spectrum needed to define signal to noise ratio
    ! - OMI pressure temperature profiles and reflecance data
    ! - polarization correction file
    ! - TOMS V8 climatology

  use DISAMAR_file
  use dataStructures
  use inputModule
  use mathTools,    only: spline, splint, splintLin, gaussDivPoints, getSmoothAndDiffXsec, polyInt, &
                          GaussDivPoints
  use ramansspecs,  only: ConvoluteSpecRaman

  ! default is private
  private
  public  :: getAbsorptionCoef, getAbsorptionCoefStatic, readScatteringModels, readGOME2slitfunctions
  public  :: getHRSolarIrradiance, getHRSolarIrradianceStatic, readAPcovFromFile
  public  :: getRadianceRefSpectrumfromFile, getNextNonCommentedLine
  public  :: readPolarizationCorrectionFile, getCharatisticBiasfromFile
  public  :: read_TOMS_V8_climatology, fillTemperatureNodes, fillOzoneNodes

  contains

    subroutine getNextNonCommentedLine(errS, configurationFileUnit)

      ! position reading pointer at the beginning of
      ! the next line that does not start with the '#' symbol

      implicit none

      type(errorType), intent(inout) :: errS
      integer, intent (in) :: configurationFileUnit

      character(len=1), parameter :: commentSign = '#'
      character(len=1)            :: firstChar

      do
        read(configurationFileUnit, *, end = 10) firstChar
        if ( firstChar /= commentSign ) then
          ! go back to the beginning of the record
          backspace configurationFileUnit
          exit ! jump out of do loop
        end if
      end do

      return

  10  continue

      call logDebugI('unexpected end of file encountered at unit number = ', configurationFileUnit )

    end subroutine getNextNonCommentedLine


    subroutine getHRSolarIrradiance (errS, wavelMRS, solarIrradianceS, RRS_RingS, wavelHRS)

     ! Read the solar spectrum and interpolate to the high-resolution grids for simulation and
     ! retrieval

       implicit none

       type(errorType),              intent(inout) :: errS
       type(wavelHRType),            intent(in)    :: wavelMRS
       type(SolarIrrType),           intent(inout) :: solarIrradianceS
       type(RRS_RingType), optional, intent(inout) :: RRS_RingS
       type(wavelHRType),  optional, intent(in)    :: wavelHRS


     ! local
       integer               :: openErrorSolIrrFile
       integer               :: Nheader
       integer               :: i, iwave, nwavel, status_spline, statusSplint
       real(8)               :: startWavel, endWavel, w
       real(8)               :: solIrr_watt, solIrr_photons
       real(8), allocatable  :: w_fitwindow(:), solIrr_fitwindow(:)
       real(8), allocatable  :: SDsolIrr_fitwindow(:)
       logical               :: errorFitwindowCoveredStart
       integer               :: allocStatus, deallocStatus
       type(file_type)       :: file
       integer               :: status
       character(len=300)    :: line
       logical, parameter    :: verbose = .false.

       call enter('getHRSolarIrradiance')

       ! initialize
       errorFitwindowCoveredStart = .true.

       ! determine the waveength range needed
       if ( present(wavelHRS) ) then
         startWavel = wavelHRS%wavel(1)
         endWavel   = wavelHRS%wavel(wavelHRS%nwavel)
       else
         startWavel = wavelMRS%wavel(1)
         endWavel   = wavelMRS%wavel(wavelMRS%nwavel)
       end if

       ! open solar irradiance file

       openErrorSolIrrFile = load_file(solarIrradianceS%solarIrrFilename, file)
       if (openErrorSolIrrFile /= 0) then
         call logDebug('ERROR in getHRSolarIrradiance: failed to open file')
         call logDebug('solarIrradianceS%solarIrrFilename: '// solarIrradianceS%solarIrrFilename)
         call mystop(errS, 'stopped due to solar irradiance file error')
         if (errorCheck(errS)) return
       end if

       status = file_readline(file, line)
       if (status /= 0) goto 40
       read(line,*) Nheader
       do i = 1, Nheader                          ! read away header lines
         status = file_readline(file, line)
         if (status /= 0) goto 40
       end do

       ! determine the number of wavelengths needed for the current fit window
       nwavel = 0
       do
         status = file_readline(file, line)
         if (status /= 0) goto 40
         read (line, *, end=40) w

         if ( w < startWavel ) then
           errorFitwindowCoveredStart = .false.
           cycle  ! read next line
         end if

         if ( (w >= startWavel) .AND. (w <= endWavel)) then
           nwavel = nwavel + 1
         else
           if ( w > endWavel ) exit  ! exit do loop
         end if
       end do

       if ( errorFitwindowCoveredStart ) then
         call logDebug('ERROR: solar irradiance spectrum does not cover the start of the fit window')
         call logDebug('file name: '// trim(solarIrradianceS%solarIrrFilename))
         write(errS%temp,'(A,2F8.2)') 'extended fit window: ', startWavel, endWavel
         call errorAddLine(errS, errS%temp)
         call mystop(errS, '2 stopped because irradiance does not cover fit window')
         if (errorCheck(errS)) return
       end if

       ! allocate memory space
       allocate( w_fitwindow(nwavel), solIrr_fitwindow(nwavel), SDsolIrr_fitwindow(nwavel), &
                 STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('ERROR: allocation failed')
         call logDebug('in subroutine getHRSolarIrradiance')
         call logDebug('in module readModule')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       file%cursor = 1

       status = file_readline(file, line)
       if (status /= 0) goto 40
       read(line,*) Nheader
       do i = 1, Nheader                          ! read away header lines
         status = file_readline(file, line)
         if (status /= 0) goto 40
       end do

       iwave = 0
       solIrr_photons = 0.0d0
       do
         status = file_readline(file, line)
         if (status /= 0) goto 40
         read(line, *, end=40) w, solIrr_watt, solIrr_photons
         if ((w >= startWavel) .AND. (w <= endWavel)) then
           iwave = iwave + 1
           w_fitwindow(iwave)      = w
           solIrr_fitwindow(iwave) = solIrr_photons
!          write(intermediateFileUnit,'(3E20.10)') w, solIrr_watt, solIrr_photons
         else
           if (w > endWavel) exit  ! exit do loop
         end if
       end do

       ! prepare for spline interpolation (second order derivatives)
       call spline(errS, w_fitwindow, solIrr_fitwindow, SDsolIrr_fitwindow, status_spline)
       if (errorCheck(errS)) return

       do iwave = 1, wavelMRS%nwavel
         solarIrradianceS%solIrrMR(iwave) = splint(errS, w_fitwindow, solIrr_fitwindow, SDsolIrr_fitwindow, &
                        wavelMRS%wavel(iwave)+solarIrradianceS%wavelShift, statusSplint)
!         solarIrradianceS%solIrrMR(iwave) = splintLin(w_fitwindow, solIrr_fitwindow,  &
!                        wavelMRS%wavel(iwave)+solarIrradianceS%wavelShift, statusSplint)
       end do

       if ( present(wavelHRS) ) then
         do iwave = 1, wavelHRS%nwavel
           solarIrradianceS%solIrrHR(iwave) = splint(errS, w_fitwindow, solIrr_fitwindow, SDsolIrr_fitwindow, &
                          wavelHRS%wavel(iwave)+solarIrradianceS%wavelShift, statusSplint)
!             solarIrradianceS%solIrrHR(iwave) = splintLin(w_fitwindow, solIrr_fitwindow,  &
!                           wavelHRS%wavel(iwave)+solarIrradianceS%wavelShift, statusSplint)
         end do
       end if

       if ( verbose ) then
         write(intermediateFileUnit, *)
         write(intermediateFileUnit, *) 'solar irradiance read'
         do iwave = 1, nwavel
           write(intermediateFileUnit, '(F12.7, ES20.10)') w_fitwindow(iwave),  solIrr_fitwindow(iwave)
         end do
         write(intermediateFileUnit, *)
         write(intermediateFileUnit, *) 'solar irradiance interpolated'
         do iwave = 1, wavelHRS%nwavel
           write(intermediateFileUnit, '(F12.7, ES20.10)') wavelHRS%wavel(iwave)+solarIrradianceS%wavelShift, &
                                                           solarIrradianceS%solIrrHR(iwave)
         end do
       end if ! verbose

       deallocate( w_fitwindow, solIrr_fitwindow, SDsolIrr_fitwindow, STAT = deallocStatus )
       if ( deallocStatus /= 0 ) then
         call logDebug('ERROR: deallocation failed')
         call logDebug('in subroutine getHRSolarIrradiance')
         call logDebug('in module readModule')
         call mystop(errS, 'stopped because deallocation failed')
         if (errorCheck(errS)) return
       end if

       ! fill high resolution radiance Ring spectrum (IR) , normal and differential
       if ( present( RRS_RingS) ) then

         ! calculate the convolution with the Raman lines and divide by the Raman scattering cross section
         call ConvoluteSpecRaman(errS, wavelMRS%wavel, RRS_RingS%temperature, &
                                 solarIrradianceS%solIrrMR, RRS_RingS%radRingHR)
         if (errorCheck(errS)) return
         ! calculate the sun-normalized radiance Ring spectrum (IR/F)
         RRS_RingS%RingHR(:) = RRS_RingS%radRingHR(:) / solarIrradianceS%solIrrMR(:)

       end if ! present( RRS_RingS)

       if ( verbose ) then
         if ( present( RRS_RingS) ) then
           write(intermediateFileUnit,*)
           write(intermediateFileUnit,*) 'output from getHRSolarIrradiance'
           write(intermediateFileUnit,*) '  wavelength     solarIrr      radRingHR       RingHR'
           do iwave = 1, wavelHRS%nwavel
             write(intermediateFileUnit,'(F12.6, 3ES15.7)') wavelHRS%wavel(iwave),            &
                                                            solarIrradianceS%solIrrHR(iwave), &
                                                            RRS_RingS%radRingHR(iwave),       &
                                                            RRS_RingS%RingHR(iwave)
           end do ! iwave
         end if ! present( RRS_RingS)
       end if ! verbose

       goto 99999

  40   continue
       ! error encountered while reading solar irradiance
       call logDebug('ERROR: solar irradiance spectrum does not cover fit window' )
       call logDebug('file name: '// trim(solarIrradianceS%solarIrrFilename))
       write(errS%temp,'(A,2F8.2)') 'extended fit window: ', startWavel, endWavel
       call errorAddLine(errS, errS%temp)
       call logDebug('unexpected End Of File encountered')
       call mystop(errS, '3 stopped because irradiance does not cover fit window')
       if (errorCheck(errS)) return

99999  continue
       call exit('getHRSolarIrradiance')

    end subroutine getHRSolarIrradiance


    subroutine getHRSolarIrradianceStatic(errS, staticS, wavelMRS, solarIrradianceS, RRS_RingS, wavelHRS)

     ! Read the solar spectrum and interpolate to the high-resolution grids for simulation and
     ! retrieval

       implicit none

       type(errorType),              intent(inout) :: errS
       type(staticType),             intent(in)    :: staticS
       type(wavelHRType),            intent(in)    :: wavelMRS
       type(SolarIrrType),           intent(inout) :: solarIrradianceS
       type(RRS_RingType), optional, intent(inout) :: RRS_RingS
       type(wavelHRType),  optional, intent(in)    :: wavelHRS


     ! local
       integer               :: i, iwave, nwavel, status_spline, statusSplint

       real(8)               :: startWavel, endWavel, w
       real(8)               :: solIrr_watt, solIrr_photons
       real(8), allocatable  :: w_fitwindow(:), solIrr_fitwindow(:)
       real(8), allocatable  :: SDsolIrr_fitwindow(:)

       logical               :: errorFitwindowCoveredStart

       integer               :: allocStatus, deallocStatus

       logical, parameter    :: verbose = .false.

       call enter('getHRSolarIrradianceStatic')

       ! initialize
       errorFitwindowCoveredStart = .true.

       ! determine the waveength range needed
       if ( present(wavelHRS) ) then
         startWavel = wavelHRS%wavel(1)
         endWavel   = wavelHRS%wavel(wavelHRS%nwavel)
       else
         startWavel = wavelMRS%wavel(1)
         endWavel   = wavelMRS%wavel(wavelMRS%nwavel)
       end if

       nwavel = 0
       do i = 1, size(staticS%hires_wavelength)
         if ( staticS%hires_wavelength(i) < startWavel ) then
           errorFitwindowCoveredStart = .false.
           cycle
         end if

         if ( (staticS%hires_wavelength(i) >= startWavel) .AND. (staticS%hires_wavelength(i) <= endWavel)) then
           nwavel = nwavel + 1
         else
           if ( staticS%hires_wavelength(i) > endWavel ) exit  ! exit do loop
         end if
       end do

       if ( errorFitwindowCoveredStart ) then
         call logDebug('ERROR: solar irradiance spectrum does not cover the start of the fit window')
         call logDebug('file name: '// trim(solarIrradianceS%solarIrrFilename))
         write(errS%temp,'(A,2F8.2)') 'extended fit window: ', startWavel, endWavel
         call errorAddLine(errS, errS%temp)
         call mystop(errS, '1 stopped because irradiance does not cover fit window')
         if (errorCheck(errS)) return
       end if

       if (nwavel <= 5) then
         call logDebug('ERROR: Only found 5 (or fewer) points in the fit window')
         call logDebug('file name: '// trim(solarIrradianceS%solarIrrFilename))
         write(errS%temp,'(A,2F8.2)') 'extended fit window: ', startWavel, endWavel
         call errorAddLine(errS, errS%temp)
         call mystop(errS, '1 stopped because highres irradiance does not contain enough points')
         if (errorCheck(errS)) return
       end if
       ! allocate memory space
       allocate( w_fitwindow(nwavel), solIrr_fitwindow(nwavel), SDsolIrr_fitwindow(nwavel), &
                 STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('ERROR: allocation failed')
         call logDebug('in subroutine getHRSolarIrradianceStatic')
         call logDebug('in module readModule')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       iwave = 0
       do i = 1, size(staticS%hires_wavelength)
         w = staticS%hires_wavelength(i)
         solIrr_photons = staticS%hires_wavelength(i)
         if ( (w >= startWavel) .AND. (w <= endWavel)) then
           iwave = iwave + 1
           w_fitwindow(iwave)      = w
           solIrr_fitwindow(iwave) = solIrr_photons
         else
           if ( w > endWavel ) exit  ! exit do loop
         end if
       end do

       ! prepare for spline interpolation (second order derivatives)
       call spline(errS, w_fitwindow, solIrr_fitwindow, SDsolIrr_fitwindow, status_spline)
       if (errorCheck(errS)) return

       do iwave = 1, wavelMRS%nwavel
         solarIrradianceS%solIrrMR(iwave) = splint(errS, w_fitwindow, solIrr_fitwindow, SDsolIrr_fitwindow, &
                        wavelMRS%wavel(iwave)+solarIrradianceS%wavelShift, statusSplint)
!         solarIrradianceS%solIrrMR(iwave) = splintLin(w_fitwindow, solIrr_fitwindow,  &
!                        wavelMRS%wavel(iwave)+solarIrradianceS%wavelShift, statusSplint)
! JdH Debug make solar irradiance a constant as function of the wavelength
!          solarIrradianceS%solIrrMR(iwave) =  solarIrradianceS%solIrrMR(1)
       end do

       if ( present(wavelHRS) ) then
         do iwave = 1, wavelHRS%nwavel
           solarIrradianceS%solIrrHR(iwave) = splint(errS, w_fitwindow, solIrr_fitwindow, SDsolIrr_fitwindow, &
                          wavelHRS%wavel(iwave)+solarIrradianceS%wavelShift, statusSplint)
!             solarIrradianceS%solIrrHR(iwave) = splintLin(w_fitwindow, solIrr_fitwindow,  &
!                           wavelHRS%wavel(iwave)+solarIrradianceS%wavelShift, statusSplint)
! JdH Debug make solar irradiance a constant as function of the wavelength
!          solarIrradianceS%solIrrHR(iwave) =  solarIrradianceS%solIrrHR(1)
         end do
       end if

       if ( verbose ) then
         write(intermediateFileUnit, *)
         write(intermediateFileUnit, *) 'solar irradiance read'
         do iwave = 1, nwavel
           write(intermediateFileUnit, '(F12.7, ES20.10)') w_fitwindow(iwave),  solIrr_fitwindow(iwave)
         end do
         write(intermediateFileUnit, *)
         write(intermediateFileUnit, *) 'solar irradiance interpolated'
         do iwave = 1, wavelHRS%nwavel
           write(intermediateFileUnit, '(F12.7, ES20.10)') wavelHRS%wavel(iwave)+solarIrradianceS%wavelShift, &
                                                           solarIrradianceS%solIrrHR(iwave)
         end do
       end if ! verbose

       deallocate( w_fitwindow, solIrr_fitwindow, SDsolIrr_fitwindow, STAT = deallocStatus )
       if ( deallocStatus /= 0 ) then
         call logDebug('ERROR: deallocation failed')
         call logDebug('in subroutine getHRSolarIrradianceStatic')
         call logDebug('in module readModule')
         call mystop(errS, 'stopped because deallocation failed')
         if (errorCheck(errS)) return
       end if

       ! fill high resolution radiance Ring spectrum (IR) , normal and differential
       if ( present( RRS_RingS) ) then

         ! calculate the convolution with the Raman lines and divide by the Raman scattering cross section
         call ConvoluteSpecRaman(errS, wavelMRS%wavel, RRS_RingS%temperature, &
                                 solarIrradianceS%solIrrMR, RRS_RingS%radRingHR)
         if (errorCheck(errS)) return
         ! calculate the sun-normalized radiance Ring spectrum (IR/F)
         RRS_RingS%RingHR(:) = RRS_RingS%radRingHR(:) / solarIrradianceS%solIrrMR(:)

       end if ! present( RRS_RingS)

       if ( verbose ) then
         if ( present( RRS_RingS) ) then
           write(intermediateFileUnit,*)
           write(intermediateFileUnit,*) 'output from getHRSolarIrradiance'
           write(intermediateFileUnit,*) '  wavelength     solarIrr      radRingHR       RingHR'
           do iwave = 1, wavelHRS%nwavel
             write(intermediateFileUnit,'(F12.6, 3ES15.7)') wavelHRS%wavel(iwave),            &
                                                            solarIrradianceS%solIrrHR(iwave), &
                                                            RRS_RingS%radRingHR(iwave),       &
                                                            RRS_RingS%RingHR(iwave)
           end do ! iwave
         end if ! present( RRS_RingS)
       end if ! verbose

       call exit('getHRSolarIrradianceStatic')

    end subroutine getHRSolarIrradianceStatic



    subroutine getRadianceRefSpectrumfromFile (errS, wavelHRS, earthRadianceS )

     ! Read the radiance reference spectrum from file and interpolate to the instrument spectral grid

      implicit none

      type(errorType),           intent(inout) :: errS
      type(wavelHRType),         intent(in)    :: wavelHRS
      type(EarthRadianceType),   intent(inout) :: earthRadianceS

     ! local
       integer               :: openErrorRefSpecFile
       integer               :: i, iwave, nwavel, statusSplint, nheader
       real(8)               :: startWavel, endWavel, w
       real(8)               :: refRadiance
       real(8), allocatable  :: w_fitwindow(:), refRadiance_fitwindow(:)
       real(8), allocatable  :: SDrefRadiance_fitwindow(:)
       logical               :: errorFitwindowCoveredStart
       integer               :: allocStatus, deallocStatus
       type(file_type)       :: file
       integer               :: status
       character(len=300)    :: line
       logical, parameter    :: verbose    = .false.

       call enter('getRadianceRefSpectrumfromFile')

       ! initialize
       errorFitwindowCoveredStart = .true.

       ! determine the waveength range needed
       startWavel = wavelHRS%wavel(1)
       endWavel   = wavelHRS%wavel(wavelHRS%nwavel)

       if (verbose ) then
         write(intermediateFileUnit,'(A)') 'start and end wavelength'
         write(intermediateFileUnit,'(2F12.6)') startWavel, endWavel
       end if

       ! open file

       openErrorRefSpecFile = load_file(earthRadianceS%radianceRefFileName, file)
       if (openErrorRefSpecFile /= 0) then
         call logDebug('ERROR in getRadianceRefSpectrumfromFile: failed to open file' )
         call logDebug('earthRadianceS%radianceRefFileName: '//trim(earthRadianceS%radianceRefFileName))
         call mystop(errS, 'stopped because of reference radiance file error')
         if (errorCheck(errS)) return
       end if

       ! read away header lines
       status = file_readline(file, line)
       if (status /= 0) goto 40
       read(line, *) nheader
       do i = 1, nheader
         status = file_readline(file, line)
         if (status /= 0) goto 40
       end do

       ! determine the number of wavelengths needed for the current fit window
       nwavel = 0
       do
         status = file_readline(file, line)
         if (status /= 0) goto 40
         read(line, *, end=40) w

         if (w < startWavel) then
           errorFitwindowCoveredStart = .false.
           cycle  ! read next line
         end if

         if ((w > startWavel) .AND. (w < endWavel)) then
           nwavel = nwavel + 1
         end if
         if (w > endWavel) exit  ! exit do loop
       end do

       if ( errorFitwindowCoveredStart ) then
         call logDebug('ERROR: radiance reference spectrum does not cover the start of the fit window')
         call logDebug('file name: '// trim(earthRadianceS%radianceRefFileName))
         write(errS%temp,'(A,2F8.2)') 'fit window: ', startWavel, endWavel
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped in readModule: getRadianceRefSpectrumfromFile')
         if (errorCheck(errS)) return
       end if

       if ( nwavel < 2 ) then
         call logDebug('ERROR: less than 2 wavelengths found in fit window')
         call logDebug('for radiance reference spectrum ')
         call logDebug('CAN NOT USE LINEAR INTERPOLATION ')
         call logDebug('file name: '// trim(earthRadianceS%radianceRefFileName))
         write(errS%temp,'(A,2F8.2)') 'fit window: ', startWavel, endWavel
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped because less than 2 wavelengths were found')
         if (errorCheck(errS)) return
       end if

       ! allocate memory space
       allocate( w_fitwindow(nwavel), refRadiance_fitwindow(nwavel), SDrefRadiance_fitwindow(nwavel), STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('ERROR: allocation failed')
         call logDebug('in subroutine getRadianceRefSpectrumfromFile')
         call logDebug('in module readModule')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       file%cursor = 1

       ! read away header lines
       status = file_readline(file, line)
       if (status /= 0) goto 40
       read(line, *) nheader
       do i = 1, nheader
         status = file_readline(file, line)
         if (status /= 0) goto 40
       end do

       iwave = 0
       refRadiance = 0.0d0
       do
         status = file_readline(file, line)
         if (status /= 0) goto 40
         read(line, *, end=40) w, refRadiance

         if ((w > startWavel) .AND. (w < endWavel)) then
           iwave = iwave + 1
           w_fitwindow(iwave)      = w
           refRadiance_fitwindow(iwave) = refRadiance
         ! write(intermediateFileUnit,'(F10.4, E20.10)') w, refRadiance
         else
           if (w > endWavel) exit  ! exit do loop
         end if
       end do

       ! Use linear interpolation to get the references spectrum on the high-resolution
       ! wavelength grid so that it can be convoluted with the slit function. Note that
       ! for line absorption spectra the convolution with the slit function can be
       ! important for defining the signal to noise ratio.
       do iwave = 1, wavelHRS%nwavel
         earthRadianceS%radianceRefHR(iwave) = splintLin(errS, w_fitwindow, refRadiance_fitwindow,  &
                        wavelHRS%wavel(iwave), statusSplint)
       end do

       if (verbose ) then
         write(intermediateFileUnit,'(A)') 'wavelength and high resolution reference radiance spectrum'
         do iwave = 1, wavelHRS%nwavel
           write(intermediateFileUnit,'(F12.6,ES14.5)') wavelHRS%wavel(iwave), earthRadianceS%radianceRefHR(iwave)
         end do
       end if

       deallocate( w_fitwindow, refRadiance_fitwindow, SDrefRadiance_fitwindow , STAT = deallocStatus )
       if ( deallocStatus /= 0 ) then
         call logDebug('ERROR: deallocation failed')
         call logDebug('in subroutine getRadianceRefSpectrumfromFile')
         call logDebug('in module readModule')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       goto 99999

       ! error encountered

  40   continue

       call logDebug('ERROR: radiance reference spectrum does not cover extended fit window' )
       call logDebug('extended fit window: (lambdaMin - 2*FWHM, lambdaMax + 2*FWHM)')
       call logDebug('extended fit window is required for convolution with slit function')
       call logDebug('file name: '// trim(earthRadianceS%radianceRefFileName))
       write(errS%temp,'(A,2F8.2)') 'fit window: ', startWavel, endWavel
       call errorAddLine(errS, errS%temp)
       call logDebug('unexpected End Of File encountered')
       call mystop(errS, 'stopped because EOF encountered')
       if (errorCheck(errS)) return

99999  continue
       call exit('getRadianceRefSpectrumfromFile')

    end subroutine getRadianceRefSpectrumfromFile


    subroutine getCharatisticBiasfromFile (errS, wavelInstrS, earthRadStrayLightS, earthRadianceS)

      ! Read the charactistic bias spectrum from file and interpolate to the instrument spectral grid

       implicit none

       type(errorType),           intent(inout) :: errS
       type(wavelInstrType),      intent(in)    :: wavelInstrS       
       type(straylightType),      intent(inout) :: earthRadStrayLightS  ! additive offset
       type(earthRadianceType),   intent(inout) :: earthRadianceS

     ! local
       integer               :: openErrorCharatisticBiasFile
       integer               :: i, iwave, nwavel, statusSplint, nheader
       real(8)               :: startWavel, endWavel, w
       real(8)               :: bias
       real(8), allocatable  :: w_fitwindow(:), bias_fitwindow(:)
       logical               :: errorFitwindowCoveredStart
       integer               :: allocStatus, deallocStatus
       type(file_type)       :: file
       integer               :: status
       character(len=300)    :: line
       logical, parameter    :: verbose    = .true.

       call enter('getCharatisticBiasfromFile')

       ! initialize
       errorFitwindowCoveredStart = .true.

       ! determine the waveength range needed
       startWavel = wavelInstrS%wavel(1)
       endWavel   = wavelInstrS%wavel(wavelInstrS%nwavel)

       if (verbose ) then
         write(intermediateFileUnit,'(A)') 'start and end wavelength'
         write(intermediateFileUnit,'(2F12.6)') startWavel, endWavel
       end if

       ! open file

       openErrorCharatisticBiasFile = load_file(earthRadStrayLightS%fileNameCharacteristicBias, file)
       if (openErrorCharatisticBiasFile /= 0) then
         call logDebug('ERROR in getCharatisticBiasfromFile: failed to open file' )
         call logDebug(trim(earthRadStrayLightS%fileNameCharacteristicBias))
         call mystop(errS, 'stopped because of characteristic bias file error')
         if (errorCheck(errS)) return
       end if

       ! read away header lines
       status = file_readline(file, line)
       if (status /= 0) goto 40
       read(line, *) nheader
       do i = 1, nheader
         status = file_readline(file, line)
         if (status /= 0) goto 40
       end do

       ! determine the number of wavelengths needed for the current fit window
       nwavel = 0
       do
         status = file_readline(file, line)
         if (status /= 0) goto 40
         read(line, *, end=40) w

         if (w < startWavel) then
           errorFitwindowCoveredStart = .false.
           cycle  ! read next line
         end if

         if ((w > startWavel) .AND. (w < endWavel)) then
           nwavel = nwavel + 1
         end if
         if (w > endWavel) exit  ! exit do loop
       end do

       if ( errorFitwindowCoveredStart ) then
         call logDebug('ERROR: characteristic bias file does not cover the start of the fit window')
         call logDebug('file name: '// trim(earthRadStrayLightS%fileNameCharacteristicBias))
         write(errS%temp,'(A,2F8.2)') 'fit window: ', startWavel, endWavel
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped in readModule: getCharatisticBiasfromFile')
         if (errorCheck(errS)) return
       end if

       if ( nwavel < 2 ) then
         call logDebug('ERROR: less than 2 wavelengths found in fit window')
         call logDebug('for radiance reference spectrum ')
         call logDebug('CAN NOT USE LINEAR INTERPOLATION ')
         call logDebug('file name: '// trim(earthRadStrayLightS%fileNameCharacteristicBias))
         write(errS%temp,'(A,2F8.2)') 'fit window: ', startWavel, endWavel
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped because less than 2 wavelengths were found')
         if (errorCheck(errS)) return
       end if

       ! allocate memory space
       allocate( w_fitwindow(nwavel), bias_fitwindow(nwavel), STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('ERROR: allocation failed')
         call logDebug('in subroutine getCharatisticBiasfromFile')
         call logDebug('in module readModule')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       file%cursor = 1

       ! read away header lines
       status = file_readline(file, line)
       if (status /= 0) goto 40
       read(line, *) nheader
       do i = 1, nheader
         status = file_readline(file, line)
         if (status /= 0) goto 40
       end do

       iwave = 0
       bias = 0.0d0
       do
         status = file_readline(file, line)
         if (status /= 0) goto 40
         read(line, *, end=40) w, bias

         if ((w > startWavel) .AND. (w < endWavel)) then
           iwave = iwave + 1
           w_fitwindow(iwave)      = w
           bias_fitwindow(iwave)   = bias
         ! write(intermediateFileUnit,'(F10.4, E20.10)') w, bias
         else
           if (w > endWavel) exit  ! exit do loop
         end if
       end do

! JdH Debug
!       if ( .not. associated( earthRadStrayLightS%radianceCB ) ) then
!         allocate( earthRadStrayLightS%radianceCB(wavelInstrS%nwavel), STAT = allocStatus )
!       end if
        if ( associated( earthRadStrayLightS%radianceCB )) then
          deallocate( earthRadStrayLightS%radianceCB, STAT = allocStatus )
        end if
        allocate( earthRadStrayLightS%radianceCB(wavelInstrS%nwavel), STAT = allocStatus )

       ! use linear interpolation to get the bias spectrum on the instrument wavelength grid
       do iwave = 1, wavelInstrS%nwavel
         earthRadStrayLightS%radianceCB(iwave) = splintLin(errS, w_fitwindow, bias_fitwindow,  &
                        wavelInstrS%wavel(iwave), statusSplint)
       end do

       if (verbose ) then
         write(intermediateFileUnit,'(A)') 'wavelength and charatistic bias earth radiance spectrum'
         do iwave = 1, wavelInstrS%nwavel
           write(intermediateFileUnit,'(F12.6,ES14.5)') wavelInstrS%wavel(iwave), &
                                        earthRadStrayLightS%radianceCB(iwave)
         end do
       end if

       deallocate( w_fitwindow, bias_fitwindow, STAT = deallocStatus )
       if ( deallocStatus /= 0 ) then
         call logDebug('ERROR: deallocation failed')
         call logDebug('in subroutine getRadianceRefSpectrumfromFile')
         call logDebug('in module readModule')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       goto 99999

       ! error encountered

  40   continue

       call logDebug('ERROR: characteristic bias on file does not cover fit window' )
       call logDebug('file name: '// trim(earthRadStrayLightS%fileNameCharacteristicBias))
       call logDebug('fit window: (lambdaMin, lambdaMax)')
       write(errS%temp,'(A,2F8.2)') 'fit window: ', startWavel, endWavel
       call errorAddLine(errS, errS%temp)
       call logDebug('unexpected End Of File encountered')
       call mystop(errS, 'stopped because EOF encountered')
       if (errorCheck(errS)) return

99999  continue
       call exit('getCharatisticBiasfromFile')

    end subroutine getCharatisticBiasfromFile



    subroutine getAbsorptionCoef (errS, wavelHRS, XsecS, Xsec0, Xsec1, Xsec2)

     ! Read the polynomial coefficients of the absorption cross section spectrum
     ! Calculates absorption cross section at the specified wavelength grid

     ! ASSUMES:
     !  first line of the file contains the scale factor
     !  Header lines in the input file ( number of header lines should be given in the second line )
     !  wavelength on file is given in nm and are ascending
     !  trace gas absorption cross section on file is given in terms of
     !  Xsec = scale_factor * (x0 + TC * x1 + TC * TC * x2)  cm2/mol
     !  where TC is the temperature in degree Celsius
     !
     !  for O2-O2 the unit is cm**5/molecule**2 where molecule is the O2 molecule, not O2-O2.

     use mathTools, only: splint, spline

     implicit none

     type(errorType), intent(inout) :: errS
     type(wavelHRType),    intent(in)  :: wavelHRS        ! spectral information
     type(XsecType),       intent(in)  :: XsecS           ! absorption cross sections

     real(8)             , intent(out) :: Xsec0(wavelHRS%nwavel)
     real(8)             , intent(out) :: Xsec1(wavelHRS%nwavel)
     real(8)             , intent(out) :: Xsec2(wavelHRS%nwavel)

     ! local
     integer               :: OpenErrorXsecFile
     integer               :: nheader
     integer               :: i, iwave, nwv, status_spline
     real(8)               :: w_read, x0_read, x1_read, x2_read
     real(8), allocatable  :: w(:), x0(:), x1(:), x2(:)
     real(8), allocatable  :: sdx0(:), sdx1(:), sdx2(:)
     real(8)               :: scale_factor
     logical               :: errorFitwindowCoveredStart
     integer               :: allocStatus, sumAllocStatus
     integer               :: deallocStatus, sumDeallocStatus
     type(file_type)       :: file
     integer               :: status
     character(len=300)    :: line

     call enter('getAbsorptionCoef')

     ! initialize
     errorFitwindowCoveredStart = .true.

     ! open file with absorption coefficients
     OpenErrorXsecFile = load_file(XsecS%XsectionFileName, file)
     if (OpenErrorXsecFile /= 0) then
       call logDebug('ERROR in getAbsorptionCoef')
       call logDebug('Error in opening file XsecS%XsectionFileName: '// XsecS%XsectionFileName)
       call mystop(errS, 'stopped in readModule: getAbsorptionCoef: error when opening file')
       if (errorCheck(errS)) return
     end if

     ! read scale factor
     status = file_readline(file, line)
     if (status /= 0) goto 40
     read(line, *) scale_factor

     ! read away header lines
     status = file_readline(file, line)
     if (status /= 0) goto 40
     read(line, *) nheader
     do i = 1, nheader                          ! read away header lines
       status = file_readline(file, line)
       if (status /= 0) goto 40
     end do

     ! determine the number of wavelengths within the fit window

     nwv = 0
     do
       status = file_readline(file, line)
       if (status /= 0) goto 40
       if (len(trim(line)) .le. 1) cycle
       read(line, *, end=40) w_read
       if (w_read < wavelHRS%wavel(1)) then
         errorFitwindowCoveredStart = .false.
         cycle  ! read next line
       end if
       if ((w_read >= wavelHRS%wavel(1)) .AND. (w_read <= wavelHRS%wavel(wavelHRS%nwavel))) then
         nwv = nwv + 1
       else
         if (w_read > wavelHRS%wavel(wavelHRS%nwavel)) exit  ! exit do loop
       end if
     end do

     if ( errorFitwindowCoveredStart ) then
       call logDebug('ERROR: absorption cross sections do not cover the start of the fit window')
       call logDebug('file name: '// trim(XsecS%XsectionFileName))
       write(errS%temp,'(A,2F8.2)') 'fit window: ', wavelHRS%wavel(1), wavelHRS%wavel(wavelHRS%nwavel)
       call errorAddLine(errS, errS%temp)
       call mystop(errS, 'stopped in readModule: getAbsorptionCoef: absorption Xsections do not cover start of the fit window')
       if (errorCheck(errS)) return
     end if

!     call logDebugI(  'ReadAbsorptionXsec: number of trace gas data is: ', nwv)

     ! claim memory now that the number of wavelengths in the fit window is known

     sumAllocStatus = 0

     allocate( w(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( x0(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( x1(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( x2(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( sdx0(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( sdx1(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( sdx2(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     if ( sumAllocStatus /= 0 ) then
       call logDebug('ERROR: allocation failed in subroutine getAbsorptionCoef')
       call logDebug('for w, x0, x1, x2, sdx0, sdx1, or sdx2')
       call mystop(errS, 'stopped because allocation failed')
       if (errorCheck(errS)) return
     end if

     file%cursor = 1


     ! repeat reading the file, but read, apart from the wavelengths, also the
     ! values of the polynomial coefficients and store them in arrays with the
     ! proper length

     ! read scale factor
     status = file_readline(file, line)
     if (status /= 0) goto 40
     read(line, *) scale_factor

     ! read away header lines
     status = file_readline(file, line)
     if (status /= 0) goto 40
     read(line, *) nheader
     do i = 1, nheader                          ! read away header lines
       status = file_readline(file, line)
       if (status /= 0) goto 40
     end do

     nwv = 0
     x0_read = 0.0d0
     x1_read = 0.0d0
     x2_read = 0.0d0
     do
       status = file_readline(file, line)
       if (status /= 0) goto 40
       if (len(trim(line)) .le. 1) cycle
       read(line, *, end=40, iostat=status) w_read, x0_read, x1_read, x2_read
       if ((w_read >= wavelHRS%wavel(1)) .AND. (w_read <= wavelHRS%wavel(wavelHRS%nwavel))) then
         nwv = nwv + 1
         w(nwv)  = w_read
         x0(nwv) = x0_read
         x1(nwv) = x1_read
         x2(nwv) = x2_read
!        write(intermediateFileUnit,'(4E20.10)') w_read, x0_read, x1_read, x2_read
       else
         if (w_read > wavelHRS%wavel(wavelHRS%nwavel) ) exit  ! exit do loop
       end if
     end do

     ! The absorption coefficients are now known on the wavelength grid that is specified
     ! in the file that was read. However, we need the values on the high-resolution
     ! wavelength grid and use cubic spline interpolation for that purpose.

     ! prepare for spline interpolation (calculate second order derivatives)
     call spline(errS, w, x0, sdx0, status_spline)
     if (errorCheck(errS)) return
     call spline(errS, w, x1, sdx1, status_spline)
     if (errorCheck(errS)) return
     call spline(errS, w, x2, sdx2, status_spline)
     if (errorCheck(errS)) return

     do iwave = 1, wavelHRS%nwavel
       Xsec0(iwave) = scale_factor * splint(errS, w, x0, sdx0, wavelHRS%wavel(iwave), status_spline)
       Xsec1(iwave) = scale_factor * splint(errS, w, x1, sdx1, wavelHRS%wavel(iwave), status_spline)
       Xsec2(iwave) = scale_factor * splint(errS, w, x2, sdx2, wavelHRS%wavel(iwave), status_spline)
     end do

     sumDeallocStatus = 0

     deallocate( w,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( x0,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( x1,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( x2,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( sdx0,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( sdx1,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( sdx2,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     if ( sumDeallocStatus /= 0 ) then
       call logDebug('ERROR: deallocation failed in subroutine getAbsorptionCoef')
       call logDebug('for w, x0, x1, x2, sdx0, sdx1, or sdx2')
       call mystop(errS, 'stopped because deallocation failed')
       if (errorCheck(errS)) return
     end if

     goto 99999

  40 continue

     call logDebug('ERROR: absorption cross sections do not cover the end of the fit window')
     call logDebug('file name: '// trim(XsecS%XsectionFileName))
     write(errS%temp,'(A,2F8.2)') 'fit window: ', wavelHRS%wavel(1), wavelHRS%wavel(wavelHRS%nwavel)
     call errorAddLine(errS, errS%temp)
     call logDebug('unexpected End Of File encountered')
     call mystop(errS, 'stopped because cross sections do not cover the fit window')
     if (errorCheck(errS)) return

99999 continue
     call exit('getAbsorptionCoef')

    end subroutine getAbsorptionCoef


    subroutine getAbsorptionCoefStatic(errS, staticS, name, wavelHRS, XsecS, Xsec0, Xsec1, Xsec2)

     ! Read the polynomial coefficients of the absorption cross section spectrum
     ! Calculates absorption cross section at the specified wavelength grid

     ! ASSUMES:
     !  first line of the file contains the scale factor
     !  Header lines in the input file ( number of header lines should be given in the second line )
     !  wavelength on file is given in nm and are ascending
     !  trace gas absorption cross section on file is given in terms of
     !  Xsec = scale_factor * (x0 + TC * x1 + TC * TC * x2)  cm2/mol
     !  where TC is the temperature in degree Celsius
     !
     !  for O2-O2 the unit is cm**5/molecule**2 where molecule is the O2 molecule, not O2-O2.

     use mathTools, only: splint, spline

     implicit none

     type(errorType),      intent(inout) :: errS
     type(staticType),     intent(in)    :: staticS
     character*(*),        intent(in)    :: name
     type(wavelHRType),    intent(in)    :: wavelHRS        ! spectral information
     type(XsecType),       intent(in)    :: XsecS           ! absorption cross sections

     real(8)             , intent(out)   :: Xsec0(wavelHRS%nwavel)
     real(8)             , intent(out)   :: Xsec1(wavelHRS%nwavel)
     real(8)             , intent(out)   :: Xsec2(wavelHRS%nwavel)

     ! local
     integer               :: i, iwave, nwv, status_spline

     real(8), allocatable  :: w(:), x0(:), x1(:), x2(:)
     real(8), allocatable  :: sdx0(:), sdx1(:), sdx2(:)

     logical               :: errorFitwindowCoveredStart

     integer               :: allocStatus, sumAllocStatus
     integer               :: deallocStatus, sumDeallocStatus
     integer               :: ixs

     call enter('getAbsorptionCoefStatic')

     ixs = 0
     do i = 1, staticS%xsnum
       if (trim(name) .eq. trim(staticS%xs(i)%name)) then
         ixs = i
         exit
       end if
     end do
     if (ixs .eq. 0) then
       call mystop(errS, 'stopped in readModule: getAbsorptionCoefStatic: cross section data ' // trim(name) // ' not found')
       if (errorCheck(errS)) return
       return
     end if

     ! initialize
     errorFitwindowCoveredStart = .true.

     ! determine the number of wavelengths within the fit window
     nwv = 0
     do i = 1, staticS%xs(ixs)%numwav
       if ( staticS%xs(ixs)%wavelength(i) < wavelHRS%wavel(1) ) then
         errorFitwindowCoveredStart = .false.
         cycle  ! read next line
       end if
       if ((staticS%xs(ixs)%wavelength(i) >= wavelHRS%wavel(1)) .AND. &
                (staticS%xs(ixs)%wavelength(i) <= wavelHRS%wavel(wavelHRS%nwavel))) then
         nwv = nwv + 1
       else
         if ( staticS%xs(ixs)%wavelength(i) > wavelHRS%wavel(wavelHRS%nwavel) ) exit  ! exit do loop
       end if
     end do

     if ( errorFitwindowCoveredStart ) then
       call logDebug('ERROR: absorption cross sections do not cover the start of the fit window')
       write(errS%temp,'(A,2F8.2)') 'fit window: ', wavelHRS%wavel(1), wavelHRS%wavel(wavelHRS%nwavel)
       call errorAddLine(errS, errS%temp)
       call mystop(errS, &
           'stopped in readModule: getAbsorptionCoefStatic: absorption Xsections do not cover start of the fit window')
       if (errorCheck(errS)) return
     end if

     call logDebugI('ReadAbsorptionXsec: number of trace gas data is: ', nwv)

     ! claim memory now that the number of wavelengths in the fit window is known

     sumAllocStatus = 0

     allocate( w(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( x0(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( x1(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( x2(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( sdx0(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( sdx1(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     allocate( sdx2(nwv),  STAT = allocStatus )
     sumAllocStatus = sumAllocStatus + allocStatus

     if ( sumAllocStatus /= 0 ) then
       call logDebug('ERROR: allocation failed in subroutine getAbsorptionCoefStatic')
       call logDebug('for w, x0, x1, x2, sdx0, sdx1, or sdx2')
       call mystop(errS, 'stopped because allocation failed')
       if (errorCheck(errS)) return
     end if

     nwv = 0
     do i = 1, staticS%xs(ixs)%numwav
       if ((staticS%xs(ixs)%wavelength(i) >= wavelHRS%wavel(1)) .AND. &
           (staticS%xs(ixs)%wavelength(i) <= wavelHRS%wavel(wavelHRS%nwavel)) ) then
         nwv = nwv + 1
         w(nwv)  = staticS%xs(ixs)%wavelength(i)
         x0(nwv) = staticS%xs(ixs)%a1(i)
         x1(nwv) = staticS%xs(ixs)%a2(i)
         x2(nwv) = staticS%xs(ixs)%a3(i)
       else
         if (staticS%xs(ixs)%wavelength(i) > wavelHRS%wavel(wavelHRS%nwavel) ) exit  ! exit do loop
       end if
     end do

     ! The absorption coefficients are now known on the wavelength grid that is specified
     ! in the file that was read. However, we need the values on the high-resolution
     ! wavelength grid and use cubic spline interpolation for that purpose.

     ! prepare for spline interpolation (calculate second order derivatives)
     call spline(errS, w, x0, sdx0, status_spline)
     if (errorCheck(errS)) return
     call spline(errS, w, x1, sdx1, status_spline)
     if (errorCheck(errS)) return
     call spline(errS, w, x2, sdx2, status_spline)
     if (errorCheck(errS)) return

     do iwave = 1, wavelHRS%nwavel
       Xsec0(iwave) = splint(errS, w, x0, sdx0, wavelHRS%wavel(iwave), status_spline)
       Xsec1(iwave) = splint(errS, w, x1, sdx1, wavelHRS%wavel(iwave), status_spline)
       Xsec2(iwave) = splint(errS, w, x2, sdx2, wavelHRS%wavel(iwave), status_spline)
     end do

     sumDeallocStatus = 0

     deallocate( w,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( x0,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( x1,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( x2,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( sdx0,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( sdx1,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     deallocate( sdx2,  STAT = deallocStatus )
     sumDeallocStatus = sumDeallocStatus + deallocStatus

     if ( sumDeallocStatus /= 0 ) then
       call logDebug('ERROR: deallocation failed in subroutine getAbsorptionCoefStatic')
       call logDebug('for w, x0, x1, x2, sdx0, sdx1, or sdx2')
       call mystop(errS, 'stopped because deallocation failed')
       if (errorCheck(errS)) return
     end if

     call exit('getAbsorptionCoefStatic')

    end subroutine getAbsorptionCoefStatic


    subroutine readAPcovFromFile(errS, traceGasS)

    ! This routine is called if the a-priori covariance is to be read from file.
    ! If there is more than one trace gas whose a-priori covariance is to be read from file,
    ! it is assumed that the file names stored in traceGasS%APcovarianceFileName differ.

    ! It is assumed that that covariances on file are the covariances for the logarithm of the
    ! volume mixing ratio. They are transformed here into covariances for the volume mixing ratio.

    ! It is also assumed that the altitudes [in km] for the a-priori covariance correspond with those
    ! in the configuration file for the trace gas considered. This is tested.

       implicit none

       type(errorType), intent(inout) :: errS
       type(traceGasType),  intent(inout) :: traceGasS

       integer             :: OpenErrorAPcovFile
       integer             :: i, ialt, jalt
       real(8)             :: altread(0:traceGasS%nalt)
       real(8)             :: cov_ln_vmr_AP(0:traceGasS%nalt, 0:traceGasS%nalt)
       integer             :: Nheader
       logical, parameter  :: verbose = .false.
       type(file_type)     :: file
       integer             :: status
       character(len=300)  :: line

       call enter('readAPcovFromFile')

       OpenErrorAPcovFile = load_file(traceGasS%APcovarianceFileName, file)
       if (OpenErrorAPcovFile /= 0) then
         call logDebug('ERROR in subroutine readAPcovFromFile: failed to open file')
         call logDebug('traceGasS%APcovarianceFileName: '// traceGasS%APcovarianceFileName)
         call mystop(errS, 'stopped because file with a-priori covariance data could not be opened')
         if (errorCheck(errS)) return
       end if

       status = file_readline(file, line)
       read(line, *) Nheader
       do i = 1, Nheader               ! read away header lines
         status = file_readline(file, line)
       end do
       status = file_readline(file, line) ! read line with altitudes and covariances
       do ialt = 0,traceGasS%nalt
         status = file_readline(file, line)
         read(line, *) altread(ialt), (cov_ln_vmr_AP(ialt, jalt), jalt = 0,traceGasS%nalt)
         if ( abs( altread(ialt) - traceGasS%alt(ialt) ) > 1.0d-3 ) then
           call logDebug('ERROR when reading a-priori covariance data from file')
           call logDebug('different altitude grids file name: '// traceGasS%APcovarianceFileName)
           call logDebug('altitude grid in configuration file differs from')
           call logDebug('altitude grid in file with a-priori covariance data')
           call logDebug('index altitude and altitudes that differ:')
           write(errS%temp,'(I4,2F12.5)') ialt, altread(ialt), traceGasS%alt(ialt)
           call errorAddLine(errS, errS%temp)
           call mystop(errS, 'stopped because of different altitude grids')
           if (errorCheck(errS)) return
         end if
       end do

       ! transform to vmr
       if ( traceGasS%useSaDiagFromFile ) then
          traceGasS%covVmrAP = 0.0d0
          do ialt = 0, traceGasS%nalt
            traceGasS%covVmrAP(ialt, ialt) =  cov_ln_vmr_AP(ialt, ialt) * traceGasS%vmrAP(ialt)**2
          end do
       else
         do ialt = 0, traceGasS%nalt
           do jalt  = 0, traceGasS%nalt
             traceGasS%covVmrAP(ialt, jalt) = &
                traceGasS%vmrAP(ialt) * cov_ln_vmr_AP(ialt, jalt) * traceGasS%vmrAP(jalt)
           end do
         end do
       end if

       if (verbose) then
         write(intermediateFileUnit,*)
         write(intermediateFileUnit,*) 'a-priori covariance read from file'
         do ialt = 0, traceGasS%nalt
          write(intermediateFileUnit, '(F10.3,100E15.7)' ) altread(ialt), &
                                (cov_ln_vmr_AP(ialt, jalt), jalt = 0, traceGasS%nalt)
         end do
         write(intermediateFileUnit,*)
         write(intermediateFileUnit,*) 'a-priori covariance read from file and transformed to Sa_vmr'
         do ialt = 0, traceGasS%nalt
          write(intermediateFileUnit, '(F10.3,100E15.7)' ) altread(ialt), &
                      (traceGasS%covVmrAP(ialt, jalt), jalt = 0, traceGasS%nalt)
         end do
       end if

       call exit('readAPcovFromFile')

    end subroutine readAPcovFromFile


    subroutine readPolarizationCorrectionFile (errS, numSpectrBands, polCorrectionS, &
                                               wavelInstrRadRetrS)

      ! Read the polarization correction file and fill the polarization correction structure
      ! The polarization correction listed in the file may have a wavelegth grid that differs
      ! from the wavelenth grid used in the retrieval. Cubic spline interpolation is used to
      ! obtain the correction on the proper wavelength grid. The interpolation is not part
      ! of this subroutine, but calculating the derivatives for spline interpolation is part
      ! of the subroutine (fill SDCorrection in polCorrectionS)

      ! The structure wavelInstrRadRetrS is used to test whether the polarization correction
      ! data read from file covers the fit window. If not, an error message is generated and
      ! processing stops.

      implicit none

      type(errorType),         intent(inout) :: errS
      integer,                 intent(in)    :: numSpectrBands
      type(polCorrectionType), intent(inout) :: polCorrectionS(numSpectrBands)
      type(wavelInstrType),    intent(in)    :: wavelInstrRadRetrS(numSpectrBands)

      ! local
      integer, parameter   :: maxNumWavelengths = 1200
      integer, parameter   :: maxNumChannels    = 10
      integer :: openErrorPolCorrectionFile
      integer :: iwave, status_spline, allocStatus, iband
      integer :: ichannel, nchannel
      integer :: nwavel(maxNumChannels)
      real    :: startWavel, endWavel, value
      real    :: correction(maxNumWavelengths, maxNumChannels)
      real    :: wavelength(maxNumWavelengths, maxNumChannels)
      real(8), allocatable :: SDcorrection(:)  ! help array for spline interpolation
      character(LEN = 40)  :: identifier
      logical              :: errorFitwindowCoveredStart
      logical              :: errorFitwindowCoveredEnd
      logical              :: eof
      type(file_type)      :: file
      integer              :: status
      character(len=300)   :: line

      call enter('readPolarizationCorrectionFile')

      ! open file (it is assumed that the file name is the same for each spectral band)

      openErrorPolCorrectionFile = load_file(polCorrectionS(1)%polarizationCorrectionFileName, file)
      if (openErrorPolCorrectionFile /= 0) then
      call logDebug('ERROR in opening polarization correction file')
        write(errS%temp,*) 'polCorrectionS%polarizationCorrectionFileName: ', &
                     polCorrectionS(1)%polarizationCorrectionFileName
        call errorAddLine(errS, errS%temp)
        call mystop(errS, 'stopped because of open error')
        if (errorCheck(errS)) return
      end if

      ! set options for the parser
      !call input_options(errS, file, echo_lines=.false.,skip_blank_lines=.true.,error_flag=1, &
      !                   concat_string='&')
      !if (errorCheck(errS)) return

      ! initialize
      errorFitwindowCoveredStart = .true.
      errorFitwindowCoveredEnd   = .true.
      ichannel     = 1
      nchannel     = 1

      do
        call readline(errS, eof, file)
        if (errorCheck(errS)) return
        if (eof) exit

        call reada(errS, file, identifier)
        if (errorCheck(errS)) return

        select case (identifier)

           case('start_channel')
             iwave = 1

           case('end_channel')

             nchannel = ichannel
             ichannel = ichannel + 1

             if ( ichannel > maxNumChannels + 1) then
               call logDebug('ERROR subroutine readPolarizationCorrectionFile: too many channels specified')
               call logDebug('file name: '//trim(polCorrectionS(1)%polarizationCorrectionFileName))
               call logDebugI('maximum number = ', maxNumChannels)
               call logDebug('increase maxNumChannels')
               call logDebug('in subroutine readPolarizationCorrectionFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because too many channels are specified')
               if (errorCheck(errS)) return
             end if

           case('corr')

             if ( file%parser%nitems /= 3 ) then
               call logDebug('ERROR in polarization correction file: incorect number of items on a line')
               call logDebug('file name: '//trim(polCorrectionS(1)%polarizationCorrectionFileName))
               write(errS%temp,*) 'for channel and wavelength numbers', ichannel, iwave
               call errorAddLine(errS, errS%temp)
               call logDebug('detected by subroutine readPolarizationCorrectionFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because of error in external reflectance file')
               if (errorCheck(errS)) return
             end if

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             wavelength(iwave, ichannel)  = value

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             correction(iwave, ichannel) = value

             nwavel(ichannel) = iwave
             iwave            = iwave + 1

             if ( iwave > maxNumWavelengths ) then
               call logDebug('ERROR in reading polarization correction data from file')
               call logDebug('file name: '//trim(polCorrectionS(1)%polarizationCorrectionFileName))
               call logDebug('too many wavelengths specified')
               call logDebugI('maximum number = ', maxNumWavelengths)
               call logDebug('increase maxNumWavelengths')
               call logDebug('in subroutine readPolarizationCorrectionFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because too many wavelengths are specified')
               if (errorCheck(errS)) return
             end if

           case('end_file')

             exit ! exit do loop

           case default

             call logDebug("ERROR in file with polarization correction data")
             call logDebug('file name: '//trim(polCorrectionS(1)%polarizationCorrectionFileName))
             call logDebug("identifier is not recognized: "//trim(identifier))
             call logDebug("possible identifiers are:")
             call logDebug('start_channel')
             call logDebug('corr')
             call logDebug('end_channel')
             call logDebug('end_file')
             call mystop(errS, 'stopped because identifier is not recognized')
             if (errorCheck(errS)) return

        end select

      end do


      ! determine the wavelength range needed per spectral band

      do iband = 1, numSpectrBands
        ! determine the wavelength range needed
        startWavel = wavelInstrRadRetrS(iband)%wavel(1)
        endWavel   = wavelInstrRadRetrS(iband)%wavel(wavelInstrRadRetrS(iband)%nwavel)

        if ( wavelength(1, iband) <= startWavel ) then
            errorFitwindowCoveredStart = .false.
        end if
        if ( wavelength(nwavel(iband), iband) >= endWavel ) then
          errorFitwindowCoveredEnd = .false.
        end if

        if ( errorFitwindowCoveredStart .or. errorFitwindowCoveredEnd ) then
        call logDebug('ERROR: polarization correction file does not cover fit window')
          write(errS%temp,'(A,2F8.2)') 'fit window: ', startWavel, endWavel
          call errorAddLine(errS, errS%temp)
          call mystop(errS, 'stopped because polarization correction does not cover the fit window')
          if (errorCheck(errS)) return
        end if

        ! allocate memory space
        call freeMemPolCorrectionS(errS, polCorrectionS(iband))
        ! initialize
        polCorrectionS(iband)%nwavel           = nwavel(iband)
        polCorrectionS(iband)%nsurfaceAlbedo   = 1
        polCorrectionS(iband)%nmu              = 1
        polCorrectionS(iband)%nmu0             = 1
        polCorrectionS(iband)%nozoneColumn     = 1
        polCorrectionS(iband)%nsurfacePressure = 1
        polCorrectionS(iband)%nlatitude        = 1
        ! perform allocation
        call claimMemPolCorrectionS(errS, polCorrectionS(iband))
        if (errorCheck(errS)) return

        ! fill values in polCorrectionS
        do iwave = 1, nwavel(iband)
          polCorrectionS(iband)%wavel(iwave)      = wavelength(iwave, iband)
          polCorrectionS(iband)%correction(iwave) = correction(iwave, iband)
        end do

        allocate( SDcorrection(nwavel(iband)), STAT=allocStatus )
        if (allocStatus /= 0 ) then
          call logDebug('ERROR in reading polarization correction data from file')
          call logDebug('allocation of SDcorrection failed')
          call logDebug('in subroutine readPolarizationCorrectionFile')
          call logDebug('in module readModule')
          call mystop(errS, 'stopped because allocation failed')
          if (errorCheck(errS)) return
        end if
        ! prepare for spline interpolation (second order derivatives)
        call spline(errS, dble(polCorrectionS(iband)%wavel), &
                          dble(polCorrectionS(iband)%correction), &
                          SDCorrection, status_spline)
        if (errorCheck(errS)) return

        polCorrectionS(iband)%SDCorrection(:) = real(SDCorrection(:))

        deallocate( SDcorrection, STAT=allocStatus )
        if (allocStatus /= 0 ) then
          call logDebug('ERROR in reading polarization correction data from file')
          call logDebug('deallocation of SDcorrection failed')
          call logDebug('in subroutine readPolarizationCorrectionFile')
          call logDebug('in module readModule')
          call mystop(errS, 'stopped because deallocation failed')
          if (errorCheck(errS)) return
        end if

      end do ! iband

      call exit('readPolarizationCorrectionFile')

    end subroutine readPolarizationCorrectionFile


    subroutine readScatteringModels(errS, mieScatS)

      ! read coefficients for the expansion of generalized spherical functions
      ! and associated parameters from file and store them in the structure mieScatS

      implicit none

      type(errorType), intent(inout) :: errS
      type(mieScatType), intent(inout)   :: mieScatS

      ! local
      integer :: openErrorMieScatFile
      integer :: iwave, iCoeff, nheader, iheader, iType, index
      type(file_type) :: file
      integer :: status
      character(len=300) :: line

      call enter('readScatteringModels')

      ! open file

      openErrorMieScatFile = load_file(mieScatS%fileNameExpCoef, file)
      if (openErrorMieScatFile /= 0) then
        call logDebug('ERROR in opening Mie scattering file')
        call logDebug('mieScatS%fileNameExpCoef: '// trim(mieScatS%fileNameExpCoef))
        call mystop(errS, 'stopped because of open error')
        if (errorCheck(errS)) return
      end if

      ! read number of header lines and skip those linea
      status = file_readline(file, line)
      read(line, *) nheader
      do iheader = 1, nheader
        status = file_readline(file, line)
      end do
      status = file_readline(file, line)
      read(line,*) mieScatS%numExpCoef
      status = file_readline(file, line)
      read(line,*) mieScatS%nwavel
      status = file_readline(file, line)
      read(line,*) mieScatS%numberParticles
      status = file_readline(file, line)
      read(line,*) mieScatS%volume
      status = file_readline(file, line)
      read(line,*) mieScatS%reff
      status = file_readline(file, line)
      read(line,*) mieScatS%Cext550nm

      ! allocate memory
      call claimMemMieScatS(errS, mieScatS)
      if (errorCheck(errS)) return

      ! fill arrays
      do iwave = 1, mieScatS%nwavel
        status = file_readline(file, line)
        status = file_readline(file, line)
        read(line,'(F15.8)')  mieScatS%wavel(iwave)
        status = file_readline(file, line)
        read(line,'(F15.8)')  mieScatS%Cext(iwave)
        status = file_readline(file, line)
        read(line,'(F15.8)')  mieScatS%a(iwave)
        status = file_readline(file, line)
        status = file_readline(file, line)
        do iCoeff = 0, mieScatS%numExpCoef
          status = file_readline(file, line)
          read(line,'(I4,6ES15.6)') index, (mieScatS%expCoef(iType,iCoeff,iwave), iType = 1,6 )
        end do

        ! remove normalization
        mieScatS%Cext(iwave) = mieScatS%Cext(iwave) * mieScatS%Cext550nm
      end do ! iwave

      call exit('readScatteringModels')

    end subroutine readScatteringModels


    subroutine readGOME2slitfunctions(errS, slitFunctionSpecsS)

      ! This subroutine reads data for GOME 2 slit functions.
      ! The slit functions have been measured for nominal wavelengths
      ! between 242 and 769 nm

      ! identification of the slit function is based on
      ! the nominal wavelength (area below the slit functions is the
      ! same left and right form the nominal wavelength) and the
      ! spectal band number
      ! Band 1 = 242.00 - 312.99 nm
      ! Band 2 = 313.00 - 399.99 nm
      ! Band 3 = 400.00 - 599.99 nm
      ! Band 4 = 600.00 - 770.00 nm
      ! Make sure that no data points are used
      ! with a spectral band number and wavelength that does not conform
      ! to these intervals, othewise a wrong slit function is used.

      implicit none

      type(errorType), intent(inout) :: errS
      type(slitFunctionType), intent(inout)   :: slitFunctionSpecsS

      ! local
      logical :: eof
      integer :: openErrorSlitFunctionFile
      integer :: iNominal, nNominal, iSlit, nSlit
      real(8) :: wavelenght
      integer :: allocStatus
      integer :: sumAllocStatus
      type(file_type) :: file
      integer :: status
      character(len=300) :: line

      call enter('readGOME2slitfunctions')

      ! open file

      openErrorSlitFunctionFile = load_file(slitFunctionSpecsS%slitfunctionFileName, file)
      if (openErrorSlitFunctionFile /= 0) then
        call logDebug('ERROR in opening slit function file')
        write(errS%temp,*) 'radianceS%slitFunctionSpecsS%slitfunctionFileName: ', &
                     trim(slitFunctionSpecsS%slitfunctionFileName)
        call errorAddLine(errS, errS%temp)
        call mystop(errS, 'stopped because of open error')
        if (errorCheck(errS)) return
      end if

      ! set options for the parser
      !call input_options(errS, file, echo_lines=.false.,skip_blank_lines=.true.,error_flag=1, &
      !                   concat_string='&')
      !if (errorCheck(errS)) return

      ! initialize
      nNominal = 0
      nSlit    = 0

      ! read the number of nominal wavelengths
      do ! reading loop
        call readline(errS, eof, file)
        if (errorCheck(errS)) return
        if (eof) exit
        if ( file%parser%nitems > 3 ) then
          nNominal = nNominal + 1
        end if
      end do

      slitFunctionSpecsS%nwavelNominal = nNominal

      file%cursor = 1

      ! read the number entries for the slit function
      do ! reading loop
        call readline(errS, eof, file)
        if (errorCheck(errS)) return
        if (eof) exit
        if ( file%parser%nitems == 3 ) then
          nSlit = nSlit + 1
        end if
        if ( (file%parser%nitems > 3) .and. (nSlit > 0) ) exit
      end do

      slitFunctionSpecsS%nwavelListed = nSlit

      ! allocate memory for the slit function

      sumAllocStatus = 0

      allocate( slitFunctionSpecsS%bandNumber(slitFunctionSpecsS%nwavelNominal), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( slitFunctionSpecsS%pixelNumber(slitFunctionSpecsS%nwavelNominal), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( slitFunctionSpecsS%wavelNominal(slitFunctionSpecsS%nwavelNominal), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( slitFunctionSpecsS%deltawavelListed(slitFunctionSpecsS%nwavelListed, &
                                                    slitFunctionSpecsS%nwavelNominal), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( slitFunctionSpecsS%SlitFunctionTable(slitFunctionSpecsS%nwavelListed, &
                                                    slitFunctionSpecsS%nwavelNominal), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      if (sumAllocStatus /= 0) then
        Print *, 'readGOME2slitfunctions has nonzero sumAllocStatus. Alloc Failed'
        call mystop(errS, 'readGOME2slitfunctions has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
      end if

      ! read values for the slit function

      file%cursor = 1

      ! read values for the slit function

      ! initialize
      iNominal       = 0
      iSlit          = 0
      slitFunctionSpecsS%deltawavelListed  = 0.0d0
      slitFunctionSpecsS%SlitFunctionTable = 0.0d0

      do ! reading loop
        call readline(errS, eof, file)
        if (errorCheck(errS)) return
        if (eof) exit
        if ( file%parser%nitems > 3 ) then
          iNominal = iNominal + 1
          call readf(errS, file, slitFunctionSpecsS%wavelNominal(iNominal) )
          if (errorCheck(errS)) return
          call readi(errS, file, slitFunctionSpecsS%bandNumber  (iNominal) )
          if (errorCheck(errS)) return
          call readi(errS, file, slitFunctionSpecsS%pixelNumber (iNominal) )
          if (errorCheck(errS)) return
          iSlit          = 0  ! initialize slit index
        else
          iSlit = iSlit + 1
          call readf(errS, file, wavelenght ) ! wavelength is not stored
          call readf(errS, file, slitFunctionSpecsS%deltawavelListed (iSlit, iNominal) )
          if (errorCheck(errS)) return
          call readf(errS, file, slitFunctionSpecsS%SlitFunctionTable(iSlit, iNominal) )
          if (errorCheck(errS)) return
        end if

      end do

      call exit('readGOME2slitfunctions')

    end subroutine readGOME2slitfunctions


    subroutine read_TOMS_V8_climatology(errS, O3ClimS)

      ! read the TOMS V8 climatology (ozone profile and temperature)
      ! interpolate temperatures to pressure levels in

      implicit none

      type(errorType), intent(inout) :: errS
      type(O3ClimType),   intent(inout) :: O3ClimS

      !local
      integer :: i, m, n, l, p
      integer :: openErrorTOMS_V8_Temp_File
      integer :: openErrorTOMS_V8_O3_File
      logical, parameter :: verbose = .false.
      type(file_type) :: file
      integer :: status
      character(len=300) :: line

      call enter('read_TOMS_V8_climatology')

      ! initialize
      O3ClimS%dim_months_clim        = 12
      O3ClimS%dim_lat_clim           = 18
      O3ClimS%dim_total_column_clim  = 10
      O3ClimS%dim_pressures_clim     = 11
      O3ClimS%dim_pressures_vmr      = 12

      if ( .not. associated( O3ClimS%nodes_months ) ) then
        call claimMemO3ClimS(errS, O3ClimS)
        if (errorCheck(errS)) return
      else
        return ! apperently the climatology has been read already
      end if

      do i = 1, O3ClimS%dim_months_clim
        O3ClimS%nodes_months(i) =  i
      end do

      do i = 1, O3ClimS%dim_lat_clim
        O3ClimS%nodes_latitudes(i) =  -85.0d0 + 10.0d0 * (i - 1)
      end do

      do i = 1, O3ClimS%dim_total_column_clim
        O3ClimS%nodes_total_column(i) =  125.0d0 + 50.0d0 * (i - 1)
      end do

      do i = 1, O3ClimS%dim_pressures_clim
        O3ClimS%nodes_pressures(i) =  2.0d0**(-i + 1)*1013.25d0
      end do

      ! open temperature file
      openErrorTOMS_V8_Temp_File = load_file(O3ClimS%temperatureClimFileName, file)
      if (openErrorTOMS_V8_Temp_File /= 0) then
        call logDebug('ERROR in opening TOMS V8 Temp file')
        write(errS%temp,*) 'O3ClimS%temperatureClimFileName: ', &
                     trim( O3ClimS%temperatureClimFileName)
        call errorAddLine(errS, errS%temp)
        call mystop(errS, 'stopped because of open error')
        if (errorCheck(errS)) return
      end if

      ! read temperature climatology
      do m = 1, O3ClimS%dim_months_clim  ! m = month index  1 = Jan, 2 = Febr, ..., 12 = Dec
        status = file_readline(file, line) ! read away header for the months and latitudes
        do l = 1, O3ClimS%dim_lat_clim   ! l = latitude index
          status = file_readline(file, line)
          read(line,'(1X,11F7.1)') &
            (O3ClimS%clim_temperature(p, l, m), p = 1,O3ClimS%dim_pressures_clim) ! p = pressure index
        end do  ! l
      end do ! m

      ! open ozone climatology file
      openErrorTOMS_V8_O3_File = load_file(O3ClimS%ozoneClimFileName, file)
      if (openErrorTOMS_V8_O3_File /= 0) then
        call logDebug('ERROR in opening TOMS V8 ozone file')
        write(errS%temp,*) 'O3ClimS%ozoneClimFileName: ', &
                     trim( O3ClimS%ozoneClimFileName)
        call errorAddLine(errS, errS%temp)
        call mystop(errS, 'stopped because of open error')
        if (errorCheck(errS)) return
      end if

      ! read ozone climatology
      do m = 1, O3ClimS%dim_months_clim            ! m = month index  1 = Jan, 2 = Febr, ..., 12 = Dec
        do l = 1, O3ClimS%dim_lat_clim             ! l = latitude index
          status = file_readline(file, line)       ! read away header for the months and latitudes
          do n = 1, O3ClimS%dim_total_column_clim  ! n = total ozone column index
            status = file_readline(file, line)
            read(line,'(F8.1, 6F7.2, 5F6.2)') O3ClimS%nodes_total_column(n), &
             (O3ClimS%clim_O3(p, n, l, m), p = 1, O3ClimS%dim_pressures_clim) ! p = pressure index
          end do ! n
        end do  ! l
      end do ! m

      if ( verbose ) then

        write(intermediateFileUnit, *) 'Temperature climatology'
        write(intermediateFileUnit, *)
        do m = 1, O3ClimS%dim_months_clim  ! m = month index  1 = Jan, 2 = Febr, ..., 12 = Dec
          write(intermediateFileUnit,'(A, I4)') 'month = ', m
          do l = 1, O3ClimS%dim_lat_clim   ! l = latitude index
            write(intermediateFileUnit,'(11F7.1)') &
                (O3ClimS%clim_temperature(p, l, m), p = 1, O3ClimS%dim_pressures_clim) ! p = pressure index
          end do  ! l
        end do ! m

        write(intermediateFileUnit, *)
        write(intermediateFileUnit, *) 'ozone climatology'
        write(intermediateFileUnit, *)
        do m = 1, O3ClimS%dim_months_clim   ! m = month index  1 = Jan, 2 = Febr, ..., 12 = Dec
          do l = 1, O3ClimS%dim_lat_clim    ! l = latitude index
            write(intermediateFileUnit,'(A, I4, A, F7.1)') 'month = ', m, ' latitude = ', O3ClimS%nodes_latitudes(l)
            do n = 1, O3ClimS%dim_total_column_clim
              write(intermediateFileUnit,'(F7.1, 11F7.2)') O3ClimS%nodes_total_column(n), &
               (O3ClimS%clim_O3(p, n, l, m), p = 1, O3ClimS%dim_pressures_clim)
            end do ! n
          end do  ! l
        end do ! m

      end if

      call exit('read_TOMS_V8_climatology')

    end subroutine read_TOMS_V8_climatology


    subroutine fillTemperatureNodes(errS, O3ClimS, gasPTS)

      ! fill temperature nodes using the temperature climatology

      type(errorType),  intent(inout) :: errS
      type(O3ClimType), intent(in)    :: O3ClimS
      type(gasPTType),  intent(inout) :: gasPTS

      ! local
      real(8) :: temperatureClimMonthLat11 (O3ClimS%dim_pressures_clim)
      real(8) :: temperatureClimMonthLat21 (O3ClimS%dim_pressures_clim)
      real(8) :: temperatureClimMonthLat12 (O3ClimS%dim_pressures_clim)
      real(8) :: temperatureClimMonthLat22 (O3ClimS%dim_pressures_clim)
      real(8) :: temperatureClim           (O3ClimS%dim_pressures_clim)
      real(8) :: ln_pressureClim           (O3ClimS%dim_pressures_clim)
      integer :: month1, month2
      integer :: latitude1, latitude2, i_latitude
      real(8) :: fracLat, fracDay
      real(8) :: ln_pressure, pressure
      integer :: i_pressure
      integer :: status

      logical, parameter :: verbose = .false.

      call enter('fillTemperatureNodes')

      ! initialize
      status = 0
      latitude1 = 1
      latitude2 = 2
      month1    = 1
      month2    = 2
      fracLat   = 1.0d0
      fracDay   = 1.0d0

      ! it is assumed that the temperature of a certain month pertains to day 15 of that month
      ! month1 and month2 are used to interpolate in time
      ! if day_of_month = 20 and month = 3 then month1 =  3 and month2 = 4
      ! if day_of_month = 10 and month = 3 then month1 =  2 and month2 = 3
      ! if day_of_month = 10 and month = 1 then month1 = 12 and month2 = 1

      if ( O3ClimS%day_of_month >= 15 ) then
        month1 = O3ClimS%month
        month2 = month1 + 1
        if( month2 == 13 ) month2 = 1
        fracDay = 1.0d0 - ( O3ClimS%day_of_month - 15 ) / 30.0d0
      else
        month1 = O3ClimS%month - 1
        month2 = month1 + 1
        if( month1 == 0 ) month1 = 12
        fracDay = 1.0d0 - ( O3ClimS%day_of_month + 15 ) / 30.0d0
      end if

      do i_latitude = 1, O3ClimS%dim_lat_clim - 1
        if ( O3ClimS%latitude >= O3ClimS%nodes_latitudes(i_latitude) .and. &
             O3ClimS%latitude <  O3ClimS%nodes_latitudes(i_latitude +1)    ) then
          latitude1 = i_latitude
          latitude2 = i_latitude + 1
          fracLat = 1.0d0 - ( O3ClimS%latitude - O3ClimS%nodes_latitudes(i_latitude) ) / 10.0d0
        end if
      end do ! i_latitude
      if ( O3ClimS%latitude < O3ClimS%nodes_latitudes(1) ) then
        latitude1 = 1
        latitude2 = 2
        fracLat   = 1.0d0
      end if
      if ( O3ClimS%latitude >= O3ClimS%nodes_latitudes( O3ClimS%dim_lat_clim ) ) then
        latitude1 = O3ClimS%dim_lat_clim - 1
        latitude2 = O3ClimS%dim_lat_clim
        fracLat   = 0.0d0
      end if

      temperatureClimMonthLat11(:) = O3ClimS%clim_temperature(:,latitude1, month1)
      temperatureClimMonthLat21(:) = O3ClimS%clim_temperature(:,latitude2, month1)
      temperatureClimMonthLat12(:) = O3ClimS%clim_temperature(:,latitude1, month2)
      temperatureClimMonthLat22(:) = O3ClimS%clim_temperature(:,latitude2, month2)
      temperatureClim(:) =        fracDay  *        fracLat  * temperatureClimMonthLat11(:) + &
                                  fracDay  * (1.0 - fracLat) * temperatureClimMonthLat21(:) + &
                           (1.0 - fracDay) *        fracLat  * temperatureClimMonthLat12(:) + &
                           (1.0 - fracDay) * (1.0 - fracLat) * temperatureClimMonthLat22(:)

      ! interpolate to pressures specified in configuration file, using the logarithm of the pressure
      do i_pressure = O3ClimS%dim_pressures_clim, 1, - 1
        ln_pressureClim(i_pressure) = log( O3ClimS%nodes_pressures(i_pressure) )
      end do
      do i_pressure = 0, gasPTS%npressureNodes
        pressure      = gasPTS%pressureNodes(i_pressure)
        if ( pressure > O3ClimS%nodes_pressures(1) ) then
          pressure    = O3ClimS%nodes_pressures(1)
        end if
        if ( pressure < O3ClimS%nodes_pressures(O3ClimS%dim_pressures_clim) ) then
          pressure    = O3ClimS%nodes_pressures(O3ClimS%dim_pressures_clim)
        end if
        ln_pressure   = log( pressure )
        gasPTS%temperatureNodesAP(i_pressure) =  splintLin(errS, ln_pressureClim, temperatureClim, ln_pressure, status)
        if ( verbose ) then
          write(intermediateFileUnit,'(2F10.2)') gasPTS%temperatureNodes  (i_pressure), gasPTS%temperatureNodesAP(i_pressure)
        end if
        gasPTS%temperatureNodes  (i_pressure) =  gasPTS%temperatureNodesAP(i_pressure)
      end do ! i_pressure

      call exit('fillTemperatureNodes')

    end subroutine fillTemperatureNodes


    subroutine fillOzoneNodes(errS, O3ClimS, traceGasS)

      ! fill ozone nodes using the TOMS ozone climatology

      type(errorType),     intent(inout) :: errS
      type(O3ClimType),    intent(inout) :: O3ClimS
      type(traceGasType),  intent(inout) :: traceGasS

      ! local
      real(8) :: ozoneClimColLatMonth_111 (O3ClimS%dim_pressures_clim)
      real(8) :: ozoneClimColLatMonth_112 (O3ClimS%dim_pressures_clim)
      real(8) :: ozoneClimColLatMonth_121 (O3ClimS%dim_pressures_clim)
      real(8) :: ozoneClimColLatMonth_122 (O3ClimS%dim_pressures_clim)
      real(8) :: ozoneClimColLatMonth_211 (O3ClimS%dim_pressures_clim)
      real(8) :: ozoneClimColLatMonth_212 (O3ClimS%dim_pressures_clim)
      real(8) :: ozoneClimColLatMonth_221 (O3ClimS%dim_pressures_clim)
      real(8) :: ozoneClimColLatMonth_222 (O3ClimS%dim_pressures_clim)
      real(8) :: ozoneClim                (O3ClimS%dim_pressures_clim)
      real(8) :: ozoneClim_vmr            (O3ClimS%dim_pressures_vmr)
      real(8) :: ln_pressure_vmr          (O3ClimS%dim_pressures_vmr)
      integer :: month1, month2
      integer :: latitude1, latitude2, i_latitude
      integer :: column1, column2, i_column, number_total_columns
      integer :: i_pressure
      real(8) :: fracLat, fracMonth, fracCol
      real(8) :: sum_ozone_column
      real(8) :: ln_pressure, pressure
      integer :: status
      logical :: node_not_found

      real(8), parameter :: DUToCm2 = 2.68668d16
      logical, parameter :: verbose = .false.

      call enter('fillOzoneNodes')

      ! initialization
      status = 0
      month1 = 1
      month2 = 2
      latitude1 = 1
      latitude2 = 2
      fracLat   = 1.0d0
      fracMonth = 1.0d0
      fracCol   = 1.0d0

      ! it is assumed that the ozone of a certain month pertains to day 15 of that month
      ! month1 and month2 are used to interpolate in time
      ! if day_of_month = 20 and month = 3 then month1 =  3 and month2 = 4
      ! if day_of_month = 10 and month = 3 then month1 =  2 and month2 = 3
      ! if day_of_month = 10 and month = 1 then month1 = 12 and month2 = 1

      ! determine interpolation nodes for months
      if ( O3ClimS%day_of_month >= 15 ) then
        month1 = O3ClimS%month
        month2 = month1 + 1
        if( month2 == 13 ) month2 = 1
        fracMonth = 1.0d0 - ( O3ClimS%day_of_month - 15 ) / 30.0d0
      else
        month1 = O3ClimS%month - 1
        month2 = month1 + 1
        if( month1 == 0 ) month1 = 12
        fracMonth = 1.0d0 - ( O3ClimS%day_of_month + 15 ) / 30.0d0
      end if

      ! determine interpolation nodes for latitude
      do i_latitude = 1, O3ClimS%dim_lat_clim - 1
        if ( O3ClimS%latitude >= O3ClimS%nodes_latitudes(i_latitude) .and. &
             O3ClimS%latitude <  O3ClimS%nodes_latitudes(i_latitude +1)    ) then
          latitude1 = i_latitude
          latitude2 = i_latitude + 1
          fracLat = 1.0d0 - ( O3ClimS%latitude - O3ClimS%nodes_latitudes(i_latitude) ) / 10.0d0
        end if
      end do ! i_latitude
      if ( O3ClimS%latitude < O3ClimS%nodes_latitudes(1) ) then
        latitude1 = 1
        latitude2 = 2
        fracLat   = 1.0d0
      end if
      if ( O3ClimS%latitude >= O3ClimS%nodes_latitudes( O3ClimS%dim_lat_clim ) ) then
        latitude1 = O3ClimS%dim_lat_clim - 1
        latitude2 = O3ClimS%dim_lat_clim
        fracLat   = 0.0d0
      end if

      ! determine average column from climatology
      number_total_columns = 0
      sum_ozone_column = 0.0d0
      do i_column = 1, O3ClimS%dim_total_column_clim
        if ( O3ClimS%clim_O3(1, i_column, latitude1, month1) > 990.0 ) cycle
        number_total_columns = number_total_columns + 1
        sum_ozone_column = sum_ozone_column + O3ClimS%nodes_total_column( i_column )
      end do ! i_column
      O3ClimS%totalOzoneColumn = sum_ozone_column / number_total_columns

      ! determine the interpolation nodes for the column
      node_not_found = .true.
      column1 = 0
      do i_column = 1, O3ClimS%dim_total_column_clim - 1
        if ( O3ClimS%nodes_total_column(i_column    ) > 990.0 ) cycle
        if ( O3ClimS%nodes_total_column(i_column + 1) > 990.0 ) cycle
        if ( O3ClimS%totalOzoneColumn >= O3ClimS%nodes_total_column(i_column) .and. &
             O3ClimS%totalOzoneColumn <  O3ClimS%nodes_total_column(i_column +1)    ) then
          column1 = i_column
          column2 = i_column + 1
          fracCol = 1.0d0 - ( O3ClimS%totalOzoneColumn - O3ClimS%nodes_total_column(i_column) ) / 50.0d0
          node_not_found = .false.
        end if
      end do ! i_column

      if ( node_not_found ) then
        call logDebug('ERROR in module readModule, subroutine fillOzoneNodes')
        call logDebug('cannot calculate fracCol')
        call mystop(errS, 'cannot calculate fracCol' )
        if (errorCheck(errS)) return
      end if

      ! total ozone column outside range of climatology
      if ( column1 == 0 ) then
        call logDebug('ERROR total ozone column outside range of climatology')
        call mystop(errS,  'total ozone column outside range of climatology' )
        if (errorCheck(errS)) return
      end if

! JdH Debug
     !write(intermediateFileUnit,'(A, F10.3, 3(A, F10.4))') 'total_O3', O3ClimS%totalOzoneColumn, &
     !              ' fracCol = ', fracCol, ' fracLat = ', fracLat, ' fracMonth = ', fracMonth
     !write(intermediateFileUnit,'(A, F10.3, 3(A, I4))') 'total_O3', O3ClimS%totalOzoneColumn, &
     !              ' column1 = ', column1, ' latitude1 = ', latitude1, ' month1 = ', month1
     !write(intermediateFileUnit,'(A, F10.3, 3(A, I4))') 'total_O3', O3ClimS%totalOzoneColumn, &
     !              ' column2 = ', column2, ' latitude2 = ', latitude2, ' month2 = ', month2

     ozoneClimColLatMonth_111(:) = O3ClimS%clim_O3(:, column1, latitude1, month1)
     ozoneClimColLatMonth_112(:) = O3ClimS%clim_O3(:, column1, latitude1, month2)
     ozoneClimColLatMonth_121(:) = O3ClimS%clim_O3(:, column1, latitude2, month1)
     ozoneClimColLatMonth_122(:) = O3ClimS%clim_O3(:, column1, latitude2, month2)
     ozoneClimColLatMonth_211(:) = O3ClimS%clim_O3(:, column2, latitude1, month1)
     ozoneClimColLatMonth_212(:) = O3ClimS%clim_O3(:, column2, latitude1, month2)
     ozoneClimColLatMonth_221(:) = O3ClimS%clim_O3(:, column2, latitude2, month1)
     ozoneClimColLatMonth_222(:) = O3ClimS%clim_O3(:, column2, latitude2, month2)

! JdH Debug
     !write(intermediateFileUnit,'(A, 12F10.3)') '111 ', ozoneClimColLatMonth_111
     !write(intermediateFileUnit,'(A, 12F10.3)') '112 ', ozoneClimColLatMonth_112
     !write(intermediateFileUnit,'(A, 12F10.3)') '121 ', ozoneClimColLatMonth_121
     !write(intermediateFileUnit,'(A, 12F10.3)') '122 ', ozoneClimColLatMonth_122
     !write(intermediateFileUnit,'(A, 12F10.3)') '211 ', ozoneClimColLatMonth_211
     !write(intermediateFileUnit,'(A, 12F10.3)') '212 ', ozoneClimColLatMonth_212
     !write(intermediateFileUnit,'(A, 12F10.3)') '221 ', ozoneClimColLatMonth_221
     !write(intermediateFileUnit,'(A, 12F10.3)') '222 ', ozoneClimColLatMonth_222

      ozoneClim(:) =       fracCol   *        fracLat  *        fracMonth  * ozoneClimColLatMonth_111(:) + &
                           fracCol   *        fracLat  * (1.0 - fracMonth) * ozoneClimColLatMonth_112(:) + &
                           fracCol   * (1.0 - fracLat) *        fracMonth  * ozoneClimColLatMonth_121(:) + &
                           fracCol   * (1.0 - fracLat) * (1.0 - fracMonth) * ozoneClimColLatMonth_122(:) + &
                    (1.0 - fracCol)  *        fracLat  *        fracMonth  * ozoneClimColLatMonth_211(:) + &
                    (1.0 - fracCol)  *        fracLat  * (1.0 - fracMonth) * ozoneClimColLatMonth_212(:) + &
                    (1.0 - fracCol)  * (1.0 - fracLat) *        fracMonth  * ozoneClimColLatMonth_221(:) + &
                    (1.0 - fracCol)  * (1.0 - fracLat) * (1.0 - fracMonth) * ozoneClimColLatMonth_222(:)

      ! fill the pressure levels at the middle of the layers
      do i_pressure = 2, O3ClimS%dim_pressures_vmr -1
        O3ClimS%nodes_pressures_vmr(i_pressure) = 0.5d0 * O3ClimS%nodes_pressures(i_pressure - 1 )      &
                                                + 0.5d0 * O3ClimS%nodes_pressures(i_pressure     )
      end do
      ! fill highest pressure
      O3ClimS%nodes_pressures_vmr(1) = 1050.0
      ! fill lowest pressure
      O3ClimS%nodes_pressures_vmr(O3ClimS%dim_pressures_vmr) = 0.05d0

! JdH Debug
      ! write interpolated climatology
      !write(intermediateFileUnit,*) 'interpolated ozone values in DU'
      !do i_pressure = 1, O3ClimS%dim_pressures_clim
      !  write(intermediateFileUnit,'(F10.3, F10.2)') O3ClimS%nodes_pressures(i_pressure), ozoneClim(i_pressure)
      !end do

      ! translates Dobson Units for a layer to the mean vmr of that layer
      if ( O3ClimS%dim_pressures_vmr > 3 ) then
        do i_pressure = 2, O3ClimS%dim_pressures_vmr - 1
          ozoneClim_vmr(i_pressure) = 1.27d0 * ozoneClim(i_pressure - 1)  &
               / (O3ClimS%nodes_pressures(i_pressure - 1 ) - O3ClimS%nodes_pressures(i_pressure) )
        end do
        ozoneClim_vmr(1) = ozoneClim_vmr(2)
        ozoneClim_vmr(O3ClimS%dim_pressures_vmr) = 0.2d0
      else
        call logDebug('ERROR in module readModule, subroutine fillOzoneNodes')
        call logDebug('O3ClimS%dim_pressures_vmr is too small')
        if (errorCheck(errS)) return
      end if

      !write(intermediateFileUnit,*) 'interpolated ozone values in vmr at midlevel pressures and at boundaries'
      !do i_pressure = 1, O3ClimS%dim_pressures_vmr
      !  write(intermediateFileUnit,'(F10.3, 2F8.2)') O3ClimS%nodes_pressures_vmr(i_pressure), ozoneClim_vmr(i_pressure)
      !end do

      ! interpolate to pressures specified in configuration file, using the logarithm of the pressure
      do i_pressure = O3ClimS%dim_pressures_vmr, 1, - 1
        ln_pressure_vmr(i_pressure) = log( O3ClimS%nodes_pressures_vmr(i_pressure) )
      end do
      do i_pressure = 0, traceGasS%nalt
        pressure      = traceGasS%pressure(i_pressure)
        if ( pressure > O3ClimS%nodes_pressures_vmr(1) ) then
          pressure    = O3ClimS%nodes_pressures_vmr(1)
        end if
        if ( pressure < O3ClimS%nodes_pressures_vmr(O3ClimS%dim_pressures_vmr) ) then
          pressure    = O3ClimS%nodes_pressures_vmr(O3ClimS%dim_pressures_vmr)
        end if
        ln_pressure   = log( pressure )
        traceGasS%vmrAP(i_pressure) =  splintLin(errS, ln_pressure_vmr, ozoneClim_vmr, ln_pressure, status)
        if ( verbose ) then
          write(intermediateFileUnit,'(F10.3, 2F10.2)') pressure, traceGasS%vmr (i_pressure), &
                                                        traceGasS%vmrAP(i_pressure)
        end if
        traceGasS%vmr(i_pressure) = traceGasS%vmrAP(i_pressure)
      end do ! i_pressure

      do i_pressure = 0,  traceGasS%nalt
        traceGasS%covVmrAP(i_pressure, i_pressure) = (1.0d-2 * O3ClimS%relError_vmr &
                                                   * traceGasS%vmrAP(i_pressure))**2
        traceGasS%covVmr  (i_pressure, i_pressure) = traceGasS%covVmrAP(i_pressure, i_pressure)
      end do ! ipressure

      call exit('fillOzoneNodes')

    end subroutine fillOzoneNodes


  end module readModule
