!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module HITRANModule

  use DISAMAR_file
  use dataStructures
  use errorHandlingModule
  use mathTools

  ! default is private
  private
  public  :: getAbsorptionXsecLineAbs, getLinePositions

  ! the type definitions are private to the module
  type HITRANType
    ! contains line parameters parameters
    ! for which a Voigt profile is used
    integer            :: gasIndex                  ! index specifying the gas
    integer            :: nISO                      ! number of ISO value
    integer, pointer   :: ISO(:)          => null() ! (nISO) isotope numbers (1=most abundant, 2=seond most abundant.)
    integer, pointer   :: isotopologue(:) => null() ! (nISO) isotopoloque numbers corresponding to ISO
    real(8), pointer   :: molWeight(:)    => null() ! (nISO) molecular weight corresponding to ISO
    real(8), pointer   :: abundance(:)    => null() ! (nISO) isotope fraction

    integer            :: nlines                    ! number of lines
    integer, pointer   :: isotope(:)      => null() ! (nlines) ISO number
    real(8), pointer   :: Sig(:)          => null() ! (nlines) wave numbers (cm-1)
    real(8), pointer   :: S(:)            => null() ! (nlines) line strength
    real(8), pointer   :: gam(:)          => null() ! (nlines) doppler width
    real(8), pointer   :: E(:)            => null() ! (nlines) energies of the lower levels of the lines (cm-1)
    real(8), pointer   :: bet(:)          => null() ! (nlines) temperature dependence of the width
    real(8), pointer   :: delt(:)         => null() ! (nlines) pressure shift of line
  end type HITRANType

  type SDFType
    ! contains parameters for strong absorption
    ! for these lines CIA and line mixing is applied
    integer            :: nlines                   !          number of strong lines (all with isotope = 1)
    real(8), pointer   :: sigLines(:)    => null() ! (nlines) wave numbers for the lines (cm-1)
    real(8), pointer   :: modSigLines(:) => null() ! (nlines) modified wave numbers due to collisional shift (cm-1)
    real(8), pointer   :: PopuT0(:)      => null() ! (nlines) populations of the Lower Levels of the Lines at T0 = 296 K
    real(8), pointer   :: PopuT (:)      => null() ! (nlines) populations of the Lower Levels at temperature T
    real(8), pointer   :: DipoR(:)       => null() ! (nlines) Rigid rotor dipole
    real(8), pointer   :: Dipo0(:)       => null() ! (nlines) true Dipole transition Moments of the Lines at T0
    real(8), pointer   :: Dipo(:)        => null() ! (nlines) true Dipole transition Moments of the Lines at T
    real(8), pointer   :: E(:)           => null() ! (nlines) energies of the Lower levels of the lines (cm-1)
    real(8), pointer   :: HWT0(:)        => null() ! (nlines) air-broadened Half-Widths (at 296 K) of the lines (Cm-1/Atm)
    real(8), pointer   :: HWT(:)         => null() ! (nlines) air-broadened Half-Widths of the lines (Cm-1/Atm)
    real(8), pointer   :: BHW(:)         => null() ! (nlines) temperature Dependence Coefficients of HWT0
    real(8), pointer   :: SHIFT(:)       => null() ! (nlines) derivative of collision induced wavenumber shift w.r.t. Ptot
    real(8), pointer   :: YT(:)          => null() ! (nlines) first order line mixing coefficient
    integer, pointer   :: m1(:)          => null() ! (nlines)
  end type SDFType

  type RMFType ! line mixing
    integer            :: nRMF             ! number relaxation matrix elements
    real(8), pointer   :: WT0(:) => null() ! (nRMF) Air-broadened Relaxation Operator Elements (at 296 K)
                                           !        of all Couples of Lines (Cm-1/Atm)
    real(8), pointer   :: WT(:)  => null() ! (nRMF) Air-broadened Relaxation Operator Elements (at 296 K)
                                           !        of all Couples of Lines (Cm-1/Atm)
    real(8), pointer   :: BW(:)  => null() ! (nRMF) Temperature Dependence Coefficients of WT0
  end type RMFType

  ! constants
  real(8), parameter :: pi           = 3.1415926536d0
  real(8), parameter :: gasconstant  = 8.3144621d0   ! universal gas constant = N_A * k_B
  real(8), parameter :: c            = 2.99792458d8  ! speed of light (m/s)
  real(8), parameter :: T0           = 296.0d0       ! reference temperature K
  real(8), parameter :: k_Boltzmann  = 1.3806488d-23 ! Boltzmann constant J K-1
  real(8), parameter :: hc_kB        = 1.4387770d0   ! h*c/kB (in cm/K)

  contains

    subroutine getAbsorptionXsecLineAbs(errS,  wavelHRS, traceGasS, XsecS )

      ! calculate the absorption cross sections based on the LISA code

      implicit none

      type(errorType), intent(inout) :: errS
      type(wavelHRType),    intent(in)    :: wavelHRS        ! high resolution wavelength grid
      type(traceGasType),   intent(in)    :: traceGasS       ! atmospheric trace gas properties
      type(XsecType),       intent(inout) :: XsecS           ! absorption cross sections

      ! local type declaration
      integer  :: iwave, iP

      ! HITRAN line parameter structure
      type( HITRANType )  :: hitranS             ! HITRAN data structure

      ! data structures for line mixing O2 A band
      type( SDFType )     :: SDFS          ! data for line mixing
      type( RMFType )     :: RMFS          ! relaxation matrix data (line mixing)

      ! parameters to translate the volume absorption coefficient
      ! into the absorption cross section
      real(8)            :: T               ! temperature in Kelvin
      real(8)            :: P               ! pressure in atm
      real(8)            :: wavelStart      ! start of high resolution wavelength grid
      real(8)            :: wavelEnd        ! end of high resolution wavelength grid

      ! waveNumbers
      real(8)  :: waveNumbers   (wavelHRS%nwavel) ! waveNumbers

      ! logicals to determine whether we need line mixing and how to use it
      logical  :: filterStrongLinesO2A
      logical  :: useLM    ! if useLM = .true. calculate line mixing for the O2 A-band

      real(8)  :: Xsec(wavelHRS%nwavel)      ! absorption cross section without line mixing for strong lines
      real(8)  :: Xsec_LM(wavelHRS%nwavel)   ! absorption cross section due to line mixing for strong lines

      wavelStart = wavelHRS%wavel(1)
      wavelEnd   = wavelHRS%wavel(wavelHRS%nwavel)

      call fillMolecularParameters(errS, XsecS, hitranS)
      if (errorCheck(errS)) return

      ! set useLM
      filterStrongLinesO2A  = .false.
      useLM                 = .false.
      if ( (abs(XsecS%factorLM) > 1.0d-8 ) .and. (XsecS%gasIndex == 7) ) then
        useLM                 = .true.
        filterStrongLinesO2A  = .true.
        ! test spectral range
        if (  (wavelStart > 775.0d0) .or. (wavelEnd < 755.0d0) ) then
          filterStrongLinesO2A  = .false.
          useLM                 = .false.
          call logDebug('WARNING: factorLM > 1.0d-8 but line mixing is turned off')
          call logDebug('because wavelength range is outside O2 A-band (755, 775)')
          write(errS%temp,'(A, F10.3)') 'wavelStart = ', wavelStart
          call errorAddLine(errS, errS%temp)
          write(errS%temp,'(A, F10.3)') 'wavelEnd   = ', wavelEnd
          call errorAddLine(errS, errS%temp)
        end if
      end if

      ! read the data file; memory is allocated there also
      call readLineParameters(errS,  filterStrongLinesO2A, XsecS%XsectionFileName, &
                               XsecS%cutoff, wavelStart, wavelEnd, hitranS )
      if (errorCheck(errS)) return

      if ( useLM ) then
        ! read data for line mixing
        call readSDF(errS, O2_LISA_SDFXsecFileName, SDFS)
        if (errorCheck(errS)) return
        call readRMF(errS, O2_LISA_RMFXsecFileName, RMFS)
        if (errorCheck(errS)) return
        !  fill waveNumber array
        do iwave = 1, wavelHRS%nwavel
          waveNumbers(wavelHRS%nwavel + 1 - iwave) = 1.0d7 / wavelHRS%wavel(iwave)
        end do
      end if ! useLM

      do iP = 0, traceGasS%nalt

        T = traceGasS%temperature(iP)              ! T in K
        P = traceGasS%pressure(iP) / 1013.25d0     ! P  in atm

! JdH Debug
!       keep pressure to surface pressure wen calculating absorption cross sections
!        P = traceGasS%pressure(0) / 1013.25d0

        P = traceGasS%pressure(iP) / 1013.25d0

        ! perform calculations for absorption cross section - no line mixing
        call CalculatAbsXsec(errS,  T, P, XsecS%cutoff, wavelHRS%wavel(:), XsecS%Xsec(:, iP), hitranS)
        if (errorCheck(errS)) return

        if ( filterStrongLinesO2A ) then

          ! strong lines are filtered for O2 A band for the JPL database
          ! the absorption for strong lines is calculated from SDFS and RMFS and returned in Xsec
          call CalculateLineMixingXsec(errS, T, P, waveNumbers, hitranS, SDFS, RMFS, Xsec, Xsec_LM)
          if (errorCheck(errS)) return
          if ( useLM ) then
            ! add results for weak lines, strong lines, and line mixing
            ! use reverse order for strong lines and LM as they are ordered by wavenumber not wavelength
            do iwave = 1, wavelHRS%nwavel
              XsecS%Xsec(iwave, iP) = XsecS%Xsec(iwave, iP) + Xsec(wavelHRS%nwavel+1 - iwave) &
                                    + XsecS%factorLM * Xsec_LM(wavelHRS%nwavel+1 - iwave)
            end do
          else
            ! add results for weak lines and strong lines
            ! use reverse order for strong lines they are ordered by wavenumber not wavelength
            do iwave = 1, wavelHRS%nwavel
              XsecS%Xsec(iwave, iP) = XsecS%Xsec(iwave, iP) + Xsec(wavelHRS%nwavel + 1 - iwave)
            end do
          end if ! useLM

        else

          ! no filtering therefore XsecS%Xsec contains results for the weak and strong lines
          if ( useLM ) then
            call CalculateLineMixingXsec(errS, T, P, waveNumbers, hitranS, SDFS, RMFS, Xsec, Xsec_LM)
            if (errorCheck(errS)) return
            ! add results for line mixing
            do iwave = 1, wavelHRS%nwavel
              XsecS%Xsec(iwave, iP) = XsecS%Xsec(iwave, iP) &
                                    + XsecS%factorLM * Xsec_LM(wavelHRS%nwavel +1 - iwave)
            end do
          end if ! useLM

        end if ! filterStrongLinesO2A

      end do ! loop over pressures

      ! apply scale factor for absorption cross section
      XsecS%Xsec(:,:) = traceGasS%scaleFactorXsec * XsecS%Xsec(:,:)

      ! clean up

      call freeMemHITRAN_lineParameters(errS, hitranS)
      if (errorCheck(errS)) return
      call freeMemHITRAN_ISO(errS, hitranS)
      if (errorCheck(errS)) return
      if ( useLM ) then
        call freeMemSDFS(errS, SDFS)
        if (errorCheck(errS)) return
        call freeMemRMFS(errS, RMFS)
        if (errorCheck(errS)) return
      end if

    end subroutine getAbsorptionXsecLineAbs


    subroutine getLinePositions(errS,  wavelStart, wavelEnd, XsecS, absLinesS )

     ! Subroutine getLinePositions gives the wavelengths
     ! of the absorption lines (in nm) and the intensity of the lines
     ! that fall in the wavelength range covered by wavelHRS%wavel.
     ! The arrays wavelLines and intensityLines are allocated here.

      implicit none

      type(errorType), intent(inout) :: errS
      real(8),              intent(in)    :: wavelStart      ! start of high resolution wavelength grid
      real(8),              intent(in)    :: wavelEnd        ! end of high resolution wavelength grid
      type( XsecType ),     intent(inout) :: XsecS           ! contains data for reading HITRAN file
      type(absLinesType),   intent(inout) :: absLinesS       ! line positions and line strength

      ! HITRAN line parameter structure
      type( HITRANType )  :: hitranS             ! HITRAN data structure

      integer :: iwave

      logical, parameter :: verbose = .false.

      call enter('getLinePositions')

      call fillMolecularParameters(errS, XsecS, hitranS)
      if (errorCheck(errS)) return

      ! read the data file; memory is allocated there also
      call readLinePositions(errS,  absLinesS%XsectionFileName, XsecS%cutoff, wavelStart, wavelEnd, hitranS )
      if (errorCheck(errS)) return

      ! claim memory
      absLinesS%nlinePos = hitranS%nlines
      call claimMemAbsLinesS(errS,  absLinesS )
      if (errorCheck(errS)) return

      ! fill line positions and line strength in data structure absLinesS
      do iwave = 1, hitranS%nlines
        ! convert cm-1 into nm
        absLinesS%linePos(hitranS%nlines + 1 - iwave) = 1.0d7 / hitranS%Sig(iwave)
        absLinesS%S      (hitranS%nlines + 1 - iwave) =         hitranS%S  (iwave)
      end do

      ! clean up

      call freeMemHITRAN_lineParameters(errS,  hitranS )
      if (errorCheck(errS)) return
      call freeMemHITRAN_ISO(errS,  hitranS )
      if (errorCheck(errS)) return

      call exit('getLinePositions')

    end subroutine getLinePositions


    subroutine fillMolecularParameters(errS,  XsecS, hitranS )

      ! claim memory for the arrays ISO, isotopologue, molWeight, and abundance based on nISO
      ! fill isotopologue number and molecular weight
      ! depending on the value of gasIndex and the ISO numbers

      implicit none

      type(errorType), intent(inout) :: errS
      type( XsecType ),     intent(in)    :: XsecS
      type( HITRANType ),   intent(inout) :: hitranS       ! HITRAN data structure

      ! local
      integer :: iso

      ! claim memory
      call claimMemHITRAN_ISO(errS,  XsecS%nISO, hitranS )
      if (errorCheck(errS)) return

      hitranS%gasIndex = XsecS%gasIndex

      select case ( hitranS%gasIndex )

        case(1) ! H2O
          do iso = 1, XsecS%nISO
            hitranS%ISO(iso) = XsecS%ISO(iso)
            select case ( XsecS%ISO(iso) )
              case (1)
                hitranS%isotopologue(iso) = 161
                hitranS%molWeight(iso)    = 18.010565d0
                hitranS%abundance(iso)    = 0.997317d0
              case (2)
                hitranS%isotopologue(iso) = 181
                hitranS%molWeight(iso)    = 20.014811d0
                hitranS%abundance(iso)    = 1.99983d-3
              case (3)
                hitranS%isotopologue(iso) = 171
                hitranS%molWeight(iso)    = 19.014780d0
                hitranS%abundance(iso)    = 3.71884d-4
              case (4)
                hitranS%isotopologue(iso) = 162
                hitranS%molWeight(iso)    = 19.016740d0
                hitranS%abundance(iso)    = 3.10693d-4
              case (5)
                hitranS%isotopologue(iso) = 182
                hitranS%molWeight(iso)    = 21.020985d0
                hitranS%abundance(iso)    = 6.23003d-7
              case (6)
                hitranS%isotopologue(iso) = 172
                hitranS%molWeight(iso)    = 20.020956d0
                hitranS%abundance(iso)    = 1.15853d-7
              case default

            end select
          end do ! iso

        case(2) ! CO2
          do iso = 1, XsecS%nISO
            hitranS%ISO(iso) = XsecS%ISO(iso)
            select case ( XsecS%ISO(iso) )
              case (1)
                hitranS%isotopologue(iso) = 626
                hitranS%molWeight(iso)    = 43.989830d0
                hitranS%abundance(iso)    = 0.984204d0
              case (2)
                hitranS%isotopologue(iso) = 636
                hitranS%molWeight(iso)    = 44.993185d0
                hitranS%abundance(iso)    = 1.10574d-2
              case (3)
                hitranS%isotopologue(iso) = 628
                hitranS%molWeight(iso)    = 45.994076d0
                hitranS%abundance(iso)    = 3.94707d-3
              case (4)
                hitranS%isotopologue(iso) = 627
                hitranS%molWeight(iso)    = 44.994045d0
                hitranS%abundance(iso)    = 7.33989d-4
              case (5)
                hitranS%isotopologue(iso) = 638
                hitranS%molWeight(iso)    = 46.997431d0
                hitranS%abundance(iso)    = 4.43446d-5
              case (6)
                hitranS%isotopologue(iso) = 637
                hitranS%molWeight(iso)    = 45.997400d0
                hitranS%abundance(iso)    = 8.24623d-6
              case default

            end select
          end do ! iso

        case(5) ! CO
          do iso = 1, XsecS%nISO
            hitranS%ISO(iso) = XsecS%ISO(iso)
            select case ( XsecS%ISO(iso) )
              case (1)
                hitranS%isotopologue(iso) = 26
                hitranS%molWeight(iso)    = 27.994915d0
                hitranS%abundance(iso)    = 0.986544d0
              case (2)
                hitranS%isotopologue(iso) = 36
                hitranS%molWeight(iso)    = 28.998270d0
                hitranS%abundance(iso)    = 1.10836d-2
              case (3)
                hitranS%isotopologue(iso) = 28
                hitranS%molWeight(iso)    = 29.999161d0
                hitranS%abundance(iso)    = 1.97822d-3
              case (4)
                hitranS%isotopologue(iso) = 27
                hitranS%molWeight(iso)    = 28.999130d0
                hitranS%abundance(iso)    = 3.67867d-4
              case (5)
                hitranS%isotopologue(iso) = 38
                hitranS%molWeight(iso)    = 31.002516d0
                hitranS%abundance(iso)    = 2.22250d-5
              case (6)
                hitranS%isotopologue(iso) = 37
                hitranS%molWeight(iso)    = 30.002485d0
                hitranS%abundance(iso)    = 4.13292d-6
              case default

            end select
          end do ! iso

        case(6) ! CH4
          do iso = 1, XsecS%nISO
            hitranS%ISO(iso) = XsecS%ISO(iso)
            select case ( XsecS%ISO(iso) )
              case (1)
                hitranS%isotopologue(iso) = 211
                hitranS%molWeight(iso)    = 16.031300d0
                hitranS%abundance(iso)    = 0.988274d0
              case (2)
                hitranS%isotopologue(iso) = 311
                hitranS%molWeight(iso)    = 17.034655d0
                hitranS%abundance(iso)    = 1.11031d-2
              case (3)
                hitranS%isotopologue(iso) = 212
                hitranS%molWeight(iso)    = 17.037475d0
                hitranS%abundance(iso)    = 6.15751d-4
              case default

            end select
          end do ! iso

        case(7) ! O2
          do iso = 1, XsecS%nISO
            hitranS%ISO(iso) = XsecS%ISO(iso)
            select case ( XsecS%ISO(iso) )
              case (1)
                hitranS%isotopologue(iso) = 66
                hitranS%molWeight(iso)    = 31.989830d0
                hitranS%abundance(iso)    = 0.995262d0
              case (2)
                hitranS%isotopologue(iso) = 68
                hitranS%molWeight(iso)    = 33.994076d0
                hitranS%abundance(iso)    = 3.99141d-3
              case (3)
                hitranS%isotopologue(iso) = 67
                hitranS%molWeight(iso)    = 32.994045d0
                hitranS%abundance(iso)    = 7.42235d-4
              case default

            end select
          end do ! iso

        case(11) ! NH3
          do iso = 1, XsecS%nISO
            hitranS%ISO(iso) = XsecS%ISO(iso)
            select case ( XsecS%ISO(iso) )
              case (1)
                hitranS%isotopologue(iso) = 4111
                hitranS%molWeight(iso)    = 17.026549
                hitranS%abundance(iso)    = 0.995872d0
              case (2)
                hitranS%isotopologue(iso) = 5111
                hitranS%molWeight(iso)    = 18.023583
                hitranS%abundance(iso)    = 3.66129d-3
              case default

            end select
          end do ! iso

        case default

          call logDebug('ERROR: gasIndex not a valid number')
          call logDebug('in subroutine fillMolecularParameters')
          call logDebug('in module HITRANModule')
          call logDebugI('index = ', hitranS%gasIndex)
          call mystop(errS, 'stopped because gasIndex is not valid')
          if (errorCheck(errS)) return

      end select

    end subroutine fillMolecularParameters


    subroutine CalculatAbsXsec(errS,  T, P, cutoff, wavel, absXsec, hitranS)

      ! Calculate the absorption cross section without line mixing

      ! Based on the code provided by Tran and modified by Sneep
      ! Changed from a stand alone F77 code into a F90 module by Johan de Haan - 2012
      ! Eliminated the use of a mean wavenumber for the Doppler width - March 2014

      implicit none

      type(errorType), intent(inout) :: errS
      real(8),          intent(in)    :: T              ! in kelvin
      real(8),          intent(in)    :: P              ! in atm
      real(8),          intent(in)    :: cutoff         ! in cm-1
      real(8),          intent(in)    :: wavel(:)       ! in nm
      type(HITRANType), intent(in)    :: hitranS
      real(8),          intent(out)   :: absXsec(:)

      real(8) :: GamD, Cte, Cte1, Cte2
      integer :: iso, iLine, iSig, nLines
      real(8) :: rapQ(hitranS%nISO)
      real(8) :: gsi, QT, QT0, XX, YY, WR, WI, aa
      real(8) :: HWT, Lsig, LS, SigC
      real(8) :: waveNumber( size(wavel) )

      integer, parameter :: NTtabulated = 119
      real(8) :: Ttabulated(NTtabulated)
      real(8) :: Qtabulated(NTtabulated)
      real(8) :: DSpline(NTtabulated)
      integer :: status, indexMinloc(1)
      integer :: startSig, endSig

      call getTtabulated(errS, NTtabulated, Ttabulated)
      if (errorCheck(errS)) return

      if ( ( T > Ttabulated(NTtabulated) ) .or. ( T < Ttabulated(1) ) ) then
        call logDebug('Temperature out of range')
        call logDebugD('minimum temperature = ', Ttabulated(1))
        call logDebugD('maximum temperature = ', Ttabulated(NTtabulated))
        call logDebugD('        temperature = ', T)
        call logDebug(' ERROR occurs in  CalculatAbsXsec')
        call logDebug(' in  module HITRANModule')
        call mystop(errS, 'stopped because temperature is out of range')
        if (errorCheck(errS)) return
      end if

      waveNumber(:) = 1.0d7 / wavel(:)

      do iso= 1, hitranS%nISO
        if ( hitranS%iso(iso) == 0 ) cycle
        call getxgj(errS, hitranS, hitranS%isotopologue(iso), gsi)
        if (errorCheck(errS)) return
        call getQtabulated(errS, hitranS, hitranS%isotopologue(iso), NTtabulated, Qtabulated)
        if (errorCheck(errS)) return
        call spline (errS,  Ttabulated, Qtabulated, DSpline, status)
        if (errorCheck(errS)) return
        QT  = splint (errS, Ttabulated, Qtabulated, DSpline, T , status)
        QT0 = splint (errS, Ttabulated, Qtabulated, DSpline, T0, status)
        rapQ(iso) = QT0/QT
      end do

      nLines = size( hitranS%Sig )
      absXsec = 0.0d0
      do iLine = 1, nLines
        HWT  = hitranS%gam(iLine) * (T0/T)**hitranS%bet(iLine)
        Lsig = hitranS%sig(iLine)+ hitranS%delt(iLine) * P
! JdH turn off pressure shift
!       Lsig = hitranS%sig(iLine)
        LS   = hitranS%S(iLine) * rapQ(hitranS%isotope(iLine)) &
              * exp(hc_kB * hitranS%E(iLine) * (1/T0-1/T) ) / Lsig
        LS   = LS * 0.1013d0 / k_Boltzmann / T &
              /( 1.0d0 - exp( -hc_kB * Lsig /T0 ) )
        indexMinloc = minloc(abs(waveNumber(:) - Lsig - cutoff))
        startSig    = indexMinloc(1)
        indexMinloc = minloc(abs(waveNumber(:) - Lsig + cutoff))
        endSig   = indexMinloc(1)
        GamD    = DopplerWidth(T, Lsig, hitranS%molWeight(hitranS%isotope(iLine)))
        Cte     = sqrt(log(2.0d0))/GamD
        Cte1    = Cte/sqrt(pi)
        do iSig = startSig, endSig
          SigC = waveNumber(iSig)
          Cte2=SigC*(1.0d0-exp(-hc_kB*SigC/T))
          ! Complex Probability Function for the "Real" Q-Lines
          XX = ( LSig - SigC ) * Cte
          YY = HWT * P * Cte
          Call CPF(errS, XX,YY,WR,WI)
          if (errorCheck(errS)) return
          ! Voigt absorption coefficient
          aa = Cte1 * P * LS * WR * Cte2               ! aa is the volume absorption coefficient
                                                       ! for a volume mixing ratio of 1.0
          aa = aa * T * 1.380658d-19 / P / 1013.25d0   ! aa is now the absorption cross section in cm2/molecule
          absXsec(iSig) = absXsec(iSig) + aa
        end do ! iSig
      end do ! iLine

    end subroutine CalculatAbsXsec


    subroutine CalculateLineMixingXsec(errS, T, P, waveNumbers, hitranS, SDFS, RMFS, Xsec, Xsec_LM)

    ! subroutine to calculate absorption coefficients due to first order line mixing
    ! for selected strong lines assuming a volume mixing ratio equal to 1.0.
    !
    ! Input/Output Parameters of Routine (Arguments)
    ! ---------------------------------
    ! T       : Temperature in Kelvin (Input).
    ! P       : Total pressure in Atmosphere (Input).
    ! The output wavenumber grid is specified with waveNumbers (Input)
    !
    ! Data read from file
    ! --------------------------------
    ! SDFS    : parameters for the strong lines
    ! RMFS    : parameters for line mixing
    !
    ! Output Quantities (arguments)
    ! -----------------
    ! Xsec            : Absorption cross section due to strong lines without line mixing ( cm2 / molecule )
    !                  (assuming Voigt Line-Shapes) (cm-1)
    ! Xsec_LM         : Absorption cross section due to line mixing ( cm2 / molecule )
    !                  (assuming Voigt Line-Shapes) (cm-1)
    !
    ! Called Routines: 'ConvTP' (CONVert data to current T and Press)
    ! ---------------  'CPF'    (Complex Probability Function)
    !
    ! Double Precision Version
    !
    ! H. Tran, last change 25 November 2009
    ! Maarten Sneep, 2011 - restructured the Fortran 77 code - removed the use of common
    ! Johan de Haan, March 2012 - changed code to a Fortran 90 module for DISAMAR
    !                             calculate only the Xsec that is due to line mixing

      implicit none

    ! Subroutine parameters
      type(errorType), intent(inout) :: errS
      real(8), intent(in) :: T
      real(8), intent(in) :: P
      real(8), intent(in) :: waveNumbers(:)
      type(HITRANType), intent(in)    :: hitranS
      type( SDFType ),  intent(inout) :: SDFS       ! data for strong lines
      type( RMFType ),  intent(inout) :: RMFS       ! relaxation matrix data (line mixing)

    ! Output arguments
      real(8), intent(out) :: Xsec(:)        ! absorption cross section for strong lines without LM
      real(8), intent(out) :: Xsec_LM(:)     ! absorption cross section due to line mixing

    ! Local variables
      integer :: iLine, isig
      real(8) :: SigMoy, GamD, Cte, Cte1, Cte2
      real(8) :: SigC
      real(8) :: aa, bb, XX, YY, WR, WI
      real(8) :: nO2
      real(8) :: abs_no_LM  ( size( waveNumbers) )
      real(8) :: abs_with_LM( size( waveNumbers) )

    ! Array size check and initialization to zero is already done
    ! in the main program, at least for the main output arrays.
    !
    ! Check that the number of lines and the relaxation matrix match in
    ! size.

      if (SDFS%nlines**2 .ne. RMFS%nRMF) then
        write(errS%temp,5005) SDFS%nlines, RMFS%nRMF, SDFS%nlines**2
        call errorAddLine(errS, errS%temp)
        call mystop(errS, 'Stopped with an error')
        if (errorCheck(errS)) return
      end if
 5005 format(1x,'****** Encountered a problem ******',               &
             /,1x,'The size of the spectroscopic datafile (',I6,')', &
             /,1x,'is not consistent with the size of the ',         &
             /,1x,'relaxation matrix (',I6,', expected ',I6,').',    &
             /,1x,'****** Program STOP ******',///)

    ! Convert Band Data to Considered Temperature and Pressure
      Call ConvTP(errS, T, P, hitranS, SDFS, RMFS, SigMoy)
      if (errorCheck(errS)) return

      ! Compute the Doppler Width at SigMoy for ISO = 1
      GamD = DopplerWidth(T, SigMoy, hitranS%molWeight(1))
      Cte = sqrt ( log(2.0d0) )/GamD
      Cte1 = Cte / sqrt(pi)

      ! initialize
      abs_no_LM(:)   = 0.0d0
      abs_with_LM(:) = 0.0d0

      ! Various Lines
      do iLine=1, SDFS%nLines
        ! Various WaveNumbers
        do isig = 1, size(waveNumbers)
          SigC = waveNumbers(iSig)
          Cte2 = SigC * (1.d0-exp( -hc_kB*SigC/T ))
          ! Complex Probability Function for the "Real" Q-Lines
          XX = (SDFS%modSigLines(iLine)-SigC) * Cte
          YY = SDFS%HWT(iLine) * P * Cte
          Call CPF(errS, XX, YY, WR, WI)
          if (errorCheck(errS)) return
          ! Voigt Absorption Coefficient
          aa = Cte1 * P * SDFS%PopuT(iLine) * (SDFS%Dipo(iLine)**2) * WR
          abs_no_LM(iSig) = abs_no_LM(iSig) + aa * Cte2
          ! First Order Line-Mixing Absorption Coefficient
          bb = Cte1 * P * SDFS%PopuT(iLine) &
             * (SDFS%Dipo(iLine)**2) * (WR - SDFS%YT(iLine) * WI)
          abs_with_LM(iSig)     = abs_with_LM(iSig) + bb * Cte2
        end do
      end do

      ! convert the volume absorption coefficient (cm-1) to absorption cross section (cm2 / molecule)
      !   sigma is the absorption cross section in cm2 / molecule
      !   kabs is the volume absorption coefficient in cm-1
      !   kabs = n_air * xO2 * sigma (by definition)
      !   nair = p / T / kB
      !        with nair in in molecules cm-3
      !        p in hPa
      !        T in Kelvin
      !        kB = 1.380658d-19  Boltzmann's constant (for cm-3, not for m-3)

       nO2 = 1013.25 * P / T / 1.380658d-19  ! in molecules cm-3; assumed vmr = 1.0
       Xsec(:)    =  abs_no_LM(:) / nO2
       Xsec_LM(:) = ( abs_with_LM(:) - abs_no_LM(:) ) / nO2

    end subroutine CalculateLineMixingXsec


    subroutine ConvTP(errS, T, P,  hitranS, SDFS, RMFS, SigMoy)

      ! "ConvTP": CONVert to Temperature and Pressure
      ! ........................................................
      ! .   Subroutine to convert the data read by Subroutine  .
      ! .   "Readline" to the condition of temperature 'T' and .
      ! .   pressure 'Ptot' for all retained lines             .
      ! ........................................................

      ! Input/Output Parameters of Routine (Arguments or Common)
      ! ----------------------------------
      ! nLines  : Number of lines of A band (Input).
      ! T       : Temperature in Kelvin (Input).
      ! P       : Total Pressure in Atmosphere (Input).
      ! SigMoy  : The Population-Averaged value of the Positions
      !           of the Lines in the current band (Output).
      !
      ! Other important Output Quantities (through Common Statements)
      ! ----------------------------------
      ! HWT     : Air Broadened HalfWidths of the Lines for the
      !          Considered Temperature and Pressure (Cm-1)
      ! PopuT   : Populations of the Lower Levels of the Lines
      !          at Temperature T
      ! YT      : Air Broadened First Order Line Mixing Coefficients
      !           of the Lines for the Considered Temperature and
      !           Pressure (No Unit)
      ! .........................................................
      ! Called Routines: "QT_O2"      (Partition Function of O2)
      !
      ! Called By: "CalculateLineMixingXsec"
      ! ---------------------------------------------
      !
      ! Double Precision Version

      ! H. Tran, last change 04/02/2008
      ! Modified by Johan de Haan
      ! =========================================================

      implicit none

      ! Arguments
      type(errorType), intent(inout) :: errS
      real(8) :: T, P
      type(HITRANType), intent(in)    :: hitranS
      type( SDFType ),  intent(inout) :: SDFS          ! data for strong lines
      type( RMFType ),  intent(inout) :: RMFS          ! relaxation matrix data (line mixing)
      real(8),          intent(out)   :: SigMoy        ! The Population-Averaged value of the Positions
                                                      ! of the Lines in the current band

      ! Local variables
      real(8) :: QT, QT0, gsi, rapq1
      real(8) :: SumWgt, Wgt, yy

      Integer :: iLine, iLineP, LK, status

      ! Other parameters
      real(8) :: WW0(SDFS%nLines,SDFS%nLines)
      real(8) :: Sup(SDFS%nLines)
      real(8) :: Slow(SDFS%nLines)

      integer, parameter :: NTtabulated = 119
      real(8) :: Ttabulated(NTtabulated)
      real(8) :: Qtabulated(NTtabulated)
      real(8) :: DSpline(NTtabulated)

      real(8), parameter :: A = 1.43877696D0

      ! consider only ISO = 1 for line mixing
      call getTtabulated(errS, NTtabulated, Ttabulated)
      if (errorCheck(errS)) return
      call getxgj(errS, hitranS, hitranS%isotopologue(1), gsi)
      if (errorCheck(errS)) return
      call getQtabulated(errS, hitranS, hitranS%isotopologue(1), NTtabulated, Qtabulated)
      if (errorCheck(errS)) return
      call spline (errS,  Ttabulated, Qtabulated, DSpline, status)
      if (errorCheck(errS)) return
      QT  = splint ( errS, Ttabulated, Qtabulated, DSpline, T , status)
      QT0 = splint ( errS, Ttabulated, Qtabulated, DSpline, T0, status)
      rapQ1 = QT0/QT

      ! Collisional shift
      SDFS%modSigLines(:) = SDFS%sigLines(:) + P * SDFS%SHIFT(:)

      ! Calculation of the population of the lower level, dipole elements
      ! and relaxation matrix elements at temperature T
      do iLine= 1, SDFS%nLines
        SDFS%PopuT(iLine)= SDFS%PopuT0(iLine) * rapq1 &
                         * exp( A * SDFS%E(iLine) * ( 1/T0 - 1/T ) )
        SDFS%Dipo(iLine) = SDFS%Dipo0(iLine) * sqrt( T0/T )
        do iLineP= 1, SDFS%nLines
          LK = iLineP + (iLine-1) * SDFS%nLines
          WW0(iLine,iLineP) = RMFS%WT0(LK) * ( T0/T )** RMFS%BW(LK)
        end do
      end do

      ! Calculation of matrix elements of the upward transitions
      ! from the detailed balance
      do iLine=1, SDFS%nLines
        do iLineP=1,SDFS%nLines
          if (SDFS%E(iLineP).ge.SDFS%E(iLine)) then
            WW0(iLineP,iLine)=WW0(iLine,iLineP)*SDFS%PopuT(iLineP)/SDFS%PopuT(iLine)
          end if
        end do
      end do

      ! Diagonal elements by line-broadening at temperature T
      do iLine=1,SDFS%nLines
        SDFS%HWT(iLine)  = SDFS%HWT0(iLine)*(T0/T)**SDFS%BHW(iLine)
        WW0(iLine,iLine) = SDFS%HWT(iLine)
      end do

      ! Sigmoy
      SigMoy=0.d0
      SumWgt=0.d0
      do iLine=1, SDFS%nLines
        Wgt = SDFS%PopuT(iLine) * (SDFS%Dipo(iLine)**2)
        SigMoy = SigMoy + SDFS%modSigLines(iLine) * Wgt
        SumWgt = SumWgt + Wgt
      end do
      SigMoy = SigMoy / SumWgt

      ! "Renormalization" procedure
      do iLine=1, SDFS%nLines
        Sup(iLine)=0.d0
        Slow(iLine)=0.d0
        do iLineP=1,  SDFS%nLines
          if (iLineP.le.iLine) then
            Sup(iLine) = Sup(iLine) + SDFS%DipoR(iLineP)*WW0(iLineP,iLine)
          else
            Slow(iLine) = Slow(iLine) + SDFS%DipoR(iLineP)*WW0(iLineP,iLine)
          end if
        end do
        do iLineP=1, SDFS%nLines
          if (iLineP.gt.iLine) then
            WW0(iLineP,iLine)=-WW0(iLineP,iLine) * (Sup(iLine)-SDFS%DipoR(iLine) &
                    *(1.0D0-abs(SDFS%m1(iLine))/36.0D0)**2.0D0*0.04D0)/Slow(iLine)
            WW0(iLine,iLineP) = WW0(iLineP,iLine) * SDFS%PopuT(iLine) / SDFS%PopuT(iLineP)
          end if
        end do
      end do

      ! Computation of the first order line-coupling coefficients
      do iLine = 1, SDFS%nLines
        YY=0.D0
        do iLineP = 1, SDFS%nLines
          if (iLine.ne.iLineP.and.SDFS%modSigLines(iLine).ne.SDFS%modSigLines(iLineP)) then
            YY = YY + 2*SDFS%Dipo(iLineP)/SDFS%Dipo(iLine)* WW0(iLineP,iLine) &
              /(SDFS%modSigLines(iLine) - SDFS%modSigLines(iLineP))
          endif
        end do
        SDFS%YT(iLine) = P*YY
      end do

    end subroutine ConvTP


    subroutine CPF(errS, X,Y,WR,WI)

      ! "CPF": Complex Probability Function
      ! .................................................
      ! .   Subroutine to Compute the Complex           .
      ! .   Probability Function W(z=X+iY)              .
      ! .   W(z)=exp(-z**2)*Erfc(-i*z) with Y>=0        .
      ! .   Which Appears when Convoluting a Complex    .
      ! .   Lorentzian Profile by a Gaussian Shape      .
      ! .................................................

      ! WR       : Real Part of W(z)
      ! WI       : Imaginary Part of W(z)

      ! This Routine was Taken from the Paper by J. Humlicek, which
      ! is Available in Page 309 of Volume 21 of the 1979 Issue of
      ! the Journal of Quantitative Spectroscopy and Radiative Transfer
      ! Please Refer to this Paper for More Information

      ! Accessed Files:  None
      ! --------------
      !
      ! Called Routines: None
      ! ---------------
      !
      ! Called By: 'CompAbs' (COMPute ABSorpton)
      !
      !
      ! Double Precision Version
      !
      ! J.-M. Hartmann, last change 06 March 1997
      ! ****************************************************************


      implicit none

      type(errorType), intent(inout) :: errS
      real(8), intent(in)  :: X,Y
      real(8), intent(out) :: WR,WI

      integer :: I
      real(8) :: T,U,S,Y1,Y2,Y3,R,R2,D,D1,D2,D3,D4

      Dimension T(6),U(6),S(6)
      Data T/0.314240376d0, 0.947788391d0, 1.59768264d0, 2.27950708d0,  &
             3.020637030d0, 3.8897249d0/
      Data U/1.011728050d0, -0.75197147d0, 1.2557727d-2, 1.00220082d-2, &
            -2.42068135d-4, 5.00848061d-7/
      Data S/1.393237d0, 0.231152406d0, -0.155351466d0, 6.21836624d-3,  &
             9.19082986d-5, -6.27525958d-7/

      WR=0.0d0
      WI=0.0d0
      Y1=Y+1.5d0
      Y2=Y1*Y1
      if( (Y > 0.85d0) .OR. ( abs(X) < (18.1d0*Y + 1.65d0) ) ) then

        ! Region I

        do I=1,6
          R=X-T(I)
          D=1.d0/(R*R+Y2)
          D1=Y1*D
          D2=R*D
          R=X+T(I)
          D=1.d0/(R*R+Y2)
          D3=Y1*D
          D4=R*D
          WR=WR+U(I)*(D1+D3)-S(I)*(D2-D4)
          WI=WI+U(I)*(D2+D4)+S(I)*(D1-D3)
        end do

      else

        ! Region II

        If( DABS(X).LT.12.d0 ) WR=DEXP(-X*X)
        Y3=Y+3.d0
        do I=1,6
          R=X-T(I)
          R2=R*R
          D=1.d0/(R2+Y2)
          D1=Y1*D
          D2=R*D
          WR=WR+Y*(U(I)*(R*D2-1.5d0*D1)+S(I)*Y3*D2)/(R2+2.25d0)
          R=X+T(I)
          R2=R*R
          D=1.d0/(R2+Y2)
          D3=Y1*D
          D4=R*D
          WR=WR+Y*(U(I)*(R*D4-1.5d0*D3)-S(I)*Y3*D4)/(R2+2.25d0)
          WI=WI+U(I)*(D2+D4)+S(I)*(D1-D3)
        end do

      end if

    end subroutine CPF


    function DopplerWidth (T, sig, aMass)

      ! Simple function to calculate the Doppler width
      ! used in CompAbs and CompAbsOthers.

      ! Maarten Sneep, 2011.
      ! JdH: This is the Doppler half-width at half max that is calculated
      ! JdH: After multiplication of aMass with Avogadro's number NA we get the mass in gram, not in kg
      !      hence we need to divide the mass by 1.0d3 to get the mass in kg))

      implicit none

      real(8), intent(in) :: T      ! temperature in K
      real(8), intent(in) :: sig    ! wavenumber in cm-1
      real(8), intent(in) :: aMass  ! mass
      real(8)             :: DopplerWidth

      ! calculation of $\gamma_D$
      DopplerWidth = sqrt( 2.0d0 * log(2.0d0) * gasconstant / (c**2.0)) &
                   * sqrt( T ) / sqrt( aMass / 1.0d3 ) * sig

    end function DopplerWidth


    subroutine readLineParameters(errS,  filterStrongLinesO2A, fileName, cutoff, wavelStart, wavelEnd, hitranS )

      ! Read a HITRAN like file with line parameters with the name fileName.
      ! filename can also be a path with respect to the directory where the executable is stored
      ! Only those lines between wavenumberStart - cutoff  and  wavenumberEnd + cutoff are read / stored
      ! where the wavenumbers are calculated from the wavelengths
      ! cutoff is in cm-1; wavelStart and wavelEnd are in nm
      ! The results are stored in the structure hitranS

      implicit none

      type(errorType), intent(inout) :: errS
      logical,              intent(in)    :: filterStrongLinesO2A
      character(LEN=250),   intent(in)    :: fileName
      real(8),              intent(in)    :: cutoff
      real(8),              intent(in)    :: wavelStart, wavelEnd
      type(HITRANType),     intent(inout) :: hitranS

      ! local
      character(LEN=300) :: line
      logical :: cycleFlag
      integer :: ios, nline, i, status
      integer :: iISO, isotope_read, gasIndex_read
      integer :: ic1_read, ic2_read, Nf_read
      real(8) :: wavenumberStart, wavenumberEnd
      real(8) :: Sig_read, S_read, gam_read
      real(8) :: E_read, bet_read, delt_read
      type(file_type) :: file

      logical, parameter :: verbose = .false.

      call enter('readLineParameters')

      ios = load_file(fileName, file)
      if(ios.ne.0) then
        write(errS%temp,'(2A)') 'The file ', trim(fileName)
        call errorAddLine(errS, errS%temp)
        write(errS%temp,'(A)')  'could not be opened'
        call errorAddLine(errS, errS%temp)
        call mystop(errS, 'Stopped with an error')
        if (errorCheck(errS)) return
      endif

      wavenumberStart = 1.0d7 / wavelEnd
      wavenumberEnd   = 1.0d7 / wavelStart

      ! initialize
      nline        = 0
      isotope_read = 1
      Sig_read     = 100000.0d0
      S_read       = 100000.0d0

      ! read away line parameters that do not contribute
      do
        status = file_readline(file, line)
        if (status /= 0) goto 200
        read(line, '(i2,i1,f12.6)', end=200) gasIndex_read, isotope_read, Sig_read
        if ( Sig_read < wavenumberStart - cutoff ) then
          cycle
        else
          status = file_backspace(file)
          if (status /= 0) goto 200
          exit
        end if
      end do

      do
        status = file_readline(file, line)
        if (status /= 0) goto 200
        if ( (filename == O2_XsecFileName) .and. filterStrongLinesO2A ) then
          read(line, '(i2,i1,f12.6,e10.3,10x,f5.4,5x,f10.4,f4.2,f8.6,2i3,10x,i2)', end=200) &
            gasIndex_read, isotope_read, Sig_read, S_read, E_read, gam_read, bet_read, delt_read, ic1_read, ic2_read, Nf_read
        else
          read(line, '(i2,i1,f12.6)', end=200) gasIndex_read, isotope_read, Sig_read
        end if

        if ( gasIndex_read /= hitranS%gasIndex ) then
          call logDebug('ERROR: gasIndex in HITRAN file is not correct')
          call logDebug('the file name used is      : ' // trim(fileName))
          call logDebugI('the gasIndex in the file is: ', gasIndex_read)
          call logDebugI('the gasIndex expected is   : ', hitranS%gasIndex)
          call mystop(errS, 'stopped because of wrong HITRAN file')
          if (errorCheck(errS)) return
        end if

        ! ignore isotope numbers with values beyond hitranS%nISO
        if ( (isotope_read < 1 ) .or. (isotope_read > hitranS%nISO ) ) cycle

        !if ( (isotope_read < 1 ) .or. (isotope_read > hitranS%nISO ) ) then
        !  call mystop(errS, 'stopped because of wrong isotope number - 1')
        !end if

        ! skip lines with the wrong isotope number
        cycleFlag = .true.
        do iISO = 1, hitranS%nISO
          if ( isotope_read == hitranS%ISO(iISO) ) then
            cycleFlag = .false.
            exit ! exit iISO loop
          end if
        end do ! iISO

        if ( cycleFlag ) cycle  ! read next line

        ! skip strong lines if filterStrongLinesO2A is .true. and we deal with the O2 A-band
        if ( (filename == O2_XsecFileName) .and. filterStrongLinesO2A ) then
          if ( (isotope_read == 1) .and. (ic1_read == 5) .and. (ic2_read == 1 ) .and. ( Nf_read <= 35) ) cycle
        end if

        nline = nline + 1

        if ( Sig_read > wavenumberEnd + cutoff ) then
          nline = nline - 1
          exit ! exit do loop
        end if

      end do

  200 continue

      file%cursor = 1

      hitranS%nlines = nline  ! set the number of lines in the data structure

      call claimMemHITRAN_lineParameters (errS, hitranS)
      if (errorCheck(errS)) return

      ! read away line parameters that do not contribute
      do
        status = file_readline(file, line)
        if (status /= 0) goto 300
        read(line, '(i2,i1,f12.6)', end=300) gasIndex_read, isotope_read, Sig_read
        if ( Sig_read < wavenumberStart - cutoff ) then
          cycle
        else
          status = file_backspace(file)
          if (status /= 0) goto 300
          exit
        end if
      end do

      i = 1

      do
        status = file_readline(file, line)
        if (status /= 0) goto 300
        if ( (filename == O2_XsecFileName) .and. filterStrongLinesO2A ) then
          read(line, '(i2,i1,f12.6,e10.3,10x,f5.4,5x,f10.4,f4.2,f8.6,2i3,10x,i2)', end=300) &
            gasIndex_read, isotope_read, Sig_read, S_read, gam_read, E_read, bet_read, delt_read, ic1_read, ic2_read, Nf_read
        else
          read(line, '(i2,i1,f12.6,e10.3,10x,f5.4,5x,f10.4,f4.2,f8.6 )', end=300) &
            gasIndex_read, isotope_read, Sig_read, S_read, gam_read, E_read, bet_read, delt_read
        end if

        ! ignore isotope numbers with values beyond hitranS%nISO
        if ( (isotope_read < 1 ) .or. (isotope_read > hitranS%nISO ) ) cycle

        !!if ( (isotope_read < 1 ) .or. (isotope_read > hitranS%nISO ) ) then
        !!  call mystop(errS, 'stopped because of wrong isotope number - 2')
        !!end if

        ! skip lines with the wrong isotope number
        cycleFlag = .true.
        do iISO = 1, hitranS%nISO
          if ( isotope_read == hitranS%ISO(iISO) ) then
            cycleFlag = .false.
            exit ! exit iISO loop
          end if
        end do ! iISO

        if ( cycleFlag ) cycle  ! read next line

        ! skip strong lines
        if ( (filename == O2_XsecFileName) .and. filterStrongLinesO2A ) then
          if ( (isotope_read == 1) .and. (ic1_read == 5) .and. (ic2_read == 1 ) .and. ( Nf_read <= 35) ) cycle
        end if

        if ( Sig_read > wavenumberEnd + cutoff ) then

          exit ! exit do loop

        else

          hitranS%isotope(i) = isotope_read
          hitranS%Sig(i)     = Sig_read
          hitranS%S(i)       = S_read
          hitranS%gam(i)     = gam_read
          hitranS%E(i)       = E_read
          hitranS%bet(i)     = bet_read
          hitranS%delt(i)    = delt_read

          i = i + 1

        end if

      end do

  300 continue

      if ( verbose ) then
        write(intermediateFileUnit,'(A)') 'readLineParameters verbose output'
        write(intermediateFileUnit,'(A)') 'isotope Sig  S  gam E bet delt'
        do i = 1, hitranS%nlines
          write(intermediateFileUnit,'(I3, F15.6, 5E15.6)') hitranS%isotope(i), hitranS%Sig(i), &
            hitranS%S(i), hitranS%gam(i), hitranS%E(i), hitranS%bet(i), hitranS%delt(i)
        end do
      end if ! verbose

      call exit('readLineParameters')

    end subroutine readLineParameters


    subroutine readLinePositions(errS, fileName, cutoff, wavelStart, wavelEnd, hitranS)

      ! Read the line positions and line strength from a HITRAN like file with the name fileName.
      ! filename can also be a path with respect to the directory where the executable is stored
      ! Only those lines between wavenumberStart - cutoff  and  wavenumberEnd + cutoff are read / stored
      ! where the wavenumbers are calculated from the wavelengths
      ! cutoff is in cm-1; wavelStart and wavelEnd are in nm
      ! The results are stored in the structure hitranS

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=250),   intent(in)    :: fileName
      real(8),              intent(in)    :: cutoff
      real(8),              intent(in)    :: wavelStart, wavelEnd
      type(HITRANType),     intent(inout) :: hitranS

      ! local
      character(LEN=300) :: line
      logical :: cycleFlag
      integer :: ios, nline, i
      integer :: iISO, isotope_read, gasIndex_read
      real(8) :: wavenumberStart, wavenumberEnd
      real(8) :: Sig_read, S_read
      type(file_type) :: file

      logical, parameter :: verbose = .false.

      call enter('readLinePositions')

      ios = load_file(fileName, file)
      if(ios.ne.0) then
        write(errS%temp,'(2A)') 'The file ', trim(fileName)
        call errorAddLine(errS, errS%temp)
        write(errS%temp,'(A)')  'could not be opened'
        call errorAddLine(errS, errS%temp)
        call mystop(errS, 'Stopped with an error')
        if (errorCheck(errS)) return
      endif

      wavenumberStart = 1.0d7 / wavelEnd
      wavenumberEnd   = 1.0d7 / wavelStart

      ! initialize
      nline = 0
      isotope_read = 1
      Sig_read     = 100000.d0
      S_read       = 100000.d0
      do
        ios = file_readline(file, line)
        if (ios /= 0) goto 200
        read(line, '(i2,i1,f12.6)', end=200) gasIndex_read, isotope_read, Sig_read

        if ( gasIndex_read /= hitranS%gasIndex ) then
          call logDebug('ERROR: gasIndex in HITRAN file is not correct')
          call logDebug('the file name used is      : ' // trim(fileName))
          call logDebugI('the gasIndex in the file is: ', gasIndex_read)
          call logDebugI('the gasIndex expected is   : ', hitranS%gasIndex)
          call mystop(errS, 'stopped because of wrong HITRAN file')
          if (errorCheck(errS)) return
        end if

        ! ignore isotope numbers with values beyond hitranS%nISO
        if ( (isotope_read < 1 ) .or. (isotope_read > hitranS%nISO ) ) cycle

        !if ( (isotope_read < 1 ) .or. (isotope_read > hitranS%nISO ) ) then
        !  call mystop(errS, 'stopped because of wrong isotope number - 3')
        !end if

        if ( Sig_read < wavenumberStart - cutoff ) cycle

        ! skip lines with the wrong isotope number
        cycleFlag = .true.
        do iISO = 1, hitranS%nISO
          if ( isotope_read == hitranS%ISO(iISO) ) then
            cycleFlag = .false.
            exit ! exit iISO loop
          end if
        end do ! iISO

        if ( cycleFlag ) cycle  ! read next line

        nline = nline + 1

        if ( Sig_read > wavenumberEnd + cutoff ) then
          nline = nline - 1
          exit ! exit do loop
        end if

      end do

  200 continue

      file%cursor = 1

      hitranS%nlines = nline  ! set the number of lines in the data structure

      call claimMemHITRAN_lineParameters (errS, hitranS)
      if (errorCheck(errS)) return

      i = 1
      do
        ios = file_readline(file, line)
        if (ios /= 0) goto 300
        read(line, '(2x,i1,f12.6,e10.3)', end=300) &
          isotope_read, Sig_read, S_read

        ! ignore isotope numbers with values beyond hitranS%nISO
        if ( (isotope_read < 1 ) .or. (isotope_read > hitranS%nISO ) ) cycle

        !if ( (isotope_read < 1 ) .or. (isotope_read > hitranS%nISO ) ) then
        !  call mystop(errS, 'stopped because of wrong isotope number - 4')
        !end if

        if ( Sig_read < wavenumberStart - cutoff ) cycle

        ! skip lines with the wrong isotope number
        cycleFlag = .true.
        do iISO = 1, hitranS%nISO
          if ( isotope_read == hitranS%ISO(iISO) ) then
            cycleFlag = .false.
            exit ! exit iISO loop
          end if
        end do ! iISO

        if ( cycleFlag ) cycle  ! read next line

        if ( Sig_read > wavenumberEnd + cutoff ) then

          exit ! exit do loop

        else

          hitranS%isotope(i) = isotope_read
          hitranS%Sig(i)     = Sig_read
          hitranS%S(i)       = S_read

          i = i + 1

        end if

      end do

  300 continue

      if ( verbose ) then
        write(intermediateFileUnit,'(A)') 'readLinePositions verbose output'
        write(intermediateFileUnit,'(A)') 'isotope Sig  S'
        do i = 1, hitranS%nlines
          write(intermediateFileUnit,'(I3, F15.6, E15.6)') hitranS%isotope(i), hitranS%Sig(i), hitranS%S(i)
        end do
      end if ! verbose

      call exit('readLinePositions')

    end subroutine readLinePositions


    subroutine readSDF(errS, fileName, SDFS)

      ! Read the file 'SDF.dat'

      implicit none

      type(errorType),    intent(inout) :: errS
      character(LEN=250), intent(in)    :: fileName
      type(SDFType)     , intent(inout) :: SDFS

      ! Locals
      character(LEN=1)    :: R
      character(LEN=300)  :: line
      integer             :: ios1, ios2, nline, Nf, i
      real(8)             :: SBHW
      character(LEN= 250) :: second_filename
      type(file_type)     :: file

      real(8) :: sigLines_read

      call enter('readSDF')

      ! initialize
      ios1 = 0
      ios2 = 0

      ios1 = load_file(fileName, file)

      !  The operational version of DISAMAR has a different path than the science version.
      !  Try a different path if opening of the file fails
      if (ios1 .ne. 0 ) then
        second_filename = 'RefSpec/O2A_LISA_SDF.dat'
        ios2 = load_file(second_filename, file)

        if(ios2.ne.0) then
          write(errS%temp,'(2A)') 'The file ', fileName
          call errorAddLine(errS, errS%temp)
          write(errS%temp,'(2A)') 'or the file ', second_filename
          call errorAddLine(errS, errS%temp)
          write(errS%temp,'(A)')  'could not be opened'
          call errorAddLine(errS, errS%temp)
          call mystop(errS, 'Stopped with an error')
          if (errorCheck(errS)) return
        endif
      end if

      ! determine the number of lines
      nline = 0
      do
        ios1 = file_readline(file, line)
        if (ios1 /= 0) goto 200
        read(line,'(f12.6)', end=200) sigLines_read
        nline = nline + 1
      end do

  200 continue

      file%cursor = 1

      SDFS%nlines = nline
      call claimMemSDFS(errS,  SDFS )
      if (errorCheck(errS)) return

      do i = 1, SDFS%nlines
        ios1 = file_readline(file, line)
        if (ios1 /= 0) goto 300
        read(line, '(f12.6,2x,e9.3,2x,e9.3,1x,e9.3,2x,f10.4,2x,f5.4,2x,f4.2,2x,F8.6,4x,A1,i3)', end=300) &
             SDFS%sigLines(i), SDFS%PopuT0(i), SDFS%DipoR(i), SDFS%Dipo0(i), SDFS%E(i), SDFS%HWT0(i), &
             SDFS%BHW(i), SDFS%SHIFT(i), R, Nf

        if (R == 'P') then
          SDFS%m1(i)= -Nf
          Nf = Nf - 1
        elseif (R == 'R') then
          SDFS%m1(i)=Nf+1
          Nf = Nf + 1
        endif

        ! Comments made by Maarten Sneep:

        ! I don't quite understand this part.
        ! The value of HWT0 from the database file is completely
        ! overwritten. I would expect the original value to be used in either
        ! SBHW or for it to appear on the RHS of the HWT0(iLine) assignment.

        ! Read the article: Tran & Hartmann (2008). page 3, paragraph 8.
        ! Direct reference to Yang et al. (2005), caption of figure 2.

        ! Change: all constants are now double precision.
        SBHW = 0.02204d0 + 0.03749d0  &
             / (1.0d0 + 0.05428d0 * Nf - 1.19d-3*Nf**2 + 2.073d-6*Nf**4)

        ! Factor 1.023: Tran & Hartmann (2008). Page 5, 1st column, below eq 1.
        ! rest: Yang et al. (2005), caption of figure 2.
        SDFS%HWT0(i) = 1.023d0*1.012d0*SBHW / sqrt(1.0d0+((Nf-5.0d0)/55.0d0)**2)
      end do

  300 continue

      call exit('readSDF')

    end subroutine readSDF


    subroutine readRMF(errS, fileName, RMFS)

      ! Read the file 'RMF.dat'

      implicit none

      type(errorType), intent(inout) :: errS
      character(LEN=250),   intent(in)    :: fileName
      type(RMFType),        intent(inout) :: RMFS

      ! local variables
      integer             :: ios1, ios2, nRMF, i

      real(8)             :: WT0_read
      character(LEN= 250) :: second_filename
      character(LEN=300)  :: line
      type(file_type)     :: file

      call enter('readRMF')

      ! initialize
      ios1 = 0
      ios2 = 0

      ! Open File with RMF data

      ios1 = load_file(fileName, file)

      ! The operational version of DISAMAR has a different path than the science version.
      ! Try a different path if opening of the file fails
      if (ios1 .ne. 0 ) then
        second_filename = 'RefSpec/O2A_LISA_RMF.dat'
        ios2 = load_file(second_filename, file)

        if(ios2.ne.0) then
          write(errS%temp,'(2A)') 'The file ', fileName
          call errorAddLine(errS, errS%temp)
          write(errS%temp,'(2A)') 'or the file ', second_filename
          call errorAddLine(errS, errS%temp)
          write(errS%temp,'(A)')  'could not be opened'
          call errorAddLine(errS, errS%temp)
          call mystop(errS, 'Stopped with an error')
          if (errorCheck(errS)) return
        endif
      end if

      ! determine the number of lines in the file
      nRMF = 0
      do
        ios1 = file_readline(file, line)
        if (ios1 /= 0) goto 200
        read(line, * , end=200) WT0_read
        nRMF = nRMF + 1
      end do

  200 continue

      file%cursor = 1

      RMFS%nRMF = nRMF

      call claimMemRMFS(errS, RMFS)
      if (errorCheck(errS)) return

      do i = 1, nRMF
        ios1 = file_readline(file, line)
        if (ios1 /= 0) goto 300
        read(line, '(E15.7,F16.12)' , end=300) RMFS%WT0(i), RMFS%BW(i)
      end do

 300  continue

      call exit('readRMF')

    end subroutine readRMF


    subroutine getTtabulated(errS, NTtabulated, Ttabulated)

      ! The partition functions are tabulated for a temperature grid.
      ! Here the temperature grid is defined.

      implicit none

      type(errorType), intent(inout) :: errS
      integer, intent(in)  :: NTtabulated
      real(8), intent(out) :: Ttabulated(NTtabulated)

      ! There must be 119 temperatures here.

      Ttabulated =  (/    60.0d0,  85.0d0, 110.0d0, 135.0d0, 160.0d0, 185.0d0, 210.0d0, 235.0d0, &
       260.0d0, 285.0d0, 310.0d0, 335.0d0, 360.0d0, 385.0d0, 410.0d0, 435.0d0, 460.0d0, 485.0d0, &
       510.0d0, 535.0d0, 560.0d0, 585.0d0, 610.0d0, 635.0d0, 660.0d0, 685.0d0, 710.0d0, 735.0d0, &
       760.0d0, 785.0d0, 810.0d0, 835.0d0, 860.0d0, 885.0d0, 910.0d0, 935.0d0, 960.0d0, 985.0d0, &
      1010.0d0,1035.0d0,1060.0d0,1085.0d0,1110.0d0,1135.0d0,1160.0d0,1185.0d0,1210.0d0,1235.0d0, &
      1260.0d0,1285.0d0,1310.0d0,1335.0d0,1360.0d0,1385.0d0,1410.0d0,1435.0d0,1460.0d0,1485.0d0, &
      1510.0d0,1535.0d0,1560.0d0,1585.0d0,1610.0d0,1635.0d0,1660.0d0,1685.0d0,1710.0d0,1735.0d0, &
      1760.0d0,1785.0d0,1810.0d0,1835.0d0,1860.0d0,1885.0d0,1910.0d0,1935.0d0,1960.0d0,1985.0d0, &
      2010.0d0,2035.0d0,2060.0d0,2085.0d0,2110.0d0,2135.0d0,2160.0d0,2185.0d0,2210.0d0,2235.0d0, &
      2260.0d0,2285.0d0,2310.0d0,2335.0d0,2360.0d0,2385.0d0,2410.0d0,2435.0d0,2460.0d0,2485.0d0, &
      2510.0d0,2535.0d0,2560.0d0,2585.0d0,2610.0d0,2635.0d0,2660.0d0,2685.0d0,2710.0d0,2735.0d0, &
      2760.0d0,2785.0d0,2810.0d0,2835.0d0,2860.0d0,2885.0d0,2910.0d0,2935.0d0,2960.0d0,2985.0d0, &
      3010.0d0/)

    end subroutine getTtabulated


    subroutine getxgj(errS, hitranS, isotopologue, xgj)

      !
      implicit none

      type(errorType), intent(inout) :: errS
      type(HITRANType), intent(in)  :: hitranS
      integer,          intent(in)  :: isotopologue
      real(8),          intent(out) :: xgj

      select case ( hitranS%gasIndex )

        case(1) ! H2O

          select case ( isotopologue )
            case (161)
              xgj =  1.0d0
            case (181)
              xgj =  1.0d0
            case (171)
              xgj =  6.0d0
            case (162)
              xgj =  6.0d0
            case (182)
              xgj =  6.0d0
            case (172)
              xgj = 36.0d0

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no xgj tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for xgj')
              if (errorCheck(errS)) return

          end select

        case(2) ! CO2

          select case ( isotopologue )
            case (626)
              xgj =  1.0d0
            case (636)
              xgj =  2.0d0
            case (628)
              xgj =  1.0d0
            case (627)
              xgj =  6.0d0
            case (638)
              xgj =  2.0d0
            case (637)
              xgj = 12.0d0

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no xgj tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for xgj')
              if (errorCheck(errS)) return

          end select

        case(5) ! CO

          select case ( isotopologue )
            case (26)
              xgj =  1.0d0
            case (36)
              xgj =  2.0d0
            case (28)
              xgj =  1.0d0
            case (27)
              xgj =  6.0d0
            case (38)
              xgj =  2.0d0
            case (37)
              xgj = 12.0d0

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no xgj tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for xgj')
              if (errorCheck(errS)) return

          end select

        case(6) ! CH4

          select case ( isotopologue )

            case (211)
              xgj =  1.0d0
            case (311)
              xgj =  2.0d0
            case (212)
              xgj =  3.0d0

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no Q tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for Q')
              if (errorCheck(errS)) return

          end select

        case(7) ! O2

          select case ( isotopologue )
            case (66)
              xgj =  1.0d0
            case (68)
              xgj =  1.0d0
            case (67)
              xgj =  6.0d0

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no Q tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for Q')
              if (errorCheck(errS)) return

          end select

        case(11) ! NH3

          select case ( isotopologue )
            case (4111)
              xgj =  3.0d0
            case (5111)
              xgj =  2.0d0

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no Q tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for Q')
              if (errorCheck(errS)) return

          end select

        case default

          call logDebug('ERROR: gasIndex not a valid number')
          call logDebug('in subroutine getxgj')
          call logDebug('in module HITRANModule')
          call logDebugI('index = ', hitranS%gasIndex)
          call mystop(errS, 'stopped because gasIndex is not valid')
          if (errorCheck(errS)) return

      end select

    end subroutine getxgj


    subroutine getQtabulated(errS, hitranS, isotopologue, NTtabulated, Qtabulated)

      !
      implicit none

      type(errorType),  intent(inout) :: errS
      type(HITRANType), intent(in)    :: hitranS
      integer,          intent(in)    :: isotopologue
      integer,          intent(in)  :: NTtabulated
      real(8),          intent(out) :: Qtabulated(NTtabulated)

      select case ( hitranS%gasIndex )

        case(1) ! H2O

          select case ( isotopologue )
            case (161)
              Qtabulated =           (/ 0.16824D+02, 0.27771D+02, 0.40408D+02, &
              0.54549D+02, 0.70054D+02, 0.86817D+02, 0.10475D+03, 0.12380D+03, &
              0.14391D+03, 0.16503D+03, 0.18714D+03, 0.21021D+03, 0.23425D+03, &
              0.25924D+03, 0.28518D+03, 0.31209D+03, 0.33997D+03, 0.36883D+03, &
              0.39870D+03, 0.42959D+03, 0.46152D+03, 0.49452D+03, 0.52860D+03, &
              0.56380D+03, 0.60015D+03, 0.63766D+03, 0.67637D+03, 0.71631D+03, &
              0.75750D+03, 0.79999D+03, 0.84380D+03, 0.88897D+03, 0.93553D+03, &
              0.98353D+03, 0.10330D+04, 0.10840D+04, 0.11365D+04, 0.11906D+04, &
              0.12463D+04, 0.13037D+04, 0.13628D+04, 0.14237D+04, 0.14863D+04, &
              0.15509D+04, 0.16173D+04, 0.16856D+04, 0.17559D+04, 0.18283D+04, &
              0.19028D+04, 0.19793D+04, 0.20581D+04, 0.21391D+04, 0.22224D+04, &
              0.23080D+04, 0.24067D+04, 0.24975D+04, 0.25908D+04, 0.26867D+04, &
              0.27853D+04, 0.28865D+04, 0.29904D+04, 0.30972D+04, 0.32068D+04, &
              0.33194D+04, 0.34349D+04, 0.35535D+04, 0.36752D+04, 0.38001D+04, &
              0.39282D+04, 0.40597D+04, 0.41945D+04, 0.43327D+04, 0.44745D+04, &
              0.46199D+04, 0.47688D+04, 0.49215D+04, 0.50780D+04, 0.52384D+04, &
              0.54027D+04, 0.55710D+04, 0.57434D+04, 0.59200D+04, 0.61008D+04, &
              0.62859D+04, 0.64754D+04, 0.66693D+04, 0.68679D+04, 0.70710D+04, &
              0.72788D+04, 0.74915D+04, 0.77090D+04, 0.79315D+04, 0.81590D+04, &
              0.83917D+04, 0.86296D+04, 0.88728D+04, 0.91214D+04, 0.93755D+04, &
              0.96351D+04, 0.99005D+04, 0.10171D+05, 0.10448D+05, 0.10731D+05, &
              0.11020D+05, 0.11315D+05, 0.11617D+05, 0.11924D+05, 0.12238D+05, &
              0.12559D+05, 0.12886D+05, 0.13220D+05, 0.13561D+05, 0.13909D+05, &
              0.14263D+05, 0.14625D+05, 0.14995D+05, 0.15371D+05, 0.15755D+05, &
              0.16147D+05/)

            case (181)
              Qtabulated =           (/ 0.15960D+02, 0.26999D+02, 0.39743D+02, &
              0.54003D+02, 0.69639D+02, 0.86543D+02, 0.10463D+03, 0.12384D+03, &
              0.14412D+03, 0.16542D+03, 0.18773D+03, 0.21103D+03, 0.23531D+03, &
              0.26057D+03, 0.28681D+03, 0.31406D+03, 0.34226D+03, 0.37130D+03, &
              0.40135D+03, 0.43243D+03, 0.46456D+03, 0.49777D+03, 0.53206D+03, &
              0.56748D+03, 0.60405D+03, 0.64179D+03, 0.68074D+03, 0.72093D+03, &
              0.76238D+03, 0.80513D+03, 0.84922D+03, 0.89467D+03, 0.94152D+03, &
              0.98982D+03, 0.10396D+04, 0.10909D+04, 0.11437D+04, 0.11982D+04, &
              0.12543D+04, 0.13120D+04, 0.13715D+04, 0.14328D+04, 0.14959D+04, &
              0.15608D+04, 0.16276D+04, 0.16964D+04, 0.17672D+04, 0.18401D+04, &
              0.19151D+04, 0.19922D+04, 0.20715D+04, 0.21531D+04, 0.22370D+04, &
              0.23232D+04, 0.24118D+04, 0.25030D+04, 0.25967D+04, 0.26929D+04, &
              0.27918D+04, 0.28934D+04, 0.29978D+04, 0.31050D+04, 0.32151D+04, &
              0.33281D+04, 0.34441D+04, 0.35632D+04, 0.36854D+04, 0.38108D+04, &
              0.39395D+04, 0.40715D+04, 0.42070D+04, 0.43459D+04, 0.44883D+04, &
              0.46343D+04, 0.47840D+04, 0.49374D+04, 0.50946D+04, 0.52558D+04, &
              0.54209D+04, 0.55900D+04, 0.57632D+04, 0.59407D+04, 0.61224D+04, &
              0.63084D+04, 0.64988D+04, 0.66938D+04, 0.68933D+04, 0.70975D+04, &
              0.73064D+04, 0.75202D+04, 0.77389D+04, 0.79625D+04, 0.81913D+04, &
              0.84252D+04, 0.86644D+04, 0.89089D+04, 0.91588D+04, 0.94143D+04, &
              0.96754D+04, 0.99422D+04, 0.10215D+05, 0.10493D+05, 0.10778D+05, &
              0.11068D+05, 0.11365D+05, 0.11668D+05, 0.11977D+05, 0.12293D+05, &
              0.12616D+05, 0.12945D+05, 0.13281D+05, 0.13624D+05, 0.13973D+05, &
              0.14330D+05, 0.14694D+05, 0.15066D+05, 0.15445D+05, 0.15831D+05, &
              0.16225D+05/)
            case (171)
              Qtabulated =           (/ 0.95371D+02, 0.16134D+03, 0.23750D+03, &
              0.32273D+03, 0.41617D+03, 0.51722D+03, 0.62540D+03, 0.74036D+03, &
              0.86185D+03, 0.98970D+03, 0.11238D+04, 0.12642D+04, 0.14097D+04, &
              0.15599D+04, 0.17159D+04, 0.18777D+04, 0.20453D+04, 0.22188D+04, &
              0.23983D+04, 0.25840D+04, 0.27760D+04, 0.29743D+04, 0.31792D+04, &
              0.33907D+04, 0.36091D+04, 0.38346D+04, 0.40672D+04, 0.43072D+04, &
              0.45547D+04, 0.48100D+04, 0.50732D+04, 0.53446D+04, 0.56244D+04, &
              0.59128D+04, 0.62100D+04, 0.65162D+04, 0.68317D+04, 0.71567D+04, &
              0.74915D+04, 0.78363D+04, 0.81914D+04, 0.85571D+04, 0.89335D+04, &
              0.93211D+04, 0.97200D+04, 0.10131D+05, 0.10553D+05, 0.10988D+05, &
              0.11435D+05, 0.11895D+05, 0.12368D+05, 0.12855D+05, 0.13356D+05, &
              0.13870D+05, 0.14399D+05, 0.14943D+05, 0.15502D+05, 0.16076D+05, &
              0.16666D+05, 0.17272D+05, 0.17895D+05, 0.18534D+05, 0.19191D+05, &
              0.19865D+05, 0.20557D+05, 0.21267D+05, 0.21996D+05, 0.22744D+05, &
              0.23512D+05, 0.24299D+05, 0.25106D+05, 0.25935D+05, 0.26784D+05, &
              0.27655D+05, 0.28547D+05, 0.29462D+05, 0.30400D+05, 0.31361D+05, &
              0.32345D+05, 0.33353D+05, 0.34386D+05, 0.35444D+05, 0.36527D+05, &
              0.37637D+05, 0.38772D+05, 0.39934D+05, 0.41124D+05, 0.42341D+05, &
              0.43587D+05, 0.44861D+05, 0.46165D+05, 0.47498D+05, 0.48862D+05, &
              0.50256D+05, 0.51682D+05, 0.53139D+05, 0.54629D+05, 0.56152D+05, &
              0.57708D+05, 0.59299D+05, 0.60923D+05, 0.62583D+05, 0.64279D+05, &
              0.66011D+05, 0.67779D+05, 0.69585D+05, 0.71429D+05, 0.73312D+05, &
              0.75234D+05, 0.77195D+05, 0.79197D+05, 0.81240D+05, 0.83325D+05, &
              0.85452D+05, 0.87622D+05, 0.89835D+05, 0.92093D+05, 0.94395D+05, &
              0.96743D+05/)
            case (162)
              Qtabulated =           (/ 0.75792D+02, 0.12986D+03, 0.19244D+03, &
              0.26253D+03, 0.33942D+03, 0.42259D+03, 0.51161D+03, 0.60619D+03, &
              0.70609D+03, 0.81117D+03, 0.92132D+03, 0.10365D+04, 0.11567D+04, &
              0.12820D+04, 0.14124D+04, 0.15481D+04, 0.16891D+04, 0.18355D+04, &
              0.19876D+04, 0.21455D+04, 0.23092D+04, 0.24791D+04, 0.26551D+04, &
              0.28376D+04, 0.30268D+04, 0.32258D+04, 0.34288D+04, 0.36392D+04, &
              0.38571D+04, 0.40828D+04, 0.43165D+04, 0.45584D+04, 0.48089D+04, &
              0.50681D+04, 0.53363D+04, 0.56139D+04, 0.59009D+04, 0.61979D+04, &
              0.65049D+04, 0.68224D+04, 0.71506D+04, 0.74898D+04, 0.78403D+04, &
              0.82024D+04, 0.85765D+04, 0.89628D+04, 0.93618D+04, 0.97736D+04, &
              0.10199D+05, 0.10637D+05, 0.11090D+05, 0.11557D+05, 0.12039D+05, &
              0.12535D+05, 0.13047D+05, 0.13575D+05, 0.14119D+05, 0.14679D+05, &
              0.15257D+05, 0.15851D+05, 0.16464D+05, 0.17094D+05, 0.17743D+05, &
              0.18411D+05, 0.19098D+05, 0.19805D+05, 0.20532D+05, 0.21280D+05, &
              0.22049D+05, 0.22840D+05, 0.23652D+05, 0.24487D+05, 0.25345D+05, &
              0.26227D+05, 0.27132D+05, 0.28062D+05, 0.29016D+05, 0.29997D+05, &
              0.31002D+05, 0.32035D+05, 0.33094D+05, 0.34180D+05, 0.35295D+05, &
              0.36438D+05, 0.37610D+05, 0.38812D+05, 0.40044D+05, 0.41306D+05, &
              0.42600D+05, 0.43926D+05, 0.45284D+05, 0.46675D+05, 0.48100D+05, &
              0.49559D+05, 0.51053D+05, 0.52583D+05, 0.54148D+05, 0.55750D+05, &
              0.57390D+05, 0.59067D+05, 0.60783D+05, 0.62539D+05, 0.64334D+05, &
              0.66170D+05, 0.68047D+05, 0.69967D+05, 0.71929D+05, 0.73934D+05, &
              0.75983D+05, 0.78078D+05, 0.80217D+05, 0.82403D+05, 0.84636D+05, &
              0.86917D+05, 0.89246D+05, 0.91625D+05, 0.94053D+05, 0.96533D+05, &
              0.99064D+05/)
            case (182)
              Qtabulated =           (/ 0.82770D+02, 0.13749D+03, 0.20083D+03, &
              0.27176D+03, 0.34955D+03, 0.43370D+03, 0.52376D+03, 0.61944D+03, &
              0.72050D+03, 0.82679D+03, 0.93821D+03, 0.10547D+04, 0.11763D+04, &
              0.13031D+04, 0.14350D+04, 0.15723D+04, 0.17150D+04, 0.18633D+04, &
              0.20172D+04, 0.21770D+04, 0.23429D+04, 0.25149D+04, 0.26934D+04, &
              0.28784D+04, 0.30702D+04, 0.32690D+04, 0.34750D+04, 0.36885D+04, &
              0.39096D+04, 0.41386D+04, 0.43758D+04, 0.46213D+04, 0.48755D+04, &
              0.51386D+04, 0.54109D+04, 0.56927D+04, 0.59841D+04, 0.62856D+04, &
              0.65973D+04, 0.69197D+04, 0.72529D+04, 0.75973D+04, 0.79533D+04, &
              0.83210D+04, 0.87009D+04, 0.90933D+04, 0.94985D+04, 0.99168D+04, &
              0.10348D+05, 0.10794D+05, 0.11254D+05, 0.11728D+05, 0.12217D+05, &
              0.12722D+05, 0.13242D+05, 0.13778D+05, 0.14331D+05, 0.14900D+05, &
              0.15486D+05, 0.16091D+05, 0.16713D+05, 0.17353D+05, 0.18012D+05, &
              0.18691D+05, 0.19389D+05, 0.20108D+05, 0.20847D+05, 0.21607D+05, &
              0.22388D+05, 0.23191D+05, 0.24017D+05, 0.24866D+05, 0.25738D+05, &
              0.26633D+05, 0.27553D+05, 0.28498D+05, 0.29468D+05, 0.30464D+05, &
              0.31486D+05, 0.32536D+05, 0.33612D+05, 0.34716D+05, 0.35849D+05, &
              0.37011D+05, 0.38202D+05, 0.39424D+05, 0.40676D+05, 0.41959D+05, &
              0.43274D+05, 0.44622D+05, 0.46002D+05, 0.47416D+05, 0.48864D+05, &
              0.50348D+05, 0.51866D+05, 0.53421D+05, 0.55012D+05, 0.56640D+05, &
              0.58307D+05, 0.60012D+05, 0.61757D+05, 0.63541D+05, 0.65366D+05, &
              0.67233D+05, 0.69141D+05, 0.71092D+05, 0.73087D+05, 0.75125D+05, &
              0.77209D+05, 0.79338D+05, 0.81513D+05, 0.83736D+05, 0.86006D+05, &
              0.88324D+05, 0.90693D+05, 0.93111D+05, 0.95580D+05, 0.98100D+05, &
              0.10067D+06/)
            case (172)
              Qtabulated =           (/ 0.49379D+03, 0.82021D+03, 0.11980D+04, &
              0.16211D+04, 0.20851D+04, 0.25870D+04, 0.31242D+04, 0.36949D+04, &
              0.42977D+04, 0.49317D+04, 0.55963D+04, 0.62911D+04, 0.70164D+04, &
              0.77722D+04, 0.85591D+04, 0.93777D+04, 0.10228D+05, 0.11112D+05, &
              0.12030D+05, 0.12983D+05, 0.13971D+05, 0.14997D+05, 0.16061D+05, &
              0.17163D+05, 0.18306D+05, 0.19491D+05, 0.20719D+05, 0.21991D+05, &
              0.23309D+05, 0.24673D+05, 0.26086D+05, 0.27549D+05, 0.29064D+05, &
              0.30631D+05, 0.32254D+05, 0.33932D+05, 0.35669D+05, 0.37464D+05, &
              0.39321D+05, 0.41242D+05, 0.43227D+05, 0.45279D+05, 0.47399D+05, &
              0.49589D+05, 0.51852D+05, 0.54189D+05, 0.56602D+05, 0.59094D+05, &
              0.61666D+05, 0.64320D+05, 0.67058D+05, 0.69883D+05, 0.72796D+05, &
              0.75801D+05, 0.78899D+05, 0.82092D+05, 0.85382D+05, 0.88773D+05, &
              0.92266D+05, 0.95863D+05, 0.99568D+05, 0.10338D+06, 0.10731D+06, &
              0.11135D+06, 0.11551D+06, 0.11979D+06, 0.12419D+06, 0.12871D+06, &
              0.13337D+06, 0.13815D+06, 0.14307D+06, 0.14812D+06, 0.15331D+06, &
              0.15865D+06, 0.16412D+06, 0.16975D+06, 0.17553D+06, 0.18146D+06, &
              0.18754D+06, 0.19379D+06, 0.20020D+06, 0.20678D+06, 0.21352D+06, &
              0.22044D+06, 0.22753D+06, 0.23480D+06, 0.24226D+06, 0.24990D+06, &
              0.25773D+06, 0.26575D+06, 0.27397D+06, 0.28239D+06, 0.29102D+06, &
              0.29985D+06, 0.30889D+06, 0.31814D+06, 0.32762D+06, 0.33731D+06, &
              0.34724D+06, 0.35739D+06, 0.36777D+06, 0.37840D+06, 0.38926D+06, &
              0.40038D+06, 0.41174D+06, 0.42335D+06, 0.43523D+06, 0.44737D+06, &
              0.45977D+06, 0.47245D+06, 0.48540D+06, 0.49863D+06, 0.51214D+06, &
              0.52595D+06, 0.54005D+06, 0.55444D+06, 0.56914D+06, 0.58415D+06, &
              0.59947D+06/)

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no Q tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for Q')
              if (errorCheck(errS)) return

          end select

        case(2) ! CO2

          select case ( isotopologue )
            case (626)
              Qtabulated =           (/ 0.53642D+02, 0.75947D+02, 0.98292D+02, &
              0.12078D+03, 0.14364D+03, 0.16714D+03, 0.19160D+03, 0.21731D+03, &
              0.24454D+03, 0.27355D+03, 0.30456D+03, 0.33778D+03, 0.37343D+03, &
              0.41170D+03, 0.45280D+03, 0.49692D+03, 0.54427D+03, 0.59505D+03, &
              0.64948D+03, 0.70779D+03, 0.77019D+03, 0.83693D+03, 0.90825D+03, &
              0.98440D+03, 0.10656D+04, 0.11522D+04, 0.12445D+04, 0.13427D+04, &
              0.14471D+04, 0.15580D+04, 0.16759D+04, 0.18009D+04, 0.19334D+04, &
              0.20739D+04, 0.22225D+04, 0.23798D+04, 0.25462D+04, 0.27219D+04, &
              0.29074D+04, 0.31032D+04, 0.33097D+04, 0.35272D+04, 0.37564D+04, &
              0.39976D+04, 0.42514D+04, 0.45181D+04, 0.47985D+04, 0.50929D+04, &
              0.54019D+04, 0.57260D+04, 0.60659D+04, 0.64221D+04, 0.67952D+04, &
              0.71859D+04, 0.75946D+04, 0.80222D+04, 0.84691D+04, 0.89362D+04, &
              0.94241D+04, 0.99335D+04, 0.10465D+05, 0.11020D+05, 0.11598D+05, &
              0.12201D+05, 0.12828D+05, 0.13482D+05, 0.14163D+05, 0.14872D+05, &
              0.15609D+05, 0.16376D+05, 0.17173D+05, 0.18001D+05, 0.18861D+05, &
              0.19754D+05, 0.20682D+05, 0.21644D+05, 0.22643D+05, 0.23678D+05, &
              0.24752D+05, 0.25865D+05, 0.27018D+05, 0.28212D+05, 0.29449D+05, &
              0.30730D+05, 0.32055D+05, 0.33426D+05, 0.34845D+05, 0.36312D+05, &
              0.37828D+05, 0.39395D+05, 0.41015D+05, 0.42688D+05, 0.44416D+05, &
              0.46199D+05, 0.48041D+05, 0.49942D+05, 0.51902D+05, 0.53925D+05, &
              0.56011D+05, 0.58162D+05, 0.60379D+05, 0.62664D+05, 0.65019D+05, &
              0.67444D+05, 0.69942D+05, 0.72515D+05, 0.75163D+05, 0.77890D+05, &
              0.80695D+05, 0.83582D+05, 0.86551D+05, 0.89605D+05, 0.92746D+05, &
              0.95975D+05, 0.99294D+05, 0.10271D+06, 0.10621D+06, 0.10981D+06, &
              0.11351D+06/)
            case (636)
              Qtabulated =           (/ 0.10728D+03, 0.15189D+03, 0.19659D+03, &
              0.24164D+03, 0.28753D+03, 0.33486D+03, 0.38429D+03, 0.43643D+03, &
              0.49184D+03, 0.55104D+03, 0.61449D+03, 0.68263D+03, 0.75589D+03, &
              0.83468D+03, 0.91943D+03, 0.10106D+04, 0.11085D+04, 0.12137D+04, &
              0.13266D+04, 0.14477D+04, 0.15774D+04, 0.17163D+04, 0.18649D+04, &
              0.20237D+04, 0.21933D+04, 0.23743D+04, 0.25673D+04, 0.27729D+04, &
              0.29917D+04, 0.32245D+04, 0.34718D+04, 0.37345D+04, 0.40132D+04, &
              0.43087D+04, 0.46218D+04, 0.49533D+04, 0.53041D+04, 0.56749D+04, &
              0.60668D+04, 0.64805D+04, 0.69171D+04, 0.73774D+04, 0.78626D+04, &
              0.83736D+04, 0.89114D+04, 0.94772D+04, 0.10072D+05, 0.10697D+05, &
              0.11353D+05, 0.12042D+05, 0.12765D+05, 0.13523D+05, 0.14317D+05, &
              0.15148D+05, 0.16019D+05, 0.16930D+05, 0.17883D+05, 0.18879D+05, &
              0.19920D+05, 0.21008D+05, 0.22143D+05, 0.23328D+05, 0.24563D+05, &
              0.25852D+05, 0.27195D+05, 0.28594D+05, 0.30051D+05, 0.31568D+05, &
              0.33146D+05, 0.34788D+05, 0.36496D+05, 0.38271D+05, 0.40115D+05, &
              0.42031D+05, 0.44021D+05, 0.46086D+05, 0.48230D+05, 0.50453D+05, &
              0.52759D+05, 0.55150D+05, 0.57628D+05, 0.60195D+05, 0.62854D+05, &
              0.65608D+05, 0.68459D+05, 0.71409D+05, 0.74461D+05, 0.77618D+05, &
              0.80883D+05, 0.84258D+05, 0.87746D+05, 0.91350D+05, 0.95073D+05, &
              0.98918D+05, 0.10289D+06, 0.10698D+06, 0.11121D+06, 0.11558D+06, &
              0.12008D+06, 0.12472D+06, 0.12950D+06, 0.13443D+06, 0.13952D+06, &
              0.14475D+06, 0.15015D+06, 0.15571D+06, 0.16143D+06, 0.16732D+06, &
              0.17338D+06, 0.17962D+06, 0.18604D+06, 0.19264D+06, 0.19943D+06, &
              0.20642D+06, 0.21360D+06, 0.22098D+06, 0.22856D+06, 0.23636D+06, &
              0.24436D+06/)
            case (628)
              Qtabulated =           (/ 0.11368D+03, 0.16096D+03, 0.20833D+03, &
              0.25603D+03, 0.30452D+03, 0.35442D+03, 0.40640D+03, 0.46110D+03, &
              0.51910D+03, 0.58093D+03, 0.64709D+03, 0.71804D+03, 0.79422D+03, &
              0.87607D+03, 0.96402D+03, 0.10585D+04, 0.11600D+04, 0.12689D+04, &
              0.13857D+04, 0.15108D+04, 0.16449D+04, 0.17883D+04, 0.19416D+04, &
              0.21054D+04, 0.22803D+04, 0.24668D+04, 0.26655D+04, 0.28770D+04, &
              0.31021D+04, 0.33414D+04, 0.35956D+04, 0.38654D+04, 0.41516D+04, &
              0.44549D+04, 0.47761D+04, 0.51160D+04, 0.54755D+04, 0.58555D+04, &
              0.62568D+04, 0.66804D+04, 0.71273D+04, 0.75982D+04, 0.80944D+04, &
              0.86169D+04, 0.91666D+04, 0.97446D+04, 0.10352D+05, 0.10990D+05, &
              0.11660D+05, 0.12363D+05, 0.13101D+05, 0.13874D+05, 0.14683D+05, &
              0.15531D+05, 0.16418D+05, 0.17347D+05, 0.18317D+05, 0.19332D+05, &
              0.20392D+05, 0.21499D+05, 0.22654D+05, 0.23859D+05, 0.25116D+05, &
              0.26426D+05, 0.27792D+05, 0.29214D+05, 0.30695D+05, 0.32236D+05, &
              0.33840D+05, 0.35508D+05, 0.37242D+05, 0.39045D+05, 0.40917D+05, &
              0.42862D+05, 0.44881D+05, 0.46977D+05, 0.49152D+05, 0.51407D+05, &
              0.53746D+05, 0.56171D+05, 0.58683D+05, 0.61286D+05, 0.63981D+05, &
              0.66772D+05, 0.69661D+05, 0.72650D+05, 0.75742D+05, 0.78940D+05, &
              0.82246D+05, 0.85664D+05, 0.89196D+05, 0.92845D+05, 0.96613D+05, &
              0.10050D+06, 0.10452D+06, 0.10867D+06, 0.11295D+06, 0.11736D+06, &
              0.12191D+06, 0.12661D+06, 0.13145D+06, 0.13643D+06, 0.14157D+06, &
              0.14687D+06, 0.15232D+06, 0.15794D+06, 0.16372D+06, 0.16968D+06, &
              0.17580D+06, 0.18211D+06, 0.18859D+06, 0.19526D+06, 0.20213D+06, &
              0.20918D+06, 0.21643D+06, 0.22388D+06, 0.23154D+06, 0.23941D+06, &
              0.24750D+06/)
            case (627)
              Qtabulated =           (/ 0.66338D+03, 0.93923D+03, 0.12156D+04, &
              0.14938D+04, 0.17766D+04, 0.20676D+04, 0.23705D+04, 0.26891D+04, &
              0.30267D+04, 0.33866D+04, 0.37714D+04, 0.41839D+04, 0.46267D+04, &
              0.51023D+04, 0.56132D+04, 0.61618D+04, 0.67508D+04, 0.73827D+04, &
              0.80603D+04, 0.87863D+04, 0.95636D+04, 0.10395D+05, 0.11284D+05, &
              0.12233D+05, 0.13246D+05, 0.14326D+05, 0.15477D+05, 0.16702D+05, &
              0.18005D+05, 0.19390D+05, 0.20861D+05, 0.22422D+05, 0.24077D+05, &
              0.25832D+05, 0.27689D+05, 0.29655D+05, 0.31734D+05, 0.33931D+05, &
              0.36250D+05, 0.38698D+05, 0.41280D+05, 0.44002D+05, 0.46869D+05, &
              0.49886D+05, 0.53062D+05, 0.56400D+05, 0.59909D+05, 0.63594D+05, &
              0.67462D+05, 0.71521D+05, 0.75777D+05, 0.80238D+05, 0.84911D+05, &
              0.89804D+05, 0.94925D+05, 0.10028D+06, 0.10588D+06, 0.11173D+06, &
              0.11785D+06, 0.12423D+06, 0.13090D+06, 0.13785D+06, 0.14510D+06, &
              0.15265D+06, 0.16053D+06, 0.16873D+06, 0.17727D+06, 0.18615D+06, &
              0.19540D+06, 0.20501D+06, 0.21501D+06, 0.22540D+06, 0.23619D+06, &
              0.24740D+06, 0.25904D+06, 0.27112D+06, 0.28365D+06, 0.29664D+06, &
              0.31012D+06, 0.32409D+06, 0.33856D+06, 0.35356D+06, 0.36908D+06, &
              0.38516D+06, 0.40180D+06, 0.41902D+06, 0.43683D+06, 0.45525D+06, &
              0.47429D+06, 0.49397D+06, 0.51431D+06, 0.53532D+06, 0.55702D+06, &
              0.57943D+06, 0.60256D+06, 0.62644D+06, 0.65107D+06, 0.67648D+06, &
              0.70269D+06, 0.72972D+06, 0.75758D+06, 0.78629D+06, 0.81588D+06, &
              0.84636D+06, 0.87775D+06, 0.91008D+06, 0.94337D+06, 0.97763D+06, &
              0.10129D+07, 0.10492D+07, 0.10865D+07, 0.11249D+07, 0.11644D+07, &
              0.12050D+07, 0.12467D+07, 0.12896D+07, 0.13337D+07, 0.13789D+07, &
              0.14255D+07/)
            case (638)
              Qtabulated =           (/ 0.22737D+03, 0.32194D+03, 0.41671D+03, &
              0.51226D+03, 0.60963D+03, 0.71017D+03, 0.81528D+03, 0.92628D+03, &
              0.10444D+04, 0.11707D+04, 0.13061D+04, 0.14518D+04, 0.16085D+04, &
              0.17772D+04, 0.19588D+04, 0.21542D+04, 0.23644D+04, 0.25903D+04, &
              0.28330D+04, 0.30934D+04, 0.33726D+04, 0.36717D+04, 0.39918D+04, &
              0.43342D+04, 0.47001D+04, 0.50907D+04, 0.55074D+04, 0.59515D+04, &
              0.64244D+04, 0.69276D+04, 0.74626D+04, 0.80310D+04, 0.86344D+04, &
              0.92744D+04, 0.99528D+04, 0.10671D+05, 0.11432D+05, 0.12236D+05, &
              0.13086D+05, 0.13984D+05, 0.14932D+05, 0.15932D+05, 0.16985D+05, &
              0.18096D+05, 0.19265D+05, 0.20495D+05, 0.21788D+05, 0.23148D+05, &
              0.24576D+05, 0.26075D+05, 0.27648D+05, 0.29298D+05, 0.31027D+05, &
              0.32839D+05, 0.34736D+05, 0.36721D+05, 0.38798D+05, 0.40970D+05, &
              0.43240D+05, 0.45611D+05, 0.48087D+05, 0.50671D+05, 0.53368D+05, &
              0.56180D+05, 0.59111D+05, 0.62165D+05, 0.65347D+05, 0.68659D+05, &
              0.72107D+05, 0.75694D+05, 0.79425D+05, 0.83303D+05, 0.87334D+05, &
              0.91522D+05, 0.95872D+05, 0.10039D+06, 0.10507D+06, 0.10994D+06, &
              0.11498D+06, 0.12021D+06, 0.12563D+06, 0.13125D+06, 0.13707D+06, &
              0.14309D+06, 0.14933D+06, 0.15579D+06, 0.16247D+06, 0.16938D+06, &
              0.17653D+06, 0.18392D+06, 0.19156D+06, 0.19946D+06, 0.20761D+06, &
              0.21604D+06, 0.22473D+06, 0.23371D+06, 0.24298D+06, 0.25254D+06, &
              0.26240D+06, 0.27258D+06, 0.28307D+06, 0.29388D+06, 0.30502D+06, &
              0.31651D+06, 0.32834D+06, 0.34052D+06, 0.35307D+06, 0.36599D+06, &
              0.37929D+06, 0.39298D+06, 0.40706D+06, 0.42155D+06, 0.43645D+06, &
              0.45178D+06, 0.46753D+06, 0.48373D+06, 0.50038D+06, 0.51748D+06, &
              0.53506D+06/)
            case (637)
              Qtabulated =           (/ 0.13267D+04, 0.18785D+04, 0.24314D+04, &
              0.29888D+04, 0.35566D+04, 0.41426D+04, 0.47550D+04, 0.54013D+04, &
              0.60886D+04, 0.68232D+04, 0.76109D+04, 0.84574D+04, 0.93678D+04, &
              0.10348D+05, 0.11402D+05, 0.12536D+05, 0.13755D+05, 0.15065D+05, &
              0.16471D+05, 0.17980D+05, 0.19598D+05, 0.21330D+05, 0.23184D+05, &
              0.25166D+05, 0.27283D+05, 0.29543D+05, 0.31953D+05, 0.34521D+05, &
              0.37256D+05, 0.40164D+05, 0.43256D+05, 0.46541D+05, 0.50026D+05, &
              0.53723D+05, 0.57641D+05, 0.61790D+05, 0.66180D+05, 0.70823D+05, &
              0.75729D+05, 0.80910D+05, 0.86378D+05, 0.92145D+05, 0.98224D+05, &
              0.10463D+06, 0.11137D+06, 0.11846D+06, 0.12592D+06, 0.13375D+06, &
              0.14198D+06, 0.15062D+06, 0.15969D+06, 0.16920D+06, 0.17916D+06, &
              0.18959D+06, 0.20052D+06, 0.21196D+06, 0.22392D+06, 0.23642D+06, &
              0.24949D+06, 0.26314D+06, 0.27740D+06, 0.29227D+06, 0.30779D+06, &
              0.32398D+06, 0.34085D+06, 0.35842D+06, 0.37673D+06, 0.39579D+06, &
              0.41563D+06, 0.43626D+06, 0.45772D+06, 0.48003D+06, 0.50322D+06, &
              0.52730D+06, 0.55232D+06, 0.57829D+06, 0.60524D+06, 0.63320D+06, &
              0.66219D+06, 0.69226D+06, 0.72342D+06, 0.75571D+06, 0.78916D+06, &
              0.82380D+06, 0.85966D+06, 0.89678D+06, 0.93518D+06, 0.97490D+06, &
              0.10160D+07, 0.10585D+07, 0.11023D+07, 0.11477D+07, 0.11946D+07, &
              0.12430D+07, 0.12929D+07, 0.13445D+07, 0.13977D+07, 0.14526D+07, &
              0.15093D+07, 0.15677D+07, 0.16280D+07, 0.16901D+07, 0.17541D+07, &
              0.18200D+07, 0.18880D+07, 0.19579D+07, 0.20300D+07, 0.21042D+07, &
              0.21805D+07, 0.22591D+07, 0.23400D+07, 0.24232D+07, 0.25087D+07, &
              0.25967D+07, 0.26871D+07, 0.27801D+07, 0.28757D+07, 0.29739D+07, &
              0.30747D+07/)

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no Q tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for Q')
              if (errorCheck(errS)) return

          end select

        case(5) ! CO

          select case ( isotopologue )
            case (26)
              Qtabulated =           (/ 0.21942D+02, 0.30949D+02, 0.39960D+02, &
              0.48975D+02, 0.57993D+02, 0.67015D+02, 0.76040D+02, 0.85069D+02, &
              0.94102D+02, 0.10314D+03, 0.11218D+03, 0.12123D+03, 0.13029D+03, &
              0.13936D+03, 0.14845D+03, 0.15756D+03, 0.16669D+03, 0.17585D+03, &
              0.18505D+03, 0.19429D+03, 0.20359D+03, 0.21293D+03, 0.22233D+03, &
              0.23181D+03, 0.24135D+03, 0.25096D+03, 0.26066D+03, 0.27045D+03, &
              0.28032D+03, 0.29030D+03, 0.30037D+03, 0.31053D+03, 0.32081D+03, &
              0.33120D+03, 0.34170D+03, 0.35231D+03, 0.36304D+03, 0.37388D+03, &
              0.38486D+03, 0.39595D+03, 0.40717D+03, 0.41852D+03, 0.42999D+03, &
              0.44160D+03, 0.45334D+03, 0.46522D+03, 0.47723D+03, 0.48937D+03, &
              0.50165D+03, 0.51406D+03, 0.52662D+03, 0.53932D+03, 0.55216D+03, &
              0.56513D+03, 0.57825D+03, 0.59152D+03, 0.60492D+03, 0.61847D+03, &
              0.63217D+03, 0.64601D+03, 0.65999D+03, 0.67412D+03, 0.68840D+03, &
              0.70282D+03, 0.71739D+03, 0.73211D+03, 0.74698D+03, 0.76200D+03, &
              0.77716D+03, 0.79247D+03, 0.80793D+03, 0.82355D+03, 0.83931D+03, &
              0.85522D+03, 0.87128D+03, 0.88749D+03, 0.90386D+03, 0.92037D+03, &
              0.93703D+03, 0.95385D+03, 0.97082D+03, 0.98794D+03, 0.10052D+04, &
              0.10226D+04, 0.10402D+04, 0.10580D+04, 0.10758D+04, 0.10939D+04, &
              0.11121D+04, 0.11304D+04, 0.11489D+04, 0.11675D+04, 0.11864D+04, &
              0.12053D+04, 0.12244D+04, 0.12437D+04, 0.12631D+04, 0.12827D+04, &
              0.13024D+04, 0.13223D+04, 0.13423D+04, 0.13625D+04, 0.13829D+04, &
              0.14034D+04, 0.14240D+04, 0.14448D+04, 0.14658D+04, 0.14870D+04, &
              0.15082D+04, 0.15297D+04, 0.15513D+04, 0.15730D+04, 0.15949D+04, &
              0.16170D+04, 0.16392D+04, 0.16616D+04, 0.16841D+04, 0.17068D+04, &
              0.17296D+04/)
            case (36)
              Qtabulated =           (/ 0.45875D+02, 0.64721D+02, 0.83574D+02, &
              0.10243D+03, 0.12130D+03, 0.14018D+03, 0.15906D+03, 0.17795D+03, &
              0.19685D+03, 0.21576D+03, 0.23468D+03, 0.25362D+03, 0.27257D+03, &
              0.29156D+03, 0.31059D+03, 0.32966D+03, 0.34879D+03, 0.36799D+03, &
              0.38727D+03, 0.40665D+03, 0.42614D+03, 0.44575D+03, 0.46549D+03, &
              0.48539D+03, 0.50544D+03, 0.52566D+03, 0.54606D+03, 0.56665D+03, &
              0.58744D+03, 0.60843D+03, 0.62965D+03, 0.65108D+03, 0.67275D+03, &
              0.69466D+03, 0.71681D+03, 0.73921D+03, 0.76187D+03, 0.78478D+03, &
              0.80796D+03, 0.83141D+03, 0.85512D+03, 0.87912D+03, 0.90339D+03, &
              0.92795D+03, 0.95279D+03, 0.97792D+03, 0.10033D+04, 0.10291D+04, &
              0.10551D+04, 0.10814D+04, 0.11080D+04, 0.11349D+04, 0.11621D+04, &
              0.11896D+04, 0.12174D+04, 0.12455D+04, 0.12739D+04, 0.13027D+04, &
              0.13317D+04, 0.13611D+04, 0.13908D+04, 0.14208D+04, 0.14510D+04, &
              0.14817D+04, 0.15126D+04, 0.15438D+04, 0.15754D+04, 0.16073D+04, &
              0.16395D+04, 0.16720D+04, 0.17049D+04, 0.17380D+04, 0.17715D+04, &
              0.18053D+04, 0.18394D+04, 0.18739D+04, 0.19086D+04, 0.19437D+04, &
              0.19792D+04, 0.20149D+04, 0.20510D+04, 0.20874D+04, 0.21241D+04, &
              0.21611D+04, 0.21985D+04, 0.22362D+04, 0.22742D+04, 0.23126D+04, &
              0.23513D+04, 0.23903D+04, 0.24296D+04, 0.24693D+04, 0.25093D+04, &
              0.25496D+04, 0.25903D+04, 0.26312D+04, 0.26725D+04, 0.27142D+04, &
              0.27562D+04, 0.27985D+04, 0.28411D+04, 0.28841D+04, 0.29274D+04, &
              0.29710D+04, 0.30150D+04, 0.30593D+04, 0.31039D+04, 0.31489D+04, &
              0.31942D+04, 0.32398D+04, 0.32858D+04, 0.33321D+04, 0.33787D+04, &
              0.34257D+04, 0.34730D+04, 0.35207D+04, 0.35686D+04, 0.36170D+04, &
              0.36656D+04/)
            case (28)
              Qtabulated =           (/ 0.23024D+02, 0.32483D+02, 0.41946D+02, &
              0.51412D+02, 0.60882D+02, 0.70356D+02, 0.79834D+02, 0.89315D+02, &
              0.98801D+02, 0.10829D+03, 0.11779D+03, 0.12729D+03, 0.13681D+03, &
              0.14634D+03, 0.15589D+03, 0.16546D+03, 0.17506D+03, 0.18470D+03, &
              0.19438D+03, 0.20411D+03, 0.21389D+03, 0.22374D+03, 0.23365D+03, &
              0.24364D+03, 0.25371D+03, 0.26386D+03, 0.27411D+03, 0.28444D+03, &
              0.29489D+03, 0.30543D+03, 0.31608D+03, 0.32685D+03, 0.33773D+03, &
              0.34873D+03, 0.35986D+03, 0.37111D+03, 0.38249D+03, 0.39400D+03, &
              0.40565D+03, 0.41742D+03, 0.42934D+03, 0.44139D+03, 0.45359D+03, &
              0.46592D+03, 0.47841D+03, 0.49103D+03, 0.50380D+03, 0.51672D+03, &
              0.52979D+03, 0.54300D+03, 0.55637D+03, 0.56989D+03, 0.58356D+03, &
              0.59738D+03, 0.61136D+03, 0.62549D+03, 0.63977D+03, 0.65421D+03, &
              0.66881D+03, 0.68357D+03, 0.69847D+03, 0.71354D+03, 0.72877D+03, &
              0.74415D+03, 0.75969D+03, 0.77540D+03, 0.79126D+03, 0.80728D+03, &
              0.82346D+03, 0.83981D+03, 0.85631D+03, 0.87297D+03, 0.88980D+03, &
              0.90679D+03, 0.92394D+03, 0.94125D+03, 0.95873D+03, 0.97636D+03, &
              0.99417D+03, 0.10121D+04, 0.10303D+04, 0.10485D+04, 0.10670D+04, &
              0.10856D+04, 0.11044D+04, 0.11234D+04, 0.11425D+04, 0.11617D+04, &
              0.11812D+04, 0.12008D+04, 0.12206D+04, 0.12405D+04, 0.12606D+04, &
              0.12809D+04, 0.13013D+04, 0.13219D+04, 0.13427D+04, 0.13636D+04, &
              0.13847D+04, 0.14060D+04, 0.14274D+04, 0.14490D+04, 0.14708D+04, &
              0.14927D+04, 0.15148D+04, 0.15371D+04, 0.15595D+04, 0.15821D+04, &
              0.16049D+04, 0.16278D+04, 0.16509D+04, 0.16742D+04, 0.16976D+04, &
              0.17212D+04, 0.17450D+04, 0.17690D+04, 0.17931D+04, 0.18174D+04, &
              0.18418D+04/)
            case (27)
              Qtabulated =           (/ 0.13502D+03, 0.19046D+03, 0.24593D+03, &
              0.30143D+03, 0.35694D+03, 0.41248D+03, 0.46804D+03, 0.52362D+03, &
              0.57922D+03, 0.63485D+03, 0.69052D+03, 0.74623D+03, 0.80201D+03, &
              0.85786D+03, 0.91382D+03, 0.96991D+03, 0.10262D+04, 0.10826D+04, &
              0.11393D+04, 0.11963D+04, 0.12536D+04, 0.13112D+04, 0.13692D+04, &
              0.14276D+04, 0.14865D+04, 0.15459D+04, 0.16057D+04, 0.16662D+04, &
              0.17272D+04, 0.17888D+04, 0.18510D+04, 0.19139D+04, 0.19774D+04, &
              0.20416D+04, 0.21066D+04, 0.21722D+04, 0.22386D+04, 0.23058D+04, &
              0.23737D+04, 0.24424D+04, 0.25118D+04, 0.25821D+04, 0.26532D+04, &
              0.27251D+04, 0.27978D+04, 0.28714D+04, 0.29458D+04, 0.30211D+04, &
              0.30972D+04, 0.31742D+04, 0.32520D+04, 0.33307D+04, 0.34104D+04, &
              0.34908D+04, 0.35722D+04, 0.36545D+04, 0.37376D+04, 0.38217D+04, &
              0.39066D+04, 0.39925D+04, 0.40793D+04, 0.41670D+04, 0.42556D+04, &
              0.43451D+04, 0.44355D+04, 0.45269D+04, 0.46191D+04, 0.47124D+04, &
              0.48065D+04, 0.49016D+04, 0.49976D+04, 0.50945D+04, 0.51923D+04, &
              0.52912D+04, 0.53909D+04, 0.54916D+04, 0.55932D+04, 0.56957D+04, &
              0.57993D+04, 0.59037D+04, 0.60091D+04, 0.61155D+04, 0.62228D+04, &
              0.63310D+04, 0.64402D+04, 0.65504D+04, 0.66615D+04, 0.67735D+04, &
              0.68866D+04, 0.70005D+04, 0.71154D+04, 0.72313D+04, 0.73481D+04, &
              0.74660D+04, 0.75847D+04, 0.77045D+04, 0.78251D+04, 0.79468D+04, &
              0.80694D+04, 0.81930D+04, 0.83175D+04, 0.84431D+04, 0.85695D+04, &
              0.86970D+04, 0.88254D+04, 0.89548D+04, 0.90852D+04, 0.92164D+04, &
              0.93487D+04, 0.94820D+04, 0.96163D+04, 0.97515D+04, 0.98877D+04, &
              0.10025D+05, 0.10163D+05, 0.10302D+05, 0.10442D+05, 0.10583D+05, &
              0.10725D+05/)
            case (38)
              Qtabulated =           (/ 0.48251D+02, 0.68086D+02, 0.87930D+02, &
              0.10778D+03, 0.12764D+03, 0.14751D+03, 0.16738D+03, 0.18727D+03, &
              0.20716D+03, 0.22706D+03, 0.24698D+03, 0.26692D+03, 0.28688D+03, &
              0.30687D+03, 0.32691D+03, 0.34701D+03, 0.36717D+03, 0.38742D+03, &
              0.40776D+03, 0.42821D+03, 0.44880D+03, 0.46951D+03, 0.49039D+03, &
              0.51142D+03, 0.53264D+03, 0.55404D+03, 0.57565D+03, 0.59747D+03, &
              0.61951D+03, 0.64179D+03, 0.66430D+03, 0.68707D+03, 0.71008D+03, &
              0.73336D+03, 0.75691D+03, 0.78073D+03, 0.80483D+03, 0.82922D+03, &
              0.85390D+03, 0.87887D+03, 0.90413D+03, 0.92970D+03, 0.95558D+03, &
              0.98176D+03, 0.10082D+04, 0.10351D+04, 0.10622D+04, 0.10896D+04, &
              0.11174D+04, 0.11455D+04, 0.11739D+04, 0.12026D+04, 0.12317D+04, &
              0.12611D+04, 0.12908D+04, 0.13209D+04, 0.13512D+04, 0.13820D+04, &
              0.14130D+04, 0.14444D+04, 0.14762D+04, 0.15082D+04, 0.15407D+04, &
              0.15734D+04, 0.16065D+04, 0.16400D+04, 0.16737D+04, 0.17079D+04, &
              0.17424D+04, 0.17772D+04, 0.18123D+04, 0.18479D+04, 0.18837D+04, &
              0.19199D+04, 0.19565D+04, 0.19934D+04, 0.20306D+04, 0.20683D+04, &
              0.21062D+04, 0.21445D+04, 0.21832D+04, 0.22222D+04, 0.22615D+04, &
              0.23013D+04, 0.23413D+04, 0.23817D+04, 0.24225D+04, 0.24636D+04, &
              0.25051D+04, 0.25470D+04, 0.25892D+04, 0.26317D+04, 0.26746D+04, &
              0.27179D+04, 0.27615D+04, 0.28054D+04, 0.28498D+04, 0.28945D+04, &
              0.29395D+04, 0.29849D+04, 0.30306D+04, 0.30768D+04, 0.31232D+04, &
              0.31701D+04, 0.32173D+04, 0.32648D+04, 0.33127D+04, 0.33610D+04, &
              0.34096D+04, 0.34586D+04, 0.35080D+04, 0.35577D+04, 0.36077D+04, &
              0.36582D+04, 0.37090D+04, 0.37601D+04, 0.38116D+04, 0.38635D+04, &
              0.39158D+04/)
            case (37)
              Qtabulated =           (/ 0.28263D+03, 0.39877D+03, 0.51497D+03, &
              0.63121D+03, 0.74749D+03, 0.86382D+03, 0.98020D+03, 0.10966D+04, &
              0.12131D+04, 0.13296D+04, 0.14462D+04, 0.15629D+04, 0.16797D+04, &
              0.17966D+04, 0.19138D+04, 0.20313D+04, 0.21490D+04, 0.22672D+04, &
              0.23858D+04, 0.25049D+04, 0.26247D+04, 0.27452D+04, 0.28665D+04, &
              0.29886D+04, 0.31116D+04, 0.32356D+04, 0.33606D+04, 0.34868D+04, &
              0.36141D+04, 0.37427D+04, 0.38725D+04, 0.40036D+04, 0.41361D+04, &
              0.42700D+04, 0.44054D+04, 0.45422D+04, 0.46805D+04, 0.48204D+04, &
              0.49618D+04, 0.51049D+04, 0.52495D+04, 0.53958D+04, 0.55438D+04, &
              0.56934D+04, 0.58448D+04, 0.59979D+04, 0.61527D+04, 0.63092D+04, &
              0.64675D+04, 0.66276D+04, 0.67895D+04, 0.69531D+04, 0.71187D+04, &
              0.72859D+04, 0.74551D+04, 0.76261D+04, 0.77989D+04, 0.79736D+04, &
              0.81501D+04, 0.83285D+04, 0.85088D+04, 0.86910D+04, 0.88751D+04, &
              0.90610D+04, 0.92488D+04, 0.94385D+04, 0.96302D+04, 0.98238D+04, &
              0.10019D+05, 0.10217D+05, 0.10416D+05, 0.10617D+05, 0.10820D+05, &
              0.11025D+05, 0.11233D+05, 0.11441D+05, 0.11652D+05, 0.11865D+05, &
              0.12080D+05, 0.12297D+05, 0.12516D+05, 0.12736D+05, 0.12959D+05, &
              0.13184D+05, 0.13410D+05, 0.13639D+05, 0.13869D+05, 0.14102D+05, &
              0.14336D+05, 0.14573D+05, 0.14811D+05, 0.15051D+05, 0.15294D+05, &
              0.15538D+05, 0.15784D+05, 0.16033D+05, 0.16283D+05, 0.16535D+05, &
              0.16790D+05, 0.17046D+05, 0.17304D+05, 0.17564D+05, 0.17827D+05, &
              0.18091D+05, 0.18357D+05, 0.18626D+05, 0.18896D+05, 0.19168D+05, &
              0.19443D+05, 0.19719D+05, 0.19997D+05, 0.20277D+05, 0.20560D+05, &
              0.20844D+05, 0.21130D+05, 0.21419D+05, 0.21709D+05, 0.22002D+05, &
              0.22296D+05/)

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no Q tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for Q')
              if (errorCheck(errS)) return

          end select

        case(6) ! CH4

          select case ( isotopologue )
            case (211)
              Qtabulated =           (/ 0.54791D+02, 0.91526D+02, 0.13410D+03, &
              0.18179D+03, 0.23412D+03, 0.29072D+03, 0.35137D+03, 0.41596D+03, &
              0.48450D+03, 0.55713D+03, 0.63413D+03, 0.71585D+03, 0.80281D+03, &
              0.89552D+03, 0.99464D+03, 0.11009D+04, 0.12150D+04, 0.13377D+04, &
              0.14700D+04, 0.16129D+04, 0.17672D+04, 0.19341D+04, 0.21149D+04, &
              0.23106D+04, 0.25227D+04, 0.27525D+04, 0.30017D+04, 0.32719D+04, &
              0.35649D+04, 0.38826D+04, 0.42271D+04, 0.46005D+04, 0.50054D+04, &
              0.54442D+04, 0.59197D+04, 0.64348D+04, 0.69927D+04, 0.75969D+04, &
              0.82510D+04, 0.89588D+04, 0.97245D+04, 0.10553D+05, 0.11448D+05, &
              0.12416D+05, 0.13462D+05, 0.14591D+05, 0.15810D+05, 0.17127D+05, &
              0.18546D+05, 0.20078D+05, 0.21729D+05, 0.23508D+05, 0.25425D+05, &
              0.27489D+05, 0.29710D+05, 0.32101D+05, 0.34673D+05, 0.37438D+05, &
              0.40410D+05, 0.43603D+05, 0.47032D+05, 0.50713D+05, 0.54663D+05, &
              0.58901D+05, 0.63446D+05, 0.68317D+05, 0.73536D+05, 0.79127D+05, &
              0.85113D+05, 0.91519D+05, 0.98374D+05, 0.10570D+06, 0.11354D+06, &
              0.12192D+06, 0.13086D+06, 0.14042D+06, 0.15062D+06, 0.16151D+06, &
              0.17312D+06, 0.18550D+06, 0.19871D+06, 0.21277D+06, 0.22776D+06, &
              0.24372D+06, 0.26071D+06, 0.27878D+06, 0.29802D+06, 0.31847D+06, &
              0.34021D+06, 0.36332D+06, 0.38787D+06, 0.41394D+06, 0.44162D+06, &
              0.47100D+06, 0.50218D+06, 0.53524D+06, 0.57030D+06, 0.60747D+06, &
              0.64685D+06, 0.68857D+06, 0.73276D+06, 0.77954D+06, 0.82906D+06, &
              0.88145D+06, 0.93687D+06, 0.99548D+06, 0.10574D+07, 0.11229D+07, &
              0.11921D+07, 0.12652D+07, 0.13424D+07, 0.14238D+07, 0.15098D+07, &
              0.16005D+07, 0.16962D+07, 0.17972D+07, 0.19036D+07, 0.20157D+07, &
              0.21339D+07/)
            case (311)
              Qtabulated =           (/ 0.10958D+03, 0.18304D+03, 0.26818D+03, &
              0.36356D+03, 0.46820D+03, 0.58141D+03, 0.70270D+03, 0.83186D+03, &
              0.96893D+03, 0.11142D+04, 0.12682D+04, 0.14316D+04, 0.16055D+04, &
              0.17909D+04, 0.19891D+04, 0.22016D+04, 0.24297D+04, 0.26752D+04, &
              0.29399D+04, 0.32255D+04, 0.35342D+04, 0.38680D+04, 0.42294D+04, &
              0.46208D+04, 0.50449D+04, 0.55046D+04, 0.60030D+04, 0.65434D+04, &
              0.71293D+04, 0.77646D+04, 0.84535D+04, 0.92004D+04, 0.10010D+05, &
              0.10888D+05, 0.11838D+05, 0.12869D+05, 0.13984D+05, 0.15193D+05, &
              0.16501D+05, 0.17916D+05, 0.19448D+05, 0.21104D+05, 0.22895D+05, &
              0.24830D+05, 0.26921D+05, 0.29180D+05, 0.31618D+05, 0.34250D+05, &
              0.37090D+05, 0.40152D+05, 0.43454D+05, 0.47012D+05, 0.50845D+05, &
              0.54973D+05, 0.59416D+05, 0.64197D+05, 0.69340D+05, 0.74870D+05, &
              0.80813D+05, 0.87198D+05, 0.94055D+05, 0.10142D+06, 0.10932D+06, &
              0.11779D+06, 0.12688D+06, 0.13662D+06, 0.14706D+06, 0.15824D+06, &
              0.17021D+06, 0.18302D+06, 0.19673D+06, 0.21139D+06, 0.22706D+06, &
              0.24381D+06, 0.26171D+06, 0.28082D+06, 0.30122D+06, 0.32299D+06, &
              0.34621D+06, 0.37097D+06, 0.39737D+06, 0.42551D+06, 0.45548D+06, &
              0.48739D+06, 0.52136D+06, 0.55752D+06, 0.59598D+06, 0.63688D+06, &
              0.68036D+06, 0.72657D+06, 0.77566D+06, 0.82780D+06, 0.88316D+06, &
              0.94191D+06, 0.10043D+07, 0.10704D+07, 0.11405D+07, 0.12148D+07, &
              0.12936D+07, 0.13770D+07, 0.14654D+07, 0.15589D+07, 0.16579D+07, &
              0.17627D+07, 0.18736D+07, 0.19908D+07, 0.21147D+07, 0.22456D+07, &
              0.23840D+07, 0.25301D+07, 0.26844D+07, 0.28474D+07, 0.30193D+07, &
              0.32007D+07, 0.33921D+07, 0.35939D+07, 0.38067D+07, 0.40310D+07, &
              0.42673D+07/)
            case (212)
              Qtabulated =           (/ 0.44079D+03, 0.73786D+03, 0.10822D+04, &
              0.14679D+04, 0.18912D+04, 0.23493D+04, 0.28400D+04, 0.33627D+04, &
              0.39174D+04, 0.45053D+04, 0.51285D+04, 0.57900D+04, 0.64938D+04, &
              0.72443D+04, 0.80465D+04, 0.89064D+04, 0.98299D+04, 0.10824D+05, &
              0.11895D+05, 0.13051D+05, 0.14300D+05, 0.15652D+05, 0.17115D+05, &
              0.18699D+05, 0.20416D+05, 0.22277D+05, 0.24294D+05, 0.26481D+05, &
              0.28853D+05, 0.31425D+05, 0.34214D+05, 0.37237D+05, 0.40515D+05, &
              0.44067D+05, 0.47916D+05, 0.52087D+05, 0.56604D+05, 0.61495D+05, &
              0.66790D+05, 0.72521D+05, 0.78720D+05, 0.85426D+05, 0.92675D+05, &
              0.10051D+06, 0.10898D+06, 0.11812D+06, 0.12799D+06, 0.13865D+06, &
              0.15014D+06, 0.16254D+06, 0.17591D+06, 0.19031D+06, 0.20583D+06, &
              0.22254D+06, 0.24053D+06, 0.25989D+06, 0.28071D+06, 0.30310D+06, &
              0.32716D+06, 0.35301D+06, 0.38077D+06, 0.41058D+06, 0.44257D+06, &
              0.47688D+06, 0.51367D+06, 0.55311D+06, 0.59537D+06, 0.64064D+06, &
              0.68910D+06, 0.74098D+06, 0.79647D+06, 0.85583D+06, 0.91928D+06, &
              0.98710D+06, 0.10595D+07, 0.11369D+07, 0.12195D+07, 0.13076D+07, &
              0.14017D+07, 0.15019D+07, 0.16088D+07, 0.17227D+07, 0.18441D+07, &
              0.19733D+07, 0.21108D+07, 0.22572D+07, 0.24129D+07, 0.25785D+07, &
              0.27545D+07, 0.29416D+07, 0.31404D+07, 0.33515D+07, 0.35756D+07, &
              0.38135D+07, 0.40659D+07, 0.43336D+07, 0.46174D+07, 0.49183D+07, &
              0.52372D+07, 0.55750D+07, 0.59327D+07, 0.63114D+07, 0.67123D+07, &
              0.71365D+07, 0.75852D+07, 0.80596D+07, 0.85612D+07, 0.90914D+07, &
              0.96515D+07, 0.10243D+08, 0.10868D+08, 0.11527D+08, 0.12224D+08, &
              0.12958D+08, 0.13733D+08, 0.14550D+08, 0.15411D+08, 0.16319D+08, &
              0.17275D+08/)

            case default
              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no Q tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for Q')
              if (errorCheck(errS)) return

          end select

        case(7) ! O2

          select case ( isotopologue )

            case (66)

              Qtabulated =           (/ 0.44334D+02, 0.62460D+02, 0.80596D+02, &
              0.98738D+02, 0.11688D+03, 0.13503D+03, 0.15319D+03, 0.17136D+03, &
              0.18954D+03, 0.20775D+03, 0.22600D+03, 0.24431D+03, 0.26270D+03, &
              0.28119D+03, 0.29981D+03, 0.31857D+03, 0.33750D+03, 0.35662D+03, &
              0.37594D+03, 0.39550D+03, 0.41529D+03, 0.43535D+03, 0.45568D+03, &
              0.47630D+03, 0.49722D+03, 0.51844D+03, 0.53998D+03, 0.56185D+03, &
              0.58406D+03, 0.60660D+03, 0.62949D+03, 0.65274D+03, 0.67635D+03, &
              0.70031D+03, 0.72465D+03, 0.74936D+03, 0.77444D+03, 0.79990D+03, &
              0.82574D+03, 0.85197D+03, 0.87858D+03, 0.90558D+03, 0.93297D+03, &
              0.96076D+03, 0.98895D+03, 0.10175D+04, 0.10465D+04, 0.10759D+04, &
              0.11057D+04, 0.11359D+04, 0.11665D+04, 0.11976D+04, 0.12290D+04, &
              0.12609D+04, 0.12931D+04, 0.13258D+04, 0.13590D+04, 0.13925D+04, &
              0.14265D+04, 0.14609D+04, 0.14958D+04, 0.15311D+04, 0.15669D+04, &
              0.16031D+04, 0.16397D+04, 0.16768D+04, 0.17144D+04, 0.17524D+04, &
              0.17909D+04, 0.18298D+04, 0.18692D+04, 0.19091D+04, 0.19495D+04, &
              0.19904D+04, 0.20318D+04, 0.20736D+04, 0.21160D+04, 0.21588D+04, &
              0.22022D+04, 0.22461D+04, 0.22905D+04, 0.23354D+04, 0.23809D+04, &
              0.24268D+04, 0.24734D+04, 0.25204D+04, 0.25680D+04, 0.26162D+04, &
              0.26649D+04, 0.27142D+04, 0.27641D+04, 0.28145D+04, 0.28655D+04, &
              0.29171D+04, 0.29693D+04, 0.30221D+04, 0.30755D+04, 0.31295D+04, &
              0.31841D+04, 0.32393D+04, 0.32951D+04, 0.33516D+04, 0.34087D+04, &
              0.34665D+04, 0.35249D+04, 0.35839D+04, 0.36436D+04, 0.37040D+04, &
              0.37650D+04, 0.38267D+04, 0.38891D+04, 0.39522D+04, 0.40159D+04, &
              0.40804D+04, 0.41455D+04, 0.42114D+04, 0.42780D+04, 0.43452D+04, &
              0.44132D+04/)

            case (68)

              Qtabulated =           (/ 0.89206D+02, 0.12759D+03, 0.16600D+03, &
              0.20442D+03, 0.24285D+03, 0.28128D+03, 0.31973D+03, 0.35821D+03, &
              0.39672D+03, 0.43530D+03, 0.47398D+03, 0.51281D+03, 0.55183D+03, &
              0.59108D+03, 0.63062D+03, 0.67051D+03, 0.71078D+03, 0.75148D+03, &
              0.79265D+03, 0.83435D+03, 0.87659D+03, 0.91941D+03, 0.96285D+03, &
              0.10069D+04, 0.10517D+04, 0.10971D+04, 0.11432D+04, 0.11901D+04, &
              0.12377D+04, 0.12861D+04, 0.13352D+04, 0.13851D+04, 0.14358D+04, &
              0.14872D+04, 0.15395D+04, 0.15926D+04, 0.16466D+04, 0.17013D+04, &
              0.17569D+04, 0.18134D+04, 0.18706D+04, 0.19288D+04, 0.19877D+04, &
              0.20476D+04, 0.21083D+04, 0.21698D+04, 0.22323D+04, 0.22956D+04, &
              0.23598D+04, 0.24248D+04, 0.24908D+04, 0.25576D+04, 0.26253D+04, &
              0.26940D+04, 0.27635D+04, 0.28339D+04, 0.29052D+04, 0.29775D+04, &
              0.30506D+04, 0.31247D+04, 0.31997D+04, 0.32756D+04, 0.33524D+04, &
              0.34302D+04, 0.35089D+04, 0.35885D+04, 0.36691D+04, 0.37506D+04, &
              0.38331D+04, 0.39166D+04, 0.40010D+04, 0.40864D+04, 0.41727D+04, &
              0.42601D+04, 0.43484D+04, 0.44377D+04, 0.45280D+04, 0.46193D+04, &
              0.47116D+04, 0.48049D+04, 0.48992D+04, 0.49946D+04, 0.50909D+04, &
              0.51883D+04, 0.52868D+04, 0.53863D+04, 0.54868D+04, 0.55884D+04, &
              0.56911D+04, 0.57949D+04, 0.58997D+04, 0.60056D+04, 0.61126D+04, &
              0.62207D+04, 0.63298D+04, 0.64401D+04, 0.65516D+04, 0.66641D+04, &
              0.67778D+04, 0.68926D+04, 0.70085D+04, 0.71256D+04, 0.72439D+04, &
              0.73633D+04, 0.74839D+04, 0.76056D+04, 0.77286D+04, 0.78527D+04, &
              0.79781D+04, 0.81046D+04, 0.82324D+04, 0.83613D+04, 0.84915D+04, &
              0.86229D+04, 0.87556D+04, 0.88895D+04, 0.90247D+04, 0.91611D+04, &
              0.92988D+04/)

            case (67)

              Qtabulated =           (/ 0.52071E+03, 0.74484E+03, 0.96908E+03, &
              0.11934E+04, 0.14177E+04, 0.16422E+04, 0.18667E+04, 0.20913E+04, &
              0.23161E+04, 0.25413E+04, 0.27671E+04, 0.29936E+04, 0.32212E+04, &
              0.34501E+04, 0.36806E+04, 0.39130E+04, 0.41476E+04, 0.43846E+04, &
              0.46242E+04, 0.48668E+04, 0.51125E+04, 0.53615E+04, 0.56140E+04, &
              0.58701E+04, 0.61300E+04, 0.63938E+04, 0.66617E+04, 0.69337E+04, &
              0.72099E+04, 0.74904E+04, 0.77754E+04, 0.80647E+04, 0.83586E+04, &
              0.86571E+04, 0.89602E+04, 0.92680E+04, 0.95805E+04, 0.98977E+04, &
              0.10220E+05, 0.10547E+05, 0.10878E+05, 0.11215E+05, 0.11556E+05, &
              0.11903E+05, 0.12254E+05, 0.12611E+05, 0.12972E+05, 0.13338E+05, &
              0.13710E+05, 0.14086E+05, 0.14468E+05, 0.14855E+05, 0.15247E+05, &
              0.15644E+05, 0.16046E+05, 0.16453E+05, 0.16866E+05, 0.17283E+05, &
              0.17706E+05, 0.18135E+05, 0.18568E+05, 0.19007E+05, 0.19452E+05, &
              0.19901E+05, 0.20356E+05, 0.20817E+05, 0.21283E+05, 0.21754E+05, &
              0.22231E+05, 0.22713E+05, 0.23201E+05, 0.23695E+05, 0.24194E+05, &
              0.24699E+05, 0.25209E+05, 0.25725E+05, 0.26247E+05, 0.26775E+05, &
              0.27308E+05, 0.27847E+05, 0.28393E+05, 0.28944E+05, 0.29500E+05, &
              0.30063E+05, 0.30632E+05, 0.31207E+05, 0.31788E+05, 0.32375E+05, &
              0.32968E+05, 0.33568E+05, 0.34173E+05, 0.34785E+05, 0.35403E+05, &
              0.36028E+05, 0.36659E+05, 0.37296E+05, 0.37939E+05, 0.38590E+05, &
              0.39246E+05, 0.39909E+05, 0.40579E+05, 0.41256E+05, 0.41939E+05, &
              0.42629E+05, 0.43325E+05, 0.44029E+05, 0.44739E+05, 0.45456E+05, &
              0.46180E+05, 0.46911E+05, 0.47649E+05, 0.48394E+05, 0.49146E+05, &
              0.49905E+05, 0.50671E+05, 0.51445E+05, 0.52226E+05, 0.53014E+05, &
              0.53809E+05/)

            case default

              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no Q tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for xgj')
              if (errorCheck(errS)) return

          end select

        case(11) ! NH3

          select case ( isotopologue )

            case (4111)

              Qtabulated =           (/ 0.16013E+03, 0.26692E+03, 0.39067E+03, &
              0.52933E+03, 0.68153E+03, 0.84641E+03, 0.10234E+04, 0.12125E+04, &
              0.14136E+04, 0.16272E+04, 0.18537E+04, 0.20937E+04, 0.23481E+04, &
              0.26177E+04, 0.29035E+04, 0.32065E+04, 0.35279E+04, 0.38688E+04, &
              0.42304E+04, 0.46141E+04, 0.50212E+04, 0.54531E+04, 0.59114E+04, &
              0.63976E+04, 0.69133E+04, 0.74602E+04, 0.80401E+04, 0.86549E+04, &
              0.93066E+04, 0.99971E+04, 0.10729E+05, 0.11504E+05, 0.12324E+05, &
              0.13193E+05, 0.14112E+05, 0.15085E+05, 0.16114E+05, 0.17201E+05, &
              0.18352E+05, 0.19567E+05, 0.20851E+05, 0.22208E+05, 0.23640E+05, &
              0.25152E+05, 0.26747E+05, 0.28430E+05, 0.30205E+05, 0.32077E+05, &
              0.34050E+05, 0.36128E+05, 0.38317E+05, 0.40623E+05, 0.43050E+05, &
              0.45605E+05, 0.48292E+05, 0.51119E+05, 0.54091E+05, 0.57215E+05, &
              0.60498E+05, 0.63947E+05, 0.67569E+05, 0.71372E+05, 0.75364E+05, &
              0.79552E+05, 0.83946E+05, 0.88553E+05, 0.93384E+05, 0.98447E+05, &
              0.10375E+06, 0.10931E+06, 0.11513E+06, 0.12122E+06, 0.12760E+06, &
              0.13427E+06, 0.14125E+06, 0.14855E+06, 0.15619E+06, 0.16417E+06, &
              0.17250E+06, 0.18121E+06, 0.19031E+06, 0.19981E+06, 0.20973E+06, &
              0.22008E+06, 0.23088E+06, 0.24215E+06, 0.25390E+06, 0.26615E+06, &
              0.27892E+06, 0.29223E+06, 0.30610E+06, 0.32055E+06, 0.33559E+06, &
              0.35125E+06, 0.36756E+06, 0.38453E+06, 0.40219E+06, 0.42056E+06, &
              0.43967E+06, 0.45953E+06, 0.48019E+06, 0.50165E+06, 0.52396E+06, &
              0.54714E+06, 0.57122E+06, 0.59622E+06, 0.62218E+06, 0.64913E+06, &
              0.67710E+06, 0.70613E+06, 0.73624E+06, 0.76748E+06, 0.79988E+06, &
              0.83347E+06, 0.86829E+06, 0.90439E+06, 0.94180E+06, 0.98056E+06, &
              0.10207E+07/)

            case (5111)

              Qtabulated =           (/ 0.10697E+03, 0.17832E+03, 0.26100E+03, &
              0.35364E+03, 0.45533E+03, 0.56549E+03, 0.68377E+03, 0.81007E+03, &
              0.94447E+03, 0.10872E+04, 0.12385E+04, 0.13988E+04, 0.15688E+04, &
              0.17490E+04, 0.19399E+04, 0.21424E+04, 0.23571E+04, 0.25848E+04, &
              0.28264E+04, 0.30828E+04, 0.33548E+04, 0.36434E+04, 0.39496E+04, &
              0.42745E+04, 0.46190E+04, 0.49845E+04, 0.53720E+04, 0.57828E+04, &
              0.62182E+04, 0.66796E+04, 0.71684E+04, 0.76862E+04, 0.82344E+04, &
              0.88149E+04, 0.94292E+04, 0.10079E+05, 0.10767E+05, 0.11494E+05, &
              0.12262E+05, 0.13074E+05, 0.13932E+05, 0.14839E+05, 0.15796E+05, &
              0.16806E+05, 0.17872E+05, 0.18997E+05, 0.20183E+05, 0.21434E+05, &
              0.22752E+05, 0.24141E+05, 0.25604E+05, 0.27145E+05, 0.28767E+05, &
              0.30475E+05, 0.32271E+05, 0.34160E+05, 0.36146E+05, 0.38234E+05, &
              0.40428E+05, 0.42733E+05, 0.45154E+05, 0.47696E+05, 0.50364E+05, &
              0.53163E+05, 0.56100E+05, 0.59180E+05, 0.62408E+05, 0.65792E+05, &
              0.69339E+05, 0.73053E+05, 0.76943E+05, 0.81016E+05, 0.85279E+05, &
              0.89740E+05, 0.94406E+05, 0.99287E+05, 0.10439E+06, 0.10972E+06, &
              0.11530E+06, 0.12112E+06, 0.12720E+06, 0.13355E+06, 0.14018E+06, &
              0.14711E+06, 0.15433E+06, 0.16186E+06, 0.16971E+06, 0.17791E+06, &
              0.18645E+06, 0.19534E+06, 0.20462E+06, 0.21428E+06, 0.22434E+06, &
              0.23481E+06, 0.24572E+06, 0.25706E+06, 0.26887E+06, 0.28116E+06, &
              0.29393E+06, 0.30722E+06, 0.32103E+06, 0.33539E+06, 0.35031E+06, &
              0.36581E+06, 0.38191E+06, 0.39864E+06, 0.41600E+06, 0.43403E+06, &
              0.45274E+06, 0.47215E+06, 0.49230E+06, 0.51319E+06, 0.53487E+06, &
              0.55734E+06, 0.58064E+06, 0.60478E+06, 0.62981E+06, 0.65574E+06, &
              0.68260E+06/)

            case default

              call logDebugI('ERROR: isotopologue = ', isotopologue)
              call logDebug('no Q tabulated for this isotopologue')
              call logDebugI('gasIndex = ', hitranS%gasIndex)
              call mystop(errS, 'stopped because isotopologue number is not availble for xgj')
              if (errorCheck(errS)) return

          end select

        case default

          call logDebug('ERROR: gasIndex not a valid number')
          call logDebug('in subroutine getQtabulated')
          call logDebug('in module HITRANModule')
          call logDebugI('index = ', hitranS%gasIndex)
          call mystop(errS, 'stopped because gasIndex is not valid')
          if (errorCheck(errS)) return

      end select

    end subroutine getQtabulated


    subroutine claimMemHITRAN_lineParameters(errS, hitranS)

      implicit none

      type(errorType), intent(inout) :: errS
      type(HITRANType), intent(inout) :: hitranS

      integer :: allocStatus
      integer :: sumAllocStatus

      allocStatus    = 0
      sumAllocStatus = 0

      allocate( hitranS%isotope( hitranS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( hitranS%Sig    ( hitranS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( hitranS%S      ( hitranS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( hitranS%gam    ( hitranS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( hitranS%E      ( hitranS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( hitranS%bet    ( hitranS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( hitranS%delt   ( hitranS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      if (sumAllocStatus /= 0) then
        print *, 'claimMemHITRAN_lineParameters has nonzero sumAllocStatus. Alloc Failed'
        call mystop(errS, 'claimMemHITRAN_lineParameters has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
      end if

    end subroutine claimMemHITRAN_lineParameters


    subroutine claimMemHITRAN_ISO(errS, nISO, hitranS)

      implicit none

      type(errorType), intent(inout) :: errS
      integer,          intent(in)    :: nISO
      type(HITRANType), intent(inout) :: hitranS

      integer :: allocStatus
      integer :: sumAllocStatus

      allocStatus    = 0
      sumAllocStatus = 0

      hitranS%nISO = nISO

      allocate( hitranS%ISO          ( nISO ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( hitranS%isotopologue ( nISO ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( hitranS%molWeight    ( nISO ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( hitranS%abundance    ( nISO ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      if (sumAllocStatus /= 0) then
        print *, 'claimMemHITRAN_ISO has nonzero sumAllocStatus. Alloc Failed'
        call mystop(errS, 'claimMemHITRAN_ISO has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
      end if

      ! initialize
      hitranS%ISO          = 0
      hitranS%isotopologue = 0
      hitranS%molWeight    = 0.0d0
      hitranS%abundance    = 0.0d0

    end subroutine claimMemHITRAN_ISO


    subroutine claimMemSDFS(errS, SDFS)

      implicit none

      type(errorType), intent(inout) :: errS
      type(SDFType), intent(inout) :: SDFS

      integer :: allocStatus
      integer :: sumAllocStatus

      allocStatus    = 0
      sumAllocStatus = 0

      allocate( SDFS%sigLines   ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%modSigLines( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%PopuT0     ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%PopuT      ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%DipoR      ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%Dipo0      ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%Dipo       ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%E          ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%HWT0       ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%HWT        ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%BHW        ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%SHIFT      ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%YT         ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDFS%m1         ( SDFS%nlines ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      if (sumAllocStatus /= 0) then
        print *, 'claimMemSDFS has nonzero sumAllocStatus. Alloc Failed'
        call mystop(errS, 'claimMemSDFS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
      end if

    end subroutine claimMemSDFS

    subroutine claimMemRMFS(errS, RMFS)

      implicit none

      type(errorType), intent(inout) :: errS
      type(RMFType), intent(inout) :: RMFS

      integer :: allocStatus
      integer :: sumAllocStatus

      allocStatus    = 0
      sumAllocStatus = 0

      allocate( RMFS%WT0 ( RMFS%nRMF ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( RMFS%WT  ( RMFS%nRMF ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( RMFS%BW  ( RMFS%nRMF ), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      if (sumAllocStatus /= 0) then
        print *, 'claimMemRMFS has nonzero sumAllocStatus. Alloc Failed'
        call mystop(errS, 'claimMemRMFS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
      end if

    end subroutine claimMemRMFS


    subroutine freeMemHITRAN_lineParameters(errS, hitranS)

      implicit none

      type(errorType), intent(inout) :: errS
      type(HITRANType), intent(inout) :: hitranS

      integer :: deallocStatus
      integer :: sumdeAllocStatus

      deallocStatus    = 0
      sumdeAllocStatus = 0

      deallocate( hitranS%isotope, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( hitranS%Sig    , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( hitranS%S      , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( hitranS%gam    , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( hitranS%E      , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( hitranS%bet    , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( hitranS%delt    , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus

      if (sumdeAllocStatus /= 0) then
        print *, 'freeMemHITRAN_lineParameters has nonzero sumdeAllocStatus. deAlloc Failed'
        call mystop(errS, 'freeMemHITRAN_lineParameters has nonzero sumdeAllocStatus. deAlloc Failed')
        if (errorCheck(errS)) return
      end if

    end subroutine freeMemHITRAN_lineParameters


    subroutine freeMemHITRAN_ISO(errS, hitranS)

      implicit none

      type(errorType), intent(inout) :: errS
      type(HITRANType), intent(inout) :: hitranS

      integer :: deallocStatus
      integer :: sumdeAllocStatus

      deallocStatus    = 0
      sumdeAllocStatus = 0

      deallocate( hitranS%ISO,          STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( hitranS%isotopologue, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( hitranS%molWeight,    STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( hitranS%abundance,    STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus

      if (sumdeAllocStatus /= 0) then
        print *, 'freeMemHITRAN_ISO has nonzero sumAllocStatus. deAlloc Failed'
        call mystop(errS, 'freeMemHITRAN_ISO has nonzero sumAllocStatus. deAlloc Failed')
        if (errorCheck(errS)) return
      end if

    end subroutine freeMemHITRAN_ISO


    subroutine freeMemSDFS(errS, SDFS)

      implicit none

      type(errorType), intent(inout) :: errS
      type(SDFType), intent(inout) :: SDFS

      integer :: deallocStatus
      integer :: sumdeAllocStatus

      deallocStatus    = 0
      sumdeAllocStatus = 0

      deallocate( SDFS%sigLines   , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%modsigLines, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%PopuT0     , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%PopuT      , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%DipoR      , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%Dipo0      , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%Dipo       , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%E          , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%HWT0       , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%HWT        , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%BHW        , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%SHIFT      , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%YT         , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDFS%m1         , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus

      if (sumdeAllocStatus /= 0) then
        print *, 'freeMemSDFS has nonzero sumdeAllocStatus. deAlloc Failed'
        call mystop(errS, 'freeMemSDFS has nonzero sumdeAllocStatus. deAlloc Failed')
        if (errorCheck(errS)) return
      end if

    end subroutine freeMemSDFS


    subroutine freeMemRMFS(errS, RMFS)

      implicit none

      type(errorType), intent(inout) :: errS
      type(RMFType), intent(inout) :: RMFS

      integer :: deallocStatus
      integer :: sumdeAllocStatus

      deallocStatus    = 0
      sumdeAllocStatus = 0

      deallocate( RMFS%WT0, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( RMFS%WT , STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( RMFS%BW,  STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus

      if (sumdeAllocStatus /= 0) then
        print *, 'freeMemRMFS has nonzero sumdeAllocStatus. deAlloc Failed'
        call mystop(errS, 'freeMemRMFS has nonzero sumdeAllocStatus. deAlloc Failed')
        if (errorCheck(errS)) return
      end if

    end subroutine freeMemRMFS

end module HITRANModule
