!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

! ------------------------------------------------------------------------------
!
!
! ------------------------------------------------------------------------------

module DISAMAR_file

use DISAMAR_log

implicit none

! variables used by the input parser
type file_parser_type
    character(len=800) :: char = ""
    character(len=8) :: concat = "&"
    logical :: skipbl = .true.
    logical :: clear = .true.
    logical :: more
    integer :: item = 0
    integer :: nitems = 0
    integer :: loc(0:80) = 0
    integer :: end(80) = 0
    integer :: line(0:10) = 0
    integer :: level  = 0
    integer :: nerror = 0
    integer :: ir = 5
    integer :: last = 0
    integer :: unit(0:10)
    integer :: lc = 3
end type

type file_type
    character(len=256) :: name
    character, allocatable :: buffer(:)
    integer :: length
    integer :: cursor
    type(file_parser_type) :: parser
end type

type file_pointer_type
    type(file_type), pointer :: file
end type

integer, parameter :: file_list_max_length = 100

type file_list_type
    type(file_pointer_type), dimension(file_list_max_length) :: file_pointer
    integer :: length
end type

type(file_list_type) :: file_list

contains

    ! ------------------------------------------------------------------------------

    function load_file(name, file)

        implicit none

        integer :: load_file
        character(len=*) :: name
        type(file_type) :: file

        call enter('load_file')
        load_file = 0

        load_file = disamar_load_file(name, file%buffer)
        if (load_file == 0) then
            file%length = size(file%buffer)
        else
            file%length = 0
        end if
        file%cursor = 1

99999   continue
        call exit('load_file')

    end function load_file

    ! ------------------------------------------------------------------------------

    function disamar_load_file(name, buffer)

        implicit none

        integer :: disamar_load_file
        character(len=*) :: name
        character, allocatable :: buffer(:)

        character :: ch
        integer :: status
        integer :: unit
        integer :: length
        logical :: exist

        call enter('disamar_load_file')
        disamar_load_file = 0

        unit = 10

        call disamar_logger('opening ' // trim(name), LOG_DEBUG)
        open(unit=unit, action='read', status='old', iostat=status, &
            file=name, access='stream')

        if (status .ne. 0) then
            disamar_load_file = 1
            goto 99999
        end if

        inquire(unit=unit, size=length, exist=exist)
        allocate(buffer(length))

        length = 0
        do
            read(unit, end=100) ch
            length = length + 1
            buffer(length) = ch
        end do

100     continue
        close(unit=unit)

99999   continue
        call exit('disamar_load_file')

    end function disamar_load_file

    ! ------------------------------------------------------------------------------

    function disamar_check(status, message)

        implicit none

        logical :: disamar_check
        integer :: status
        character(len=*), optional :: message

        disamar_check = .false.
        if (status /= 0) then
            if (present(message)) call disamar_logger(message, LOG_ERROR)
            disamar_check = .true.
        end if

99999   continue

    end function


    ! ------------------------------------------------------------------------------

    function parse_str(str, delim, nsubstr, substr)

        implicit none

        integer :: parse_str
        character(len=*) :: str
        character(len=*) :: delim
        integer :: nsubstr
        character(len=*), dimension(*) :: substr

        integer :: i
        integer :: j
        integer :: n

        parse_str = 0
        nsubstr = 0

        n = len_trim(str)
        if (n == 0) goto 99999

        i = 1
        do while (i <= n)
            j = index(str(i:n), ' ')
            if (j == 0) j = n - i + 1
            nsubstr = nsubstr + 1
            substr(nsubstr) = str(i:i+j-1)
            i = i + j
        end do

99999   continue

    end function

    ! ------------------------------------------------------------------------------

    function file_get(file_name, file_index)

        implicit none

        integer :: file_get
        character(len=*) :: file_name
        integer :: file_index

        integer :: i

        file_get = 0

        do i = 1, file_list%length
            if (file_list%file_pointer(i)%file%name == file_name) then
                file_index = i
                goto 99999
            end if
        end do

        if (file_list%length >= file_list_max_length) then
            file_get = 1
            goto 99999
        end if

        file_list%length = file_list%length + 1
        allocate(file_list%file_pointer(file_list%length)%file)
        file_list%file_pointer(file_list%length)%file%name = file_name
        file_list%file_pointer(file_list%length)%file%length = 0
        file_list%file_pointer(file_list%length)%file%cursor = 1
        file_index = file_list%length

99999   continue

    end function

    ! ------------------------------------------------------------------------------

function file_readline(file, line)

    use DISAMAR_log

    implicit none

    integer :: file_readline
    type(file_type) :: file
    character(len=*) :: line

    character :: ch
    integer :: start
    integer :: index
    integer :: length

!    call enter('file_readline')

    file_readline = 0
    start = file%cursor
    index = 1
    length = len(line)

    do
        if (file%cursor > file%length) then
            file_readline = 1
            exit
        end if
        ch = file%buffer(file%cursor)
        if (ch == achar(13)) ch = ' '
        if (ch == NEW_LINE('A')) exit
        if (index <= length) line(index:index) = ch
        file%cursor = file%cursor + 1
        index = index + 1
    end do
    do while (index <= length)
        line(index:index) = ' '
        index = index + 1
    end do

    file%cursor = file%cursor + 1

999 continue
!    call exit('file_readline')

end function

! ------------------------------------------------------------------------------

    function file_backspace(file)

        implicit none

        integer :: file_backspace
        type(file_type) :: file

        character :: ch

        file_backspace = 0

        do
            if (file%cursor == 0) exit
            file%cursor = file%cursor - 1
            ch = file%buffer(file%cursor)
            if (ch == NEW_LINE('A')) exit
        end do
        do
            if (file%cursor == 0) exit
            file%cursor = file%cursor - 1
            ch = file%buffer(file%cursor)
            if (ch == NEW_LINE('A')) exit
        end do

        file%cursor = file%cursor + 1

99999   continue

    end function file_backspace

! ------------------------------------------------------------------------------

    function file_rewind(file)

        implicit none

        integer :: file_rewind
        type(file_type) :: file

        file_rewind = 0
        file%cursor = 1

99999   continue

    end function file_rewind

! ------------------------------------------------------------------------------

end module
