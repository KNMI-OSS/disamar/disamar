!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module DISAMAR_interface

    !
    !
    !
    !
    !

    use, intrinsic         :: iso_c_binding
    use DISAMAR_log
    use dataStructures
    use staticDataModule
    use disamarModule

    implicit none

    integer, parameter :: STATUS_S_SUCCESS                                 = 0  !< No error.
    integer, parameter :: STATUS_E_ASSERTION_ERROR                         = 1  !< Assertion failed.
    integer, parameter :: STATUS_E_GENERIC_EXCEPTION                       = 2  !< Catch all error.
    integer, parameter :: STATUS_E_ABORT_ERROR                             = 3  !< Processor has been aborted.
    integer, parameter :: STATUS_E_CONFIGURATION_ERROR                     = 4  !< Error in the configuration of the processor.
    integer, parameter :: STATUS_E_KEY_ERROR                               = 5  !< Unrecognised key in the key-value interface.
    integer, parameter :: STATUS_E_BUFFER_SIZE_ERROR                       = 6  !< Provided buffer is too small (retry with increased buffer).
    integer, parameter :: STATUS_E_DIMENSIONS_ERROR                        = 7  !< The dimension sizes are not as expected.
    integer, parameter :: STATUS_E_MEMORY_ALLOCATION_ERROR                 = 8  !< Memory allocation failed.
    integer, parameter :: STATUS_E_CONSISTENCY_ERROR                       = 9  !< Provided input is not internally consistent.
    integer, parameter :: STATUS_E_RANK_ERROR                              = 10 !< The rank of the provided array is incorrect.
    integer, parameter :: STATUS_E_MEMORY_DEALLOCATION_ERROR               = 11 !< Error while releasing memory.
    integer, parameter :: STATUS_E_EXPECTED_SCALAR                         = 12 !< Expected a single value (rank 1, size 1).
    integer, parameter :: STATUS_E_INCOMPLETE_INPUT_DATA                   = 13 !< Not all input data has been transferred
    integer, parameter :: STATUS_E_READING_UNAVAILABLE_DATA                = 14 !< The data you are trying to read has not been set (yet).
    integer, parameter :: STATUS_E_DATA_SEQUENCE_ERROR                     = 15 !< The order in which parameters are passed in is incorrect.
    integer, parameter :: STATUS_E_VALUE_ERROR                             = 16 !< The value passed to the interface is outside the valid range for this parameter

    contains

    ! -------------------------------------------------------------------------------

    function c_disamar_allocate_static_workspace() bind(C)

        implicit none

        integer(c_int) :: c_disamar_allocate_static_workspace

        call enter('c_disamar_allocate_static_workspace')
        c_disamar_allocate_static_workspace = STATUS_S_SUCCESS

        c_disamar_allocate_static_workspace = disamar_allocate_static_workspace()

        staticS%fort = .false.

99999   continue
        call exit('c_disamar_allocate_static_workspace')

    end function c_disamar_allocate_static_workspace

    ! -------------------------------------------------------------------------------

    function disamar_allocate_static_workspace()

        implicit none

        integer :: disamar_allocate_static_workspace

        type(errorType) :: errS

        call enter('disamar_allocate_static_workspace')
        disamar_allocate_static_workspace = STATUS_S_SUCCESS

        call errorInit(errS)
        call errorSetInteractive(errS, .true.)

        call claimStaticS(errS, staticS)
        if (errorCheck(errS)) goto 99999

        staticS%fort = .true.

99999   continue
        call exit('disamar_allocate_static_workspace')

    end function disamar_allocate_static_workspace

    ! -------------------------------------------------------------------------------

    function c_disamar_allocate_dynamic_workspace(c_dynamic_workspace) bind(C)

        implicit none

        integer(c_int) :: c_disamar_allocate_dynamic_workspace
        type(c_ptr) :: c_dynamic_workspace

        type(globalType), pointer :: dynamic_workspace

        call enter('c_disamar_allocate_dynamic_workspace')
        c_disamar_allocate_dynamic_workspace = STATUS_S_SUCCESS

        c_disamar_allocate_dynamic_workspace = disamar_allocate_dynamic_workspace(dynamic_workspace)
        c_dynamic_workspace = c_loc(dynamic_workspace)

99999   continue
        call exit('c_disamar_allocate_dynamic_workspace')

    end function c_disamar_allocate_dynamic_workspace

    ! -------------------------------------------------------------------------------

    function disamar_allocate_dynamic_workspace(dynamic_workspace)

        implicit none

        integer :: disamar_allocate_dynamic_workspace
        type(globalType), pointer :: dynamic_workspace

        type(errorType) :: errS

        call enter('disamar_allocate_dynamic_workspace')
        disamar_allocate_dynamic_workspace = STATUS_S_SUCCESS

        call errorInit(errS)
        call errorSetInteractive(errS, .true.)

        call claimGlobalS(errS, dynamic_workspace)

        if (errorCheck(errS)) goto 99999

99999   continue
        call exit('disamar_allocate_dynamic_workspace')

    end function disamar_allocate_dynamic_workspace

    ! -------------------------------------------------------------------------------

    function c_disamar_deallocate_static_workspace() bind(C)

        implicit none

        integer(c_int) :: c_disamar_deallocate_static_workspace

        call enter('c_disamar_deallocate_static_workspace')
        c_disamar_deallocate_static_workspace = STATUS_S_SUCCESS

        c_disamar_deallocate_static_workspace = disamar_deallocate_static_workspace()

99999   continue
        call exit('c_disamar_deallocate_static_workspace')

    end function c_disamar_deallocate_static_workspace

    ! -------------------------------------------------------------------------------

    function disamar_deallocate_static_workspace()

        implicit none

        integer :: disamar_deallocate_static_workspace

        type(errorType) :: errS

        call enter('disamar_deallocate_static_workspace')
        disamar_deallocate_static_workspace = STATUS_S_SUCCESS

        call errorInit(errS)
        call errorSetInteractive(errS, .true.)

        call freeStaticS(errS, staticS)
        if (errorCheck(errS)) goto 99999

99999   continue
        call exit('disamar_deallocate_static_workspace')

    end function disamar_deallocate_static_workspace

    ! -------------------------------------------------------------------------------

    function c_disamar_deallocate_dynamic_workspace(c_dynamic_workspace) bind(C)

        implicit none

        integer(c_int) :: c_disamar_deallocate_dynamic_workspace
        type(c_ptr) :: c_dynamic_workspace

        type(globalType), pointer :: dynamic_workspace

        call enter('c_disamar_deallocate_dynamic_workspace')
        c_disamar_deallocate_dynamic_workspace = STATUS_S_SUCCESS

        call c_f_pointer(c_dynamic_workspace, dynamic_workspace)
        c_disamar_deallocate_dynamic_workspace = disamar_deallocate_dynamic_workspace(dynamic_workspace)

99999   continue
        call exit('c_disamar_deallocate_dynamic_workspace')

    end function c_disamar_deallocate_dynamic_workspace

    ! -------------------------------------------------------------------------------

    function disamar_deallocate_dynamic_workspace(dynamic_workspace)

        implicit none

        integer :: disamar_deallocate_dynamic_workspace
        type(globalType), pointer :: dynamic_workspace

        type(errorType) :: errS

        call enter('disamar_deallocate_dynamic_workspace')
        disamar_deallocate_dynamic_workspace = STATUS_S_SUCCESS

        call errorInit(errS)
        call errorSetInteractive(errS, .true.)

        call freeGlobalS(errS, dynamic_workspace)
        if (errorCheck(errS)) goto 99999

99999   continue
        call exit('disamar_deallocate_dynamic_workspace')

    end function disamar_deallocate_dynamic_workspace

    ! -------------------------------------------------------------------------------

    function c_disamar_set_static_input_c(c_key, c_lkey, c_value, c_lvalue) bind(C)

        implicit none

        integer(c_int) :: c_disamar_set_static_input_c
        type(c_ptr), value :: c_key
        integer(c_int), value :: c_lkey
        type(c_ptr), value :: c_value
        integer(c_int), value :: c_lvalue

        character(kind=c_char, len=c_lkey), pointer :: fp_key
        character(len=:), allocatable :: key
        character(kind=c_char, len=c_lvalue), pointer :: fp_value
        character(len=:), allocatable :: value
        character :: value2(c_lvalue)
        integer :: i

        call enter('c_disamar_set_static_input_c')
        c_disamar_set_static_input_c = STATUS_S_SUCCESS

        call c_f_pointer(cptr=c_key, fptr=fp_key)
        key = fp_key
        fp_key => null()

        call c_f_pointer(cptr=c_value, fptr=fp_value)
        value = fp_value
        fp_value => null()

        do i = 1, c_lvalue
            value2(i) = value(i:i)
        end do

        c_disamar_set_static_input_c = disamar_set_static_input_c(key, len(key), value2, size(value2))

99999   continue
        call exit('c_disamar_set_static_input_c')

    end function c_disamar_set_static_input_c

    ! -------------------------------------------------------------------------------

    function disamar_set_static_input_c(key, lkey, value, lvalue)

        implicit none

        integer :: disamar_set_static_input_c
        character(len=*) :: key
        integer :: lkey
        character :: value(:)
        integer :: lvalue

        call enter('disamar_set_static_input_c')
        disamar_set_static_input_c = STATUS_S_SUCCESS

        if (key .eq. 'config') then
            allocate(staticS%configFileS%buffer(size(value)))
            staticS%configFileS%buffer(:) = value(:)
            staticS%configFileS%length = size(value)
            staticS%configFileS%cursor = 1
        else
            call disamar_logger("Unknown key: " // trim(key), LOG_DEBUG)
            disamar_set_static_input_c = STATUS_E_KEY_ERROR
        end if

99999   continue
        call exit('disamar_set_static_input_c')

    end function disamar_set_static_input_c

    !  -------------------------------------------------------------------------------

    function c_disamar_set_input_d(c_key, c_lkey, c_ndims, c_dims, c_value, c_dynamic_workspace) bind(C)

        implicit none

        integer(c_int) :: c_disamar_set_input_d
        type(c_ptr), value :: c_key
        integer(c_int), value :: c_lkey
        integer(c_int), value :: c_ndims
        type(c_ptr), value :: c_dims
        type(c_ptr), value :: c_value
        type(c_ptr) :: c_dynamic_workspace

        character(kind=c_char, len=c_lkey), pointer :: fp_key
        character(len=:), allocatable :: key
        type(globalType), pointer :: dynamic_workspace
        integer, pointer :: dims(:)
        real(8), pointer :: value(:)
        real(8), pointer :: value_reordered(:)
        integer :: i
        integer :: nvalue
        integer :: ndims

        call enter('c_disamar_set_input_d')
        c_disamar_set_input_d = STATUS_S_SUCCESS

        call c_f_pointer(c_key, fp_key)
        key = fp_key
        fp_key => null()

        ndims = c_ndims
        call c_f_pointer(c_dims, dims, shape=[ndims])

        nvalue = 1
        do i = 1, ndims
            nvalue = nvalue * dims(i)
        end do

        call c_f_pointer(c_value, value, shape=[nvalue])
        call c_f_pointer(c_dynamic_workspace, dynamic_workspace)

        c_disamar_set_input_d = disamar_set_input_d(key, len(key), ndims, dims, value, dynamic_workspace)

99999   continue
        call exit('c_disamar_set_input_d')

    end function c_disamar_set_input_d

    ! -------------------------------------------------------------------------------

    function disamar_set_input_d(key, lkey, ndims, dims, value, dynamic_workspace)

        implicit none

        integer :: disamar_set_input_d
        character(len=*) :: key
        integer :: lkey
        integer :: ndims
        integer :: dims(:)
        real(8) :: value(:)
        type(globalType), pointer :: dynamic_workspace

        integer :: i, j, k, idx, nvalue

        call enter('disamar_set_input_d')
        disamar_set_input_d = STATUS_S_SUCCESS

        staticS%operational = 1

        nvalue = 1
        do i = 1, ndims
            nvalue = nvalue * dims(i)
        end do

        select case (trim(key))

            case ('o2_refspec_wavelength')
                if (.not. associated(staticS%hr_wavel)) then
                    allocate(staticS%hr_wavel)
                else
                    if ((nvalue /= staticS%hr_wavel%nwavel) .and. (staticS%hr_wavel%nwavel .gt. 0)) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                end if

                if (.not. associated(staticS%o2xsection)) then
                    allocate(staticS%o2xsection)
                else
                    if ((nvalue /= staticS%o2xsection%nwavelHR) .and. (staticS%o2xsection%nwavelHR .gt. 0)) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                end if

                if (.not. associated(staticS%o2o2xsection)) then
                    allocate(staticS%o2o2xsection)
                else
                    if ((nvalue /= staticS%o2o2xsection%nwavelHR) .and. (staticS%o2o2xsection%nwavelHR .gt. 0)) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                end if

                if (associated(staticS%hr_wavel%wavel)) then
                    deallocate(staticS%hr_wavel%wavel)
                end if

                staticS%hr_wavel%nwavel = nvalue
                staticS%o2xsection%nwavelHR = nvalue
                staticS%o2o2xsection%nwavelHR = nvalue

                allocate(staticS%hr_wavel%wavel(nvalue))
                staticS%hr_wavel%wavel(1:nvalue) = value(1:nvalue)

            case ('o2_refspec_weight')
                if (.not. associated(staticS%hr_wavel)) then
                    allocate(staticS%hr_wavel)
                else
                    if ((nvalue /= staticS%hr_wavel%nwavel) .and. (staticS%hr_wavel%nwavel .gt. 0)) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                end if

                if (.not. associated(staticS%o2xsection)) then
                    allocate(staticS%o2xsection)
                else
                    if ((nvalue /= staticS%o2xsection%nwavelHR) .and. (staticS%o2xsection%nwavelHR .gt. 0)) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                end if

                if (.not. associated(staticS%o2o2xsection)) then
                    allocate(staticS%o2o2xsection)
                else
                    if ((nvalue /= staticS%o2o2xsection%nwavelHR) .and. (staticS%o2o2xsection%nwavelHR .gt. 0)) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                end if

                if (associated(staticS%hr_wavel%weights)) then
                    deallocate(staticS%hr_wavel%weights)
                end if

                staticS%hr_wavel%nwavel = nvalue
                staticS%o2xsection%nwavelHR = nvalue
                staticS%o2o2xsection%nwavelHR = nvalue

                allocate(staticS%hr_wavel%weights(nvalue))
                staticS%hr_wavel%weights(1:nvalue) = value(1:nvalue)

            case ('o2_refspec_pressure_min')
                if (.not. associated(staticS%o2xsection)) then
                    allocate(staticS%o2xsection)
                end if
                if (.not. associated(staticS%o2o2xsection)) then
                    allocate(staticS%o2o2xsection)
                end if

                if (nvalue .gt. 1) then
                    disamar_set_input_d = STATUS_E_EXPECTED_SCALAR
                    goto 99999
                end if

                staticS%o2xsection%pmin_LUT = value(1)
                staticS%o2o2xsection%pmin_LUT = value(1)

            case ('o2_refspec_pressure_max')
                if (.not. associated(staticS%o2xsection)) then
                    allocate(staticS%o2xsection)
                end if
                if (.not. associated(staticS%o2o2xsection)) then
                    allocate(staticS%o2o2xsection)
                end if

                if (nvalue /= 1) then
                    disamar_set_input_d = STATUS_E_EXPECTED_SCALAR
                    goto 99999
                end if

                staticS%o2xsection%pmax_LUT = value(1)
                staticS%o2o2xsection%pmax_LUT = value(1)

            case ('o2_refspec_temperature_min')
                if (nvalue /= 1) then
                    disamar_set_input_d = STATUS_E_EXPECTED_SCALAR
                    goto 99999
                end if

                if (.not. associated(staticS%o2xsection)) then
                    allocate(staticS%o2xsection)
                end if
                if (.not. associated(staticS%o2o2xsection)) then
                    allocate(staticS%o2o2xsection)
                end if

                staticS%o2xsection%Tmin_LUT = value(1)
                staticS%o2o2xsection%Tmin_LUT = value(1)

            case ('o2_refspec_temperature_max')
                if (nvalue /= 1) then
                    disamar_set_input_d = STATUS_E_EXPECTED_SCALAR
                    goto 99999
                end if

                if (.not. associated(staticS%o2xsection)) then
                    allocate(staticS%o2xsection)
                end if
                if (.not. associated(staticS%o2o2xsection)) then
                    allocate(staticS%o2o2xsection)
                end if

                staticS%o2xsection%Tmax_LUT = value(1)
                staticS%o2o2xsection%Tmax_LUT = value(1)

            case ('o2_refspec_coefficients')
                if (.not. associated(staticS%o2xsection)) then
                    disamar_set_input_d = STATUS_E_DATA_SEQUENCE_ERROR
                    goto 99999
                end if

                if (staticS%fort) then
                    if (ndims /= 3 .or. &
                        dims(1) /= staticS%o2xsection%nwavelHR .or. &
                        dims(2) /= staticS%o2xsection%ncoeff_lnp_LUT .or. &
                        dims(3) /= staticS%o2xsection%ncoeff_lnT_LUT) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                else
                    if (ndims /= 3 .or. &
                        dims(3) /= staticS%o2xsection%nwavelHR .or. &
                        dims(2) /= staticS%o2xsection%ncoeff_lnp_LUT .or. &
                        dims(1) /= staticS%o2xsection%ncoeff_lnT_LUT) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                endif

                if (associated(staticS%o2xsection%coeff_lnTlnp_LUT)) then
                    deallocate(staticS%o2xsection%coeff_lnTlnp_LUT)
                end if
                allocate(staticS%o2xsection%coeff_lnTlnp_LUT(dims(1), dims(2), dims(3)))
                if (staticS%fort) then ! rearrange memory when calling from Fortran
                    do i = 1, staticS%o2xsection%ncoeff_lnT_LUT
                        do j = 1, staticS%o2xsection%ncoeff_lnp_LUT
                            do k = 1, staticS%o2xsection%nwavelHR
                                idx = ((k-1)*staticS%o2xsection%ncoeff_lnp_LUT + (j-1)) * staticS%o2xsection%ncoeff_lnT_LUT + (i-1)
                                staticS%o2xsection%coeff_lnTlnp_LUT(i, j, k) = value(idx)
                            end do
                        end do
                    end do
                else
                    staticS%o2xsection%coeff_lnTlnp_LUT(1:dims(1), 1:dims(2), 1:dims(3)) = reshape(value(1:nvalue), dims(1:3))
                endif
                staticS%o2xsection%createXsecPolyLUT = .false.
                staticS%o2XsectionLoaded = .true.

            case ('o2o2_refspec_coefficients')
                if (.not. associated(staticS%o2o2xsection)) then
                    disamar_set_input_d = STATUS_E_DATA_SEQUENCE_ERROR
                    goto 99999
                end if

                if (staticS%fort) then
                    if (ndims /= 3 .or. &
                        dims(1) /= staticS%o2xsection%nwavelHR .or. &
                        dims(2) /= staticS%o2xsection%ncoeff_lnp_LUT .or. &
                        dims(3) /= staticS%o2xsection%ncoeff_lnT_LUT) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                else
                    if (ndims /= 3 .or. &
                        dims(3) /= staticS%o2xsection%nwavelHR .or. &
                        dims(2) /= staticS%o2xsection%ncoeff_lnp_LUT .or. &
                        dims(1) /= staticS%o2xsection%ncoeff_lnT_LUT) then
                        disamar_set_input_d = STATUS_E_CONSISTENCY_ERROR
                        goto 99999
                    end if
                endif

                if (associated(staticS%o2o2xsection%coeff_lnTlnp_LUT)) then
                    deallocate(staticS%o2o2xsection%coeff_lnTlnp_LUT)
                end if
                allocate(staticS%o2o2xsection%coeff_lnTlnp_LUT(dims(1), dims(2), dims(3)))
                if (staticS%fort) then
                    ! rearrange memory when calling from Fortran
                    do i = 1, staticS%o2xsection%ncoeff_lnT_LUT
                        do j = 1, staticS%o2xsection%ncoeff_lnp_LUT
                            do k = 1, staticS%o2xsection%nwavelHR
                                idx = ((k-1)*staticS%o2xsection%ncoeff_lnp_LUT + (j-1)) * staticS%o2xsection%ncoeff_lnT_LUT + (i-1)
                                staticS%o2o2xsection%coeff_lnTlnp_LUT(i, j, k) = value(idx)
                            end do
                        end do
                    end do
                else
                    staticS%o2o2xsection%coeff_lnTlnp_LUT(1:dims(1), 1:dims(2), 1:dims(3)) = reshape(value(1:nvalue), dims(1:3))
                end if
                staticS%o2o2xsection%createXsecPolyLUT = .false.
                staticS%o2XsectionLoaded = .true.

            case ('hires_wavelength')
                if (allocated(staticS%hires_wavelength)) then
                    deallocate(staticS%hires_wavelength)
                end if
                allocate(staticS%hires_wavelength(nvalue))
                staticS%hires_wavelength(1:nvalue) = value(1:nvalue)

            case ('hires_solar')
                if (allocated(staticS%hires_solar)) then
                    deallocate(staticS%hires_solar)
                end if
                allocate(staticS%hires_solar(nvalue))
                staticS%hires_solar(1:nvalue) = value(1:nvalue)

            case ('isrf_wavelength')
                if (allocated(staticS%isrf_wavelength)) then
                    deallocate(staticS%isrf_wavelength)
                end if
                allocate(staticS%isrf_wavelength(nvalue))
                staticS%isrf_wavelength(1:nvalue) = value(1:nvalue)

            case ('isrf_offset')
                if (allocated(staticS%isrf_offset)) then
                    deallocate(staticS%isrf_offset)
                end if
                allocate(staticS%isrf_offset(nvalue))
                staticS%isrf_offset(1:nvalue) = value(1:nvalue)

            case ('isrf')
                if (allocated(staticS%isrf)) then
                    deallocate(staticS%isrf)
                end if
                allocate(staticS%isrf(nvalue))
                staticS%isrf(1:nvalue) = value(1:nvalue)

            case ('solar_zenith_angle')
                dynamic_workspace%inputS%solarZenithAngle = value(1)

            case ('solar_azimuth_angle')
                dynamic_workspace%inputS%solarAzimuthAngle = value(1)

            case ('viewing_zenith_angle')
                dynamic_workspace%inputS%instrumentNadirAngle = value(1)

            case ('viewing_azimuth_angle')
                dynamic_workspace%inputS%instrumentAzimuthAngle = value(1)

            case ('surface_pressure')
                dynamic_workspace%inputS%surfacePressure = value(1)

            case ('surface_altitude')
                dynamic_workspace%inputS%surfaceAltitude = value(1)

            case ('pressure')
                if (allocated(dynamic_workspace%inputS%pressure)) then
                    deallocate(dynamic_workspace%inputS%pressure)
                end if
                allocate(dynamic_workspace%inputS%pressure(nvalue))
                dynamic_workspace%inputS%pressure(1:nvalue) = value(1:nvalue)

            case ('temperature')
                if (allocated(dynamic_workspace%inputS%temperature)) then
                    deallocate(dynamic_workspace%inputS%temperature)
                end if
                allocate(dynamic_workspace%inputS%temperature(nvalue))
                dynamic_workspace%inputS%temperature(1:nvalue) = value(1:nvalue)

            case ('radiance')
                if (allocated(dynamic_workspace%inputS%radiance)) then
                    deallocate(dynamic_workspace%inputS%radiance)
                end if
                allocate(dynamic_workspace%inputS%radiance(1:nvalue))
                dynamic_workspace%inputS%radiance(1:nvalue) = value(1:nvalue)

            case ('radiance_error')
                if (allocated(dynamic_workspace%inputS%radianceError)) then
                    deallocate(dynamic_workspace%inputS%radianceError)
                end if
                allocate(dynamic_workspace%inputS%radianceError(1:nvalue))
                dynamic_workspace%inputS%radianceError(1:nvalue) = value(1:nvalue)

            case ('radiance_noise')
                if (allocated(dynamic_workspace%inputS%radianceNoise)) then
                    deallocate(dynamic_workspace%inputS%radianceNoise)
                end if
                allocate(dynamic_workspace%inputS%radianceNoise(1:nvalue))
                dynamic_workspace%inputS%radianceNoise(1:nvalue) = value(1:nvalue)

            case ('radiance_wavelength')
                if (allocated(dynamic_workspace%inputS%radianceWavelength)) then
                    deallocate(dynamic_workspace%inputS%radianceWavelength)
                end if
                allocate(dynamic_workspace%inputS%radianceWavelength(1:nvalue))
                dynamic_workspace%inputS%radianceWavelength(1:nvalue) = value(1:nvalue)

            case ('irradiance')
                if (allocated(dynamic_workspace%inputS%irradiance)) then
                    deallocate(dynamic_workspace%inputS%irradiance)
                end if
                allocate(dynamic_workspace%inputS%irradiance(1:nvalue))
                dynamic_workspace%inputS%irradiance(1:nvalue) = value(1:nvalue)

            case ('irradiance_error')
                if (allocated(dynamic_workspace%inputS%irradianceError)) then
                    deallocate(dynamic_workspace%inputS%irradianceError)
                end if
                allocate(dynamic_workspace%inputS%irradianceError(1:nvalue))
                dynamic_workspace%inputS%irradianceError(1:nvalue) = value(1:nvalue)

            case ('irradiance_noise')
                if (allocated(dynamic_workspace%inputS%irradianceNoise)) then
                    deallocate(dynamic_workspace%inputS%irradianceNoise)
                end if
                allocate(dynamic_workspace%inputS%irradianceNoise(1:nvalue))
                dynamic_workspace%inputS%irradianceNoise(1:nvalue) = value(1:nvalue)

            case ('irradiance_wavelength')
                if (allocated(dynamic_workspace%inputS%irradianceWavelength)) then
                    deallocate(dynamic_workspace%inputS%irradianceWavelength)
                end if
                allocate(dynamic_workspace%inputS%irradianceWavelength(1:nvalue))
                dynamic_workspace%inputS%irradianceWavelength(1:nvalue) = value(1:nvalue)

            case ('surface_albedo')
                if (allocated(dynamic_workspace%inputS%surfaceAlbedo)) then
                    deallocate(dynamic_workspace%inputS%surfaceAlbedo)
                end if
                allocate(dynamic_workspace%inputS%surfaceAlbedo(1:nvalue))
                dynamic_workspace%inputS%surfaceAlbedo(1:nvalue) = value(1:nvalue)

            case ('surface_albedo_wavelength')
                if (allocated(dynamic_workspace%inputS%surfaceAlbedoWavelength)) then
                    deallocate(dynamic_workspace%inputS%surfaceAlbedoWavelength)
                end if
                allocate(dynamic_workspace%inputS%surfaceAlbedoWavelength(1:nvalue))
                dynamic_workspace%inputS%surfaceAlbedoWavelength(1:nvalue) = value(1:nvalue)

            case default
                call disamar_logger("Unknown key: " // trim(key), LOG_DEBUG)
                disamar_set_input_d = STATUS_E_KEY_ERROR

        end select

99999   continue
        call exit('disamar_set_input_d')

    end function disamar_set_input_d

    ! -------------------------------------------------------------------------------

    function c_disamar_set_input_i(c_key, c_lkey, c_ndims, c_dims, c_value, c_dynamic_workspace) bind(C)

        implicit none

        integer(c_int) :: c_disamar_set_input_i
        type(c_ptr), value :: c_key
        integer(c_int), value :: c_lkey
        integer(c_int), value :: c_ndims
        type(c_ptr), value :: c_dims
        type(c_ptr), value :: c_value
        type(c_ptr) :: c_dynamic_workspace

        character(kind=c_char, len=c_lkey), pointer :: fp_key
        character(len=:), allocatable :: key
        type(globalType), pointer :: dynamic_workspace
        integer, pointer :: dims(:)
        integer, pointer :: value(:)
        integer :: i
        integer :: nvalue
        integer :: ndims

        call enter('c_disamar_set_input_i')
        c_disamar_set_input_i = STATUS_S_SUCCESS

        call c_f_pointer(c_key, fp_key)
        key = fp_key
        fp_key => null()

        ndims = c_ndims
        call c_f_pointer(c_dims, dims, shape=[ndims])

        nvalue = 1
        do i = 1, ndims
            nvalue = nvalue * dims(i)
        end do

        call c_f_pointer(c_value, value, shape=[nvalue])
        call c_f_pointer(c_dynamic_workspace, dynamic_workspace)

        c_disamar_set_input_i = disamar_set_input_i(key, len(key), ndims, dims, value, dynamic_workspace)

99999   continue
        call exit('c_disamar_set_input_i')

    end function c_disamar_set_input_i

    ! -------------------------------------------------------------------------------

    function disamar_set_input_i(key, lkey, ndims, dims, value, dynamic_workspace)

        implicit none

        integer :: disamar_set_input_i
        character(len=*) :: key
        integer :: lkey
        integer :: ndims
        integer :: dims(:)
        integer :: value(:)
        type(globalType), pointer :: dynamic_workspace

        integer :: i, nvalue

        call enter('disamar_set_input_i')
        disamar_set_input_i = STATUS_S_SUCCESS

        staticS%operational = 1

        nvalue = 1
        do i = 1, ndims
            nvalue = nvalue * dims(i)
        end do

        select case (trim(key))

            case ('nrows')
                staticS%nrows = value(1)

            case ('pixel_index')
                dynamic_workspace%inputS%pixel_index = value(1)

            case ('o2_refspec_npressure')
                if (.not. associated(staticS%o2xsection)) then
                    allocate(staticS%o2xsection)
                end if
                if (.not. associated(staticS%o2o2xsection)) then
                    allocate(staticS%o2o2xsection)
                end if

                staticS%o2xsection%ncoeff_lnp_LUT = value(1)
                staticS%o2o2xsection%ncoeff_lnp_LUT = value(1)

            case ('o2_refspec_ntemperature')
                if (.not. associated(staticS%o2xsection)) then
                    allocate(staticS%o2xsection)
                end if
                if (.not. associated(staticS%o2o2xsection)) then
                    allocate(staticS%o2o2xsection)
                end if

                staticS%o2xsection%ncoeff_lnT_LUT = value(1)
                staticS%o2o2xsection%ncoeff_lnT_LUT = value(1)

            case default
                call disamar_logger("Unknown key: " // trim(key), LOG_DEBUG)
                disamar_set_input_i = STATUS_E_KEY_ERROR

        end select

99999   continue
        call exit('disamar_set_input_i')

    end function disamar_set_input_i

    ! -------------------------------------------------------------------------------

    function c_disamar_retrieval(c_dynamic_workspace) bind(C)

        implicit none

        integer(c_int) :: c_disamar_retrieval
        type(c_ptr) :: c_dynamic_workspace

        type(globalType), pointer :: dynamic_workspace

        call enter('c_disamar_retrieval')
        c_disamar_retrieval = STATUS_S_SUCCESS

        call c_f_pointer(c_dynamic_workspace, dynamic_workspace)
        c_disamar_retrieval = disamar_retrieval(dynamic_workspace)

99999   continue
        call exit('c_disamar_retrieval')

    end function c_disamar_retrieval

    ! -------------------------------------------------------------------------------

    function disamar_retrieval(dynamic_workspace)

        implicit none

        integer :: disamar_retrieval
        type(globalType), pointer :: dynamic_workspace

        type(errorType) :: errS

        call enter('disamar_retrieval')
        disamar_retrieval = STATUS_S_SUCCESS

        if (staticS%configFileS%length .eq. 0) then
            disamar_retrieval = STATUS_E_CONFIGURATION_ERROR
            goto 99999
        end if

        allocate(dynamic_workspace%configFileS%buffer(staticS%configFileS%length))
        dynamic_workspace%configFileS%buffer(:) = staticS%configFileS%buffer(:)
        dynamic_workspace%configFileS%length = staticS%configFileS%length
        dynamic_workspace%configFileS%cursor = 1

        if (staticS%operational == 1) then
            if (size(staticS%hires_wavelength) .eq. 0) then
                disamar_retrieval = STATUS_E_CONFIGURATION_ERROR
                goto 99999
            end if

            if (size(staticS%hires_solar) .eq. 0) then
                disamar_retrieval = STATUS_E_CONFIGURATION_ERROR
                goto 99999
            end if
        end if

        call errorInit(errS)
        call errorSetInteractive(errS, .false.)

        call init(errS, dynamic_workspace)
        if (errorCheck(errS)) then
            disamar_retrieval = STATUS_E_CONFIGURATION_ERROR
            goto 99999
        end if

        call retrieve(errS, dynamic_workspace)
        if (errorCheck(errS)) then
            disamar_retrieval = STATUS_E_GENERIC_EXCEPTION
            goto 99999
        end if

99999   continue
        call exit('disamar_retrieval')

    end function disamar_retrieval

    ! -------------------------------------------------------------------------------

    function c_disamar_get_output_c(c_key, c_lkey, c_value, c_lvalue, c_dynamic_workspace) bind(C)

        implicit none

        integer(c_int) :: c_disamar_get_output_c
        type(c_ptr), value :: c_key
        integer(c_int), value :: c_lkey
        type(c_ptr), value :: c_value
        integer(c_int) :: c_lvalue
        type(c_ptr) :: c_dynamic_workspace

        character(kind=c_char, len=c_lkey), pointer :: fp_key
        character(len=:), allocatable :: key
        character(kind=c_char, len=c_lvalue), pointer :: value
        type(globalType), pointer :: dynamic_workspace
        integer :: lkey
        integer :: lvalue

        call enter('c_disamar_get_output_c')
        c_disamar_get_output_c = STATUS_S_SUCCESS

        call c_f_pointer(c_key, fp_key)
        key = fp_key
        lkey = c_lkey

        call c_f_pointer(cptr=c_value, fptr=value)
        lvalue = c_lvalue

        call c_f_pointer(c_dynamic_workspace, dynamic_workspace)

        c_disamar_get_output_c = disamar_get_output_c(key, lkey, value, lvalue, dynamic_workspace)
        if (c_disamar_get_output_c /= STATUS_S_SUCCESS) then
            value(1:1) = c_null_char
            goto 99999
        end if

        value(lvalue+1:lvalue+1) = c_null_char

99999   continue
        c_lvalue = lvalue
        call exit('c_disamar_get_output_c')

    end function c_disamar_get_output_c

    ! -------------------------------------------------------------------------------

    function disamar_get_output_c(key, lkey, value, lvalue, dynamic_workspace)

        implicit none

        integer :: disamar_get_output_c
        character(len=*) :: key
        integer :: lkey
        integer :: lvalue
        character(len=lvalue) :: value
        type(globalType), pointer :: dynamic_workspace

        integer :: istate, length = 0

        call enter('disamar_get_output_c')
        disamar_get_output_c = STATUS_S_SUCCESS

        select case (trim(key))

            case ("version")
                length = len(trim(DISAMAR_version_number))
                if ((length + 1) > lvalue) then
                    disamar_get_output_c = STATUS_E_BUFFER_SIZE_ERROR
                    lvalue = length
                    goto 99999
                end if
                value = DISAMAR_version_number
                lvalue = len(trim(value))

            case ("state_vector_elements")
                value = ""
                if (dynamic_workspace%retrS%nstate > 0) then
                    length = len(trim(dynamic_workspace%retrS%codeFitParameters(1)))
                    do istate = 2, dynamic_workspace%retrS%nstate
                        length = length + 1 + len(trim(dynamic_workspace%retrS%codeFitParameters(istate)))
                    end do
                    if ((length + 1) > lvalue) then
                        disamar_get_output_c = STATUS_E_BUFFER_SIZE_ERROR
                        lvalue = length
                        goto 99999
                    end if
                    value = dynamic_workspace%retrS%codeFitParameters(1)
                    do istate = 2, dynamic_workspace%retrS%nstate
                        value = trim(value) // ',' // dynamic_workspace%retrS%codeFitParameters(istate)
                    end do
                end if
                lvalue = len(trim(value))

            case default
                call disamar_logger("Unknown key: " // trim(key), LOG_DEBUG)
                disamar_get_output_c = STATUS_E_KEY_ERROR

        end select

99999   continue
        call exit('disamar_get_output_c')

    end function disamar_get_output_c

    ! -------------------------------------------------------------------------------

    function c_disamar_get_output_d(c_key, c_lkey, c_ndims, c_dims, c_value, c_dynamic_workspace) bind(C)

        implicit none

        integer(c_int) :: c_disamar_get_output_d
        type(c_ptr), value :: c_key
        integer(c_int), value :: c_lkey
        integer(c_int), value :: c_ndims
        type(c_ptr), value :: c_dims
        type(c_ptr), value :: c_value
        type(c_ptr) :: c_dynamic_workspace

        character(kind=c_char, len=c_lkey), pointer :: fp_key
        character(len=:), allocatable :: key
        type(globalType), pointer :: dynamic_workspace
        integer, pointer :: dims(:)
        real(8), pointer :: value(:)
        integer :: i
        integer :: nvalue
        integer :: ndims

        call enter('c_disamar_get_output_d')
        c_disamar_get_output_d = STATUS_S_SUCCESS

        call c_f_pointer(c_key, fp_key)
        key = fp_key
        fp_key => null()

        ndims = c_ndims
        call c_f_pointer(c_dims, dims, shape=[ndims])

        nvalue = 1
        do i = 1, ndims
            nvalue = nvalue * dims(i)
        end do

        call c_f_pointer(c_value, value, shape=[nvalue])
        call c_f_pointer(c_dynamic_workspace, dynamic_workspace)

        c_disamar_get_output_d = disamar_get_output_d(key, len(key), ndims, dims, value, dynamic_workspace)

99999   continue
        call exit('c_disamar_get_output_d')

    end function c_disamar_get_output_d

    ! -------------------------------------------------------------------------------

    function disamar_get_output_d(key, lkey, ndims, dims, value, dynamic_workspace)

        implicit none

        integer :: disamar_get_output_d
        character(len=*) :: key
        integer :: lkey
        integer :: ndims
        integer :: dims(:)
        real(8) :: value(:)
        type(globalType), pointer :: dynamic_workspace

        integer :: i, j, n, n1
        integer nvalue

        call enter('disamar_get_output_d')
        disamar_get_output_d = STATUS_S_SUCCESS

        nvalue = 1
        do i = 1, ndims
            nvalue = nvalue * dims(i)
        end do

        select case (trim(key))

            case ("assumed_layer_pressure_thickness_hPa")
                value(1) = dynamic_workspace%cloudAerosolRTMgridRetrS%intervalBoundsAP_P(1) - &
                           dynamic_workspace%cloudAerosolRTMgridRetrS%intervalBoundsAP_P(2)

            case ("aerosol_optical_thickness")
                i = dynamic_workspace%cloudAerosolRTMgridRetrS%numIntervalFit
                value(1) = dynamic_workspace%cloudAerosolRTMgridRetrS%intervalAerTau(i)

            case ("aerosol_optical_thickness_precision")
                value(1) = sqrt(dynamic_workspace%cloudAerosolRTMgridRetrS%varAerTau)

            case ("aerosol_mid_pressure")
                i = dynamic_workspace%cloudAerosolRTMgridRetrS%numIntervalFit
                value(1) = (dynamic_workspace%cloudAerosolRTMgridRetrS%intervalBounds_P(i) + &
                            dynamic_workspace%cloudAerosolRTMgridRetrS%intervalBounds_P(i - 1)) / 2

            case ("aerosol_mid_pressure_precision")
                i = dynamic_workspace%cloudAerosolRTMgridRetrS%numIntervalFit
                value(1) = sqrt(dynamic_workspace%cloudAerosolRTMgridRetrS%varIntervalBounds_P(i))

            case ("aerosol_mid_height")
                i = dynamic_workspace%cloudAerosolRTMgridRetrS%numIntervalFit
                call getAerosol_mid_altitude(value(1), log((dynamic_workspace%cloudAerosolRTMgridRetrS%intervalBounds_P(i) + &
                                                            dynamic_workspace%cloudAerosolRTMgridRetrS%intervalBounds_P(i - 1)) / 2), &
                                             dynamic_workspace%gasPTRetrS%lnpressure, dynamic_workspace%gasPTRetrS%altAP, &
                                             dynamic_workspace%gasPTRetrS%npressure)

            case ("aerosol_mid_height_precision")
                i = dynamic_workspace%cloudAerosolRTMgridRetrS%numIntervalFit
                value(1) = sqrt(dynamic_workspace%cloudAerosolRTMgridRetrS%varIntervalBounds(i))

            case ("chi_square")
                value(1) = dynamic_workspace%retrS%chi2

           case ("chi_square_reflectance")
                value(1) = dynamic_workspace%retrS%chi2R

            case ("chi_square_state_vector")
                value(1) = dynamic_workspace%retrS%chi2x

            case ("degrees_of_freedom")
                value(1) = dynamic_workspace%diagnosticS%DFS

            case ("root_mean_square_error_of_fit")
                value(1) = dynamic_workspace%retrS%rmse

            case ("covariance_matrix")
                n1 = size(dynamic_workspace%diagnosticS%error_correlation_other, 1)
                if (ndims /= 1) then
                    disamar_get_output_d = STATUS_E_RANK_ERROR
                    goto 99999
                end if
                if (nvalue .lt. (n1 * n1)) then
                    disamar_get_output_d = STATUS_E_BUFFER_SIZE_ERROR
                    goto 99999
                end if
                n = 0
                do i = 1, n1
                    do j = 1, n1
                        n = n + 1
                        value(n) = dynamic_workspace%diagnosticS%error_correlation_other(j, i)
                    end do
                end do

            case ("surface_albedo")
                n = 0
                if (associated(dynamic_workspace%surfaceRetrS)) then
                    n = sum(dynamic_workspace%surfaceRetrS(:)%nAlbedo)
                    if (nvalue .lt. n) then
                        disamar_get_output_d = STATUS_E_BUFFER_SIZE_ERROR
                        goto 99999
                    end if
                    j = 1
                    do i = 1, size(dynamic_workspace%surfaceRetrS)
                        n1 = dynamic_workspace%surfaceRetrS(i)%nAlbedo
                        if (n1 .gt. 0) then
                            value(j:j+n1-1) = dynamic_workspace%surfaceRetrS(i)%albedo(:)
                            j = j + n1
                        end if
                    end do
                end if

            case default
                call disamar_logger("Unknown key: " // trim(key), LOG_DEBUG)
                disamar_get_output_d = STATUS_E_KEY_ERROR

        end select

99999   continue
        call exit('disamar_get_output_d')

    end function disamar_get_output_d

    ! -------------------------------------------------------------------------------

    function c_disamar_get_output_i(c_key, c_lkey, c_ndims, c_dims, c_value, c_dynamic_workspace) bind(C)

        implicit none

        integer(c_int) :: c_disamar_get_output_i
        type(c_ptr), value :: c_key
        integer(c_int), value :: c_lkey
        integer(c_int), value :: c_ndims
        type(c_ptr), value :: c_dims
        type(c_ptr), value :: c_value
        type(c_ptr) :: c_dynamic_workspace

        character(kind=c_char, len=c_lkey), pointer :: fp_key
        character(len=:), allocatable :: key
        type(globalType), pointer :: dynamic_workspace
        integer, pointer :: dims(:)
        integer, pointer :: value(:)
        integer :: i
        integer :: nvalue
        integer :: ndims

        call enter('c_disamar_get_output_i')
        c_disamar_get_output_i = STATUS_S_SUCCESS

        call c_f_pointer(c_key, fp_key)
        key = fp_key
        fp_key => null()

        ndims = c_ndims
        call c_f_pointer(c_dims, dims, shape=[ndims])

        nvalue = 1
        do i = 1, ndims
            nvalue = nvalue * dims(i)
        end do

        call c_f_pointer(c_value, value, shape=[nvalue])
        call c_f_pointer(c_dynamic_workspace, dynamic_workspace)

        c_disamar_get_output_i = disamar_get_output_i(key, len(key), ndims, dims, value, dynamic_workspace)

99999   continue
        call exit('c_disamar_get_output_i')

    end function c_disamar_get_output_i

    ! -------------------------------------------------------------------------------

    function disamar_get_output_i(key, lkey, ndims, dims, value, dynamic_workspace)

        implicit none

        integer :: disamar_get_output_i
        character(len=*) :: key
        integer :: lkey
        integer :: ndims
        integer :: dims(:)
        integer :: value(:)
        type(globalType), pointer :: dynamic_workspace

        call enter('disamar_get_output_i')
        disamar_get_output_i = STATUS_S_SUCCESS

        select case (trim(key))

            case ("state_vector_length")
                value(1) = size(dynamic_workspace%diagnosticS%error_correlation_other, 1)

            case ("convergence_status")
                value(1) = 0
                if (dynamic_workspace%retrS%isConverged .eqv. .false.) value(1) = 1
                if (dynamic_workspace%iteration == dynamic_workspace%retrS%maxNumIterations) value(1) = 2
                if (dynamic_workspace%retrS%stateVectorElementAdjusted > 0) then
                    if (dynamic_workspace%retrS%codeFitParameters(dynamic_workspace%retrS%stateVectorElementAdjusted) == 'aerosolTau') then
                        value(1) = 3
                    end if
                end if
                if (dynamic_workspace%retrS%isConvergedBoundary) value(1) = 4

            case ("number_of_iterations")
                value(1) = dynamic_workspace%retrS%numIterations

            case ("number_of_spectral_points_in_retrieval")
                value(1) = dynamic_workspace%retrS%nwavelRetr

            case default
                call disamar_logger("Unknown key: " // trim(key), LOG_DEBUG)
                disamar_get_output_i = STATUS_E_KEY_ERROR

        end select

99999   continue
        call exit('disamar_get_output_i')

    end function disamar_get_output_i

    ! -------------------------------------------------------------------------------

end module DISAMAR_interface
