!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

! -----------------------------------------------------------------------------
!
! TROPOMI NL L2 Data Processors
!
! Copyright 2013-2015 KNMI
! This software is the proprietary information of KNMI
! All rights Reserved
!
! -----------------------------------------------------------------------------

module NetCDFmodule

  use netcdf

  implicit none

  integer, parameter :: logError   = 8
  integer, parameter :: logWarning = 5
  integer, parameter :: logInfo    = 4
  integer, parameter :: logDebug   = 2

  integer, parameter          :: FILENAME_LEN = 256
  integer, parameter          :: STR_LEN = 256

  integer                     :: ncfileid
  integer                     :: ncgroupid
  character*(FILENAME_LEN)    :: ncfilename

  interface NetCDF_put_var
     module procedure NetCDF_put_var_f3, NetCDF_put_var_f4
  end interface

  interface NetCDF_get_var
     module procedure NetCDF_get_var_d1, NetCDF_get_var_f1, NetCDF_get_var_f2, NetCDF_get_var_d3, NetCDF_get_var_f3, &
                      NetCDF_get_var_f4, NetCDF_get_var_d6, NetCDF_get_var_i1, NetCDF_get_var_i2, NetCDF_get_var_i3
  end interface

  interface NetCDF_put_att
     module procedure NetCDF_put_att_str
  end interface

  interface NetCDF_get_att
     module procedure NetCDF_get_att_i, NetCDF_get_att_str, NetCDF_get_att_d
  end interface

  interface NetCDF_get_var_att
     module procedure NetCDF_get_var_att_d
  end interface

contains

  ! -----------------------------------------------------------------------------

  function NetCDF_open(filename, mode)

    integer :: NetCDF_open
    character*(*) :: filename
    integer :: mode
    integer :: status

    ncfilename = filename

    call NetCDF_log_debug("Opening '" // trim(ncfilename) // "'")

    status = nf90_open(trim(ncfilename), mode, ncfileid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to open file '"  // trim(ncfilename) // "'")
       NetCDF_open = 1
       return
    end if

    ncgroupid = ncfileid

    NetCDF_open = 0

  end function NetCDF_open

  ! -----------------------------------------------------------------------------

  function NetCDF_close()

    integer NetCDF_close
    integer status

    call NetCDF_log_debug("Closing '" // trim(ncfilename) // "'")

    status = nf90_close(ncfileid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to close file '"  // trim(ncfilename) // "'")
       NetCDF_close = 1
       return
    end if

    NetCDF_close = 0

  end function NetCDF_close

  ! -----------------------------------------------------------------------------

  function NetCDF_set_group(name)

    integer NetCDF_set_group
    character*(*) name
    integer status
    integer ncnewgroupid
    integer i, j, n

    i = 1
    j = 1
    n = len(trim(name))

    do j = 1, n
       if (j == n) then
          status = nf90_inq_grp_ncid(ncgroupid, name(i:j), ncnewgroupid)
          if (status /= 0) then
             call NetCDF_log_error("Failed to inquire id for group '"  // trim(name(i:j)) // "'")
             NetCDF_set_group = 1
             return
          end if
          ncgroupid = ncnewgroupid
       else if (name(j:j) == "/") then
          if (i == j) then
             ncgroupid = ncfileid
          else
             status = nf90_inq_grp_ncid(ncgroupid, name(i:j-1), ncnewgroupid)
             if (status /= 0) then
                call NetCDF_log_error("Failed to inquire id for group '"  // trim(name(i:j-1)) // "'")
                NetCDF_set_group = 1
                return
             end if
             ncgroupid = ncnewgroupid
          end if
          i = j + 1
       end if
    end do

    NetCDF_set_group = 0

  end function NetCDF_set_group

  ! -----------------------------------------------------------------------------

  function NetCDF_get_dimension(name, value)

    integer NetCDF_get_dimension
    character*(*) name
    integer value
    integer status
    integer ncdimid

    status = nf90_inq_dimid(ncgroupid, name, ncdimid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for dimension '"  // name // "'")
       NetCDF_get_dimension = 1
       return
    end if

    status = nf90_inquire_dimension(ncgroupid, ncdimid, len=value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dimension '" // name // "'")
       NetCDF_get_dimension = 1
       return
    end if

    NetCDF_get_dimension = 0

  end function NetCDF_get_dimension

  ! -----------------------------------------------------------------------------

  function NetCDF_get_att_i(name, value)

    integer :: NetCDF_get_att_i
    character*(*) :: name
    integer :: value
    integer :: status

    status = nf90_get_att(ncgroupid, NF90_GLOBAL, name, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get attribute '"  // name // "'")
       NetCDF_get_att_i = 1
       return
    end if

    NetCDF_get_att_i = 0

  end function NetCDF_get_att_i

  ! -----------------------------------------------------------------------------

  function NetCDF_get_att_d(name, value)

    integer :: NetCDF_get_att_d
    character*(*) :: name
    real(8) :: value
    integer :: status

    status = nf90_get_att(ncgroupid, NF90_GLOBAL, name, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get attribute '"  // name // "'")
       NetCDF_get_att_d = 1
       return
    end if

    NetCDF_get_att_d = 0

end function NetCDF_get_att_d

  ! -----------------------------------------------------------------------------

  function NetCDF_get_att_str(name, value)

    integer NetCDF_get_att_str
    character*(*) :: name
    character*(*) :: value
    integer status

    status = nf90_get_att(ncgroupid, NF90_GLOBAL, name, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get attribute '"  // name // "'")
       NetCDF_get_att_str = 1
       return
    end if

    NetCDF_get_att_str = 0

  end function NetCDF_get_att_str

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_att_d(varname, attname, value)
    integer NetCDF_get_var_att_d
    character*(*) :: varname
    character*(*) :: attname
    integer       :: varid
    real(8) :: value
    integer status

    status = nf90_inq_varid(ncgroupid, varname, varid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '"  // varname // "'")
       NetCDF_get_var_att_d = 1
       return
    end if

    status = nf90_get_att(ncgroupid, varid, attname, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get attribute '"  // attname // "'")
       NetCDF_get_var_att_d = 1
       return
    end if

    NetCDF_get_var_att_d = 0
  end function NetCDF_get_var_att_d

  ! -----------------------------------------------------------------------------

  function NetCDF_put_att_str(name, value)

    integer NetCDF_put_att_str
    character*(*) :: name
    character*(*) :: value
    integer status

    status = nf90_put_att(ncgroupid, NF90_GLOBAL, name, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to put attribute '"  // name // "'")
       NetCDF_put_att_str = 1
       return
    end if

    NetCDF_put_att_str = 0

  end function NetCDF_put_att_str

  ! -----------------------------------------------------------------------------

  function NetCDF_put_var_f3(name, value)

    integer NetCDF_put_var_f3
    character*(*) name
    real(4), allocatable :: value(:,:,:)
    integer status
    integer ncvarid
    integer ndims
    integer, dimension(nf90_max_var_dims) :: dimids

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_put_var_f3 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_put_var_f3 = 1
       return
    end if

    if (ndims /= 3) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_put_var_f3 = 1
       return
    end if

    status = nf90_put_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to put variable '" // name // "'")
       NetCDF_put_var_f3 = 1
       return
    end if

    NetCDF_put_var_f3 = 0

  end function NetCDF_put_var_f3

  ! -----------------------------------------------------------------------------

  function NetCDF_put_var_f4(name, value)

    integer NetCDF_put_var_f4
    character*(*) name
    real(4), allocatable :: value(:,:,:,:)
    integer status
    integer ncvarid
    integer ndims
    integer, dimension(nf90_max_var_dims) :: dimids

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_put_var_f4 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_put_var_f4 = 1
       return
    end if

    if (ndims /= 4) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_put_var_f4 = 1
       return
    end if

    status = nf90_put_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to put variable '" // name // "'")
       NetCDF_put_var_f4 = 1
       return
    end if

    NetCDF_put_var_f4 = 0

  end function NetCDF_put_var_f4

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_p1(name, value)

    integer NetCDF_get_var_p1
    character*(*) name
    real, pointer :: value(:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_p1 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_p1 = 1
       return
    end if

    if (ndims /= 1) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_p1 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_p1 = 1
          return
       end if

    end do

    allocate(value(dims(1)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_p1 = 1
       return
    end if

    NetCDF_get_var_p1 = 0

  end function NetCDF_get_var_p1

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_d1(name, value)

    integer NetCDF_get_var_d1
    character*(*) name
    real(8), pointer :: value(:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_d1 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_d1 = 1
       return
    end if

    if (ndims /= 1) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_d1 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_d1 = 1
          return
       end if

    end do

    allocate(value(dims(1)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_d1 = 1
       return
    end if

    NetCDF_get_var_d1 = 0

  end function NetCDF_get_var_d1

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_d3(name, value)

    integer NetCDF_get_var_d3
    character*(*) name
    real(8), pointer :: value(:, :, :)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_d3 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_d3 = 1
       return
    end if

    if (ndims /= 3) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_d3 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_d3 = 1
          return
       end if

    end do

    allocate(value(dims(1), dims(2), dims(3)), stat=status)
    if (status /= 0) then
       call NetCDF_log_error("Memory allocation failure for '" // name // "'")
       NetCDF_get_var_d3 = 1
       return
    end if

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_d3 = 1
       return
    end if

    NetCDF_get_var_d3 = 0

  end function NetCDF_get_var_d3

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_d6(name, value)

    integer NetCDF_get_var_d6
    character*(*) name
    real, allocatable :: value(:,:,:,:,:,:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       goto 999
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       goto 999
    end if

    if (ndims /= 6) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       status = 1
       goto 999
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          goto 999
       end if

    end do

    allocate(value(dims(1), dims(2), dims(3), dims(4), dims(5), dims(6)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       goto 999
    end if

999 continue

    NetCDF_get_var_d6 = status

  end function NetCDF_get_var_d6

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_f1(name, value)

    integer NetCDF_get_var_f1
    character*(*) name
    real(4), pointer :: value(:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_f1 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_f1 = 1
       return
    end if

    if (ndims /= 1) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_f1 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_f1 = 1
          return
       end if

    end do

    allocate(value(dims(1)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_f1 = 1
       return
    end if

    NetCDF_get_var_f1 = 0

  end function NetCDF_get_var_f1

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_f2(name, value)

    integer NetCDF_get_var_f2
    character*(*) name
    real, allocatable :: value(:,:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       goto 999
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       goto 999
    end if

    if (ndims /= 2) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       status = 1
       goto 999
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          goto 999
       end if

    end do

    allocate(value(dims(1), dims(2)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       goto 999
    end if

999 continue

    NetCDF_get_var_f2 = status

  end function NetCDF_get_var_f2

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_f3(name, value)

    integer NetCDF_get_var_f3
    character*(*) name
    real(4), pointer :: value(:,:,:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_f3 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_f3 = 1
       return
    end if

    if (ndims /= 3) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_f3 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_f3 = 1
          return
       end if

    end do

    allocate(value(dims(1), dims(2), dims(3)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_f3 = 1
       return
    end if

    NetCDF_get_var_f3 = 0

  end function NetCDF_get_var_f3

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_f4(name, value)

    integer NetCDF_get_var_f4
    character*(*) name
    real(4), allocatable :: value(:,:,:,:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_f4 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_f4 = 1
       return
    end if

    if (ndims /= 4) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_f4 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_f4 = 1
          return
       end if

    end do

    allocate(value(dims(1), dims(2), dims(3), dims(4)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_f4 = 1
       return
    end if

    NetCDF_get_var_f4 = 0

  end function NetCDF_get_var_f4

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_p7(name, value)

    integer NetCDF_get_var_p7
    character*(*) name
    real(4), pointer :: value(:,:,:,:,:,:,:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_p7 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_p7 = 1
       return
    end if

    if (ndims /= 7) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_p7 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_p7 = 1
          return
       end if

    end do

    allocate(value(dims(1), dims(2), dims(3), dims(4), dims(5), dims(6), dims(7)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_p7 = 1
       return
    end if

    NetCDF_get_var_p7 = 0

  end function NetCDF_get_var_p7

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_i1(name, value)

    integer NetCDF_get_var_i1
    character*(*) name
    integer, pointer :: value(:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_i1 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_i1 = 1
       return
    end if

    if (ndims /= 1) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_i1 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_i1 = 1
          return
       end if

    end do

    allocate(value(dims(1)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_i1 = 1
       return
    end if

    NetCDF_get_var_i1 = 0

  end function NetCDF_get_var_i1

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_i2(name, value)

    integer NetCDF_get_var_i2
    character*(*) name
    integer, allocatable :: value(:,:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_i2 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_i2 = 1
       return
    end if

    if (ndims /= 2) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_i2 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_i2 = 1
          return
       end if

    end do

    allocate(value(dims(1), dims(2)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_i2 = 1
       return
    end if

    NetCDF_get_var_i2 = 0

  end function NetCDF_get_var_i2

  ! -----------------------------------------------------------------------------

  function NetCDF_get_var_i3(name, value)

    integer NetCDF_get_var_i3
    character*(*) name
    integer, allocatable :: value(:,:,:)
    integer status
    integer ncvarid
    integer ndims, idim
    integer, dimension(nf90_max_var_dims) :: dimids
    integer, dimension(nf90_max_var_dims) :: dims

    status = nf90_inq_varid(ncgroupid, name, ncvarid)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire id for variable '"  // name // "'")
       NetCDF_get_var_i3 = 1
       return
    end if

    status = nf90_inquire_variable(ncgroupid, ncvarid, dimids = dimids, ndims = ndims)
    if (status /= 0) then
       call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
       NetCDF_get_var_i3 = 1
       return
    end if

    if (ndims /= 3) then
       call NetCDF_log_error("Number of dimensions not supported for variable '" // name // "'")
       NetCDF_get_var_i3 = 1
       return
    end if

    do idim = 1, ndims

       status = nf90_inquire_dimension(ncgroupid, dimids(idim), len = dims(idim))
       if (status /= 0) then
          call NetCDF_log_error("Failed to inquire dims for variable '"  // name // "'")
          NetCDF_get_var_i3 = 1
          return
       end if

    end do

    allocate(value(dims(1), dims(2), dims(3)))

    status = nf90_get_var(ncgroupid, ncvarid, value)
    if (status /= 0) then
       call NetCDF_log_error("Failed to get variable '" // name // "'")
       NetCDF_get_var_i3 = 1
       return
    end if

    NetCDF_get_var_i3 = 0

  end function NetCDF_get_var_i3

  ! -----------------------------------------------------------------------------

  subroutine NetCDF_log_info(text)

    character*(*) :: text

#ifdef TROPNLL2DP
    call fortranlogknmi(text, len(text), logInfo)
#else
    write(*,*) trim(text)
#endif

  end subroutine NetCDF_log_info

  ! -----------------------------------------------------------------------------

  subroutine NetCDF_log_debug(text)

    character*(*) :: text

#ifdef TROPNLL2DP
    call fortranlogknmi(text, len(text), logDebug)
#else
    write(*,*) trim(text)
#endif

  end subroutine NetCDF_log_debug

  ! -----------------------------------------------------------------------------

  subroutine NetCDF_log_warning(text)

    character*(*) :: text

#ifdef TROPNLL2DP
    call fortranlogknmi(text, len(text), logWarning)
#else
    write(*,*) trim(text)
#endif

  end subroutine NetCDF_log_Warning

  ! -----------------------------------------------------------------------------

  subroutine NetCDF_log_error(text)

    character*(*) :: text

#ifdef TROPNLL2DP
    call fortranlogknmi(text, len(text), logWarning)
#else
    write(*,*) trim(text)
#endif

  end subroutine NetCDF_log_error

  ! -----------------------------------------------------------------------------

end module NetCDFmodule
