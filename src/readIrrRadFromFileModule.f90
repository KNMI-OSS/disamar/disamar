!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

  module readIrrRadFromFileModule

    ! This module contains several subroutines:
    ! - read irradiance and radiance data and and store them in the dta structures
    !   wavelInstrIrrSimS and wavelInstrRadSimS no futher processing takes place
    ! - bring solar irradiance to earth radiance wavelength grid assuming that the
    !   wavelengths read from file are correct. Here the high resolution solar irradiance
    !   is used.
    ! - calibrate the wavelength grid for the solar irradiance using the high resolution solar
    !   irradiance grid
    ! - shift solar irradiance due to Doppler shift
    ! - update the wavelength grid for the radiance given fitted shift parameters for the radiance
    ! -   

  use dataStructures
  use staticDataModule
  use DISAMAR_log
  use DISAMAR_file
  use inputModule
  use readModule,               only: getHRSolarIrradiance, getHRSolarIrradianceStatic
  use mathTools,                only: spline, splint, splintLin, gaussDivPoints, GaussDivPoints, polyInt
  use radianceIrradianceModule, only: integrateSlitFunctionIrr

  ! default is private
  private 
  public  :: readIrrRadFromFile, readIrrRadPostProcess, setMRWavelengthGrid

  contains

      subroutine readIrrRadFromFile(errS, globalS, numSpectrBands, controlSimS,               &
                                    wavelInstrIrrSimS, wavelHRSimS, wavelMRIrrSimS,           &
                                    wavelInstrRadSimS, wavelMRRadSimS,                        &
                                    solarIrradianceSimS, earthRadianceSimS,                   &
                                    wavelInstrIrrRetrS, wavelHRRetrS, wavelInstrRadRetrS,     &
                                    solarIrradianceRetrS, earthRadianceRetrS)

    ! Read ASCII file with irradiance and radiance data
    ! values read are wavelength (nm), SNR, and irradiance or radiance.
    ! Header lines starting with '# ' in the first and second column are skipped.
    ! Example:
    ! # Header 1
    ! # Header 2
    ! start_fit_window_irr
    ! irr  460.1500     3000.88   3.402296E+14
    ! irr  460.3000     2901.97   3.202296E+14
    ! end_fit_window_irr
    ! start_fit_window_rad
    ! rad  460.1500     1485.88   1.116153E+13
    ! rad  460.3000     1445.97   1.116153E+13
    ! end_fit_window_rad
    ! start fit window_irr
    ! irr  560.1500     3285.88   3.402296E+14
    ! irr  560.3000     3245.97   3.472296E+14
    ! end_fit_window_irr
    ! start fit window_rad
    ! rad  560.1500     1285.88   2.402296E+13
    ! rad  560.3000     1245.97   2.472296E+13
    ! end_fit_window_rad
    ! end_file
    !
    ! An older version of the file where the sun-normalized radiance
    ! is read can still be used.
    !
    ! NOTE: the module inputModule is used (parser)
    ! Remarks: The wavelength intervals are called fit windows because they are fit windows
    !          when the file read is the result of a simulation. However, they are (parts of) 
    !          spectral channels when the file read is a measured spectrum. Note that the spectral
    !          channels and the fit windows can (partly) overlap. It is demanded that
    !          each fit window specified in the configuration file falls within a spectral
    !          channel that is read here. That means that in the configuration file
    !          two fit windows have to be specified if the original window contains a channel
    !          boundary. This is done because in general the FWHM of the slit function will
    !          differ for different channels.
    

      implicit none

      type(errorType), intent(inout) :: errS
      type(globalType),        intent(inout) :: globalS
      integer,                 intent(in)    :: numSpectrBands                       ! number of spectral bands / fit windows
      type(controlType),       intent(in)    :: controlSimS                          ! control parameters (simulation)
      type(wavelInstrType),    intent(inout) :: wavelInstrIrrSimS(numSpectrBands)    ! wavelength grid for measured irradiance
      type(wavelHRType),       intent(inout) :: wavelHRSimS(numSpectrBands)          ! high resolution wavel grid (ir)radiance
      type(wavelHRType),       intent(inout) :: wavelMRIrrSimS(numSpectrBands)       ! high resolution wavel grid (ir)radiance
      type(wavelInstrType),    intent(inout) :: wavelInstrRadSimS(numSpectrBands)    ! wavelength grid for measured radiance
      type(wavelHRType),       intent(inout) :: wavelMRRadSimS(numSpectrBands)       ! wavelength grid for measured radiance
      type(solarIrrType),      intent(inout) :: solarIrradianceSimS(numSpectrBands)  ! solar irr on radiance wavelength grid
      type(earthRadianceType), intent(inout) :: earthRadianceSimS(numSpectrBands)    ! earth radiance read from file
      type(wavelInstrType),    intent(inout) :: wavelInstrIrrRetrS(numSpectrBands)   ! wavelength grid for measured irradiance
      type(wavelHRType),       intent(inout) :: wavelHRRetrS(numSpectrBands)         ! high resolution wavel grid (ir)radiance
      type(wavelInstrType),    intent(inout) :: wavelInstrRadRetrS(numSpectrBands)   ! wavelength grid for measured radiance
      type(solarIrrType),      intent(inout) :: solarIrradianceRetrS(numSpectrBands) ! solar irr on radiance wavelength grid
      type(earthRadianceType), intent(inout) :: earthRadianceRetrS(numSpectrBands)   ! earth radiance read from file

      ! local
      integer, parameter :: maxNumWavelengths = 4500
      integer, parameter :: maxNumChannels = 10

      integer            :: OpenError
      integer            :: iwave, iwave_irr, iwave_rad
      integer            :: ichannel, ichannel_irr, ichannel_rad 
      integer            :: nchannel, nchannel_irr, nchannel_rad
      integer            :: nwavel(maxNumChannels), nwavel_irr(maxNumChannels), nwavel_rad(maxNumChannels)

      real(8)            :: wavelength    (maxNumWavelengths, maxNumChannels)
      real(8)            :: wavelength_irr(maxNumWavelengths, maxNumChannels)
      real(8)            :: wavelength_rad(maxNumWavelengths, maxNumChannels)
      real(8)            :: SNR           (maxNumWavelengths, maxNumChannels)
      real(8)            :: SNR_irr       (maxNumWavelengths, maxNumChannels)
      real(8)            :: SNR_rad       (maxNumWavelengths, maxNumChannels)
      real(8)            :: reflectance   (maxNumWavelengths, maxNumChannels)
      real(8)            :: irradiance    (maxNumWavelengths, maxNumChannels)
      real(8)            :: radiance      (maxNumWavelengths, maxNumChannels)

      logical            :: old_version

      logical            :: eof
      logical            :: wavelength_grids_differ

      type(solarIrrType) :: solarIrradianceReadS(numSpectrBands) ! solar irr on radiance wavelength grid
                                                                 ! only used for high sampling interpolation

      character(LEN = 40)   :: identifier
      real(8)               :: value       ! dummy value
      type(file_type)       :: file

      call enter('readIrrRadFromFile')

      OpenError = load_file(controlSimS%ASCIIinputIrrRadFileName, file)
      if (OpenError /= 0) then
        call logDebug('ERROR in subroutine readIrrRadFromFile: failed to open file')
        call logDebug('in module readModule')
        call logDebug('controlS%ASCIIinputIrrRadFileName: '// controlSimS%ASCIIinputIrrRadFileName)
         call mystop(errS, 'stopped because file could not be opened')
         if (errorCheck(errS)) goto 99999
      end if

      ! initialize
      wavelength_grids_differ = .false.
      old_version = .false.

      wavelength    (:,:)  = 0.0d0
      wavelength_irr(:,:)  = 0.0d0
      wavelength_rad(:,:)  = 0.0d0
      SNR(:,:)             = 0.0d0
      SNR_irr(:,:)         = 0.0d0
      SNR_rad(:,:)         = 0.0d0
      reflectance(:,:)     = 0.0d0
      irradiance(:,:)      = 0.0d0
      radiance(:,:)        = 0.0d0
      nwavel(:)            = 0
      nwavel_irr(:)        = 0
      nwavel_rad(:)        = 0

      ! set options for the parser
!      call input_options(errS, file, echo_lines=.false.,skip_blank_lines=.true.,error_flag=1, &
!                         concat_string='&')
!      if (errorCheck(errS)) goto 99999

      ! Read data from file and store it in the arrays wavelength, SNR and reflectance

      ! initialize
      ichannel     = 1
      ichannel_irr = 1
      ichannel_rad = 1
      nchannel     = 1
      nchannel_irr = 1
      nchannel_rad = 1

      do
        call readline(errS, eof, file)
        if (errorCheck(errS)) goto 99999
        if (eof) exit

        call reada(errS, file, identifier)
        if (errorCheck(errS)) goto 99999

        select case (identifier)

           case('start_fit_window')

             old_version = .true.
             iwave = 1

           case('start_channel_irr')

             old_version = .false.
             iwave_irr = 1

           case('start_channel_rad')

             old_version = .false.
             iwave_rad = 1

           case('end_fit_window')

             old_version = .true.
             nchannel = ichannel
             ichannel = ichannel + 1

             if ( ichannel > maxNumChannels + 1) then
               call logDebug('ERROR subroutine readIrrRadFromFile: too many channels specified')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebugI('maximum number = ', maxNumChannels)
               call logDebug('increase maxNumChannels')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readIrrRadFromFileModule')
               call mystop(errS, 'stopped because too many channels are specified')
               if (errorCheck(errS)) goto 99999
             end if

           case('end_channel_irr')

             old_version = .false.
             nchannel_irr = ichannel_irr
             ichannel_irr = ichannel_irr + 1

             if ( ichannel_irr > maxNumChannels + 1) then
               call logDebug('ERROR subroutine readIrrRadFromFile: too many channels specified')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebugI('maximum number = ', maxNumChannels)
               call logDebug('increase maxNumChannels')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readIrrRadFromFileModule')
               call mystop(errS, 'stopped because too many channels are specified')
               if (errorCheck(errS)) goto 99999
             end if

           case('end_channel_rad')

             old_version = .false.
             nchannel_rad = ichannel_rad
             ichannel_rad = ichannel_rad + 1

             if ( ichannel_rad > maxNumChannels + 1) then
               call logDebug('ERROR subroutine readIrrRadFromFile: too many channels specified')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebugI('maximum number = ', maxNumChannels)
               call logDebug('increase maxNumChannels')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readIrrRadFromFileModule')
               call mystop(errS, 'stopped because too many channels are specified')
               if (errorCheck(errS)) goto 99999
             end if

           case('data')

             old_version = .true.

             if ( file%parser%nitems /= 4 ) then
               call logDebug('ERROR in external irradiance - radiance file: incorect number of items on a line')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               write(errS%temp,*) 'for channel and wavelength numbers', ichannel, iwave
               call errorAddLine(errS, errS%temp)
               call logDebug('detected by subroutine readIrrRadFromFile')
               call logDebug('in module readIrrRadFromFileModule')
               call mystop(errS, 'stopped because of error in external reflectance file')
               if (errorCheck(errS)) goto 99999
             end if

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             wavelength(iwave, ichannel)  = value

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             SNR(iwave, ichannel)         = value

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             reflectance(iwave, ichannel) = value

             nwavel(ichannel) = iwave
             iwave            = iwave + 1

             if ( iwave > maxNumWavelengths ) then
               call logDebug('ERROR in reading reflectance data from file')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebug('too many wavelengths specified')
               call logDebugI('maximum number = ', maxNumWavelengths)
               call logDebug('increase maxNumWavelengths')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readIrrRadFromFileModule')
               call mystop(errS, 'stopped because too many wavelengths are specified')
               if (errorCheck(errS)) goto 99999
             end if

           case('irr')

             old_version = .false.

             if ( file%parser%nitems /= 4 ) then
               call logDebug('ERROR in external irradiance - radiance file: incorect number of items on a line')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               write(errS%temp,*) 'for channel and wavelength numbers', ichannel_irr, iwave_irr
               call errorAddLine(errS, errS%temp)
               call logDebug('detected by subroutine readIrrRadFromFile')
               call logDebug('in module readIrrRadFromFileModule')
               call mystop(errS, 'stopped because of error in external irradiance - radiance file')
               if (errorCheck(errS)) goto 99999
             end if

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             wavelength_irr(iwave_irr, ichannel_irr)  = value

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             SNR_irr(iwave_irr, ichannel_irr)         = value

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             irradiance(iwave_irr, ichannel_irr)      = value

             nwavel_irr(ichannel_irr) = iwave_irr
             iwave_irr                = iwave_irr + 1

             if ( iwave_irr > maxNumWavelengths ) then
               call logDebug('ERROR in reading radiance - irradiance data from file')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebug('too many wavelengths specified')
               call logDebugI('maximum number = ', maxNumWavelengths)
               call logDebug('increase maxNumWavelengths')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readIrrRadFromFileModule')
               call mystop(errS, 'stopped because too many wavelengths are specified')
               if (errorCheck(errS)) goto 99999
             end if

           case('rad')

             old_version = .false.

             if ( file%parser%nitems /= 4 ) then
               call logDebug('ERROR in external irradiance - radiance file: incorect number of items on a line')
               call logDebug('when reading radiance data')
               write(errS%temp,*) 'for channel and wavelength numbers', nchannel_rad, iwave_rad
               call errorAddLine(errS, errS%temp)
               call logDebug('detected by subroutine readIrrRadFromFile')
               call logDebug('in module readIrrRadFromFileModule')
               call mystop(errS, 'stopped because of error in reflectance file')
               if (errorCheck(errS)) goto 99999
             end if

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             wavelength_rad(iwave_rad, ichannel_rad)  = value

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             SNR_rad(iwave_rad, ichannel_rad)         = value

             call readf(errS, file, value)
             if (errorCheck(errS)) return
             radiance(iwave_rad, ichannel_rad)        = value

             nwavel_rad(ichannel_rad) = iwave_rad
             iwave_rad                = iwave_rad + 1

             if ( iwave_rad > maxNumWavelengths ) then
               call logDebug('ERROR in reading radiance - irradiance data from file')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebug('too many wavelengths specified')
               call logDebugI('maximum number = ', maxNumWavelengths)
               call logDebug('increase maxNumWavelengths')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readIrrRadFromFileModule')
               call mystop(errS, 'stopped because too many wavelengths are specified')
               if (errorCheck(errS)) goto 99999
             end if
              
           case('end_file')

             exit ! exit do loop

           case default

             call logDebug("ERROR in file with irradiance - radiance data")
             call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
             call logDebug("identifier is not recognized: "//trim(identifier))
             call logDebug("possible identifiers are:")
             call logDebug('start_fit_window  (old version only)')
             call logDebug('data              (old version only)')
             call logDebug('end_fit_window    (old version only)')
             call logDebug('start_channel_irr' )
             call logDebug('irr')
             call logDebug('end_channel_irr')
             call logDebug('start_fit_window_rad')
             call logDebug('rad')
             call logDebug('end_channel_rad')
             call logDebug('end_file')
             call mystop(errS, 'stopped because identifier is not recognized')
             if (errorCheck(errS)) goto 99999

        end select

      end do

      call readIrrRadPostProcess(errS, globalS, numSpectrBands, controlSimS, old_version,  &
                                 wavelInstrIrrSimS, wavelHRSimS, wavelMRIrrSimS,           &
                                 wavelInstrRadSimS, wavelMRRadSimS,                        &
                                 solarIrradianceSimS, earthRadianceSimS,                   &
                                 wavelInstrIrrRetrS, wavelHRRetrS, wavelInstrRadRetrS,     &
                                 solarIrradianceRetrS, earthRadianceRetrS,                 &
                                 maxNumWavelengths, maxNumChannels,                        &
                                 nchannel, wavelength, SNR, reflectance, nwavel,           &
                                 nchannel_irr, wavelength_irr, SNR_irr, irradiance, nwavel_irr, &
                                 nchannel_rad, wavelength_rad, SNR_rad, radiance, nwavel_rad)
      if (errorCheck(errS)) goto 99999

99999 continue
      call exit('readIrrRadFromFile')

    end subroutine readIrrRadFromFile



    subroutine readIrrRadPostProcess(errS, globalS, numSpectrBands, controlSimS, old_version,  &
                                     wavelInstrIrrSimS, wavelHRSimS, wavelMRIrrSimS,           &
                                     wavelInstrRadSimS, wavelMRRadSimS,                        &
                                     solarIrradianceSimS, earthRadianceSimS,                   &
                                     wavelInstrIrrRetrS, wavelHRRetrS, wavelInstrRadRetrS,     &
                                     solarIrradianceRetrS, earthRadianceRetrS,                 &
                                     maxNumWavelengths, maxNumChannels,                        &
                                     nchannel, wavelength, SNR, reflectance, nwavel,           &
                                     nchannel_irr, wavelength_irr, SNR_irr, irradiance, nwavel_irr, &
                                     nchannel_rad, wavelength_rad, SNR_rad, radiance, nwavel_rad)

      implicit none

      type(errorType),         intent(inout) :: errS
      type(globalType),        intent(inout) :: globalS
      integer,                 intent(in)    :: numSpectrBands                       ! number of spectral bands / fit windows
      type(controlType),       intent(in)    :: controlSimS                          ! control parameters (simulation)
      logical,                 intent(in)    :: old_version
      type(wavelInstrType),    intent(inout) :: wavelInstrIrrSimS(numSpectrBands)    ! wavelength grid for measured irradiance
      type(wavelHRType),       intent(inout) :: wavelHRSimS(numSpectrBands)          ! high resolution wavel grid (ir)radiance
      type(wavelHRType),       intent(inout) :: wavelMRIrrSimS(numSpectrBands)       ! high resolution wavel grid (ir)radiance
      type(wavelInstrType),    intent(inout) :: wavelInstrRadSimS(numSpectrBands)    ! wavelength grid for measured radiance
      type(wavelHRType),       intent(inout) :: wavelMRRadSimS(numSpectrBands)       ! wavelength grid for measured radiance
      type(solarIrrType),      intent(inout) :: solarIrradianceSimS(numSpectrBands)  ! solar irr on radiance wavelength grid
      type(earthRadianceType), intent(inout) :: earthRadianceSimS(numSpectrBands)    ! earth radiance read from file
      type(wavelInstrType),    intent(inout) :: wavelInstrIrrRetrS(numSpectrBands)   ! wavelength grid for measured irradiance
      type(wavelHRType),       intent(inout) :: wavelHRRetrS(numSpectrBands)         ! high resolution wavel grid (ir)radiance
      type(wavelInstrType),    intent(inout) :: wavelInstrRadRetrS(numSpectrBands)   ! wavelength grid for measured radiance
      type(solarIrrType),      intent(inout) :: solarIrradianceRetrS(numSpectrBands) ! solar irr on radiance wavelength grid
      type(earthRadianceType), intent(inout) :: earthRadianceRetrS(numSpectrBands)   ! earth radiance read from file
      integer,                 intent(in)    :: maxNumWavelengths
      integer,                 intent(in)    :: maxNumChannels
      integer,                 intent(in)    :: nchannel
      real(8),                 intent(inout) :: wavelength    (maxNumWavelengths, maxNumChannels)
      real(8),                 intent(in)    :: SNR           (maxNumWavelengths, maxNumChannels)
      real(8),                 intent(in)    :: reflectance   (maxNumWavelengths, maxNumChannels)
      integer,                 intent(in)    :: nwavel(maxNumChannels)
      integer,                 intent(in)    :: nchannel_irr
      real(8),                 intent(inout) :: wavelength_irr(maxNumWavelengths, maxNumChannels)
      real(8),                 intent(in)    :: SNR_irr       (maxNumWavelengths, maxNumChannels)
      real(8),                 intent(in)    :: irradiance    (maxNumWavelengths, maxNumChannels)
      integer,                 intent(in)    :: nwavel_irr(maxNumChannels)
      integer,                 intent(in)    :: nchannel_rad
      real(8),                 intent(inout) :: wavelength_rad(maxNumWavelengths, maxNumChannels)
      real(8),                 intent(in)    :: SNR_rad       (maxNumWavelengths, maxNumChannels)
      real(8),                 intent(in)    :: radiance      (maxNumWavelengths, maxNumChannels)
      integer,                 intent(in)    :: nwavel_rad(maxNumChannels)

      ! local

      integer            :: iband, iwave, iwave_irr, iwave_rad, iTrace, index_irr, index_rad
      integer            :: ichannel, ichannel_irr, ichannel_rad 
      integer            :: numChannelsForFitWindow, numChannelsForFitWindow_irr, numChannelsForFitWindow_rad

      integer            :: indexArray    (maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_irr(maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_rad(maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_irr_mod(maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_rad_mod(maxNumWavelengths, maxNumChannels)

      integer            :: counter, ipair, i_column
      real(8)            :: wavel, FWHM
      logical            :: addWavel

      integer            :: channelNumber    (numSpectrBands)  ! channel number for each fit window
      integer            :: channelNumber_irr(numSpectrBands)  ! channel number for each fit window
      integer            :: channelNumber_rad(numSpectrBands)  ! channel number for each fit window

      logical            :: wavelength_grids_differ

      logical, parameter    :: verbose = .false.

      type(solarIrrType) :: solarIrradianceReadS(numSpectrBands) ! solar irr on radiance wavelength grid
                                                                 ! only used for high sampling interpolation

      call enter('readIrrRadPostProcess')

      if ( old_version ) then

        ! code for the old version of the external data file
        ! to make the code backward compatable

        ! Select the channel for each fit window for the Earth radiance

        do iband = 1, numSpectrBands
          numChannelsForFitWindow = 0
          do ichannel = 1, nchannel
            if ( ( wavelInstrIrrSimS(iband)%startWavel >= wavelength(                1, ichannel) - 0.01d0) .and. &
                 ( wavelInstrIrrSimS(iband)%endWavel   <= wavelength( nwavel(ichannel), ichannel) + 0.01d0 ) ) then
              channelNumber(iband) = ichannel
              numChannelsForFitWindow = numChannelsForFitWindow + 1
            end if
          end do ! ichannel
          if ( numChannelsForFitWindow > 1 ) then
            call logDebug("ERROR in configuration file")
            call logDebug("spectral band / fit window in overlap region of spectral channels")
            call logDebug("can not select which channel to use")
            call logDebug("change fit window in configuration file")
            call logDebug("so that spectral channel can be selected")
            call logDebugI("error ocurred for fit window = ", iband)
            call mystop(errS, 'stopped because spectral channel can not be selected')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          if ( numChannelsForFitWindow < 1 ) then
            call logDebug("ERROR in configuration file")
            call logDebug("fit window lies (partly) outside the wavelength range covered by the channels")
            call logDebug("either make the fit window smaller or split it into two or more fit windows")
            call logDebugI("error ocurred for fit window = ", iband)
            call mystop(errS, 'stopped because fit window lies (partly) outside the channels')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          
        end do ! iband

        ! determine the number of wavelengths in each fit window, taking into account excluded parts

        do iband = 1, numSpectrBands
          counter = 0
          do iwave = 1, nwavel(channelNumber(iband))
            wavel = wavelength(iwave, channelNumber(iband))
            addWavel = .false.
            if ( (wavel >= wavelInstrIrrSimS(iband)%startWavel - 0.01d0) .and. &
                 (wavel <= wavelInstrIrrSimS(iband)%endWavel   + 0.01d0) ) addWavel = .true.
            do ipair = 1, wavelInstrIrrSimS(iband)%nExclude
              if ( (wavel >= wavelInstrIrrSimS(iband)%excludeStart(ipair)) .and. &
                   (wavel <= wavelInstrIrrSimS(iband)%excludeEnd(ipair)) )       &
              addWavel = .false.
            end do ! ipair
            if ( addWavel ) counter = counter + 1
          end do ! iwave
          call freeMemWavelInstrS(errS, wavelInstrIrrSimS (iband))    ! free memory for irradiance
          call freeMemWavelInstrS(errS, wavelInstrRadSimS(iband))     ! free memory for radiance
          call freeMemWavelInstrS(errS, wavelInstrIrrRetrS (iband))   ! free memory for irradiance
          call freeMemWavelInstrS(errS, wavelInstrRadRetrS(iband))    ! free memory for radiance
          wavelInstrIrrSimS(iband)%nwavel  = counter
          wavelInstrRadSimS(iband)%nwavel  = counter
          wavelInstrIrrRetrS(iband)%nwavel = counter
          wavelInstrRadRetrS(iband)%nwavel = counter
          call claimMemWavelInstrS(errS, wavelInstrIrrSimS (iband))    ! claim memory for irradiance
          call claimMemWavelInstrS(errS, wavelInstrRadSimS(iband))     ! claim memory for radiance
          call claimMemWavelInstrS(errS, wavelInstrIrrRetrS (iband))   ! claim memory for irradiance
          call claimMemWavelInstrS(errS, wavelInstrRadRetrS(iband))    ! claim memory for radiance
        end do ! iband

        ! fill wavelengths and indexArray

        do iband = 1, numSpectrBands
          counter = 0
          do iwave = 1, nwavel(channelNumber(iband))
            addWavel = .false.
            wavel = wavelength(iwave, channelNumber(iband))
            if ( (wavel >= wavelInstrIrrSimS(iband)%startWavel - 0.01d0) .and. &
                 (wavel <= wavelInstrIrrSimS(iband)%endWavel   + 0.01d0) )   addWavel = .true.
            do ipair = 1, wavelInstrIrrSimS(iband)%nExclude
              if ( (wavel >= wavelInstrIrrSimS(iband)%excludeStart(ipair) ) .and. &
                   (wavel <= wavelInstrIrrSimS(iband)%excludeEnd  (ipair) ) ) addWavel = .false.
            end do ! ipair
            if ( addWavel ) then 
              counter = counter + 1
              wavelInstrIrrSimS(iband)%wavel(counter) = wavel
              wavelInstrRadSimS(iband)%wavel(counter) = wavel
              wavelInstrIrrRetrS(iband)%wavel(counter) = wavel
              wavelInstrRadRetrS(iband)%wavel(counter) = wavel
              indexArray(counter,iband) = iwave
            end if
          end do ! iwave
          if ( counter < 2 ) then
            call logDebug('ERROR when reading reflectance data from file')
            call logDebug('values for at least 2 wavelengths per spectral band are expected')
            call logDebug('but results for less wavelengths were read')
            call logDebugI('for band number = ', iband)
            call logDebug('error produced by subroutine readReflFromFile')
            call logDebug('in module readIrrRadFromFileModule')
            call mystop(errS, 'stopped because not enough data was read')
            if (errorCheck(errS)) return
          end if
        end do ! iband

        call set_nwavelInstr(errS, globalS)
        if (errorCheck(errS)) return
        call set_nwavelMR(errS, globalS)
        if (errorCheck(errS)) return
        call freeMemRetrS_wavel_only_not_Se  (errS,  globalS%retrS )
        if (errorCheck(errS)) return
        call claimMemRetrS_wavel_only_not_Se (errS,  globalS%retrS )
        if (errorCheck(errS)) return
        call freeMemDiagnosticS_wavel_only   (errS,  globalS%diagnosticS )
        if (errorCheck(errS)) return
        call claimMemDiagnosticS_wavel_only  (errS,  globalS%diagnosticS )
        if (errorCheck(errS)) return

        do iband =  1, globalS%numSpectrBands
          do iTrace = 1, globalS%nTrace
            call freeMemXsecS(errS, globalS%XsecHRSimS(iband,iTrace))
            if (errorCheck(errS)) return
            call freeMemXsecLUTS(errS, globalS%XsecHRLUTSimS(iband,iTrace))
            if (errorCheck(errS)) return
            call freeMemXsecS(errS, globalS%XsecHRRetrS(iband,iTrace))
            if (errorCheck(errS)) return
            call freeMemXsecLUTS(errS, globalS%XsecHRLUTRetrS(iband,iTrace))
            if (errorCheck(errS)) return
            call claimMemXsecS(errS, globalS%XsecHRSimS(iband,iTrace))
            if (errorCheck(errS)) return
            call claimMemXsecLUTS(errS, globalS%XsecHRLUTSimS(iband,iTrace))
            if (errorCheck(errS)) return
            call claimMemXsecS(errS, globalS%XsecHRRetrS(iband,iTrace))
            if (errorCheck(errS)) return
            call claimMemXsecLUTS(errS, globalS%XsecHRLUTRetrS(iband,iTrace))
            if (errorCheck(errS)) return
          end do ! iTrace
        end do ! iband

        do iband = 1, globalS%numSpectrBands
          call freeMemSolarIrradianceS (errS,  globalS%solarIrradianceSimS(iband) )
          if (errorCheck(errS)) return
          call claimMemSolarIrradianceS(errS,  globalS%solarIrradianceSimS(iband) )
          if (errorCheck(errS)) return
          call freeMemSolarIrradianceS (errS,  globalS%solarIrradianceRetrS(iband) )
          if (errorCheck(errS)) return
          call claimMemSolarIrradianceS(errS,  globalS%solarIrradianceRetrS(iband) )
          if (errorCheck(errS)) return
          call freeMemEarthRadianceS   (errS,  globalS%earthRadianceSimS(iband) )
          if (errorCheck(errS)) return
          call claimMemEarthRadianceS  (errS,  globalS%earthRadianceSimS(iband) )
          if (errorCheck(errS)) return
          call freeMemEarthRadianceS   (errS,  globalS%earthRadianceRetrS(iband) )
          if (errorCheck(errS)) return
          call claimMemEarthRadianceS  (errS,  globalS%earthRadianceRetrS(iband) )
          if (errorCheck(errS)) return
          call freeMemWeakAbsS         (errS,  globalS%weakAbsSimS(iband) )
          if (errorCheck(errS)) return
          call claimMemWeakAbsS        (errS,  globalS%weakAbsSimS(iband) )
          if (errorCheck(errS)) return
          call freeMemWeakAbsS         (errS,  globalS%weakAbsRetrS(iband) )
          if (errorCheck(errS)) return
          call claimMemWeakAbsS        (errS,  globalS%weakAbsRetrS(iband) )
          if (errorCheck(errS)) return
          call freeMemRRS_RingS        (errS,  globalS%RRS_RingSimS(iband) )
          if (errorCheck(errS)) return
          call claimMemRRS_RingS       (errS,  globalS%RRS_RingSimS(iband) )
          if (errorCheck(errS)) return
          call freeMemRRS_RingS        (errS,  globalS%RRS_RingRetrS(iband) )
          if (errorCheck(errS)) return
          call claimMemRRS_RingS       (errS,  globalS%RRS_RingRetrS(iband) )
          if (errorCheck(errS)) return
        end do

        ! Fill values for irradiance and radiance and signal to noise ratio
        ! note that we have the old version of the .sim (input) file here
        ! where the reflectance is listed and not the irradiance and radiance separately.
        ! Therefore we set the solar irradiance to 1.0 and fill the radiance with the
        ! reflectance.

        do iband = 1, numSpectrBands
          do iwave = 1, wavelInstrRadSimS(iband)%nwavel
            solarIrradianceSimS(iband)%solIrrMeas(iwave)    = 1.0d0
            solarIrradianceSimS(iband)%SN(iwave)            = 1.0d8
            solarIrradianceSimS(iband)%solIrrError(iwave)   = 1.0d-8
            earthRadianceSimS(iband)%rad_meas(1,iwave)      = reflectance(indexArray(iwave,iband), channelNumber(iband))
            earthRadianceSimS(iband)%SN(iwave)              = SNR(indexArray(iwave,iband), channelNumber(iband))
            earthRadianceSimS(iband)%SNrefspec(iwave)       = 0.0d0  ! value is not known
            earthRadianceSimS(iband)%rad_error(iwave)       = earthRadianceSimS(iband)%rad_meas(1,iwave) &
                                                            / earthRadianceSimS(iband)%SN(iwave)
            solarIrradianceRetrS(iband)%solIrrMeas(iwave)   = 1.0d0
            solarIrradianceRetrS(iband)%SN(iwave)           = 1.0d8
            solarIrradianceRetrS(iband)%solIrrError(iwave)  = 1.0d-8
            earthRadianceRetrS(iband)%rad_meas(1,iwave)     = reflectance(indexArray(iwave,iband), channelNumber(iband))
            earthRadianceRetrS(iband)%SN(iwave)             = SNR(indexArray(iwave,iband), channelNumber(iband))
            earthRadianceRetrS(iband)%SNrefspec(iwave)      = 0.0d0  ! value is not known
            earthRadianceRetrS(iband)%rad_error(iwave)      = earthRadianceSimS(iband)%rad_meas(1,iwave) &
                                                            / earthRadianceSimS(iband)%SN(iwave)
          end do ! iwave            
        end do ! iband

      else

        ! new version of the sim file
        ! Select the channel for each fit window for the Earth radiance based on the
        ! specifications in the configuration file
        ! Note that we will eventually fit the sun-normalized radiance and not the irradiance

        do iband = 1, numSpectrBands
          numChannelsForFitWindow_rad = 0
          do ichannel_rad = 1, nchannel_rad
            if (( wavelInstrIrrSimS(iband)%startWavel >= wavelength_rad(                        1, ichannel_rad) - 0.1d0) .and. &
                ( wavelInstrIrrSimS(iband)%endWavel   <= wavelength_rad( nwavel_rad(ichannel_rad), ichannel_rad) + 0.1d0) ) then
              channelNumber_rad(iband) = ichannel_rad
              numChannelsForFitWindow_rad = numChannelsForFitWindow_rad + 1
            end if
          end do ! ichannel_rad
          if ( numChannelsForFitWindow_rad > 1 ) then
            write(errS%temp,*)
            call errorAddLine(errS, errS%temp)
            call logDebug("ERROR in configuration file")
            call logDebug("spectral band / fit window in overlap region of spectral channels")
            call logDebug("can not select which channel to use")
            call logDebug("change fit window in configuration file")
            call logDebug("so that spectral channel can be selected")
            call logDebugI("error ocurred for fit window = ", iband)
            call logDebug("error detected by subroutine readIrrRadFromFile")
            call logDebug("in module readIrrRadFromFileModule")
            call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
            call mystop(errS, 'stopped because spectral channel can not be selected')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          if ( numChannelsForFitWindow_rad < 1 ) then
            write(errS%temp,'(A,F10.4)') 'start wavelength = ', wavelInstrIrrSimS(iband)%startWavel
            call errorAddLine(errS, errS%temp)
            write(errS%temp,'(A,F10.4)') 'end   wavelength = ', wavelInstrIrrSimS(iband)%endWavel
            call errorAddLine(errS, errS%temp)
            write(errS%temp,'(A,F10.4)') 'read start wavelength = ', wavelength_rad(1, ichannel_rad)
            call errorAddLine(errS, errS%temp)
            write(errS%temp,'(A,F10.4)') 'read end   wavelength = ', wavelength_rad( nwavel_rad(ichannel_rad), ichannel_rad)
            call errorAddLine(errS, errS%temp)
            write(errS%temp,*)
            call errorAddLine(errS, errS%temp)
            call logDebug("ERROR in configuration file: fit window lies (partly)")
            call logDebug("outside the wavelength range covered by the channels")
            call logDebug("either make the fit window smaller or ")
            call logDebug("split it into two or more fit windows located in different channels")
            call logDebugI("error ocurred for fit window = ", iband)
            call logDebug("error detected by subroutine readIrrRadFromFile")
            call logDebug("in module readIrrRadFromFileModule")
            call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
            call mystop(errS, 'stopped because fit window lies (partly) outside the channels')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          
        end do ! iband

        ! Select the channel for each fit window for the irradiance based on the
        ! specifications in the configuration file

        do iband = 1, numSpectrBands
          numChannelsForFitWindow_irr = 0
          do ichannel_irr = 1, nchannel_irr
            if (( wavelInstrIrrSimS(iband)%startWavel >= wavelength_irr(                        1, ichannel_irr) - 0.1d0) .and. &
                ( wavelInstrIrrSimS(iband)%endWavel   <= wavelength_irr( nwavel_irr(ichannel_irr), ichannel_irr) + 0.1d0) ) then
              channelNumber_irr(iband) = ichannel_irr
              numChannelsForFitWindow_irr = numChannelsForFitWindow_irr + 1
            end if
          end do ! ichannel_rad
          if ( numChannelsForFitWindow_irr > 1 ) then
            call logDebug("ERROR in configuration file")
            call logDebug("spectral band / fit window in overlap region of spectral channels")
            call logDebug("can not select which channel to use")
            call logDebug("change fit window in configuration file")
            call logDebug("so that spectral channel can be selected")
            call logDebugI("error ocurred for fit window = ", iband)
            call logDebug("error detected by subroutine readIrrRadFromFile")
            call logDebug("in module readIrrRadFromFileModule")
            call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
            call mystop(errS, 'stopped because spectral channel can not be selected')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          if ( numChannelsForFitWindow_irr < 1 ) then
            write(errS%temp,'(A,F10.4)') 'start wavelength = ', wavelInstrIrrSimS(iband)%startWavel
            call errorAddLine(errS, errS%temp)
            write(errS%temp,'(A,F10.4)') 'end   wavelength = ', wavelInstrIrrSimS(iband)%endWavel
            call errorAddLine(errS, errS%temp)
            write(errS%temp,'(A,F10.4)') 'read start wavelength = ', wavelength_rad(1, ichannel_rad)
            call errorAddLine(errS, errS%temp)
            write(errS%temp,'(A,F10.4)') 'read end   wavelength = ', wavelength_rad( nwavel_rad(ichannel_rad), ichannel_rad)
            call errorAddLine(errS, errS%temp)
            call logDebug("ERROR in configuration file: fit window lies (partly)")
            call logDebug("outside the wavelength range covered by the channels")
            call logDebug("either make the fit window smaller or ")
            call logDebug("split it into two or more fit windows located in different channels")
            call logDebugI("error ocurred for fit window = ", iband)
            call logDebug("error detected by subroutine readIrrRadFromFile")
            call logDebug("in module readIrrRadFromFileModule")
            call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
            call mystop(errS, 'stopped because fit window lies (partly) outside the channels')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          
        end do ! iband

        do iband = 1, numSpectrBands

          ! determine the number of wavelengths for irradiance in each fit window, taking into account excluded parts
          counter = 0
          do iwave = 1, nwavel_irr(channelNumber_irr(iband))
            wavel = wavelength_irr(iwave, channelNumber_irr(iband))
            addWavel = .false.
            if ( (wavel >= wavelInstrIrrSimS(iband)%startWavel - 0.01d0) .and. &
                 (wavel <= wavelInstrIrrSimS(iband)%endWavel   + 0.01d0) ) addWavel = .true.
            do ipair = 1, wavelInstrIrrSimS(iband)%nExclude
              if ( (wavel >= wavelInstrIrrSimS(iband)%excludeStart(ipair)) .and. &
                   (wavel <= wavelInstrIrrSimS(iband)%excludeEnd(ipair)) )       &
              addWavel = .false.
            end do ! ipair
            if ( addWavel ) then
              counter = counter + 1
            end if
          end do ! iwave

          wavelInstrIrrSimS(iband)%nwavel  = counter
          wavelInstrIrrRetrS(iband)%nwavel = counter

          call freeMemWavelInstrS(errS, wavelInstrIrrSimS (iband))     ! free memory for irradiance
          call freeMemWavelInstrS(errS, wavelInstrIrrRetrS (iband))    ! free memory for irradiance
          call claimMemWavelInstrS(errS, wavelInstrIrrSimS (iband))    ! claim memory for irradiance
          call claimMemWavelInstrS(errS, wavelInstrIrrRetrS (iband))   ! claim memory for irradiance

          ! fill wavelength arrays  
          counter = 0
          do iwave = 1, nwavel_irr(channelNumber_irr(iband))
            wavel = wavelength_irr(iwave, channelNumber_irr(iband))
            addWavel = .false.
            if ( (wavel >= wavelInstrIrrSimS(iband)%startWavel - 0.01d0) .and. &
                 (wavel <= wavelInstrIrrSimS(iband)%endWavel   + 0.01d0) ) addWavel = .true.
            do ipair = 1, wavelInstrIrrSimS(iband)%nExclude
              if ( (wavel >= wavelInstrIrrSimS(iband)%excludeStart(ipair)) .and. &
                   (wavel <= wavelInstrIrrSimS(iband)%excludeEnd(ipair)) )       &
              addWavel = .false.
            end do ! ipair
            if ( addWavel ) then
              counter = counter + 1
              wavelInstrIrrSimS(iband)%wavelNominal(counter)  = wavelength_irr(iwave, channelNumber_irr(iband))
              wavelInstrIrrSimS(iband)%wavel(counter)         = wavelength_irr(iwave, channelNumber_irr(iband))
              wavelInstrIrrSimS(iband)%wavelShift(counter)    = wavelength_irr(iwave, channelNumber_irr(iband))
              wavelInstrIrrRetrS(iband)%wavelNominal(counter) = wavelength_irr(iwave, channelNumber_irr(iband))
              wavelInstrIrrRetrS(iband)%wavel(counter)        = wavelength_irr(iwave, channelNumber_irr(iband))
              wavelInstrIrrRetrS(iband)%wavelShift(counter)   = wavelength_irr(iwave, channelNumber_irr(iband))
              indexArray_irr(counter,iband) = iwave
            end if
          end do ! iwave

        end do ! iband

        do iband = 1, numSpectrBands

         ! determine number of wavelengths on the radiance grid
          counter = 0
          do iwave = 1, nwavel_rad(channelNumber_rad(iband))
            addWavel = .false.
            wavel = wavelength_rad(iwave, channelNumber_rad(iband))
            if ( (wavel >= wavelInstrRadSimS(iband)%startWavel - 0.01d0) .and. &
                 (wavel <= wavelInstrRadSimS(iband)%endWavel   + 0.01d0) ) addWavel = .true.
            do ipair = 1, wavelInstrRadSimS(iband)%nExclude
              if ( (wavel >= wavelInstrRadSimS(iband)%excludeStart(ipair)) .and. &
                   (wavel <= wavelInstrRadSimS(iband)%excludeEnd  (ipair)) )   addWavel = .false.
            end do ! ipair
            if ( addWavel ) then 
              counter = counter + 1
            end if ! addWavel
          end do ! iwave
          if ( counter < 3 ) then
            call logDebug('ERROR when reading radiance data from file')
            call logDebug('values for at least 3 wavelengths per spectral band are expected')
            call logDebug('but results for less wavelengths were read')
            call logDebugI('for band number = ', iband)
            call logDebug('error produced by subroutine readIrrRadFromFile')
            call logDebug('in module readIrrRadFromFileModule')
            call mystop(errS, 'stopped because not enough data was read')
            if (errorCheck(errS)) return
          end if

          wavelInstrRadSimS(iband)%nwavel  = counter
          wavelInstrRadRetrS(iband)%nwavel = counter

          ! test that we have the same number of wavelengths for radiance and irradiance
          !if ( wavelInstrRadSimS(iband)%nwavel /= wavelInstrIrrSimS(iband)%nwavel ) then
          !  call mystop(errS, 'stopped because not enough data was read')
          !end if

          call freeMemWavelInstrS(errS, wavelInstrRadSimS(iband))      ! free memory for radiance
          call freeMemWavelInstrS(errS, wavelInstrRadRetrS(iband))     ! free memory for radiance
          call claimMemWavelInstrS(errS, wavelInstrRadSimS(iband))     ! claim memory for radiance
          call claimMemWavelInstrS(errS, wavelInstrRadRetrS(iband))    ! claim memory for radiance

         ! fill wavelengths on the radiance grid
          counter = 0
          do iwave = 1, nwavel_rad(channelNumber_rad(iband))
            addWavel = .false.
            wavel = wavelength_rad(iwave, channelNumber_rad(iband))
            if ( (wavel >= wavelInstrRadSimS(iband)%startWavel - 0.01d0) .and. &
                 (wavel <= wavelInstrRadSimS(iband)%endWavel   + 0.01d0) ) addWavel = .true.
            do ipair = 1, wavelInstrRadSimS(iband)%nExclude
              if ( (wavel >= wavelInstrRadSimS(iband)%excludeStart(ipair)) .and. &
                   (wavel <= wavelInstrRadSimS(iband)%excludeEnd  (ipair)) )   addWavel = .false.
            end do ! ipair
            if ( addWavel ) then 
              counter = counter + 1
              wavelInstrRadSimS(iband)%wavelNominal(counter)  = wavelength_rad(iwave, channelNumber_rad(iband))
              wavelInstrRadSimS(iband)%wavel(counter)         = wavelength_rad(iwave, channelNumber_rad(iband))
              wavelInstrRadSimS(iband)%wavelShift(counter)    = 0.0d0
              wavelInstrRadRetrS(iband)%wavelNominal(counter) = wavelength_rad(iwave, channelNumber_rad(iband))
              wavelInstrRadRetrS(iband)%wavel(counter)        = wavelength_rad(iwave, channelNumber_rad(iband))
              wavelInstrRadRetrS(iband)%wavelShift(counter)   = 0.0d0
              indexArray_rad(counter,iband) = iwave
            end if ! addWavel
          end do ! iwave
          if ( counter < 3 ) then
            call logDebug('ERROR when reading irradiance and radiance data from file')
            call logDebug('values for at least 3 wavelengths per spectral band are expected')
            call logDebug('but results for less wavelengths were read')
            call logDebugI('for band number = ', iband)
            call logDebug('error produced by subroutine readIrrRadFromFile')
            call logDebug('in module readModule')
            call mystop(errS, 'stopped because not enough data was read')
            if (errorCheck(errS)) return
          end if
        end do ! iband

        ! use only radiance and irradiance data whose wavelength differs less than the threshold
        do iband = 1, numSpectrBands
          counter = 0
          index_irr = 0
          index_rad = 0
          do iwave_irr = 1, wavelInstrIrrSimS(iband)%nwavel
            index_irr = index_irr + 1
            do iwave_rad = 1, wavelInstrRadSimS(iband)%nwavel
               index_rad = index_rad + 1
               if ( abs ( wavelInstrIrrSimS(iband)%wavelNominal(iwave_irr) -     &
                          wavelInstrRadSimS(iband)%wavelNominal(iwave_rad) ) <   &
                          threshWavelDiffRadIrr ) then
                 counter = counter + 1
                 indexArray_irr_mod(counter,iband) = indexArray_irr(iwave_irr,iband) 
                 indexArray_rad_mod(counter,iband) = indexArray_rad(iwave_rad,iband) 
                 wavelength_irr(counter, iband) = wavelInstrIrrSimS(iband)%wavelNominal(iwave_irr)
                 wavelength_rad(counter, iband) = wavelInstrRadSimS(iband)%wavelNominal(iwave_rad)
               end if
            end do ! iwave_rad
          end do ! iwave_irr
          wavelInstrIrrSimS(iband)%nwavel  = counter
          wavelInstrIrrRetrS(iband)%nwavel = counter
          wavelInstrRadSimS(iband)%nwavel  = counter
          wavelInstrRadRetrS(iband)%nwavel = counter

          ! deallocate and allocate the relevant arrays
          call freeMemWavelInstrS(errS, wavelInstrIrrSimS(iband))      ! free memory for irradiance
          call freeMemWavelInstrS(errS, wavelInstrIrrRetrS(iband))     ! free memory for irradiance
          call claimMemWavelInstrS(errS, wavelInstrIrrSimS(iband))     ! claim memory for irradiance
          call claimMemWavelInstrS(errS, wavelInstrIrrRetrS(iband))    ! claim memory for irradiance
          call freeMemWavelInstrS(errS, wavelInstrRadSimS(iband))      ! free memory for radiance
          call freeMemWavelInstrS(errS, wavelInstrRadRetrS(iband))     ! free memory for radiance
          call claimMemWavelInstrS(errS, wavelInstrRadSimS(iband))     ! claim memory for radiance
          call claimMemWavelInstrS(errS, wavelInstrRadRetrS(iband))    ! claim memory for radiance

          ! fill the wavelength grids
          do iwave_irr = 1, wavelInstrIrrSimS(iband)%nwavel
            wavelInstrIrrSimS(iband)%wavelNominal(iwave_irr)  = wavelength_irr(iwave_irr, iband)
            wavelInstrIrrSimS(iband)%wavel(iwave_irr)         = wavelength_irr(iwave_irr, iband)
            wavelInstrIrrSimS(iband)%wavelShift(iwave_irr)    = 0.0d0
            wavelInstrIrrRetrS(iband)%wavelNominal(iwave_irr) = wavelength_irr(iwave_irr, iband)
            wavelInstrIrrRetrS(iband)%wavel(iwave_irr)        = wavelength_irr(iwave_irr, iband)
            wavelInstrIrrRetrS(iband)%wavelShift(iwave_irr)   = 0.0d0
          end do ! iwave_irr
          do iwave_rad = 1, wavelInstrRadSimS(iband)%nwavel
            wavelInstrRadSimS(iband)%wavelNominal(iwave_rad)  = wavelength_rad(iwave_rad, iband)
            wavelInstrRadSimS(iband)%wavel(iwave_rad)         = wavelength_rad(iwave_rad, iband)
            wavelInstrRadSimS(iband)%wavelShift(iwave_rad)    = 0.0d0
            wavelInstrRadRetrS(iband)%wavelNominal(iwave_rad) = wavelength_rad(iwave_rad, iband)
            wavelInstrRadRetrS(iband)%wavel(iwave_rad)        = wavelength_rad(iwave_rad, iband)
            wavelInstrRadRetrS(iband)%wavelShift(iwave_rad)   = 0.0d0
          end do ! iwave_irr
        end do ! iband

        call set_nwavelInstr(errS, globalS)
        if (errorCheck(errS)) return
        call set_nwavelMR(errS, globalS)
        if (errorCheck(errS)) return
        call freeMemRetrS_wavel_only_not_Se  (errS,  globalS%retrS )
        if (errorCheck(errS)) return
        call claimMemRetrS_wavel_only_not_Se (errS,  globalS%retrS )
        if (errorCheck(errS)) return
        call freeMemDiagnosticS_wavel_only   (errS,  globalS%diagnosticS )
        if (errorCheck(errS)) return
        call claimMemDiagnosticS_wavel_only  (errS,  globalS%diagnosticS )
        if (errorCheck(errS)) return

        do iband =  1, globalS%numSpectrBands
          do itrace = 1, globalS%nTrace
            call freeMemXsecS(errS, globalS%XsecHRSimS(iband,iTrace))
            if (errorCheck(errS)) return
            call freeMemXsecLUTS(errS, globalS%XsecHRLUTSimS(iband,iTrace))
            if (errorCheck(errS)) return
            call freeMemXsecS(errS, globalS%XsecHRRetrS(iband,iTrace))
            if (errorCheck(errS)) return
            call freeMemXsecLUTS(errS, globalS%XsecHRLUTRetrS(iband,iTrace))
            if (errorCheck(errS)) return
            call claimMemXsecS(errS, globalS%XsecHRSimS(iband,iTrace))
            if (errorCheck(errS)) return
            call claimMemXsecLUTS(errS, globalS%XsecHRLUTSimS(iband,iTrace))
            if (errorCheck(errS)) return
            call claimMemXsecS(errS, globalS%XsecHRRetrS(iband,iTrace))
            if (errorCheck(errS)) return
            call claimMemXsecLUTS(errS, globalS%XsecHRLUTRetrS(iband,iTrace))
            if (errorCheck(errS)) return
          end do ! iTrace
        end do ! iband

        do iband = 1, globalS%numSpectrBands
          call freeMemSolarIrradianceS (errS,  globalS%solarIrradianceSimS(iband) )
          if (errorCheck(errS)) return
          call claimMemSolarIrradianceS(errS,  globalS%solarIrradianceSimS(iband) )
          if (errorCheck(errS)) return
          call freeMemSolarIrradianceS (errS,  globalS%solarIrradianceRetrS(iband) )
          if (errorCheck(errS)) return
          call claimMemSolarIrradianceS(errS,  globalS%solarIrradianceRetrS(iband) )
          if (errorCheck(errS)) return
          call freeMemEarthRadianceS   (errS,  globalS%earthRadianceSimS(iband) )
          if (errorCheck(errS)) return
          call claimMemEarthRadianceS  (errS,  globalS%earthRadianceSimS(iband) )
          if (errorCheck(errS)) return
          call freeMemEarthRadianceS   (errS,  globalS%earthRadianceRetrS(iband) )
          if (errorCheck(errS)) return
          call claimMemEarthRadianceS  (errS,  globalS%earthRadianceRetrS(iband) )
          if (errorCheck(errS)) return
          call freeMemRRS_RingS        (errS,  globalS%RRS_RingSimS(iband) )
          if (errorCheck(errS)) return
          call claimMemRRS_RingS       (errS,  globalS%RRS_RingSimS(iband) )
          if (errorCheck(errS)) return
          call freeMemRRS_RingS        (errS,  globalS%RRS_RingRetrS(iband) )
          if (errorCheck(errS)) return
          call claimMemRRS_RingS       (errS,  globalS%RRS_RingRetrS(iband) )
          if (errorCheck(errS)) return
        end do

        do i_column = 1, globalS%ncolumn
          call freeMemColumnSGainOnly (errS, globalS%columnSimS(i_column))
          if (errorCheck(errS)) return
          call claimMemColumnSGainOnly(errS, globalS%columnSimS(i_column))
          if (errorCheck(errS)) return
          call freeMemColumnSGainOnly (errS, globalS%columnRetrS(i_column))
          if (errorCheck(errS)) return
          call claimMemColumnSGainOnly(errS, globalS%columnRetrS(i_column))
          if (errorCheck(errS)) return
        end do ! i_column

        ! test whether the wavelength grids for radiance and irradiance are the same
        ! within the threshold. If they are the same high sampling interpolation to
        ! bring the solar irradiance to the radiance grid is skipped.
        wavelength_grids_differ = .false.
        do iband = 1, globalS%numSpectrBands
          do iwave = 1, wavelInstrIrrSimS(iband)%nwavel
            if ( abs( wavelength_irr(iwave, iband) - &
                      wavelength_rad(iwave, iband) ) > threshWavelDiff ) then
              wavelength_grids_differ = .true.
              exit
            end if
          end do ! iwave
        end do ! iband

        ! test whether the wavelength grids for radiance and irradiance differ 
        ! more than threshWavelDiffRadIrr (specified near the beginning of the
        ! datastructures.f90 file). If they differ more an error message is
        ! generated. A typical value for threshWavelDiffRadIrr is 3.0E-2
        do iband = 1, globalS%numSpectrBands
          do iwave = 1, wavelInstrIrrSimS(iband)%nwavel
            if ( abs( wavelength_irr(iwave, iband) - &
                      wavelength_rad(iwave, iband) ) > threshWavelDiffRadIrr ) then
              call logDebug('ERROR when analyzing irradiance and radiance data read from file')
              call logDebug('difference in wavelength for radiance and irradiance must be')
              call logDebug('less than threshWavelDiffRadIrr')
              call logDebugD('threshWavelDiffRadIrr = ', threshWavelDiffRadIrr)
              call logDebug('larger difference in wavelength encountered')
              call logDebugI('for band number = ', iband)
              write(errS%temp,*) 'irradiance wavelength = ', wavelength_irr(iwave, iband)
              call errorAddLine(errS, errS%temp)
              write(errS%temp,*) 'radiance wavelength   = ', wavelength_rad(iwave, iband)
              call errorAddLine(errS, errS%temp)
              call logDebug('error produced by subroutine readIrrRadFromFile')
              call logDebug('in module readIrrRadFromFileModule')
              call mystop(errS, 'stopped as wavelength difference irradiance and radiance is too large')
              if (errorCheck(errS)) goto 99999
            end if
          end do ! iwave
        end do ! iband

        if ( wavelength_grids_differ ) then
          ! bring irradiance to the radiance wavelength grid

          ! first store read irradiance data in solarIrradianceReadS(:)
          do iband = 1, numSpectrBands
            solarIrradianceReadS(iband)%nwavelHR = wavelHRSimS(iband)%nwavel       ! number of wavelengths on HR grid
            solarIrradianceReadS(iband)%nwavelMR = wavelHRSimS(iband)%nwavel       ! number of wavelengths on MR grid
            solarIrradianceReadS(iband)%nwavel   = wavelInstrIrrSimS(iband)%nwavel ! number of wavelengths on instrument grid
            call claimMemSolarIrradianceS(errS, solarIrradianceReadS(iband))       ! allocate memory for solar irradiance spectra
            do iwave = 1, wavelInstrIrrSimS(iband)%nwavel
              solarIrradianceReadS(iband)%solIrrMeas(iwave) = irradiance(indexArray_irr_mod(iwave,iband), channelNumber_irr(iband))
            end do ! iwave
            ! copy slit function parameters
            solarIrradianceReadS(iband)%slitFunctionSpecsS = solarIrradianceSimS(iband)%slitFunctionSpecsS
          end do ! iband

          ! set up HR wavelength grid
          do iband = 1, numSpectrBands
            FWHM = solarIrradianceSimS(iband)%slitFunctionSpecsS%FWHM
            call setupHRWavelengthGridIrr(errS, wavelInstrIrrSimS(iband), wavelHRSimS(iband), FWHM)
            if (errorCheck(errS)) return
          end do ! iband

          ! Get solar irradiance on the high-resolution grid (integration grid) and calculate the
          ! solar irradiance on the measured (simulated) radiance and irradiance grids 
          do iband = 1, numSpectrBands
            ! convolute HR solar irradiance with slit function on read irradiance grid
            if (staticS%operational == 1) then
              call getHRSolarIrradianceStatic(errS, staticS, wavelMRIrrSimS(iband), solarIrradianceSimS(iband) )
              if (errorCheck(errS)) return
            else
              call getHRSolarIrradiance(errS, wavelMRIrrSimS(iband), solarIrradianceSimS(iband) )
              if (errorCheck(errS)) return
            end if
            solarIrradianceReadS(iband)%solarIrrFileName = solarIrradianceSimS(iband)%solarIrrFileName
            if (staticS%operational == 1) then
              call getHRSolarIrradianceStatic(errS, staticS, wavelMRIrrSimS(iband), solarIrradianceReadS(iband) )
              if (errorCheck(errS)) return
            else
              call getHRSolarIrradiance(errS, wavelMRIrrSimS(iband), solarIrradianceReadS(iband) )
              if (errorCheck(errS)) return
            end if
            ! integrateSlitFunctionIrr puts the convoluted values in %solIrr(:) not in %solIrrMeas(:)
            call integrateSlitFunctionIrr(errS, controlSimS, wavelHRSimS(iband), wavelInstrIrrSimS(iband), &
                                          solarIrradianceReadS(iband))
            if (errorCheck(errS)) return
            call integrateSlitFunctionIrr(errS, controlSimS, wavelHRSimS(iband), wavelInstrRadSimS(iband), &
                                          solarIrradianceSimS(iband))
            if (errorCheck(errS)) return
            ! replace solarIrradianceSimS with the measured values but corrected for wavelength differences between
            ! the irradiance and radiance wavelength grid
            do iwave = 1, wavelInstrIrrSimS(iband)%nwavel
              solarIrradianceSimS(iband)%solIrrMeas(iwave) = &
                                        solarIrradianceReadS(iband)%solIrrMeas(iwave) &   ! read values
                                      * solarIrradianceSimS(iband)%solIrrMR(iwave)    &   ! values on radiance grid
                                      / solarIrradianceReadS(iband)%solIrrMR(iwave)       ! values on irradiance grid
              wavelInstrIrrSimS(iband)%wavel(iwave) = wavelInstrRadSimS(iband)%wavel(iwave)
            end do ! iwave
          end do ! iband

        else

          do iband = 1, numSpectrBands
            call freeMemSolarIrradianceS(errS, solarIrradianceSimS(iband))         ! deallocate memory for solar irradiance spectra
            if ( controlSimS%ignoreSlit ) then
              solarIrradianceSimS(iband)%nwavelMR  = wavelInstrIrrSimS(iband)%nwavel
              solarIrradianceRetrS(iband)%nwavelMR = wavelInstrIrrRetrS(iband)%nwavel
            else
              solarIrradianceSimS(iband)%nwavelMR  = wavelHRSimS(iband)%nwavel
              solarIrradianceRetrS(iband)%nwavelMR = wavelHRRetrS(iband)%nwavel
            end if
            solarIrradianceSimS(iband) %nwavelHR = wavelHRSimS(iband)%nwavel       ! number of wavelengths on HR grid
            solarIrradianceSimS(iband) %nwavel   = wavelInstrIrrSimS(iband)%nwavel ! number of wavelengths on instrument grid
            solarIrradianceRetrS(iband)%nwavelHR = wavelHRSimS(iband)%nwavel       ! number of wavelengths on HR grid
            solarIrradianceRetrS(iband)%nwavel   = wavelInstrIrrSimS(iband)%nwavel ! number of wavelengths on instrument grid
            call claimMemSolarIrradianceS(errS, solarIrradianceSimS(iband))              ! allocate memory for solar irr spectra
            do iwave = 1, wavelInstrIrrSimS(iband)%nwavel
              solarIrradianceSimS(iband)%solIrrMeas(iwave) = irradiance(indexArray_rad_mod(iwave,iband), channelNumber_rad(iband))
            end do ! iwave
          end do ! iband

        end if ! wavelength_grids_differ

        do iband = 1, numSpectrBands
          do iwave = 1, wavelInstrIrrSimS(iband)%nwavel
            solarIrradianceSimS(iband)%SN(iwave)            = SNR_irr(indexArray_irr_mod(iwave,iband), channelNumber_irr(iband))
            if ( solarIrradianceSimS(iband)%SN(iwave) > solarIrradianceSimS(iband)%SNmax ) &
                 solarIrradianceSimS(iband)%SN(iwave) = solarIrradianceSimS(iband)%SNmax
            solarIrradianceSimS(iband)%solIrrError(iwave)   = solarIrradianceSimS(iband)%solIrrMeas(iwave) &
                                                            /  solarIrradianceSimS(iband)%SN(iwave)
            earthRadianceSimS(iband)%rad_meas(1,iwave)      = radiance(indexArray_rad_mod(iwave,iband), channelNumber_rad(iband))
            earthRadianceSimS(iband)%SN(iwave)              = SNR_rad(indexArray_rad_mod(iwave,iband), channelNumber_rad(iband))
            if ( earthRadianceSimS(iband)%SN(iwave) > earthRadianceSimS(iband)%SNmax ) &
                 earthRadianceSimS(iband)%SN(iwave) = earthRadianceSimS(iband)%SNmax
            earthRadianceSimS(iband)%SNrefspec(iwave)       = 0.0d0  ! value is not known
            earthRadianceSimS(iband)%rad_error(iwave)       = earthRadianceSimS(iband)%rad_meas(1,iwave) &
                                                            / earthRadianceSimS(iband)%SN(iwave)   
          end do ! iwave            
        end do ! iband

      end if ! old_version

      if ( verbose ) then
        write(intermediateFileUnit,*)
        do iband = 1, numSpectrBands
          write(intermediateFileUnit,'(A)') &
          ' wavelength and solar irrandiance read and wavelength and earth radiance on radiance grid'
          do iwave = 1, wavelInstrRadSimS(iband)%nwavel
            write(intermediateFileUnit,'(F12.6, ES20.10, F12.6, ES20.10 )') wavelInstrIrrSimS(iband)%wavel(iwave),         &
                                                                            solarIrradianceSimS(iband)%solIrrMeas(iwave),  &
                                                                            wavelInstrRadSimS(iband)%wavel(iwave),         &
                                                                            earthRadianceSimS(iband)%rad_meas(1,iwave)
          end do ! iwave
          write(intermediateFileUnit,'(A)') &
          ' wavelength and solar SNR and wavelength and earth SNR'
          do iwave = 1, wavelInstrRadSimS(iband)%nwavel
            write(intermediateFileUnit,'(F12.6, ES20.10, F12.6, ES20.10 )') wavelInstrIrrSimS(iband)%wavel(iwave), &
                                                                            solarIrradianceSimS(iband)%SN(iwave),  &
                                                                            wavelInstrRadSimS(iband)%wavel(iwave), &
                                                                            earthRadianceSimS(iband)%SN(iwave)
          end do ! iwave
        end do ! iband
      end if ! verbose

99999 continue
      call exit('readIrrRadPostProcess')

    end subroutine readIrrRadPostProcess


    subroutine setupHRWavelengthGridIrr(errS, wavelInstrS, wavelHRS,  FWHM)

      ! We assume that we can use spline interpolation on the solar spectrum
      ! to get the solar irradiance at the high resolution (HR) wavelength grids
      ! for simulation and retrieval, i.e. the high resolution solar spectrum
      ! is not undersampled.

      ! This subroutine allocates and fills the array wavel in the structures wavelHRS

       implicit none

       type(errorType),      intent(inout) :: errS
       type(wavelInstrType), intent(in)    :: wavelInstrS
       type(wavelHRType),    intent(inout) :: wavelHRS
       real(8),              intent(in)    :: FWHM

       real(8)    :: waveStart, waveEnd
       type boundariesType
         integer          :: numBoundaries
         real(8), pointer :: boundaries(:)  => null()
       end type boundariesType

       real(8), allocatable :: intervalBoundaries(:)
       real(8)              :: maxInterval
       !  x0(i,j) are the division points (x0(i), i = 1:j) and w0 the corresponding weights
       real(8), allocatable :: x0(:,:)
       real(8), allocatable :: w0(:,:)
       real(8)              :: dw, sw, wavel, newWavel

       logical              :: addWavel
       integer              :: nwavel
       real(8), allocatable :: wavelBand(:)        ! (nwavel) full band - not accounting for excluded parts
       real(8), allocatable :: wavelBandWeight(:)  ! (nwavel) weights for full band - not accounting for excluded parts

       integer              :: iwave, nlines, iinterval, index, ipair
       integer              :: iGauss, nGauss, nGaussMax, nGaussMin

       integer              :: allocStatus
       integer              :: deallocStatus

       logical, parameter   :: verbose = .false.

       ! initialize
       waveStart = wavelInstrS%startWavel - 2.0d0 * FWHM
       waveEnd   = wavelInstrS%endWavel   + 2.0d0 * FWHM

       ! starting at waveStart we add a boundary that lies one FWHM further
       nlines = 0
       wavel = waveStart
       do 
         nlines = nlines + 1
         newWavel = wavel + FWHM
         wavel = newWavel
         if ( wavel > waveEnd ) exit
       end do

       allocStatus = 0
       allocate ( intervalBoundaries(0:nlines), STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('FATAL ERROR: allocation failed')
         call logDebug('for intervalBoundaries')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       ! fill values for the array intervalBoundaries
       nlines = 0
       wavel = waveStart
       intervalBoundaries(0) = wavel
       do 
         nlines = nlines + 1
         newWavel = wavel + FWHM
         wavel = newWavel
         intervalBoundaries(nlines) = wavel
         if ( wavel > waveEnd ) exit
       end do

       ! fill Gausspoints and weights arrays
       nGaussMax = wavelHRS%nGaussFWHM
       nGaussMin = wavelHRS%nGaussFWHM

       ! allocate arrays for gausspoints and weight on (0,1)
       ! they are deallocated just before leaving this subroutine

       allocStatus = 0
       allocate ( x0(nGaussMax,nGaussMax), w0(nGaussMax,nGaussMax), STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('FATAL ERROR: allocation failed')
         call logDebug('for x0 or w0')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       do iGauss = nGaussMin, nGaussMax
         call GaussDivPoints(errS, 0.0d0, 1.0d0, x0(:,iGauss), w0(:,iGauss), iGauss)
         if (errorCheck(errS)) return
       end do

       ! determine the number of wavelengths for the high resolution simulation grid
       maxInterval = 0.0d0
       do iinterval = 2, size(intervalBoundaries) - 2
         dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)
         if ( dw >  maxInterval )  maxInterval = dw    
       end do

       index = 0
       do iinterval = 1, size(intervalBoundaries) - 1
         dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)      ! width of interval
         sw = intervalBoundaries(iinterval-1)                                      ! start wavelength for interval
         nGauss = max( nGaussMin, nint( nGaussMax * dw / maxInterval ) )
         if (nGauss >  nGaussMax ) nGauss =  nGaussMax
         do iGauss = 1, nGauss
           index = index + 1
         end do
       end do

       nwavel = index 

       allocStatus = 0
       allocate ( wavelBand(nwavel), wavelBandWeight(nwavel), STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('FATAL ERROR: allocation failed')
         call logDebug('for wavelBand or wavelBandWeight')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       ! fill wavelength grid for the entire spectral band
       index = 1
       do iinterval = 1, size(intervalBoundaries) - 1
         dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)
         sw = intervalBoundaries(iinterval-1)
         nGauss = max(  nGaussMin, nint( nGaussMax * dw / maxInterval ) )
         if (nGauss >  nGaussMax ) nGauss = nGaussMax
         do iGauss = 1, nGauss
           wavelBand(index)       = sw + dw * x0(iGauss, nGauss)
           wavelBandWeight(index) = dw * w0(iGauss, nGauss)
           index = index + 1
         end do
       end do

       ! deal with excluded intervals

       ! copy excluded intervals and reduce them with 4 * FWHM (2 left and 2 right)
       wavelHRS%nExclude        = wavelInstrS%nExclude
       wavelHRS%excludeStart(:) = wavelInstrS%excludeStart(:) + 2 * FWHM
       wavelHRS%excludeEnd(:)   = wavelInstrS%excludeEnd(:)   - 2 * FWHM

       ! determine the lengths of the wavelength array taking into account excluded wavelengths
       if ( wavelHRS%nExclude > 0 ) then
         index = 0
         do iwave = 1, nwavel
           wavel = wavelBand(iwave)
           addWavel = .true.
           do ipair = 1, wavelHRS%nExclude
             if ( (wavel >= wavelHRS%excludeStart(ipair)) .and. (wavel <= wavelHRS%excludeEnd(ipair)) ) &
               addWavel = .false.
           end do ! ipair
           if ( addWavel ) index = index + 1
         end do ! iwave

         wavelHRS%nwavel = index

       else

         wavelHRS%nwavel = nwavel

       end if

       ! claim memory space
       call claimMemWavelHRS(errS, wavelHRS)          ! claim memory for high resolution wavelength grid

       ! fill wavelengths

       if ( wavelHRS%nExclude > 0 ) then

         index = 0
         do iwave = 1, nwavel
           wavel = wavelBand(iwave)
           addWavel = .true.
           do ipair = 1, wavelInstrS%nExclude
             if ( (wavel >= wavelHRS%excludeStart(ipair)) .and. (wavel <= wavelHRS%excludeEnd(ipair)) ) &
               addWavel = .false.
           end do ! ipair
           if ( addWavel ) then
             index = index + 1
             wavelHRS%wavel(index)  = wavelBand(iwave)
             wavelHRS%weight(index) = wavelBandWeight(iwave)
           end if
         end do ! iwave

       else

         do iwave = 1, nwavel
           wavelHRS%wavel(iwave)  = wavelBand(iwave)
           wavelHRS%weight(iwave) = wavelBandWeight(iwave)
         end do

       end if

       if ( verbose ) then

         write(intermediateFileUnit, '(A, 2F10.4)') 'wavelength range = ', waveStart, waveEnd

         write(intermediateFileUnit, *) 
         write( intermediateFileUnit, *) ' interval boundaries'
         do iinterval = 0, size(intervalBoundaries) -1
           write(intermediateFileUnit, '(F15.6)') intervalBoundaries(iinterval)
         end do
         
         write(intermediateFileUnit, *) 
         write(intermediateFileUnit,'(A)') 'wavelengths for the high resolution grid'
         do iwave = 1, wavelHRS%nwavel
           write(intermediateFileUnit,'(F12.5)') wavelHRS%wavel(iwave)
         end do

       end if ! verbose

       ! clean up
       deallocStatus  = 0
       deallocate( intervalBoundaries, x0, w0, wavelBand, wavelBandWeight, STAT = deallocStatus )
       if ( deallocStatus /= 0 ) then
         call logDebug('FATAL ERROR: deallocation failed')
         call logDebug('for intervalBoundaries, x0, w0, wavelBand, or wavelBandWeight')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because deallocation failed')
         if (errorCheck(errS)) return
       end if

    end subroutine setupHRWavelengthGridIrr

    subroutine set_nwavelInstr(errS, globalS)
      type(errorType), intent(inout) :: errS
      type(globalType) :: globalS

      ! set values for the number of wavelengths on the instrument grid

      integer :: iTrace, iband, icolumn

      do iTrace = 1, globalS%nTrace
        do iband = 1, globalS%numSpectrBands
          globalS%XsecHRSimS (iband,iTrace)%nwavel  = globalS%wavelInstrRadSimS(iband)%nwavel
          globalS%XsecHRRetrS(iband,iTrace)%nwavel  = globalS%wavelInstrRadRetrS(iband)%nwavel
        end do
      end do

      do icolumn = 1, globalS%ncolumn
        globalS%columnSimS (icolumn)%nwavel  = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
        globalS%columnRetrS(icolumn)%nwavel  = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
      end do ! icolumn

      do iband = 1, globalS%numSpectrBands
        globalS%solarIrradianceSimS (iband)%nwavel     = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%solarIrradianceRetrS(iband)%nwavel     = globalS%wavelInstrRadRetrS(iband)%nwavel
        globalS%earthRadianceSimS(iband)%nwavel        = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%earthRadianceRetrS(iband)%nwavel       = globalS%wavelInstrRadRetrS(iband)%nwavel
        globalS%weakAbsSimS(iband)%nwavelInstr         = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%weakAbsRetrS(iband)%nwavelInstr        = globalS%wavelInstrRadRetrS(iband)%nwavel
        globalS%RRS_RingSimS(iband)%nwavel             = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%RRS_RingRetrS(iband)%nwavel            = globalS%wavelInstrRadRetrS(iband)%nwavel
        globalS%earthRadStrayLightSimS(iband)%nwavelCB = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%earthRadStrayLightRetrS(iband)%nwavelCB= globalS%wavelInstrRadRetrS(iband)%nwavel
      end do

      globalS%retrS%nwavelRetr                       = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
      globalS%diagnosticS%nwavelRetr                 = globalS%retrS%nwavelRetr

    end subroutine set_nwavelInstr

    subroutine setMRWavelengthGrid(errS, controlS, wavelInstrS, wavelHRS, wavelMRS)
      ! set medium resolution wavelength grid
      ! if controlS%ignoreSlit = .true. use instrument grid
      ! if controlS%ignoreSlit = .false. use HR grid
      implicit none

      type(errorType), intent(inout) :: errS
      type(controlType),    intent(in)    :: controlS      ! control parameters
      type(wavelInstrType), intent(in)    :: wavelInstrS
      type(wavelHRType),    intent(in)    :: wavelHRS
      type(wavelHRType),    intent(inout) :: wavelMRS

      wavelMRS%nGaussMax       = wavelHRS%nGaussMax
      wavelMRS%nGaussMin       = wavelHRS%nGaussMin
      wavelMRS%nGaussFWHM      = wavelHRS%nGaussFWHM
      wavelMRS%nExclude        = wavelHRS%nExclude
      wavelMRS%excludeStart(:) = wavelHRS%excludeStart(:)
      wavelMRS%excludeEnd(:)   = wavelHRS%excludeEnd(:)

      ! free memory space
      call freeMemWavelHRS(errS, wavelMRS)
      if (errorCheck(errS)) return

      if ( controlS%ignoreSlit ) then
        wavelMRS%nwavel = wavelInstrS%nwavel
      else
        wavelMRS%nwavel = wavelHRS%nwavel
      end if ! controlS%ignoreSlit

      ! claim memory space
      call claimMemWavelHRS(errS, wavelMRS)
      if (errorCheck(errS)) return
      
      if ( controlS%ignoreSlit ) then
        wavelMRS%wavel(:)  = wavelInstrS%wavel(:)
        wavelMRS%weight(:) = 0.0d0
      else
        wavelMRS%wavel(:)  = wavelHRS%wavel(:)
        wavelMRS%weight(:) = wavelHRS%weight(:)
      end if ! controlS%ignoreSlit

    end subroutine setMRWavelengthGrid


    subroutine set_nwavelMR(errS, globalS)
      type(errorType), intent(inout) :: errS
      type(globalType) :: globalS

      integer :: iband

      do iband = 1, globalS%numSpectrBands
        globalS%solarIrradianceSimS (iband)%nwavelMR = globalS%wavelMRRadSimS (iband)%nwavel
        globalS%solarIrradianceRetrS(iband)%nwavelMR = globalS%wavelMRRadRetrS(iband)%nwavel
      end do

    end subroutine set_nwavelMR

  end module readIrrRadFromFileModule


