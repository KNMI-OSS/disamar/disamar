!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!


module staticDataModule

    use dataStructures

    type(staticType), pointer :: staticS

    save

end module staticDataModule

