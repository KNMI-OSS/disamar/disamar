!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module radianceIrradianceModule

  ! This module contains subroutines to calculate the radiance and irradiance as measured
  ! by a remote sensing instrument in UV-VIS-NIR-SWIR

  use dataStructures
  use mathTools,            only: GaussDivPoints, splintLin, splint, spline, polyInt, polyDeriv, &
                                  locate, getSmoothAndDiffXsec, mgeo, slitfunction
  use propAtmosphereModule, only: getOptPropAtm
  use LabosModule,          only: layerBasedOrdersScattering, singleScattering, allocCoef, deallocCoef, &
                                  fillPlmVector, layerBasedOrdersScattering_fc, apply_q3, apply_q4
  use readModule,           only: getRadianceRefSpectrumfromFile, getCharatisticBiasfromFile
  use ramansspecs,          only: ConvoluteSpecRaman, ConvoluteSpecRamanMS, TotalRamanXsecScatWavel, RayXsec

  ! default is private
  private 
  public  :: calcReflAndDeriv, calculate_dn_dnode, calculate_K_clr, calculate_K_cld
  public  :: setPolynomialDerivatives_clr, setPolynomialDerivatives_cld, calculate_wfnode
  public  :: integrateSlitFunctionIrr, integrateSlitFunctionRad, ignorePolarizationScrambler
  public  :: specifyNoise, fillReflectanceSim, fillReflectanceRetr, fillAltPresGridRTM
  public  :: addSpecFeaturesIrr, addSpecFeaturesRad, addSimpleOffsets, addSmear, addStraylight, addRingSpec
  public  :: fillReferenceSpectrum, fillRadianceAtMulOffsetNodes, fillRadianceAtStrayLightNodes, addMulOffset
  public  :: fillReflCalibrationError, setRingDerivatives, calculateRTM_Ring_spectra
  public  :: fillRadianceDerivativesHRgrid
  public  :: allocateUD, deallocateUD

  contains


  subroutine fillAltPresGridRTM(errS, cloudAerosolRTMgridS, optPropRTMGridS)

    ! Fill altitude grids and gaussian weights for radiative transfer
    ! these are stored in optPropRTMGridS%RTMaltitude and optPropRTMGridS%RTMweight
    ! the altitudes and weights for the sub-grid are also calculated here

    ! to prepare for integration over pressure, fill also values on the pressure grid

    implicit none

    type(errorType),               intent(inout) :: errS
    type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS

    ! local
    integer              :: index, maxGausspoints, igauss, ibound, ilayer
    real(8)              :: dzLay

    real(8), allocatable :: x0(:), w0(:)     ! gaussian points on (a,b)

    integer :: allocStatus, deallocStatus

    logical, parameter   :: verbose = .false.


    optPropRTMGridS%intervalBounds   = cloudAerosolRTMgridS%intervalBounds

    ! fill RTMaltitude and RTMweights
    maxGausspoints = maxval(cloudAerosolRTMgridS%intervalnGauss)
    allocate( x0(maxGausspoints), w0(maxGausspoints), STAT = allocStatus )
    if ( allocStatus /= 0 ) then
      call logDebug('FATAL ERROR: allocation failed')
      call logDebug('for x0(maxGausspoints) or w0(maxGausspoints)')
      call logDebug('in subroutine fillAltPresGridRTM')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if

    index = 0

    optPropRTMGridS%RTMaltitude(0) = cloudAerosolRTMgridS%intervalBounds(0)
    optPropRTMGridS%RTMweight(0)   = 0.0d0

    do ibound = 1, cloudAerosolRTMgridS%ninterval
      call GaussDivPoints(errS, cloudAerosolRTMgridS%intervalBounds(ibound-1), &
                          cloudAerosolRTMgridS%intervalBounds(ibound),   &
                          x0, w0, cloudAerosolRTMgridS%intervalnGauss(ibound))
      if (errorCheck(errS)) return
      do igauss = 1, cloudAerosolRTMgridS%intervalnGauss(ibound)  
        index = index + 1 
        optPropRTMGridS%RTMaltitude(index) = x0(igauss)
        optPropRTMGridS%RTMweight  (index) = w0(igauss)
      end do ! gauss
      index = index + 1
      optPropRTMGridS%RTMaltitude(index) = cloudAerosolRTMgridS%intervalBounds(ibound)
      optPropRTMGridS%RTMweight  (index) = 0.0d0
    end do ! ibound 

    deallocate( x0, w0, STAT = deallocStatus )
    if ( deallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: deallocation failed')
      call logDebug('for x0(maxGausspoints) or w0(maxGausspoints)')
      call logDebug('in subroutine fillAltPresGridRTM')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

    ! fill altitude and weight for the sublayers
    allocate ( x0(optPropRTMGridS%nGaussLay), w0(optPropRTMGridS%nGaussLay), STAT = allocStatus )
    if ( allocStatus /= 0 ) then
      call logDebug('FATAL ERROR: allocation failed')
      call logDebug('for x0(optPropRTMGridS%nGaussLay) or w0(optPropRTMGridS%nGaussLay)')
      call logDebug('in subroutine fillAltPresGridRTM')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if

    call GaussDivPoints(errS, 0.0d0, 1.0d0, x0, w0, optPropRTMGridS%nGaussLay)
    if (errorCheck(errS)) return

    index = 0

    optPropRTMGridS%RTMaltitudeSub(0) = optPropRTMGridS%RTMaltitude(0)
    optPropRTMGridS%RTMweightSub  (0) = 0.0d0
    do ilayer = 1, optPropRTMGridS%RTMnlayer
      dzLay = optPropRTMGridS%RTMaltitude(ilayer) - optPropRTMGridS%RTMaltitude(ilayer-1)
        do iGauss = 1, optPropRTMGridS%ngaussLay
          index = index + 1
          optPropRTMGridS%RTMweightSub  (index) = w0(iGauss) * dzLay
          optPropRTMGridS%RTMaltitudeSub(index) = optPropRTMGridS%RTMaltitude(ilayer-1) &
                                                + x0(iGauss) * dzLay
        end do
        index = index +1
        optPropRTMGridS%RTMaltitudeSub(index) = optPropRTMGridS%RTMaltitude(ilayer)
        optPropRTMGridS%RTMweightSub  (index) = 0.0d0
    end do ! ilayer
  
    deallocate( x0, w0, STAT = deallocStatus )
    if ( deallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: deallocation failed')
      call logDebug('for x0(optPropRTMGridS%nGaussLay) or w0(optPropRTMGridS%nGaussLay)')
      call logDebug('in subroutine fillAltPresGridRTM')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

    if (verbose) then
      write(intermediateFileUnit, *) 
      write(intermediateFileUnit,'(A)') 'RTM altitude grids calculated in radianceIrradianceModule:fillAltPresGridRTM'
      write(intermediateFileUnit,'(A)') '   altitude        weight   for RTM grid'
      do ilayer = 0, optPropRTMGridS%RTMnlayer
         write(intermediateFileUnit,'(2F15.8)') optPropRTMGridS%RTMaltitude(ilayer), &
                                                optPropRTMGridS%RTMweight(ilayer)
      end do
      write(intermediateFileUnit,'(A)') '   altitude        weight   for RTM subgrid'
      do ilayer = 0, optPropRTMGridS%RTMnlayerSub
         write(intermediateFileUnit,'(2F15.8)') optPropRTMGridS%RTMaltitudeSub(ilayer), &
                                                optPropRTMGridS%RTMweightSub(ilayer)
      end do
      write(intermediateFileUnit, *) 
      write(intermediateFileUnit,'(A, 2F12.8)') 'sum of weights for RTM grid and subgrid', &
                            sum(optPropRTMGridS%RTMweight), sum(optPropRTMGridS%RTMweightSub)

    end if 

  end subroutine fillAltPresGridRTM


  subroutine fillRadianceDerivativesHRgrid(errS, geometryS, controlS, reflDerivHRS, &
                                           solarIrradianceS, earthRadianceS)

    ! fill the earth radiance and derivatives on the high resolution spectral grid
    ! by multiplying the solar irradiance with the reflectance and the factor geometryS%u0 * PI

    implicit none

    type(errorType),           intent(inout) :: errS
    type(geometryType),        intent(in)    :: geometryS         ! geometrical information
    type(controlType),         intent(in)    :: controlS          ! control parameters for radiative transfer (simulation)
    type(reflDerivType),       intent(in)    :: reflDerivHRS      ! reflectance(R) and derivatives(K) on HR grid
    type(solarIrrType),        intent(in)    :: solarIrradianceS  ! solar irradiance on HR and instrument grid
    type(earthRadianceType),   intent(inout) :: earthRadianceS    ! radiance and derivatives on HR and instrument grid

    ! local
    integer             :: istate, ilevel, iSV, iwave
    real(8), parameter  :: PI = 3.141592653589793d0

    ! contribution of scattering and absorption of sunlight to the earthRadiance
    ! is mu0 * R * SolIrr / PI with mu0 the cosine of the solar zenith angle (at surface)
    ! R is the reflectance and SolIrr is the solar irradiance 

    ! contribution to the radiance of surface emission is given by
    ! E * wfEmission / PI where E is the isotropic emission at the surface per unit area.

    do iSV = 1, controlS%dimSV
      earthRadianceS%rad_clr_HR(iSV,:)    = geometryS%u0 * reflDerivHRS%refl_clr(iSV,:)    * solarIrradianceS%solIrrMR(:) / PI
      earthRadianceS%rad_cld_HR(iSV,:)    = geometryS%u0 * reflDerivHRS%refl_cld(iSV,:)    * solarIrradianceS%solIrrMR(:) / PI
      earthRadianceS%rad_ns_HR(iSV,:)     = geometryS%u0 * reflDerivHRS%refl_ns(iSV,:)     * solarIrradianceS%solIrrMR(:) / PI
      earthRadianceS%rad_elas_HR(iSV,:)   = geometryS%u0 * reflDerivHRS%refl_elas(iSV,:)   * solarIrradianceS%solIrrMR(:) / PI
      earthRadianceS%rad_inelas_HR(iSV,:) = geometryS%u0 * reflDerivHRS%refl_inelas(iSV,:) * solarIrradianceS%solIrrMR(:) / PI
      earthRadianceS%rad_HR(iSV,:)        = geometryS%u0 * reflDerivHRS%refl(iSV,:)        * solarIrradianceS%solIrrMR(:) / PI
    end do ! iSV

    do ilevel = 0, earthRadianceS%RTMnlayer
      do iwave = 1, earthRadianceS%nwavelHR
        do iSV = 1,  controlS%dimSV
          earthRadianceS%contribRadHR(iSV, iwave, ilevel)   = geometryS%u0 &
              * reflDerivHRS%contribRefl(iSV,iwave,ilevel)  * solarIrradianceS%solIrrMR(iwave) / PI
          earthRadianceS%intFieldUpHR(iSV, iwave, ilevel)   = geometryS%u0 &
              * reflDerivHRS%intFieldUp(iSV,iwave,ilevel)   * solarIrradianceS%solIrrMR(iwave) / PI
          earthRadianceS%intFieldDownHR(iSV, iwave, ilevel) = geometryS%u0 &
              * reflDerivHRS%intFieldDown(iSV,iwave,ilevel) * solarIrradianceS%solIrrMR(iwave) / PI
        end do ! iSV
      end do ! iwave
    end do ! ilevel

    if ( controlS%calculateDerivatives ) then
      do istate = 1,  reflDerivHRS%nstate
        earthRadianceS%KHR_clr_lnvmr(:,istate) = geometryS%u0 * reflDerivHRS%K_clr_lnvmr(:,istate) &
                                               * solarIrradianceS%solIrrMR(:) / PI
        earthRadianceS%KHR_cld_lnvmr(:,istate) = geometryS%u0 * reflDerivHRS%K_cld_lnvmr(:,istate) &
                                               * solarIrradianceS%solIrrMR(:) / PI
        earthRadianceS%KHR_lnvmr(:,istate)     = geometryS%u0 * reflDerivHRS%K_lnvmr(:,istate) &
                                               * solarIrradianceS%solIrrMR(:) / PI
        earthRadianceS%KHR_vmr(:,istate)       = geometryS%u0 * reflDerivHRS%K_vmr  (:,istate) &
                                               * solarIrradianceS%solIrrMR(:) / PI
        earthRadianceS%KHR_ndens(:,istate)     = geometryS%u0 * reflDerivHRS%K_ndens(:,istate) &
                                               * solarIrradianceS%solIrrMR(:) / PI
      end do

      do istate = 1,  reflDerivHRS%nstateCol
        earthRadianceS%KHR_ndensCol(:,istate) = geometryS%u0 * reflDerivHRS%KHR_ndensCol(:,istate) &
                                              * solarIrradianceS%solIrrMR(:) / PI
      end do
    else
      earthRadianceS%KHR_clr_lnvmr = 0.0d0
      earthRadianceS%KHR_cld_lnvmr = 0.0d0
      earthRadianceS%KHR_lnvmr     = 0.0d0
      earthRadianceS%KHR_vmr       = 0.0d0
      earthRadianceS%KHR_ndens     = 0.0d0
      earthRadianceS%KHR_ndensCol  = 0.0d0
    end if

  end subroutine fillRadianceDerivativesHRgrid


  subroutine integrateSlitFunctionIrr(errS, controlS, wavelHRS, wavelInstrS, solarIrradianceS, &
                                      earthRadianceS, RRS_RingS)

    implicit none

    type(errorType),                   intent(inout) :: errS
    type(controlType),                 intent(in)    :: controlS          ! control parameters
    type(wavelHRType),                 intent(in)    :: wavelHRS          ! high/instr resolution wavelength grid
    type(wavelInstrType),              intent(in)    :: wavelInstrS       ! wavelength grid for (simulated) measurements
    type(solarIrrType),                intent(inout) :: solarIrradianceS  ! solar irradiance on HR and instrument grid
    type(earthRadianceType), optional, intent(inout) :: earthRadianceS    ! radiance and derivatives on HR and instrument grid
    type(RRS_RingType),      optional, intent(inout) :: RRS_RingS         ! Ring spectrum

    ! Integrate irradiance over slit function
    ! using Gaussian integration. Apply a wavelength shift for the slit function
    ! representing an error in the wavelength calibration of the instrument.

    ! local
    real(8)             :: slitfunctionValues(wavelHRS%nwavel)
    integer             :: iwave
    integer             :: startIndex, endIndex, index
    real(8)             :: irradiance, radRing
    real(8)             :: RingSmooth(wavelInstrS%nwavel)       ! smooth part of Ring spectrum - fitted polynomial

    logical, parameter  :: verbose    = .false.

    if ( controlS%ignoreSlit ) then
      solarIrradianceS%solIrr(:) = solarIrradianceS%solIrrMR(:)
    else
      ! convolute HR solar irradiance with slit function for solar irradiance
      do iwave = 1, wavelInstrS%nwavel
        slitfunctionValues = slitfunction(errS, wavelHRS, iwave, wavelInstrS%nwavel, wavelInstrS%wavel,  &
                                          solarIrradianceS%slitFunctionSpecsS, startIndex, endIndex)
        irradiance = 0.0d0
        do index = startIndex, endIndex
          ! multiply with gaussian weights for integration
          slitfunctionValues(index) = wavelHRS%weight(index) * slitfunctionValues(index)
          irradiance = irradiance + slitfunctionValues(index) * solarIrradianceS%solIrrHR(index)
        end do
        solarIrradianceS%solIrr(iwave) = irradiance
      end do ! iwave
     end if ! controlS%ignoreSlit

    if ( controlS%ignoreSlit ) then
    
      if ( present(RRS_RingS) ) then
        RRS_RingS%radRing(:) = RRS_RingS%radRingHR(:)
      end if
    
    else

      if ( present(RRS_RingS) .and. present(earthRadianceS) ) then
        ! convolute HR radiance Ring spectrum with slit function for earth radiance
        do iwave = 1, wavelInstrS%nwavel
          slitfunctionValues = slitfunction(errS, wavelHRS, iwave, wavelInstrS%nwavel, wavelInstrS%wavel,  &
                                            earthRadianceS%slitFunctionSpecsS, startIndex, endIndex)
          radRing     = 0.0d0
          do index = startIndex, endIndex
            ! multiply with gaussian weights for integration
            slitfunctionValues(index) = wavelHRS%weight(index) * slitfunctionValues(index)
            radRing = radRing + slitfunctionValues(index) * RRS_RingS%radRingHR(index)
           end do
          RRS_RingS%radRing(iwave) = radRing
        end do ! iwave

      ! calculate Ring spectrum on instrument grid by dividing the radiance Ring
      ! spectrum by the solar irradiance on the instrument grid
      RRS_RingS%Ring(:) = RRS_RingS%radRing(:) / solarIrradianceS%solIrr(:)
      ! calculate differential Ring spectrum on instrument grid
      call getSmoothAndDiffXsec(errS, RRS_RingS%degreePoly, wavelInstrS%nwavel, wavelInstrS%wavel, &
                                RRS_RingS%Ring, RingSmooth,  RRS_RingS%diffRing)
      if (errorCheck(errS)) return

      end if ! 

    end if ! controlS%ignoreSlit

    if ( verbose ) then
      if ( present(RRS_RingS) ) then 
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,'(A)') 'instrument resolution solar irradiance'
        write(intermediateFileUnit,'(A)') 'wavelength shifts have been applied but noise and offsets not'
        write(intermediateFileUnit,'(A)') '   wavel[nm]     solIrr        Ring         diffRing      radRing'
        do iwave = 1, wavelInstrS%nwavel
          write(intermediateFileUnit,'(F12.6,4ES14.5)') wavelInstrS%wavel(iwave),       &
                                                        solarIrradianceS%solIrr(iwave), &
                                                        RRS_RingS%Ring(iwave),          &
                                                        RRS_RingS%diffRing(iwave),      &
                                                        RRS_RingS%radRing(iwave)
        end do ! iwave
      else
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,'(A)') 'instrument resolution solar irradiance'
        write(intermediateFileUnit,'(A)') 'wavelength shifts have been applied but noise and offsets not'
        write(intermediateFileUnit,'(A)') '   wavel[nm]     solIrr'
        do iwave = 1, wavelInstrS%nwavel
          write(intermediateFileUnit,'(F12.6,ES14.5)') wavelInstrS%wavel(iwave), solarIrradianceS%solIrr(iwave)
        end do ! iwave
      end if ! present(RRS_RingS)
    end if ! verbose

  end subroutine integrateSlitFunctionIrr


  subroutine integrateSlitFunctionRad(errS, controlS, wavelHRS, wavelInstrS, retrS, solarIrradianceS, &
                                      earthRadianceS)

    implicit none

    type(errorType),           intent(inout) :: errS
    type(controlType),         intent(in)    :: controlS          ! control parameters for radiative transfer (simulation)
    type(wavelHRType),         intent(in)    :: wavelHRS          ! high resolution wavelength grid for simulation
    type(wavelInstrType),      intent(in)    :: wavelInstrS       ! wavelength grid for (simulated) measurements
    type(retrType),            intent(in)    :: retrS             ! retrieval parameters
    type(solarIrrType),        intent(in)    :: solarIrradianceS  ! solar irradiance on HR and instrument grid
    type(earthRadianceType),   intent(inout) :: earthRadianceS    ! radiance and derivatives on HR and instrument grid


    ! Integrate radiance over slit function
    ! using Gaussian integration. Apply a wavelength shift for the slit function
    ! representing an error in the wavelength calibration of the instrument.

    ! local
    real(8)             :: slitfunctionValues(wavelHRS%nwavel)
    integer             :: iwave, istate, ilevel, i
    integer             :: startIndex, endIndex, index
    integer             :: dimSV, iSV

    logical, parameter  :: verbose    = .false.
    logical, parameter  :: verboseSlitFunction    = .true.
    logical, parameter  :: verboseCol = .false.   ! write dL/dndens(zi) with L is radiance convoluted with slit
                                                  ! function, ndens is number density and zi the Gaussian points
                                                  ! on the column grid; repeated for all trace gases

    dimSV = controlS%dimSV

    if ( controlS%ignoreSlit ) then

      ! monochromatic mode: do not integrate over the slit function
      earthRadianceS%rad_clr   (:,:)       = earthRadianceS%rad_clr_HR   (:,:)
      earthRadianceS%rad_cld   (:,:)       = earthRadianceS%rad_cld_HR   (:,:)
      earthRadianceS%rad       (:,:)       = earthRadianceS%rad_HR       (:,:)
      earthRadianceS%rad_meas  (:,:)       = earthRadianceS%rad_HR       (:,:)
      earthRadianceS%rad_ns    (:,:)       = earthRadianceS%rad_ns_HR    (:,:)
      earthRadianceS%rad_elas  (:,:)       = earthRadianceS%rad_elas_HR  (:,:)
      earthRadianceS%rad_inelas(:,:)       = earthRadianceS%rad_inelas_HR(:,:)
      earthRadianceS%contribRad  (:, :, :) = earthRadianceS%contribRadHR(:,:,:)
      earthRadianceS%intFieldUp  (:, :, :) = earthRadianceS%intFieldUpHR(:,:,:)
      earthRadianceS%intFieldDown(:, :, :) = earthRadianceS%intFieldDown(:,:,:)
      if ( controlS%calculateDerivatives ) then
        earthRadianceS%K_clr_lnvmr(:,:) = earthRadianceS%KHR_clr_lnvmr(:,:)
        earthRadianceS%K_cld_lnvmr(:,:) = earthRadianceS%KHR_cld_lnvmr(:,:)
        earthRadianceS%K_lnvmr    (:,:) = earthRadianceS%KHR_lnvmr    (:,:)
        earthRadianceS%K_vmr      (:,:) = earthRadianceS%KHR_vmr      (:,:)
        earthRadianceS%K_ndens    (:,:) = earthRadianceS%KHR_ndens    (:,:)
        earthRadianceS%K_ndensCol (:,:) = earthRadianceS%KHR_ndensCol (:,:)
      else
        earthRadianceS%K_clr_lnvmr(:,:) = 0.0d0
        earthRadianceS%K_cld_lnvmr(:,:) = 0.0d0
        earthRadianceS%K_lnvmr    (:,:) = 0.0d0
        earthRadianceS%K_vmr      (:,:) = 0.0d0
        earthRadianceS%K_ndens    (:,:) = 0.0d0
        earthRadianceS%K_ndensCol (:,:) = 0.0d0
      end if ! controlS%calculateDerivatives

    else

      do iwave = 1, wavelInstrS%nwavel
        slitfunctionValues = slitfunction(errS, wavelHRS, iwave, wavelInstrS%nwavel, wavelInstrS%wavel,  &
                                          earthRadianceS%slitFunctionSpecsS, startIndex, endIndex)
        if ( verboseSlitFunction .and. iwave ==5 ) then
           write(addtionalOutputUnit,*)
           write(addtionalOutputUnit,'(A)') 'wavelength    slitfunctionValues'
           do i = startIndex, endIndex
             write(addtionalOutputUnit,'(2F15.9)') wavelHRS%wavel(i), slitfunctionValues(i)
           end do
           write(addtionalOutputUnit,*)
           write(addtionalOutputUnit,'(A, F10.4)') 'mean             ', earthRadianceS%slitFunctionSpecsS%mean
           write(addtionalOutputUnit,'(A, F10.4)') 'standardDeviation', earthRadianceS%slitFunctionSpecsS%standardDeviation
           write(addtionalOutputUnit,'(A, F10.4)') 'skewness         ', earthRadianceS%slitFunctionSpecsS%skewness
           write(addtionalOutputUnit,'(A, F10.4)') 'kurtosis         ', earthRadianceS%slitFunctionSpecsS%kurtosis
           write(addtionalOutputUnit,'(A, F10.4)') 'slitIntegrated   ', earthRadianceS%slitFunctionSpecsS%slitIntegrated
        end if
        ! multiply with gaussian weights for wavelength integration
        do index = startIndex, endIndex
          slitfunctionValues(index) = wavelHRS%weight(index) * slitfunctionValues(index)
        end do
        do iSV = 1, dimSV
          earthRadianceS%rad_clr(iSV,iwave)     = dot_product(earthRadianceS%rad_clr_HR(iSV,startIndex:endIndex),    &
                                                              slitfunctionValues(startIndex:endIndex) )
          earthRadianceS%rad_cld(iSV,iwave)     = dot_product(earthRadianceS%rad_cld_HR(iSV,startIndex:endIndex),    &
                                                              slitfunctionValues(startIndex:endIndex) )
          earthRadianceS%rad(iSV,iwave)         = dot_product(earthRadianceS%rad_HR(iSV,startIndex:endIndex),        &
                                                              slitfunctionValues(startIndex:endIndex) )
          earthRadianceS%rad_meas(iSV,iwave)    = earthRadianceS%rad(iSV,iwave)
          earthRadianceS%rad_ns(iSV,iwave)      = dot_product(earthRadianceS%rad_ns_HR(iSV,startIndex:endIndex),     &
                                                              slitfunctionValues(startIndex:endIndex) )
          earthRadianceS%rad_elas(iSV,iwave)    = dot_product(earthRadianceS%rad_elas_HR(iSV,startIndex:endIndex),   &
                                                              slitfunctionValues(startIndex:endIndex) )
          earthRadianceS%rad_inelas(iSV,iwave)  = dot_product(earthRadianceS%rad_inelas_HR(iSV,startIndex:endIndex), &
                                                              slitfunctionValues(startIndex:endIndex) )
        end do

        do ilevel = 0,  earthRadianceS%RTMnlayer
          do iSV = 1, dimSV
            earthRadianceS%contribRad(ISV, iwave, ilevel) = &
            dot_product(earthRadianceS%contribRadHR(iSV,startIndex:endIndex,ilevel), &
                        slitfunctionValues(startIndex:endIndex))
            earthRadianceS%intFieldUp(ISV, iwave, ilevel) = &
            dot_product(earthRadianceS%intFieldUpHR(iSV,startIndex:endIndex,ilevel), &
                        slitfunctionValues(startIndex:endIndex))
            earthRadianceS%intFieldDown(ISV, iwave, ilevel) = &
            dot_product(earthRadianceS%intFieldDownHR(iSV,startIndex:endIndex,ilevel), &
                        slitfunctionValues(startIndex:endIndex))
          end do
        end do
        if ( controlS%calculateDerivatives ) then
          do istate = 1, retrS%nstate
            earthRadianceS%K_clr_lnvmr(iwave,istate) = dot_product(                                              &
                                                       earthRadianceS%KHR_clr_lnvmr(startIndex:endIndex,istate), &
                                                       slitfunctionValues(startIndex:endIndex) )
            earthRadianceS%K_cld_lnvmr(iwave,istate) = dot_product(                                              &
                                                       earthRadianceS%KHR_cld_lnvmr(startIndex:endIndex,istate), &
                                                       slitfunctionValues(startIndex:endIndex) )
            earthRadianceS%K_lnvmr(iwave,istate)     = dot_product(                                              &
                                                       earthRadianceS%KHR_lnvmr(startIndex:endIndex,istate),     &
                                                       slitfunctionValues(startIndex:endIndex) )
            earthRadianceS%K_vmr  (iwave,istate)     = dot_product(                                              &
                                                       earthRadianceS%KHR_vmr(startIndex:endIndex,istate),       &
                                                       slitfunctionValues(startIndex:endIndex) )
            earthRadianceS%K_ndens(iwave,istate)     = dot_product(                                              &
                                                       earthRadianceS%KHR_ndens(startIndex:endIndex,istate),     &
                                                       slitfunctionValues(startIndex:endIndex) )
          end do
          do istate = 1, earthRadianceS%nstateCol
            earthRadianceS%K_ndensCol(iwave,istate)  = dot_product(                                              &
                                                       earthRadianceS%KHR_ndensCol(startIndex:endIndex,istate),  &
                                                       slitfunctionValues(startIndex:endIndex) ) 
          end do ! istate

        else
          earthRadianceS%K_clr_lnvmr(iwave,:) = 0.0d0
          earthRadianceS%K_cld_lnvmr(iwave,:) = 0.0d0
          earthRadianceS%K_lnvmr(iwave,:)     = 0.0d0
          earthRadianceS%K_vmr(iwave,:)       = 0.0d0
          earthRadianceS%K_ndens(iwave,:)     = 0.0d0
          earthRadianceS%K_ndensCol(iwave,:)  = 0.0d0
        end if
      end do ! iwave

    end if ! controlS%ignoreSlit

    if ( verbose .or. controlS%writeInstrResReflAndDeriv) then
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') 'instrument resolution solar irradiance, radiance and derivatives'
      write(addtionalOutputUnit,'(A)') 'the derivatives are dL/dln(vmr) where L = earth radiance '
      write(addtionalOutputUnit,'(A)') 'wavelength shifts (no for _ns) have been applied but noise and offsets not'
      write(addtionalOutputUnit,'(A, 300A14)') '   wavel[nm]     solIrr       radiance     radiance_ns', &
                                       ( trim(retrS%codeFitParameters(istate)), istate = 1, retrS%nstate )
      if ( controlS%ignoreSlit ) then
        do iwave = 1, wavelInstrS%nwavel
          write(addtionalOutputUnit,'(F12.6,300ES14.5)') wavelInstrS%wavel(iwave),          &
                                                         solarIrradianceS%solIrrMR(iwave),  &
                                                         earthRadianceS%rad(1,iwave),       &
                                                         earthRadianceS%rad_ns(1,iwave),    &
                  ( earthRadianceS%K_lnvmr(iwave,istate), istate = 1, earthRadianceS%nstate )
        end do ! iwave
      else
        do iwave = 1, wavelInstrS%nwavel
          write(addtionalOutputUnit,'(F12.6,300ES14.5)') wavelInstrS%wavel(iwave),          &
                                                         solarIrradianceS%solIrr(iwave),    &
                                                         earthRadianceS%rad(1,iwave),       &
                                                         earthRadianceS%rad_ns(1,iwave),    &
                  ( earthRadianceS%K_lnvmr(iwave,istate), istate = 1, earthRadianceS%nstate )
        end do ! iwave
      end if ! controlS%ignoreSlit

      if ( controlS%dimSV > 2 ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'wavelength  degree lin pol   (%)'
        do iwave = 1, wavelInstrS%nwavel
          write(addtionalOutputUnit,'(3F12.6)') wavelInstrS%wavel(iwave),                       &
                                          100.0d0 * sqrt(   earthRadianceS%rad(2,iwave)**2      &
                                                          + earthRadianceS%rad(3,iwave)**2 )    &
                                                       /  earthRadianceS%rad(1,iwave)
        end do ! iwave
      end if ! controlS%dimSV > 2
    end if

    if ( verboseCol ) then

      write(intermediateFileUnit,*)
      write(intermediateFileUnit,'(A)') 'd2L/dndens/dz on column grid after convolution with slit function'
      write(intermediateFileUnit,*)
      do iwave = 1, wavelInstrS%nwavel
        write(intermediateFileUnit,'(F8.3, 4X,200ES12.3)') wavelInstrS%wavel(iwave), &
                    (earthRadianceS%K_ndensCol(iwave,istate), istate = 1, earthRadianceS%nstateCol) 
      end do ! iwave

    end if

  end subroutine integrateSlitFunctionRad


  subroutine convoluteSlitFunction(errS, wavelMRS, wavelHRS, wavelSpec, slitFunctionSpecsS, spec, specConv)

    ! convolute a spectrum 'spec' with a slit function specified by 'slitFunctionSpecsS'
    ! the wavelengths for the spectrum are given by wavelSpec
    ! - first the spectrum 'spec' is cubic spline interpolated to the high resolution grid
    ! - then numerical integration is performed using the weights stored in wavelHRS

    implicit none

    type(errorType),        intent(inout) :: errS
    type(wavelHRType),      intent(in)    :: wavelMRS            ! high-instr resolution wavelength grid
    type(wavelHRType),      intent(in)    :: wavelHRS            ! high resolution wavelength grid
    type(slitFunctionType), intent(inout) :: slitFunctionSpecsS  ! slit function specification
    real(8),                intent(in)    :: wavelSpec(:)        ! wavelength grid for spectrum 'spec'
    real(8),                intent(in)    :: spec(:)             ! spectrum to be convoluted
    real(8),                intent(out)   :: specConv(:)         ! spectrum to be convoluted

    ! local
    real(8)    :: slitfunctionValues(wavelHRS%nwavel)
    integer    :: iwave, nwavel
    integer    :: startIndex, endIndex, index
    real(8)    :: convoluted, slitfunctionIntegrated

    nwavel = wavelMRS%nwavel
    ! convolute spectrum 'spec' with slit function
    do iwave = 1, nwavel
      slitfunctionValues = slitfunction(errS, wavelHRS, iwave, nwavel, wavelSpec,  &
                                        slitFunctionSpecsS, startIndex, endIndex)
      convoluted = 0.0d0
      slitfunctionIntegrated = 0.0d0
      do index = startIndex, endIndex
        ! multiply with gaussian weights for integration
        slitfunctionValues(index) = wavelHRS%weight(index) * slitfunctionValues(index)
        convoluted                = convoluted             + slitfunctionValues(index) * spec(index)
        slitfunctionIntegrated    = slitfunctionIntegrated + slitfunctionValues(index)
      end do
      specConv(iwave) = convoluted / slitfunctionIntegrated
    end do ! iwave

  end subroutine convoluteSlitFunction


  subroutine calculateRTM_Ring_spectra(errS, controlS, wavelInstrS, solarIrradianceS, earthRadianceS, RRS_RingS)

    ! calculate radiance Ring spectra from radiative transfer and store them in RRS_RingS
    ! fluorescence - if present -is included in the filling in 

    implicit none

    type(errorType),         intent(inout) :: errS
    type(controlType),       intent(in)    :: controlS          ! control parameters for radiative transfer (simulation)
    type(wavelInstrType),    intent(in)    :: wavelInstrS       ! wavelength grid for (simulated) measurements
    type(solarIrrType),      intent(in)    :: solarIrradianceS  ! solar irradiance on HR and instrument grid
    type(earthRadianceType), intent(in)    :: earthRadianceS    ! radiance and derivatives on HR and instrument grid
    type(RRS_RingType),      intent(inout) :: RRS_RingS         ! Ring spectrum

    ! local
    real(8)  :: RingSmooth(wavelInstrS%nwavel)       ! smooth part of Ring spectrum: fitted polynomial
    
    if ( RRS_RingS%useRRS .or.  RRS_RingS%approximateRRS) then
    ! high resolution spectra
      RRS_RingS%RTMradElasHR(:) = earthRadianceS%rad_elas_HR(1,:)
      RRS_RingS%RTMradRingHR(:) = earthRadianceS%rad_inelas_HR(1,:)
      RRS_RingS%RTMRingHR(:)    = earthRadianceS%rad_inelas_HR(1,:) / solarIrradianceS%solIrrMR(:)

      ! instrument resolution spectra
      RRS_RingS%RTMradElas(:) = earthRadianceS%rad_elas(1,:)
      RRS_RingS%RTMradRing(:) = earthRadianceS%rad_inelas(1,:)

      if (controlS%ignoreSlit ) then
        RRS_RingS%RTMRing(:)    = earthRadianceS%rad_inelas(1,:) / solarIrradianceS%solIrrMR(:)
      else
        RRS_RingS%RTMRing(:)    = earthRadianceS%rad_inelas(1,:) / solarIrradianceS%solIrr(:)
      end if

      ! differential instrument resolution spectrum
      call getSmoothAndDiffXsec(errS, RRS_RingS%degreePoly, wavelInstrS%nwavel, wavelInstrS%wavel, &
                                RRS_RingS%RTMRing, RingSmooth,  RRS_RingS%RTMdiffRing)
      if (errorCheck(errS)) return
    end if

    if ( RRS_RingS%useRRS ) then
    ! high resolution spectra
      RRS_RingS%RTM_FIHR(:)     = 100.0d0 * ( earthRadianceS%rad_HR(1,:) - earthRadianceS%rad_ns_HR(1,:) ) &
                                / earthRadianceS%rad_ns_HR(1,:) ! joiner definition: (Iinel - Iel) / Iel]
      !write(*,*) 'min, max rad_ns_HR =' , minval(earthRadianceS%rad_ns_HR(1,:) ),   maxval(  earthRadianceS%rad_ns_HR(1,:) )                     
                                
      ! instrument resolution spectra
      ! joiner definition: (Iinel - Iel)/ Iel in percent
      RRS_RingS%RTM_FI(:)     = 100.0d0 * ( earthRadianceS%rad(1,:) - earthRadianceS%rad_ns(1,:) ) &
                                / earthRadianceS%rad_ns(1,:) ! joiner definition: (Iinel - Iel) / Iel]
! JdH Dbug
      ! Spurr definition: (Iinel - Iel)/ Iinel in percent (makes little difference with Joiner definition)
      !RRS_RingS%RTM_FI(:)     = 100.0d0 * ( earthRadianceS%rad(1,:) - earthRadianceS%rad_ns(1,:) ) &
      !                          / earthRadianceS%rad(1,:)

    end if


  end subroutine calculateRTM_Ring_spectra


  subroutine integrateSlitFunctionRefSpec(errS, controlS, wavelHRS, wavelInstrS, earthRadianceS)

    implicit none

    type(errorType),           intent(inout) :: errS
    type(controlType),         intent(in)    :: controlS          ! control parameters for radiative transfer (simulation)
    type(wavelHRType),         intent(in)    :: wavelHRS          ! high resolution wavelength grid for simulation
    type(wavelInstrType),      intent(in)    :: wavelInstrS       ! wavelength grid for (simulated) measurements
    type(earthRadianceType),   intent(inout) :: earthRadianceS    ! earth radiance  and ref Spec on HR and instr grid

    ! Integrate radiance and if iteration == 0 also irradiance over slit function
    ! using Gaussian integration. Apply a wavelength shift for the slit function
    ! representing an error in the wavelength calibration of the instrument.

    ! local
    real(8)             :: slitfunctionValues(wavelHRS%nwavel)
    integer             :: iwave
    integer             :: startIndex, endIndex, index
    real(8)             :: radiance

    logical, parameter  :: verbose    = .false.

    if ( controlS%ignoreSlit ) then

      ! monochromatic mode: do not integrate over the slit function
      earthRadianceS%radianceRef(:) = earthRadianceS%radianceRefHR(:)

    else

      do iwave = 1, wavelInstrS%nwavel
        slitfunctionValues = slitfunction(errS, wavelHRS, iwave, wavelInstrS%nwavel, wavelInstrS%wavel, &
                                          earthRadianceS%slitFunctionSpecsS, startIndex, endIndex)
        radiance = 0.0d0
        do index = startIndex, endIndex
          ! multiply with gaussian weights for integration
          slitfunctionValues(index) = wavelHRS%weight(index) * slitfunctionValues(index)
          radiance = radiance + slitfunctionValues(index) * earthRadianceS%radianceRefHR(index)
        end do
        earthRadianceS%radianceRef(iwave) = radiance
      end do ! iwave

    end if ! controlS%ignoreSlit

    if ( verbose ) then
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,'(A)') 'instrument resolution earth radiance reference spectrum'
      write(intermediateFileUnit,'(A)') '   wavel[nm]     radianceRefSpec '
      do iwave = 1, wavelInstrS%nwavel
        write(intermediateFileUnit,'(F12.6,300ES14.5)') wavelInstrS%wavel(iwave),      &
                                                        earthRadianceS%radianceRef(iwave)

      end do ! iwave

    end if ! verbose

  end subroutine integrateSlitFunctionRefSpec


  subroutine calcReflAndDeriv(errS, maxFourierTermLUT, numSpectrBands, nTrace, wavelMRS, wavelHRS,              &
                              XsecHRS, RRS_RingS, geometryS,                                                    &
                              retrS, controlS, cloudAerosolRTMgridS, gasPTS, traceGasS, surfaceS, LambCloudS,   &
                              mieAerS, mieCldS, createLUTS, cldAerFractionS, mulOffsetS, strayLightS,           &
                              solarIrrS, optPropRTMGridS, dn_dnodeS, reflDerivHRS)

   ! calcuates the sun-normalized radiance (also called reflectance here) and the derivatives
   ! on a high-resolution spectral grid and stores the results in reflDerivHRS

    implicit none

    type(errorType),               intent(inout) :: errS
    integer,                       intent(in)    :: maxFourierTermLUT
    integer,                       intent(in)    :: numSpectrBands                  ! number of spectral bands
    integer,                       intent(in)    :: nTrace                          ! number of trace gases
    type(wavelHRType),             intent(in)    :: wavelMRS(numSpectrBands)        ! instr - high resolution wavelength grid
    type(wavelHRType),             intent(in)    :: wavelHRS(numSpectrBands)        ! high resolution wavelength grid
    type(XsecType),                intent(in)    :: XsecHRS(numSpectrBands,nTrace)  ! absorption cross section
    type(RRS_RingType),            intent(inout) :: RRS_RingS(numSpectrBands)       ! Raman / Ring information
    type(geometryType),            intent(in)    :: geometryS                       ! geometrical information
    type(retrType),                intent(in)    :: retrS                           ! contains info on the fit parameters
    type(controlType),             intent(inout) :: controlS                        ! control parameters for RTM
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS            ! cloud-aerosol properties
    type(gasPTType),               intent(inout) :: gasPTS                          ! atmospheric pressure and temperature
    type(traceGasType),            intent(inout) :: traceGasS(nTrace)               ! atmospheric trace gas properties
    type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)        ! surface properties
    type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)      ! Lambertian cloud properties
    type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)        ! specification expansion coefficients
    type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)        ! specification expansion coefficients
    type(createLUTType),           intent(inout) :: createLUTS(0:maxFourierTermLUT, numSpectrBands)
    type(cldAerFractionType),      intent(inout) :: cldAerFractionS(numSpectrBands) ! cloud / aerosol fraction
    type(mulOffsetType),           intent(inout) :: mulOffsetS(numSpectrBands)      ! multiplicative offset
    type(straylightType),          intent(inout) :: strayLightS(numSpectrBands)     ! stray light
    type(SolarIrrType),            intent(inout) :: solarIrrS(numSpectrBands)       ! solar irradiance
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS                 ! optical properties on RTM altitude grid
    type(dn_dnodeType),            intent(inout) :: dn_dnodeS(0:nTrace)             ! derivatives for spline interpolation
    type(reflDerivType),           intent(inout) :: reflDerivHRS(numSpectrBands)    ! reflectance and derivatives

    ! local
    type(reflDerivType) :: reflDeriv_CabannesHRS    ! reflectance and derivatives for Cabannes scattering
    type(reflDerivType) :: reflDeriv_RayleighHRS    ! reflectance and derivatives for Rayleigh scattering
    real(8), allocatable :: reflRaman    (:)        ! Raman reflectance
    real(8), allocatable :: reflRamanConv(:)        ! Raman reflectance convolutted with Raman lines
    real(8), allocatable :: Ring         (:)        ! Ring spectrum

    integer :: iband, allocstatus, sumAllocstatus
    real(8) :: theta, theta0    ! VZA and SZA used in mgeo
    real(8), parameter :: temperature = 250.0d0  ! temperature for RRS
    real(8), parameter :: altitude    = 30.0d0   ! altitude for mgeo: altitude of the ozone layer


    ! use line-by-line calculations
    do iband = 1, numSpectrBands

      if ( RRS_RingS(iband)%useRRS .or. createLUTS(0, iband)%reflectanceLUT) then
        call CalculateReflAndDeriv_lbl_RRS(errS, maxFourierTermLUT, iband, nTrace, wavelMRS(iband), wavelHRS(iband), &
                                           XsecHRS(iband,:), RRS_RingS(iband), geometryS, retrS, controlS,           &
                                           cloudAerosolRTMgridS, gasPTS, traceGasS, surfaceS(iband),                 &
                                           LambCloudS(iband), mieAerS, mieCldS, createLUTS(:,iband),                 &
                                           cldAerFractionS(iband), mulOffsetS(iband), strayLightS(iband),            &
                                           solarIrrS(iband), optPropRTMGridS, dn_dnodeS, reflDerivHRS(iband))
                                           
        if (errorCheck(errS)) return
      else ! useRRS

        if ( RRS_RingS(iband)%approximateRRS ) then
          sumAllocstatus = 0
          allocate( reflRaman    ( wavelMRS(iband)%nwavel), STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus
          allocate( reflRamanConv( wavelMRS(iband)%nwavel), STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus
          allocate( Ring( wavelMRS(iband)%nwavel), STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus

          ! set values for size of arrays
          reflDeriv_CabannesHRS%nwavel    = reflDerivHRS(iband)%nwavel
          reflDeriv_CabannesHRS%RTMnlayer = reflDerivHRS(iband)%RTMnlayer
          reflDeriv_CabannesHRS%nstate    = reflDerivHRS(iband)%nstate
          reflDeriv_CabannesHRS%nstateCol = reflDerivHRS(iband)%nstateCol
          reflDeriv_CabannesHRS%nTrace    = reflDerivHRS(iband)%nTrace
          reflDeriv_CabannesHRS%nGaussCol = reflDerivHRS(iband)%nGaussCol
          reflDeriv_CabannesHRS%dimSV     = reflDerivHRS(iband)%dimSV

          reflDeriv_RayleighHRS%nwavel    = reflDerivHRS(iband)%nwavel
          reflDeriv_RayleighHRS%RTMnlayer = reflDerivHRS(iband)%RTMnlayer
          reflDeriv_RayleighHRS%nstate    = reflDerivHRS(iband)%nstate
          reflDeriv_RayleighHRS%nstateCol = reflDerivHRS(iband)%nstateCol
          reflDeriv_RayleighHRS%nTrace    = reflDerivHRS(iband)%nTrace
          reflDeriv_RayleighHRS%nGaussCol = reflDerivHRS(iband)%nGaussCol
          reflDeriv_RayleighHRS%dimSV     = reflDerivHRS(iband)%dimSV

          call claimMemReflDerivS(errS, reflDeriv_CabannesHRS)
          if (errorCheck(errS)) return
          call claimMemReflDerivS(errS, reflDeriv_RayleighHRS)
          if (errorCheck(errS)) return

          sumAllocstatus = 0
          allocate( reflDeriv_CabannesHRS%KHR_ndensCol(reflDerivHRS(iband)%nwavel,reflDerivHRS(iband)%nstateCol), &
                    STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus

          allocate( reflDeriv_CabannesHRS%KHR_clr_ndensCol(reflDerivHRS(iband)%nwavel,reflDerivHRS(iband)%nstateCol), &
                    STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus

          allocate( reflDeriv_CabannesHRS%KHR_cld_ndensCol(reflDerivHRS(iband)%nwavel,reflDerivHRS(iband)%nstateCol), &
                    STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus

          allocate( reflDeriv_RayleighHRS%KHR_ndensCol(reflDerivHRS(iband)%nwavel,reflDerivHRS(iband)%nstateCol), &
                    STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus

          allocate( reflDeriv_RayleighHRS%KHR_clr_ndensCol(reflDerivHRS(iband)%nwavel,reflDerivHRS(iband)%nstateCol), &
                    STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus

          allocate( reflDeriv_RayleighHRS%KHR_cld_ndensCol(reflDerivHRS(iband)%nwavel,reflDerivHRS(iband)%nstateCol), &
                    STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus

          if ( sumAllocstatus /= 0 ) then
            call mystop (errS, 'allocation in subroutine calcReflAndDeriv failed')
            if (errorCheck(errS)) return
          end if

          RRS_RingS(iband)%useCabannes = .false.
          call CalculateReflAndDeriv_lbl (errS, iband, nTrace, wavelMRS(iband), XsecHRS(iband,:),                    &
                                          geometryS, retrS, controlS, cloudAerosolRTMgridS, gasPTS,                  &
                                          traceGasS, surfaceS(iband), LambCloudS(iband), mieAerS, mieCldS,           &
                                          cldAerFractionS(iband), RRS_RingS(iband),                                  &
                                          mulOffsetS(iband), strayLightS(iband), solarIrrS(iband),                   &
                                          optPropRTMGridS, dn_dnodeS, reflDeriv_RayleighHRS)
          if (errorCheck(errS)) return

          RRS_RingS(iband)%useCabannes = .true.
          call CalculateReflAndDeriv_lbl (errS, iband, nTrace, wavelMRS(iband), XsecHRS(iband,:),                    &
                                          geometryS, retrS, controlS, cloudAerosolRTMgridS, gasPTS,                  &
                                          traceGasS, surfaceS(iband), LambCloudS(iband), mieAerS, mieCldS,           &
                                          cldAerFractionS(iband), RRS_RingS(iband),                                  &
                                          mulOffsetS(iband), strayLightS(iband), solarIrrS(iband),                   &
                                          optPropRTMGridS, dn_dnodeS, reflDeriv_CabannesHRS)
          if (errorCheck(errS)) return

          ! set all values to those for Rayleigh scattering
          reflDerivHRS(iband)%bsca                = reflDeriv_RayleighHRS%bsca
          reflDerivHRS(iband)%babs                = reflDeriv_RayleighHRS%babs
          reflDerivHRS(iband)%depolarization      = reflDeriv_RayleighHRS%depolarization
          reflDerivHRS(iband)%refl                = reflDeriv_RayleighHRS%refl
          reflDerivHRS(iband)%refl_clr            = reflDeriv_RayleighHRS%refl_clr
          reflDerivHRS(iband)%refl_cld            = reflDeriv_RayleighHRS%refl_cld
          reflDerivHRS(iband)%refl_ns             = reflDeriv_RayleighHRS%refl_ns
          reflDerivHRS(iband)%refl_ns_clr         = reflDeriv_RayleighHRS%refl_ns_clr
          reflDerivHRS(iband)%refl_ns_cld         = reflDeriv_RayleighHRS%refl_ns_cld
          reflDerivHRS(iband)%refl_elas           = reflDeriv_RayleighHRS%refl_elas
          reflDerivHRS(iband)%refl_inelas         = reflDeriv_RayleighHRS%refl_inelas
          reflDerivHRS(iband)%refl_elas_clr       = reflDeriv_RayleighHRS%refl_elas_clr
          reflDerivHRS(iband)%refl_elas_cld       = reflDeriv_RayleighHRS%refl_elas_cld
          reflDerivHRS(iband)%refl_inelas_clr     = reflDeriv_RayleighHRS%refl_inelas_clr
          reflDerivHRS(iband)%refl_inelas_cld     = reflDeriv_RayleighHRS%refl_inelas_cld
          reflDerivHRS(iband)%contribRefl         = reflDeriv_RayleighHRS%contribRefl
          reflDerivHRS(iband)%intFieldUp          = reflDeriv_RayleighHRS%intFieldUp
          reflDerivHRS(iband)%intFieldDown        = reflDeriv_RayleighHRS%intFieldDown
          reflDerivHRS(iband)%polCorrection       = reflDeriv_RayleighHRS%polCorrection
          reflDerivHRS(iband)%altResAMFabs_clr    = reflDeriv_RayleighHRS%altResAMFabs_clr
          reflDerivHRS(iband)%altResAMFabs_cld    = reflDeriv_RayleighHRS%altResAMFabs_cld
          reflDerivHRS(iband)%altResAMFscaAer_clr = reflDeriv_RayleighHRS%altResAMFscaAer_clr
          reflDerivHRS(iband)%altResAMFscaAer_cld = reflDeriv_RayleighHRS%altResAMFscaAer_cld
          reflDerivHRS(iband)%K_lnvmr             = reflDeriv_RayleighHRS%K_lnvmr
          reflDerivHRS(iband)%K_clr_lnvmr         = reflDeriv_RayleighHRS%K_clr_lnvmr
          reflDerivHRS(iband)%K_cld_lnvmr         = reflDeriv_RayleighHRS%K_cld_lnvmr
          reflDerivHRS(iband)%K_vmr               = reflDeriv_RayleighHRS%K_vmr
          reflDerivHRS(iband)%K_clr_vmr           = reflDeriv_RayleighHRS%K_clr_vmr
          reflDerivHRS(iband)%K_cld_vmr           = reflDeriv_RayleighHRS%K_cld_vmr
          reflDerivHRS(iband)%K_ndens             = reflDeriv_RayleighHRS%K_ndens
          reflDerivHRS(iband)%K_clr_ndens         = reflDeriv_RayleighHRS%K_clr_ndens
          reflDerivHRS(iband)%K_cld_ndens         = reflDeriv_RayleighHRS%K_cld_ndens
          reflDerivHRS(iband)%KHR_ndensCol        = reflDeriv_RayleighHRS%KHR_ndensCol
          reflDerivHRS(iband)%KHR_clr_ndensCol    = reflDeriv_RayleighHRS%KHR_clr_ndensCol
          reflDerivHRS(iband)%KHR_cld_ndensCol    = reflDeriv_RayleighHRS%KHR_cld_ndensCol
          reflDerivHRS(iband)%KHR_ndensCol        = reflDeriv_RayleighHRS%KHR_ndensCol
          reflDerivHRS(iband)%KHR_clr_ndensCol    = reflDeriv_RayleighHRS%KHR_clr_ndensCol
          reflDerivHRS(iband)%KHR_cld_ndensCol    = reflDeriv_RayleighHRS%KHR_cld_ndensCol


          ! Calculate Raman reflectance
          reflRaman(:) = reflDeriv_RayleighHRS%refl(1,:) - reflDeriv_CabannesHRS%refl(1,:)
          
          ! convolute Raman reflectance with Raman lines, wiping out spectral structures
          call ConvoluteSpecRaman(errS, wavelMRS(iband)%wavel, temperature, reflRaman, reflRamanConv)
          if (errorCheck(errS)) return
          
          theta  = geometryS%vza
          theta0 = geometryS%sza

          if ( controlS%ignoreSlit ) then
            ! In fillRadianceDerivativesHRgrid the reflectance is multiplied with solarIrrS(iband)%solIrrMR(:)
            ! but we need multiplication with  solarIrrS(iband)%solIrr(:) for the inelastic part.
            ! The factor solarIrrS(iband)%solIrr(:) / solarIrrS(iband)%solIrrMR(:) compensates for this.
            Ring(:) = RRS_RingS(iband)%RingHR(:) * solarIrrS(iband)%solIrr(:) / solarIrrS(iband)%solIrrMR(:)
          else
            Ring(:) = RRS_RingS(iband)%RingHR(:)
          end if

          reflDerivHRS(iband)%refl_elas(:,:)   = reflDeriv_CabannesHRS%refl(:,:)
          reflDerivHRS(iband)%refl_inelas(1,:) = Ring(:)                                                              &
                                 * ( mgeo(theta,altitude) * reflRaman(:) + mgeo(theta0,altitude) * reflRamanConv(:) ) &
                                 / ( mgeo(theta,altitude) + mgeo(theta0,altitude) )
          reflDerivHRS(iband)%refl(:,:) =  reflDerivHRS(iband)%refl_elas(:,:) + reflDerivHRS(iband)%refl_inelas(:,:)

          ! note that freeMemReflDerivS also deallocates the arrays ...Col allocated above
          call freeMemReflDerivS(errS, reflDeriv_CabannesHRS)
          if (errorCheck(errS)) return
          call freeMemReflDerivS(errS, reflDeriv_RayleighHRS)
          if (errorCheck(errS)) return
          sumAllocstatus = 0
          deallocate( reflRaman, STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus
          deallocate( reflRamanConv, STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus
          deallocate( Ring, STAT = allocstatus )
          sumAllocstatus = sumAllocstatus + allocstatus

          if ( sumAllocstatus /= 0 ) then
            call mystop (errS, 'deallocation in subroutine calcReflAndDeriv failed')
            if (errorCheck(errS)) return
          end if

        else ! approximateRRS

          ! no correction for rotational Raman scattering
          call CalculateReflAndDeriv_lbl (errS, iband, nTrace, wavelMRS(iband),                      &
                                          XsecHRS(iband,:), geometryS, retrS, controlS,              &
                                          cloudAerosolRTMgridS, gasPTS, traceGasS, surfaceS(iband),  &
                                          LambCloudS(iband), mieAerS, mieCldS,                       &
                                          cldAerFractionS(iband), RRS_RingS(iband),                  &
                                          mulOffsetS(iband), strayLightS(iband), solarIrrS(iband),   &
                                          optPropRTMGridS, dn_dnodeS, reflDerivHRS(iband))
          if (errorCheck(errS)) return

        end if  ! approximateRRS
      end if ! useRRS

    end do ! iband

  end subroutine calcReflAndDeriv


  subroutine CalculateReflAndDeriv_lbl_RRS(errS, maxFourierTermLUT, iband, nTrace, wavelMRS,           &
                                           wavelHRS, XsecHRS, RRS_RingS, geometryS, retrS, controlS,   &
                                           cloudAerosolRTMgridS,gasPTS, traceGasS, surfaceS,           &
                                           LambCloudS, mieAerS, mieCldS, createLUTS, cldAerFractionS,  &
                                           mulOffsetS, strayLightS, solarIrrS, optPropRTMGridS,        &
                                           dn_dnodeS, reflDerivHRS)

    ! calculateReflAndDeriv_lbl calculates the reflectance and derivatives for one spectral band,
    ! using only elastic scattering and line-by-line calculations. The results are stored in reflDerivHRS
    ! where HR stands for high-resolution.

    implicit none

    type(errorType),               intent(inout) :: errS
    integer,                       intent(in)    :: maxFourierTermLUT
    integer,                       intent(in)    :: iband                     ! number wavelength band
    integer,                       intent(in)    :: nTrace                    ! number of trace gases
    type(wavelHRType),             intent(in)    :: wavelMRS                  ! instr - high resolution wavelength grid
    type(wavelHRType),             intent(in)    :: wavelHRS                  ! high resolution wavelength grid
    type(XsecType),                intent(in)    :: XsecHRS(nTrace)           ! altitude dependent absorption Xsections
    type(RRS_RingType),            intent(in)    :: RRS_RingS                 ! Raman / Ring information
    type(geometryType),            intent(in)    :: geometryS                 ! information on geometry
    type(retrType),                intent(in)    :: retrS                     ! contains info on the fit parameters
    type(controlType),             intent(inout) :: controlS                  ! control parameters for radiative transfer
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS      ! cloud-aerosol properties
    type(gasPTType),               intent(inout) :: gasPTS                    ! atmospheric pressure and temperature
    type(traceGasType),            intent(inout) :: traceGasS(nTrace)         ! atmospheric trace gas properties
    type(LambertianType),          intent(inout) :: surfaceS                  ! surface properties for a spectral band
    type(LambertianType),          intent(inout) :: LambCloudS                ! Lambertian cloud properties
    type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)  ! specification expansion coefficients
    type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)  ! specification expansion coefficients
    type(createLUTType),           intent(inout) :: createLUTS(0:maxFourierTermLUT)
    type(cldAerFractionType),      intent(inout) :: cldAerFractionS           ! cloud / aerosol  fraction
    type(mulOffsetType),           intent(inout) :: mulOffsetS                ! multiplicative offset
    type(straylightType),          intent(inout) :: strayLightS               ! stray light
    type(SolarIrrType),            intent(inout) :: solarIrrS                 ! solar irradiance
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS           ! optical properties on RTM grid
    type(dn_dnodeType),            intent(inout) :: dn_dnodeS(0:nTrace)       ! modify derivatives for spline interpolation
    type(reflDerivType),           intent(inout) :: reflDerivHRS              ! reflectance and derivatives

    ! local

    logical    :: cloudyPart
    integer    :: iwave, istate, iTrace, iFourier 
    integer    :: iSV, imu, imu0, ind, ilevel
    integer    :: statusRTM
    integer    :: nwavel, nmutot, nmuextra, nGauss, RTMnlayer, maxExpCoef, FourierMax

    real(8) :: cos_m_dphi         ! cosine( m * azimuth diff)
    real(8) :: sin_m_dphi         ! sine  ( m * azimuth diff)
    real(8) :: cs_m_dphi          ! cosine or sine  ( m * azimuth diff)
    real(8) :: factor

    integer :: dimSV, dimSV_fc ! dimension Stokes vector for the Fourier coefficients

    ! Reflectance and basic derivatives returned by layerBasedOrdersScattering
    ! note that the combination LambScaCloud has a Lambertian and scattering cloud
    ! as the Lambertian surface is at the top of the fit interval the weighting functions
    ! for cloud and aerosol particles are not needed in case. Therefore, only the
    ! weighting function for the cloud albedo, and scattering and absorption by gas are
    ! required for this combination.

    ! summed over Fourier coefficients
    real(8)       :: surfAlb_Spec(wavelMRS%nwavel)
    real(8)       :: surfEmission_Spec(wavelMRS%nwavel)
    real(8)       :: cloudAlb_Spec(wavelMRS%nwavel)

    real(8)       :: refl_Clear_Spec(wavelMRS%nwavel, controlS%dimSV)          ! elastic + inelastic
    real(8)       :: refl_Cloud_Spec(wavelMRS%nwavel, controlS%dimSV)          ! elastic + inelastic
    real(8)       :: refl_Clear_ns_Spec(wavelMRS%nwavel, controlS%dimSV)       ! elastic + not shifted inelastic     
    real(8)       :: refl_Cloud_ns_Spec(wavelMRS%nwavel, controlS%dimSV)       ! elastic + not shifted inelastic
    real(8)       :: refl_Clear_elas_Spec(wavelMRS%nwavel, controlS%dimSV)     ! elastic
    real(8)       :: refl_Cloud_elas_Spec(wavelMRS%nwavel, controlS%dimSV)     ! elastic
    real(8)       :: refl_Clear_inelas_Spec(wavelMRS%nwavel, controlS%dimSV)   ! inelastic
    real(8)       :: refl_Cloud_inelas_Spec(wavelMRS%nwavel, controlS%dimSV)   ! inelastic

    real(8)       :: contribRefl_Clear_Spec(wavelMRS%nwavel, controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: contribRefl_Cloud_Spec(wavelMRS%nwavel, controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)

    type(UDType)  :: UD_Clear_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    type(UDType)  :: UD_Cloud_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkscaGas_Clear_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaGas_Cloud_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkscaAerAbove_Clr_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaAerBelow_Clr_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaAerAbove_Cld_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaAerBelow_Cld_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkscaCldAbove_Clr_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaCldBelow_Clr_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaCldAbove_Cld_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaCldBelow_Cld_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkabs_Clear_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkabs_Cloud_Spec(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfSurfaceAlbedo_Clear_Spec(wavelMRS%nwavel)
    real(8)       :: wfSurfaceAlbedo_Cloud_Spec(wavelMRS%nwavel)
    real(8)       :: wfSurfaceEmission_Clear_Spec(wavelMRS%nwavel)
    real(8)       :: wfSurfaceEmission_Cloud_Spec(wavelMRS%nwavel)

    real(8)       :: wfCloudAlbedo_Spec(wavelMRS%nwavel)
    real(8)       :: wfCloudFraction_Spec(wavelMRS%nwavel)

    ! Fourier coefficients
    real(8)       :: refl_Clear_ns_Spec_fc(wavelMRS%nwavel, controlS%dimSV)     ! elastic + not shifted inelastic 
    real(8)       :: refl_Cloud_ns_Spec_fc(wavelMRS%nwavel, controlS%dimSV)     ! elastic + not shifted inelastic
    real(8)       :: refl_Clear_elas_Spec_fc(wavelMRS%nwavel, controlS%dimSV)   ! elastic
    real(8)       :: refl_Cloud_elas_Spec_fc(wavelMRS%nwavel, controlS%dimSV)   ! elastic
    real(8)       :: refl_Clear_inelas_Spec_fc(wavelMRS%nwavel, controlS%dimSV) ! inelastic
    real(8)       :: refl_Cloud_inelas_Spec_fc(wavelMRS%nwavel, controlS%dimSV) ! inelastic
    real(8)       :: refl_Clear_RRS_ns_Spec_fc(wavelMRS%nwavel)
    real(8)       :: refl_Cloud_RRS_ns_Spec_fc(wavelMRS%nwavel)
    real(8)       :: refl_Clear_RRS_Spec_fc   (wavelMRS%nwavel)
    real(8)       :: refl_Cloud_RRS_Spec_fc   (wavelMRS%nwavel)

    real(8)       :: contribRefl_Clear_Spec_fc(wavelMRS%nwavel, controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: contribRefl_Cloud_Spec_fc(wavelMRS%nwavel, controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)

    type(UDType)  :: UD_Clear_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    type(UDType)  :: UD_Cloud_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    type(UDType)  :: UD_Clear_Spec_conv_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    type(UDType)  :: UD_Cloud_Spec_conv_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkscaGas_Clear_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaGas_Cloud_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkscaAerAboveClr_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaAerBelowClr_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaAerAboveCld_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaAerBelowCld_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkscaCldAboveClr_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaCldBelowClr_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaCldAboveCld_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaCldBelowCld_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkabs_Clear_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkabs_Cloud_Spec_fc(wavelMRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfSurfaceAlbedo_Clear_Spec_fc(wavelMRS%nwavel)
    real(8)       :: wfSurfaceAlbedo_Cloud_Spec_fc(wavelMRS%nwavel)
    real(8)       :: wfSurfaceEmission_Clear_Spec_fc(wavelMRS%nwavel)
    real(8)       :: wfSurfaceEmission_Cloud_Spec_fc(wavelMRS%nwavel)

    real(8)       :: wfCloudAlbedo_Spec_fc(wavelMRS%nwavel)
    real(8)       :: wfCloudFraction_Spec_fc(wavelMRS%nwavel)

    real(8)       :: cf, cfMax, cfMin                   ! cloud fraction, maximal and minimal cloud fractions
 
    real(8), parameter :: PI = 3.141592653589793d0

    logical, parameter :: verbose = .false.

    ! initialise
    surfAlb_Spec(:)                       = 0.0d0
    surfEmission_Spec(:)                  = 0.0d0
    cloudAlb_Spec(:)                      = 0.0d0

    refl_Clear_Spec(:,:)                  = 0.0d0
    refl_Cloud_Spec(:,:)                  = 0.0d0
    refl_Clear_ns_Spec(:,:)               = 0.0d0
    refl_Cloud_ns_Spec(:,:)               = 0.0d0
    refl_Clear_elas_Spec(:,:)             = 0.0d0
    refl_Cloud_elas_Spec(:,:)             = 0.0d0
    refl_Clear_inelas_Spec(:,:)           = 0.0d0
    refl_Cloud_inelas_Spec(:,:)           = 0.0d0

    contribRefl_Clear_Spec(:,:,:)         = 0.0d0
    contribRefl_Cloud_Spec(:,:,:)         = 0.0d0

    wfInterfkscaGas_Clear_Spec(:,:)       = 0.0d0
    wfInterfkscaGas_Cloud_Spec(:,:)       = 0.0d0

    wfInterfkscaAerAbove_Clr_Spec(:,:)    = 0.0d0
    wfInterfkscaAerBelow_Clr_Spec(:,:)    = 0.0d0
    wfInterfkscaAerAbove_Cld_Spec(:,:)    = 0.0d0
    wfInterfkscaAerBelow_Cld_Spec(:,:)    = 0.0d0

    wfInterfkscaCldAbove_Clr_Spec(:,:)    = 0.0d0
    wfInterfkscaCldBelow_Clr_Spec(:,:)    = 0.0d0
    wfInterfkscaCldAbove_Cld_Spec(:,:)    = 0.0d0
    wfInterfkscaCldBelow_Cld_Spec(:,:)    = 0.0d0

    wfInterfkabs_Clear_Spec(:,:)          = 0.0d0
    wfInterfkabs_Cloud_Spec(:,:)          = 0.0d0

    wfSurfaceAlbedo_Clear_Spec(:)         = 0.0d0
    wfSurfaceAlbedo_Cloud_Spec(:)         = 0.0d0
    wfSurfaceEmission_Clear_Spec(:)       = 0.0d0
    wfSurfaceEmission_Cloud_Spec(:)       = 0.0d0

    wfCloudAlbedo_Spec(:)                 = 0.0d0
    wfCloudFraction_Spec(:)               = 0.0d0

    refl_Clear_ns_Spec_fc(:,:)            = 0.0d0
    refl_Cloud_ns_Spec_fc(:,:)            = 0.0d0
    refl_Clear_elas_Spec_fc(:,:)          = 0.0d0
    refl_Cloud_elas_Spec_fc(:,:)          = 0.0d0
    refl_Clear_inelas_Spec_fc(:,:)        = 0.0d0
    refl_Cloud_inelas_Spec_fc(:,:)        = 0.0d0

    contribRefl_Clear_Spec_fc(:,:,:)      = 0.0d0
    contribRefl_Cloud_Spec_fc(:,:,:)      = 0.0d0

    wfInterfkscaGas_Clear_Spec_fc(:,:)    = 0.0d0
    wfInterfkscaGas_Cloud_Spec_fc(:,:)    = 0.0d0

    wfInterfkscaAerAboveClr_Spec_fc(:,:)  = 0.0d0
    wfInterfkscaAerBelowClr_Spec_fc(:,:)  = 0.0d0
    wfInterfkscaAerAboveCld_Spec_fc(:,:)  = 0.0d0
    wfInterfkscaAerBelowCld_Spec_fc(:,:)  = 0.0d0

    wfInterfkscaCldAboveClr_Spec_fc(:,:)  = 0.0d0
    wfInterfkscaCldBelowClr_Spec_fc(:,:)  = 0.0d0
    wfInterfkscaCldAboveCld_Spec_fc(:,:)  = 0.0d0
    wfInterfkscaCldBelowCld_Spec_fc(:,:)  = 0.0d0

    wfInterfkabs_Clear_Spec_fc(:,:)       = 0.0d0
    wfInterfkabs_Cloud_Spec_fc(:,:)       = 0.0d0

    wfSurfaceAlbedo_Clear_Spec_fc(:)      = 0.0d0
    wfSurfaceAlbedo_Cloud_Spec_fc(:)      = 0.0d0
    wfSurfaceEmission_Clear_Spec_fc(:)    = 0.0d0
    wfSurfaceEmission_Cloud_Spec_fc(:)    = 0.0d0

    wfCloudAlbedo_Spec_fc(:)              = 0.0d0
    wfCloudFraction_Spec_fc(:)            = 0.0d0

    nwavel     = wavelMRS%nwavel
    nGauss     =  controlS%nStreams/2
    nmuextra   = 2
    nmutot     = nGauss + nmuextra
    RTMnlayer  = optPropRTMGridS%RTMnlayer
    maxExpCoef = optPropRTMGridS%maxExpCoef
    dimSV      = controlS%dimSV

    ! allocate and intialize
    call allocateUD_Spec(errS, nwavel, nmutot, controlS%dimSV, RTMnlayer, UD_Clear_Spec, UD_Cloud_Spec)
    if (errorCheck(errS)) return
    call allocateUD_Spec(errS, nwavel, nmutot, controlS%dimSV, RTMnlayer, UD_Clear_Spec_fc, UD_Cloud_Spec_fc)
    if (errorCheck(errS)) return
    call allocateUD_Spec(errS, nwavel, nmutot, controlS%dimSV, RTMnlayer, UD_Clear_Spec_conv_fc, UD_Cloud_Spec_conv_fc)
    if (errorCheck(errS)) return

    call fillAltPresGridRTM(errS, cloudAerosolRTMgridS, OptPropRTMGridS)
    if (errorCheck(errS)) return

    ! claim memory for U and D if a LUT is to be made, only if useRRS is .true.
    if ( RRS_RingS%useRRS ) then
      do iFourier = 0, maxFourierTermLUT
        if (createLUTS(iFourier)%reflectanceLUT .OR. createLUTS(iFourier)%correctionLUT ) then
          createLUTS(iFourier)%nlevel = optPropRTMGridS%RTMnlayer
          call claimMemCreateLUTS(errS, createLUTS(iFourier))
          if (errorCheck(errS)) return
        end if
      end do
    end if

    if ( controlS%calculateDerivatives ) then
      if ( (retrS%numIterations == 1 ) .OR. cloudAerosolRTMgridS%fitIntervalDP    &
                                       .OR. cloudAerosolRTMgridS%fitIntervalTop   &
                                       .OR. cloudAerosolRTMgridS%fitIntervalBot ) then
        ! calculate the change in a profile if the value at a node changes; index 0 is for air
        call calculate_dn_dnode(errS, gasPTS%npressureNodes, gasPTS%altNodes, optPropRTMGridS%RTMnlayerSub, &
                                optPropRTMGridS%RTMaltitudeSub, dn_dnodeS(0)%dn_dnode, gasPTS%useLinInterp)
        if (errorCheck(errS)) return
        do iTrace = 1, nTrace
          call calculate_dn_dnode(errS, traceGasS(iTrace)%nalt, traceGasS(iTrace)%alt, optPropRTMGridS%RTMnlayerSub, &
                     optPropRTMGridS%RTMaltitudeSub, dn_dnodeS(iTrace)%dn_dnode, traceGasS(iTrace)%useLinInterp)
          if (errorCheck(errS)) return
        end do
      end if
    end if ! controlS%calculateDerivatives

    ! calculate cfMax and cfMin these are used when 
    ! the derivatives for the cloudy part of the pixel are calculated
    if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
      cfMax = cloudAerosolRTMgridS%cldAerFractionAllBands
      cfMin = cloudAerosolRTMgridS%cldAerFractionAllBands
    else
      cfMin = maxCldFraction
      cfMax = minCldFraction
      do iwave = 1, wavelMRS%nwavel
        cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelMRS%wavel(iwave))
        if ( cf < cfMin ) cfMin = cf
        if ( cf > cfMax ) cfMax = cf
      end do
    end if

    ! determine the number of Fourier coefficients
    ! for perpendicular directions
    if ( ( (1.0d0 - geometryS%uu) < 1.0d-5 ) .or. ( (1.0d0 - geometryS%u0) < 1.0d-5 ) ) then 
      if ( dimSV == 1 ) then
        FourierMax = 0
      else
        FourierMax = 2    ! azimuth dependence due to reference plane Stokes parameters 
      end if
    else
      FourierMax = maxExpCoef
    end if

    ! START FOURIER LOOP

    do iFourier = 0, FourierMax

      if ( createLUTS(iFourier)%reflectanceLUT .or. createLUTS(iFourier)%correctionLUT ) then
          dimSV_fc = dimSV
          createLUTS(iFourier)%dimSV = dimSV
      else
        if ( iFourier > controlS%fourierFloorScalar ) then
          dimSV_fc = 1
        else
          dimSV_fc = dimSV
        end if
      end if

      ! calculate propertie for the cloudy part of the pixel when
      ! - cloud is present
      ! - at least one of the following is true: 
      !     * cloud fraction is above the threshold
      !     * cloud fraction is fitted
      if ( cloudAerosolRTMgridS%cloudpresent  .and.          &
         ( (cfMax > controlS%cloudFracThreshold)        .or. &
         cloudAerosolRTMgridS%fitCldAerFractionAllBands .or. & 
         cldAerFractionS%fitCldAerFraction ) ) then

        ! loop over wavelengths
        do iwave = 1, wavelMRS%nwavel

          ! set cloud fraction
          if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
            cf = cloudAerosolRTMgridS%cldAerFractionAllBands
          else
            cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelMRS%wavel(iwave))
          end if

          ! do calculations only for the cloudy part of the pixel when the cloud fraction is larger then 1.0
          if ( cf >= 1.0d0 ) then

            if ( cloudAerosolRTMgridS%useLambertianCloud ) then
              ! for a Lambertian cloud the optical properties of the atmosphere are the same as those
              ! for the cloud free part, except for the Lambertian cloud
              ! the Lambertian cloud is placed at a certain altitude in the atmosphere
              ! the index for the altitude on the RTM altitude grid is given by cloudAerosolRTMgridS%RTMnlevelCloud
              cloudyPart = .false. ! use propeties for the atmosphere without scattering cloud particles
              call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelMRS, XsecHRS, RRS_RingS,    &
                                  controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,       &
                                  mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                   &
                                  surfAlb_Spec(iwave), surfEmission_Spec(iwave),                    &
                                  cloudAlb_Spec(iwave), reflDerivHRS%depolarization(iwave))
              if (errorCheck(errS)) return

              call layerBasedOrdersScattering_fc(errS,    &
                iFourier, maxFourierTermLUT, dimSV,       &
                dimSV_fc, optPropRTMGridS, RRS_RingS,     & 
                controlS, createLUTS(iFourier),           &
                geometryS, iwave, wavelMRS,               & 
                cloudAlb_Spec(iwave),                     & 
                cloudAerosolRTMgridS%RTMnlevelCloud,      &   ! index for altitude Lambertian cloud
                reflDerivHRS%babs(iwave),                 &   ! output
                reflDerivHRS%bsca(iwave),                 &   ! output
                refl_Cloud_elas_Spec_fc(iwave,:),         &   ! output 
                contribRefl_Cloud_Spec_fc(iwave,:,:),     &   ! output
                UD_Cloud_Spec_fc(iwave,:),                &   ! output
                wfInterfkscaGas_Cloud_Spec_fc(iwave,:),   &   ! output 
                wfInterfkscaAerAboveCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaAerBelowCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaCldAboveCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaCldBelowCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkabs_Cloud_Spec_fc(iwave,:),      &   ! output
                wfCloudAlbedo_Spec_fc(iwave),             &   ! output 
                wfSurfaceEmission_Cloud_Spec_fc(iwave),   &   ! output
                statusRTM)
              if (errorCheck(errS)) return

            else ! cloudAerosolRTMgridS%useLambertianCloud

              ! use scattering cloud
              cloudyPart = .true. ! use atmospheric properties with a scattering cloud
              call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelMRS, XsecHRS, RRS_RingS, &
                                  controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,    &
                                  mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                &
                                  surfAlb_Spec(iwave), surfEmission_Spec(iwave),                 &
                                  cloudAlb_Spec(iwave), reflDerivHRS%depolarization(iwave))
              if (errorCheck(errS)) return
              ! the altitude of the Lambertian surface is now the ground surface which has level nr = 0
              call layerBasedOrdersScattering_fc(errS,    &
                iFourier, maxFourierTermLUT, dimSV,       &
                dimSV_fc, optPropRTMGridS, RRS_RingS,     & 
                controlS, createLUTS(iFourier),           &
                geometryS, iwave, wavelMRS,               & 
                surfAlb_Spec(iwave),                      &
                0,                                        &   ! index for Lambertian surface - here ground surface
                reflDerivHRS%babs(iwave),                 &   ! output
                reflDerivHRS%bsca(iwave),                 &   ! output
                refl_Cloud_elas_Spec_fc(iwave,:),         &   ! output 
                contribRefl_Cloud_Spec_fc(iwave,:,:),     &   ! output
                UD_Cloud_Spec_fc(iwave,:),                &   ! output
                wfInterfkscaGas_Cloud_Spec_fc(iwave,:),   &   ! output 
                wfInterfkscaAerAboveCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaAerBelowCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaCldAboveCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaCldBelowCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkabs_Cloud_Spec_fc(iwave,:),      &   ! output
                wfCloudAlbedo_Spec_fc(iwave),             &   ! output 
                wfSurfaceEmission_Cloud_Spec_fc(iwave),   &   ! output      
                statusRTM)
              if (errorCheck(errS)) return

            end if ! cloudAerosolRTMgridS%useLambertianCloud

            ! set values for the cloud-free part of the pixel to zero
            refl_Clear_elas_Spec_fc(iwave,:)          = 0.0d0
            contribRefl_Clear_Spec_fc(iwave,:,:)      = 0.0d0
            wfInterfkscaGas_Clear_Spec_fc(iwave,:)    = 0.0d0
            wfInterfkscaAerAboveClr_Spec_fc(iwave,:)  = 0.0d0
            wfInterfkscaAerBelowClr_Spec_fc(iwave,:)  = 0.0d0
            wfInterfkscaCldAboveClr_Spec_fc(iwave,:)  = 0.0d0
            wfInterfkscaCldBelowClr_Spec_fc(iwave,:)  = 0.0d0
            wfInterfkabs_Clear_Spec_fc(iwave,:)       = 0.0d0
            wfSurfaceAlbedo_Clear_Spec_fc(iwave)      = 0.0d0

          else ! cf >= 1.0d0

            ! do calculations for the clear and cloudy part of the pixel because cf < 1.0

            ! CLEAR PART

            cloudyPart = .false.
            call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelMRS, XsecHRS, RRS_RingS, &
                                controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,    &
                                mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                &
                                surfAlb_Spec(iwave), surfEmission_Spec(iwave),                 &
                                cloudAlb_Spec(iwave), reflDerivHRS%depolarization(iwave))
            if (errorCheck(errS)) return

            call layerBasedOrdersScattering_fc(errS,    &
              iFourier, maxFourierTermLUT, dimSV,       &
              dimSV_fc, optPropRTMGridS, RRS_RingS,     & 
              controlS, createLUTS(iFourier),           &
              geometryS, iwave, wavelMRS,               & 
              surfAlb_Spec(iwave),                      & 
              0,                                        &   ! index for Lambertian surface - here ground surface
              reflDerivHRS%babs(iwave),                 &   ! output
              reflDerivHRS%bsca(iwave),                 &   ! output
              refl_Clear_elas_Spec_fc(iwave,:),         &   ! output
              contribRefl_Clear_Spec_fc(iwave,:,:),     &   ! output
              UD_Clear_Spec_fc(iwave,:),                &   ! output
              wfInterfkscaGas_Clear_Spec_fc(iwave,:),   &   ! output 
              wfInterfkscaAerAboveClr_Spec_fc(iwave,:), &   ! output 
              wfInterfkscaAerBelowClr_Spec_fc(iwave,:), &   ! output 
              wfInterfkscaCldAboveClr_Spec_fc(iwave,:), &   ! output 
              wfInterfkscaCldBelowClr_Spec_fc(iwave,:), &   ! output 
              wfInterfkabs_Clear_Spec_fc(iwave,:),      &   ! output
              wfSurfaceAlbedo_Clear_Spec_fc(iwave),     &   ! output 
              wfSurfaceEmission_Clear_Spec_fc(iwave),   &   ! output 
              statusRTM)
            if (errorCheck(errS)) return

            ! CLOUDY PART

            ! distinguish between Lambertian and scattering cloud
            if ( cloudAerosolRTMgridS%useLambertianCloud ) then
              ! for a Lambertian cloud the optical properties of the atmosphere are the same as those
              ! for the cloud free part, except for the Lambertian cloud
              ! the Lambertian cloud is placed at a certain altitude in the atmosphere
              ! the index for the altitude on the RTM altitude grid is given by cloudAerosolRTMgridS%RTMnlevelCloud
              cloudyPart = .false. ! use propeties for a cloud-free atmosphere
              call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelMRS, XsecHRS, RRS_RingS, &
                                  controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,    &
                                  mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                &
                                  surfAlb_Spec(iwave), surfEmission_Spec(iwave),                 &
                                  cloudAlb_Spec(iwave), reflDerivHRS%depolarization(iwave))
              if (errorCheck(errS)) return

              call layerBasedOrdersScattering_fc(errS,    &
                iFourier, maxFourierTermLUT, dimSV,       &
                dimSV_fc, optPropRTMGridS, RRS_RingS,     & 
                controlS, createLUTS(iFourier),           &
                geometryS, iwave, wavelMRS,               & 
                cloudAlb_Spec(iwave),                     & 
                cloudAerosolRTMgridS%RTMnlevelCloud,      &   ! index for altitude Lambertian cloud
                reflDerivHRS%babs(iwave),                 &   ! output
                reflDerivHRS%bsca(iwave),                 &   ! output
                refl_Cloud_elas_Spec_fc(iwave,:),         &   ! output 
                contribRefl_Cloud_Spec_fc(iwave,:,:),     &   ! output
                UD_Cloud_Spec_fc(iwave,:),                &   ! output
                wfInterfkscaGas_Cloud_Spec_fc(iwave,:),   &   ! output 
                wfInterfkscaAerAboveCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaAerBelowCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaCldAboveCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaCldBelowCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkabs_Cloud_Spec_fc(iwave,:),      &   ! output
                wfCloudAlbedo_Spec_fc(iwave),             &   ! output
                wfSurfaceEmission_Cloud_Spec_fc(iwave),   &   ! output
                statusRTM)
              if (errorCheck(errS)) return

            else  ! cloudAerosolRTMgridS%useLambertianCloud

              ! use scattering cloud
              cloudyPart = .true. ! use atmospheric properties with a scattering cloud
              call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelMRS, XsecHRS, RRS_RingS, &
                                  controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,    &
                                  mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                &
                                  surfAlb_Spec(iwave), surfEmission_Spec(iwave),                 &
                                  cloudAlb_Spec(iwave), reflDerivHRS%depolarization(iwave))
              if (errorCheck(errS)) return
              ! the altitude of the Lambertian surface is now the ground surface which has level nr = 0
              call layerBasedOrdersScattering_fc(errS,    &
                iFourier, maxFourierTermLUT, dimSV,       &
                dimSV_fc, optPropRTMGridS, RRS_RingS,     & 
                controlS, createLUTS(iFourier),           &
                geometryS, iwave, wavelMRS,               & 
                surfAlb_Spec(iwave),                      & 
                0,                                        &   ! index for Lambertian surface - here ground surface
                reflDerivHRS%babs(iwave),                 &   ! output
                reflDerivHRS%bsca(iwave),                 &   ! output
                refl_Cloud_elas_Spec_fc(iwave,:),         &   ! output 
                contribRefl_Cloud_Spec_fc(iwave,:,:),     &   ! output
                UD_Cloud_Spec_fc(iwave,:),                &   ! output
                wfInterfkscaGas_Cloud_Spec_fc(iwave,:),   &   ! output 
                wfInterfkscaAerAboveCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaAerBelowCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaCldAboveCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkscaCldBelowCld_Spec_fc(iwave,:), &   ! output 
                wfInterfkabs_Cloud_Spec_fc(iwave,:),      &   ! output
                wfCloudAlbedo_Spec_fc(iwave),             &   ! output 
                wfSurfaceEmission_Cloud_Spec_fc(iwave),   &   ! output      
                statusRTM)
              if (errorCheck(errS)) return
            end if ! cloudAerosolRTMgridS%useLambertianCloud

          end if ! cf > 1.0

        end do ! iwave

      else ! cloudAerosolRTMgridS%cloudpresent .......

       ! calculations if no cloud is present and no derivatives w.r.t. the cloud fraction is needed

        cf = 0.0d0
        cloudyPart = .false.
        do iwave = 1, wavelMRS%nwavel
          call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelMRS, XsecHRS, RRS_RingS, &
                              controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,    &
                              mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                &
                              surfAlb_Spec(iwave), surfEmission_Spec(iwave),                 &
                              cloudAlb_Spec(iwave), reflDerivHRS%depolarization(iwave))
          if (errorCheck(errS)) return

          call layerBasedOrdersScattering_fc(errS,    &
            iFourier, maxFourierTermLUT, dimSV,       &
            dimSV_fc, optPropRTMGridS, RRS_RingS,     & 
            controlS, createLUTS(iFourier),           &
            geometryS, iwave, wavelMRS,               & 
            surfAlb_Spec(iwave),                      & 
            0,                                        &   ! index for Lambertian surface - here ground surface
            reflDerivHRS%babs(iwave),                 &   ! output
            reflDerivHRS%bsca(iwave),                 &   ! output
            refl_Clear_elas_Spec_fc(iwave,:),         &   ! output
            contribRefl_Clear_Spec_fc(iwave,:,:),     &   ! output
            UD_Clear_Spec_fc(iwave,:),                &   ! output
            wfInterfkscaGas_Clear_Spec_fc(iwave,:),   &   ! output 
            wfInterfkscaAerAboveClr_Spec_fc(iwave,:), &   ! output 
            wfInterfkscaAerBelowClr_Spec_fc(iwave,:), &   ! output 
            wfInterfkscaCldAboveClr_Spec_fc(iwave,:), &   ! output 
            wfInterfkscaCldBelowClr_Spec_fc(iwave,:), &   ! output 
            wfInterfkabs_Clear_Spec_fc(iwave,:),      &   ! output
            wfSurfaceAlbedo_Clear_Spec_fc(iwave),     &   ! output 
            wfSurfaceEmission_Clear_Spec_fc(iwave),   &   ! output 
            statusRTM)
          if (errorCheck(errS)) return

        end do ! iwave

      end if ! cloud present

      if ( RRS_RingS%useRRS .and. ( iFourier <=2 )) then

        call calcUD_conv(errS, nwavel, nmutot, 0, RTMnlayer, dimSV_fc, controlS, RRS_RingS, UD_Clear_Spec_fc, &
                         wavelMRS, wavelHRS, solarIrrS, optPropRTMGridS, UD_Clear_Spec_conv_fc)
        if (errorCheck(errS)) return
        call calcUD_conv(errS, nwavel, nmutot, 0, RTMnlayer, dimSV_fc, controlS, RRS_RingS, UD_Cloud_Spec_fc, &
                         wavelMRS, wavelHRS, solarIrrS, optPropRTMGridS, UD_Cloud_Spec_conv_fc)
        if (errorCheck(errS)) return

        call calcContribRRS(errS, nwavel, RTMnlayer, nmutot, nmuextra, nGauss, iFourier, dimSV_fc, maxExpCoef,  &
                            UD_Clear_Spec_fc, UD_Clear_Spec_conv_fc, wavelMRS, optPropRTMGridS, & 
                            geometryS, refl_Clear_RRS_ns_Spec_fc, refl_Clear_RRS_Spec_fc)
        if (errorCheck(errS)) return

        call calcContribRRS(errS, nwavel, RTMnlayer, nmutot, nmuextra, nGauss, iFourier, dimSV_fc, maxExpCoef,  &
                            UD_Cloud_Spec_fc, UD_Cloud_Spec_conv_fc, wavelMRS, optPropRTMGridS, & 
                            geometryS, refl_Cloud_RRS_ns_Spec_fc, refl_Cloud_RRS_Spec_fc)
        if (errorCheck(errS)) return
     
        
        refl_Clear_ns_Spec_fc(:,:)     = refl_Clear_elas_Spec_fc(:,:) 
        refl_Clear_elas_Spec_fc(:,1)   = refl_Clear_elas_Spec_fc(:,1) - refl_Clear_RRS_ns_Spec_fc(:)
        refl_Clear_inelas_Spec_fc(:,1) = refl_Clear_RRS_Spec_fc(:)
     
        refl_Cloud_ns_Spec_fc(:,:)     = refl_Cloud_elas_Spec_fc(:,:) 
        refl_Cloud_elas_Spec_fc(:,1)   = refl_Cloud_elas_Spec_fc(:,1) - refl_Cloud_RRS_ns_Spec_fc(:) 
        refl_Cloud_inelas_Spec_fc(:,1) = refl_Cloud_RRS_Spec_fc(:)
       
      end if ! controlS%useRRS .and. ( iFourier <=2 )

      ! sum over Fourier terms

      cos_m_dphi = cos(iFourier * geometryS%dphiRad )
      sin_m_dphi = sin(iFourier * geometryS%dphiRad )

      factor = 2.0d0
      if (iFourier == 0) factor = 1.0d0

      wfSurfaceAlbedo_Clear_Spec    = wfSurfaceAlbedo_Clear_Spec    + factor * wfSurfaceAlbedo_Clear_Spec_fc   * cos_m_dphi
      wfSurfaceAlbedo_Cloud_Spec    = wfSurfaceAlbedo_Cloud_Spec    + factor * wfSurfaceAlbedo_Cloud_Spec_fc   * cos_m_dphi
      wfSurfaceEmission_Clear_Spec  = wfSurfaceEmission_Clear_Spec  + factor * wfSurfaceEmission_Clear_Spec_fc * cos_m_dphi
      wfSurfaceEmission_Cloud_Spec  = wfSurfaceEmission_Cloud_Spec  + factor * wfSurfaceEmission_Cloud_Spec_fc * cos_m_dphi
      wfCloudAlbedo_Spec            = wfCloudAlbedo_Spec            + factor * wfCloudAlbedo_Spec_fc           * cos_m_dphi
      wfInterfkscaGas_Clear_Spec    = wfInterfkscaGas_Clear_Spec    + factor * wfInterfkscaGas_Clear_Spec_fc   * cos_m_dphi
      wfInterfkscaGas_Cloud_Spec    = wfInterfkscaGas_Cloud_Spec    + factor * wfInterfkscaGas_Cloud_Spec_fc   * cos_m_dphi
      wfInterfkscaAerAbove_Clr_Spec = wfInterfkscaAerAbove_Clr_Spec + factor * wfInterfkscaAerAboveClr_Spec_fc * cos_m_dphi
      wfInterfkscaAerAbove_Cld_Spec = wfInterfkscaAerAbove_Cld_Spec + factor * wfInterfkscaAerAboveCld_Spec_fc * cos_m_dphi
      wfInterfkscaAerBelow_Clr_Spec = wfInterfkscaAerBelow_Clr_Spec + factor * wfInterfkscaAerBelowClr_Spec_fc * cos_m_dphi
      wfInterfkscaAerBelow_Cld_Spec = wfInterfkscaAerBelow_Cld_Spec + factor * wfInterfkscaAerBelowCld_Spec_fc * cos_m_dphi
      wfInterfkscaCldAbove_Clr_Spec = wfInterfkscaCldAbove_Clr_Spec + factor * wfInterfkscaCldAboveClr_Spec_fc * cos_m_dphi
      wfInterfkscaCldAbove_Cld_Spec = wfInterfkscaCldAbove_Cld_Spec + factor * wfInterfkscaCldAboveCld_Spec_fc * cos_m_dphi
      wfInterfkscaCldBelow_Clr_Spec = wfInterfkscaCldBelow_Clr_Spec + factor * wfInterfkscaCldBelowClr_Spec_fc * cos_m_dphi
      wfInterfkscaCldBelow_Cld_Spec = wfInterfkscaCldBelow_Cld_Spec + factor * wfInterfkscaCldBelowCld_Spec_fc * cos_m_dphi
      wfInterfkabs_Clear_Spec       = wfInterfkabs_Clear_Spec       + factor * wfInterfkabs_Clear_Spec_fc      * cos_m_dphi
      wfInterfkabs_Cloud_Spec       = wfInterfkabs_Cloud_Spec       + factor * wfInterfkabs_Cloud_Spec_fc      * cos_m_dphi

      do iSV = 1, dimSV_fc
        if ( iSV > 2 ) then
          cs_m_dphi = sin_m_dphi
        else
          cs_m_dphi = cos_m_dphi
        end if
        refl_Clear_ns_Spec(:,iSV)   = refl_Clear_ns_Spec(:,iSV)   + factor * refl_Clear_ns_Spec_fc(:,iSV)   * cs_m_dphi
        refl_Cloud_ns_Spec(:,iSV)   = refl_Cloud_ns_Spec(:,iSV)   + factor * refl_Cloud_ns_Spec_fc(:,iSV)   * cs_m_dphi
        refl_Clear_elas_Spec(:,iSV) = refl_Clear_elas_Spec(:,iSV) + factor * refl_Clear_elas_Spec_fc(:,iSV) * cs_m_dphi
        refl_Cloud_elas_Spec(:,iSV) = refl_Cloud_elas_Spec(:,iSV) + factor * refl_Cloud_elas_Spec_fc(:,iSV) * cs_m_dphi
        contribRefl_Clear_Spec(:,iSV,:) = contribRefl_Clear_Spec(:,iSV,:) &
                                        + factor * contribRefl_Clear_Spec_fc(:,iSV,:) * cs_m_dphi
        contribRefl_Cloud_Spec(:,iSV,:) = contribRefl_Cloud_Spec(:,iSV,:) &
                                        + factor * contribRefl_Cloud_Spec_fc(:,iSV,:) * cs_m_dphi
        if ( RRS_RingS%useRRS ) then
          if ( iFourier <=2 ) then
            refl_Clear_inelas_Spec(:,iSV) = refl_Clear_inelas_Spec(:,iSV) + factor * refl_Clear_inelas_Spec_fc(:,iSV) * cs_m_dphi
            refl_Cloud_inelas_Spec(:,iSV) = refl_Cloud_inelas_Spec(:,iSV) + factor * refl_Cloud_inelas_Spec_fc(:,iSV) * cs_m_dphi
          end if
        end if
      end do ! iSV

      ! sum over Fourier terms of the internal radiation field
      if ( iFourier == 0 ) then
        do ilevel = 0, optPropRTMGridS%RTMnlayer
          do iwave = 1,  wavelMRS%nwavel
            UD_Clear_Spec(iwave, ilevel)%E(:) = UD_Clear_Spec_fc(iwave, ilevel)%E(:)
            UD_Cloud_Spec(iwave, ilevel)%E(:) = UD_Cloud_Spec_fc(iwave, ilevel)%E(:)
          end do ! iwave
        end do ! ilevel
      end if ! iFourier == 0
      
      do iSV = 1, dimSV_fc
        if ( iSV > 2 ) then
          cs_m_dphi = sin_m_dphi
        else
          cs_m_dphi = cos_m_dphi
        end if
        do ilevel = 0, optPropRTMGridS%RTMnlayer
          do iwave = 1,  wavelMRS%nwavel
            do imu0 = 1, 2
              do imu = 1, nmutot
                ind = 1 + (imu -1) * dimSV_fc
                UD_Clear_Spec(iwave, ilevel)%D(ind,imu0) = UD_Clear_Spec(iwave, ilevel)%D(ind,imu0) &
                                                         + factor *  UD_Clear_Spec_fc(iwave, ilevel)%D(ind,imu0) * cs_m_dphi
                UD_Clear_Spec(iwave, ilevel)%U(ind,imu0) = UD_Clear_Spec(iwave, ilevel)%U(ind,imu0) &
                                                         + factor *  UD_Clear_Spec_fc(iwave, ilevel)%U(ind,imu0) * cs_m_dphi
                UD_Cloud_Spec(iwave, ilevel)%D(ind,imu0) = UD_Cloud_Spec(iwave, ilevel)%D(ind,imu0) &
                                                         + factor *  UD_Cloud_Spec_fc(iwave, ilevel)%D(ind,imu0) * cs_m_dphi
                UD_Cloud_Spec(iwave, ilevel)%U(ind,imu0) = UD_Cloud_Spec(iwave, ilevel)%U(ind,imu0) &
                                                         + factor *  UD_Cloud_Spec_fc(iwave, ilevel)%U(ind,imu0) * cs_m_dphi
              end do ! imu
            end do ! imu0
          end do ! iwave
        end do ! ilevel
      end do ! iSV

    end do ! iFourier

    ! END OF FOURIER LOOP

    ! fill air mass factors for absorption the cloudy and cloud free part before adding surface emission
    ! to the reflectance
    do ilevel = 0, optPropRTMGridS%RTMnlayer
      if ( any( refl_Clear_Spec(:,1) < 1.0d-6 ) ) then
        reflDerivHRS%altResAMFabs_clr(:,ilevel) = 0.0d0
      else
        reflDerivHRS%altResAMFabs_clr(:,ilevel)    = - wfInterfkabs_Clear_Spec(:,ilevel) / refl_Clear_Spec(:,1)
      end if
      if ( any( refl_Cloud_Spec(:,1) < 1.0d-6 ) ) then
        reflDerivHRS%altResAMFabs_cld(:,ilevel) = 0.0d0
      else
        reflDerivHRS%altResAMFabs_cld(:,ilevel)    = - wfInterfkabs_Cloud_Spec(:,ilevel) / refl_Cloud_Spec(:,1)
      end if
    end do
    
    ! fill air mass factors for scattering by aerosol for the cloudy and cloud free part
    do ilevel = 0, optPropRTMGridS%RTMnlayer
      if ( any( refl_Clear_Spec(:,1) < 1.0d-6 ) ) then
        reflDerivHRS%altResAMFscaAer_clr(:,ilevel) = 0.0d0
      else
        reflDerivHRS%altResAMFscaAer_clr(:,ilevel) = wfInterfkscaAerAbove_Clr_Spec(:,ilevel) / refl_Clear_Spec(:,1)
      end if
      if ( any( refl_Cloud_Spec(:,1) < 1.0d-6 ) ) then
        reflDerivHRS%altResAMFscaAer_cld(:,ilevel) = 0.0d0
      else
        reflDerivHRS%altResAMFscaAer_cld(:,ilevel) = wfInterfkscaAerAbove_Cld_Spec(:,ilevel) / refl_Cloud_Spec(:,1)
      end if
    end do

    ! fill data structure reflDerivHRS - clear part
    do iSV = 1, controlS%dimSV
      reflDerivHRS%refl_ns_clr(iSV,:)     = refl_Clear_ns_Spec(:,iSV)
      reflDerivHRS%refl_elas_clr(iSV,:)   = refl_Clear_elas_Spec(:,iSV) 
      reflDerivHRS%refl_inelas_clr(iSV,:) = refl_Clear_inelas_Spec(:,iSV) 
      reflDerivHRS%refl_clr(iSV,:)        = reflDerivHRS%refl_elas_clr(iSV,:) + reflDerivHRS%refl_inelas_clr(iSV,:)
    end do

    ! add surface emission for the Stokes vector I
    reflDerivHRS%refl_inelas_clr(1,:) = 1.0d12 * surfEmission_Spec(:) * wfSurfaceEmission_Clear_Spec(:) * PI  &
                                      / geometryS%u0 / solarIrrS%solIrrMR(:)                                 &
                                      + reflDerivHRS%refl_inelas_clr(1,:)
    ! assume surface emission is a spectrally smooth function of wavelength so that RRS has no effect
    ! on emergent light => use Rayleigh scattering for the transmission through the atmosphere
    reflDerivHRS%refl_ns_clr(1,:)    = 1.0d12 * surfEmission_Spec(:) * wfSurfaceEmission_Clear_Spec(:) * PI  &
                                     / geometryS%u0 / solarIrrS%solIrrMR(:) + reflDerivHRS%refl_ns_clr(1,:)
    reflDerivHRS%refl_clr(1,:)       = reflDerivHRS%refl_elas_clr(1,:) + reflDerivHRS%refl_inelas_clr(1,:)

    ! fill data structure reflDerivHRS - cloud part
    do iSV = 1, controlS%dimSV
      reflDerivHRS%refl_ns_cld(iSV,:)     = refl_Cloud_ns_Spec(:,iSV)
      reflDerivHRS%refl_elas_cld(iSV,:)   = refl_Cloud_elas_Spec(:,iSV) 
      reflDerivHRS%refl_inelas_cld(iSV,:) = refl_Cloud_inelas_Spec(:,iSV) 
      reflDerivHRS%refl_cld(iSV,:)        = reflDerivHRS%refl_elas_cld(iSV,:) + reflDerivHRS%refl_inelas_cld(iSV,:)
    end do

    ! add surface emission for the Stokes vector I
    reflDerivHRS%refl_inelas_cld(1,:) = 1.0d12 * surfEmission_Spec(:) * wfSurfaceEmission_Cloud_Spec(:) * PI  &
                                      / geometryS%u0 / solarIrrS%solIrrMR(:)                                  &
                                      + reflDerivHRS%refl_inelas_cld(1,:)
    reflDerivHRS%refl_ns_cld(1,:)    = 1.0d12 * surfEmission_Spec(:) * wfSurfaceEmission_Cloud_Spec(:) * PI   &
                                     / geometryS%u0 / solarIrrS%solIrrMR(:) + reflDerivHRS%refl_ns_cld(1,:)
    reflDerivHRS%refl_cld(1,:)       = reflDerivHRS%refl_elas_cld(1,:) + reflDerivHRS%refl_inelas_cld(1,:)

    ! combine clear and cloud part
    do iwave = 1, wavelMRS%nwavel
      ! set cloud fraction
      if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
        cf = cloudAerosolRTMgridS%cldAerFractionAllBands
      else
        cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelMRS%wavel(iwave))
      end if
      if ( cf < 1.0d0 ) then
        do iSV = 1, controlS%dimSV
          reflDerivHRS%refl(iSV, iwave)    = cf * reflDerivHRS%refl_cld(iSV, iwave)         &
                                           + (1.0d0 - cf) * reflDerivHRS%refl_clr(iSV, iwave)
          reflDerivHRS%refl_ns(iSV, iwave) = cf * reflDerivHRS%refl_ns_cld(iSV, iwave)      &
                                           + (1.0d0 - cf) * reflDerivHRS%refl_ns_clr(iSV, iwave)
          if ( RRS_RingS%useRRS ) then
            reflDerivHRS%refl_elas(iSV, iwave)   = cf * reflDerivHRS%refl_elas_cld(iSV, iwave)            &
                                                 + (1.0d0 - cf) * reflDerivHRS%refl_elas_clr(iSV, iwave)
            reflDerivHRS%refl_inelas(iSV, iwave) = cf * reflDerivHRS%refl_inelas_cld(iSV, iwave)          &
                                                 + (1.0d0 - cf) * reflDerivHRS%refl_inelas_clr(iSV, iwave)
            reflDerivHRS%refl(iSV, iwave)        = reflDerivHRS%refl_elas(iSV, iwave) + reflDerivHRS%refl_inelas(iSV, iwave)
          end if ! RRS_RingS%useRRS
        end do ! iSV
      else
        do iSV = 1, controlS%dimSV
          reflDerivHRS%refl(iSV, iwave)    = cf * reflDerivHRS%refl_cld(iSV, iwave)
          reflDerivHRS%refl_ns(iSV, iwave) = cf * reflDerivHRS%refl_cld(iSV, iwave)
        end do
        if ( RRS_RingS%useRRS ) then
          do iSV = 1, controlS%dimSV
            reflDerivHRS%refl_elas(iSV, iwave)   = cf * reflDerivHRS%refl_elas_cld(iSV, iwave)
            reflDerivHRS%refl_inelas(iSV, iwave) = cf * reflDerivHRS%refl_inelas_cld(iSV, iwave)
            reflDerivHRS%refl(iSV, iwave)        = reflDerivHRS%refl_elas(iSV, iwave) + reflDerivHRS%refl_inelas(iSV, iwave)
          end do ! iSV
        end if ! RRS_RingS%useRRS
      end if ! cf < 1.0d0
    end do ! iwave

    ! fill contribution functiom and internal field for the entire pixel (emission does not contribute)
    do iSV = 1, controlS%dimSV
      imu = (iSV - 1) * geometryS%nmutot + geometryS%nmutot - 1
      imu0 = 2
      do ilevel = 0, reflDerivHRS%RTMnlayer
        do iwave = 1, wavelMRS%nwavel
          reflDerivHRS%contribRefl(iSV,iwave,ilevel)  = cf * contribRefl_Cloud_Spec(iwave,iSV,ilevel) &
                                                      + (1.0d0 - cf) * contribRefl_Clear_Spec(iwave,iSV,ilevel)
          reflDerivHRS%intFieldUp  (iSV,iwave,ilevel) = cf * UD_Cloud_Spec(iwave,ilevel)%U(imu, imu0) &
                                                      + (1.0d0 - cf) * UD_Clear_Spec(iwave,ilevel)%U(imu, imu0)
          reflDerivHRS%intFieldDown(iSV,iwave,ilevel) = cf * UD_Cloud_Spec(iwave,ilevel)%D(imu, imu0) &
                                                      + (1.0d0 - cf) * UD_Clear_Spec(iwave,ilevel)%D(imu, imu0)
        end do ! iwave
      end do ! ilevel
    end do ! iSV

    if ( controlS%calculateDerivatives ) then

      wfCloudFraction_Spec(:)  = reflDerivHRS%refl_cld(1,:) - reflDerivHRS%refl_clr(1,:)

      do iwave = 1, wavelMRS%nwavel
        cloudyPart = .false.
        call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelMRS, XsecHRS, RRS_RingS, &
                            controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,    &
                            mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                &
                            surfAlb_Spec(iwave), surfEmission_Spec(iwave),                 &
                            cloudAlb_Spec(iwave), reflDerivHRS%depolarization(iwave))
        if (errorCheck(errS)) return
        call calculate_K_clr(errS, iband, nTrace, traceGasS, gasPTS,                         &
                             strayLightS, solarIrrS, cloudAerosolRTMgridS,                   &
                             optPropRTMGridS, retrS, geometryS, wavelMRS, iwave, dn_dnodeS,  &
                             wfInterfkabs_Clear_Spec(iwave,:),                               &
                             wfInterfkscaGas_Clear_Spec(iwave,:),                            &
                             wfInterfkscaAerAbove_Clr_Spec(iwave,:),                         &
                             wfInterfkscaAerBelow_Clr_Spec(iwave,:),                         &
                             wfSurfaceAlbedo_Clear_Spec(iwave),                              &
                             wfSurfaceEmission_Clear_Spec(iwave),                            &
                             wfCloudFraction_Spec(iwave), reflDerivHRS)
        if (errorCheck(errS)) return
      end do ! iwave

      do iwave = 1, wavelMRS%nwavel
        ! distinguish between Lambertian and scattering cloud
        if ( cloudAerosolRTMgridS%useLambertianCloud ) then
          cloudyPart = .false. ! use atmospheric properties for a Lambertian cloud, i.e. the 
                               ! absorption properties are the same as for the cloud free part
        else
          cloudyPart = .true. ! use atmospheric properties with a scattering cloud
        end if
        call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelMRS, XsecHRS, RRS_RingS,  &
                            controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,     &
                            mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                 &
                            surfAlb_Spec(iwave), surfEmission_Spec(iwave),                  &
                            cloudAlb_Spec(iwave), reflDerivHRS%depolarization(iwave))
        if (errorCheck(errS)) return
        call calculate_K_cld(errS, iband, nTrace, cfMin, cfMax, traceGasS, gasPTS, LambCloudS,  &
                             strayLightS, solarIrrS, cloudAerosolRTMgridS,                      &
                             optPropRTMGridS, retrS, geometryS, wavelMRS, iwave, dn_dnodeS,     &
                             wfInterfkabs_Cloud_Spec(iwave,:),                                  &
                             wfInterfkscaGas_Cloud_Spec(iwave,:),                               &
                             wfInterfkscaAerAbove_Cld_Spec(iwave,:),                            &
                             wfInterfkscaAerBelow_Cld_Spec(iwave,:),                            &
                             wfInterfkscaCldAbove_Cld_Spec(iwave,:),                            &
                             wfInterfkscaCldBelow_Cld_Spec(iwave,:),                            &
                             wfSurfaceAlbedo_Cloud_Spec(iwave),                                 &
                             wfSurfaceEmission_Cloud_Spec(iwave), wfCloudAlbedo_Spec(iwave),    &
                             wfCloudFraction_Spec(iwave), reflDerivHRS)
        if (errorCheck(errS)) return
      end do ! iwave

    else ! controlS%calculateDerivatives
                  
      reflDerivHRS%K_clr_lnvmr(:,:)      = 0.0d0
      reflDerivHRS%K_cld_lnvmr(:,:)      = 0.0d0
      reflDerivHRS%K_clr_vmr(:,:)        = 0.0d0
      reflDerivHRS%K_cld_vmr(:,:)        = 0.0d0
      reflDerivHRS%K_clr_ndens(:,:)      = 0.0d0
      reflDerivHRS%K_cld_ndens(:,:)      = 0.0d0
      reflDerivHRS%KHR_clr_ndensCol(:,:) = 0.0d0
      reflDerivHRS%KHR_cld_ndensCol(:,:) = 0.0d0

    end if ! controlS%calculateDerivatives

    ! complete the calculation of derivatives for polynomials
    call setPolynomialDerivatives_clr(errS, iband, nTrace, wavelMRS, retrS, cloudAerosolRTMgridS, &
                                      surfaceS, LambCloudS, cldAerFractionS, mulOffsetS, strayLightS, reflDerivHRS)
    if (errorCheck(errS)) return
    call setPolynomialDerivatives_cld(errS, iband, nTrace, wavelMRS, retrS, cloudAerosolRTMgridS, &
                                      surfaceS, LambCloudS, cldAerFractionS, mulOffsetS, strayLightS, reflDerivHRS)
    if (errorCheck(errS)) return

    ! combine the derivatives for the clear and cloudy part
    do iwave = 1, wavelMRS%nwavel
      ! set cloud fraction
      if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
        cf = cloudAerosolRTMgridS%cldAerFractionAllBands
      else
        cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelMRS%wavel(iwave))
      end if
      do istate = 1, retrS%nstate
        reflDerivHRS%K_lnvmr(iwave, istate) = cf * reflDerivHRS%K_cld_lnvmr(iwave, istate)     &
                                            + (1.0d0 - cf) * reflDerivHRS%K_clr_lnvmr(iwave, istate) 
        reflDerivHRS%K_vmr  (iwave, istate) = cf * reflDerivHRS%K_cld_vmr(iwave, istate)       &
                                            + (1.0d0 - cf) * reflDerivHRS%K_clr_vmr(iwave, istate)
        reflDerivHRS%K_ndens(iwave, istate) = cf * reflDerivHRS%K_cld_ndens(iwave, istate)     &
                                            + (1.0d0 - cf) * reflDerivHRS%K_clr_ndens(iwave, istate)
      end do ! istate
      do istate = 1, reflDerivHRS%nstateCol
        reflDerivHRS%KHR_ndensCol(iwave, istate) = cf * reflDerivHRS%KHR_cld_ndensCol(iwave, istate)  &
                                                 + (1.0d0 - cf) * reflDerivHRS%KHR_clr_ndensCol(iwave, istate)
      end do ! istate
    end do ! iwave

    if ( verbose .or. controlS%writeHighResReflAndDeriv) then

      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') &
           ' reflectance on high resolution wavelength grid (not sun-normalized radiance)'
      write(addtionalOutputUnit,'(A)') '      wavel     bsca       babs    depolarization   Stokes parameters for reflectance '
      do iwave = 1, wavelMRS%nwavel
        write(addtionalOutputUnit,'(4F12.6,4E16.7)') wavelMRS%wavel(iwave),    &
                                                     reflDerivHRS%bsca(iwave), &
                                                     reflDerivHRS%babs(iwave), &
                                           reflDerivHRS%depolarization(iwave), &
                     ( reflDerivHRS%refl(iSV,iwave), iSV = 1, controlS%dimSV )
      end do ! iwave

      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') ' derivatives dR(1,1)/dx on high rsolution wavelength grid'
      write(addtionalOutputUnit,'(A, 20A16)') '   wavel    ', &
                      (trim(retrS%codeFitParameters(istate)), istate = 1, reflDerivHRS%nstate)
      do iwave = 1, wavelMRS%nwavel
        write(addtionalOutputUnit,'(F12.6,100E16.7)') wavelMRS%wavel(iwave),           &
              ( reflDerivHRS%K_lnvmr(iwave,istate), istate = 1, reflDerivHRS%nstate )
      end do
    end if

    ! clean up
    call deallocateUD_Spec(errS, wavelMRS%nwavel, optPropRTMGridS%RTMnlayer, UD_Clear_Spec, UD_Cloud_Spec)
    if (errorCheck(errS)) return
    call deallocateUD_Spec(errS, wavelMRS%nwavel, optPropRTMGridS%RTMnlayer, UD_Clear_Spec_fc, UD_Cloud_Spec_fc)
    if (errorCheck(errS)) return
    call deallocateUD_Spec(errS, wavelMRS%nwavel, optPropRTMGridS%RTMnlayer, &
                           UD_Clear_Spec_conv_fc, UD_Cloud_Spec_conv_fc)
    if (errorCheck(errS)) return

  end subroutine CalculateReflAndDeriv_lbl_RRS


  subroutine calcUD_conv(errS, nwavel, nmutot, startLevel, endLevel, dimSV_fc, controlS, RRS_RingS, UD_fc, &
                         wavelMRS, wavelHRS, solarIrradianceS, optPropRTMGridS, UD_fc_conv)

    ! convolute the internal field given by UD_fc with Raman lines
    ! and the solar spectrum. Next divide by the solar spectrum to get
    ! sun-normalized convoluted internal fields

    implicit none

    type(errorType),           intent(inout) :: errS
    integer(4),                intent(in)    :: nwavel, nmutot, startLevel, endLevel, dimSV_fc
    type(controlType),         intent(in)    :: controlS
    type(RRS_RingType),        intent(in)    :: RRS_RingS                 ! Raman / Ring information
    type(UDType),              intent(in)    :: UD_fc(nwavel, 0:endLevel)
    type(wavelHRType),         intent(in)    :: wavelMRS
    type(wavelHRType),         intent(in)    :: wavelHRS
    type(solarIrrType),        intent(inout) :: solarIrradianceS
    type(optPropRTMGridType),  intent(in)    :: optPropRTMGridS ! only the temperature is used

    type(UDType),            intent(inout) :: UD_fc_conv(nwavel, 0:endLevel)

    ! local
    integer(4)  :: imu, imu0, ilevel, index, iwave
    integer(4)  :: ispec, nspec, nwavelHR

    integer(4)  :: status

    real(8)              :: temperature, wavelength, normFact
    real(8)              :: wavelengthGrid(nwavel)
    real(8), allocatable :: spec(:,:), specConvRRSSlit(:,:)   ! (nwavel, nspec)
    real(8), allocatable :: specHR(:,:), specConvRRS(:,:)     ! (nwavelHR, nspec)
    integer(4)           :: allocStatus

    logical, parameter   :: verbose = .false.

! JdH: - first interpolate spectra in spec (using spline interpolation) to HR grid
!      - on HR grid multiply with HR solar irradiance
!      - then convolute with Raman lines => convoluted on HR grid
!      - then convolute with slit function => U and D and exp(-tau) on instr grid

    wavelengthGrid(:) = wavelMRS%wavel(:)

    nwavelHR = wavelHRS%nwavel

    nspec = dimSV_fc * nmutot * 5  ! U, D, and Etop

    allocate ( spec(nwavel, nspec), specConvRRSSlit(nwavel, nspec), specHR(nwavelHR, nspec), &
               specConvRRS(nwavelHR, nspec), STAT=allocStatus)

    if ( allocStatus /= 0 ) then
      call logDebug(' in calcUD_conv in radianceIrradianceModule allocation failed ')
      call mystop(errS, ' in calcUD_conv in radianceIrradianceModule allocation failed ')
      if (errorCheck(errS)) return
    end if

    ! convolute spectra having the same air temperature 
    do ilevel = startLevel, endLevel
  
      ! pack spectra (saves calculation time for the convolution)
      index = 0
      do imu = 1,  dimSV_fc * nmutot
        index = index + 1
        do iwave = 1, nwavel
          spec(iwave, index) = UD_fc(iwave, ilevel)%E(imu)
        end do
      end do
      do imu0 = 1, 2
        do imu = 1,  dimSV_fc * nmutot
          index = index + 1
          do iwave = 1, nwavel
            spec(iwave, index) = UD_fc(iwave, ilevel)%U(imu,imu0)
          end do
        end do
      end do
      do imu0 = 1, 2
        do imu = 1,  dimSV_fc * nmutot
          index = index + 1
          do iwave = 1, nwavel
            spec(iwave, index) = UD_fc(iwave, ilevel)%D(imu,imu0)
          end do
        end do
      end do

      if ( index /= nspec ) then
      call mystop(errS, ' in calcUD_conv in radianceIrradianceModule packing spectra failed')
      if (errorCheck(errS)) return
      end if

      if ( controlS%ignoreSlit ) then
       ! linear interpolate to get spectra on high resoltion wavelength grid
       ! note that in the UV spectra can vary strongly with wavelength => spline interpolation gives artefacts
        do ispec = 1, nspec
          do iwave = 1, nwavelHR
            specHR(iwave, ispec) = splintlin(errS, wavelengthGrid, spec(:,ispec), wavelHRS%wavel(iwave), status)
            ! prevent extrapolation
            if ( wavelHRS%wavel(iwave) < wavelengthGrid(1)      ) specHR(iwave, ispec) = spec(1, ispec)
            if ( wavelHRS%wavel(iwave) > wavelengthGrid(nwavel) ) specHR(iwave, ispec) = spec(nwavel, ispec)
          end do
        end do ! ispec
      else
        specHR = spec
      end if ! controlS%ignoreSlit

      ! mulitply interpolated HR spectra with HR solar irradiance
      do ispec = 1, nspec
        specHR(:, ispec) = specHR(:, ispec) *  solarIrradianceS%solIrrHR(:)
      end do ! ispec

      ! perform convolution with rotational Raman lines
      temperature = optPropRTMGridS%RTMtemperature(ilevel)
      call ConvoluteSpecRamanMS(errS, RRS_RingS%fractionRamanLines, wavelHRS%wavel, nspec, temperature, specHR, &
                                specConvRRS, status)
      if (errorCheck(errS)) return

      ! convolute spectra in 'specConvRRS' with the slit function if controlS%ignoreSlit = .true.
      if ( controlS%ignoreSlit ) then
        do ispec = 1, nspec
          ! In principle we need the slit function for the radiance here, but the slit function for the irradiance will do
          call convoluteSlitFunction(errS, wavelMRS, wavelHRS, wavelengthGrid, solarIrradianceS%slitFunctionSpecsS, &
                                     specConvRRS(:,ispec), specConvRRSSlit(:,ispec))
          if (errorCheck(errS)) return
        end do ! ispec
      else
        specConvRRSSlit = specConvRRS
      end if ! controlS%ignoreSlit

      ! divide packed convoluted spectra by the solar irradiance
      if ( controlS%ignoreSlit ) then
        do ispec = 1, nspec
          specConvRRSSlit(:, ispec) = specConvRRSSlit(:, ispec) /  solarIrradianceS%solIrr(:)
        end do ! ispec
      else
        do ispec = 1, nspec
          specConvRRSSlit(:, ispec) = specConvRRSSlit(:, ispec) /  solarIrradianceS%solIrrMR(:)
        end do ! ispec
      end if ! controlS%ignoreSlit

      ! unpack spectra
      index = 0
      do imu = 1,  dimSV_fc * nmutot
        index = index + 1
        do iwave = 1, nwavel
          UD_fc_conv(iwave, ilevel)%E(imu) = specConvRRSSlit(iwave, index)
        end do
      end do
      do imu0 = 1, 2
        do imu = 1,  dimSV_fc * nmutot
          index = index + 1
          do iwave = 1, nwavel
            UD_fc_conv(iwave, ilevel)%U(imu,imu0) = specConvRRSSlit(iwave, index)
          end do
        end do
      end do
      do imu0 = 1, 2
        do imu = 1,  dimSV_fc * nmutot
          index = index + 1
          do iwave = 1, nwavel
            UD_fc_conv(iwave, ilevel)%D(imu,imu0) = specConvRRSSlit(iwave, index)
          end do
        end do
      end do

    end do ! ilevel

    if ( verbose ) then
      write(intermediateFileUnit,*) 
      write(intermediateFileUnit,*) ' convolution with Raman lines '
      do ilevel = startLevel, endLevel
        temperature = optPropRTMGridS%RTMtemperature(ilevel)
        write(intermediateFileUnit,'(A,F8.3)') 'altitude (km) = ', optPropRTMGridS%RTMaltitude(ilevel)
        write(intermediateFileUnit,*) ' wavel         U          U_conv       D       D_conv '
        do iwave = 1, nwavel
          wavelength = wavelHRS%wavel(iwave)
          normFact = TotalRamanXsecScatWavel(temperature, wavelength) * 1.0d4    ! (Xsec in cm2)
          write(intermediateFileUnit, '(F10.4, 4F15.8)') wavelHRS%wavel(iwave),  &
                 UD_fc(iwave, ilevel)%U(nmutot-1,2), UD_fc_conv(iwave, ilevel)%U(nmutot-1,2)/normFact,  &
                 UD_fc(iwave, ilevel)%D(nmutot-1,2), UD_fc_conv(iwave, ilevel)%D(nmutot-1,2)/normFact
        end do ! iwave
      end do ! ilevel

    end if ! verbose

    ! clean up
    deallocate ( spec , specHR, specConvRRS, specConvRRSSlit )

  end subroutine calcUD_conv


  subroutine calcContribRRS(errS, nwavel, RTMnlayer, nmutot, nmuextra, nGauss, iFourier, dimSV_fc,  &
                            maxExpCoef, UDspec_fc, UDspec_fc_conv, wavelHRS, optPropRTMGridS, &
                            geometryS, refl_RRS_ns_spec_fc, refl_RRS_spec_fc)

    ! Calculate refl_RRS_ns_spec_fc (Raman scattering without wavelength shifts)
    ! and refl_RRS_spec_fc (Raman scattering with wavelength shifts)

    ! we do not actually add Raman scattering particles to the atmosphere, but want
    ! to know how much of the light is scrambled by Raman scattering. Hence, we should
    ! omit the terms related to attenuation, as the attenuation is not changed.

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                  intent(in)  :: nwavel, RTMnlayer
    integer,                  intent(in)  :: nmutot, nmuextra, nGauss
    integer,                  intent(in)  :: iFourier, dimSV_fc, maxExpCoef
    type(UDType),             intent(in)  :: UDspec_fc     (nwavel, 0:RTMnlayer)
    type(UDType),             intent(in)  :: UDspec_fc_conv(nwavel, 0:RTMnlayer)
    type(wavelHRType),        intent(in)  :: wavelHRS
    type(optPropRTMGridType), intent(in)  :: optPropRTMGridS
    type(geometryType),       intent(in)  :: geometryS

    real(8),                  intent(out) :: refl_RRS_ns_spec_fc(nwavel)
    real(8),                  intent(out) :: refl_RRS_spec_fc(nwavel)

    ! local

    integer(4) :: ilevel, iwave
    real(8)    :: temperature, wavelength

    real(8)    :: Zplus_prime(dimSV_fc*nmutot,dimSV_fc*nmutot),   Zmin_prime(dimSV_fc*nmutot,dimSV_fc*nmutot)

    real(8)    :: ZpE     (dimSV_fc*nmutot,nmuextra),    ZmE     (dimSV_fc*nmutot,nmuextra)
    real(8)    :: ZpE_conv(dimSV_fc*nmutot,nmuextra),    ZmE_conv(dimSV_fc*nmutot,nmuextra)
    real(8)    :: ZpD     (dimSV_fc*nmutot,nmuextra),    ZmD     (dimSV_fc*nmutot,nmuextra)
    real(8)    :: ZpD_conv(dimSV_fc*nmutot,nmuextra),    ZmD_conv(dimSV_fc*nmutot,nmuextra)
    real(8)    :: ZpU     (dimSV_fc*nmutot,nmuextra),    ZmU     (dimSV_fc*nmutot,nmuextra)
    real(8)    :: ZpU_conv(dimSV_fc*nmutot,nmuextra),    ZmU_conv(dimSV_fc*nmutot,nmuextra)

    real(8)    :: UT(nmuextra,dimSV_fc*nmutot), DT(nmuextra,dimSV_fc*nmutot), ET(dimSV_fc*nmutot)

    real(8)    :: ETZmE(nmuextra,nmuextra), ETZmE_conv(nmuextra,nmuextra)
    real(8)    :: ETZmD(nmuextra,nmuextra), ETZmD_conv(nmuextra,nmuextra)

    real(8)    :: DTZmE(nmuextra,nmuextra), DTZmE_conv(nmuextra,nmuextra)
    real(8)    :: DTZmD(nmuextra,nmuextra), DTZmD_conv(nmuextra,nmuextra)

    real(8)    :: ETZpU(nmuextra,nmuextra), ETZpU_conv(nmuextra,nmuextra)
    real(8)    :: DTZpU(nmuextra,nmuextra), DTZpU_conv(nmuextra,nmuextra)

    real(8)    :: UTZpE(nmuextra,nmuextra), UTZpE_conv(nmuextra,nmuextra)
    real(8)    :: UTZpD(nmuextra,nmuextra), UTZpD_conv(nmuextra,nmuextra)

    real(8)    :: UTZmU(nmuextra,nmuextra), UTZmU_conv(nmuextra,nmuextra)

    real(8)    :: sum_terms_ns(nmuextra,nmuextra)
    real(8)    :: sum_terms   (nmuextra,nmuextra)

    real(8)    :: XsecRRS

    type(fc_coefficients), pointer  :: fcCoef(:)

    character(LEN= 50) :: string

    character(LEN = 20) :: arrayName

    logical, parameter :: verbose = .false.

    ! only terms with iFourier < 3 can contribute
    if ( iFourier > 2 ) then
      refl_RRS_ns_spec_fc  = 0.0d0
      refl_RRS_spec_fc     = 0.0d0
      return
    end if

    ! initialize
    refl_RRS_ns_spec_fc  = 0.0d0
    refl_RRS_spec_fc     = 0.0d0

    ! allocate the elements of the structure 'fcCoef'
    call allocCoef(errS, fcCoef, nmutot, dimSV_fc, maxExpCoef)
    if (errorCheck(errS)) return

    ! fill generalized spherical function used to calculate the Fourier coefficient
    ! of the phase function / phase matrix for RRS.
    ! Note that there are at most three expansion coefficients (0, 1, and 2) 
    call fillPlmVector(errS, fcCoef, iFourier, dimSV_fc, nmutot, 2, geometryS)
    if (errorCheck(errS)) return

    ! fourier coefficients of the phase function for rotational Raman scattering
    call fillZplusZminprime(errS, fcCoef, iFourier, 2, dimSV_fc, nmutot,    &
                            optPropRTMGridS%phasefCoefRRS, geometryS, &
                            Zplus_prime, Zmin_prime, string)
    if (errorCheck(errS)) return

    do iwave = 1, nwavel
      do ilevel = 0, RTMnlayer

        ! calculate the product Zplus_prime Etop etc  nmuextra, 
        ZpE      = ZtimesE(dimSV_fc, nmutot, nGauss, nmuextra, Zplus_prime, UDspec_fc     (iwave,ilevel)%E )
        ZmE      = ZtimesE(dimSV_fc, nmutot, nGauss, nmuextra, Zmin_prime , UDspec_fc     (iwave,ilevel)%E )
        ZpE_conv = ZtimesE(dimSV_fc, nmutot, nGauss, nmuextra, Zplus_prime, UDspec_fc_conv(iwave,ilevel)%E )
        ZmE_conv = ZtimesE(dimSV_fc, nmutot, nGauss, nmuextra, Zmin_prime , UDspec_fc_conv(iwave,ilevel)%E )

        ! calculate the product Zplus_prime D etc
        ZpD      = ZtimesU(dimSV_fc, nmutot, nGauss, nmuextra, Zplus_prime, UDspec_fc     (iwave,ilevel)%D )
        ZmD      = ZtimesU(dimSV_fc, nmutot, nGauss, nmuextra, Zmin_prime , UDspec_fc     (iwave,ilevel)%D )
        ZpD_conv = ZtimesU(dimSV_fc, nmutot, nGauss, nmuextra, Zplus_prime, UDspec_fc_conv(iwave,ilevel)%D )
        ZmD_conv = ZtimesU(dimSV_fc, nmutot, nGauss, nmuextra, Zmin_prime , UDspec_fc_conv(iwave,ilevel)%D )

        ! calculate the product Zplus_prime U etc
        ZpU      = ZtimesU(dimSV_fc, nmutot, nGauss, nmuextra, Zplus_prime, UDspec_fc     (iwave,ilevel)%U )
        ZmU      = ZtimesU(dimSV_fc, nmutot, nGauss, nmuextra, Zmin_prime , UDspec_fc     (iwave,ilevel)%U )
        ZpU_conv = ZtimesU(dimSV_fc, nmutot, nGauss, nmuextra, Zplus_prime, UDspec_fc_conv(iwave,ilevel)%U )
        ZmU_conv = ZtimesU(dimSV_fc, nmutot, nGauss, nmuextra, Zmin_prime , UDspec_fc_conv(iwave,ilevel)%U )

        ! introduce shorthand notation for light traveling towards the sensor
        ET = UDspec_fc(iwave,ilevel)%E
        UT = transpose( apply_q3(dimSV_fc, nmutot, UDspec_fc(iwave,ilevel)%U) )
        DT = transpose( apply_q4(dimSV_fc, nmutot, UDspec_fc(iwave,ilevel)%D) )

        ETZmE = ETtimesU (dimSV_fc, nmutot, nGauss, nmuextra, ET, ZmE)
        ETZmD = ETtimesU (dimSV_fc, nmutot, nGauss, nmuextra, ET, ZmD)
        DTZmE = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, DT, ZmE)
        DTZmD = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, DT, ZmD)
        ETZpU = ETtimesU (dimSV_fc, nmutot, nGauss, nmuextra, ET, ZpU)
        DTZpU = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, DT, ZpU)
        UTZpE = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, UT, ZpE)
        UTZpD = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, UT, ZpD)
        UTZmU = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, UT, ZmU)

        ETZmE_conv = ETtimesU (dimSV_fc, nmutot, nGauss, nmuextra, ET, ZmE_conv)
        ETZmD_conv = ETtimesU (dimSV_fc, nmutot, nGauss, nmuextra, ET, ZmD_conv)
        DTZmE_conv = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, DT, ZmE_conv)
        DTZmD_conv = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, DT, ZmD_conv)
        ETZpU_conv = ETtimesU (dimSV_fc, nmutot, nGauss, nmuextra, ET, ZpU_conv)
        DTZpU_conv = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, DT, ZpU_conv)
        UTZpE_conv = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, UT, ZpE_conv)
        UTZpD_conv = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, UT, ZpD_conv)
        UTZmU_conv = UTtimesU (dimSV_fc, nmutot, nGauss, nmuextra, UT, ZmU_conv)

        sum_terms_ns = ETZmE + ETZmD + DTZmE + DTZmD + ETZpU + DTZpU + UTZpE + UTZpD + UTZmU
        ! multiply this sum of terms with the total RRS cross section
        ! as we need the cross section in cm2, not in m2, we need an additional factor 1.0d4
        temperature  = optPropRTMGridS%RTMtemperature(ilevel)
        wavelength   = wavelHRS%wavel(iwave)
        XsecRRS      = 1.0d4 * TotalRamanXsecScatWavel(temperature, wavelength)
        sum_terms_ns = sum_terms_ns * XsecRRS

        ! the cross section for RRS is accounted for when the convolution with the Raman
        ! lines takes place => no multiplication with XsecRRS for sum_terms
        sum_terms    = ETZmE_conv + ETZmD_conv + DTZmE_conv + DTZmD_conv              &
                     + ETZpU_conv + DTZpU_conv + UTZpE_conv + UTZpD_conv + UTZmU_conv

        ! number density is in molecules cm-3 ; cross section is in cm2 therefore the contribution
        ! is for a layer of 1 cm thickness. However, we want the contribution per km and
        ! multiply with 1.0d5.
        ! the weights are given per km
        refl_RRS_ns_spec_fc(iwave) = refl_RRS_ns_spec_fc(iwave) + optPropRTMGridS%RTMweight(ilevel)  * 1.0d5 &
                                                                * optPropRTMGridS%RTMndensAir(ilevel)        &
                                                                * sum_terms_ns(1, nmuextra)
        refl_RRS_spec_fc(iwave)    = refl_RRS_spec_fc(iwave)    + optPropRTMGridS%RTMweight(ilevel)  * 1.0d5 &
                                                                * optPropRTMGridS%RTMndensAir(ilevel)        &
                                                                * sum_terms   (1, nmuextra)

        if (verbose ) then
          if ( ilevel == 30 ) then
            if ( iwave == 100 ) then
              write(intermediateFileUnit,'(A, F10.4, 2I4)')  'wavelength iFourier dimSV_fc = ', wavelength, iFourier, dimSV_fc
              arrayName = 'UD_fc%D'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, XsecRRS*UDspec_fc(iwave,ilevel)%D)
              if (errorCheck(errS)) return
              arrayName = 'UD_fc_conv%D'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, UDspec_fc_conv(iwave,ilevel)%D)
              if (errorCheck(errS)) return
              arrayName = 'UD_fc%U'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, XsecRRS*UDspec_fc(iwave,ilevel)%U)
              if (errorCheck(errS)) return
              arrayName = 'UD_fc_conv%U'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, UDspec_fc_conv(iwave,ilevel)%U)
              if (errorCheck(errS)) return
              arrayName = 'ZpE'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, XsecRRS*ZpE)
              if (errorCheck(errS)) return
              arrayName = 'ZpE_conv'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, ZpE_conv)
              if (errorCheck(errS)) return
              arrayName = 'ZpD'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, XsecRRS*ZpD)
              if (errorCheck(errS)) return
              arrayName = 'ZpD_conv'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, ZpD_conv)
              if (errorCheck(errS)) return
              arrayName = 'ZmD'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, XsecRRS*ZmD)
              if (errorCheck(errS)) return
              arrayName = 'ZmD_conv'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, ZmD_conv)
              if (errorCheck(errS)) return
              arrayName = 'ZmE'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, XsecRRS*ZmE)
              if (errorCheck(errS)) return
              arrayName = ' ZmE_conv'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, ZmE_conv)
              if (errorCheck(errS)) return
              arrayName = 'ZmU'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, XsecRRS*ZmU)
              if (errorCheck(errS)) return
              arrayName = 'ZmU_conv'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, ZmU_conv)
              if (errorCheck(errS)) return
              arrayName = 'ZpU'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, XsecRRS*ZpU)
              if (errorCheck(errS)) return
              arrayName = 'ZpU_conv'
              call writeArray(errS, arrayName, ilevel, iwave, dimSV_fc*nmutot, nmuextra, ZpU_conv)
              if (errorCheck(errS)) return
              arrayName = 'ETZmE'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, XsecRRS*ETZmE)
              if (errorCheck(errS)) return
              arrayName = 'ETZmE_conv'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, ETZmE_conv)
              if (errorCheck(errS)) return
              arrayName = 'ETZmD'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, XsecRRS*ETZmD)
              if (errorCheck(errS)) return
              arrayName = 'ETZmD_conv'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, ETZmD_conv)
              if (errorCheck(errS)) return
              arrayName = 'DTZmE'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, XsecRRS*DTZmE)
              if (errorCheck(errS)) return
              arrayName = 'DTZmE_conv'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, DTZmE_conv)
              if (errorCheck(errS)) return
              arrayName = 'DTZmD'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, XsecRRS*DTZmD)
              if (errorCheck(errS)) return
              arrayName = 'DTZmD_conv'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, DTZmD_conv)
              if (errorCheck(errS)) return
              arrayName = 'ETZpU'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, XsecRRS*ETZpU)
              if (errorCheck(errS)) return
              arrayName = 'ETZpU_conv'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, ETZpU_conv)
              if (errorCheck(errS)) return
              arrayName = 'DTZpU'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, XsecRRS*ETZpU)
              if (errorCheck(errS)) return
              arrayName = 'DTZpU_conv'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, DTZpU_conv)
              if (errorCheck(errS)) return
              arrayName = 'UTZpE'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, XsecRRS*UTZpE)
              if (errorCheck(errS)) return
              arrayName = 'UTZpE_conv'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, UTZpE_conv)
              if (errorCheck(errS)) return
              arrayName = 'UTZpD'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, XsecRRS*UTZpD)
              if (errorCheck(errS)) return
              arrayName = 'UTZpD_conv'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, UTZpD_conv)
              if (errorCheck(errS)) return
              arrayName = 'UTZmU'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, XsecRRS*UTZmU)
              if (errorCheck(errS)) return
              arrayName = 'UTZmU_conv'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, UTZmU_conv)
              if (errorCheck(errS)) return
              arrayName = 'sum_terms_ns'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, sum_terms_ns)
              if (errorCheck(errS)) return
              arrayName = 'sum_terms'
              call writeArray(errS, arrayName, ilevel, iwave, nmuextra, nmuextra, sum_terms)
              if (errorCheck(errS)) return
            end if
          end if
        end if ! verbose
      end do ! ilevel
    end do ! iwave

    !if (verbose) then
    !  do iwave = 1, nwavel
    !    wavelength   = wavelHRS%wavel(iwave)
    !    write(intermediateFileUnit,'(A, I4, F12.5, 2F15.10)') 'refl_RRS_spec_fc(100) and refl_RRS_ns_spec_fc = ', &
    !                                   iFourier, wavelength, refl_RRS_spec_fc(iwave), refl_RRS_ns_spec_fc(iwave)
    !  end do ! iwave
    !end if ! verbose
    
    ! clean up
    call deallocCoef(errS, fcCoef, maxExpCoef)
    if (errorCheck(errS)) return

  end subroutine calcContribRRS


  pure function ZtimesE(dimSV_fc, nmutot, nGauss, nmuextra, Z, E)

    ! Z contains the Fourier coefficient of the phase function / matrix
    ! E contains the attenuation
    ! only the last two columns of Z are used

    implicit none

    integer, intent(in)  :: dimSV_fc, nmutot, nGauss, nmuextra
    real(8), intent(in)  :: E(dimSV_fc * nmutot)
    real(8), intent(in)  :: Z(dimSV_fc * nmutot, dimSV_fc * nmutot)
    real(8)              :: ZtimesE(dimSV_fc * nmutot, nmuextra)

    integer :: imu0, imu, ind0

    do imu0 = 1, nmuextra
      ind0 = dimSV_fc * (nGauss + imu0 - 1) + 1
      do imu =  1, dimSV_fc * nmutot
        ZtimesE(imu, imu0) = Z(imu, ind0) * E(ind0)
      end do
    end do

  end function ZtimesE


  function ETtimesU(dimSV_fc, nmutot, nGauss, nmuextra, E, U)

    ! ET contains the attenuation
    ! U contains the internal radiation
    ! only the last columns of U are used

    implicit none

    integer, intent(in)  :: dimSV_fc, nmutot, nGauss, nmuextra
    real(8), intent(in)  :: E(dimSV_fc * nmutot)
    real(8), intent(in)  :: U(dimSV_fc * nmutot, nmuextra)
    real(8)              :: ETtimesU(nmuextra, nmuextra)

    integer :: imu, imu0, ind

    do imu0 = 1, nmuextra
      do imu = 1, nmuextra 
        ind = dimSV_fc * (nGauss + imu0 - 1) + 1
        ETtimesU(imu, imu0) = E(ind) * U(ind, imu0)
      end do
    end do

  end function ETtimesU


  pure function ZtimesU(dimSV_fc, nmutot, nGauss, nmuextra, Z, U)

    ! Z contains the Fourier coefficient of the phase function / matrix
    ! U contains the internal radiation field
 
    implicit none

    integer, intent(in)  :: dimSV_fc, nmutot, nGauss, nmuextra
    real(8), intent(in)  :: Z(dimSV_fc * nmutot,dimSV_fc * nmutot)
    real(8), intent(in)  :: U(dimSV_fc * nmutot, nmuextra)
    real(8)              :: ZtimesU(dimSV_fc * nmutot, nmuextra)

    integer :: imu0, imu, ig

    do imu0 = 1, nmuextra
      do imu =  1, dimSV_fc * nmutot
        ZtimesU(imu, imu0) = 0.0d0
        do ig = 1, dimSV_fc * nGauss
          ZtimesU(imu, imu0) = ZtimesU(imu, imu0) + Z(imu, ig) * U(ig, imu0)
        end do
      end do
    end do

  end function ZtimesU


  pure function EtimesU(dimSV_fc, nmutot, nGauss, nmuextra, E, U)

    ! E contains the attenuation 

    implicit none

    integer, intent(in)  :: dimSV_fc, nmutot, nGauss, nmuextra
    real(8), intent(in)  :: E(dimSV_fc * nmutot)
    real(8), intent(in)  :: U(dimSV_fc * nmutot, nmuextra)
    real(8)              :: EtimesU(nmuextra, nmuextra)

    integer :: imu0, imu, ind

    do imu0 = 1, nmuextra
      do imu =  1, nmuextra
        ind = dimSV_fc * (nGauss + imu -1) + 1
        EtimesU(imu, imu0) = E(ind) * U(ind, imu0)
      end do
    end do

  end function EtimesU


  pure function UTtimesU(dimSV_fc, nmutot, nGauss, nmuextra, UT, U)

    ! UT contains the transposed of the internal radiation field
    ! U  contains the internal radiation field
 
    implicit none

    integer, intent(in)  :: dimSV_fc, nmutot, nGauss, nmuextra
    real(8), intent(in)  :: UT(nmuextra, dimSV_fc * nmutot)
    real(8), intent(in)  :: U (dimSV_fc * nmutot, nmuextra)
    real(8)              :: UTtimesU(nmuextra, nmuextra)

    integer :: imu0, imu, ig

    do imu0 = 1, nmuextra
      do imu =  1, nmuextra
        UTtimesU(imu, imu0) = 0.0d0
        do ig = 1, nGauss
          UTtimesU(imu, imu0) = UTtimesU(imu, imu0) + UT(imu, ig) * U(ig, imu0)
        end do
      end do
    end do

  end function UTtimesU


  subroutine writeArray(errS, arrayName, ilevel, iwave, nmutot, nmuextra, arrV)

    implicit none

      type(errorType), intent(inout) :: errS
    character(LEN = 20), intent(in) :: arrayName
    integer,             intent(in) :: ilevel, iwave, nmutot, nmuextra
    real(8),             intent(in) :: arrV(nmutot,nmutot)

    ! local
    integer  :: imu, imu0

    write(intermediateFileUnit,*) 
    write(intermediateFileUnit,'(A20,A,2I4)') arrayName, 'ilevel iwave = ', ilevel, iwave
    do imu = 1, nmutot
      write(intermediateFileUnit,'(50E15.5)') (arrV(imu, imu0), imu0 = 1, nmuextra)
    end do

  end subroutine writeArray


    subroutine fillZplusZminPrime(errS, fcCoef, iFourier, maxExpCoef, dimSV_fc, nmutot, phasefCoef,  &
                                  geometryS, Zplus_prime, Zmin_prime, string)

      ! fill the Fourier coefficients of the phase function or phase matrix for RRS
      ! using Zplus = D1 * sum[ (D2 * Plm(+u) * D1) * (D2* Sl * D2) * (D1 * Plm(+u) * D2) ] * D1 and
      !       Zmin  = D1 * sum[ (D2 * Plm(-u) * D1) * (D2* Sl * D2) * (D1 * Plm(+u) * D2) ] * D1

      ! the sum starts at the Fourier index iFourier and runs to maxExpCoef
      ! Assuming that there are 4 Stokes vectors ( dimSV_fc = 4) we have

      !       |  1    0    0   0  |              |  1   0     0   0  |
      !       |  0    1    1   0  |              |  0  1/2   1/2  0  |
      ! D1 =  |  0    1   -1   0  |       D2 =   |  0  1/2  -1/2  0  |
      !       |  0    0    0   1  |              |  0   0     0   1  |

      ! D1 * D2 = 1  and D2 * D1 = 1 where 1 is the unit matrix

      !                       | Plm_0(u)    0         0       0     |
      !                       |    0     Plm_-2(u)    0       0     |
      ! (D2 * Plm(u) * D1) =  |    0        0      Plm_+2(u)  0     |  = (D1 * Plm(u) * D2)
      !                       |    0        0         0    Plm_0(u) |

      ! and the expansion coefficients (D2 * Plm(u) * D2) are given by (index l omitted)

      !                  | alpha1       beta1/2             beta1/2           0     |
      !                  | beta1/2  (alpha2+alpha3)/4  (alpha2-alpha3)/4   beta2/2  |
      ! (D2* Sl * D2) =  | beta1/2  (alpha2-alpha3)/4  (alpha2+alpha3)/4  -beta2/2  |
      !                  |    0        -beta2/2             beta2/2        alpha4   |

      ! note that the factor (-1)**iFourier is omitted because the imaginary parts have been ignored
      ! when calculating the generalized spherical functions Plm_0(u), Plm_-2(u), and Plm_+2(u).

      ! The values of the expansion coefficients are stored in phasefCoef
      ! as a (dimSV, dimSV) sub matrix and for dimSV = 4 we have (index l omitted)

      !      | alpha1  beta1    0       0    |
      !      | beta1  alpha2    0       0    |
      ! S =  |    0      0    alpha3  beta2  |
      !      |    0      0   -beta2   alpha4 |

      ! For dimSV_fc = 1 (no polarization) this reduces to
      !  Zplus = sum[ Plm_0(+u) * alpha1 * Plm_0(+u)]
      !  Zplus = sum[ Plm_0(-u) * alpha1 * Plm_0(+u)]


      implicit none

      ! the values of D2 * Plm(u) * D1 are stored in the one dimensional arrays
      !    fcCoef(l)%PlmPlus(dimSV_fc*nmutot)  and
      !    fcCoef(l)%PlmPlus(dimSV_fc*nmutot)
      ! where l = 0, 1, ...maxExpCoef
 
      type(errorType), intent(inout) :: errS
      type(fc_coefficients), pointer     :: fcCoef(:)
      integer,               intent(in)  :: iFourier, maxExpCoef, dimSV_fc, nmutot
      real(8),               intent(in)  :: phasefCoef(dimSV_fc,dimSV_fc, 0:maxExpCoef)
      type(geometryType),    intent(in)  :: geometryS
      real(8),               intent(out) :: Zplus_prime(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8),               intent(out) :: Zmin_prime (dimSV_fc*nmutot,dimSV_fc*nmutot)
      character(LEN= 50), optional, intent(in)  :: string

      ! local
      integer :: iCoef, imu, imu0, iSV, jSV, ind, ind0
      real(8) :: D1(dimSV_fc, dimSV_fc)
      real(8) :: D2_S_D2(dimSV_fc, dimSV_fc)
      real(8) :: weight, fourmumu0

      logical, parameter :: verbose = .false.


      ! initialize
      Zplus_prime = 0.0d0
      Zmin_prime  = 0.0d0

      select case (dimSV_fc)

       case(1)

          do iCoef = iFourier, maxExpCoef
           do imu0 = 1, nmutot
             do imu  = 1, nmutot
               Zplus_prime(imu,imu0) = Zplus_prime(imu,imu0) + phasefCoef( 1, 1, iCoef)  &
                                     * fcCoef(iCoef)%PlmPlus(imu) * fcCoef(iCoef)%PlmPlus(imu0)
               Zmin_prime (imu,imu0) = Zmin_prime(imu,imu0)  + phasefCoef( 1, 1, iCoef)  &
                                     * fcCoef(iCoef)%PlmMin(imu)  * fcCoef(iCoef)%PlmPlus(imu0)
             end do ! imu
           end do ! imuo
         end do ! iCoef

       case(3)

         do iCoef = iFourier, maxExpCoef
           D2_S_D2( 1, 1) =   phasefCoef( 1, 1, iCoef)
           D2_S_D2( 2, 1) =   0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 3, 1) =   0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 1, 2) =   0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 1, 3) =   0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 2, 2) =   0.25d0 * ( phasefCoef( 2, 2, iCoef) + phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 3, 3) =   0.25d0 * ( phasefCoef( 2, 2, iCoef) + phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 3, 2) =   0.25d0 * ( phasefCoef( 2, 2, iCoef) - phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 2, 3) =   0.25d0 * ( phasefCoef( 2, 2, iCoef) - phasefCoef( 3, 3, iCoef) )
           do imu0 = 1, nmutot
             do jSV = 1, dimSV_fc
               ind0 = jSV + (imu0 - 1) * dimSV_fc
               do imu  = 1, nmutot
                 do iSV = 1, dimSV_fc
                   ind = iSV + (imu - 1) * dimSV_fc
                   Zplus_prime(ind,ind0) = Zplus_prime(ind,ind0) + D2_S_D2( iSV, jSV)  &
                                         * fcCoef(iCoef)%PlmPlus(ind) * fcCoef(iCoef)%PlmPlus(ind0)
                   Zmin_prime (ind,ind0) = Zmin_prime(ind,ind0)  + D2_S_D2( iSV, jSV)  &
                                         * fcCoef(iCoef)%PlmMin(ind)  * fcCoef(iCoef)%PlmPlus(ind0)
                end do ! iSV
              end do ! imu
             end do ! jSV
           end do ! imu0
         end do ! iCoef         

       case(4)

         do iCoef = iFourier, maxExpCoef
           D2_S_D2( 1, 1) =  phasefCoef( 1, 1, iCoef)
           D2_S_D2( 2, 1) =  0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 3, 1) =  0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 4, 1) =  0.0d0
           D2_S_D2( 1, 2) =  0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 1, 3) =  0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 1, 4) =  0.0d0
           D2_S_D2( 2, 2) =  0.25d0 * ( phasefCoef( 2, 2, iCoef) + phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 3, 3) =  0.25d0 * ( phasefCoef( 2, 2, iCoef) + phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 3, 2) =  0.25d0 * ( phasefCoef( 2, 2, iCoef) - phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 2, 3) =  0.25d0 * ( phasefCoef( 2, 2, iCoef) - phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 4, 1) =  0.0d0
           D2_S_D2( 4, 2) = -0.50d0 * phasefCoef( 3, 4, iCoef) ! phasefCoef( 3, 4, iCoef) = beta2
           D2_S_D2( 4, 3) =  0.50d0 * phasefCoef( 3, 4, iCoef)
           D2_S_D2( 2, 4) =  0.50d0 * phasefCoef( 3, 4, iCoef)
           D2_S_D2( 3, 4) = -0.50d0 * phasefCoef( 3, 4, iCoef)
           D2_S_D2( 4, 4) = phasefCoef( 4, 4, iCoef)
           do imu0 = 1, nmutot
             do jSV = 1, dimSV_fc
               ind0 = jSV + (imu0 - 1) * dimSV_fc
               do imu  = 1, nmutot
                 do iSV = 1, dimSV_fc
                   ind = iSV + (imu - 1) * dimSV_fc
                   Zplus_prime(ind,ind0) = Zplus_prime(ind,ind0) + D2_S_D2( iSV, jSV)  &
                                         * fcCoef(iCoef)%PlmPlus(ind) * fcCoef(iCoef)%PlmPlus(ind0)
                   Zmin_prime (ind,ind0) = Zmin_prime(ind,ind0)  + D2_S_D2( iSV, jSV)  &
                                         * fcCoef(iCoef)%PlmMin(ind)  * fcCoef(iCoef)%PlmPlus(ind0)
                end do ! iSV
              end do ! imu
             end do ! jSV
           end do ! imu0
         end do ! iCoef         

       case default

         call logDebug('ERROR: incorrect value for dimSV_fc in fillZplusZminPrime')
         call logDebug('in subrutine fillZplusZmin in RRSaddingModule')
         call logDebug('dimSV_fc can be 1, 3, or 4')
         call logDebugI('value provided is ', dimSV_fc)
         call mystop(errS, 'stopped because an incorrect value for dimSV_fc is used')
         if (errorCheck(errS)) return
      end select

      if ( dimSV_fc >= 3) then
        ! pre- and post multiply with D1
        ! fill D1
        D1 = 0.0d0
        do iSV = 1, dimSV_fc
          D1(iSV,iSV) = 1.0d0
        end do
        D1(3,3) = -1.0d0
        D1(2,3) =  1.0d0
        D1(3,2) =  1.0d0

        do imu0 = 1, nmutot
          ind0 = 1 + (imu0 - 1) * dimSV_fc
          do imu  = 1, nmutot
            ind = 1 + (imu - 1) * dimSV_fc
            Zplus_prime(ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1) = &
              matmul( D1, matmul( Zplus_prime(ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1), D1) )
            Zmin_prime(ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1)  = &
              matmul( D1, matmul( Zmin_prime (ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1), D1) )
          end do ! imu
        end do ! imu0
        
      end if

      ! divide by 4 mu mu0
      do imu0 = 1, nmutot
        ind0 = 1 + (imu0 - 1) * dimSV_fc
        do imu  = 1, nmutot
          ind = 1 + (imu - 1) * dimSV_fc
          fourmumu0 = 4.0d0 * geometryS%u(imu) *  geometryS%u(imu0)
          Zplus_prime(ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1) = &
            Zplus_prime(ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1) / fourmumu0
          Zmin_prime(ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1)  = &
            Zmin_prime (ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1) / fourmumu0
        end do ! imu
      end do ! imu0

      if ( verbose ) then

        ! remove Gaussian weights
        do imu0 = 1, nmutot
          do jSV = 1, dimSV_fc
            ind0 = jSV + (imu0 - 1) * dimSV_fc
            do imu  = 1, nmutot
              weight = geometryS%w(imu) * geometryS%w(imu0)
              do iSV = 1, dimSV_fc
                ind = iSV + (imu - 1) * dimSV_fc
                Zplus_prime(ind,ind0) = Zplus_prime(ind,ind0) / weight
                Zmin_prime (ind,ind0) = Zmin_prime(ind,ind0)  / weight
             end do ! iSV
           end do ! imu
          end do ! jSV
        end do ! imu0

        write(intermediateFileUnit, '(A, I4)') 'output from fillZplusZmin: Zplus for m = ', iFourier
        if ( present(string) ) write(intermediateFileUnit, '(2A)') 'further info: ', trim(string)
!         do imu  = 1, nmutot
         do imu  = nmutot-1, nmutot
           do iSV = 1, dimSV_fc
             ind = iSV + (imu - 1) * dimSV_fc
!             write(intermediateFileUnit, '(100F15.10)') (Zplus_prime(ind,ind0), ind0 = 1, dimSV_fc*nmutot)
             write(intermediateFileUnit, '(100(1pe13.5))') (Zplus_prime(ind,ind0), ind0 = dimSV_fc*(nmutot-2)+1, dimSV_fc*nmutot)
          end do ! iSV
        end do ! imu

        write(intermediateFileUnit, '(A, I4)') 'output from fillZplusZmin: Zmin for m = ', iFourier
!         do imu  = 1, nmutot
         do imu  = nmutot-1, nmutot
           do iSV = 1, dimSV_fc
             ind = iSV + (imu - 1) * dimSV_fc
!             write(intermediateFileUnit, '(100F15.10)') (Zplus_prime(ind,ind0), ind0 = 1, dimSV_fc*nmutot)
             write(intermediateFileUnit, '(100(1pe13.5))') (Zmin_prime(ind,ind0), ind0 = dimSV_fc*(nmutot-2)+1, dimSV_fc*nmutot)
          end do ! iSV
        end do ! imu

        ! restore Gaussian weights
        do imu0 = 1, nmutot
          do jSV = 1, dimSV_fc
            ind0 = jSV + (imu0 - 1) * dimSV_fc
            do imu  = 1, nmutot
              weight = geometryS%w(imu) * geometryS%w(imu0)
              do iSV = 1, dimSV_fc
                ind = iSV + (imu - 1) * dimSV_fc
                Zplus_prime(ind,ind0) = Zplus_prime(ind,ind0) * weight
                Zmin_prime (ind,ind0) = Zmin_prime(ind,ind0)  * weight
             end do ! iSV
           end do ! imu
          end do ! jSV
        end do ! imu0

      end if ! verbose

    end subroutine fillZplusZminPrime


  subroutine allocateUD_Spec(errS, nwavel, nmutot, dimSV, RTMnlayer, UD_Clear_Spec, UD_Cloud_Spec)
    ! allocate  arrays in UD_Clear_Spec, UD_Cloud_Spec
    ! and initialize them to zero

    implicit none

      type(errorType), intent(inout) :: errS
    integer, intent(in)         :: nwavel, nmutot, dimSV, RTMnlayer
    type(UDType), intent(inout) :: UD_Clear_Spec(nwavel, 0:RTMnlayer)
    type(UDType), intent(inout) :: UD_Cloud_Spec(nwavel, 0:RTMnlayer)

    ! local
    integer  :: allocStatus, sumAllocStatus
    integer  :: iwave, ilevel

    allocStatus    = 0
    sumAllocStatus = 0

    do ilevel = 0, RTMnlayer
      do iwave = 1, nwavel
        allocate( UD_Clear_Spec(iwave, ilevel)%E(dimSV*nmutot),   STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( UD_Clear_Spec(iwave, ilevel)%D(dimSV*nmutot,2), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( UD_Clear_Spec(iwave, ilevel)%U(dimSV*nmutot,2), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
      end do ! iwave
    end do ! ilevel

    do ilevel = 0, RTMnlayer
      do iwave = 1, nwavel
        allocate( UD_Cloud_Spec(iwave, ilevel)%E(dimSV*nmutot),   STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( UD_Cloud_Spec(iwave, ilevel)%D(dimSV*nmutot,2), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( UD_Cloud_Spec(iwave, ilevel)%U(dimSV*nmutot,2), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
      end do ! iwave
    end do ! ilevel

    if (sumAllocStatus /= 0) then 
        Print *, 'allocateUD has nonzero sumAllocStatus. Alloc Failed'
        Print *, 'in subroutine allocateUD_Spec in module radianceIrradianceModule'
        call mystop(errS, 'allocateUD has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

    ! initialize
    do ilevel = 0, RTMnlayer
      do iwave = 1, nwavel
      UD_Clear_Spec(iwave, ilevel)%E   = 0.0d0
      UD_Clear_Spec(iwave, ilevel)%D   = 0.0d0
      UD_Clear_Spec(iwave, ilevel)%U   = 0.0d0
      UD_Cloud_Spec(iwave, ilevel)%E   = 0.0d0
      UD_Cloud_Spec(iwave, ilevel)%D   = 0.0d0
      UD_Cloud_Spec(iwave, ilevel)%U   = 0.0d0
      end do ! iwave
    end do ! ilevel    

  end subroutine allocateUD_Spec


  subroutine deallocateUD_Spec(errS, nwavel, RTMnlayer, UD_Clear_Spec, UD_Cloud_Spec)
    ! deallocate  arrays in UD_Clear_Spec, UD_Cloud_Spec

    implicit none

      type(errorType), intent(inout) :: errS
    integer, intent(in) :: nwavel, RTMnlayer
    type(UDType)        :: UD_Clear_Spec(nwavel, 0:RTMnlayer)
    type(UDType)        :: UD_Cloud_Spec(nwavel, 0:RTMnlayer)

    ! local
    integer  :: deallocStatus, sumdeAllocStatus
    integer  :: ilevel, iwave

    deallocStatus    = 0
    sumdeAllocStatus = 0

    do ilevel = 0, RTMnlayer
      do iwave = 1, nwavel
        deallocate( UD_Clear_Spec(iwave, ilevel)%E, STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus
        deallocate( UD_Clear_Spec(iwave, ilevel)%D, STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus
        deallocate( UD_Clear_Spec(iwave, ilevel)%U, STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      end do ! iwave
    end do ! ilevel

    do ilevel = 0, RTMnlayer
      do iwave = 1, nwavel
        deallocate( UD_Cloud_Spec(iwave, ilevel)%E, STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus
        deallocate( UD_Cloud_Spec(iwave, ilevel)%D, STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus
        deallocate( UD_Cloud_Spec(iwave, ilevel)%U, STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      end do ! iwave
    end do ! ilevel

    if (sumdeAllocStatus /= 0) then 
        Print *, 'deallocateUD has nonzero sumAllocStatus. Alloc Failed'
        Print *, 'in subroutine deallocateUD_Spec in module radianceIrradianceModule'
        call mystop(errS, 'deallocateUD has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine deallocateUD_Spec


  subroutine allocateUD(errS, nmutot, dimSV, RTMnlayer, UD_Clear, UD_Cloud)
    ! allocate  arrays in UD_Clear, UD_Cloud
    ! and initialize them to zero

    implicit none

      type(errorType), intent(inout) :: errS
    integer, intent(in)       :: nmutot, dimSV, RTMnlayer
    type(UDType), intent(out) :: UD_Clear(0:RTMnlayer)
    type(UDType), intent(out) :: UD_Cloud(0:RTMnlayer)

    ! local
    integer  :: allocStatus, sumAllocStatus
    integer  :: ilevel

    allocStatus    = 0
    sumAllocStatus = 0

    do ilevel = 0, RTMnlayer
      allocate( UD_Clear(ilevel)%E(dimSV*nmutot),   STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( UD_Clear(ilevel)%D(dimSV*nmutot,2), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( UD_Clear(ilevel)%U(dimSV*nmutot,2), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end do ! ilevel

    do ilevel = 0, RTMnlayer
      allocate( UD_Cloud(ilevel)%E(dimSV*nmutot),   STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( UD_Cloud(ilevel)%D(dimSV*nmutot,2), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( UD_Cloud(ilevel)%U(dimSV*nmutot,2), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end do ! ilevel

    if (sumAllocStatus /= 0) then 
        Print *, 'allocateUD has nonzero sumAllocStatus. Alloc Failed'
        call mystop(errS, 'allocateUD has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

    ! initialize
    do ilevel = 0, RTMnlayer
      UD_Clear(ilevel)%E   = 0.0d0
      UD_Clear(ilevel)%D   = 0.0d0
      UD_Clear(ilevel)%U   = 0.0d0
      UD_Cloud(ilevel)%E   = 0.0d0
      UD_Cloud(ilevel)%D   = 0.0d0
      UD_Cloud(ilevel)%U   = 0.0d0
    end do ! ilevel    

  end subroutine allocateUD


  subroutine deallocateUD(errS, RTMnlayer, UD_Clear, UD_Cloud)
    ! deallocate  arrays in UD_Clear, UD_Cloud

    implicit none

      type(errorType), intent(inout) :: errS
    integer, intent(in) :: RTMnlayer
    type(UDType)        :: UD_Clear(0:RTMnlayer)
    type(UDType)        :: UD_Cloud(0:RTMnlayer)

    ! local
    integer  :: deallocStatus, sumdeAllocStatus
    integer  :: ilevel

    deallocStatus    = 0
    sumdeAllocStatus = 0

    do ilevel = 0, RTMnlayer
      deallocate( UD_Clear(ilevel)%E, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( UD_Clear(ilevel)%D, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( UD_Clear(ilevel)%U, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end do ! ilevel

    do ilevel = 0, RTMnlayer
      deallocate( UD_Cloud(ilevel)%E, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( UD_Cloud(ilevel)%D, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( UD_Cloud(ilevel)%U, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end do ! ilevel

    if (sumdeAllocStatus /= 0) then 
        Print *, 'deallocateUD has nonzero sumAllocStatus. Alloc Failed'
        call mystop(errS, 'deallocateUD has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine deallocateUD


  subroutine calculateReflAndDeriv_lbl(errS, iband, nTrace, wavelHRS, XsecHRS,                    &
                                       geometryS, retrS, controlS, cloudAerosolRTMgridS, gasPTS,  &
                                       traceGasS, surfaceS, LambCloudS, mieAerS, mieCldS,         &
                                       cldAerFractionS, RRS_RingS,                                &
                                       mulOffsetS, strayLightS, solarIrrS, optPropRTMGridS,       &
                                       dn_dnodeS, reflDerivHRS)

    ! calculateReflAndDeriv_lbl calculates the reflectance and derivatives for one spectral band,
    ! using only elastic scattering and line-by-line calculations. The results are stored in reflDerivHRS
    ! where HR stands for high-resolution.

    implicit none

    type(errorType),               intent(inout) :: errS
    integer,                       intent(in)    :: iband                     ! number wavelength band
    integer,                       intent(in)    :: nTrace                    ! number of trace gases
    type(wavelHRType),             intent(in)    :: wavelHRS                  ! high resolution wavelength grid
    type(XsecType),                intent(in)    :: XsecHRS(nTrace)           ! altitude dependent absorption Xsections
    type(geometryType),            intent(in)    :: geometryS                 ! information on geometry
    type(retrType),                intent(in)    :: retrS                     ! contains info on the fit parameters
    type(controlType),             intent(inout) :: controlS                  ! control parameters for radiative transfer
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS      ! cloud-aerosol properties
    type(gasPTType),               intent(inout) :: gasPTS                    ! atmospheric pressure and temperature
    type(traceGasType),            intent(inout) :: traceGasS(nTrace)         ! atmospheric trace gas properties
    type(LambertianType),          intent(inout) :: surfaceS                  ! surface properties for a spectral band
    type(LambertianType),          intent(inout) :: LambCloudS                ! Lambertian cloud properties
    type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)  ! specification expansion coefficients
    type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)  ! specification expansion coefficients
    type(cldAerFractionType),      intent(inout) :: cldAerFractionS           ! cloud / aerosol  fraction
    type(RRS_RingType),            intent(in)    :: RRS_RingS                 ! Raman / Ring information
    type(mulOffsetType),           intent(inout) :: mulOffsetS                ! multiplicative offset
    type(straylightType),          intent(inout) :: strayLightS               ! stray light
    type(SolarIrrType),            intent(inout) :: solarIrrS                 ! solar irradiance
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS           ! optical properties on RTM grid
    type(dn_dnodeType),            intent(inout) :: dn_dnodeS(0:nTrace)       ! modify derivatives for spline interpolation
    type(reflDerivType),           intent(inout) :: reflDerivHRS              ! reflectance and derivatives

    ! local

    logical    :: cloudyPart
    integer    :: iwave, istate, iTrace, statusRTM, iSV, imu, imu0, ilevel

    ! Reflectance and basic derivatives returned by layerBasedOrdersScattering
    ! note that the combination LambScaCloud has a Lambertian and scattering cloud
    ! as the Lambertian surface is at the top of the fit interval the weighting functions
    ! for cloud and aerosol particles are not needed in case. Therefore, only the
    ! weighting function for the cloud albedo, and scattering and absorption by gas are
    ! required for this combination.

    real(8)       :: refl_Clear(controlS%dimSV)
    real(8)       :: refl_Cloud(controlS%dimSV)

    real(8)       :: contribRefl_Clear(controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)
    real(8)       :: contribRefl_Cloud(controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)

    type(UDType)  :: UD_Clear(0:optPropRTMGridS%RTMnlayer)
    type(UDType)  :: UD_Cloud(0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkscaGas_Clear(0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaGas_Cloud(0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkscaAerAbove_Clear(0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaAerBelow_Clear(0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaAerAbove_Cloud(0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaAerBelow_Cloud(0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkscaCldAbove_Clear(0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaCldBelow_Clear(0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaCldAbove_Cloud(0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkscaCldBelow_Cloud(0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfInterfkabs_Clear(0:optPropRTMGridS%RTMnlayer)
    real(8)       :: wfInterfkabs_Cloud(0:optPropRTMGridS%RTMnlayer)

    real(8)       :: wfSurfaceAlbedo_Clear
    real(8)       :: wfSurfaceAlbedo_Cloud
    real(8)       :: wfSurfaceEmission_Clear
    real(8)       :: wfSurfaceEmission_Cloud

    real(8)       :: wfCloudAlbedo

    ! weighting functions for the cloud fraction
    real(8)       :: wfCloudFraction
    real(8)       :: cf, cfMax, cfMin                   ! cloud fraction, maximal and minimal cloud fractions
 
    real(8)    :: surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave

    real(8), parameter :: PI = 3.141592653589793d0

    logical, parameter :: verbose = .false.

    ! initialise
    surfEmission_iwave = 0.0d0
    wfCloudFraction    = 0.0d0

    call allocateUD(errS, geometryS%nmutot, controlS%dimSV, optPropRTMGridS%RTMnlayer, UD_Clear, UD_Cloud)
    if (errorCheck(errS)) return

    call fillAltPresGridRTM(errS, cloudAerosolRTMgridS, OptPropRTMGridS)
    if (errorCheck(errS)) return

    if ( controlS%calculateDerivatives ) then
      if ( (retrS%numIterations == 1 ) .OR. cloudAerosolRTMgridS%fitIntervalDP    &
                                       .OR. cloudAerosolRTMgridS%fitIntervalTop   &
                                       .OR. cloudAerosolRTMgridS%fitIntervalBot ) then
        ! calculate the change in a profile if the value at a node changes; index 0 is for air
        call calculate_dn_dnode(errS, gasPTS%npressureNodes, gasPTS%altNodes, optPropRTMGridS%RTMnlayerSub, &
                                optPropRTMGridS%RTMaltitudeSub, dn_dnodeS(0)%dn_dnode, gasPTS%useLinInterp)
        if (errorCheck(errS)) return
        do iTrace = 1, nTrace
          call calculate_dn_dnode(errS, traceGasS(iTrace)%nalt, traceGasS(iTrace)%alt, optPropRTMGridS%RTMnlayerSub, &
                     optPropRTMGridS%RTMaltitudeSub, dn_dnodeS(iTrace)%dn_dnode, traceGasS(iTrace)%useLinInterp)
          if (errorCheck(errS)) return
        end do
      end if
    end if ! controlS%calculateDerivatives

    ! calculate cfMax and cfMin these are used when 
    ! the derivatives for the cloudy part of the pixel are calculated
    if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
      cfMax = cloudAerosolRTMgridS%cldAerFractionAllBands
      cfMin = cloudAerosolRTMgridS%cldAerFractionAllBands
    else
      cfMin = maxCldFraction
      cfMax = minCldFraction
      do iwave = 1, wavelHRS%nwavel
        cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelHRS%wavel(iwave))
        if ( cf < cfMin ) cfMin = cf
        if ( cf > cfMax ) cfMax = cf
      end do
    end if

    ! calculate propertie for the cloudy part of the pixel when
    ! - cloud is present
    ! - at least one of the following is true: 
    !     * cloud fraction is above the threshold
    !     * cloud fraction is fitted
    if ( cloudAerosolRTMgridS%cloudpresent  .and.          &
       ( (cfMax > controlS%cloudFracThreshold)        .or. &
       cloudAerosolRTMgridS%fitCldAerFractionAllBands .or. & 
       cldAerFractionS%fitCldAerFraction ) ) then

      ! loop over wavelengths
      do iwave = 1, wavelHRS%nwavel

        ! set cloud fraction
        if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
          cf = cloudAerosolRTMgridS%cldAerFractionAllBands
        else
          cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelHRS%wavel(iwave))
        end if

        ! do calculations only for the cloudy part of the pixel when the cloud fraction is larger then 1.0
        if ( cf >= 1.0d0 ) then
          if ( cloudAerosolRTMgridS%useLambertianCloud ) then
            ! for a Lambertian cloud the optical properties of the atmosphere are the same as those
            ! for the cloud free part, except for the Lambertian cloud
            ! the Lambertian cloud is placed at a certain altitude in the atmosphere
            ! the index for the altitude on the RTM altitude grid is given by cloudAerosolRTMgridS%RTMnlevelCloud
            cloudyPart = .false. ! use propeties for the atmosphere without scattering cloud particles
            call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelHRS, XsecHRS, RRS_RingS,  &
                                controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,     &
                                mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                 &
                                surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,              &
                                reflDerivHRS%depolarization(iwave))
            if (errorCheck(errS)) return
            if ( controlS%singleScatteringOnly ) then
              call singleScattering(errS,            &
                optPropRTMGridS,                     & 
                controlS,                            & 
                geometryS,                           &
                cloudAlb_iwave,                      &
                cloudAerosolRTMgridS%RTMnlevelCloud, &   ! index for altitude Lambertian cloud
                reflDerivHRS%babs(iwave),            &   ! output
                reflDerivHRS%bsca(iwave),            &   ! output
                refl_Cloud,                          &   ! output 
                contribRefl_Cloud,                   &   ! output
                wfInterfkscaGas_Cloud,               &   ! output 
                wfInterfkscaAerAbove_Cloud,          &   ! output 
                wfInterfkscaAerBelow_Cloud,          &   ! output 
                wfInterfkscaCldAbove_Cloud,          &   ! output 
                wfInterfkscaCldBelow_Cloud,          &   ! output 
                wfInterfkabs_Cloud,                  &   ! output
                wfCloudAlbedo,                       &   ! output
                wfSurfaceEmission_Cloud,             &   ! output      
                statusRTM)
              if (errorCheck(errS)) return
            else ! controlS%singleScatteringOnly
              call layerBasedOrdersScattering(errS,  &
                optPropRTMGridS,                     & 
                controlS,                            &
                geometryS,                           & 
                cloudAlb_iwave,                      & 
                cloudAerosolRTMgridS%RTMnlevelCloud, &   ! index for altitude Lambertian cloud
                reflDerivHRS%babs(iwave),            &   ! output
                reflDerivHRS%bsca(iwave),            &   ! output
                refl_Cloud,                          &   ! output 
                contribRefl_Cloud,                   &   ! output
                UD_Cloud,                            &   ! output
                wfInterfkscaGas_Cloud,               &   ! output 
                wfInterfkscaAerAbove_Cloud,          &   ! output 
                wfInterfkscaAerBelow_Cloud,          &   ! output 
                wfInterfkscaCldAbove_Cloud,          &   ! output 
                wfInterfkscaCldBelow_Cloud,          &   ! output 
                wfInterfkabs_Cloud,                  &   ! output
                wfCloudAlbedo,                       &   ! output 
                wfSurfaceEmission_Cloud,             &   ! output      
                statusRTM)
              if (errorCheck(errS)) return
            end if ! controlS%singleScatteringOnly
          else ! cloudAerosolRTMgridS%useLambertianCloud
            ! use scattering cloud
            cloudyPart = .true. ! use atmospheric properties with a scattering cloud
            call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelHRS, XsecHRS, RRS_RingS,    &
                                controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS, &
                                mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,             &
                                surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,          &
                                reflDerivHRS%depolarization(iwave))
            if (errorCheck(errS)) return
            ! the altitude of the Lambertian surface is now the ground surface which has level nr = 0
            if ( controlS%singleScatteringOnly ) then
              call singleScattering(errS,            &
                optPropRTMGridS,                     & 
                controlS,                            & 
                geometryS,                           &
                surfAlb_iwave,                       & 
                0,                                   &   ! index for Lambertian surface - here ground surface
                reflDerivHRS%babs(iwave),            &   ! output
                reflDerivHRS%bsca(iwave),            &   ! output
                refl_Cloud,                          &   ! output 
                contribRefl_Cloud,                   &   ! output
                wfInterfkscaGas_Cloud,               &   ! output 
                wfInterfkscaAerAbove_Cloud,          &   ! output 
                wfInterfkscaAerBelow_Cloud,          &   ! output 
                wfInterfkscaCldAbove_Cloud,          &   ! output 
                wfInterfkscaCldBelow_Cloud,          &   ! output 
                wfInterfkabs_Cloud,                  &   ! output
                wfSurfaceAlbedo_Cloud,               &   ! output 
                wfSurfaceEmission_Cloud,             &   ! output 
                statusRTM)
              if (errorCheck(errS)) return
            else ! controlS%singleScatteringOnly
              call layerBasedOrdersScattering(errS,  &
                optPropRTMGridS,                     & 
                controlS,                            &
                geometryS,                           & 
                surfAlb_iwave,                       & 
                0,                                   &   ! index for Lambertian surface - here ground surface
                reflDerivHRS%babs(iwave),            &   ! output
                reflDerivHRS%bsca(iwave),            &   ! output
                refl_Cloud,                          &   ! output 
                contribRefl_Cloud,                   &   ! output
                UD_Cloud,                            &   ! output
                wfInterfkscaGas_Cloud,               &   ! output 
                wfInterfkscaAerAbove_Cloud,          &   ! output 
                wfInterfkscaAerBelow_Cloud,          &   ! output 
                wfInterfkscaCldAbove_Cloud,          &   ! output 
                wfInterfkscaCldBelow_Cloud,          &   ! output 
                wfInterfkabs_Cloud,                  &   ! output
                wfSurfaceAlbedo_Cloud,               &   ! output 
                wfSurfaceEmission_Cloud,             &   ! output 
                statusRTM)
              if (errorCheck(errS)) return
            end if ! controlS%singleScatteringOnly
          end if ! cloudAerosolRTMgridS%useLambertianCloud

          ! set values for the cloud-free part of the pixel to zero
          refl_Clear                                = 0.0d0
          contribRefl_Clear                         = 0.0d0
          wfInterfkscaGas_Clear                     = 0.0d0
          wfInterfkscaAerAbove_Clear                = 0.0d0
          wfInterfkscaAerBelow_Clear                = 0.0d0
          wfInterfkscaCldAbove_Clear                = 0.0d0
          wfInterfkscaCldBelow_Clear                = 0.0d0
          wfInterfkabs_Clear                        = 0.0d0
          wfSurfaceAlbedo_Clear                     = 0.0d0
          reflDerivHRS%refl_clr(:,iwave)            = 0.0d0
          reflDerivHRS%altResAMFabs_clr(iwave,:)    = 0.0d0
          reflDerivHRS%altResAMFscaAer_clr(iwave,:) = 0.0d0
          reflDerivHRS%K_clr_lnvmr(iwave,:)         = 0.0d0
          reflDerivHRS%K_clr_vmr(iwave,:)           = 0.0d0
          reflDerivHRS%K_clr_ndens(iwave,:)         = 0.0d0
          reflDerivHRS%KHR_clr_ndensCol(iwave,:)    = 0.0d0

          ! fill values for the cloudy part of the pixel for reflected light and
          ! surface emission (ignore emission for Stokes parameters other than I)
          reflDerivHRS%refl_cld(1,iwave) = refl_Cloud(1)                                     &
                                         + surfEmission_iwave * wfSurfaceEmission_Cloud * PI &
                                         / geometryS%u0 / solarIrrS%solIrrMR(iwave)
          do iSV = 2, controlS%dimSV
            reflDerivHRS%refl_cld(iSV,iwave) = refl_Cloud(iSV)
          end do
          reflDerivHRS%refl(:,iwave) = cf * reflDerivHRS%refl_cld(:,iwave)
          do iSV = 1, controlS%dimSV
            imu = (iSV - 1) * geometryS%nmutot + geometryS%nmutot - 1
            imu0 = 2
            reflDerivHRS%contribRefl(iSV,iwave,:)     = cf * contribRefl_Cloud(iSV,:)
            do ilevel = 0, reflDerivHRS%RTMnlayer
              reflDerivHRS%intFieldUp  (iSV,iwave,ilevel) = cf * UD_Cloud(ilevel)%U(imu, imu0)
              reflDerivHRS%intFieldDown(iSV,iwave,ilevel) = cf * UD_Cloud(ilevel)%D(imu, imu0)
            end do ! ilevel
          end do ! iSV
          reflDerivHRS%altResAMFabs_cld(iwave,:)    = - wfInterfkabs_Cloud(:) / refl_Cloud(1)
          reflDerivHRS%altResAMFscaAer_cld(iwave,:) = wfInterfkscaAerAbove_Cloud(:) / refl_Cloud(1)
          wfCloudFraction  = reflDerivHRS%refl_cld(1,iwave)  ! ignore the cloud free part as it is not present

          ! determine derivatives
          if ( controlS%calculateDerivatives ) then
            call calculate_K_cld(errS, iband, nTrace, cfMin, cfMax, traceGasS, gasPTS, LambCloudS,           &
                                 strayLightS, solarIrrS, cloudAerosolRTMgridS,                         &
                                 optPropRTMGridS, retrS, geometryS, wavelHRS, iwave, dn_dnodeS,        &
                                 wfInterfkabs_Cloud, wfInterfkscaGas_Cloud,                            &
                                 wfInterfkscaAerAbove_Cloud, wfInterfkscaAerBelow_Cloud,               &
                                 wfInterfkscaCldAbove_Cloud, wfInterfkscaCldBelow_Cloud,               &
                                 wfSurfaceAlbedo_Cloud, wfSurfaceEmission_Cloud, wfCloudAlbedo,        &
                                 wfCloudFraction, reflDerivHRS) 
            if (errorCheck(errS)) return
          else ! controlS%calculateDerivatives
            reflDerivHRS%K_cld_lnvmr(iwave,:)      = 0.0d0
            reflDerivHRS%K_cld_vmr(iwave,:)        = 0.0d0
            reflDerivHRS%K_cld_ndens(iwave,:)      = 0.0d0
            reflDerivHRS%KHR_cld_ndensCol(iwave,:) = 0.0d0
          end if ! controlS%calculateDerivatives

        else ! cf >= 1.0d0

          ! do calculations for the clear and cloudy part of the pixel because cf < 1.0
          ! while a cloud is present

          ! CLEAR PART

          cloudyPart = .false.
          call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelHRS, XsecHRS, RRS_RingS,     &
                              controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,  &
                              mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,              &
                              surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,           &
                              reflDerivHRS%depolarization(iwave))
          if (errorCheck(errS)) return
          if ( controlS%singleScatteringOnly ) then
            call singleScattering(errS,            &
              optPropRTMGridS,                     & 
              controlS,                            & 
              geometryS,                           &
              surfAlb_iwave,                       & 
              0,                                   &   ! index for Lambertian surface - here ground surface
              reflDerivHRS%babs(iwave),            &   ! output
              reflDerivHRS%bsca(iwave),            &   ! output
              refl_Clear,                          &   ! output 
              contribRefl_Clear,                   &   ! output
              wfInterfkscaGas_Clear,               &   ! output 
              wfInterfkscaAerAbove_Clear,          &   ! output 
              wfInterfkscaAerBelow_Clear,          &   ! output 
              wfInterfkscaCldAbove_Clear,          &   ! output 
              wfInterfkscaCldBelow_Clear,          &   ! output 
              wfInterfkabs_Clear,                  &   ! output
              wfSurfaceAlbedo_Clear,               &   ! output 
              wfSurfaceEmission_Clear,             &   ! output 
              statusRTM)
            if (errorCheck(errS)) return
          else ! controlS%singleScatteringOnly
            call layerBasedOrdersScattering(errS,  &
              optPropRTMGridS,                     & 
              controlS,                            &
              geometryS,                           &
              surfAlb_iwave,                       & 
              0,                                   &   ! index for Lambertian surface - here ground surface
              reflDerivHRS%babs(iwave),            &   ! output
              reflDerivHRS%bsca(iwave),            &   ! output
              refl_Clear,                          &   ! output 
              contribRefl_Clear,                   &   ! output
              UD_Clear,                            &   ! output
              wfInterfkscaGas_Clear,               &   ! output 
              wfInterfkscaAerAbove_Clear,          &   ! output 
              wfInterfkscaAerBelow_Clear,          &   ! output 
              wfInterfkscaCldAbove_Clear,          &   ! output 
              wfInterfkscaCldBelow_Clear,          &   ! output 
              wfInterfkabs_Clear,                  &   ! output
              wfSurfaceAlbedo_Clear,               &   ! output 
              wfSurfaceEmission_Clear,             &   ! output 
              statusRTM)
            if (errorCheck(errS)) return
          end if ! controlS%singleScatteringOnly

          ! CLOUDY PART

          ! distinguish between Lambertian and scattering cloud
          if ( cloudAerosolRTMgridS%useLambertianCloud ) then
            ! for a Lambertian cloud the optical properties of the atmosphere are the same as those
            ! for the cloud free part, except for the Lambertian cloud
            ! the Lambertian cloud is placed at a certain altitude in the atmosphere
            ! the index for the altitude on the RTM altitude grid is given by cloudAerosolRTMgridS%RTMnlevelCloud
            cloudyPart = .false. ! use propeties for a cloud-free atmosphere
            call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelHRS, XsecHRS, RRS_RingS,    &
                                controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS, &
                                mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,             &
                                surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,          &
                                reflDerivHRS%depolarization(iwave))
            if (errorCheck(errS)) return
            if ( controlS%singleScatteringOnly ) then
              call singleScattering(errS,            &
                optPropRTMGridS,                     & 
                controlS,                            & 
                geometryS,                           &
                cloudAlb_iwave,                      & 
                cloudAerosolRTMgridS%RTMnlevelCloud, &   ! index for altitude Lambertian cloud
                reflDerivHRS%babs(iwave),            &   ! output
                reflDerivHRS%bsca(iwave),            &   ! output
                refl_Cloud,                          &   ! output 
                contribRefl_Cloud,                   &   ! output
                wfInterfkscaGas_Cloud,               &   ! output 
                wfInterfkscaAerAbove_Cloud,          &   ! output 
                wfInterfkscaAerBelow_Cloud,          &   ! output 
                wfInterfkscaCldAbove_Cloud,          &   ! output 
                wfInterfkscaCldBelow_Cloud,          &   ! output 
                wfInterfkabs_Cloud,                  &   ! output
                wfCloudAlbedo,                       &   ! output 
                wfSurfaceEmission_Cloud,             &   ! output 
                statusRTM)
              if (errorCheck(errS)) return
            else ! controlS%singleScatteringOnly
              call layerBasedOrdersScattering(errS,  &
                optPropRTMGridS,                     & 
                controlS,                            &
                geometryS,                           & 
                cloudAlb_iwave,                      & 
                cloudAerosolRTMgridS%RTMnlevelCloud, &   ! index for altitude Lambertian cloud
                reflDerivHRS%babs(iwave),            &   ! output
                reflDerivHRS%bsca(iwave),            &   ! output
                refl_Cloud,                          &   ! output 
                contribRefl_Cloud,                   &   ! output
                UD_Cloud,                            &   ! output
                wfInterfkscaGas_Cloud,               &   ! output 
                wfInterfkscaAerAbove_Cloud,          &   ! output 
                wfInterfkscaAerBelow_Cloud,          &   ! output 
                wfInterfkscaCldAbove_Cloud,          &   ! output 
                wfInterfkscaCldBelow_Cloud,          &   ! output 
                wfInterfkabs_Cloud,                  &   ! output
                wfCloudAlbedo,                       &   ! output 
                wfSurfaceEmission_Cloud,             &   ! output 
                statusRTM)
              if (errorCheck(errS)) return
            end if ! controlS%singleScatteringOnly
          else  ! cloudAerosolRTMgridS%useLambertianCloud
            ! use scattering cloud
            cloudyPart = .true. ! use atmospheric properties with a scattering cloud
            call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelHRS, XsecHRS, RRS_RingS, &
                                controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,    &
                                mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                &
                                surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,             &
                                reflDerivHRS%depolarization(iwave))
            if (errorCheck(errS)) return
            ! the altitude of the Lambertian surface is now the ground surface which has level nr = 0
            if ( controlS%singleScatteringOnly ) then
              call singleScattering(errS,            &
                optPropRTMGridS,                     & 
                controlS,                            & 
                geometryS,                           &
                surfAlb_iwave,                       & 
                0,                                   &   ! index for Lambertian surface - here ground surface
                reflDerivHRS%babs(iwave),            &   ! output
                reflDerivHRS%bsca(iwave),            &   ! output
                refl_Cloud,                          &   ! output 
                contribRefl_Cloud,                   &   ! output
                wfInterfkscaGas_Cloud,               &   ! output 
                wfInterfkscaAerAbove_Cloud,          &   ! output 
                wfInterfkscaAerBelow_Cloud,          &   ! output 
                wfInterfkscaCldAbove_Cloud,          &   ! output 
                wfInterfkscaCldBelow_Cloud,          &   ! output 
                wfInterfkabs_Cloud,                  &   ! output
                wfSurfaceAlbedo_Cloud,               &   ! output 
                wfSurfaceEmission_Cloud,             &   ! output 
                statusRTM)
              if (errorCheck(errS)) return
            else ! controlS%singleScatteringOnly
              call layerBasedOrdersScattering(errS,  &
                optPropRTMGridS,                     & 
                controlS,                            &
                geometryS,                           & 
                surfAlb_iwave,                       & 
                0,                                   &   ! index for Lambertian surface - here ground surface
                reflDerivHRS%babs(iwave),            &   ! output
                reflDerivHRS%bsca(iwave),            &   ! output
                refl_Cloud,                          &   ! output 
                contribRefl_Cloud,                   &   ! output
                UD_Cloud,                            &   ! output
                wfInterfkscaGas_Cloud,               &   ! output 
                wfInterfkscaAerAbove_Cloud,          &   ! output 
                wfInterfkscaAerBelow_Cloud,          &   ! output 
                wfInterfkscaCldAbove_Cloud,          &   ! output 
                wfInterfkscaCldBelow_Cloud,          &   ! output 
                wfInterfkabs_Cloud,                  &   ! output
                wfSurfaceAlbedo_Cloud,               &   ! output 
                wfSurfaceEmission_Cloud,             &   ! output 
                statusRTM)
              if (errorCheck(errS)) return
            end if ! controlS%singleScatteringOnly
          end if ! cloudAerosolRTMgridS%useLambertianCloud

          ! fill air mass factors for absorption the cloudy and cloud free part before adding surface emission
          ! to the reflectance
          reflDerivHRS%altResAMFabs_clr(iwave,:)    = - wfInterfkabs_Clear(:) / refl_Clear(1)
          reflDerivHRS%altResAMFabs_cld(iwave,:)    = - wfInterfkabs_Cloud(:) / refl_Cloud(1)
          ! fill air mass factors for scattering by aerosol for the cloudy and cloud free part
          reflDerivHRS%altResAMFscaAer_clr(iwave,:) = wfInterfkscaAerAbove_Clear(:) / refl_Clear(1)
          reflDerivHRS%altResAMFscaAer_cld(iwave,:) = wfInterfkscaAerAbove_Cloud(:) / refl_Cloud(1)

          ! fill values for the cloudy and cloud free part of the pixel reflected light and
          ! surface emission (ignore emission for Stokes parameters other than I)
          reflDerivHRS%refl_cld(1,iwave) = refl_Cloud(1)                                              &
                                         + 1.0d12 * surfEmission_iwave * wfSurfaceEmission_Cloud * PI &
                                         / geometryS%u0 / solarIrrS%solIrrMR(iwave)
          do iSV = 2, controlS%dimSV
            reflDerivHRS%refl_cld(iSV,iwave) = refl_Cloud(iSV)
          end do
          reflDerivHRS%refl_clr(1,iwave) = refl_Clear(1)                                              &
                                         + 1.0d12 * surfEmission_iwave * wfSurfaceEmission_Clear * PI &
                                         / geometryS%u0 / solarIrrS%solIrrMR(iwave)
          do iSV = 2, controlS%dimSV
            reflDerivHRS%refl_clr(iSV,iwave) = refl_Clear(iSV)
          end do

          ! fill reflectance + emission for the entire pixel
          reflDerivHRS%refl(:,iwave) = cf * reflDerivHRS%refl_cld(:,iwave) &
                                     + (1.0d0 - cf) * reflDerivHRS%refl_clr(:,iwave)

          ! fill contribution functiom and internal field for the entire pixel (emission does not contribute)
          do iSV = 1, controlS%dimSV
            imu = (iSV - 1) * geometryS%nmutot + geometryS%nmutot - 1
            imu0 = 2
            reflDerivHRS%contribRefl(iSV,iwave,:) = cf * contribRefl_Cloud(iSV,:) + (1.0d0 - cf) * contribRefl_Clear(iSV,:)
            do ilevel = 0, reflDerivHRS%RTMnlayer
              reflDerivHRS%intFieldUp  (iSV,iwave,ilevel) = cf * UD_Cloud(ilevel)%U(imu, imu0) &
                                                          + (1.0d0 - cf) * UD_Clear(ilevel)%U(imu, imu0)
              reflDerivHRS%intFieldDown(iSV,iwave,ilevel) = cf * UD_Cloud(ilevel)%D(imu, imu0) &
                                                          + (1.0d0 - cf) * UD_Clear(ilevel)%D(imu, imu0)
            end do ! ilevel
          end do ! iSV

          wfCloudFraction  = reflDerivHRS%refl_cld(1,iwave) - reflDerivHRS%refl_clr(1,iwave)

          if ( controlS%calculateDerivatives ) then

            call calculate_K_clr(errS, iband, nTrace, traceGasS, gasPTS,                                 &
                                 strayLightS, solarIrrS, cloudAerosolRTMgridS,                     &
                                 optPropRTMGridS, retrS, geometryS, wavelHRS, iwave, dn_dnodeS,    &
                                 wfInterfkabs_Clear, wfInterfkscaGas_Clear,                        &
                                 wfInterfkscaAerAbove_Clear, wfInterfkscaAerBelow_Clear,           &
                                 wfSurfaceAlbedo_Clear, wfSurfaceEmission_Clear, wfCloudFraction,  &
                                 reflDerivHRS) 
            if (errorCheck(errS)) return

            call calculate_K_cld(errS, iband, nTrace, cfMin, cfMax, traceGasS, gasPTS, LambCloudS,       &
                                 strayLightS, solarIrrS, cloudAerosolRTMgridS,                     &
                                 optPropRTMGridS, retrS, geometryS, wavelHRS, iwave, dn_dnodeS,    &
                                 wfInterfkabs_Cloud, wfInterfkscaGas_Cloud,                        &
                                 wfInterfkscaAerAbove_Cloud, wfInterfkscaAerBelow_Cloud,           &
                                 wfInterfkscaCldAbove_Cloud, wfInterfkscaCldBelow_Cloud,           &
                                 wfSurfaceAlbedo_Cloud, wfSurfaceEmission_Cloud, wfCloudAlbedo,    &
                                 wfCloudFraction, reflDerivHRS) 
            if (errorCheck(errS)) return

          else ! controlS%calculateDerivatives
                        
            reflDerivHRS%K_clr_lnvmr(iwave,:)      = 0.0d0
            reflDerivHRS%K_cld_lnvmr(iwave,:)      = 0.0d0
            reflDerivHRS%K_clr_vmr(iwave,:)        = 0.0d0
            reflDerivHRS%K_cld_vmr(iwave,:)        = 0.0d0
            reflDerivHRS%K_clr_ndens(iwave,:)      = 0.0d0
            reflDerivHRS%K_cld_ndens(iwave,:)      = 0.0d0
            reflDerivHRS%KHR_clr_ndensCol(iwave,:) = 0.0d0
            reflDerivHRS%KHR_cld_ndensCol(iwave,:) = 0.0d0
          end if ! controlS%calculateDerivatives

        end if ! cf > 1.0

      end do ! iwave

      ! complete the calculation of derivatives for polynomials
      call setPolynomialDerivatives_clr(errS, iband, nTrace, wavelHRS, retrS, cloudAerosolRTMgridS, &
                                        surfaceS, LambCloudS, cldAerFractionS, mulOffsetS, strayLightS, reflDerivHRS)
      if (errorCheck(errS)) return
      call setPolynomialDerivatives_cld(errS, iband, nTrace, wavelHRS, retrS, cloudAerosolRTMgridS, &
                                        surfaceS, LambCloudS, cldAerFractionS, mulOffsetS, strayLightS, reflDerivHRS)
      if (errorCheck(errS)) return

      ! combine the derivatives for the clear and cloudy part
      do iwave = 1, wavelHRS%nwavel
        ! set cloud fraction
        if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
          cf = cloudAerosolRTMgridS%cldAerFractionAllBands
        else
          cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelHRS%wavel(iwave))
        end if

        do istate = 1, retrS%nstate
          reflDerivHRS%K_lnvmr(iwave, istate) = cf * reflDerivHRS%K_cld_lnvmr(iwave, istate)     &
                                              + (1.0d0 - cf) * reflDerivHRS%K_clr_lnvmr(iwave, istate) 
          reflDerivHRS%K_vmr  (iwave, istate) = cf * reflDerivHRS%K_cld_vmr(iwave, istate)       &
                                              + (1.0d0 - cf) * reflDerivHRS%K_clr_vmr(iwave, istate)
          reflDerivHRS%K_ndens(iwave, istate) = cf * reflDerivHRS%K_cld_ndens(iwave, istate)     &
                                              + (1.0d0 - cf) * reflDerivHRS%K_clr_ndens(iwave, istate)
        end do ! istate
        do istate = 1, reflDerivHRS%nstateCol
          reflDerivHRS%KHR_ndensCol(iwave, istate) = cf * reflDerivHRS%KHR_cld_ndensCol(iwave, istate)  &
                                                   + (1.0d0 - cf) * reflDerivHRS%KHR_clr_ndensCol(iwave, istate)
        end do ! istate
      end do ! iwave

    else ! cloudAerosolRTMgridS%cloudpresent .......

      ! perform calculations only for a cloud-free pixel

      cloudyPart = .false.
      do iwave = 1, wavelHRS%nwavel
        call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelHRS, XsecHRS, RRS_RingS, &
                            controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,    &
                            mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                &
                            surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,             &
                            reflDerivHRS%depolarization(iwave))
        if (errorCheck(errS)) return

        if ( controlS%singleScatteringOnly ) then
          call singleScattering(errS,            &
            optPropRTMGridS,                     & 
            controlS,                            & 
            geometryS,                           &
            surfAlb_iwave,                       & 
            0,                                   &   ! index for Lambertian surface - here ground surface
            reflDerivHRS%babs(iwave),            &   ! output
            reflDerivHRS%bsca(iwave),            &   ! output
            refl_Clear,                          &   ! output 
            contribRefl_Clear,                   &   ! output
            wfInterfkscaGas_Clear,               &   ! output 
            wfInterfkscaAerAbove_Clear,          &   ! output 
            wfInterfkscaAerBelow_Clear,          &   ! output 
            wfInterfkscaCldAbove_Clear,          &   ! output 
            wfInterfkscaCldBelow_Clear,          &   ! output 
            wfInterfkabs_Clear,                  &   ! output
            wfSurfaceAlbedo_Clear,               &   ! output 
            wfSurfaceEmission_Clear,             &   ! output 
            statusRTM)
          if (errorCheck(errS)) return
        else
          call layerBasedOrdersScattering(errS,  &
            optPropRTMGridS,                     & 
            controlS,                            &
            geometryS,                           &
            surfAlb_iwave,                       & 
            0,                                   &   ! index for Lambertian surface - here ground surface
            reflDerivHRS%babs(iwave),            &   ! output
            reflDerivHRS%bsca(iwave),            &   ! output
            refl_Clear,                          &   ! output 
            contribRefl_Clear,                   &   ! output
            UD_Clear,                            &   ! output
            wfInterfkscaGas_Clear,               &   ! output 
            wfInterfkscaAerAbove_Clear,          &   ! output 
            wfInterfkscaAerBelow_Clear,          &   ! output 
            wfInterfkscaCldAbove_Clear,          &   ! output 
            wfInterfkscaCldBelow_Clear,          &   ! output 
            wfInterfkabs_Clear,                  &   ! output
            wfSurfaceAlbedo_Clear,               &   ! output 
            wfSurfaceEmission_Clear,             &   ! output 
            statusRTM)
          if (errorCheck(errS)) return
        end if

        ! calculate altitude resolved aer mass factors before adding surface emission to the reflectance
        reflDerivHRS%altResAMFabs_clr(iwave,:)    = - wfInterfkabs_Clear(:) / refl_Clear(1)
        reflDerivHRS%altResAMFscaAer_clr(iwave,:) =   wfInterfkscaAerAbove_Clear(:) / refl_Clear(1)

        ! add surface emission to the reflectance radiance only as surface emission is unpolarized
        reflDerivHRS%refl_clr(1,iwave) = refl_Clear(1)                                              &
                                       + 1.0d12 * surfEmission_iwave * wfSurfaceEmission_Clear * PI &
                                       / geometryS%u0 / solarIrrS%solIrrMR(iwave)
        do iSV = 2, controlS%dimSV
          reflDerivHRS%refl_clr(iSV,iwave) = refl_Clear(iSV)
        end do
        ! surface emission does not contribute to the reflectance for the Lambertian cloud part
        reflDerivHRS%refl_cld(:,iwave)            = 0.0d0
        reflDerivHRS%refl    (:,iwave)            = reflDerivHRS%refl_clr(:,iwave)

        do iSV = 1, controlS%dimSV
          reflDerivHRS%contribRefl(iSV,iwave,:)   = contribRefl_Clear(iSV,:)
        end do

        ! fill contribution function and internal field for the entire pixel (emission does not contribute)
        do iSV = 1, controlS%dimSV
          imu = (iSV - 1) * geometryS%nmutot + geometryS%nmutot - 1
          imu0 = 2
          reflDerivHRS%contribRefl(iSV,iwave,:)     = contribRefl_Clear(iSV,:)
          do ilevel = 0, reflDerivHRS%RTMnlayer
            reflDerivHRS%intFieldUp  (iSV,iwave,ilevel) = UD_Clear(ilevel)%U(imu, imu0)
            reflDerivHRS%intFieldDown(iSV,iwave,ilevel) = UD_Clear(ilevel)%D(imu, imu0)
          end do ! ilevel
        end do ! iSV

        if ( controlS%calculateDerivatives ) then
          call calculate_K_clr(errS, iband, nTrace, traceGasS, gasPTS,                                 &
                               strayLightS, solarIrrS, cloudAerosolRTMgridS,                     &
                               optPropRTMGridS, retrS, geometryS, wavelHRS, iwave, dn_dnodeS,    &
                               wfInterfkabs_Clear, wfInterfkscaGas_Clear,                        &
                               wfInterfkscaAerAbove_Clear, wfInterfkscaAerBelow_Clear,           &
                               wfSurfaceAlbedo_Clear, wfSurfaceEmission_Clear, wfCloudFraction,  &
                               reflDerivHRS) 
          if (errorCheck(errS)) return
        else
          reflDerivHRS%K_clr_lnvmr(iwave,:)      = 0.0d0
          reflDerivHRS%K_clr_vmr(iwave,:)        = 0.0d0
          reflDerivHRS%K_clr_ndens(iwave,:)      = 0.0d0
          reflDerivHRS%KHR_clr_ndensCol(iwave,:) = 0.0d0
        end if ! controlS%calculateDerivatives
      end do ! iwave

      wfCloudFraction                  = 0.0d0
      reflDerivHRS%refl_cld            = 0.0d0
      reflDerivHRS%altResAMFabs_cld    = 0.0d0
      reflDerivHRS%altResAMFscaAer_cld = 0.0d0
      reflDerivHRS%K_cld_lnvmr         = 0.0d0
      reflDerivHRS%K_cld_vmr           = 0.0d0
      reflDerivHRS%K_cld_ndens         = 0.0d0
      reflDerivHRS%KHR_cld_ndensCol    = 0.0d0

      ! complete the calculation of derivatives for polynomials
      call setPolynomialDerivatives_clr(errS, iband, nTrace, wavelHRS, retrS, cloudAerosolRTMgridS, &
                                        surfaceS, LambCloudS, cldAerFractionS, mulOffsetS, strayLightS, reflDerivHRS)
      if (errorCheck(errS)) return
      ! fill derivatives for the pixel
      do iwave = 1, wavelHRS%nwavel
        do istate = 1, retrS%nstate
          reflDerivHRS%K_lnvmr(iwave, istate) = reflDerivHRS%K_clr_lnvmr(iwave, istate) 
          reflDerivHRS%K_vmr(iwave, istate)   = reflDerivHRS%K_clr_vmr  (iwave, istate)
          reflDerivHRS%K_ndens(iwave, istate) = reflDerivHRS%K_clr_ndens(iwave, istate)
        end do ! istate
        do istate = 1, reflDerivHRS%nstateCol
          reflDerivHRS%KHR_ndensCol(iwave, istate) = reflDerivHRS%KHR_clr_ndensCol(iwave, istate)
        end do ! istate
      end do ! iwave

    end if ! cloudAerosolRTMgridS%cloudpresent .......


    if ( verbose .or. controlS%writeHighResReflAndDeriv) then

      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') &
           ' reflectance on high resolution wavelength grid (not sun-normalized radiance)'
      write(addtionalOutputUnit,'(A)') '      wavel     bsca       babs    depolarization   Stokes parameters for reflectance '
      do iwave = 1, wavelHRS%nwavel
        write(addtionalOutputUnit,'(4F12.6,4E16.7)') wavelHRS%wavel(iwave),    &
                                                     reflDerivHRS%bsca(iwave), &
                                                     reflDerivHRS%babs(iwave), &
                                           reflDerivHRS%depolarization(iwave), &
                     ( reflDerivHRS%refl(iSV,iwave), iSV = 1, controlS%dimSV )
      end do ! iwave

      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') ' derivatives dR(1,1)/dx on high rsolution wavelength grid'
      write(addtionalOutputUnit,'(A, 20A16)') '   wavel    ', &
                      (trim(retrS%codeFitParameters(istate)), istate = 1, reflDerivHRS%nstate)
      do iwave = 1, wavelHRS%nwavel
        write(addtionalOutputUnit,'(F12.6,100E16.7)') wavelHRS%wavel(iwave),           &
              ( reflDerivHRS%K_lnvmr(iwave,istate), istate = 1, reflDerivHRS%nstate )
      end do
    end if

    ! clean up
    call deallocateUD(errS, optPropRTMGridS%RTMnlayer, UD_Clear, UD_Cloud)
    if (errorCheck(errS)) return


  end subroutine calculateReflAndDeriv_lbl


  subroutine calculate_K_clr(errS, iband, nTrace, traceGasS, gasPTS,                         &
                             strayLightS, solarIrrS, cloudAerosolRTMgridS,                   &
                             optPropRTMGridS, retrS, geometryS, wavelHRS, iwave, dn_dnodeS,  &
                             wfInterfkabs_clr, wfInterfkscaGas_clr,                          &
                             wfInterfkscaAerAbove_clr, wfInterfkscaAerBelow_clr,             &
                             wfSurfaceAlbedo_clr, wfSurfaceEmission_clr, wfCloudFraction,    &
                             reflDerivHRS) 

    ! Calculate the derivatives needed in optimal estimation from the derivatives
    ! provided by the radiative transfer calculations for the clear part of the pixel
    ! Subroutines calculate_K_clr and calculate_K_cld have been separated because for DISMAS the wavlengths
    ! for the clear and cloudy part can differ as they are zero points of the differential absorption

    implicit none

    ! input
      type(errorType), intent(inout) :: errS
    integer,                       intent(in)    :: iband                ! number spectral band
    integer,                       intent(in)    :: nTrace               ! number of trace gases
    type(traceGasType),            intent(in)    :: traceGasS(nTrace)    ! atmospheric trace gas properties
    type(gasPTType),               intent(in)    :: gasPTS               ! atmospheric pressure and temperature
    type(straylightType),          intent(in)    :: strayLightS          ! stray light
    type(SolarIrrType),            intent(in)    :: solarIrrS            ! solar irradiance
    type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS ! cloud-aerosol properties
    type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS      ! optical properties on RTM grid
    type(retrType),                intent(in)    :: retrS                ! contains info on the fit parameters
    type(geometryType),            intent(in)    :: geometryS            ! geometrical information
    type(wavelHRType),             intent(in)    :: wavelHRS             ! high resolution wavelength grid
    integer,                       intent(in)    :: iwave                ! current wavelength index
    type(dn_dnodeType),            intent(in)    :: dn_dnodeS(0:nTrace)  ! used for spline interpolation
    real(8),                       intent(in)    :: wfInterfkabs_clr(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)    :: wfInterfkscaGas_clr(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)    :: wfInterfkscaAerAbove_clr(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)    :: wfInterfkscaAerBelow_clr(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)    :: wfSurfaceAlbedo_clr
    real(8),                       intent(in)    :: wfSurfaceEmission_clr
    real(8),                       intent(in)    :: wfCloudFraction
    type(reflDerivType),           intent(inout) :: reflDerivHRS         ! reflectance(R) and derivatives(K) on HR grid


    ! weighting functions constructed from the values returned by layerBasedOrdersScattering
    real(8)    :: wfintervalAerTau_clr
    real(8)    :: wfintervalAerSSA_clr
    real(8)    :: wfintervalTop_clr
    real(8)    :: wfintervalBot_clr
    real(8)    :: wfsurfAltitude
    real(8)    :: wfsurfPressure

    ! weighting functions for the profile when spline interpolation is used
    real(8)    :: wfnode_clr_lnvmr, wfnode_clr_vmr, wfnode_clr_ndens, wfnode_clr_temp

    ! local

    integer    :: ialt, index, istate, iTrace, jTrace, nAlt, statusSplint
    integer    :: ilevel
    integer    :: indexTop, indexBot
    integer    :: startIndex, endIndex, ngaussCol, indexCol

    ! derivatives interpolated to altitudes of the RTM subgrid
    real(8)    :: wfkabs_clr_int(0:optPropRTMGridS%RTMnlayerSub)   ! wfInterfkabs interpolated to sublayer grid
    real(8)    :: wfTemp_clr_int(0:optPropRTMGridS%RTMnlayerSub)   ! d2Rdz/dT/dz on sublayer grid

    ! derivatives interpolated to altitudes of the column grid
    real(8)    :: wfkabs_clr_col(optPropRTMGridS%nGaussCol)        ! wfInterfkabs interpolated to sublayer grid

    real(8)    :: tau_aer_w0                                       ! aerosol optical thickness at w0
    real(8)    :: kabsTot, fraction, kabs, ksca
    real(8)    :: wavelRatio, correctionRefWavel, acAer, wfacAer

    real(8)    :: aer_sca_clr, aer_abs_clr

    real(8)    :: wfcolumn_clr

    real(8), parameter  :: PI = 3.141592653589793d0

    ! Calculate derivatives at the altitudes for the RTM subgrid using linear interpolation.
    ! Spline interpolation gives problems for the ozone profile (large change in the derivatives
    ! with altitude at UV wavelengths) and for clouds in the O2 A band (large change in the
    ! derivative near the top of the cloud - abs(d2R/dkabs/dz) increases strongly and then
    ! decreases strongly over a range of a few hundred meters.

    ! It is more efficient to calculate the derivative at the sublayer grid outside
    ! the loop over the state vector elements. These values are then used for each profile node
    ! and for the aerosol and cloud optical thickness

    do ilevel = 0, optPropRTMGridS%RTMnlayerSub
      wfkabs_clr_int(ilevel) =  splintLin(errS, optPropRTMGridS%RTMaltitude, wfInterfkabs_clr(:), &
                                    optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
    end do

    ! calculate  wfTemp_int = d2R/dT/dz on sublayer grid
    ! d2R/dT/dz = n(z) * d2R/dkabs/dz * dXsec/dT
    ! here n(z) is the number density of the absorbing gas, d2R/dkabs/dz is the basic weighting 
    ! function for absorption, and dXsec/dT is the temperature derivative of the absorption cross section.
    ! Expansion of the layer with temperature is ignored here, but accounted for by updating
    ! the altitude grid when the temperature changes. Note that this implies that we can not
    ! fit the temperature in case of absorption by a collision complex.

    ! d2R/dkabs/dz is dimensionless 
    ! n(z) in molecules cm-3
    ! Xsec in cm2 molecules-1
    ! kabs in cm-1
    ! dXsec/dT in cm2 K-1 molecules-1
    ! for integration we need the result in km-1 as the weights are given in km, hence 
    ! we need a factor 1.0d5 to account for the fact that dz is expressed in km, not in cm.

    wfTemp_clr_int(:) = 0.0d0
    do ilevel = 0, optPropRTMGridS%RTMnlayerSub
      do iTrace = 1, nTrace
        wfTemp_clr_int(ilevel) = wfTemp_clr_int(ilevel) + 1.0d5 * optPropRTMGridS%ndensSubGas(ilevel,iTrace)  &
                               * wfkabs_clr_int(ilevel) * optPropRTMGridS%dXsecdTSubGas(ilevel,iTrace)
      end do ! iTrace
    end do ! ilevel

    ! calculate derivatives at the altitudes for the column grid
    do ilevel = 1, optPropRTMGridS%nGaussCol
      wfkabs_clr_col(ilevel) =  splintLin(errS, optPropRTMGridS%RTMaltitude, wfInterfkabs_clr(:),   &
                                optPropRTMGridS%Colaltitude(ilevel), statusSplint)
    end do

    ! wfkabs_col = d2R/dkabs/dz which is the same for all trace gases
    ! we need d2R/dndens/dz = ColXsec(:,iTrace) * d2R/dkabs/dz for each trace gas
    ! where ColXsec(:,iTrace) is given on the column altitude grid

    ! multiply here K with Gaussian weights because R(n + dn) = R(n) + K dn
    ! and K dn is a summation (matrix vector product)

    ! NOTE: kabs is attenuation per km, weights assume dz is in km => factor 1.0d5
    ngaussCol  = optPropRTMGridS%nGaussCol
    do iTrace = 1, nTrace
      startIndex = (iTrace - 1) * ngaussCol + 1
      endIndex   =  iTrace      * ngaussCol

      reflDerivHRS%KHR_clr_ndensCol(iwave, startIndex:endIndex) = wfkabs_clr_col(1:ngaussCol)  &
                                            * optPropRTMGridS%ColXsec(1:ngaussCol,iTrace)      &
                                            * optPropRTMGridS%Colweight(1:ngaussCol) * 1.0d5
    end do ! iTrace

    ! loop over al the state vector elements
    ! calculate the appropriate derivative
    ! assign the derivative t the K matrix

    ! initialize counter for derivatives for the (sub)columns
    indexCol = nTrace * ngaussCol

    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case('nodeTrace')
            
          iTrace = retrS%codeTraceGas(istate)
          ialt = retrS%codeAltitude(istate)
          nAlt = traceGasS(iTrace)%nalt
          call calculate_wfnode(errS, iTrace, iAlt, nAlt, optPropRTMGridS, dn_dnodeS(iTrace)%dn_dnode,  &
                                  wfkabs_clr_int, wfnode_clr_lnvmr, wfnode_clr_vmr, wfnode_clr_ndens)
          if (errorCheck(errS)) return

          ! assign values
          reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfnode_clr_lnvmr
          reflDerivHRS%K_clr_vmr  (iwave,istate) = wfnode_clr_vmr
          reflDerivHRS%K_clr_ndens(iwave,istate) = wfnode_clr_ndens

       ! Note: if classic DOAS is used the fit parameter is the slant column
       ! Therefore 'slantColum' is added here but the derivative is for the vertical column

        case('columnTrace', 'slantColumn')

          ! calculate dR/dN for the current wavelength

          iTrace = retrS%codeTraceGas(istate)

          wfcolumn_clr = 0.0d0
          do ilevel = 0, optPropRTMGridS%RTMnlayerSub
            ! calculate the total absorption for this level (per km => factor 1.0d-5)
            kabsTot = 0.0d0
            do jTrace = 1, nTrace
               kabsTot = kabsTot + optPropRTMGridS%XsecSubGas (ilevel, jTrace)  &
                                 * optPropRTMGridS%ndensSubGas(ilevel, jTrace)  * 1.0d-5
            end do ! jTrace
            ! calculate the fraction absorption for trace gas iTrace
            if ( kabsTot < 1.0d-50 ) then
              fraction = 0.0d0
            else
              fraction = optPropRTMGridS%XsecSubGas (ilevel, iTrace)    &
                       * optPropRTMGridS%ndensSubGas(ilevel, iTrace)    &
                       * 1.0d-5 / kabsTot
            end if

            ! wfcolumn = int[kabs_i * d2R/dkabs]  
            ! with kabs_i = fraction * kabsTot the absorption by trace gas iTrace

            ! dR/dln(N) = N * dR/dN = N * int[d2R/dn_i/dz * n_i(z)] / N = int[d2R/dkabs/dz * kabs_i(z)]

            wfcolumn_clr = wfcolumn_clr + optPropRTMGridS%RTMweightSub(ilevel) &
                           * wfkabs_clr_int(ilevel) * optPropRTMGridS%kabsSubGas(ilevel) * fraction

          end do ! level

          reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfcolumn_clr / traceGasS(iTrace)%column
          ! put dR/dN, not dR/dln(N), into K_vmr, and K_ndens
          reflDerivHRS%K_clr_vmr  (iwave,istate) = wfcolumn_clr / traceGasS(iTrace)%column
          reflDerivHRS%K_clr_ndens(iwave,istate) = wfcolumn_clr / traceGasS(iTrace)%column

        case('nodeTemp')

            !
            ! dR /dT = int_z [ d2R/dT/dz * dn_dnode ]  where dn_dnode is calculated for the current retrieved
            ! temperature profile and d2R/dT/dz is calculated above and stored in wfTemp_int

            ! dn_dnodeS(0) is for the altitude grid where pressure and temperature are specified, not for a trace gas

            ialt = retrS%codeAltitude(istate)
            nAlt = gasPTS%npressureNodes
            call calculate_wfnodeTemperature(errS, iAlt, nAlt, optPropRTMGridS, dn_dnodeS(0)%dn_dnode,  &
                                             wfTemp_clr_int, wfnode_clr_temp)
            if (errorCheck(errS)) return
            reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfnode_clr_temp
            reflDerivHRS%K_clr_vmr  (iwave,istate) = wfnode_clr_temp
            reflDerivHRS%K_clr_ndens(iwave,istate) = wfnode_clr_temp

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)          

          case('offsetTemp')

            ! dR /d(delta T) = int_z [ d2R/dT/dz ]   where d2R/dT/dz is stored in wfTemp_int

            reflDerivHRS%K_clr_lnvmr(iwave,istate) = dot_product( optPropRTMGridS%RTMweightSub, wfTemp_clr_int )
            reflDerivHRS%K_clr_vmr  (iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate)
            reflDerivHRS%K_clr_ndens(iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate)

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)

          case('surfPressure')

            ! it is assumed that the first interval contains only gas

            wfsurfAltitude = - wfInterfkscaGas_clr(0) * optPropRTMGridS%ksca(0)  &
                             - wfInterfkabs_clr(0)    * optPropRTMGridS%kabs(0)

            wfsurfPressure = - wfsurfAltitude * gasPTS%scaleHeight(0) / gasPTS%pressure(0)
            
            reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfsurfPressure
            reflDerivHRS%K_clr_vmr(iwave,istate)   = wfsurfPressure
            reflDerivHRS%K_clr_ndens(iwave,istate) = wfsurfPressure

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)

          case('surfAlbedo')

            if ( cloudAerosolRTMgridS%useAlbedoLambSurfAllBands ) then

                reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfSurfaceAlbedo_clr
                reflDerivHRS%K_clr_vmr(iwave,istate)   = wfSurfaceAlbedo_clr
                reflDerivHRS%K_clr_ndens(iwave,istate) = wfSurfaceAlbedo_clr

            else
            
              ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

              ! assign values only if the spectral band number 'iband'
              ! agrees with retrS%codeSpecBand(istate)
              if ( iband == retrS%codeSpecBand(istate) ) then
                  
                reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfSurfaceAlbedo_clr
                reflDerivHRS%K_clr_vmr(iwave,istate)   = wfSurfaceAlbedo_clr
                reflDerivHRS%K_clr_ndens(iwave,istate) = wfSurfaceAlbedo_clr

              else

                reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
                reflDerivHRS%K_clr_vmr(iwave,istate)   = 0.0d0
                reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

              end if ! iband == retrS%codeSpecBand(istate)

            end if ! cloudAerosolRTMgridS%useAlbedoLambSurfAllBands

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)

          case('surfEmission')

            ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

            ! assign values only if the spectral band number 'iband'
            ! agrees with retrS%codeSpecBand(istate)
            if ( iband == retrS%codeSpecBand(istate) ) then
                
              reflDerivHRS%K_clr_lnvmr(iwave,istate) = 1.0d12 * wfSurfaceEmission_clr * PI &
                                                     / geometryS%u0 / solarIrrS%solIrrMR(iwave)
              reflDerivHRS%K_clr_vmr(iwave,istate)   = 1.0d12 * wfSurfaceEmission_clr * PI &
                                                     / geometryS%u0 / solarIrrS%solIrrMR(iwave)
              reflDerivHRS%K_clr_ndens(iwave,istate) = 1.0d12 * wfSurfaceEmission_clr * PI &
                                                     / geometryS%u0 / solarIrrS%solIrrMR(iwave)
 
            else

              reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_clr_vmr(iwave,istate)   = 0.0d0
              reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

            end if

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)

          case('LambCldAlbedo')

            ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

            reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
            reflDerivHRS%K_clr_vmr(iwave,istate)   = 0.0d0
            reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)

          case('mulOffset')

            ! Derivatives of the reflectance (not sun-normalized radiance)  w.r.t. the percent 
            ! multiplicative offset of the radiance.
            ! A one percent multiplicative offset for the radiance is equal to a one percent
            ! multiplicative offset for the reflectance.

            ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

            if ( iband == retrS%codeSpecBand(istate) ) then

              reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.01d0 * reflDerivHRS%refl_clr(1,iwave)
              reflDerivHRS%K_clr_vmr(iwave,istate)   = reflDerivHRS%K_clr_lnvmr(iwave,istate)
              reflDerivHRS%K_clr_ndens(iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate)

            else

              reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_clr_vmr(iwave,istate)   = 0.0d0
              reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

            end if

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)


          case('straylight')

            ! derivatives of the reflectance (not radiance) w.r.t. the nodes for straylight
            ! the reflectance is given by reflDerivHRS%refl(iwave,ilos)
            ! the sun-normalized radiance without stray light is given by:
            !     radiance(iwave) = mu0 * reflDerivHRS%refl(iwave,ilos) / PI  
            ! Stray light is given in percent of the radiance:
            !     stray light(iwave)  = 0.01d0 * percentStraylight(iwave) * radiance(iwave)
            !     where percentStraylight(iwave) is found by interpolation on the values for the nodes
            ! the derivative d stray_light / d percentStraylight(index) is thus
            !    0.01d0 * radiance(iwave) * PI / mu0 
            !      * polyDeriv(errS, index, strayLightS%wavel, strayLightS%percentStraylight(wavelength)

            ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

            index = retrS%codeIndexStraylight(istate)

            if ( iband == retrS%codeSpecBand(istate) ) then
              if ( strayLightS%useCharacteristicBias ) then
                reflDerivHRS%K_clr_lnvmr(iwave,istate) =  &
                       0.01d0 *  strayLightS%radiance(index) * strayLightS%radianceCB(iwave) &
                       * PI / geometryS%u0 / solarIrrS%solIrrMR(iwave)
              else
                reflDerivHRS%K_clr_lnvmr(iwave,istate) =  &
                       0.01d0 *  strayLightS%radiance(index) * PI / geometryS%u0 / solarIrrS%solIrrMR(iwave)
              end if
              reflDerivHRS%K_clr_vmr(iwave,istate)   = reflDerivHRS%K_clr_lnvmr(iwave,istate)
              reflDerivHRS%K_clr_ndens(iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate)
            else

              reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_clr_vmr(iwave,istate)   = 0.0d0
              reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

            end if

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)

          case('diffRingCoef')

            ! do nothing here as we do not have a high resolution differentiak Ring spectrum

          case('RingCoef')          

            ! do nothing here similar as for diffRingCoef

          case('aerosolTau')

            call calculate_wfAerTau_clr(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_clr_int, &
                                        wfInterfkscaAerAbove_clr(0:optPropRTMGridS%RTMnlayer),       &
                                        wfInterfkscaAerBelow_clr(0:optPropRTMGridS%RTMnlayer),       &
                                        wfintervalAerTau_clr)
            if (errorCheck(errS)) return

            ! we need the derivative with respect to the aerosol optical thickness
            ! at the reference wavelength, not with respect to the current optical thickness
            wfintervalAerTau_clr = wfintervalAerTau_clr   &
                                 * optPropRTMGridS%kextKextAerdiv550(cloudAerosolRTMgridS%numIntervalFit)

            reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfintervalAerTau_clr
            reflDerivHRS%K_clr_vmr  (iwave,istate) = wfintervalAerTau_clr
            reflDerivHRS%K_clr_ndens(iwave,istate) = wfintervalAerTau_clr

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  wfintervalAerTau_clr


          case('aerosolSSA')

            call calculate_wfAerSSA_clr(errS, optPropRTMGridS, wfkabs_clr_int,                 &
                                        wfInterfkscaAerAbove_clr(0:optPropRTMGridS%RTMnlayer), &
                                        wfInterfkscaAerBelow_clr(0:optPropRTMGridS%RTMnlayer), &
                                        wfintervalAerSSA_clr)
            if (errorCheck(errS)) return

            reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfintervalAerSSA_clr
            reflDerivHRS%K_clr_vmr  (iwave,istate) = wfintervalAerSSA_clr
            reflDerivHRS%K_clr_ndens(iwave,istate) = wfintervalAerSSA_clr

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)

          case('aerosolAC')

            ! use dR/dalpha = dR/dtau * dtau/dalpha
            ! tau(w) = tau(w0) * (w0/w)**alpha
            ! dtau/dalpha = tau(w0) * ln(w0/w) * (w0/w)**alpha

            if ( cloudAerosolRTMgridS%useHGScatAer ) then

              call calculate_wfAerTau_clr(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_clr_int, &
                                          wfInterfkscaAerAbove_clr(0:optPropRTMGridS%RTMnlayer),       &
                                          wfInterfkscaAerBelow_clr(0:optPropRTMGridS%RTMnlayer),       &
                                          wfintervalAerTau_clr)
              if (errorCheck(errS)) return

              ! tau_aer_w0 is the optical thickness at the reference wavelength
              tau_aer_w0 = cloudAerosolRTMgridS%intervalAerTau(cloudAerosolRTMgridS%numIntervalFit)
              acAer = cloudAerosolRTMgridS%intervalAerAC(cloudAerosolRTMgridS%numIntervalFit)
              wavelRatio = cloudAerosolRTMgridS%w0 / wavelHRS%wavel(iwave)
              correctionRefWavel = tau_aer_w0 * log(wavelRatio) * wavelRatio**acAer
              wfacAer = wfintervalAerTau_clr * correctionRefWavel
              reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfacAer  ! clear part of the pixel
              reflDerivHRS%K_clr_vmr  (iwave,istate) = wfacAer
              reflDerivHRS%K_clr_ndens(iwave,istate) = wfacAer

            else

              ! for Mie scattering aerosol the Angstrom coefficient can not be fitted
              reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_clr_vmr  (iwave,istate) = 0.0d0
              reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

            end if
           
            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)

          case('cloudTau')

            reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
            reflDerivHRS%K_clr_vmr  (iwave,istate) = 0.0d0
            reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  0.0d0

          case('cloudAC')

            reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
            reflDerivHRS%K_clr_vmr  (iwave,istate) = 0.0d0
            reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  0.0d0

          case('cloudFraction')

            ! Strictly: The derivative for the cloud fraction is not defined here
            ! as we considere only the cloud free part.
            ! Hoever, when we set
            !     reflDerivHRS%K_cld_lnvmr = wfCloudFraction
            !     reflDerivHRS%K_cld_lnvmr = wfCloudFraction
            ! and combine
            !     cf * reflDerivHRS%K_cld_lnvmr and (1 - cf) *  reflDerivHRS%K_clr_lnvmr
            ! we get the proper wfCloudFraction

            if ( cloudAerosolRTMgridS%useCldAerFractionAllBands ) then

                reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfCloudFraction
                reflDerivHRS%K_clr_vmr(iwave,istate)   = wfCloudFraction
                reflDerivHRS%K_clr_ndens(iwave,istate) = wfCloudFraction

            else
            
              ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

              ! assign values only if the spectral band number 'iband'
              ! agrees with retrS%codeSpecBand(istate)
              if ( iband == retrS%codeSpecBand(istate) ) then
                  
                reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfCloudFraction
                reflDerivHRS%K_clr_vmr(iwave,istate)   = wfCloudFraction
                reflDerivHRS%K_clr_ndens(iwave,istate) = wfCloudFraction

              else

                reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
                reflDerivHRS%K_clr_vmr(iwave,istate)   = 0.0d0
                reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

              end if ! iband == retrS%codeSpecBand(istate)

            end if ! cloudAerosolRTMgridS%useCldAerFractionAllBands

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)          

          case('intervalDP')

              
            indexTop = optPropRTMGridS%indexRTMFitIntTop
            indexBot = optPropRTMGridS%indexRTMFitIntBot

            aer_sca_clr = ( wfInterfkscaAerBelow_clr(indexTop) - wfInterfkscaAerAbove_clr(indexBot) ) &
                        * optPropRTMGridS%kscaIntBelowAer(cloudAerosolRTMgridS%numIntervalFit)
            aer_abs_clr = ( wfInterfkabs_clr(indexTop) - wfInterfkabs_clr(indexBot) )                 &
                        * optPropRTMGridS%kabsIntBelowAer(cloudAerosolRTMgridS%numIntervalFit)

            wfintervalTop_clr = aer_sca_clr + aer_abs_clr

            reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfintervalTop_clr
            reflDerivHRS%K_clr_vmr  (iwave,istate) = wfintervalTop_clr
            reflDerivHRS%K_clr_ndens(iwave,istate) = wfintervalTop_clr

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  wfintervalTop_clr

          case('intervalTop')

            if ( cloudAerosolRTMgridS%aerosolPresent ) then

              call calculate_wfAerTau_clr(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_clr_int, &
                                          wfInterfkscaAerAbove_clr(0:optPropRTMGridS%RTMnlayer), &
                                          wfInterfkscaAerBelow_clr(0:optPropRTMGridS%RTMnlayer), &
                                          wfintervalAerTau_clr)
              if (errorCheck(errS)) return

              indexTop = optPropRTMGridS%indexRTMFitIntTop

              ksca = optPropRTMGridS%kscaIntBelowAer(cloudAerosolRTMgridS%numIntervalFit)
              kabs = optPropRTMGridS%kabsIntBelowAer(cloudAerosolRTMgridS%numIntervalFit)

              aer_sca_clr = wfInterfkscaAerBelow_clr(indexTop) * ksca
              aer_abs_clr = wfInterfkabs_clr(indexTop) * kabs

              wfintervalTop_clr = aer_sca_clr + aer_abs_clr - wfintervalAerTau_clr * (ksca + kabs)

              reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfintervalTop_clr
              reflDerivHRS%K_clr_vmr  (iwave,istate) = wfintervalTop_clr
              reflDerivHRS%K_clr_ndens(iwave,istate) = wfintervalTop_clr

              ! derivative for the (sub)columns
              indexCol = indexCol + 1
              reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  wfintervalTop_clr

            else

              wfintervalTop_clr = 0.0d0
              reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfintervalTop_clr
              reflDerivHRS%K_clr_vmr  (iwave,istate) = wfintervalTop_clr
              reflDerivHRS%K_clr_ndens(iwave,istate) = wfintervalTop_clr

              ! derivative for the (sub)columns
              indexCol = indexCol + 1
              reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  wfintervalTop_clr

            end if ! cloudAerosolRTMgridSimS%aerosolPresent

          case('intervalBot')

            if ( cloudAerosolRTMgridS%aerosolPresent ) then

              call calculate_wfAerTau_clr(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_clr_int, &
                                          wfInterfkscaAerAbove_clr(0:optPropRTMGridS%RTMnlayer), &
                                          wfInterfkscaAerBelow_clr(0:optPropRTMGridS%RTMnlayer), &
                                          wfintervalAerTau_clr)
              if (errorCheck(errS)) return

              indexBot = optPropRTMGridS%indexRTMFitIntBot

              ksca = optPropRTMGridS%kscaIntAboveAer(cloudAerosolRTMgridS%numIntervalFit - 1)
              kabs = optPropRTMGridS%kabsIntAboveAer(cloudAerosolRTMgridS%numIntervalFit - 1)

              aer_sca_clr = wfInterfkscaAerAbove_clr(indexBot) * ksca
              aer_abs_clr = wfInterfkabs_clr(indexBot) * kabs

              wfintervalBot_clr = - aer_sca_clr - aer_abs_clr + wfintervalAerTau_clr * (ksca + kabs)

              reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfintervalBot_clr
              reflDerivHRS%K_clr_vmr  (iwave,istate) = wfintervalBot_clr
              reflDerivHRS%K_clr_ndens(iwave,istate) = wfintervalBot_clr

              ! derivative for the (sub)columns
              indexCol = indexCol + 1
              reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  wfintervalBot_clr

            else

              wfintervalBot_clr = 0.0d0
              reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfintervalBot_clr
              reflDerivHRS%K_clr_vmr  (iwave,istate) = wfintervalBot_clr
              reflDerivHRS%K_clr_ndens(iwave,istate) = wfintervalBot_clr

              ! derivative for the (sub)columns
              indexCol = indexCol + 1
              reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) =  wfintervalBot_clr

            end if ! cloudAerosolRTMgridSimS%aerosolPresent
             

          case('lnpolyCoef')
            ! do nothing here
            ! needed if DOAS is used in the retrieval

          case default

            call logDebug('in subroutine calculate_K_clr in radianceIrradianceModule')
            call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return

        end select

      end do ! istate loop

  end subroutine calculate_K_clr


  subroutine calculate_K_cld(errS, iband, nTrace, cfMin, cfMax, traceGasS, gasPTS, LambCloudS,    &
                             strayLightS, solarIrrS, cloudAerosolRTMgridS,                  &
                             optPropRTMGridS,retrS, geometryS, wavelHRS, iwave, dn_dnodeS,  &
                             wfInterfkabs_cld, wfInterfkscaGas_cld,                         &
                             wfInterfkscaAerAbove_cld, wfInterfkscaAerBelow_cld,            &
                             wfInterfkscaCldAbove_cld, wfInterfkscaCldBelow_cld,            &
                             wfSurfaceAlbedo_cld, wfSurfaceEmission_cld, wfCloudAlbedo,     &
                             wfCloudFraction, reflDerivHRS)
                   
    ! Calculate the derivatives needed in optimal estimation from the derivatives
    ! provided by the radiative transfer calculations for the cloudy part of the pixel.
    ! Subroutines calculate_K_clr and calculate_K_cld have been separated because for DISMAS the wavlengths
    ! for the clear and cloudy part can differ as they are zero points of the differential absorption.

    implicit none

    ! input
      type(errorType), intent(inout) :: errS
    integer,                       intent(in)  :: iband                ! number spectral band
    integer,                       intent(in)  :: nTrace               ! number of trace gases
    real(8),                       intent(in)  :: cfMin, cfMax         ! minimal and maximal cloud fractions
    type(traceGasType),            intent(in)  :: traceGasS(nTrace)    ! atmospheric trace gas properties
    type(gasPTType),               intent(in)  :: gasPTS               ! atmospheric pressure and temperature
    type(LambertianType),          intent(in)  :: LambCloudS           ! Lambertian cloud properties
    type(straylightType),          intent(in)  :: strayLightS          ! stray light
    type(SolarIrrType),            intent(in)  :: solarIrrS            ! solar irradiance
    type(cloudAerosolRTMgridType), intent(in)  :: cloudAerosolRTMgridS ! cloud-aerosol properties
    type(optPropRTMGridType),      intent(in)  :: optPropRTMGridS      ! optical properties on RTM grid
    type(retrType),                intent(in)  :: retrS                ! contains info on the fit parameters
    type(geometryType),            intent(in)  :: geometryS            ! geometrical information
    type(wavelHRType),             intent(in)  :: wavelHRS             ! high resolution wavelength grid
    integer,                       intent(in)  :: iwave                ! current wavelength index
    type(dn_dnodeType),            intent(in)  :: dn_dnodeS(0:nTrace)  ! account for interpolations

    ! derivatives at RTM grid
    real(8),                       intent(in)  :: wfInterfkabs_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)  :: wfInterfkscaGas_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)  :: wfInterfkscaAerAbove_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)  :: wfInterfkscaAerBelow_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)  :: wfInterfkscaCldAbove_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)  :: wfInterfkscaCldBelow_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)  :: wfSurfaceAlbedo_cld
    real(8),                       intent(in)  :: wfSurfaceEmission_cld
    real(8),                       intent(in)  :: wfCloudAlbedo
    real(8),                       intent(in)  :: wfCloudFraction

    type(reflDerivType),        intent(inout)  :: reflDerivHRS         ! reflectance(R) and derivatives(K) on HR grid

    ! weighting functions constructed from the values returned by layerBasedOrdersScattering
    real(8)    :: wfintervalAerTau_cld
    real(8)    :: wfintervalAerSSA_cld
    real(8)    :: wfintervalCldTau
    real(8)    :: wfintervalTop_cld
    real(8)    :: wfintervalBot_cld
    real(8)    :: wfsurfAltitude
    real(8)    :: wfsurfPressure

    ! weighting functions for the profile when spline interpolation is used
    real(8)    :: wfnode_cld_lnvmr, wfnode_cld_vmr, wfnode_cld_ndens, wfnode_cld_temp

    ! local

    integer    :: ialt, index, istate, iTrace, jTrace, nAlt, statusSplint
    integer    :: ilevel
    integer    :: indexTop, indexBot
    integer    :: startIndex, endIndex, ngaussCol, indexCol

    real(8), allocatable    :: RTMalt(:)            ! altitudes on RTM grid for the fit interval or above cloud
    real(8), allocatable    :: wfk_Lambcld(:)       ! temporary array for spline interpolation
    real(8), allocatable    :: SDwfk_Lambcld(:)     ! temporary array for spline interpolation

    ! derivatives interpolated to altitudes of the RTM subgrid
    real(8)    :: wfkabs_cld_int(0:optPropRTMGridS%RTMnlayerSub)   ! wfInterfkabs interpolated to sublayer grid
    real(8)    :: wfTemp_cld_int(0:optPropRTMGridS%RTMnlayerSub)   ! d2Rdz/dT/dz on sublayer grid

    ! derivatives interpolated to altitudes of the column grid
    real(8)    :: wfkabs_cld_col(optPropRTMGridS%nGaussCol)        ! wfInterfkabs interpolated to sublayer grid

    real(8)    :: altCld                                 ! cloud fraction and altitude of Lambertian cloud
    real(8)    :: tau_aer_w0, tau_cld_w0                     ! optical thickness at w0
    real(8)    :: kabsTot, fraction, kabs, ksca, kabs_aer, kabs_cld, ksca_aer, ksca_cld
    real(8)    :: wavelRatio, correctionRefWavel
    real(8)    :: acAer, wfacAer
    real(8)    :: acCld, wfacCld

    real(8)    :: gas_sca_cld, gas_abs_cld
    real(8)    :: aer_sca_cld, aer_abs_cld
    real(8)    :: cld_sca_cld, cld_abs_cld

    real(8)    :: wfcolumn_cld

    integer    :: length

    real(8), parameter  :: PI = 3.141592653589793d0

    integer  :: allocStatus, deallocStatus
    integer  :: sumallocStatus, sumdeallocStatus

    ! Calculate derivatives at the altitudes for the RTM subgrid using linear interpolation.
    ! Spline interpolation gives problems for the ozone profile (large change in the derivatives
    ! with altitude at UV wavelengths) and for clouds in the O2 A band (large change in the
    ! derivative near the top of the cloud - abs(d2R/dkabs/dz) increases strongly and then
    ! dcreases strongly over a range of a few hundred meters.

    ! For Lambertian clouds we have a discontinuity at the cloud => limit range to the atmosphere
    ! above the cloud.

    ! for a Lambertian cloud the derivative is discontineous across the cloud
    ! interpolate only above the Lambertian cloud and set the values below the cloud to zero

    if ( cloudAerosolRTMgridS%useLambertianCloud ) then

      length = optPropRTMGridS%RTMnlayer - optPropRTMGridS%indexRTMFitIntTop + 1

      allocStatus    = 0
      sumallocStatus = 0

      allocate( RTMalt(length), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus
      allocate( wfk_Lambcld(length), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus
      allocate( SDwfk_Lambcld(length), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      if ( sumallocStatus /= 0 ) then
        call logDebug('FATAL ERROR: allocation failed')
        call logDebug('forRTMalt, wfk_Lambcld or SDwfk_Lambcld')
        call logDebug('in subroutine calculate_K')
        call logDebug('in module radianceIrradianceModule')
        call mystop(errS, 'stopped because allocation failed')
        if (errorCheck(errS)) return
      end if

      RTMalt = optPropRTMGridS%RTMaltitude(optPropRTMGridS%indexRTMFitIntTop:optPropRTMGridS%RTMnlayer)

      ! interpolation for wfkabs

      wfk_Lambcld = wfInterfkabs_cld(optPropRTMGridS%indexRTMFitIntTop:optPropRTMGridS%RTMnlayer)

      wfkabs_cld_int = 0.0d0
      do ilevel = optPropRTMGridS%indexRTMSubFitIntTop, optPropRTMGridS%RTMnlayerSub
        wfkabs_cld_int(ilevel) =  splintLin(errS, RTMalt, wfk_Lambcld,  &
                                      optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
      end do

      deallocStatus    = 0
      sumdeallocStatus = 0

      deallocate( RTMalt, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      deallocate( wfk_Lambcld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      deallocate( SDwfk_Lambcld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus

      if ( sumdeallocStatus /= 0 ) then
        call logDebug('FATAL ERROR: deallocation failed')
        call logDebug('forRTMalt, wfk_Lambcld or SDwfk_Lambcld')
        call logDebug('in subroutine calculate_K')
        call logDebug('in module radianceIrradianceModule')
        call mystop(errS, 'stopped because deallocation failed')
        if (errorCheck(errS)) return
      end if

    else  ! cloudAerosolRTMgridS%useLambertianCloud

      do ilevel = 0, optPropRTMGridS%RTMnlayerSub
        wfkabs_cld_int(ilevel) =  splintLin(errS, optPropRTMGridS%RTMaltitude, wfInterfkabs_cld(:), &
                optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
      end do

    end if ! cloudAerosolRTMgridS%useLambertianCloud

    ! calculate  wfTemp_int = d2R/dT/dz on sublayer grid
    ! d2R/dT/dz = n(z) * d2R/dkabs/dz * dXsec/dT
    ! here n(z) is the number density of the absorbing gas, d2R/dkabs/dz is the basic weighting 
    ! function for absorption, and dXsec/dT is the temperature derivative of the absorption cross section.
    ! Expansion of the layer with temperature is ignored here, but accounted for by updating
    ! the altitude grid when the temperature changes. Note that this implies that we can not
    ! fit the temperature in case of absorption by a collision complex.

    ! d2R/dkabs/dz is dimensionless 
    ! n(z) in molecules cm-3
    ! Xsec in cm2 molecules-1
    ! kabs in cm-1
    ! dXsec/dT in cm2 K-1 molecules-1
    ! for integration we need the result in km-1 as the weights are given in km, hence 
    ! we need a factor 1.0d5 to account for the fact that dz is expressed in km, not in cm.
    
    wfTemp_cld_int(:) = 0.0d0
    do ilevel = 0, optPropRTMGridS%RTMnlayerSub
      do iTrace = 1, nTrace
        wfTemp_cld_int(ilevel) = wfTemp_cld_int(ilevel) + 1.0d5 * optPropRTMGridS%ndensSubGas(ilevel,iTrace)  &
                               * wfkabs_cld_int(ilevel) * optPropRTMGridS%dXsecdTSubGas(ilevel,iTrace)
      end do ! iTrace
    end do ! ilevel

    ! calculate derivatives at the altitudes for the column grid

    ! for a Lambertian cloud the derivative is discontineous across the cloud
    ! interpolate only above the Lambertian cloud and set the values below the cloud to zero

    if ( cloudAerosolRTMgridS%useLambertianCloud ) then

      length = optPropRTMGridS%RTMnlayer - optPropRTMGridS%indexRTMFitIntTop + 1

      allocStatus    = 0
      sumallocStatus = 0

      allocate( RTMalt(length), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus
      allocate( wfk_Lambcld(length), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus
      allocate( SDwfk_Lambcld(length), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      if ( sumallocStatus /= 0 ) then
        call logDebug('FATAL ERROR: allocation failed')
        call logDebug('forRTMalt, wfk_Lambcld or SDwfk_Lambcld')
        call logDebug('in subroutine calculate_K - column properties')
        call logDebug('in module radianceIrradianceModule')
        call mystop(errS, 'stopped because allocation failed')
        if (errorCheck(errS)) return
      end if

      RTMalt      = optPropRTMGridS%RTMaltitude(optPropRTMGridS%indexRTMFitIntTop:optPropRTMGridS%RTMnlayer)
      wfk_Lambcld = wfInterfkabs_cld(optPropRTMGridS%indexRTMFitIntTop:optPropRTMGridS%RTMnlayer)

      altCld      = optPropRTMGridS%RTMaltitude(optPropRTMGridS%indexRTMFitIntTop)

      do ilevel = 1, optPropRTMGridS%nGaussCol
        if ( optPropRTMGridS%Colaltitude(ilevel) > altCld ) then
          wfkabs_cld_col(ilevel) =  splintLin(errS, RTMalt, wfk_Lambcld, optPropRTMGridS%Colaltitude(ilevel), statusSplint)
        else
          wfkabs_cld_col(ilevel) = 0.0d0
        end if
      end do

      deallocStatus    = 0
      sumdeallocStatus = 0

      deallocate( RTMalt, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      deallocate( wfk_Lambcld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      deallocate( SDwfk_Lambcld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus

      if ( sumdeallocStatus /= 0 ) then
        call logDebug('FATAL ERROR: deallocation failed')
        call logDebug('forRTMalt, wfk_Lambcld or SDwfk_Lambcld')
        call logDebug('in subroutine calculate_K - column properties')
        call logDebug('in module radianceIrradianceModule')
        call mystop(errS, 'stopped because deallocation failed')
        if (errorCheck(errS)) return
      end if

    else ! cloudAerosolRTMgridS%useLambertianCloud

      do ilevel = 1, optPropRTMGridS%nGaussCol
        wfkabs_cld_col(ilevel) =  splintLin(errS, optPropRTMGridS%RTMaltitude, wfInterfkabs_cld(:), &
                                  optPropRTMGridS%Colaltitude(ilevel), statusSplint)
      end do

    end if ! cloudAerosolRTMgridS%useLambertianCloud

    ! wfkabs_col = d2R/dkabs/dz which is the same for all trace gases
    ! we need d2R/dndens/dz = ColXsec(:,iTrace) * d2R/dkabs/dz for each trace gas
    ! where ColXsec(:,iTrace) is given on the column altitude grid

    ! multiply here K with Gaussian weights because R(n + dn) = R(n) + K dn
    ! and K dn is a summation (matrix vector product)

    ! NOTE: kabs is attenuation per km, weights assume dz is in km => factor 1.0d5
    ngaussCol  = optPropRTMGridS%nGaussCol
    do iTrace = 1, nTrace
      startIndex = (iTrace - 1) * ngaussCol + 1
      endIndex   =  iTrace      * ngaussCol

      reflDerivHRS%KHR_cld_ndensCol(iwave, startIndex:endIndex) = wfkabs_cld_col(1:ngaussCol)     &
                                         * optPropRTMGridS%ColXsec(1:ngaussCol,iTrace)    &
                                         * optPropRTMGridS%Colweight(1:ngaussCol) * 1.0d5
    end do ! iTrace

    ! loop over al the state vector elements
    ! calculate the appropriate derivative
    ! assign the derivative t the K matrix

    ! initialize counter for derivatives for the (sub)columns
    indexCol = nTrace * ngaussCol

    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case('nodeTrace')
            
          iTrace = retrS%codeTraceGas(istate)
          ialt = retrS%codeAltitude(istate)
          nAlt = traceGasS(iTrace)%nalt
          call calculate_wfnode(errS, iTrace, iAlt, nAlt, optPropRTMGridS, dn_dnodeS(iTrace)%dn_dnode,  &
                                wfkabs_cld_int, wfnode_cld_lnvmr, wfnode_cld_vmr, wfnode_cld_ndens)
          if (errorCheck(errS)) return

          ! assign values
          reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfnode_cld_lnvmr
          reflDerivHRS%K_cld_vmr  (iwave,istate) = wfnode_cld_vmr
          reflDerivHRS%K_cld_ndens(iwave,istate) = wfnode_cld_ndens

        case('columnTrace', 'slantColumn')

          ! calculate dR/dN for the current wavelength

          iTrace = retrS%codeTraceGas(istate)

          wfcolumn_cld = 0.0d0
          do ilevel = 0, optPropRTMGridS%RTMnlayerSub
            ! calculate the total absorption for this level (per km => factor 1.0d-5)
            kabsTot = 0.0d0
            do jTrace = 1, nTrace
               kabsTot = kabsTot + optPropRTMGridS%XsecSubGas (ilevel, jTrace)  &
                                 * optPropRTMGridS%ndensSubGas(ilevel, jTrace)  * 1.0d-5
            end do ! jTrace
            ! calculate the fraction absorption for trace gas iTrace
            if ( kabsTot < 1.0d-50 ) then
              fraction = 0.0d0
            else
              fraction = optPropRTMGridS%XsecSubGas (ilevel, iTrace)    &
                       * optPropRTMGridS%ndensSubGas(ilevel, iTrace)    &
                       * 1.0d-5 / kabsTot
            end if

            ! wfcolumn = int[kabs_i * d2R/dkabs]  
            ! with kabs_i = fraction * kabsTot the absorption by trace gas iTrace

            ! dR/dln(N) = N * dR/dN = N * int[d2R/dn_i/dz * n_i(z)] / N = int[d2R/dkabs/dz * kabs_i(z)]

            wfcolumn_cld = wfcolumn_cld + optPropRTMGridS%RTMweightSub(ilevel) &
                           * wfkabs_cld_int(ilevel) * optPropRTMGridS%kabsSubGas(ilevel) * fraction

          end do ! level

          reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfcolumn_cld / traceGasS(iTrace)%column
          ! put dR/dN, not dR/dln(N), into K_vmr, and K_ndens
          reflDerivHRS%K_cld_vmr  (iwave,istate) = wfcolumn_cld / traceGasS(iTrace)%column
          reflDerivHRS%K_cld_ndens(iwave,istate) = wfcolumn_cld / traceGasS(iTrace)%column

        case('nodeTemp')

            !
            ! dR /dT = int_z [ d2R/dT/dz * dn_dnode ]  where dn_dnode is calculated for the current retrieved
            ! temperature profile and d2R/dT/dz is calculated above and stored in wfTemp_int

            ! dn_dnodeS(0) is for the altitude grid where pressure and temperature are specified, not for a trace gas

            ialt = retrS%codeAltitude(istate)
            nAlt = gasPTS%npressureNodes
            call calculate_wfnodeTemperature(errS, iAlt, nAlt, optPropRTMGridS, dn_dnodeS(0)%dn_dnode,  &
                                             wfTemp_cld_int, wfnode_cld_temp)
            if (errorCheck(errS)) return

            reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfnode_cld_temp
            reflDerivHRS%K_cld_vmr  (iwave,istate) = wfnode_cld_temp
            reflDerivHRS%K_cld_ndens(iwave,istate) = wfnode_cld_temp

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)          

          case('offsetTemp')

            ! dR /d(delta T) = int_z [ d2R/dT/dz ]   where d2R/dT/dz is stored in wfTemp_int

            reflDerivHRS%K_cld_lnvmr(iwave,istate) = dot_product( optPropRTMGridS%RTMweightSub, wfTemp_cld_int )
            reflDerivHRS%K_cld_vmr  (iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate)
            reflDerivHRS%K_cld_ndens(iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate)

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)          

          case('surfPressure')

            ! it is assumed that the first interval contains only gas

            wfsurfAltitude = - wfInterfkscaGas_cld(0) * optPropRTMGridS%ksca(0)  &
                             - wfInterfkabs_cld(0)    * optPropRTMGridS%kabs(0)

            wfsurfPressure = - wfsurfAltitude * gasPTS%scaleHeight(0) / gasPTS%pressure(0)
            
            reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfsurfPressure
            reflDerivHRS%K_cld_vmr(iwave,istate)   = wfsurfPressure
            reflDerivHRS%K_cld_ndens(iwave,istate) = wfsurfPressure

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)

          case('surfAlbedo')

            ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

            ! assign values only if the spectral band number 'iband'
            ! agrees with retrS%codeSpecBand(istate)
            if ( iband == retrS%codeSpecBand(istate) ) then
                
              if ( cloudAerosolRTMgridS%useLambertianCloud ) then

                reflDerivHRS%K_cld_lnvmr(iwave,istate) =  0.0d0  ! changing the surface albedo does not affect
                                                                 ! the reflectance as the surface is below the cloud

              else ! a scattering cloud

                if ( ( cfMin > cldFractionBlockUpdateSurfAlb ) .and. &
                     ( sum( cloudAerosolRTMgridS%intervalCldTau(:) ) > OptThicknBlockUpdateSurfAlb ) ) then
                  reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
                else 
                  reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfSurfaceAlbedo_cld
                end if
                     
              end if

              reflDerivHRS%K_cld_vmr(iwave,istate)   = reflDerivHRS%K_cld_lnvmr(iwave,istate)            
              reflDerivHRS%K_cld_ndens(iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate) 

            else

              reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_vmr(iwave,istate)   = 0.0d0
              reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0

            end if

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)

          case('surfEmission')

            ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

            ! assign values only if the spectral band number 'iband'
            ! agrees with retrS%codeSpecBand(istate)
            if ( iband == retrS%codeSpecBand(istate) ) then

              if ( cloudAerosolRTMgridS%useLambertianCloud ) then

                reflDerivHRS%K_cld_lnvmr(iwave,istate) =  0.0d0

              else ! a scattering cloud

                if ( ( cfMin > cldFractionBlockUpdateSurfAlb ) .and. &
                     ( sum( cloudAerosolRTMgridS%intervalCldTau(:) ) > OptThicknBlockUpdateSurfAlb ) ) then
                  reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
                else 
                  reflDerivHRS%K_cld_lnvmr(iwave,istate) = 1.0d12 * wfSurfaceEmission_cld * PI      &
                                                         / geometryS%u0 / solarIrrS%solIrrMR(iwave)
                end if
                     
              end if

              reflDerivHRS%K_cld_vmr(iwave,istate)   = reflDerivHRS%K_cld_lnvmr(iwave,istate)            
              reflDerivHRS%K_cld_ndens(iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate) 
 
            else

              reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_vmr(iwave,istate)   = 0.0d0
              reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0

            end if

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)

          case('LambCldAlbedo')

            ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

            if ( cloudAerosolRTMgridS%useAlbedoLambCldAllBands ) then

              ! one cloud albedo for all wavelengths and all spectral bands

              if ( ( cfMax < cldFractionBlockUpdateCldPram ) .or. (.not. LambCloudS%updateAlbedo ) ) then
                ! cloud fraction is too small for updating cloud parameters => set derivative to zero
                 reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
                 reflDerivHRS%K_cld_vmr(iwave,istate)   = 0.0d0
                 reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0
              else
                reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfCloudAlbedo
                reflDerivHRS%K_cld_vmr  (iwave,istate) = wfCloudAlbedo
                reflDerivHRS%K_cld_ndens(iwave,istate) = wfCloudAlbedo
              end if 

            else

              ! cloud albedo can differ for spectral bands and can be a function of the wavelength

              index = retrS%codeIndexLambCldAlb(istate)

              ! assign values only if the spectral band number 'iband'
              ! agrees with retrS%codeSpecBand(istate)
              if ( iband == retrS%codeSpecBand(istate) ) then

                if ( ( cfMax < cldFractionBlockUpdateCldPram ) .or. (.not. LambCloudS%updateAlbedo ) ) then
                  ! cloud fraction too small to update albedo
                  reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
                  reflDerivHRS%K_cld_vmr  (iwave,istate) = 0.0d0
                  reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0
                else
                  reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfCloudAlbedo
                  reflDerivHRS%K_cld_vmr(iwave,istate)   = wfCloudAlbedo
                  reflDerivHRS%K_cld_ndens(iwave,istate) = wfCloudAlbedo
                end if

              else

                reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
                reflDerivHRS%K_cld_vmr(iwave,istate)   = 0.0d0
                reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0

              end if  ! iband == retrS%codeSpecBand(istate)

            end if ! cloudAerosolRTMgridS%useAlbedoLambCldAllBands

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)          

          case('mulOffset')

            ! Derivatives of the reflectance (not sun-normalized radiance)  w.r.t. the percent 
            ! multiplicative offset of the radiance.
            ! A one percent multiplicative offset for the radiance is equal to a one percent
            ! multiplicative offset for the reflectance.

            ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

            if ( iband == retrS%codeSpecBand(istate) ) then

              reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.01d0 * reflDerivHRS%refl_cld(1,iwave)
              reflDerivHRS%K_cld_vmr(iwave,istate)   = reflDerivHRS%K_cld_lnvmr(iwave,istate)
              reflDerivHRS%K_cld_ndens(iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate)

            else

              reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_vmr(iwave,istate)   = 0.0d0
              reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0

            end if

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)


          case('straylight')

            ! derivatives of the reflectance (not radiance) w.r.t. the nodes for straylight
            ! the reflectance is given by reflDerivHRS%refl(iwave,ilos)
            ! the radiance without stray light is given by:
            !     radiance(iwave) = mu0 * F0 * reflDerivHRS%refl(iwave,ilos) / PI  
            !     where F0 is the solar irradiance measured perpendicular to the incident beam
            ! Stray light is given in percent of the radiance:
            !     stray light(iwave)  = 0.01d0 * percentStraylight(iwave) * radiance(iwave)
            !     where percentStraylight(iwave) is found by interpolation on the values for the nodes
            ! stray light can thus be expressed as
            !     stray_light = 0.01d0 * percentStraylight(iwave) * mu0 * F0 * reflDerivHRS%refl(iwave,ilos) / PI
            ! stray light in terms of sun-normalized radiance is (divide by F0)
            !    stray_light_refl  =  0.01d0 * percentStraylight(iwave) * mu0 * reflDerivHRS%refl(iwave,ilos) / PI
            ! the derivative d stray_light_refl / d percentStraylight(index) is thus
            !    0.01d0 * reflDerivHRS%refl(iwave,ilos) * 
            !        polyDeriv(errS, index, strayLightS%wavel, strayLightS%percentStraylight, wavelength)

            ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

            index = retrS%codeIndexStraylight(istate)

            if ( iband == retrS%codeSpecBand(istate) ) then
              if ( strayLightS%useCharacteristicBias ) then
                reflDerivHRS%K_cld_lnvmr(iwave,istate) =  &
                       0.01d0 *  strayLightS%radiance(index) * strayLightS%radianceCB(iwave) &
                       * PI / geometryS%u0 / solarIrrS%solIrrMR(iwave)
              else
                reflDerivHRS%K_cld_lnvmr(iwave,istate) =  &
                       0.01d0 *  strayLightS%radiance(index) * PI / geometryS%u0 / solarIrrS%solIrrMR(iwave)
              end if
              reflDerivHRS%K_cld_vmr(iwave,istate)   = reflDerivHRS%K_cld_lnvmr(iwave,istate)
              reflDerivHRS%K_cld_ndens(iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate)

            else

              reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_vmr(iwave,istate)   = 0.0d0
              reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0

            end if

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)          

          case('diffRingCoef')

            ! do nothing here as we do not have a high resolution differential Ring spectrum
            ! and it is difficult to calculate

          case('RingCoef')          

            ! do nothing here similar as for 'diffRingCoef'

          case('aerosolTau')

            call calculate_wfAerTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int, &
                                        wfInterfkscaAerAbove_cld(0:optPropRTMGridS%RTMnlayer), &
                                        wfInterfkscaAerBelow_cld(0:optPropRTMGridS%RTMnlayer), &
                                        wfintervalAerTau_cld)
            if (errorCheck(errS)) return

            ! we need the derivative with respect to the aerosol optical thickness
            ! at the reference wavelength, not with respect to the current optical thickness
            wfintervalAerTau_cld = wfintervalAerTau_cld &
                                 * optPropRTMGridS%kextKextAerdiv550(cloudAerosolRTMgridS%numIntervalFit)

            reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfintervalAerTau_cld
            reflDerivHRS%K_cld_vmr  (iwave,istate) = wfintervalAerTau_cld
            reflDerivHRS%K_cld_ndens(iwave,istate) = wfintervalAerTau_cld

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  wfintervalAerTau_cld

          case('aerosolSSA')

            call calculate_wfAerSSA_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int, &
                                        wfInterfkscaAerAbove_cld(0:optPropRTMGridS%RTMnlayer), &
                                        wfInterfkscaAerBelow_cld(0:optPropRTMGridS%RTMnlayer), &
                                        wfintervalAerSSA_cld )
            if (errorCheck(errS)) return

            reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfintervalAerSSA_cld
            reflDerivHRS%K_cld_vmr  (iwave,istate) = wfintervalAerSSA_cld
            reflDerivHRS%K_cld_ndens(iwave,istate) = wfintervalAerSSA_cld

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  wfintervalAerSSA_cld

          case('aerosolAC')

            ! use dR/dalpha = dR/dtau * dtau/dalpha
            ! tau(w) = tau(w0) * (w0/w)**alpha
            ! dtau/dalpha = tau(w0) * ln(w0/w) * (w0/w)**alpha

            if ( cloudAerosolRTMgridS%useHGScatAer ) then

              call calculate_wfAerTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int, &
                                          wfInterfkscaAerAbove_cld(0:optPropRTMGridS%RTMnlayer), &
                                          wfInterfkscaAerBelow_cld(0:optPropRTMGridS%RTMnlayer), &
                                          wfintervalAerTau_cld)
              if (errorCheck(errS)) return

              ! tau_aer_w0 is the optical thickness at the reference wavelength
              tau_aer_w0 = cloudAerosolRTMgridS%intervalAerTau(cloudAerosolRTMgridS%numIntervalFit)
              acAer = cloudAerosolRTMgridS%intervalAerAC(cloudAerosolRTMgridS%numIntervalFit)
              wavelRatio = cloudAerosolRTMgridS%w0 / wavelHRS%wavel(iwave)
              correctionRefWavel = tau_aer_w0 * log(wavelRatio) * wavelRatio**acAer
              wfacAer = wfintervalAerTau_cld * correctionRefWavel
              reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfacAer  ! cloudy part of the pixel
              reflDerivHRS%K_cld_vmr  (iwave,istate) = wfacAer
              reflDerivHRS%K_cld_ndens(iwave,istate) = wfacAer

            else

              ! for Mie scattering the Angstrom coefficient can not be fitted: set derivatives to zero
              reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_vmr  (iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0

            end if
           
            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)

          case('cloudTau')

            if ( cfMax < cldFractionBlockUpdateCldPram ) then
              ! cloud fraction too small to update the cloud optical thickness

              reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_vmr  (iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0

            else

              call calculate_wfCldTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int,  &
                                   wfInterfkscaCldAbove_cld(0:optPropRTMGridS%RTMnlayer),         &
                                   wfInterfkscaCldBelow_cld(0:optPropRTMGridS%RTMnlayer),         &
                                   wfintervalCldTau)
              if (errorCheck(errS)) return

              ! tau_cld_w0 is the cloud optical thickness at the reference wavelength w0 = 55- nm
              tau_cld_w0 = cloudAerosolRTMgridS%intervalCldTau(cloudAerosolRTMgridS%numIntervalFit)

              ! we need the derivative with respect to the cloud opical thickness
              ! at the reference wavelength, not with respect to the current optical thickness
              wfintervalCldTau = wfintervalCldTau &
                               * optPropRTMGridS%kextKextClddiv550(cloudAerosolRTMgridS%numIntervalFit)

              ! natural logarithm of the cloud optical thickness is fitted
              ! dR/dln(tau_cld_w0) = dR/dtau_cld_w0 * dtau_cld_w0/dln(tau_cld_w0) = tau_cld_w0 * dR/dtau_cld

              if ( cloudAerosolRTMgridS%fitLnCldTau ) then 
                reflDerivHRS%K_cld_lnvmr(iwave,istate) = tau_cld_w0 * wfintervalCldTau
              else
                reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfintervalCldTau
              end if
              reflDerivHRS%K_cld_vmr  (iwave,istate)   = wfintervalCldTau
              reflDerivHRS%K_cld_ndens(iwave,istate)   = wfintervalCldTau

            end if ! cf < cldFractionBlockUpdateCldPram

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) = reflDerivHRS%K_cld_ndens(iwave,istate)

          case('cloudAC')

            ! use dR/dalpha = dR/dtau * dtau/dalpha
            ! tau(w) = tau(w0) * (w0/w)**alpha
            ! dtau/dalpha = tau(w0) * ln(w0/w) * (w0/w)**alpha

            if ( cfMax < cldFractionBlockUpdateCldPram ) then
              ! cloud fraction too small to update optical thickness

              reflDerivHRS%K_clr_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_clr_vmr  (iwave,istate) = 0.0d0
              reflDerivHRS%K_clr_ndens(iwave,istate) = 0.0d0

            else
            
              call calculate_wfCldTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int,  &
                                   wfInterfkscaCldAbove_cld(0:optPropRTMGridS%RTMnlayer),         &
                                   wfInterfkscaCldBelow_cld(0:optPropRTMGridS%RTMnlayer),         &
                                   wfintervalCldTau)
              if (errorCheck(errS)) return

              ! tau_cld_w0 is the optical thickness at the reference wavelength
              tau_cld_w0 = cloudAerosolRTMgridS%intervalCldTau(cloudAerosolRTMgridS%numIntervalFit)
              acCld = cloudAerosolRTMgridS%intervalCldAC(cloudAerosolRTMgridS%numIntervalFit)
              wavelRatio = cloudAerosolRTMgridS%w0 / wavelHRS%wavel(iwave)
              correctionRefWavel = tau_cld_w0 * log(wavelRatio) * wavelRatio**acCld
              wfacCld = wfintervalCldTau * correctionRefWavel
              reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfacCld
              reflDerivHRS%K_cld_vmr  (iwave,istate) = wfacCld
              reflDerivHRS%K_cld_ndens(iwave,istate) = wfacCld

            end if ! cf < cldFractionBlockUpdateCldPram
           
            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_clr_ndens(iwave,istate)

          case('cloudFraction')

            ! Strictly: The derivative for the cloud fraction is not defined here
            ! as we considere only the cloud covered part.
            ! Hoever, when we combine
            !  cf * reflDerivHRS%K_cld_lnvmr and (1 - cf) *  reflDerivHRS%K_clr_lnvmr
            !  we get the proper wfCloudFraction

            if ( cloudAerosolRTMgridS%useCldAerFractionAllBands ) then

                reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfCloudFraction
                reflDerivHRS%K_cld_vmr(iwave,istate)   = wfCloudFraction
                reflDerivHRS%K_cld_ndens(iwave,istate) = wfCloudFraction

            else
            
              ! calculation of the derivative for polynomials are completed in subroutine setPolynomialDerivatives

              ! assign values only if the spectral band number 'iband'
              ! agrees with retrS%codeSpecBand(istate)
              if ( iband == retrS%codeSpecBand(istate) ) then
                  
                reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfCloudFraction
                reflDerivHRS%K_cld_vmr(iwave,istate)   = wfCloudFraction
                reflDerivHRS%K_cld_ndens(iwave,istate) = wfCloudFraction

              else

                reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
                reflDerivHRS%K_cld_vmr(iwave,istate)   = 0.0d0
                reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0

              end if ! iband == retrS%codeSpecBand(istate)

            end if ! cloudAerosolRTMgridS%useCldAerFractionAllBands

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_ndensCol(iwave, indexCol) =  reflDerivHRS%K_ndens(iwave,istate)          

          case('intervalDP')

            if ( cfMax < cldFractionBlockUpdateCldPram ) then
              ! cloud fraction too small to update cloud pressure

              reflDerivHRS%K_cld_lnvmr(iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_vmr  (iwave,istate) = 0.0d0
              reflDerivHRS%K_cld_ndens(iwave,istate) = 0.0d0

            else
            
              indexTop = optPropRTMGridS%indexRTMFitIntTop
              indexBot = optPropRTMGridS%indexRTMFitIntBot

              ksca = optPropRTMGridS%kscaIntAboveGas(cloudAerosolRTMgridS%numIntervalFit)
              kabs = optPropRTMGridS%kabsIntAboveGas(cloudAerosolRTMgridS%numIntervalFit)

              if ( cloudAerosolRTMgridS%useLambertianCloud ) then

                ! for a Lambertian cloud the derivative w.r.t. the altitude of the fit interval
                ! follows by removing air above the cloud. For the cloud free part the aerosol
                ! in the fit interval moves along. It is assumed that the air above the Lambertian
                ! cloud does not contain aerosol or cloud particles (for the retrieval).



                gas_sca_cld = - wfInterfkscaGas_cld(indexTop) * ksca
                gas_abs_cld = - wfInterfkabs_cld(indexTop)    * kabs

                wfintervalTop_cld = gas_sca_cld + gas_abs_cld

              else

                aer_sca_cld = ( wfInterfkscaAerBelow_cld(indexTop) - wfInterfkscaAerAbove_cld(indexBot) ) &
                            * optPropRTMGridS%kscaIntBelowAer(cloudAerosolRTMgridS%numIntervalFit)
                aer_abs_cld = ( wfInterfkabs_cld(indexTop) - wfInterfkabs_cld(indexBot) )                 &
                            * optPropRTMGridS%kabsIntBelowAer(cloudAerosolRTMgridS%numIntervalFit)
                cld_sca_cld = ( wfInterfkscaCldBelow_cld(indexTop) - wfInterfkscaCldAbove_cld(indexBot) ) &
                            * optPropRTMGridS%kscaIntBelowCld(cloudAerosolRTMgridS%numIntervalFit)
                cld_abs_cld = ( wfInterfkabs_cld(indexTop) - wfInterfkabs_cld(indexBot) )                 &
                            * optPropRTMGridS%kabsIntBelowCld(cloudAerosolRTMgridS%numIntervalFit)

                wfintervalTop_cld = aer_sca_cld + aer_abs_cld + cld_sca_cld + cld_abs_cld

              end if ! cloudAerosolRTMgridS%useLambertianCloud


              reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfintervalTop_cld
              reflDerivHRS%K_cld_vmr  (iwave,istate) = wfintervalTop_cld
              reflDerivHRS%K_cld_ndens(iwave,istate) = wfintervalTop_cld

            end if ! cf < cldFractionBlockUpdateCldPram

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)          

          case('intervalTop')

            indexTop = optPropRTMGridS%indexRTMFitIntTop

            if ( cloudAerosolRTMgridS%useLambertianCloud ) then

              ! for a Lambertian cloud the derivative w.r.t. the altitude of the fit interval
              ! follows by removing air above the cloud. For the cloud free part the aerosol
              ! in the fit interval moves along. It is assumed that the air above the Lambertian
              ! cloud does not contain aerosol of cloud particles (for the retrieval).

              gas_sca_cld = - wfInterfkscaGas_cld(indexTop)                        &
                          * optPropRTMGridS%kscaIntAboveGas(cloudAerosolRTMgridS%numIntervalFit) 
              gas_abs_cld = - wfInterfkabs_cld(indexTop)                           &
                          * optPropRTMGridS%kabsIntAboveGas(cloudAerosolRTMgridS%numIntervalFit)

              wfintervalTop_cld = gas_sca_cld + gas_abs_cld

            else

              ! we now deal with a scattering cloud possibly mixed with aerosol

              if ( cloudAerosolRTMgridS%aerosolPresent ) then

                call calculate_wfAerTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int, &
                                            wfInterfkscaAerAbove_cld(0:optPropRTMGridS%RTMnlayer), &
                                            wfInterfkscaAerBelow_cld(0:optPropRTMGridS%RTMnlayer), &
                                            wfintervalAerTau_cld)
                if (errorCheck(errS)) return

              else

                wfintervalAerTau_cld = 0.0d0

              end if

              call calculate_wfCldTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int,  &
                                          wfInterfkscaCldAbove_cld(0:optPropRTMGridS%RTMnlayer),  &
                                          wfInterfkscaCldBelow_cld(0:optPropRTMGridS%RTMnlayer),  &
                                          wfintervalCldTau)
              if (errorCheck(errS)) return

              ksca_aer = optPropRTMGridS%kscaIntBelowAer(cloudAerosolRTMgridS%numIntervalFit)
              kabs_aer = optPropRTMGridS%kabsIntBelowAer(cloudAerosolRTMgridS%numIntervalFit)
              ksca_cld = optPropRTMGridS%kscaIntBelowCld(cloudAerosolRTMgridS%numIntervalFit)
              kabs_cld = optPropRTMGridS%kabsIntBelowCld(cloudAerosolRTMgridS%numIntervalFit)


              aer_sca_cld = wfInterfkscaAerBelow_cld(indexTop) * ksca_aer
              aer_abs_cld = wfInterfkabs_cld(indexTop)         * kabs_aer
              cld_sca_cld = wfInterfkscaCldBelow_cld(indexTop) * ksca_cld
              cld_abs_cld = wfInterfkabs_cld(indexTop)         * kabs_cld
              wfintervalTop_cld = aer_sca_cld + aer_abs_cld + cld_sca_cld + cld_abs_cld  &
                                - (ksca_aer + kabs_aer) * wfintervalAerTau_cld           &
                                - (ksca_cld + kabs_cld) * wfintervalCldTau 

            end if ! cloudAerosolRTMgridS%useLambertianCloud

            reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfintervalTop_cld
            reflDerivHRS%K_cld_vmr  (iwave,istate) = wfintervalTop_cld
            reflDerivHRS%K_cld_ndens(iwave,istate) = wfintervalTop_cld

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)

          case('intervalBot')

            ! instead of changing the optical thickness of the layer by putting more particles inside the layer
            ! one can also reduce or increase the altitude of the lower boundary, while keeping the top
            ! altitude and the volume extinction coefficient constant.

            ! Note that moving the lower boundary of the interval upwards means a decrease of the cloud/aerosol
            ! particles. Therefore we need a minus sign

            indexBot = optPropRTMGridS%indexRTMFitIntBot

            if ( cloudAerosolRTMgridS%useLambertianCloud ) then

              wfintervalBot_cld = 0.0d0

            else

              ! we now deal with a scattering cloud possibly mixed with aerosol


              if ( cloudAerosolRTMgridS%aerosolPresent ) then
                call calculate_wfAerTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int, &
                                            wfInterfkscaAerAbove_cld(0:optPropRTMGridS%RTMnlayer), &
                                            wfInterfkscaAerBelow_cld(0:optPropRTMGridS%RTMnlayer), &
                                            wfintervalAerTau_cld)
                if (errorCheck(errS)) return
              else
                wfintervalAerTau_cld = 0.0d0
              end if

              call calculate_wfCldTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int,  &
                                   wfInterfkscaCldAbove_cld(0:optPropRTMGridS%RTMnlayer),         &
                                   wfInterfkscaCldBelow_cld(0:optPropRTMGridS%RTMnlayer),         &
                                   wfintervalCldTau)
              if (errorCheck(errS)) return

              ksca_aer = optPropRTMGridS%kscaIntAboveAer(cloudAerosolRTMgridS%numIntervalFit - 1)
              kabs_aer = optPropRTMGridS%kabsIntAboveAer(cloudAerosolRTMgridS%numIntervalFit - 1)
              ksca_cld = optPropRTMGridS%kscaIntAboveCld(cloudAerosolRTMgridS%numIntervalFit - 1)
              kabs_cld = optPropRTMGridS%kabsIntAboveCld(cloudAerosolRTMgridS%numIntervalFit - 1)


              aer_sca_cld = wfInterfkscaAerAbove_cld(indexBot) * ksca_aer
              aer_abs_cld = wfInterfkabs_cld        (indexBot) * kabs_aer
              cld_sca_cld = wfInterfkscaCldAbove_cld(indexBot) * ksca_cld
              cld_abs_cld = wfInterfkabs_cld        (indexBot) * kabs_cld
              wfintervalBot_cld = -aer_sca_cld - aer_abs_cld - cld_sca_cld - cld_abs_cld  &
                                + (ksca_aer + kabs_aer) * wfintervalAerTau_cld            &
                                + (ksca_cld + kabs_cld) * wfintervalCldTau 

            end if ! cloudAerosolRTMgridS%useLambertianCloud

            reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfintervalBot_cld
            reflDerivHRS%K_cld_vmr  (iwave,istate) = wfintervalBot_cld
            reflDerivHRS%K_cld_ndens(iwave,istate) = wfintervalBot_cld

            ! derivative for the (sub)columns
            indexCol = indexCol + 1
            reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) =  reflDerivHRS%K_cld_ndens(iwave,istate)          

          case('lnpolyCoef')
            ! do nothing here
            ! needed if DOAS is used in the retrieval

          case default

            call logDebug('in subroutine calculate_K_cld in radianceIrradianceModule')
            call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return

        end select

      end do ! istate loop

  end subroutine calculate_K_cld


  subroutine calculate_wfAerTau_clr(errS, cloudAerosolRTMgridS, optPropRTMGridS,  wfkabs_clr_int, &
                       wfInterfkscaAerAbove_clr, wfInterfkscaAerBelow_clr, wfintervalAerTau_clr )
                               
    ! This subroutine calculates the the derivative dR/dtau where R is the reflectance and 
    ! tau is the aerosol optical thickness in the fit interval. Only the cloud free part of the pixel is considered.
    ! Here it is assumed that the volume extinction coefficient of the aerosol and the single scattering albedo
    ! does not change with altitude within the fit interval. The analytical expression for the drivative
    ! is given by
    ! 
    ! dR/dtau = int [ a * d2R/dksca/dz(z) + (1-a) * d2R/dkabs/dz(z) ] / (z_top - z_bot)
    !
    ! where the integration over the altitude runs from the bottom to the top of the interval: (z_bot, z_top)
    ! a is the single scattering albedo of the aerosol (constant with altitude) and
    ! d2R/dksca/dz and d2R/dkabs/dz(z) are the basic derivatives calculated by the RTM
    ! as the derivatives differ for the clear and cloudy part, we also distinguish between these parts
    ! Note that aerosol particles can also occur in cloud covered part of the pixel, but that cloud
    ! particles can not occur in the clear part of the pixel. Therefore the calculation of the derivatives
    ! is slightly different for cloud and aerosol particles, and we have different subroutines for them.

    type(errorType),               intent(inout) :: errS
    type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS ! cloud-aerosol properties
    type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS      ! optical properties on RTM grid    
    real(8),                       intent(in)    :: wfkabs_clr_int(0:optPropRTMGridS%RTMnlayerSub)
    real(8),                       intent(in)    :: wfInterfkscaAerAbove_clr(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)    :: wfInterfkscaAerBelow_clr(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(out)   :: wfintervalAerTau_clr

    integer    :: ilevel, numRTMfitInt, numRTMsubFitInt
    integer    :: index
    integer    :: startInd, endInd
    integer    :: startSubInd, endSubInd
    integer    :: statusSplint
    real(8)    :: a_aer                      ! single scattering albedo for fit interval
    real(8)    :: sum_ksca_clr, sum_kabs_clr, sum_weight

    real(8), allocatable    :: RTMalt(:)         ! altitudes on RTM grid for the fit interval or above cloud
    real(8), allocatable    :: wfksca_clr(:)     ! weighting function on RTM layer grid for the fit interval
    real(8), allocatable    :: RTMsubweight(:)   ! weights on RTM sublayer grid for the fit interval
    real(8), allocatable    :: RTMsubalt(:)      ! altitudes on RTM sublayer grid for the fit interval
    real(8), allocatable    :: wfksca_clr_int(:) ! wfInterfsca interpolated to sublayer grid

    integer :: allocStatus, deallocStatus
    integer :: sumallocStatus, sumdeallocStatus

    ! allocate arrays
    numRTMfitInt    = optPropRTMGridS%indexRTMFitIntTop &
                    - optPropRTMGridS%indexRTMFitIntBot
    numRTMsubFitInt = optPropRTMGridS%indexRTMSubFitIntTop &
                    - optPropRTMGridS%indexRTMSubFitIntBot

    allocStatus    = 0
    sumallocStatus = 0

    allocate( RTMalt        (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca_clr    (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubweight  (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubalt     (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca_clr_int(0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    if ( sumallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: allocation failed')
      call logDebug('in subroutine calculate_wfAerTau')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if
    
    ! assign values
    ! Because of spline interpolation, we need the proper derivatives at the top
    ! and bottom of the fit interval. Below and above stand for the limit when
    ! approaching an interface from below and above, respectively. Because we do not fit
    ! the altitude here, it may be that there are aerosols below or above the fit interval.
    
    startInd    = optPropRTMGridS%indexRTMFitIntBot
    endInd      = optPropRTMGridS%indexRTMFitIntTop
    startSubInd = optPropRTMGridS%indexRTMSubFitIntBot
    endSubInd   = optPropRTMGridS%indexRTMSubFitIntTop

    RTMalt(0:numRTMfitInt)  = optPropRTMGridS%RTMaltitude(startInd:endInd)

    wfksca_clr(0:numRTMfitInt - 1)  = wfInterfkscaAerAbove_clr(startInd:endInd - 1)           
    wfksca_clr(numRTMfitInt)        = wfInterfkscaAerBelow_clr(endInd)

    ! fill weights
    RTMsubweight(0:numRTMsubFitInt) = optPropRTMGridS%RTMweightSub  (startSubInd:endSubInd)           
    RTMsubalt(0:numRTMsubFitInt)    = optPropRTMGridS%RTMaltitudeSub(startSubInd:endSubInd)           

    do ilevel = 0, numRTMsubFitInt 
      wfksca_clr_int(ilevel) = splintLin(errS, RTMalt, wfksca_clr, RTMsubalt(ilevel), statusSplint)
    end do

    ! integrate
    sum_ksca_clr = 0.0d0
    sum_kabs_clr = 0.0d0
    sum_weight   = 0.0d0
    do ilevel = 0, numRTMsubFitInt 
      sum_weight   = sum_weight + RTMsubweight(ilevel)
      sum_ksca_clr = sum_ksca_clr + RTMsubweight(ilevel) * wfksca_clr_int(ilevel)
      index        = ilevel + startSubInd
      sum_kabs_clr = sum_kabs_clr + RTMsubweight(ilevel) * wfkabs_clr_int(index)
    end do ! ilevel

    a_aer = cloudAerosolRTMgridS%intervalAerSSA (cloudAerosolRTMgridS%numIntervalFit)
    wfintervalAerTau_clr =  (a_aer * sum_ksca_clr + (1.0d0 - a_aer) * sum_kabs_clr ) / sum_weight

    ! clean up
    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( RTMalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca_clr, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubweight, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca_clr_int, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if ( sumdeallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: deallocation failed')
      call logDebug('in subroutine calculate_wfAerTau')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

  end subroutine calculate_wfAerTau_clr


  subroutine calculate_wfAerTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, &
                                    wfkabs_cld_int,                        &
                                    wfInterfkscaAerAbove_cld,              & 
                                    wfInterfkscaAerBelow_cld,              &
                                    wfintervalAerTau_cld )

    ! This subroutine calculates the the derivative dR/dtau where R is the reflectance and 
    ! tau is the aerosol optical thickness in the fit interval.
    ! Here it is assumed that the volume extinction coefficient of the aerosol and the single scattering albedo
    ! does not change with altitude within the fit interval. The analytical expression for the drivative
    ! is given by
    ! 
    ! dR/dtau = int [ a * d2R/dksca/dz(z) + (1-a) * d2R/dkabs/dz(z) ] / (z_top - z_bot)
    !
    ! where the integration over the altitude runs from the bottom to the top of the interval: (z_bot, z_top)
    ! a is the single scattering albedo of the aerosol (constant with altitude) and
    ! d2R/dksca/dz and d2R/dkabs/dz(z) are the basic derivatives calculated by the RTM
    ! as the derivatives differ for the clear and cloudy part, we also distinguish between these parts
    ! Note that aerosol particles can also occur in cloud covered part of the pixel, but that cloud
    ! particles can not occur in the clear part of the pixel. Therefore the calculation of the derivatives
    ! is slightly different for cloud and aerosol particles, and we have different subroutines for them.

    type(errorType),               intent(inout) :: errS
    type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS ! cloud-aerosol properties
    type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS      ! optical properties on RTM grid    
    real(8),                       intent(in)    :: wfkabs_cld_int(0:optPropRTMGridS%RTMnlayerSub)
    real(8),                       intent(in)    :: wfInterfkscaAerAbove_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)    :: wfInterfkscaAerBelow_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(out)   :: wfintervalAerTau_cld

    integer    :: ilevel, numRTMfitInt, numRTMsubFitInt
    integer    :: index
    integer    :: startInd, endInd
    integer    :: startSubInd, endSubInd
    integer    :: statusSplint
    real(8)    :: a_aer                      ! single scattering albedo for fit interval
    real(8)    :: sum_ksca_cld, sum_kabs_cld, sum_weight

    real(8), allocatable    :: RTMalt(:)         ! altitudes on RTM grid for the fit interval or above cloud
    real(8), allocatable    :: wfksca_cld(:)     ! weighting function on RTM layer grid for the fit interval
    real(8), allocatable    :: RTMsubweight(:)   ! weights on RTM sublayer grid for the fit interval
    real(8), allocatable    :: RTMsubalt(:)      ! altitudes on RTM sublayer grid for the fit interval
    real(8), allocatable    :: wfksca_cld_int(:) ! wfInterfksca interpolated to sublayer grid

    integer :: allocStatus, deallocStatus
    integer :: sumallocStatus, sumdeallocStatus

    ! allocate arrays
    numRTMfitInt    = optPropRTMGridS%indexRTMFitIntTop &
                    - optPropRTMGridS%indexRTMFitIntBot
    numRTMsubFitInt = optPropRTMGridS%indexRTMSubFitIntTop &
                    - optPropRTMGridS%indexRTMSubFitIntBot

    allocStatus    = 0
    sumallocStatus = 0

    allocate( RTMalt        (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca_cld    (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubweight  (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubalt     (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca_cld_int(0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    if ( sumallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: allocation failed')
      call logDebug('in subroutine calculate_wfAerTau')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if
    
    ! assign values
    ! Because of spline interpolation, we need the proper derivatives at the top
    ! and bottom of the fit interval. Below and above stand for the limit when
    ! approaching an interface from below and above, respectively. Because we do not fit
    ! the altitude here, it may be that there are aerosols below or above the fit interval.
    
    startInd    = optPropRTMGridS%indexRTMFitIntBot
    endInd      = optPropRTMGridS%indexRTMFitIntTop
    startSubInd = optPropRTMGridS%indexRTMSubFitIntBot
    endSubInd   = optPropRTMGridS%indexRTMSubFitIntTop

    RTMalt(0:numRTMfitInt)  = optPropRTMGridS%RTMaltitude(startInd:endInd)

    if ( cloudAerosolRTMgridS%useLambertianCloud ) then
      wfksca_cld(0:numRTMfitInt)      = 0.0d0           
    else
      wfksca_cld(0:numRTMfitInt - 1)  = wfInterfkscaAerAbove_cld(startInd:endInd - 1)        
      wfksca_cld(numRTMfitInt)        = wfInterfkscaAerBelow_cld(endInd)
    end if          

    ! fill weights
    RTMsubweight(0:numRTMsubFitInt) = optPropRTMGridS%RTMweightSub  (startSubInd:endSubInd)           
    RTMsubalt(0:numRTMsubFitInt)    = optPropRTMGridS%RTMaltitudeSub(startSubInd:endSubInd)           

    do ilevel = 0, numRTMsubFitInt 
      wfksca_cld_int(ilevel) = splintLin(errS, RTMalt, wfksca_cld, RTMsubalt(ilevel), statusSplint)
    end do

    ! integrate
    sum_ksca_cld = 0.0d0
    sum_kabs_cld = 0.0d0
    sum_weight = 0.0d0
    do ilevel = 0, numRTMsubFitInt 
      sum_weight = sum_weight + RTMsubweight(ilevel)
      sum_ksca_cld = sum_ksca_cld + RTMsubweight(ilevel) * wfksca_cld_int(ilevel)
      index      = ilevel + startSubInd
      sum_kabs_cld = sum_kabs_cld + RTMsubweight(ilevel) * wfkabs_cld_int(index)
    end do ! ilevel

    a_aer = cloudAerosolRTMgridS%intervalAerSSA (cloudAerosolRTMgridS%numIntervalFit)
    wfintervalAerTau_cld =  (a_aer * sum_ksca_cld + (1.0d0 - a_aer) * sum_kabs_cld ) / sum_weight

    ! clean up
    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( RTMalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca_cld, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubweight, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca_cld_int, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if ( sumdeallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: deallocation failed')
      call logDebug('in subroutine calculate_wfAerTau')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

  end subroutine calculate_wfAerTau_cld


  subroutine calculate_wfAerSSA_clr(errS, optPropRTMGridS, wfkabs_clr_int,  wfInterfkscaAerAbove_clr, &
                                    wfInterfkscaAerBelow_clr, wfintervalAerSSA_clr )

    ! This subroutine calculates the the derivative dR/da where R is the reflectance and 
    ! a is the aerosol single scattering albedo in the fit interval.
    ! Here it is assumed that the single scattering albedo of the aerosol does not
    ! change with altitude within the fit interval. The analytical expression for the drivative
    ! is given by (use as side constraint that kext does not change when a changes)
    !
    ! dR/da = kext * int [ d2R/dksca/dz  - d2R/dkabs/dz ]
    !
    ! where the integration over the altitude runs from the bottom to the top of the interval: (z_bot, z_top)
    ! kext is the volume extinction coefficient of the aerosol (constant with altitude) and
    ! d2R/dksca/dz and d2R/dkabs/dz(z) are the basic derivatives calculated by the RTM.
    ! As the derivatives differ for the clear and cloudy part, we also distinguish between these parts.

    type(errorType),               intent(inout) :: errS
    type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS      ! optical properties on RTM grid    
    real(8),                       intent(in)    :: wfkabs_clr_int(0:optPropRTMGridS%RTMnlayerSub)
    real(8),                       intent(in)    :: wfInterfkscaAerAbove_clr(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)    :: wfInterfkscaAerBelow_clr(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(out)   :: wfintervalAerSSA_clr

    integer    :: ilevel, numRTMfitInt, numRTMsubFitInt
    integer    :: index
    integer    :: startInd, endInd
    integer    :: startSubInd, endSubInd
    integer    :: statusSplint
    real(8)    :: kextAer                   ! aerosol volume extinction coefficient for fit interval
    real(8)    :: sum_ksca_clr, sum_kabs_clr, sum_weight

    real(8), allocatable    :: RTMalt(:)         ! altitudes on RTM grid for the fit interval or above cloud
    real(8), allocatable    :: wfksca_clr(:)     ! weighting function on RTM layer grid for the fit interval
    real(8), allocatable    :: RTMsubweight(:)   ! weights on RTM sublayer grid for the fit interval
    real(8), allocatable    :: RTMsubalt(:)      ! altitudes on RTM sublayer grid for the fit interval
    real(8), allocatable    :: wfksca_clr_int(:) ! wfInterfsca interpolated to sublayer grid - clear part

    integer :: allocStatus, deallocStatus
    integer :: sumallocStatus, sumdeallocStatus

    ! allocate arrays
    numRTMfitInt    = optPropRTMGridS%indexRTMFitIntTop &
                    - optPropRTMGridS%indexRTMFitIntBot
    numRTMsubFitInt = optPropRTMGridS%indexRTMSubFitIntTop &
                    - optPropRTMGridS%indexRTMSubFitIntBot

    allocStatus    = 0
    sumallocStatus = 0

    allocate( RTMalt         (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca_clr     (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubweight   (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubalt      (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca_clr_int (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    if ( sumallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: allocation failed')
      call logDebug('in subroutine calculate_wfAerSSA_clr')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if
    
    ! assign values
    ! We need the proper derivatives at the top and bottom of the fit interval.
    ! Below and above stand for the limit when approaching an interface from below and above, respectively.
    
    startInd    = optPropRTMGridS%indexRTMFitIntBot
    endInd      = optPropRTMGridS%indexRTMFitIntTop
    startSubInd = optPropRTMGridS%indexRTMSubFitIntBot
    endSubInd   = optPropRTMGridS%indexRTMSubFitIntTop

    kextAer     = optPropRTMGridS%kextSubAer(startSubInd+1)
    RTMalt(0:numRTMfitInt)  = optPropRTMGridS%RTMaltitude(startInd:endInd)

    wfksca_clr(0:numRTMfitInt - 1)  = wfInterfkscaAerAbove_clr(startInd:endInd - 1)           
    wfksca_clr(numRTMfitInt)        = wfInterfkscaAerBelow_clr(endInd)

    ! fill weights
    RTMsubweight(0:numRTMsubFitInt) = optPropRTMGridS%RTMweightSub  (startSubInd:endSubInd)           
    RTMsubalt(0:numRTMsubFitInt)    = optPropRTMGridS%RTMaltitudeSub(startSubInd:endSubInd)           

    do ilevel = 0, numRTMsubFitInt 
      wfksca_clr_int(ilevel) = splintLin(errS, RTMalt, wfksca_clr, RTMsubalt(ilevel), statusSplint)
    end do

    ! integrate
    sum_ksca_clr = 0.0d0
    sum_kabs_clr = 0.0d0
    sum_weight = 0.0d0
    do ilevel = 0, numRTMsubFitInt 
      sum_weight = sum_weight + RTMsubweight(ilevel)
      sum_ksca_clr = sum_ksca_clr + RTMsubweight(ilevel) * wfksca_clr_int(ilevel)
      index      = ilevel + startSubInd
      sum_kabs_clr = sum_kabs_clr + RTMsubweight(ilevel) * wfkabs_clr_int(index)
    end do ! ilevel

    wfintervalAerSSA_clr =  kextAer * (sum_ksca_clr - sum_kabs_clr)

    ! clean up
    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( RTMalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca_clr, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubweight, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca_clr_int, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if ( sumdeallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: deallocation failed')
      call logDebug('in subroutine calculate_wfAerSSA_clr')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

  end subroutine calculate_wfAerSSA_clr


  subroutine calculate_wfAerSSA_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, wfkabs_cld_int, &
                                    wfInterfkscaAerAbove_cld, wfInterfkscaAerBelow_cld,    &
                                    wfintervalAerSSA_cld )

    ! This subroutine calculates the the derivative dR/da where R is the reflectance and 
    ! a is the aerosol single scattering albedo in the fit interval.
    ! Here it is assumed that the single scattering albedo and extinction of the aerosol does not
    ! change with altitude within the fit interval. The analytical expression for the drivative
    ! is given by
    ! 
    ! dR/da = kext * int [ d2R/dksca/dz(z) - d2R/dkabs/dz(z) ]
    !
    ! where the integration over the altitude runs from the bottom to the top of the interval: (z_bot, z_top)
    ! kext is the volume extinction coefficient of the aerosol (constant with altitude) and
    ! d2R/dksca/dz and d2R/dkabs/dz(z) are the basic derivatives calculated by the RTM.
    ! As the derivatives differ for the clear and cloudy part, we also distinguish between these parts.

    type(errorType),               intent(inout) :: errS
    type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS ! cloud-aerosol properties
    type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS      ! optical properties on RTM grid    
    real(8),                       intent(in)    :: wfkabs_cld_int(0:optPropRTMGridS%RTMnlayerSub)
    real(8),                       intent(in)    :: wfInterfkscaAerAbove_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)    :: wfInterfkscaAerBelow_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(out)   :: wfintervalAerSSA_cld

    integer    :: ilevel, numRTMfitInt, numRTMsubFitInt
    integer    :: index
    integer    :: startInd, endInd
    integer    :: startSubInd, endSubInd
    integer    :: statusSplint
    real(8)    :: kextAer                   ! aerosol volume extinction coefficient for fit interval
    real(8)    :: sum_ksca_cld, sum_kabs_cld, sum_weight

    real(8), allocatable    :: RTMalt(:)         ! altitudes on RTM grid for the fit interval or above cloud
    real(8), allocatable    :: wfksca_cld(:)     ! weighting function on RTM layer grid for the fit interval
    real(8), allocatable    :: RTMsubweight(:)   ! weights on RTM sublayer grid for the fit interval
    real(8), allocatable    :: RTMsubalt(:)      ! altitudes on RTM sublayer grid for the fit interval
    real(8), allocatable    :: wfksca_cld_int(:) ! wfInterfsca interpolated to sublayer grid - cloudy part

    integer :: allocStatus, deallocStatus
    integer :: sumallocStatus, sumdeallocStatus

    numRTMfitInt    = optPropRTMGridS%indexRTMFitIntTop &
                    - optPropRTMGridS%indexRTMFitIntBot
    numRTMsubFitInt = optPropRTMGridS%indexRTMSubFitIntTop &
                    - optPropRTMGridS%indexRTMSubFitIntBot

    allocStatus    = 0
    sumallocStatus = 0

    ! allocate arrays
    allocate( RTMalt         (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca_cld     (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubweight   (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubalt      (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca_cld_int (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    if ( sumallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: allocation failed')
      call logDebug('in subroutine calculate_wfAerSSA_cld')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if
    
    ! assign values
    ! We need the proper derivatives at the top and bottom of the fit interval.
    ! Below and above stand for the limit when approaching an interface from below and above, respectively.
    
    startInd    = optPropRTMGridS%indexRTMFitIntBot
    endInd      = optPropRTMGridS%indexRTMFitIntTop
    startSubInd = optPropRTMGridS%indexRTMSubFitIntBot
    endSubInd   = optPropRTMGridS%indexRTMSubFitIntTop

    kextAer     = optPropRTMGridS%kextSubAer(startSubInd+1)
    RTMalt(0:numRTMfitInt)  = optPropRTMGridS%RTMaltitude(startInd:endInd)

    if ( cloudAerosolRTMgridS%useLambertianCloud ) then
      wfksca_cld(0:numRTMfitInt)      = 0.0d0   ! Lambertian cloud is above the aerosol layer
    else
      wfksca_cld(0:numRTMfitInt - 1)  = wfInterfkscaAerAbove_cld(startInd:endInd - 1)        
      wfksca_cld(numRTMfitInt)        = wfInterfkscaAerBelow_cld(endInd)
    end if          

    ! fill weights
    RTMsubweight(0:numRTMsubFitInt) = optPropRTMGridS%RTMweightSub  (startSubInd:endSubInd)           
    RTMsubalt(0:numRTMsubFitInt)    = optPropRTMGridS%RTMaltitudeSub(startSubInd:endSubInd)           

    do ilevel = 0, numRTMsubFitInt 
      wfksca_cld_int(ilevel) = splintLin(errS, RTMalt, wfksca_cld, RTMsubalt(ilevel), statusSplint)
    end do

    ! integrate
    sum_ksca_cld = 0.0d0
    sum_kabs_cld = 0.0d0
    sum_weight = 0.0d0
    do ilevel = 0, numRTMsubFitInt 
      sum_weight = sum_weight + RTMsubweight(ilevel)
      sum_ksca_cld = sum_ksca_cld + RTMsubweight(ilevel) * wfksca_cld_int(ilevel)
      index      = ilevel + startSubInd
      sum_kabs_cld = sum_kabs_cld + RTMsubweight(ilevel) * wfkabs_cld_int(index)
    end do ! ilevel

    wfintervalAerSSA_cld =  kextAer * (sum_ksca_cld - sum_kabs_cld)

    ! clean up
    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( RTMalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca_cld, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubweight, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca_cld_int, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if ( sumdeallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: deallocation failed')
      call logDebug('in subroutine calculate_wfAerSSA_cld')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

  end subroutine calculate_wfAerSSA_cld


  subroutine calculate_wfCldTau_cld(errS, cloudAerosolRTMgridS, optPropRTMGridS, &
                                    wfkabs_cld_int,                        &
                                    wfInterfkscaCldAbove_cld,              &
                                    wfInterfkscaCldBelow_cld,              &
                                    wfintervalCldTau)

    ! This subroutine calculates the the derivative dR/dkext where R is the reflectance and 
    ! kext is the cloud volume extinction coefficient in the fit interval.
    ! Here it is assumed that the volume extinction coefficient of the cloud particles does not
    ! change with altitude within the fit interval. The analytical expression for the drivative
    ! is given by
    ! 
    ! dR/dkext = int [ a * d2R/dksca/dz(z) + (1-a) * d2R/dkabs/dz(z) ]  / (z_top - z_bot)
    !
    ! where the integration over the altitude runs from the bottom to the top of the interval: (z_bot, z_top)
    ! a is the single scattering albedo of the aerosol (constant with altitude) and
    ! d2R/dksca/dz and d2R/dkabs/dz(z) are the basic derivatives calculated by the RTM
    ! as the derivatives differ for the clear and cloudy part, we also distiguish between these parts
    ! Note that aerosol particles can also occur in cloud covered part of the pixel, but that cloud
    ! partcles can not occur in the clear part of the pixel. Therefore the calculation of the derivatives
    ! is slightly different for cloud and aerosol particles, and we have different subroutines for them.

    type(errorType),               intent(inout) :: errS
    type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS ! cloud-aerosol properties
    type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS      ! optical properties on RTM grid    
    real(8),                       intent(in)    :: wfkabs_cld_int(0:optPropRTMGridS%RTMnlayerSub)
    real(8),                       intent(in)    :: wfInterfkscaCldAbove_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(in)    :: wfInterfkscaCldBelow_cld(0:optPropRTMGridS%RTMnlayer)
    real(8),                       intent(out)   :: wfintervalCldTau

    integer    :: ilevel, numRTMfitInt, numRTMsubFitInt
    integer    :: index
    integer    :: startInd, endInd
    integer    :: startSubInd, endSubInd
    integer    :: statusSplint
    real(8)    :: a_cld                      ! cloud fraction and single scattering albedo for fit interval
    real(8)    :: sum_ksca, sum_kabs, sum_weight

    real(8), allocatable    :: RTMalt(:)         ! altitudes on RTM grid for the fit interval or above cloud
    real(8), allocatable    :: wfksca(:)         ! weighting function on RTM layer grid for the fit interval
    real(8), allocatable    :: RTMsubweight(:)   ! weights on RTM sublayer grid for the fit interval
    real(8), allocatable    :: RTMsubalt(:)      ! altitudes on RTM sublayer grid for the fit interval
    real(8), allocatable    :: wfksca_int(:)     ! wfInterfksca interpolated to sublayer grid

    integer :: allocStatus, deallocStatus
    integer :: sumallocStatus, sumdeallocStatus

    numRTMfitInt    = optPropRTMGridS%indexRTMFitIntTop &
                    - optPropRTMGridS%indexRTMFitIntBot
    numRTMsubFitInt = optPropRTMGridS%indexRTMSubFitIntTop &
                    - optPropRTMGridS%indexRTMSubFitIntBot

    ! allocate arrays
    allocStatus    = 0
    sumallocStatus = 0

    allocate( RTMalt      (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca      (0:numRTMfitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubweight(0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( RTMsubalt   (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfksca_int  (0:numRTMsubFitInt), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    if ( sumallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: allocation failed')
      call logDebug('in subroutine calculate_wfCldTau')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if
    
    ! assign values
    ! Because of spline interpolation, we need the proper derivatives at the top
    ! and bottom of the fit interval. Below and above stand for the limit when
    ! approaching an interface from below and above, respectively. Because we do not fit
    ! the altitude here, it may be that there are aerosols below or above the fit interval.
    
    startInd    = optPropRTMGridS%indexRTMFitIntBot
    endInd      = optPropRTMGridS%indexRTMFitIntTop
    startSubInd = optPropRTMGridS%indexRTMSubFitIntBot
    endSubInd   = optPropRTMGridS%indexRTMSubFitIntTop

    RTMalt(0:numRTMfitInt)  = optPropRTMGridS%RTMaltitude(startInd:endInd)

    wfksca(0:numRTMfitInt - 1)  = wfInterfkscaCldAbove_cld(startInd:endInd - 1)        
    wfksca(numRTMfitInt)        = wfInterfkscaCldBelow_cld(endInd)

    ! fill weights
    RTMsubweight(0:numRTMsubFitInt) = optPropRTMGridS%RTMweightSub  (startSubInd:endSubInd)           
    RTMsubalt(0:numRTMsubFitInt)    = optPropRTMGridS%RTMaltitudeSub(startSubInd:endSubInd)           

    ! use linear interpolation because spline can be inaccurate for clouds in the O2 A band
    do ilevel = 0, numRTMsubFitInt 
      wfksca_int(ilevel) = splintLin(errS, RTMalt, wfksca, RTMsubalt(ilevel), statusSplint)
    end do

    ! integrate
    sum_ksca   = 0.0d0 
    sum_kabs   = 0.0d0 
    sum_weight = 0.0d0
    do ilevel = 0, numRTMsubFitInt 
      ! after completion loop sum_weight = z_top - z_bot
      sum_weight = sum_weight + RTMsubweight(ilevel)
      sum_ksca = sum_ksca + RTMsubweight(ilevel) * wfksca_int(ilevel)
      index    = ilevel + startInd
      sum_kabs = sum_kabs + RTMsubweight(ilevel) * wfkabs_cld_int(index)
    end do ! ilevel

    a_cld = cloudAerosolRTMgridS%intervalCldSSA (cloudAerosolRTMgridS%numIntervalFit)
    wfintervalCldTau =  (a_cld * sum_ksca + (1.0d0 - a_cld) * sum_kabs) / sum_weight

    ! clean up
    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( RTMalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubweight, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( RTMsubalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfksca_int, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if ( sumdeallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: deallocation failed')
      call logDebug('in subroutine calculate_wfCldTau')
      call logDebug('in module radianceIrradianceModule')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

  end subroutine calculate_wfCldTau_cld


  subroutine calculate_dn_dnode(errS, nAltNodes, altNodes, RTMnlayerSub, RTMaltitudeSub, dn_dnode, linInterpolation)


    ! Calculate the change in the natural logarithm of the volume mixing ratio 
    ! if it changes at one node This is the same as using spline interpolation on an array of values
    ! that al zero except for one value that is equal to one. The result, dn_dnode, is calculated
    ! for all nodes (the altitudes of the retrieval grid) and for the Gaussian division points of the
    ! layers of the retrieval grid. Here one is replaced by delta.

    implicit none

      type(errorType), intent(inout) :: errS
    integer, intent(in) :: nAltNodes                        ! number of nodes -1 
    real(8), intent(in) :: altNodes(0:nAltNodes)            ! altitudes for nodes
    integer, intent(in) :: RTMnlayerSub                     ! number of sublayers -1 
    real(8), intent(in) :: RTMaltitudeSub(0:RTMnlayerSub)   ! altitudes for sublayers

    real(8), intent(out) :: dn_dnode(0:RTMnlayerSub, 0:nAltNodes)
    logical, optional, intent(in) :: linInterpolation

    ! local
    real(8) :: dn(0:nAltNodes), SDdn(0:nAltNodes)
    integer :: ilevel, jlevel, statusSpline, statusSplint

    ! control output
    logical, parameter :: verbose = .false.
    real(8), parameter :: delta   = 0.01d0

    do jlevel = 0, nAltNodes

      ! interpolate on the perturbed values which is normalized to delta
      dn = 0.0d0
      dn(jlevel) = delta
      
      if ( present( linInterpolation ) ) then
        if ( linInterpolation ) then
          do ilevel = 0, RTMnlayerSub
            dn_dnode(ilevel,jlevel) = splintLin(errS, altNodes, dn, RTMaltitudeSub(ilevel), statusSplint) / delta
          end do  ! ilevel
        else
          ! calculate values at division points
          call spline(errS, altNodes, dn, SDdn,  statusSpline)
          if (errorCheck(errS)) return
          do ilevel = 0, RTMnlayerSub
            dn_dnode(ilevel,jlevel) = splint(errS, altNodes, dn, SDdn, RTMaltitudeSub(ilevel), statusSplint) / delta
          end do  ! ilevel
        end if
      else
        ! calculate values at division points
        call spline(errS, altNodes, dn, SDdn,  statusSpline)
        if (errorCheck(errS)) return
        do ilevel = 0, RTMnlayerSub
          dn_dnode(ilevel,jlevel) = splint(errS, altNodes, dn, SDdn, RTMaltitudeSub(ilevel), statusSplint) / delta
        end do  ! ilevel
      end if
    end do ! jlevel

    if ( verbose ) then
      write(intermediateFileUnit,*) 'output from radianceIrradianceModule: calculate_dn_dnode '
      write(intermediateFileUnit,'(8X,100F12.3)') (altNodes(jlevel), jlevel = 0, nAltNodes)
      do ilevel = 0, RTMnlayerSub
        write(intermediateFileUnit,'(F8.3,100E12.3)') RTMaltitudeSub(ilevel), &
                                                      (dn_dnode(ilevel, jlevel), jlevel = 0, nAltNodes)
      end do 
    end if

  end subroutine calculate_dn_dnode


  subroutine calculate_wfnode(errS, iTrace, ialt, nAlt, optPropRTMGridS, dn_dnode, wfkabs_int, &
                              wfnode_lnvmr, wfnode_vmr, wfnode_ndens)

    ! Calculate the the derivative for a node of the trace gas profile
    ! using the derivatives interpolated to the sublayer grid (wfkabs_int) and the 
    ! profile change due to the change in a node (dn_dnode)

    ! locally we have for absorbing molecules dR/dln(errS, vmr) = kabs * dR/dkabs = (n*s) dR/d(n*s)
    ! with n the number density, s the absorption cross section and kabs = n * s
    !
    ! if the absorbing species is a collision complex we have
    ! dR/dln(vmr) = vmr * dR/dvmr = vmr * dR/dkabs * dkabs/dvmr
    ! as kabs = sig * [ 1.0d-6 * vmr * ndens_air ]**2 * 1.0d5 [km-1] for a collisin complex
    ! where  sig = absorption cross section
    !        vmr = volume mixing ratio of the molecules that collide (oxygen) in ppmv
    !        ndens_air is the number density of air molecules in molecues cm-3
    !        1.0d5 occurs because the absorption coefficient is per km
    ! dR/dln(vmr)    = dR/dkabs * vmr * sig * 1.0d5 * 2 * [ 1.0d-6 * vmr * ndens_air ] * [ 1.0d-6 * ndens_air ]
    !                = dR/dkabs * sig * 2 * ndensTrace **2 * 1.0d5
    !                = 2 * kabs * dR/dkabs 
    !                which differs a factor of two compared to the 'normal' molecles
    ! dR/dvmr        = dR/dln(vmr) / vmr                     the same for collision complex and normal molecules
    ! dR/dndensTrace =  dR/dvmr * dvmr/dndensTrace
    !                =  dR/dvmr / [ 1.0d-6 * ndens_air ]  
    !                =  dR/dvmr * vmr / ndensTrace
    !                =  dR/dln(vmr) / ndensTrace            the same for collision complex and normal molecules
    !
    ! The derivative at the node is then given by the integral over the altitude
    ! wfnode = int [ d2R/dkabs/dz * dkabs/dln(vmr) * dln(vmr)/dln(vmr_i) ]
    !        = int [ d2R/dkabs/dz * kabs(z) * dln(vmr)/dln(vmr_i) ]
    ! because we interpolate on ln(vmr) to get the atmospheric optical properties

    ! Here dln(vmr)/dln(vmr_i) is given by dn_dnode

    ! if there is more than one trace gas wfnode_lnvmr scales with kabs_i(z) / kabs(z)
    ! where kabs_i(z) is the volume absorption coefficient of trace gas i and kabs(z)
    ! is the volume absorption coefficient of all the trace gases combined.

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                  intent(in)  :: iTrace, ialt, nAlt
    type(optPropRTMGridType), intent(in)  :: optPropRTMGridS
    real(8),                  intent(in)  :: dn_dnode(0:optPropRTMGridS%RTMnlayerSub, 0:nAlt)
    real(8),                  intent(in)  :: wfkabs_int(0:optPropRTMGridS%RTMnlayerSub)

    real(8),                  intent(out) :: wfnode_lnvmr, wfnode_vmr, wfnode_ndens

    ! local
    real(8)    :: integrand_lnvmr(0:optPropRTMGridS%RTMnlayerSub)  ! function values to be integrated
    real(8)    :: integrand_vmr  (0:optPropRTMGridS%RTMnlayerSub)  ! function values to be integrated
    real(8)    :: integrand_ndens(0:optPropRTMGridS%RTMnlayerSub)  ! function values to be integrated
    integer    :: index
    real(8)    :: fraction, kabs_tot(0:optPropRTMGridS%RTMnlayerSub)

    logical, parameter :: verbose = .false.

    kabs_tot(:) = 0.0d0
    do index = 1, optPropRTMGridS%nTrace
        kabs_tot(:) = kabs_tot(:) + optPropRTMGridS%XsecSubGas (:, index)             &
                                  * optPropRTMGridS%ndensSubGas(:, index) * 1.0d-5
    end do
    do index = 0, optPropRTMGridS%RTMnlayerSub
      if ( kabs_tot(index) < 1.0d-50 ) then
        fraction = 0.0d0
      else
          fraction = optPropRTMGridS%XsecSubGas (index, iTrace) * optPropRTMGridS%ndensSubGas(index, iTrace) &
                   * 1.0d-5 / kabs_tot(index)
      end if
      ! fraction contains the fraction absorption by trace gas iTrace
      integrand_lnvmr(index) = wfkabs_int(index) * dn_dnode(index,ialt) &
                             * optPropRTMGridS%kabsSubGas(index) * fraction
      integrand_vmr  (index) = integrand_lnvmr(index) / optPropRTMGridS%vmrSubGas(index, iTrace)
      integrand_ndens(index) = integrand_lnvmr(index) / optPropRTMGridS%ndensSubGas(index, iTrace)

    end do ! index

    wfnode_lnvmr = dot_product( optPropRTMGridS%RTMweightSub, integrand_lnvmr )
    wfnode_vmr   = dot_product( optPropRTMGridS%RTMweightSub, integrand_vmr )
    wfnode_ndens = dot_product( optPropRTMGridS%RTMweightSub, integrand_ndens )

    if ( verbose ) then
      write(intermediateFileUnit,*) ' output from calculate_wfnode in radianceIrradianceModule'
      write(intermediateFileUnit,'(A,I3,ES12.3)') ' ialt and wfnode_lnvmr = ', ialt, wfnode_lnvmr
      write(intermediateFileUnit,*) ' wfkabs_int  dn_dnode   optPropRTMGridS%kabsSubGas   integrand_lnvmr'
      
      do index = 0, optPropRTMGridS%RTMnlayerSub
        write(intermediateFileUnit,'(4ES12.3)') wfkabs_int(index), dn_dnode(index,ialt), &
                                      optPropRTMGridS%kabsSubGas(index), integrand_lnvmr(index)

      end do ! index

      if ( ialt > 6) call mystop(errS, 'stopped in radianceIrradianceModule: calculate_wfnode: limit verbose output') 
                     if (errorCheck(errS)) return
    end if ! verbose

  end subroutine calculate_wfnode


  subroutine calculate_wfnodeTemperature(errS, ialt, nAlt, optPropRTMGridS, dn_dnode, wfTemp_int, &
                                         wfnodeTemp)

    ! Calculate the the derivative for a node of the temperature profile
    ! using the derivatives interpolated to the sublayer grid (wfTemp_int) and the 
    ! profile change due to the change in a node (dn_dnode)

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                  intent(in)  :: ialt, nAlt
    type(optPropRTMGridType), intent(in)  :: optPropRTMGridS
    real(8),                  intent(in)  :: dn_dnode(0:optPropRTMGridS%RTMnlayerSub, 0:nAlt)
    real(8),                  intent(in)  :: wfTemp_int(0:optPropRTMGridS%RTMnlayerSub)

    real(8),                  intent(out) :: wfnodeTemp

    ! local
    real(8)    :: integrand(0:optPropRTMGridS%RTMnlayerSub)  ! function values to be integrated
    integer    :: index

    do index = 0, optPropRTMGridS%RTMnlayerSub
      integrand(index) = wfTemp_int(index) * dn_dnode(index,ialt)
    end do ! index

    ! weigths are for km not for cm
    wfnodeTemp = dot_product( optPropRTMGridS%RTMweightSub, integrand )

  end subroutine calculate_wfnodeTemperature


  subroutine setPolynomialDerivatives_clr(errS, iband, nTrace, wavelHRS, retrS, cloudAerosolRTMgridS, &
                                          surfaceS, LambCloudS, cldAerFractionS, mulOffsetS,    &
                                          strayLightS, reflDerivHRS)

    ! The surface albedo, Lambertian cloud albedo, and straylight can be a polynomial in the wavelength,
    ! specified by the value at one (constant value) or more wavelengths, called nodes. In calculate_K
    ! the derivatives at the nodes were calculated and stored in  reflDerivHRS%K, but this gives not
    ! the proper wavelength dependence of the derivative. The calculation of the derivatives with
    ! proper wavelength dependence in completed here. Originally, this was part of calculate_K, but
    ! it is moved here because of DISMAS. In DISMAS this subroutine should be called after the values
    ! on the high-resolution grid have been calculated, simply because the derivatives are not
    ! 'smooth' enough for DISMAS, introducing substantial errors if it is kept in calculate_K.

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                       intent(in)  :: iband                ! number spectral band
    integer,                       intent(in)  :: nTrace               ! number of trace gases
    type(wavelHRType),             intent(in)  :: wavelHRS             ! high resolution wavelength grid
    type(retrType),                intent(in)  :: retrS                ! contains info on the fit parameters
    type(cloudAerosolRTMgridType), intent(in)  :: cloudAerosolRTMgridS ! cloud-aerosol properties
    type(LambertianType),          intent(in)  :: surfaceS             ! surface properties
    type(LambertianType),          intent(in)  :: LambCloudS           ! Lambertian cloud properties
    type(cldAerFractionType),      intent(in)  :: cldAerFractionS      ! cloud / aerosol fraction
    type(mulOffsetType),           intent(in)  :: mulOffsetS           ! multiplicative offset
    type(straylightType),          intent(in)  :: strayLightS          ! stray light

    type(reflDerivType),        intent(inout)  :: reflDerivHRS         ! reflectance(R) and derivatives(K) on HR grid

    ! local
    integer :: iwave, istate, index, indexCol
    real(8) :: polyderivValue

    ! initialize counter for derivatives for the (sub)columns
    indexCol = nTrace * retrS%ngaussCol

    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case( 'nodeTemp', 'offsetTemp', 'surfPressure', 'aerosolTau', 'aerosolSSA' , 'aerosolAC'  ,  &
              'cloudTau', 'cloudAC'   , 'intervalDP', 'intervalTop', 'intervalBot', 'diffRingCoef', 'RingCoef' )

          indexCol = indexCol + 1

        case('surfAlbedo')

          index    = retrS%codeIndexSurfAlb(istate)
          indexCol = indexCol + 1

          if ( .not. cloudAerosolRTMgridS%useAlbedoLambSurfAllBands ) then
            if ( iband == retrS%codeSpecBand(istate) ) then
              if ( surfaceS%nwavelAlbedo > 1 ) then
                do iwave = 1, wavelHRS%nwavel
                  polyderivValue = polyDeriv(errS, index, surfaceS%wavelAlbedo, surfaceS%albedo, wavelHRS%wavel(iwave) )
                  reflDerivHRS%K_clr_lnvmr(iwave,istate)         = reflDerivHRS%K_clr_lnvmr(iwave,istate)  &
                                                                 * polyderivValue
                  reflDerivHRS%K_clr_vmr(iwave,istate)           = reflDerivHRS%K_clr_lnvmr(iwave,istate)  
                  reflDerivHRS%K_clr_ndens(iwave,istate)         = reflDerivHRS%K_clr_vmr(iwave,istate)
                  reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol) = reflDerivHRS%K_clr_vmr(iwave,istate)
                end do ! iwave
              end if ! surfaceS%nwavelAlbedo > 1
            end if ! iband == retrS%codeSpecBand(istate)
          end if ! .not. cloudAerosolRTMgridS%useAlbedoLambCldAllBands

        case('LambCldAlbedo')

          index    = retrS%codeIndexLambCldAlb(istate)
          indexCol = indexCol + 1

          if ( .not. cloudAerosolRTMgridS%useAlbedoLambCldAllBands ) then
            if ( iband == retrS%codeSpecBand(istate) ) then
              if ( LambCloudS%nwavelAlbedo > 1 ) then
                do iwave = 1, wavelHRS%nwavel
                  polyderivValue = polyDeriv(errS, index, LambCloudS%wavelAlbedo, LambCloudS%albedo, wavelHRS%wavel(iwave) )
                  reflDerivHRS%K_clr_lnvmr(iwave,istate)          = reflDerivHRS%K_clr_lnvmr(iwave,istate)  &
                                                                  * polyderivValue
                  reflDerivHRS%K_clr_vmr(iwave,istate)            = reflDerivHRS%K_clr_lnvmr(iwave,istate)             
                  reflDerivHRS%K_clr_ndens(iwave,istate)          = reflDerivHRS%K_clr_lnvmr(iwave,istate)
                  reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol)  = reflDerivHRS%K_clr_lnvmr(iwave,istate)
                end do ! iwave
              end if ! LambCloudS%nwavelAlbedo > 1
            end if  ! iband == retrS%codeSpecBand(istate)
          end if ! .not. cloudAerosolRTMgridS%useAlbedoLambCldAllBands

        case('cloudFraction')

          index    = retrS%codeIndexCloudFraction(istate)
          indexCol = indexCol + 1

          if ( .not. cloudAerosolRTMgridS%useCldAerFractionAllBands ) then
            if ( iband == retrS%codeSpecBand(istate) ) then
              if ( cldAerFractionS%nwavelCldAerFraction > 1 ) then
                do iwave = 1, wavelHRS%nwavel
                  polyderivValue = polyDeriv(errS, index, cldAerFractionS%wavelCldAerFraction, &
                                             cldAerFractionS%cldAerFraction, wavelHRS%wavel(iwave) )
                  reflDerivHRS%K_clr_lnvmr(iwave,istate)          = reflDerivHRS%K_clr_lnvmr(iwave,istate)  &
                                                                  * polyderivValue
                  reflDerivHRS%K_clr_vmr(iwave,istate)            = reflDerivHRS%K_clr_lnvmr(iwave,istate)            
                  reflDerivHRS%K_clr_ndens(iwave,istate)          = reflDerivHRS%K_clr_vmr(iwave,istate)
                  reflDerivHRS%KHR_clr_ndensCol(iwave, indexCol)  = reflDerivHRS%K_clr_vmr(iwave,istate)
                end do ! iwave
              end if ! cloudFractionS%nwavelCloudFraction > 1
            end if  ! iband == retrS%codeSpecBand(istate)
          end if ! .not. cloudAerosolRTMgridS%useCloudFractionAllBands

        case('mulOffset')

          index    = retrS%codeIndexMulOffset(istate)
          indexCol = indexCol + 1

          if ( iband == retrS%codeSpecBand(istate) ) then
            if ( mulOffsetS%nwavel > 1 ) then
              do iwave = 1, wavelHRS%nwavel
                polyderivValue = polyDeriv(errS, index, mulOffsetS%wavel, mulOffsetS%radiance, wavelHRS%wavel(iwave) )
                reflDerivHRS%K_clr_lnvmr(iwave,istate)          = reflDerivHRS%K_clr_lnvmr(iwave,istate)  &
                                                                * polyderivValue
                reflDerivHRS%K_clr_vmr(iwave,istate)            = reflDerivHRS%K_clr_lnvmr(iwave,istate)
                reflDerivHRS%K_clr_ndens(iwave,istate)          = reflDerivHRS%K_clr_lnvmr(iwave,istate)
                reflDerivHRS%KHR_clr_ndensCol(iwave,indexCol)   = reflDerivHRS%K_clr_lnvmr(iwave,istate)          
              end do ! iwave
            end if ! LambCloudS%nwavelAlbedo > 1
          end if  ! iband == retrS%codeSpecBand(istate)

        case('straylight')

          index    = retrS%codeIndexStraylight(istate)
          indexCol = indexCol + 1

          if ( iband == retrS%codeSpecBand(istate) ) then
            if ( strayLightS%nwavel > 1 ) then
              do iwave = 1, wavelHRS%nwavel
                polyderivValue = polyDeriv(errS, index, strayLightS%wavel, strayLightS%radiance, wavelHRS%wavel(iwave) )
                reflDerivHRS%K_clr_lnvmr(iwave,istate)          = reflDerivHRS%K_clr_lnvmr(iwave,istate)  &
                                                                * polyderivValue
                reflDerivHRS%K_clr_vmr(iwave,istate)            = reflDerivHRS%K_clr_lnvmr(iwave,istate)
                reflDerivHRS%K_clr_ndens(iwave,istate)          = reflDerivHRS%K_clr_lnvmr(iwave,istate)
                reflDerivHRS%KHR_clr_ndensCol(iwave,indexCol)   = reflDerivHRS%K_clr_lnvmr(iwave,istate)          
              end do ! iwave
            end if ! LambCloudS%nwavelAlbedo > 1
          end if  ! iband == retrS%codeSpecBand(istate)

        case('surfEmission')

          index    = retrS%codeIndexSurfEmission(istate)
          indexCol = indexCol + 1

          if ( iband == retrS%codeSpecBand(istate) ) then
            if ( surfaceS%nwavelEmission > 1 ) then
              do iwave = 1, wavelHRS%nwavel
                polyderivValue = polyDeriv(errS, index, surfaceS%wavelEmission, surfaceS%emission, wavelHRS%wavel(iwave) )
                reflDerivHRS%K_clr_lnvmr(iwave,istate)          = reflDerivHRS%K_clr_lnvmr(iwave,istate)  &
                                                                * polyderivValue
                reflDerivHRS%K_clr_vmr(iwave,istate)            = reflDerivHRS%K_clr_lnvmr(iwave,istate)
                reflDerivHRS%K_clr_ndens(iwave,istate)          = reflDerivHRS%K_clr_lnvmr(iwave,istate)
                reflDerivHRS%KHR_clr_ndensCol(iwave,indexCol)   = reflDerivHRS%K_clr_ndens(iwave,istate)          
              end do ! iwave
            end if ! LambCloudS%nwavelAlbedo > 1
          end if  ! iband == retrS%codeSpecBand(istate)

      end select

    end do ! istate

  end subroutine setPolynomialDerivatives_clr


  subroutine setPolynomialDerivatives_cld(errS, iband, nTrace, wavelHRS, retrS, cloudAerosolRTMgridS, &
                                          surfaceS, LambCloudS, cldAerFractionS, mulOffsetS,    &
                                          strayLightS, reflDerivHRS)

    ! The surface albedo, Lambertian cloud albedo, cloud fraction and straylight can be a polynomial in the 
    ! wavelength, specified by the value at one (constant value) or more wavelengths, called nodes. 
    ! In calculate_K the derivatives at the nodes were calculated and stored in  reflDerivHRS%K, but this
    ! gives not the proper wavelength dependence of the derivative. The calculation of the derivatives with
    ! proper wavelength dependence in completed here. Originally, this was part of calculate_K, but
    ! it is moved here because of DISMAS. In DISMAS this subroutine should be called after the values
    ! on the high-resolution grid have been calculated, simply because the derivatives are not
    ! 'smooth' enough for DISMAS, introducing substantial errors if it is kept in calculate_K.

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                       intent(in)  :: iband                ! number spectral band
    integer,                       intent(in)  :: nTrace               ! number of trace gases
    type(wavelHRType),             intent(in)  :: wavelHRS             ! high resolution wavelength grid
    type(retrType),                intent(in)  :: retrS                ! contains info on the fit parameters
    type(cloudAerosolRTMgridType), intent(in)  :: cloudAerosolRTMgridS ! cloud-aerosol properties
    type(LambertianType),          intent(in)  :: surfaceS             ! surface properties
    type(LambertianType),          intent(in)  :: LambCloudS           ! Lambertian cloud properties
    type(cldAerFractionType),      intent(in)  :: cldAerFractionS      ! cloud / aerosol fraction
    type(mulOffsetType),           intent(in)  :: mulOffsetS           ! multiplicative offset
    type(straylightType),          intent(in)  :: strayLightS          ! stray light

    type(reflDerivType),        intent(inout)  :: reflDerivHRS         ! reflectance(R) and derivatives(K) on HR grid

    ! local
    integer :: iwave, istate, index, indexCol
    real(8) :: polyderivValue

    ! initialize counter for derivatives for the (sub)columns
    indexCol = nTrace * retrS%ngaussCol

    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case( 'nodeTemp', 'offsetTemp', 'surfPressure', 'aerosolTau', 'aerosolSSA' , 'aerosolAC'  , &
              'cloudTau', 'cloudAC'   , 'intervalDP', 'intervalTop', 'intervalBot' , 'diffRingCoef', 'RingCoef')

          indexCol = indexCol + 1

        case('surfAlbedo')

          index    = retrS%codeIndexSurfAlb(istate)
          indexCol = indexCol + 1

          if ( .not. cloudAerosolRTMgridS%useAlbedoLambSurfAllBands ) then
            if ( iband == retrS%codeSpecBand(istate) ) then
              if ( surfaceS%nwavelAlbedo > 1 ) then
                do iwave = 1, wavelHRS%nwavel
                  polyderivValue = polyDeriv(errS, index, surfaceS%wavelAlbedo, surfaceS%albedo, wavelHRS%wavel(iwave) )
                  reflDerivHRS%K_cld_lnvmr(iwave,istate)         = reflDerivHRS%K_cld_lnvmr(iwave,istate)  &
                                                                 * polyderivValue
                  reflDerivHRS%K_cld_vmr(iwave,istate)           = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                  reflDerivHRS%K_cld_ndens(iwave,istate)         = reflDerivHRS%K_cld_vmr(iwave,istate)
                  reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol) = reflDerivHRS%K_cld_vmr(iwave,istate)
                end do ! iwave
              end if ! surfaceS%nwavelAlbedo > 1
            end if ! iband == retrS%codeSpecBand(istate)
          end if ! .not. cloudAerosolRTMgridS%useAlbedoLambSurfAllBands

        case('LambCldAlbedo')

          index    = retrS%codeIndexLambCldAlb(istate)
          indexCol = indexCol + 1

          if ( .not. cloudAerosolRTMgridS%useAlbedoLambCldAllBands ) then
            if ( iband == retrS%codeSpecBand(istate) ) then
              if ( LambCloudS%nwavelAlbedo > 1 ) then
                do iwave = 1, wavelHRS%nwavel
                  polyderivValue = polyDeriv(errS, index, LambCloudS%wavelAlbedo, LambCloudS%albedo, wavelHRS%wavel(iwave) )
                  reflDerivHRS%K_cld_lnvmr(iwave,istate)          = reflDerivHRS%K_cld_lnvmr(iwave,istate)  &
                                                                  * polyderivValue
                  reflDerivHRS%K_cld_vmr(iwave,istate)            = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                  reflDerivHRS%K_cld_ndens(iwave,istate)          = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                  reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol)  = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                end do ! iwave
              end if ! LambCloudS%nwavelAlbedo > 1
            end if  ! iband == retrS%codeSpecBand(istate)
          end if ! .not. cloudAerosolRTMgridS%useAlbedoLambCldAllBands

        case('cloudFraction')

          index    = retrS%codeIndexCloudFraction(istate)
          indexCol = indexCol + 1

          if ( .not. cloudAerosolRTMgridS%useCldAerFractionAllBands ) then
            if ( iband == retrS%codeSpecBand(istate) ) then
              if ( cldAerFractionS%nwavelCldAerFraction > 1 ) then
                do iwave = 1, wavelHRS%nwavel
                  polyderivValue = polyDeriv(errS, index, cldAerFractionS%wavelCldAerFraction, &
                                             cldAerFractionS%cldAerFraction, wavelHRS%wavel(iwave) )
                  reflDerivHRS%K_cld_lnvmr(iwave,istate)          = reflDerivHRS%K_cld_lnvmr(iwave,istate)  &
                                                                  * polyderivValue
                  reflDerivHRS%K_cld_vmr(iwave,istate)            = reflDerivHRS%K_cld_lnvmr(iwave,istate)            
                  reflDerivHRS%K_cld_ndens(iwave,istate)          = reflDerivHRS%K_cld_vmr(iwave,istate)
                  reflDerivHRS%KHR_cld_ndensCol(iwave, indexCol)  = reflDerivHRS%K_cld_vmr(iwave,istate)
                end do ! iwave
              end if ! cldAerFractionS%nwavelCldAerFraction > 1
            end if  ! iband == retrS%codeSpecBand(istate)
          end if ! .not. cloudAerosolRTMgridS%useCldAerFractionAllBands

        case('mulOffset')

          index    = retrS%codeIndexMulOffset(istate)
          indexCol = indexCol + 1

          if ( iband == retrS%codeSpecBand(istate) ) then
            if ( mulOffsetS%nwavel > 1 ) then
              do iwave = 1, wavelHRS%nwavel
                polyderivValue = polyDeriv(errS, index, mulOffsetS%wavel, mulOffsetS%radiance, wavelHRS%wavel(iwave) )
                reflDerivHRS%K_cld_lnvmr(iwave,istate)          = reflDerivHRS%K_cld_lnvmr(iwave,istate)  &
                                                                * polyderivValue
                reflDerivHRS%K_cld_vmr(iwave,istate)            = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                reflDerivHRS%K_cld_ndens(iwave,istate)          = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                reflDerivHRS%KHR_cld_ndensCol(iwave,indexCol)   = reflDerivHRS%K_cld_lnvmr(iwave,istate)          
              end do ! iwave
            end if ! mulOffsetS%nwavel > 1
          end if  ! iband == retrS%codeSpecBand(istate)

        case('straylight')

          index    = retrS%codeIndexStraylight(istate)
          indexCol = indexCol + 1

          if ( iband == retrS%codeSpecBand(istate) ) then
            if ( strayLightS%nwavel > 1 ) then
              do iwave = 1, wavelHRS%nwavel
                polyderivValue = polyDeriv(errS, index, strayLightS%wavel, strayLightS%radiance, wavelHRS%wavel(iwave) )
                reflDerivHRS%K_cld_lnvmr(iwave,istate)          = reflDerivHRS%K_cld_lnvmr(iwave,istate)  &
                                                                * polyderivValue
                reflDerivHRS%K_cld_vmr(iwave,istate)            = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                reflDerivHRS%K_cld_ndens(iwave,istate)          = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                reflDerivHRS%KHR_cld_ndensCol(iwave,indexCol)   = reflDerivHRS%K_cld_ndens(iwave,istate)          
              end do ! iwave
            end if ! LambCloudS%nwavelAlbedo > 1
          end if  ! iband == retrS%codeSpecBand(istate)

        case('surfEmission')

          index    = retrS%codeIndexSurfEmission(istate)
          indexCol = indexCol + 1

          if ( iband == retrS%codeSpecBand(istate) ) then
            if ( surfaceS%nwavelEmission > 1 ) then
              do iwave = 1, wavelHRS%nwavel
                polyderivValue = polyDeriv(errS, index, surfaceS%wavelEmission, surfaceS%emission, wavelHRS%wavel(iwave) )
                reflDerivHRS%K_cld_lnvmr(iwave,istate)          = reflDerivHRS%K_cld_lnvmr(iwave,istate)  &
                                                                * polyderivValue
                reflDerivHRS%K_cld_vmr(iwave,istate)            = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                reflDerivHRS%K_cld_ndens(iwave,istate)          = reflDerivHRS%K_cld_lnvmr(iwave,istate)
                reflDerivHRS%KHR_cld_ndensCol(iwave,indexCol)   = reflDerivHRS%K_cld_ndens(iwave,istate)          
              end do ! iwave
            end if ! LambCloudS%nwavelAlbedo > 1
          end if  ! iband == retrS%codeSpecBand(istate)

      end select

    end do ! istate

  end subroutine setPolynomialDerivatives_cld


  subroutine specifyNoise(errS,  wavelInstrS, solarIrradianceS, earthRadianceS )

    ! Assume shot noise that is proportional to the received radiance
    ! The noise is gaussian distributed with a standard deviation at wavelength(nwavel) nm equal to
    !      radiance(nwavel) / signalToNoise
    ! nwavel is chosen because the reflectance in the ozone window varies strongly with wavelength

    ! similarly, noise is added to the solar irradiance

    implicit none

    type(errorType),           intent(inout) :: errS
    type(wavelInstrType),      intent(in)    :: wavelInstrS
    type(SolarIrrType),        intent(inout) :: solarIrradianceS
    type(EarthRadianceType),   intent(inout) :: earthRadianceS

    ! coefficients
    real(8)                :: A, B

    ! random generator
    integer                :: i, nseed, DateTime(8)
    integer, allocatable   :: Seed(:) 
    real(8)                :: harvest1(wavelInstrS%nwavel), harvest2(wavelInstrS%nwavel)
    real(8)                :: y1(wavelInstrS%nwavel), y2(wavelInstrS%nwavel)

    ! error
    integer                :: iwave, statusSplint
    real(8)                :: factorIrr, factorRad
    real(8), parameter     :: PI = 3.141592653589793d0

    ! specify type of seed
    logical, parameter     :: fixed_seed = .false.

    ! control output
    logical, parameter  :: verbose = .false.

    ! calculate the spectrum of the standard deviation for the noise using interpolation and
    ! (optionally) assuming Gaussian shot noise
    if ( solarIrradianceS%SNS%useShotNoise ) then
      ! interpolate linearly to get the irradiance at the wavelengths when the S/N is specified
      ! and calculate the fvalues using S/N(w) = f(w) * sqrt(irradiance(w))
      do iwave = 1,  solarIrradianceS%SNS%numSNwavel
        factorIrr =  splintLin(errS, wavelInstrS%wavel, solarIrradianceS%solIrr,  &
                               solarIrradianceS%SNS%SNwavel(iwave), statusSplint)
        solarIrradianceS%SNS%fvalues(iwave) = solarIrradianceS%SNS%SN_ref_values(iwave) &
                                            / sqrt(factorIrr)
      end do

      if ( solarIrradianceS%SNS%numSNwavel == 1 ) then
        do iwave = 1, solarIrradianceS%nwavel
          solarIrradianceS%SN(iwave) = solarIrradianceS%SNS%fvalues(1)         &
                                     * sqrt( solarIrradianceS%solIrr(iwave) )
        end do
      else
        do iwave = 1, solarIrradianceS%nwavel
          solarIrradianceS%SN(iwave) = splintlin(errS, solarIrradianceS%SNS%SNwavel, solarIrradianceS%SNS%fvalues, &
                                                  wavelInstrS%wavel(iwave), statusSplint )                         &
                                     * sqrt( solarIrradianceS%solIrr(iwave) )
        end do
      end if ! solarIrradianceS%SNS%numSNwavel == 1

    else
      if ( solarIrradianceS%SNS%numSNwavel == 1 ) then
        do iwave = 1, solarIrradianceS%nwavel
          solarIrradianceS%SN(iwave) = solarIrradianceS%SNS%SN_ref_values(1)
        end do
      else
        do iwave = 1, solarIrradianceS%nwavel
          solarIrradianceS%SN(iwave) = splintlin(errS, solarIrradianceS%SNS%SNwavel,solarIrradianceS%SNS%SN_ref_values, &
                                                 wavelInstrS%wavel(iwave), statusSplint )
        end do 
      end if  
    end if

    if ( solarIrradianceS%SNS%use_S5_SNR ) then
      call calcSNR_S5(errS, wavelInstrS, solarIrradianceS%solIrr, solarIrradianceS%SN)
      if (errorCheck(errS)) return
    end if 

    ! truncate SN for solar irradiance when it is larger than SNmax
    where ( solarIrradianceS%SN(:) > solarIrradianceS%SNmax ) solarIrradianceS%SN(:) = solarIrradianceS%SNmax  

    ! The signal to noise values specified for the solar irradiance pertain to the actual irradiance
    ! specified for the solarIrradianceS%refSpectralBinSize. This spectral bin size can differ
    ! from the actual bin size that is given by wavelInstrS%stepWavel.

    solarIrradianceS%solIrrError = solarIrradianceS%solIrr / solarIrradianceS%SN       &
                * sqrt( wavelInstrS%stepWavel / solarIrradianceS%SNS%refSpectralBinSize )

    if (verbose) then
      write(intermediateFileUnit,'(A)') &
            ' output from specifyNoise: wavel,  earthRadianceS%radianceRef'
      do iwave = 1,  wavelInstrS%nwavel
        write(intermediateFileUnit,'(f8.3, 2E15.5)') wavelInstrS%wavel(iwave),        &
                   earthRadianceS%radianceRef(iwave)
      end do
    end if

    if ( earthRadianceS%SNS%useShotNoise ) then
      ! interpolate linearly to get the irradiance at the wavelengths when the S/N is specified
      ! and calculate the fvalues using S/N(w) = f(w) * sqrt(radiance(w))
      do iwave = 1,  earthRadianceS%SNS%numSNwavel
        factorRad =  splintLin(errS, wavelInstrS%wavel, earthRadianceS%radianceRef,  &
                               earthRadianceS%SNS%SNwavel(iwave), statusSplint)
        earthRadianceS%SNS%fvalues(iwave) = earthRadianceS%SNS%SN_ref_values(iwave) &
                                          / sqrt(factorRad)
      end do

      if (verbose) then
        write(intermediateFileUnit,'(A)') &
              ' output from specifyNoise: earthRadianceS%SNS%SNwavel,  earthRadianceS%SNS%fvalues '
        do iwave = 1,  earthRadianceS%SNS%numSNwavel
          write(intermediateFileUnit,'(f8.3, 2E15.5)') earthRadianceS%SNS%SNwavel(iwave),        &
                     earthRadianceS%SNS%fvalues(iwave)
        end do
      end if


      if ( earthRadianceS%SNS%numSNwavel == 1 ) then
      
        earthRadianceS%SNrefspec(:) = earthRadianceS%SNS%fvalues(1)     &
                                    * sqrt( earthRadianceS%radianceRef(:) )
                                    
      else
        do iwave = 1, wavelInstrS%nwavel
          earthRadianceS%SNrefspec(iwave) = sqrt( earthRadianceS%radianceRef(iwave) ) &
            * splintlin(errS, earthRadianceS%SNS%SNwavel,earthRadianceS%SNS%fvalues,wavelInstrS%wavel(iwave),statusSplint)
        end do
      end if ! earthRadianceS%SNS%numSNwavel == 1

    else
      if ( earthRadianceS%SNS%numSNwavel == 1 ) then
        earthRadianceS%SNrefspec(:) = earthRadianceS%SNS%SN_ref_values(1)
      else
        do iwave = 1, wavelInstrS%nwavel
          earthRadianceS%SNrefspec(iwave) = &
          splintlin(errS, earthRadianceS%SNS%SNwavel,earthRadianceS%SNS%SN_ref_values,wavelInstrS%wavel(iwave),statusSplint)
        end do
      end if ! earthRadianceS%SNS%numSNwavel == 1
    end if ! earthRadianceS%SNS%useShotNoise

    if (verbose) then
      write(intermediateFileUnit,'(A)') &
            ' output from specifyNoise: wavel, earthRadianceS%SNrefspec'
      do iwave = 1,  wavelInstrS%nwavel
        write(intermediateFileUnit,'(f8.3, 2E15.5)') wavelInstrS%wavel(iwave),        &
                  earthRadianceS%SNrefspec(iwave)
      end do
    end if

    ! let
    ! L_ref     be the radiance reference spectrum
    ! L         be the current radiance spectrum
    ! SN_ref    be the signal to noise ratio for the reference spectrum
    ! SN        be the signal to noise ratio for the current radiance spectrum
    !
    ! assuming shot noise we have
    ! SN = SN_ref * sqrt( L / L_ref ) * sqrt( wavelInstrS%stepWavel / earthRadianceS%refSpectralBinSize )

    ! put the actual signal to noise in  earthRadianceS%SN
    earthRadianceS%SN = earthRadianceS%SNrefspec * sqrt( earthRadianceS%rad(1,:) /  earthRadianceS%radianceRef ) &
                      * sqrt( wavelInstrS%stepWavel / earthRadianceS%SNS%refSpectralBinSize )

!   use this for Fourier Transform Spectroscopy (FTS) e.g. GOSAT - no shot noise
!    earthRadianceS%SN = earthRadianceS%SNrefspec(1) * earthRadianceS%radiance(1,:) /  earthRadianceS%radianceRef(1)

    if ( earthRadianceS%SNS%useLAB_SNR ) then
      ! use the expression provided by Yaska Meijer, ESA for NO2 (May 2019)
      A = earthRadianceS%SNS%A
      B = earthRadianceS%SNS%B
      call calcLAB_SNR(errS, wavelInstrS, earthRadianceS%rad(1,:), A, B, earthRadianceS%SN)
    end if

    if ( earthRadianceS%SNS%use_S5_SNR ) then
      call calcSNR_S5(errS, wavelInstrS, earthRadianceS%rad(1,:), earthRadianceS%SN)
      if (errorCheck(errS)) return
    end if

    ! truncate SN for Earth radiance when it is larger than SNmax
    where ( earthRadianceS%SN(:) > earthRadianceS%SNmax ) earthRadianceS%SN(:) = earthRadianceS%SNmax  

    ! calculate the signal to noise ratio for the reflectance (same as for sun-normalized radiance)
    earthRadianceS%SNrefl(:) = 1.0d0 / sqrt( earthRadianceS%SN(:)**(-2) + solarIrradianceS%SN(:)**(-2) )

    earthRadianceS%rad_error = earthRadianceS%rad(1,:) / earthRadianceS%SN

    ! create gaussian noise for radiance (see function gasdev in Press et al.)
    CALL RANDOM_SEED ( )                   ! initializes seed
    CALL RANDOM_SEED (SIZE = nseed)        ! nseed is the size of the seed array
    ALLOCATE(Seed(nseed))

    if ( fixed_seed ) then
      Seed(1) = 10
      Seed(2) = 100
    else
      CALL DATE_AND_TIME(VALUES = DateTime)
      do i = 1, nseed
        Seed(i) = datetime(7+ modulo(i,2))
      end do
    end if ! fixed seed

    CALL RANDOM_SEED (PUT=Seed(1:nseed))   ! Use Seed as seed

    CALL random_number(HARVEST=harvest1)   ! random numbers, uniform on [0.1)
    CALL random_number(HARVEST=harvest2)   ! random numbers, uniform on [0.1)

    deallocate ( Seed )

    ! Box - Muller transformation for gaussian noise : see Sec. 7.2 of Press et al.

    y1 = 0d0
    where (harvest1 /= 0d0)
      y1 = sqrt(-2d0*log(harvest1)) * cos(2d0 * PI * harvest2)
      y2 = sqrt(-2d0*log(harvest1)) * sin(2d0 * PI * harvest2)
    end where

    ! add random noise
    if ( solarIrradianceS%SNS%addNoise) then
      solarIrradianceS%solIrrMeas = solarIrradianceS%solIrr + solarIrradianceS%solIrrError * y1
    else
      solarIrradianceS%solIrrMeas = solarIrradianceS%solIrr
    end if

    ! Noise is only added for the entire pixel, becuase the S/N values vary for the clear and cloudy part.
    if ( earthRadianceS%SNS%addNoise) then
      earthRadianceS%rad_meas(1,:) = earthRadianceS%rad(1,:) + earthRadianceS%rad_error * y2
    else
      earthRadianceS%rad_meas(1,:) = earthRadianceS%rad(1,:)
    end if


    if (verbose) then
      write(intermediateFileUnit,'(A)') &
            ' output from specifyNoise: wavel, radiance, radianceMeas, solIrr, and solIrrMeas '
      do iwave = 1,  wavelInstrS%nwavel
        write(intermediateFileUnit,'(f8.3, 4E15.5)') wavelInstrS%wavel(iwave),      &
                  earthRadianceS%rad(1,iwave), earthRadianceS%rad_meas(1,iwave),    &
                  solarIrradianceS%solIrr(iwave), solarIrradianceS%solIrrMeas(iwave)
      end do
      write(intermediateFileUnit,'(A)') &
            ' output from specifyNoise: wavel, radianceError, and solIrrError '
      do iwave = 1,  wavelInstrS%nwavel
        write(intermediateFileUnit,'(f8.3, 2E15.5)') wavelInstrS%wavel(iwave),        &
                  earthRadianceS%rad_error(iwave), solarIrradianceS%solIrrError(iwave)
      end do
    end if

  end subroutine specifyNoise


  subroutine calcSNR_S5(errS, wavelInstrS, radiance, SN)
    
    ! calculates the signal to noise ratio for Sentinel 5 and puts the results
    ! in the array SN. It is assumed that the radiance is given in ph/cm2/s/sr/nm.
    ! Parameterization provided by Joerg Langen for ESA in October 2013.

    implicit none

    type(errorType),      intent(inout) :: errS
    type(wavelInstrType), intent(in)    :: wavelInstrS
    real(8),              intent(inout) :: radiance(wavelInstrS%nwavel)
    real(8),              intent(inout) :: SN(wavelInstrS%nwavel)

    ! local
    integer :: i, iwave, iheader
    integer :: openErrorSNRFile, statusSplint
    integer :: nwavel, nheader
    real(8) :: w, d, a, b

    real(8) :: L_node(3), SNR_node(3)  ! obtained after linear interpolation on the wavelength

    real(8) :: wUV1(360), LminUV1(360), LrefUV1(360), LmaxUV1(360)
    real(8) :: SNRLminUV1(360), SNRLrefUV1(360), SNRLmaxUV1(360)

    real(8) :: wUV2VIS(1129), LminUV2VIS(1129), LrefUV2VIS(1129), LmaxUV2VIS(1129)
    real(8) :: SNRLminUV2VIS(1129), SNRLrefUV2VIS(1129), SNRLmaxUV2VIS(1129)

    logical, parameter :: verbose     = .false.
    logical, parameter :: use_old_SNR = .false.

    ! for 270 - 300 nm
    real(8), parameter :: a_1 = 4.70194461239d-05
    real(8), parameter :: b_1 = 3449239.8849d0
    ! for 303 - 310 nm
    real(8), parameter :: a0_4 =  4.67913725e-06  
    real(8), parameter :: a1_4 = -1.26105546e-05  
    real(8), parameter :: a2_4 =  1.39147643e-05  
    real(8), parameter :: a3_4 = -5.39067088e-06 
    real(8), parameter :: b0_4 = -407188.40771951 
    real(8), parameter :: b1_4 = 1161526.66109376 
    ! for 300 - 330 nm
    real(8), parameter :: a0_2 =  3.03796420d-07
    real(8), parameter :: a1_2 = -6.81549664d-07
    real(8), parameter :: a2_2 =  6.78226603d-07
    real(8), parameter :: a3_2 = -2.70807116d-07
    real(8), parameter :: b0_2 = 131105.24706965d0
    real(8), parameter :: b1_2 = 15500.79117382d0
    ! for 330 - 500 nm
    real(8), parameter :: a_3 = 3.7839338322d-07
    real(8), parameter :: b_3 = 787116.299872

    if ( use_old_SNR ) then
      do iwave = 1, wavelInstrS%nwavel
        w = wavelInstrS%wavel(iwave)

        if ( w <= 300.0d0 ) then
          a = a_1
          b = b_1
        end if
        if ( w >= 303.0d0 .and. w <= 310.0d0) then
          d = (w - 302.0d0) / 8.0d0
          a = a0_4 + a1_4 * d + a2_4 * d**2 + a3_4 * d**3 
          b = b0_4 + b1_4 / d 
        end if
        if ( w > 310.0d0 .and. w < 330.0d0) then
          d    = (w - 309.0d0) / 21.0d0
          a = a0_2 + a1_2 * d + a2_2 * d**2 + a3_2 * d**3 
          b = b0_2 + b1_2 / d 
        end if
        if ( w >= 330.0d0 .and. w <= 500.0d0 ) then
          a = a_3
          b = b_3
        end if

        SN(iwave) = a * radiance(iwave) &
                  / sqrt( a * radiance(iwave) + b )

        if ( w > 500.0d0 .or. (w > 300.0d0 .and. w < 303.0d0) ) then
          call logDebug('Parameterization of SNR provided by ESA for S5 is not available')
          call logDebug('for wavelengths 300 - 303 nm and')
          call logDebug('for wavelengths larger than 500 nm')
          call logDebug('use the normal specification of SNR')
          call logDebug('message from calcSNR_S5 in module radianceIrradianceModule')
          call mystop(errS, 'parameterization SNR for S5 not available')
          if (errorCheck(errS)) return
        end if

      end do ! iwave

    else 
      ! use new SNR data obtained from ESA in November 2013

      if ( (wavelInstrS%startWavel < 270.0d0) .or. (wavelInstrS%endWavel > 500.0d0) ) then
        call logDebug('wavelength range outside (270, 500) nm')
        call logDebug('no specification SNR for Sentinel 5 is available')
        write(errS%temp,'(A, F10.4)') 'wavelength start = ', wavelInstrS%startWavel
        call errorAddLine(errS, errS%temp)
        write(errS%temp,'(A, F10.4)') 'wavelength end   = ', wavelInstrS%endWavel
        call errorAddLine(errS, errS%temp)
        call myStop(errS,  'stopped because SNR is unknown for this wavelength range')
        if (errorCheck(errS)) return
      end if

      if ( (wavelInstrS%startWavel < 312.0d0) .and. (wavelInstrS%endWavel > 330.0d0) ) then
        call logDebug('wavelength range uses two spectral bands: UV1 and UV2VIS')
        call logDebug('results are not reliable for this range')
        write(errS%temp,'(A, F10.4)') 'wavelength start = ', wavelInstrS%startWavel
        call errorAddLine(errS, errS%temp)
        write(errS%temp,'(A, F10.4)') 'wavelength end   = ', wavelInstrS%endWavel
        call errorAddLine(errS, errS%temp)
        ! call myStop(errS,  'stopped because two spectral bands are needed')
      end if

      if ( (wavelInstrS%startWavel >= 270.0d0) .and. (wavelInstrS%endWavel <= 330.0d0) ) then
        ! use UV1 spectral band
         open(UNIT=S5_SNR_FileUnit, ACTION= 'READ', STATUS = 'old', IOSTAT= openErrorSNRFile, &
              FILE='RefSpec/S5_ASG_SNR_model_UV1_L_SNR.dat')
         if (openErrorSNRFile /= 0) then
           call logDebug('ERROR in calcSNR_S5: failed to open file')
           call logDebug('Filename: ' // 'RefSpec/S5_ASG_SNR_model_UV1_L_SNR.dat')
           call mystop(errS, 'stopped due to SNR file error')
           if (errorCheck(errS)) return
         end if

         nheader = 3
         do iheader = 1, nheader
           read(S5_SNR_FileUnit,*)
         end do
         nwavel  = 360
         do i = 1, nwavel
           read(S5_SNR_FileUnit,*)  wUV1(i), LminUV1(i), LrefUV1(i), LmaxUV1(i),  &
                                    SNRLminUV1(i),  SNRLrefUV1(i), SNRLmaxUV1(i)
         end do

         close(UNIT = S5_SNR_FileUnit)

         ! calculate SNR using linear interpolation
         do iwave = 1, wavelInstrS%nwavel 
           L_node(1)   = splintLin(errS, wUV1, LminUV1, wavelInstrS%wavel(iwave), statusSplint)
           L_node(2)   = splintLin(errS, wUV1, LrefUV1, wavelInstrS%wavel(iwave), statusSplint)
           L_node(3)   = splintLin(errS, wUV1, LmaxUV1, wavelInstrS%wavel(iwave), statusSplint)
           SNR_node(1) = splintLin(errS, wUV1, SNRLminUV1, wavelInstrS%wavel(iwave), statusSplint)
           SNR_node(2) = splintLin(errS, wUV1, SNRLrefUV1, wavelInstrS%wavel(iwave), statusSplint)
           SNR_node(3) = splintLin(errS, wUV1, SNRLmaxUV1, wavelInstrS%wavel(iwave), statusSplint)
           SN(iwave)   = splintLin(errS, L_node, SNR_node, radiance(iwave), statusSplint)
         end do
      end if 

      if ( (wavelInstrS%startWavel >= 312.0d0) .and. (wavelInstrS%endWavel <= 500.0d0) ) then
        ! use UV2VIS spectral band
         open(UNIT=S5_SNR_FileUnit, ACTION= 'READ', STATUS = 'old', IOSTAT= openErrorSNRFile, &
              FILE='RefSpec/S5_ASG_SNR_model_UV2VIS_L_SNR.dat')
         if (openErrorSNRFile /= 0) then
           call logDebug('ERROR in calcSNR_S5: failed to open file')
           call logDebug('Filename: ' // 'RefSpec/S5_ASG_SNR_model_UV2VIS_L_SNR.dat')
           call mystop(errS, 'stopped due to SNR file error')
           if (errorCheck(errS)) return
         end if
         nheader = 3
         do iheader = 1, nheader
           read(S5_SNR_FileUnit,*)
         end do
         nwavel  = 1129
         do i = 1, nwavel
           read(S5_SNR_FileUnit,*)  wUV2VIS(i), LminUV2VIS(i), LrefUV2VIS(i), LmaxUV2VIS(i),  &
                                    SNRLminUV2VIS(i),  SNRLrefUV2VIS(i), SNRLmaxUV2VIS(i)
         end do
         close(UNIT = S5_SNR_FileUnit)

         ! calculate SNR using linear interpolation
         do iwave = 1, wavelInstrS%nwavel 
           L_node(1)   = splintLin(errS, wUV2VIS, LminUV2VIS, wavelInstrS%wavel(iwave), statusSplint)
           L_node(2)   = splintLin(errS, wUV2VIS, LrefUV2VIS, wavelInstrS%wavel(iwave), statusSplint)
           L_node(3)   = splintLin(errS, wUV2VIS, LmaxUV2VIS, wavelInstrS%wavel(iwave), statusSplint)
           SNR_node(1) = splintLin(errS, wUV2VIS, SNRLminUV2VIS, wavelInstrS%wavel(iwave), statusSplint)
           SNR_node(2) = splintLin(errS, wUV2VIS, SNRLrefUV2VIS, wavelInstrS%wavel(iwave), statusSplint)
           SNR_node(3) = splintLin(errS, wUV2VIS, SNRLmaxUV2VIS, wavelInstrS%wavel(iwave), statusSplint)
           SN(iwave)   = splintLin(errS, L_node, SNR_node, radiance(iwave), statusSplint)
         end do

      end if

    end if ! use_old_SNR

    if ( verbose ) then
      write(intermediateFileUnit,*) 'wavelength     radiance      SNR'
      do iwave = 1, wavelInstrS%nwavel
         write(intermediateFileUnit,'(F10.4,2ES15.5)') wavelInstrS%wavel(iwave),  radiance(iwave), SN(iwave)
      end do ! iwave
    end if ! verbose

  end subroutine calcSNR_S5

  subroutine calcLAB_SNR(errS, wavelInstrS, radiance, A, B, SN)
    
    ! calculates the signal to noise ratio for the radiance according to

    ! SNR = A * radiance / sqrt( A * radiance + B*B )

    ! which differs from shot noise due to the B*B term
    ! It is assumed that the radiance is given in ph/cm2/s/sr/nm.
    ! values of A and B are given in the configuration file and stored
    ! earthRadianceSimS (iband)%SNS%A  and earthRadianceSimS (iband)%SNS%B

    implicit none

    type(errorType),      intent(inout) :: errS
    type(wavelInstrType), intent(in)    :: wavelInstrS
    real(8),              intent(inout) :: radiance(wavelInstrS%nwavel)
    real(8),              intent(in)    :: A, B
    real(8),              intent(inout) :: SN(wavelInstrS%nwavel)

    ! local
    integer  :: iwave

    do iwave = 1, wavelInstrS%nwavel
      SN(iwave) = A*radiance(iwave) / sqrt( A*radiance(iwave)  + B**2)
    end do

  end subroutine calcLAB_SNR

  subroutine fillReferenceSpectrum(errS, controlSimS, wavelHRSimS, wavelInstrSimS, earthRadianceSimS, &
                                   earthRadianceRetrS)

    ! The signal to noise values specified for the earth radiance pertain to
    ! a reference spectrum. There can be different types of reference spectra:
    ! - a reference spectrum read from file
    ! - a reference spectrum based on observing a Lambertian reflector located above the atmosphere
    !   having a certain albedo and assuming that the solar zenith angle = 0.
    ! - the currently modeled radiance spectrum
    ! In addition, the S/N ratio depends on the earthRadianceS%refSpectralBinSize that might differ
    ! from the actual bin size that is given by wavelInstrS%stepWavel. If you want calculations
    ! for different FWHM and associated wavelInstrS%stepWavel keeping the total amount of photons
    ! received constant, then one can use a fixed refSpectralBinSize.

    implicit none

      type(errorType), intent(inout) :: errS
    type(controlType),         intent(in)    :: controlSimS
    type(wavelHRType),         intent(in)    :: wavelHRSimS
    type(wavelInstrType),      intent(in)    :: wavelInstrSimS
    type(earthRadianceType),   intent(inout) :: earthRadianceSimS
    type(earthRadianceType),   intent(inout) :: earthRadianceRetrS

    select case ( earthRadianceSimS%refSpecType )

      case(0) ! reference spectrum read from file

        call getRadianceRefSpectrumfromFile(errS, wavelHRSimS, earthRadianceSimS)
        if (errorCheck(errS)) return
        call integrateSlitFunctionRefSpec(errS, controlSimS, wavelHRSimS, wavelInstrSimS, earthRadianceSimS)
        if (errorCheck(errS)) return

      case(1) ! currently modelled spectrum

        earthRadianceSimS%radianceRef(:) = earthRadianceSimS%rad(1,:)

      case default

        call logDebug(' ERROR: in specifyNoise incorrect value for earthRadianceS%refSpecType ')
        call logDebugI('        refSpecType = ', earthRadianceSimS%refSpecType)
        call mystop(errS, ' stopped because of an incorrect refSpecType')
        if (errorCheck(errS)) return

    end select

    earthRadianceRetrS%radianceRef = earthRadianceSimS%radianceRef 


  end subroutine fillReferenceSpectrum


  subroutine addSpecFeaturesIrr(errS, wavelHRS, solarIrradianceS)

    implicit none

    ! a sine is used for the spectral feature considered

      type(errorType), intent(inout) :: errS
    type(wavelHRType),         intent(in)    :: wavelHRS         ! high resolution wavelength grid for simulation
    type(solarIrrType),        intent(inout) :: solarIrradianceS ! solar irradiance on HR and instrument grid

    real(8), parameter  :: PI = 3.141592653589793d0

    integer            :: iwave
    real(8)            :: fadd, fmul
    real(8)            :: dw

    do iwave = 2, wavelHRS%nwavel
      dw = wavelHRS%wavel(iwave) - wavelHRS%wavel(1)
      fadd = solarIrradianceS%solIrrMR(1) * 0.01d0 * solarIrradianceS%featureAmplAdd   &
           * sin(dw*2.0d0*PI/solarIrradianceS%featurePeriodAdd + solarIrradianceS%featurePhaseAdd*PI/180.0d0 )

      fmul =  0.01d0 * solarIrradianceS%featureAmplMul  &
           * sin(dw*2.0d0*PI/ solarIrradianceS%featurePeriodMul + solarIrradianceS%featurePhaseMul*PI/180.0d0 )

      
      solarIrradianceS%solIrrMR(iwave) = solarIrradianceS%solIrrMR(iwave) * (1.0d0 + fmul) + fadd

    end do   

  end subroutine addSpecFeaturesIrr


  subroutine addSpecFeaturesRad(errS, wavelHRS, earthRadianceS)

    implicit none

    ! a sine is used for the spectral feature considered

      type(errorType), intent(inout) :: errS
    type(wavelHRType),         intent(in)    :: wavelHRS         ! high resolution wavelength grid for simulation
    type(earthRadianceType),   intent(inout) :: earthRadianceS   ! earth radiance on HR and instrument grid

    real(8), parameter  :: PI = 3.141592653589793d0

    integer            :: iwave
    real(8)            :: fadd, fmul
    real(8)            :: dw

    logical, parameter :: verbose = .false.

    do iwave = 2, wavelHRS%nwavel
      dw = wavelHRS%wavel(iwave) - wavelHRS%wavel(1)
! JdH changed to first wavelength instead of last wavelength
!      fadd = earthRadianceS%radiance_HR(wavelHRS%nwavel) * 0.01d0 * earthRadianceS%featureAmplAdd   &
!           * sin(dw*2.0d0*PI/earthRadianceS%featurePeriodAdd + earthRadianceS%featurePhaseAdd*PI/180.0d0 )
      fadd = earthRadianceS%rad_HR(1,1) * 0.01d0 * earthRadianceS%featureAmplAdd   &
           * sin( dw*2.0d0*PI/earthRadianceS%featurePeriodAdd + earthRadianceS%featurePhaseAdd*PI/180.0d0 )

      fmul =  0.01d0 * earthRadianceS%featureAmplMul  &
           * sin(dw*2.0d0*PI/ earthRadianceS%featurePeriodMul + earthRadianceS%featurePhaseMul*PI/180.0d0 )
      
      earthRadianceS%rad_HR   (1, iwave) = earthRadianceS%rad_HR   (1, iwave) * (1.0d0 + fmul) + fadd
      earthRadianceS%rad_ns_HR(1, iwave) = earthRadianceS%rad_ns_HR(1, iwave) * (1.0d0 + fmul) + fadd
      if (verbose) write(intermediateFileUnit, '(F12.5, 2E15.5)') wavelHRS%wavel(iwave), fadd, fmul
    end do

  end subroutine addSpecFeaturesRad


  subroutine addSimpleOffsets(errS, solarIrradianceS, earthRadianceS)

    implicit none

    ! add offsets

    type(errorType),           intent(inout) :: errS
    type(solarIrrType),        intent(inout) :: solarIrradianceS ! solar irradiance on HR and instrument grid
    type(earthRadianceType),   intent(inout) :: earthRadianceS   ! radiance and derivatives on HR and instrument grid


    solarIrradianceS%solIrrMeas = (1.0d0 + 0.01d0*solarIrradianceS%offsetMul) * solarIrradianceS%solIrrMeas
    ! reference is first wavelength
    solarIrradianceS%solIrrMeas = solarIrradianceS%solIrrMeas &
                    + 0.01d0*solarIrradianceS%offsetAdd * solarIrradianceS%solIrrMeas(1)
    solarIrradianceS%solIrr = solarIrradianceS%solIrrMeas

    earthRadianceS%rad_meas = (1.0d0 + 0.01d0*earthRadianceS%offsetMul) * earthRadianceS%rad_meas
    ! reference is first wavelength

    ! use percentage of current spectrum
    earthRadianceS%rad_meas(1,:) = earthRadianceS%rad_meas(1,:) &
                    + 0.01d0*earthRadianceS%offsetAdd * earthRadianceS%rad_meas(1,1)
    earthRadianceS%rad(1,:) = earthRadianceS%rad_meas(1,:)

  end subroutine addSimpleOffsets


  subroutine addSmear(errS, wavelInstrSimS, solarIrradianceS, earthRadianceS)

    implicit none

    ! add smear - modelling imperfect charge transport during readout of CCD

      type(errorType), intent(inout) :: errS
    type(wavelInstrType),      intent(in)    :: wavelInstrSimS
    type(solarIrrType),        intent(inout) :: solarIrradianceS ! solar irradiance on HR and instrument grid
    type(earthRadianceType),   intent(inout) :: earthRadianceS   ! radiance and derivatives on HR and instrument grid

    ! local
    integer :: iwave
    real(8) :: first, last
    real(8) :: smear

    first = solarIrradianceS%solIrrMeas(1)
    last  = solarIrradianceS%solIrrMeas(wavelInstrSimS%nwavel)
    do iwave = 1, wavelInstrSimS%nwavel - 1
      smear = 1.0d-2 * solarIrradianceS%percentSmear * solarIrradianceS%solIrrMeas(iwave)
      solarIrradianceS%solIrrMeas(iwave)       =  solarIrradianceS%solIrrMeas(iwave)      - smear
      solarIrradianceS%solIrrMeas(iwave + 1 )  =  solarIrradianceS%solIrrMeas(iwave + 1 ) + smear
    end do ! iwave
    solarIrradianceS%solIrrMeas(1)                     = first
    solarIrradianceS%solIrrMeas(wavelInstrSimS%nwavel) = last

    first = earthRadianceS%rad_meas(1, 1)
    last  = earthRadianceS%rad_meas(1, wavelInstrSimS%nwavel)
    do iwave = 1, wavelInstrSimS%nwavel - 1
      smear = 1.0d-2 * earthRadianceS%percentSmear * earthRadianceS%rad_meas(1, iwave)
      earthRadianceS%rad_meas(1, iwave)       =  earthRadianceS%rad_meas(1, iwave)      - smear
      earthRadianceS%rad_meas(1, iwave + 1 )  =  earthRadianceS%rad_meas(1, iwave + 1 ) + smear
    end do ! iwave
    earthRadianceS%rad_meas(1, 1)                     = first
    earthRadianceS%rad_meas(1, wavelInstrSimS%nwavel) = last

  end subroutine addSmear


  subroutine fillRadianceAtMulOffsetNodes (errS, wavelInstrS, earthRadianceS, mulOffsetS) 

    ! fill radiance at the wavelengths where multiplicative ofsets are specified

    implicit none

      type(errorType), intent(inout) :: errS
    type(wavelInstrType),      intent(in)    :: wavelInstrS       
    type(earthRadianceType),   intent(in)    :: earthRadianceS
    type(mulOffsetType),       intent(inout) :: mulOffsetS

    ! local
    integer                :: iwave, status

    ! use linear interpolation to find the radiance of the CURRENT spectrum 
    ! at the wavelengths where the offsets are specified

    do iwave = 1, mulOffsetS%nwavel
      mulOffsetS%radiance(iwave) =  &
        splintLin(errS, wavelInstrS%wavel, earthRadianceS%rad_meas(1,:), mulOffsetS%wavel(iwave), status)
    end do

  end subroutine fillRadianceAtMulOffsetNodes


  subroutine fillRadianceAtStrayLightNodes (errS, wavelInstrS, earthRadianceS, strayLightS) 

    ! fill radiance at the wavelengths where stray light is specified

    implicit none

    type(errorType),           intent(inout) :: errS
    type(wavelInstrType),      intent(in)    :: wavelInstrS       
    type(earthRadianceType),   intent(in)    :: earthRadianceS
    type(straylightType),      intent(inout) :: strayLightS

    ! local
    integer                :: iwave, status

    if ( strayLightS%useReferenceSpectrum ) then
      ! use linear interpolation to find the radiance of the REFERENCE spectrum 
      ! at the wavelengths where the straylight is specified

      do iwave = 1, strayLightS%nwavel
        strayLightS%radiance(iwave) =  &
          splintLin(errS, wavelInstrS%wavel, earthRadianceS%radianceRef, strayLightS%wavel(iwave), status)
      end do
    else
      ! use linear interpolation to find the radiance of the CURRENT spectrum 
      ! at the wavelengths where the straylight is specified

      do iwave = 1, strayLightS%nwavel
        strayLightS%radiance(iwave) =  &
          splintLin(errS, wavelInstrS%wavel, earthRadianceS%rad_meas(1,:), strayLightS%wavel(iwave), status)
      end do
    end if

  end subroutine fillRadianceAtStrayLightNodes


  subroutine addMulOffset(errS, wavelInstrS, earthRadMulOffsetS, earthRadianceS)

    ! depending on the flag earthRadMulOffset%useLinearInterpolation use
    ! either linear interpolated values or a low degree polynomial, to represent multiplicative offset
    ! for the earth radiance
    ! the degree of the polynomial is the number of nodes - 1 that are specified in the configuration file

    implicit none

    type(errorType),           intent(inout) :: errS
    type(wavelInstrType),      intent(in)    :: wavelInstrS       
    type(mulOffsetType),       intent(inout) :: earthRadMulOffsetS  ! multiplicative offset
    type(earthRadianceType),   intent(inout) :: earthRadianceS

    ! local
    integer                :: iwave, status
    real(8)                :: offsetSpectrum(wavelInstrS%nwavel)

    logical, parameter     :: verbose = .false.

    ! initialize
    offsetSpectrum = 0.0d0

    select case ( earthRadMulOffsetS%nwavel )

      case(1) ! no wavelength dependence stray light (in terms of percent radiance at one node)
        offsetSpectrum(:) = 0.01d0 * earthRadMulOffsetS%percentOffset(1) * earthRadianceS%rad_meas(1,:)

      case(2:100)

        if ( earthRadMulOffsetS%useLinearInterpolation ) then

          do iwave = 1, wavelInstrS%nwavel
            offsetSpectrum(iwave) = 0.01d0 *  splintLin(errS, earthRadMulOffsetS%wavel,     &
                       earthRadMulOffsetS%percentOffset, wavelInstrS%wavel(iwave), status)  &
                     * earthRadianceS%rad_meas(1,iwave)
          end do

        else

          do iwave = 1, wavelInstrS%nwavel
            offsetSpectrum(iwave) = 0.01d0 * earthRadianceS%rad_meas(1,iwave) &
              * polyInt(errS, earthRadMulOffsetS%wavel, earthRadMulOffsetS%percentOffset, wavelInstrS%wavel(iwave))
          end do

        end if
      case default

        call mystop(errS, 'incorrect number for stray light nodes in radianceIrradiance : addStraylight')
        if (errorCheck(errS)) return

    end select

    earthRadianceS%rad(1,:)      = earthRadianceS%rad(1,:)      + offsetSpectrum(:)
    earthRadianceS%rad_meas(1,:) = earthRadianceS%rad_meas(1,:) + offsetSpectrum(:)

    if ( verbose) then
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,'(A)') 'earth radiance without offset    offset    total earth radiance '
      do iwave = 1, wavelInstrS%nwavel
        write(intermediateFileUnit,'(F12.5, 3ES20.10)') wavelInstrS%wavel(iwave),                                 &
                                                        earthRadianceS%rad_meas(1,iwave) - offsetSpectrum(iwave), &
                                                        offsetSpectrum(iwave), earthRadianceS%rad_meas(1,iwave)
      end do
    end if

  end subroutine addMulOffset


  subroutine addStraylight(errS, wavelInstrS, earthRadStrayLightS, earthRadianceS)

    ! depending on the flag earthRadStrayLightS%useLinearInterpolation use
    ! either linear interpolated values or a low degree polynomial, representing stray light
    ! the stray light is added to the earth radiance.
    ! the degree of the polynomial is the number of nodes - 1 that are specified in the configuration file

    implicit none

    type(errorType),           intent(inout) :: errS
    type(wavelInstrType),      intent(in)    :: wavelInstrS       
    type(straylightType),      intent(inout) :: earthRadStrayLightS  ! additive offset
    type(earthRadianceType),   intent(inout) :: earthRadianceS

    ! local
    integer                :: iwave, status
    real(8)                :: strayLightSpectrum(wavelInstrS%nwavel)

    logical, parameter     :: verbose = .false.

    ! initialize
    strayLightSpectrum = 0.0d0

    earthRadStrayLightS%strayLightNodes = earthRadStrayLightS%radiance * earthRadStrayLightS%percentStrayLight

    select case ( earthRadStrayLightS%nwavel )

      case(1) ! no wavelength dependence stray light nodes (in terms of percent radiance at one node)
        strayLightSpectrum(:) = 0.01d0 * earthRadStrayLightS%strayLightNodes(1)

      case(2:100)

        if ( earthRadStrayLightS%useLinearInterpolation ) then

          do iwave = 1, wavelInstrS%nwavel
            strayLightSpectrum(iwave) = 0.01d0 *  splintLin(errS, earthRadStrayLightS%wavel,  &
                       earthRadStrayLightS%strayLightNodes, wavelInstrS%wavel(iwave), status)
          end do

        else

          do iwave = 1, wavelInstrS%nwavel
            strayLightSpectrum(iwave) = 0.01d0 &
              * polyInt(errS, earthRadStrayLightS%wavel, earthRadStrayLightS%strayLightNodes, wavelInstrS%wavel(iwave))
          end do

        end if
      case default

        call mystop(errS, 'incorrect number for stray light nodes in radianceIrradiance : addStraylight')
        if (errorCheck(errS)) return

    end select

    if ( earthRadStrayLightS%useCharacteristicBias ) then
      strayLightSpectrum(:) = strayLightSpectrum(:) * earthRadStrayLightS%radianceCB(:)
      earthRadianceS%rad(1,:)      = earthRadianceS%rad(1,:)      + strayLightSpectrum(:)
      earthRadianceS%rad_meas(1,:) = earthRadianceS%rad_meas(1,:) + strayLightSpectrum(:)
    else  ! useCharacteristicBias
      earthRadianceS%rad(1,:)      = earthRadianceS%rad(1,:)      + strayLightSpectrum(:)
      earthRadianceS%rad_meas(1,:) = earthRadianceS%rad_meas(1,:) + strayLightSpectrum(:)
    end if ! useCharacteristicBias

    if ( verbose) then
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,'(A)') 'earth radiance without straylight    straylight    total earth radiance '
      do iwave = 1, wavelInstrS%nwavel
        write(intermediateFileUnit,'(F12.5, 3ES20.10)') wavelInstrS%wavel(iwave),                                     &
                                                        earthRadianceS%rad_meas(1,iwave) - strayLightSpectrum(iwave), &
                                                        strayLightSpectrum(iwave), earthRadianceS%rad_meas(1,iwave)
      end do
    end if

  end subroutine addStraylight


  subroutine ignorePolarizationScrambler(errS, numSpectrBands, controlSimS, wavelInstrRadSimS, earthRadianceSimS)
    ! modify simulated radiance in order to simulate the effect of not using a polarization scrambler

    type(errorType),           intent(inout) :: errS
    integer,                   intent(in)    :: numSpectrBands
    type(controlType),         intent(in)    :: controlSimS
    type(wavelInstrType),      intent(in)    :: wavelInstrRadSimS(numSpectrBands)       
    type(earthRadianceType),   intent(inout) :: earthRadianceSimS(numSpectrBands)
  
    ! local
    integer    :: iband, nwavel, iwavel
    integer    :: allocStatus, sumAllocStatus, deallocStatus, sumdeallocStatus
    real(8)    :: factor

    real(8), parameter :: PI = 3.141592653589793d0
    real(8), parameter :: radToDegree = 180.0d0 / PI
    real(8), allocatable    :: I(:), Q(:), U(:), Il(:), Ir(:), chi(:)

    logical, parameter :: verbose = .true.

    do iband = 1, numSpectrBands
      if ( earthRadianceSimS(iband)%usePolScrambler ) then
        ! do nothing
      else
        allocStatus    = 0
        sumAllocStatus = 0
        nwavel = wavelInstrRadSimS(iband)%nwavel
        allocate( I(nwavel), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( Q(nwavel), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( U(nwavel), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( Il(nwavel), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( Ir(nwavel), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( chi(nwavel), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        if ( sumAllocStatus /= 0 ) then
          call logDebug('FATAL ERROR: allocation failed')
          call logDebugI('for iband = ', iband)
          call logDebug('in subroutine ignorePolarizationScrambler')
          call logDebug('in file radianceIrradianceModule.f90')
          call mystop(errS, 'stopped because allocation failed')
          if (errorCheck(errS)) return
        end if  ! sumAllocStatus /= 0 

        ! see Hovenier, van der Mee, and Domke (2004), display 1.1
        I(:)   = earthRadianceSimS(iband)%rad_meas(1,:)
        Q(:)   = earthRadianceSimS(iband)%rad_meas(2,:)
        U(:)   = earthRadianceSimS(iband)%rad_meas(3,:)
        Il(:)  = ( I(:) + Q(:) ) / 2.0d0
        Ir(:)  = ( I(:) - Q(:) ) / 2.0d0

        do iwavel = 1, nwavel
          if ( abs( Q(iwavel) / I(iwavel) )  < 1.0d-10 ) then
            if ( U(iwavel) >= 0.0d0 ) then
              chi(iwavel) = radToDegree * PI / 4.0d0
            else
              chi(iwavel) = radToDegree * 3.0d0 * PI / 4.0d0
            end if ! U(iwavel) >= 0.0d0 
          else
            chi(iwavel) = radToDegree * atan( U(iwavel) / Q(iwavel) ) /2.0d0
          end if
        end do ! iwavel

        ! modify earth radiance
          if (verbose ) then
            write(intermediateFileUnit,*)
            write(intermediateFileUnit,'(A)') 'wavelength       m12/m11 '
          end if ! verbose
        do iwavel = 1, nwavel
          factor = polyInt(errS, earthRadianceSimS(iband)%wavelScaleFactor, earthRadianceSimS(iband)%scaleFactor, &
                           wavelInstrRadSimS(iband)%wavel(iwavel))       
          earthRadianceSimS(iband)%rad_meas(1,iwavel) = I(iwavel) + factor * Q(iwavel)
          if (verbose ) then
            write(intermediateFileUnit,'(2F10.4)')  wavelInstrRadSimS(iband)%wavel(iwavel), factor
          end if ! verbose
        end do ! iwavel

        if ( verbose ) then
          write(intermediateFileUnit,*)
          write(intermediateFileUnit,'(A)') '  wavelength        I           Q           U'
          do iwavel = 1, nwavel
            write(intermediateFileUnit,'(F10.4, 3ES15.4)')  wavelInstrRadSimS(iband)%wavel(iwavel), &
                                                     I(iwavel), Q(iwavel), U(iwavel)
          end do

          write(intermediateFileUnit,*)
          write(intermediateFileUnit,'(A)') '  wavelength        Il         Ir          chi'
          do iwavel = 1, nwavel
            write(intermediateFileUnit,'(F10.4, 3ES15.4)')  wavelInstrRadSimS(iband)%wavel(iwavel), &
                                                     Il(iwavel), Ir(iwavel), chi(iwavel)
          end do

          write(intermediateFileUnit,*)
          write(intermediateFileUnit,'(A)') '  wavelength        I         I_modified'
          do iwavel = 1, nwavel
            write(intermediateFileUnit,'(F10.4, 2ES15.4)')  wavelInstrRadSimS(iband)%wavel(iwavel), &
                                                     I(iwavel), earthRadianceSimS(iband)%rad_meas(1,iwavel)
          end do
        end if ! verbose

        deallocStatus    = 0
        sumdeallocStatus = 0
        nwavel = wavelInstrRadSimS(iband)%nwavel
        deallocate( I, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( Q, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( U, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( Il, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( Ir, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( chi, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        if ( sumdeallocStatus /= 0 ) then
          call logDebug('FATAL ERROR: deallocation failed')
          call logDebugI('for iband = ', iband)
          call logDebug('in subroutine ignorePolarizationScrambler')
          call logDebug('in file radianceIrradianceModule.f90')
          call mystop(errS, 'stopped because deallocation failed')
          if (errorCheck(errS)) return
        end if ! sumdeallocStatus /= 0 
      end if ! usePolScrambler
    end do ! iband

  end subroutine ignorePolarizationScrambler


  subroutine addRingSpec(errS, controlS, wavelInstrS, RRS_RingS, solarIrradianceS, earthRadianceS)

    ! Add a radiance Ring spectrum to the earth radiance, multiplied with the Ring coefficient.
    ! The Ring spectrum can be a differential Ring spectrum or a full Ring spectrum.
    ! At this moment we do not consider absorption in the Earth atmosphere.

    implicit none

    type(errorType),           intent(inout) :: errS
    type(controlType),         intent(in)    :: controlS
    type(wavelInstrType),      intent(in)    :: wavelInstrS       
    type(RRS_RingType),        intent(in)    :: RRS_RingS
    type(SolarIrrType),        intent(in)    :: solarIrradianceS
    type(earthRadianceType),   intent(inout) :: earthRadianceS

    ! local
    integer             :: iwave
    real(8)             :: RingSpectrum(wavelInstrS%nwavel)

    logical, parameter  :: verbose = .false.

    ! initialize
    RingSpectrum(:) = 0.0d0

    if ( controlS%ignoreSlit ) then
      if ( RRS_RingS%addRingSpec ) then
        RingSpectrum(:) =  RRS_RingS%ringCoeff * RRS_RingS%Ring(:) * solarIrradianceS%solIrrMR(:)
      end if
      if ( RRS_RingS%addDiffRingSpec ) then
        RingSpectrum(:) =  RRS_RingS%ringCoeff * RRS_RingS%diffRing(:) * solarIrradianceS%solIrrMR(:)
      end if
    else
      if ( RRS_RingS%addRingSpec ) then
        RingSpectrum(:) =  RRS_RingS%ringCoeff * RRS_RingS%Ring(:) * solarIrradianceS%solIrr(:)
      end if
      if ( RRS_RingS%addDiffRingSpec ) then
        RingSpectrum(:) =  RRS_RingS%ringCoeff * RRS_RingS%diffRing(:) * solarIrradianceS%solIrr(:)
      end if
    end if ! controlS%ignoreSlit

    ! add Ring spectrum only to the radiance not to the other Stokes parameters => use (1,:)
    earthRadianceS%rad_meas(1,:) = earthRadianceS%rad_meas(1,:) + RingSpectrum(:)

    if ( verbose) then
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,'(A)') 'earth radiance without Ring    Ring    total earth radiance '
      do iwave = 1, wavelInstrS%nwavel
        write(intermediateFileUnit,'(F12.5, 3ES20.10)') wavelInstrS%wavel(iwave),                           &
                                                        earthRadianceS%rad_meas(1,iwave) - RingSpectrum(iwave),  &
                                                        RingSpectrum(iwave), earthRadianceS%rad_meas(1,iwave)
      end do
    end if

    if ( any( earthRadianceS%rad(1,:) < 0.0d0 ) )  then
      call logDebug('ERROR: Earth radiance becomes negative at one or more wavelengths')
      call logDebug('in subroutine addRingSpec')
      call logDebug('in module radianceIrradianceModule')
      call logDebug('ring coefficient may be too large')
      call logDebug('when a differential Ring spectrum is used')
      call mystop(errS, 'stopped because Earth radiance is not positive at all wavelengths')
      if (errorCheck(errS)) return
    end if

  end subroutine addRingSpec


  subroutine setRingDerivatives(errS, controlS, nTrace, wavelInstrS, RRS_RingS, retrS, solarIrradianceS, earthRadianceS)

    ! For Ring spectra we can not use the standard approach for the derivatives
    ! because we can not calculate in a straightfoward manner a differential Ring spectrum
    ! on a high resolution grid. The Ring spectrum becomes very large near the end of the
    ! interval and subtracting a polynomial does not work for the high resolution spectrum.
    ! Therefore we use the derivatives after the convolution with the slit function.

    ! The derivative w.r.t. the Ring coefficient is stored in the earth radiance
    ! in  earthRadianceS%K_lnvmr(iwave, iState) and in  K_vmr, K_ndens, and K_ndensCol

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                   intent(in)    :: nTrace
    type(controlType),         intent(in)    :: controlS
    type(wavelInstrType),      intent(in)    :: wavelInstrS       
    type(RRS_RingType),        intent(in)    :: RRS_RingS
    type(retrType),            intent(in)    :: retrS
    type(SolarIrrType),        intent(in)    :: solarIrradianceS
    type(earthRadianceType),   intent(inout) :: earthRadianceS

    ! local
    integer             :: iwave, istate, indexCol
    real(8)             :: RingSpectrumDeriv(wavelInstrS%nwavel)

    ! initialize
    RingSpectrumDeriv(:) = 0.0d0

    if ( RRS_RingS%fitRingSpec ) then
      RingSpectrumDeriv(:) = RRS_RingS%Ring(:)
    end if
    if ( RRS_RingS%fitDiffRingSpec ) then
      RingSpectrumDeriv(:) = RRS_RingS%diffRing(:)
    end if

    ! The derivative pertains to the reflectance and has to be put in the correct place
    ! in the matrix of the derivatives, K.

    ! initialize counter for derivatives for the (sub)columns
    indexCol = nTrace * earthRadianceS%ngaussCol

    ! update the earth radiance
    if ( controlS%ignoreSlit ) then
      earthRadianceS%rad(1,:) =  earthRadianceS%rad(1,:) &
                    +  RRS_RingS%ringCoeff * RingSpectrumDeriv(:) * solarIrradianceS%solIrrMR(:)
    else
      earthRadianceS%rad(1,:) =  earthRadianceS%rad(1,:) &
                    +  RRS_RingS%ringCoeff * RingSpectrumDeriv(:) * solarIrradianceS%solIrr(:)
    end if ! controlS%ignoreSlit

    do istate = 1, earthRadianceS%nstate

      select case (retrS%codeFitParameters(istate))

        ! This case statement is needed to get the correct index for the subcolumns, indexCol
        case( 'nodeTemp', 'offsetTemp', 'surfPressure', 'aerosolTau' , 'aerosolSSA' , 'aerosolAC' ,  &
              'cloudTau', 'cloudAC'   , 'intervalDP'  , 'intervalTop', 'intervalBot', 'surfAlbedo',  &
              'cloudFraction', 'mulOffset', 'straylight', 'surfEmission' )

          indexCol = indexCol + 1

        case('diffRingCoef', 'RingCoef')

          indexCol = indexCol + 1

          if ( controlS%ignoreSlit ) then
            do iwave = 1, wavelInstrS%nwavel
              earthRadianceS%K_lnvmr(iwave,istate) = RingSpectrumDeriv(iwave) * solarIrradianceS%solIrrMR(iwave)
            end do ! iwave
          else
            do iwave = 1, wavelInstrS%nwavel
              earthRadianceS%K_lnvmr(iwave,istate) = RingSpectrumDeriv(iwave) * solarIrradianceS%solIrr(iwave)
            end do ! iwave
          end if ! controlS%ignoreSlit

          earthRadianceS%K_vmr(:,istate)          = earthRadianceS%K_lnvmr(:,istate)
          earthRadianceS%K_ndens(:,istate)        = earthRadianceS%K_lnvmr(:,istate)
          earthRadianceS%K_ndensCol(:,indexCol)   = earthRadianceS%K_lnvmr(:,istate)

      end select

    end do ! istate

  end subroutine setRingDerivatives


  subroutine fillReflectanceSim(errS, numSpectrBands, controlS, wavelInstrSimS, solarIrradianceSimS, earthRadianceSimS, retrS)

    ! fill reflectance and the noise error in the reflectance

    implicit none

    type(errorType),         intent(inout) :: errS
    integer,                 intent(in)    :: numSpectrBands
    type(controlType),       intent(in)    :: controlS
    type(wavelInstrType),    intent(in)    :: wavelInstrSimS(:)
    type(SolarIrrType),      intent(in)    :: solarIrradianceSimS(:)
    type(EarthRadianceType), intent(in)    :: earthRadianceSimS(:)
    type(retrType),          intent(inout) :: retrS

    ! local
    integer :: iband, iwave, index

    ! note: reflectance is sun-normalized radiance here

    ! fill reflectance, noise error, and signal to noise ratio for the reference spectrum
    index = 1  ! wavelength counter
    do iband = 1, numSpectrBands
      do iwave = 1, wavelInstrSimS(iband)%nwavel
        retrS%wavelRetr(index) = wavelInstrSimS(iband)%wavel(iwave)
        retrS%reflMeas(index)  = earthRadianceSimS  (iband)%rad_meas(1,iwave) &
                               / solarIrradianceSimS(iband)%solIrrMeas(iwave)
        retrS%reflNoiseError(index) = retrS%reflMeas(index) * sqrt(  &
            ( earthRadianceSimS(iband)%rad_error(iwave) / earthRadianceSimS(iband)%rad_meas(1,iwave) )**2   &
          + ( solarIrradianceSimS(iband)%solIrrError(iwave) / solarIrradianceSimS(iband)%solIrrMeas(iwave) )**2 )
        index = index + 1
      end do ! iwave
    end do ! iband

    if ( controlS%useReflectanceFromFile) then
      retrS%SNrefspec(:) = 0.0d0
    else
      index = 1  ! wavelength counter
      do iband = 1, numSpectrBands
        do iwave = 1, wavelInstrSimS(iband)%nwavel
          retrS%SNrefspec(index) = 1.0d0 / sqrt( earthRadianceSimS(iband)%SNrefspec(iwave)**(-2)   &
                                               + solarIrradianceSimS(iband)%SN(iwave)**(-2) )
          index = index + 1
        end do ! iwave
      end do ! iband
    end if

  end subroutine fillReflectanceSim


  subroutine fillReflCalibrationError(errS, numSpectrBands, wavelInstrSimS, calibErrorReflS, retrS)

    ! fill reflectance, the noise error in the reflectance, and the systematic error in the reflectance
    ! in addition fill the error covariance, Se, for the measurement and calculate the inverse, the
    ! square root and the inverse square root of Se

    implicit none

    type(errorType),         intent(inout) :: errS
    integer,                 intent(in)    :: numSpectrBands
    type(wavelInstrType),    intent(in)    :: wavelInstrSimS(:)
    type(calibrReflType),    intent(inout) :: calibErrorReflS(:)
    type(retrType),          intent(inout) :: retrS

    ! local
    integer :: iband, iwave, index, status

    ! note: reflectance is sun-normalized radiance here

    ! fill multiplicative calibration error for the reflectance
    index = 1  ! wavelength counter
    do iband = 1, numSpectrBands
      do iwave = 1, wavelInstrSimS(iband)%nwavel
        ! polynomial interpolation of the calibration error
        retrS%reflCalibErrorMul(index) = polyInt(errS, calibErrorReflS(iband)%wavel, &
                                         calibErrorReflS(iband)%errorMul, retrS%wavelRetr(index))
        ! calibErrorReflS(iband)%error is in percent; make it an absolute error
        retrS%reflCalibErrorMul(index) = retrS%reflMeas(index) * retrS%reflCalibErrorMul(index) / 100.0d0
        index = index + 1
      end do ! iwave
    end do ! iband 

    ! fill addtive calibration error for the reflectance
    index = 1  ! wavelength counter
    do iband = 1, numSpectrBands
      do iwave = 1, calibErrorReflS(iband)%nwavel
        ! one sigma additive error for the reflectance (sun-normalized radiance)
        ! is calculated by from the additive error (in percent) / 100 times the reflectance
        ! at the wavelengths where the error is specified
        ! linear interpolation (splintLin) is used to calculate the reflectance at these wavelength
        calibErrorReflS(iband)%refladd(iwave) = splintlin(errS, retrS%wavelRetr, retrS%reflMeas,    &
                                                calibErrorReflS(iband)%wavel(iwave), status)        &
                                              * calibErrorReflS(iband)%errorAdd(iwave) / 100.0d0
      end do
        
      do iwave = 1, wavelInstrSimS(iband)%nwavel
        ! polynomial interpolation of the calibration error
         retrS%reflCalibErrorAdd(index) = polyInt(errS, calibErrorReflS(iband)%wavel, &
                                          calibErrorReflS(iband)%refladd, retrS%wavelRetr(index))
        index = index + 1
      end do ! iwave
    end do ! iband 

    ! the error covariance of the measurement, Se, is calculated in subroutine calculate_Se
    ! in the optimalEstmationModule

  end subroutine fillReflCalibrationError


  subroutine fillReflectanceRetr(errS, numSpectrBands, controlS, wavelInstrRetrS, solarIrradianceRetrS, &
                                 earthRadianceRetrS, retrS)

    ! fill modeled reflectance and derivatives in the retrieval structure
    implicit none

      type(errorType), intent(inout) :: errS
    integer,                 intent(in)    :: numSpectrBands
    type(controlType),       intent(in)    :: controlS
    type(wavelInstrType),    intent(in)    :: wavelInstrRetrS(:)
    type(SolarIrrType),      intent(in)    :: solarIrradianceRetrS(:)
    type(EarthRadianceType), intent(in)    :: earthRadianceRetrS(:)
    type(retrType),          intent(inout) :: retrS

    ! local
    integer :: iband, iwave, index, istate

    ! put reflectance and derivatives into retrieval structure
    if ( controlS%ignoreSlit ) then

      ! initialize
      index = 1
      do iband = 1, numSpectrBands
        do iwave = 1, wavelInstrRetrS(iband)%nwavel
          retrS%refl_clr(index) = earthRadianceRetrS(iband)%rad_clr(1,iwave) / solarIrradianceRetrS(iband)%solIrrMR(iwave)
          retrS%refl_cld(index) = earthRadianceRetrS(iband)%rad_cld(1,iwave) / solarIrradianceRetrS(iband)%solIrrMR(iwave)
          retrS%refl    (index) = earthRadianceRetrS(iband)%rad    (1,iwave) / solarIrradianceRetrS(iband)%solIrrMR(iwave)
          do istate = 1, retrS%nstate
            retrS%K_clr_lnvmr(index,istate) = earthRadianceRetrS(iband)  %K_clr_lnvmr(iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrrMR(iwave)
            retrS%K_cld_lnvmr(index,istate) = earthRadianceRetrS(iband)  %K_cld_lnvmr(iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrrMR(iwave)
            retrS%K_lnvmr(index,istate)     = earthRadianceRetrS(iband)  %K_lnvmr(iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrrMR(iwave)
            retrS%K_vmr  (index,istate)     = earthRadianceRetrS(iband)  %K_vmr  (iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrrMR(iwave)
            retrS%K_ndens(index,istate)     = earthRadianceRetrS(iband)  %K_ndens(iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrrMR(iwave)
          end do
          retrS%K_ndensCol(index,:) =  earthRadianceRetrS(iband)%K_ndensCol(iwave,:) &
                                      / solarIrradianceRetrS(iband)%solIrrMR(iwave)
          index = index + 1
        end do ! iwave
      end do ! iband

    else

      ! initialize
      index = 1
      do iband = 1, numSpectrBands
        do iwave = 1, wavelInstrRetrS(iband)%nwavel
          retrS%refl_clr(index) = earthRadianceRetrS(iband)%rad_clr(1,iwave) / solarIrradianceRetrS(iband)%solIrr(iwave)
          retrS%refl_cld(index) = earthRadianceRetrS(iband)%rad_cld(1,iwave) / solarIrradianceRetrS(iband)%solIrr(iwave)
          retrS%refl    (index) = earthRadianceRetrS(iband)%rad    (1,iwave) / solarIrradianceRetrS(iband)%solIrr(iwave)
          do istate = 1, retrS%nstate
            retrS%K_clr_lnvmr(index,istate) = earthRadianceRetrS(iband)  %K_clr_lnvmr(iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrr(iwave)
            retrS%K_cld_lnvmr(index,istate) = earthRadianceRetrS(iband)  %K_cld_lnvmr(iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrr(iwave)
            retrS%K_lnvmr(index,istate)     = earthRadianceRetrS(iband)  %K_lnvmr(iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrr(iwave)
            retrS%K_vmr  (index,istate)     = earthRadianceRetrS(iband)  %K_vmr  (iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrr(iwave)
            retrS%K_ndens(index,istate)     = earthRadianceRetrS(iband)  %K_ndens(iwave,istate) &
                                            / solarIrradianceRetrS(iband)%solIrr(iwave)
          end do
          retrS%K_ndensCol(index,:) =  earthRadianceRetrS(iband)%K_ndensCol(iwave,:) &
                                      / solarIrradianceRetrS(iband)%solIrr(iwave)
          index = index + 1
        end do ! iwave
      end do ! iband

    end if ! controlS%ignoreSlit

  end subroutine fillReflectanceRetr

end module radianceIrradianceModule
