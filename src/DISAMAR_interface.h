
#ifndef DISAMAR_INTERFACE_H
#define DISAMAR_INTERFACE_H

extern int c_disamar_set_log_level(int);
extern int c_disamar_set_callback_logger(void(*)(char*, int));
extern int c_disamar_allocate_static_workspace();
extern int c_disamar_set_static_input_c(char*, int, char*, int);
extern int c_disamar_allocate_dynamic_workspace(void**);
extern int c_disamar_set_input_d(char*, int, int, int*, double*, void**);
extern int c_disamar_set_input_i(char*, int, int, int*, int*, void**);
extern int c_disamar_retrieval(void**);
extern int c_disamar_get_output_c(char*, int, char*, int*, void**);
extern int c_disamar_get_output_d(char*, int, int, int*, double*, void**);
extern int c_disamar_get_output_i(char*, int, int, int*, int*, void**);
extern int c_disamar_deallocate_dynamic_workspace(void**);
extern int c_disamar_deallocate_static_workspace();

#define LOG_TRACE   0
#define LOG_DEBUG   1
#define LOG_INFO    2
#define LOG_WARNING 3
#define LOG_ERROR   4

#define STATUS_S_SUCCESS                          0  /* No error. */
#define STATUS_E_ASSERTION_ERROR                  1  /* Assertion failed. */
#define STATUS_E_GENERIC_EXCEPTION                2  /* Catch all error. */
#define STATUS_E_ABORT_ERROR                      3  /* Processor has been aborted. */
#define STATUS_E_CONFIGURATION_ERROR              4  /* Error in the configuration of the processor. */
#define STATUS_E_KEY_ERROR                        5  /* Unrecognised key in the key-value interface. */
#define STATUS_E_BUFFER_SIZE_ERROR                6  /* Provided buffer is too small (retry with increased buffer). */
#define STATUS_E_DIMENSIONS_ERROR                 7  /* The dimension sizes are not as expected. */
#define STATUS_E_MEMORY_ALLOCATION_ERROR          8  /* Memory allocation failed. */
#define STATUS_E_CONSISTENCY_ERROR                9  /* Provided input is not internally consistent. */
#define STATUS_E_RANK_ERROR                       10 /* The rank of the provided array is incorrect. */
#define STATUS_E_MEMORY_DEALLOCATION_ERROR        11 /* Error while releasing memory. */
#define STATUS_E_EXPECTED_SCALAR                  12 /* Expected a single value (rank 1, size 1). */
#define STATUS_E_INCOMPLETE_INPUT_DATA            13 /* Not all input data has been transferred */
#define STATUS_E_READING_UNAVAILABLE_DATA         14 /* The data you are trying to read has not been set (yet). */
#define STATUS_E_DATA_SEQUENCE_ERROR              15 /* The order in which parameters are passed in is incorrect. */
#define STATUS_E_VALUE_ERROR                      16 /* The value passed to the interface is outside the valid range for this parameter */
#endif
