!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module errorHandlingModule

  ! This module contains several subroutines that are used for error handling
  ! in DISAMAR.

  use DISAMAR_log

  type errorType
    logical                          :: interactive
    integer                          :: code
    integer                          :: nLines
    character(LEN=132)               :: shortMessage
    character(LEN=512),dimension(50) :: longMessage
    character(LEN=512)               :: temp
  end type errorType

  contains


  subroutine errorInit(errS)

    implicit none
    type(errorType),intent(inout) :: errS
    errS%interactive = .true.
    errS%code = 0
    errS%nLines = 0

  end subroutine errorInit


  subroutine errorSetInteractive(errS, flag)

    implicit none
    type(errorType),intent(inout) :: errS
    logical :: flag
    errS%interactive = flag

  end subroutine errorSetInteractive
  

  subroutine errorAddLine(errS, msg)

    implicit none
    type(errorType),intent(inout) :: errS
    character(LEN=*),intent(in) :: msg

    if (errS%nLines < 50) then
        errS%nLines = errS%nLines + 1
        errS%longMessage(errS%nLines) = msg
    end if
    call logDebug(msg)

  end subroutine errorAddLine


  subroutine errorSet(errS, msg)

    implicit none
    type(errorType),intent(inout) :: errS
    character(LEN=*),intent(in) :: msg

    errS%code = 1
    errS%shortMessage = msg
    call disamar_logger(trim(adjustl(msg)), LOG_ERROR) 

  end subroutine errorSet


  subroutine logInfo(str)

    implicit none
    character(LEN=*),intent(in) :: str

    call disamar_logger(trim(str), LOG_INFO)

  end subroutine logInfo

  subroutine logMessage(str, level)

    implicit none
    character(LEN=*),intent(in) :: str
	integer, intent(in)         :: level

    call disamar_logger(trim(str), level)

  end subroutine logMessage


  subroutine logDebug(str)

    implicit none
    character(LEN=*),intent(in) :: str

    call disamar_logger(trim(str), LOG_DEBUG)

  end subroutine logDebug
 
 
  subroutine logDebugA(str)

    ! Sometimes (e.g. from S5PInterfaceModule the compiler flags logDebug as ambiguous. 
	! Calling logDebugA might help
    implicit none
    character(LEN=*),intent(in) :: str

    call disamar_logger(trim(str), LOG_DEBUG)

  end subroutine logDebugA


  subroutine logDebugI(str, i)

    implicit none
    character(LEN=*),intent(in) :: str
    integer,intent(in) :: i
    character*(512) str2

    write(str2, '(A,I12)') str, i
    call disamar_logger(trim(str2), LOG_DEBUG)

  end subroutine logDebugI


  subroutine logDebugF(str, f)

    implicit none
    character(LEN=*),intent(in) :: str
    real(4),intent(in) :: f 
    character*(512) str2

    write(str2, '(A,F12.5)') str, f
    call disamar_logger(trim(str2), LOG_DEBUG)

  end subroutine logDebugF


  subroutine logDebugD(str, f)

    implicit none
    character(LEN=*),intent(in) :: str
    real(8),intent(in) :: f 
    character*(512) str2

    write(str2, '(A,F12.5)') str, f
    call disamar_logger(trim(str2), LOG_DEBUG)

  end subroutine logDebugD


  subroutine errorPrint(str)

    implicit none
    character(LEN=*),intent(in) :: str

    call disamar_logger(trim(str), LOG_ERROR)

  end subroutine errorPrint


  subroutine errorPrintF1(str, f)

    implicit none
    character(LEN=*),intent(in) :: str
    real(8) f
    character*(512) str2

    write(str2, '(A,F12.5)') str, f
    call disamar_logger(trim(str2), LOG_ERROR)

  end subroutine errorPrintF1


  subroutine errorPrintI1(str, i)

    implicit none
    character(LEN=*),intent(in) :: str
    integer i
    character*(512) str2

    write(str2, '(A,I12)') str, i
    call disamar_logger(trim(str2), LOG_ERROR)

  end subroutine errorPrintI1


  subroutine errorWrite(errS)

    implicit none
    type(errorType),intent(in) :: errS
    integer i

    if (.not. errS%interactive) then
      if (errS%code /= 0) then
        do i = 1, errS%nLines
          call fortranlog(trim(errS%longMessage(i)), len(trim(errS%longMessage(i))), 2)
        end do
        write(*,*) trim(errS%shortMessage)
      end if
    end if

  end subroutine errorWrite


  function errorCheckUnit(errS, fileUnit)

    implicit none
    type(errorType),intent(in) :: errS
	integer, intent(in) :: fileUnit
    logical errorCheckUnit

    errorCheckUnit = errorCheck(errS)
	if (errorCheckUnit) then
       close(unit = fileUnit)
	endif

  end function errorCheckUnit

  
  pure function errorCheck(errS)

    implicit none
    type(errorType),intent(in) :: errS
    logical errorCheck

    errorCheck = (errS%code /= 0)

  end function errorCheck


  subroutine mystop(errS, msg)

    implicit none
    type(errorType) :: errS
    character(LEN=*),intent(in) :: msg

    call errorSet(errS, msg);
    if (errorCheck(errS)) return
    ! CALL fortranstop(msg)
    ! stop

  end subroutine mystop

end module errorHandlingModule
