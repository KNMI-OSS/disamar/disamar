!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module DISAMARModule

  use DISAMAR_log
  use dataStructures
  use staticDataModule
  use mathTools,                only: fleg, svdfit, splintLin, svdcmp, svbksb, gaussDivPoints,      &
                                      spline, splint, polyInt, interpol6D
  use propAtmosphereModule,     only: getAbsorptionXsec, getAbsorptionXsecUsingLUT, createXsecLUT,  &
                                      getOptPropAtmSim
  use radianceIrradianceModule, only: calcReflAndDeriv,                                             &
                                      integrateSlitFunctionIrr,  integrateSlitFunctionRad,          &
                                      specifyNoise, addSpecFeaturesIrr, addSpecFeaturesRad,         &
                                      addSimpleOffsets, addSmear, addStraylight, fillReflectanceSim,&
                                      fillReflectanceRetr, fillReferenceSpectrum, addRingSpec,      &
                                      fillRadianceAtMulOffsetNodes, fillRadianceAtStrayLightNodes,  &
                                      fillRadianceDerivativesHRgrid, calculateRTM_Ring_spectra,     &
                                      fillReflCalibrationError, setRingDerivatives, addMulOffset,   &
                                      ignorePolarizationScrambler
  use optimalEstmationModule,   only: fillCodesFitParameters, updateFitParameters, fillAPriori,     &
                                      calculateNewState, updateCovFitParameters, calculate_Se,      &
                                      calculateDiagnostics, evaluateSpectralFeatures
  use subcolumnModule,          only: fillAltitudeGridCol, calculateColumnPropertiesSim,            &
                                      calculateColumnPropertiesRetr
  use readConfigFileModule,     only: readConfigFile, setFixedValues,                               &
                                      deallocateStructuresSim, deallocateStructuresRetr
  use verifyConfigFileModule,   only: verifyConfigFile
  use readModule,               only: getHRSolarIrradiance, readPolarizationCorrectionFile,         &
                                      getNextNonCommentedLine, getHRSolarIrradianceStatic,          &
                                      getCharatisticBiasfromFile
  use readIrrRadFromFileModule, only: readIrrRadFromFile, setMRWavelengthGrid
  use S5POperationalModule,     only: readIrrRadFromMemory, replaceXSecLUTData, replaceHRWavelengthData
  use writeModule,              only: print_results, print_asciiHDF, writeAsCIIinputOMO3PR, write_disamar_sim
  use doasModule,               only: performDOASfit, fillCodesFitParamDOAS
  use classic_doasModule,       only: classicDOASfit, fillCodesFitParamClDOAS
  use dismasModule,             only: calcReflAndDerivDismas
  use HITRANModule,             only: getLinePositions
  use ramansspecs,              only: NumberRamanLines, RamanLinesScatWavel

  implicit none

  contains


  subroutine init(errS, globalS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(globalType), pointer :: globalS

    ! time information
    real(8)              :: prev_time

    ! local
    integer              :: current_time_values(8)
    character(LEN =250)  :: outputFilename, intermediateFilename, errorCovFilename, asciiHDFFileName
    character(LEN =250)  :: configurationFileName

    call enter('init')

    ! unit numbers for the files are specified in dataStructures.f90 (at top)

    configurationFileName = 'Config.in'
    outputFilename        = 'disamar.out'
    intermediateFilename  = 'disamar.imed'
    errorCovFilename      = 'profile_errorCovariance.out'
    asciiHDFFileName      = 'disamar.asciiHDF'

    if (staticS%operational == 0) then
        open(UNIT=outputFileUnit,       ACTION= 'WRITE', STATUS = 'REPLACE', FILE = outputFilename      )
        open(UNIT=intermediateFileUnit, ACTION= 'WRITE', STATUS = 'REPLACE', FILE = intermediateFilename)
        open(UNIT=errorCovOutUnit,      ACTION= 'WRITE', STATUS = 'REPLACE', FILE = errorCovFilename    )
        open(UNIT=asciiHDFoutputUnit,   ACTION= 'WRITE', STATUS = 'REPLACE', FILE = asciiHDFFileName    )
    end if
    if (staticS%operational == 1) then
        configurationFileName = staticS%configFileName
        if (staticS%writeResults == 1) then
            call logDebug('Opening DISAMAR output files')
            open(UNIT=outputFileUnit,       ACTION= 'WRITE', STATUS = 'UNKNOWN', POSITION='APPEND', FILE = outputFilename      )
            open(UNIT=intermediateFileUnit, ACTION= 'WRITE', STATUS = 'UNKNOWN', POSITION='APPEND', FILE = intermediateFilename)
            open(UNIT=errorCovOutUnit,      ACTION= 'WRITE', STATUS = 'UNKNOWN', POSITION='APPEND', FILE = errorCovFilename    )
            open(UNIT=asciiHDFoutputUnit,   ACTION= 'WRITE', STATUS = 'UNKNOWN', POSITION='APPEND', FILE = asciiHDFFileName    )
        endif
    end if

    ! initialize timing information
    prev_time = current_time(current_time_values)

    ! Read the configuration file and
    ! initialize values.

    call readConfigFile(errS, globalS%configFileS,                     &
         configurationFilename, globalS%inputS, globalS,               &
         globalS%external_dataS, globalS%maxFourierTermLUT,            &
         globalS%numSpectrBands, globalS%nTrace, globalS%ncolumn,      &
         globalS%wavelHRSimS, globalS%wavelHRRetrS,                    &
         globalS%wavelMRIrrSimS, globalS%wavelMRIrrRetrS,              &
         globalS%wavelMRRadSimS, globalS%wavelMRRadRetrS,              &
         globalS%wavelInstrIrrSimS, globalS%wavelInstrIrrRetrS,        &
         globalS%wavelInstrRadSimS, globalS%wavelInstrRadRetrS,        &
         globalS%absLinesSimS, globalS%absLinesRetrS,                  &
         globalS%XsecHRSimS, globalS%XsecHRRetrS,                      &
         globalS%XsecHRLUTSimS, globalS%XsecHRLUTRetrS,                &
         globalS%cloudAerosolRTMgridSimS,                              &
         globalS%cloudAerosolRTMgridRetrS,                             &
         globalS%geometrySimS, globalS%geometryRetrS,                  &
         globalS%gasPTSimS, globalS%gasPTRetrS,                        &
         globalS%traceGasSimS, globalS%traceGasRetrS,                  &
         globalS%mieAerSimS, globalS%mieAerRetrS,                      &
         globalS%mieCldSimS, globalS%mieCldRetrS,                      &
         globalS%surfaceSimS, globalS%surfaceRetrS,                    &
         globalS%LambCloudSimS, globalS%LambCloudRetrS,                &
         globalS%LambAerSimS, globalS%LambAerRetrS,                    &
         globalS%cldAerFractionSimS, globalS%cldAerFractionRetrS,      &
         globalS%polCorrectionRetrS,                                   &
         globalS%createLUTSimS, globalS%createLUTRetrS,                &
         globalS%optPropRTMGridSimS, globalS%optPropRTMGridRetrS,      &
         globalS%controlSimS, globalS%controlRetrS,                    &
         globalS%O3ClimSimS, globalS%O3ClimRetrS,                      &
         globalS%reflDerivHRSimS, globalS%reflDerivHRRetrS,            &
         globalS%solarIrradianceSimS, globalS%solarIrradianceRetrS,    &
         globalS%earthRadianceSimS, globalS%earthRadianceRetrS,        &
         globalS%earthRadMulOffsetSimS,                                &
         globalS%earthRadMulOffsetRetrS,                               &
         globalS%earthRadStrayLightSimS,                               &
         globalS%earthRadStrayLightRetrS,                              &
         globalS%calibErrorReflS, globalS%retrS, globalS%diagnosticS,  &
         globalS%columnSimS, globalS%columnRetrS,                      &
         globalS%weakAbsSimS, globalS%weakAbsRetrS,                    &
         globalS%RRS_RingSimS, globalS%RRS_RingRetrS,                  &
         globalS%dn_dnodeSimS, globalS%dn_dnodeRetrS)
    if (errorCheck(errS)) goto 99999

99999 continue
    call exit('init')

  end subroutine init


  subroutine retrieve(errS, globalS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(globalType), pointer :: globalS

    real(8)              :: time, prev_time
    integer              :: allocStatus
    integer              :: current_time_values(8)
    integer              :: iband, iTrace, i_column, iFourier

    character(LEN =250)  :: reflectanceFileName

    call enter('retrieve')

    reflectanceFileName   = 'disamar.sim'

    call verifyConfigFile(errS, staticS%operational,         &
         globalS%maxFourierTermLUT,                                 &
         globalS%numSpectrBands, globalS%nTrace, globalS%ncolumn,   &
         globalS%wavelInstrRadSimS, globalS%wavelInstrRadRetrS,     &
         globalS%cloudAerosolRTMgridSimS,                           &
         globalS%cloudAerosolRTMgridRetrS,                          &
         globalS%createLUTSimS, globalS%createLUTRetrS,             &
         globalS%XsecHRLUTSimS, globalS%XsecHRLUTRetrS,             &
         globalS%gasPTSimS, globalS%gasPTRetrS,                     &
         globalS%geometrySimS, globalS%geometryRetrS,               &
         globalS%traceGasSimS, globalS%traceGasRetrS,               &
         globalS%mieAerSimS, globalS%mieAerRetrS,                   &
         globalS%mieCldSimS, globalS%mieCldRetrS,                   &
         globalS%surfaceSimS, globalS%surfaceRetrS,                 &
         globalS%LambCloudRetrS, globalS%LambAerRetrS,              &
         globalS%controlSimS, globalS%controlRetrS,                 &
         globalS%RRS_RingSimS, globalS%RRS_RingRetrS,               &
         globalS%calibErrorReflS, globalS%polCorrectionRetrS,       &
         globalS%columnSimS, globalS%columnRetrS,                   &
         globalS%weakAbsRetrS, globalS%retrS,                       &
         globalS%earthRadianceSimS)
    if (errorCheck(errS)) return

    call setFixedValues (                                                                                      &
         globalS%numSpectrBands, globalS%controlSimS, globalS%controlRetrS,  globalS%cloudAerosolRTMgridSimS,  &
         globalS%cloudAerosolRTMgridRetrS, globalS%surfaceSimS, globalS%LambCloudSimS, globalS%LambAerSimS,    &
         globalS%solarIrradianceRetrS, globalS%earthRadianceRetrS,  globalS%earthRadMulOffsetSimS,             &
         globalS%earthRadMulOffsetRetrS, globalS%earthRadStrayLightSimS, globalS%earthRadStrayLightRetrS)
    if (errorCheck(errS)) return

    allocStatus = 0
    allocate( globalS%nwavelRTM(globalS%numSpectrBands), STAT = allocStatus )
    if ( allocStatus /= 0 ) then
      call logDebug('FATAL ERROR: Allocation failed')
      call logDebug('for globalS%nwavelRTM in main program')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if
    allocStatus = 0
    allocate( globalS%use_abs_opt_thickn_for_AMF(globalS%numSpectrBands), STAT = allocStatus )
    if ( allocStatus /= 0 ) then
      call logDebug('FATAL ERROR: Allocation failed')
      call logDebug('for globalS%use_abs_opt_thickn_for_AMF in main program')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if

    if (staticS%operational /= 1) then
        open(UNIT= addtionalOutputUnit, ACTION= 'WRITE', STATUS = 'REPLACE', &
            FILE = globalS%controlSimS%additionalOutputFileName)
    end if

    ! Determine the number of paramters that can in principle be fitted. Only a subset
    ! of these parametes will be fitted, depending on the flags that are set in the configuration file.

    globalS%totalNodes     = sum(globalS%traceGasRetrS(:)%nalt) + globalS%nTrace
    globalS%totalNodesTemp = globalS%gasPTRetrS%npressureNodes + 1
    globalS%retrS%maxfitparameters = globalS%totalNodes                        &  ! number of nodes used for profile retrieval
                    + globalS%nTrace                                           &  ! fit total column of trace gas
                    + globalS%totalNodesTemp                                   &  ! fit temperature profile
                    + 1                                                        &  ! fit temperature offset
                    + 1                                                        &  ! fit surface pressure
                    + sum(globalS%surfaceRetrS(:)%nwavelAlbedo)                &  ! fit surface albedo (wavelength dependent)
                    + sum(globalS%surfaceRetrS(:)%nwavelEmission)              &  ! fit surface emission (wavelength dependent)
                    + sum(globalS%LambCloudRetrS(:)%nwavelAlbedo)              &  ! fit Lambertian cloud albedo (wavel dependent)
                    + sum(globalS%LambAerRetrS(:)%nwavelAlbedo)                &  ! fit Lambertian aerosol albedo (wavel dependent)
                    + sum(globalS%cldAerFractionRetrS(:)%nwavelCldAerFraction) &  ! fit cloud/aerosol fraction (wavel dependent)
                    + sum(globalS%earthRadMulOffsetRetrS(:)%nwavel)            &  ! fit multiplicative offset earth radiance
                    + sum(globalS%earthRadStrayLightRetrS(:)%nwavel)           &  ! fit straylight
                    + globalS%numSpectrBands                                   &  ! fit Ring spectrum for each spectral band
                    + 1                                                &  ! fit cloud/aerosol fraction (wavelength indep)
                    + 1                                                &  ! fit aerosol optical thickness
                    + 1                                                &  ! fit aerosol single scattering albedo
                    + 1                                                &  ! fit aerosol angstrom coefficient
                    + 1                                                &  ! fit cloud optical thickness
                    + 1                                                &  ! fit cloud angstrom coefficient
                    + 1                                                &  ! fit altitude of layer containing cloud/aerosol
                                                                          ! keeping the geometrical thickness fixed
                    + 1                                                &  ! fit top of interval containing cloud or aerosol
                    + 1                                                   ! fit bottom of interval containing cloud or aerosol


    call set_ninterval(errS, globalS)  ! atmosphere is divided in ninterval ranges to define cloud and aerosol
    call set_nalt(errS, globalS)       ! set the number of altitudes where each trace gas is spcified
    call set_RTMnlayer(errS, globalS)  ! set the number of layers used in the radiative transfer calculations
    call set_dimSV(errS, globalS)      ! set the dimension of the Stokes vector

    ! fill globalS%retrS%nstate so that we know how long the state vector is
    ! the length of the state vector determines how much memory is to be claimed
    select case ( globalS%controlRetrS%method )
      case (0, 1)  ! Optimal estimation lbl or DISMAS
        call fillCodesFitParameters(errS, globalS%numSpectrBands, globalS%nTrace, globalS%controlRetrS, globalS%traceGasRetrS,  &
                                    globalS%surfaceRetrS, globalS%LambCloudRetrS,  globalS%cldAerFractionRetrS,           &
                                    globalS%gasPTRetrS, globalS%earthRadMulOffsetRetrS,  globalS%earthRadStrayLightRetrS, &
                                    globalS%RRS_RingRetrS, globalS%cloudAerosolRTMgridRetrS, globalS%retrS)
        if (errorCheck(errS)) return
      case (2)     ! DOAS with wavelength dependent air mass factor
        call fillCodesFitParamDOAS(errS, globalS%numSpectrBands, globalS%nTrace, globalS%traceGasRetrS, globalS%weakAbsRetrS, &
                                   globalS%RRS_RingRetrS, globalS%gasPTRetrS, globalS%retrS)
        if (errorCheck(errS)) return
      case (3, 4)     ! classical DOAS with air mass factor calculated for one wavelength
        call fillCodesFitParamClDOAS(errS, globalS%numSpectrBands, globalS%nTrace, globalS%traceGasRetrS, globalS%weakAbsRetrS, &
                                     globalS%RRS_RingRetrS, globalS%gasPTRetrS, globalS%retrS)
        if (errorCheck(errS)) return
      case default
        write(errS%temp,'(A)')    'ERROR encountered: incorrect value of globalS%controlRetrS%method'
        call errorAddLine(errS, errS%temp)
        write(errS%temp,'(A,I4)') 'globalS%controlRetrS%method = ', globalS%controlRetrS%method
        call errorAddLine(errS, errS%temp)
        write(errS%temp,'(A)')    'allowed values are 0, 1, 2, 3, or 4'
        call errorAddLine(errS, errS%temp)
        write(errS%temp,'(A)')    'error occured in program main'
        call errorAddLine(errS, errS%temp)
        write(errS%temp,'(A)')    'in  select case ( globalS%controlRetrS%method ) '
        call errorAddLine(errS, errS%temp)
        call mystop(errS, 'stopped because of incorrect value of globalS%controlRetrS%method')
        if (errorCheck(errS)) return

    end select

    ! note that set_nalt has to be called before set_nstate
    call set_nstate(errS, globalS)
    if (errorCheck(errS)) return

    ! set up wavelength grids for simulation and retrieval
    ! based on the configuration file
    ! if radiance and irradiance are read from file the values
    !  for the instrument wavelength grids are overwritten
    do iband = 1, globalS%numSpectrBands

      call setupInstrWavelengthGrid(errS,  globalS%wavelInstrIrrSimS (iband) )
      if (errorCheck(errS)) return
      call setupInstrWavelengthGrid(errS,  globalS%wavelInstrIrrRetrS(iband) )
      if (errorCheck(errS)) return
      call setupInstrWavelengthGrid(errS,  globalS%wavelInstrRadSimS (iband) )
      if (errorCheck(errS)) return
      call setupInstrWavelengthGrid(errS,  globalS%wavelInstrRadRetrS(iband) )
      if (errorCheck(errS)) return

      if (staticS%operational == 1) then
        call replaceHRWavelengthData(errS, globalS%inputS, globalS)
        if (errorCheck(errS)) return
      end if

      call setupHRWavelengthGrid(errS, globalS%controlSimS, globalS%nTrace, globalS%wavelInstrRadSimS(iband),   &
                                 globalS%wavelHRSimS(iband), globalS%XsecHRSimS(iband,1:globalS%nTrace),        &
                                 globalS%absLinesSimS(iband,1:globalS%nTrace), globalS%RRS_RingSimS(iband),     &
                                 globalS%solarIrradianceSimS(iband)%slitFunctionSpecsS%FWHM)
      if (errorCheck(errS)) return

      call setupHRWavelengthGrid(errS, globalS%controlRetrS, globalS%nTrace, globalS%wavelInstrRadRetrS(iband), &
                                 globalS%wavelHRRetrS(iband), globalS%XsecHRRetrS(iband,1:globalS%nTrace),      &
                                 globalS%absLinesRetrS(iband,1:globalS%nTrace), globalS%RRS_RingRetrS(iband),   &
                                 globalS%solarIrradianceRetrS(iband)%slitFunctionSpecsS%FWHM)
      if (errorCheck(errS)) return

      ! fill MR wavelength grid
      call setMRWavelengthGrid(errS, globalS%controlSimS, globalS%wavelInstrIrrSimS(iband), &
                               globalS%wavelHRSimS(iband), &
                               globalS%wavelMRIrrSimS(iband))
      if (errorCheck(errS)) return
      call setMRWavelengthGrid(errS, globalS%controlSimS, globalS%wavelInstrRadSimS(iband), &
                               globalS%wavelHRSimS(iband), &
                               globalS%wavelMRRadSimS(iband))
      if (errorCheck(errS)) return
      call setMRWavelengthGrid(errS, globalS%controlRetrS, globalS%wavelInstrIrrRetrS(iband), globalS%wavelHRRetrS(iband), &
           globalS%wavelMRIrrRetrS(iband))
      if (errorCheck(errS)) return
      call setMRWavelengthGrid(errS, globalS%controlRetrS, globalS%wavelInstrRadRetrS(iband), globalS%wavelHRRetrS(iband), &
           globalS%wavelMRRadRetrS(iband))
      if (errorCheck(errS)) return
    end do ! iband

    call set_nwavelInstr(errS, globalS)
    if (errorCheck(errS)) return
    call set_nwavelHR(errS, globalS)
    if (errorCheck(errS)) return
    call set_nwavelMR(errS, globalS)
    if (errorCheck(errS)) return

    ! fill parameters in the createLUTS; nwavel is filled in subroutine set_nwavelInstr
    do iband = 1, globalS%numSpectrBands
      do iFourier = 0, globalS%maxFourierTermLUT
        globalS%createLUTSimS (iFourier, iband)%nmu         = globalS%geometrySimS%nmutot
        globalS%createLUTSimS (iFourier, iband)%npressure   = globalS%cloudAerosolRTMgridSimS%ninterval
        globalS%createLUTRetrS(iFourier, iband)%nmu         = globalS%geometryRetrS%nmutot
        globalS%createLUTRetrS(iFourier, iband)%npressure   = globalS%cloudAerosolRTMgridRetrS%ninterval
      end do
    end do

    do i_column = 1, globalS%ncolumn
      allocate( globalS%columnRetrS(i_column)%Gain(globalS%columnRetrS(i_column)%nsubColumn, &
                globalS%columnRetrS(i_column)%nwavel, globalS%columnRetrS(i_column)%nTrace))
    end do

    call claimMemRetrS_not_Se(errS, globalS%retrS)
    if (errorCheck(errS)) return

    ! we now claim memory for the other structures
    call claimRemainingMemory(errS, globalS)
    if (errorCheck(errS)) return

    ! fill wavelength grid, pressure, and polar angles for LUT
    call fillWavelPressureGeometryLUT(errS, globalS)
    if (errorCheck(errS)) return

    if ( globalS%controlSimS%useReflectanceFromFile ) then

      prev_time = current_time(current_time_values)

      if (staticS%operational /= 1) then
        ! read irradiance and radiance and the corresponding precision from file
        call readIrrRadFromFile(errS, globalS, globalS%numSpectrBands, globalS%controlSimS,                   &
                                globalS%wavelInstrIrrSimS, globalS%wavelHRSimS, globalS%wavelMRIrrSimS,       &
                                globalS%wavelInstrRadSimS,  globalS%wavelMRRadSimS,                           &
                                globalS%solarIrradianceSimS, globalS%earthRadianceSimS,                       &
                                globalS%wavelInstrIrrRetrS, globalS%wavelHRRetrS, globalS%wavelInstrRadRetrS, &
                                globalS%solarIrradianceRetrS, globalS%earthRadianceRetrS )
        if (errorCheck(errS)) return
      else
        ! read irradiance and radiance and the corresponding precision from memory
        call readIrrRadFromMemory(errS, globalS, globalS%numSpectrBands, globalS%controlSimS,                 &
                                globalS%wavelInstrIrrSimS, globalS%wavelHRSimS, globalS%wavelMRIrrSimS,       &
                                globalS%wavelInstrRadSimS,  globalS%wavelMRRadSimS,                           &
                                globalS%solarIrradianceSimS, globalS%earthRadianceSimS,                       &
                                globalS%wavelInstrIrrRetrS, globalS%wavelHRRetrS, globalS%wavelInstrRadRetrS, &
                                globalS%solarIrradianceRetrS, globalS%earthRadianceRetrS )
        if (errorCheck(errS)) return
      endif

      ! fill MR wavelength grid - has to be repeated because wavelength grid read may not
      ! correspond to the grid specified in the configuration file

      do iband = 1, globalS%numSpectrBands
        call setMRWavelengthGrid(errS, globalS%controlSimS, globalS%wavelInstrIrrSimS(iband), globalS%wavelHRSimS(iband), &
             globalS%wavelMRIrrSimS(iband))
        if (errorCheck(errS)) return
        call setMRWavelengthGrid(errS, globalS%controlSimS, globalS%wavelInstrRadSimS(iband), globalS%wavelHRSimS(iband), &
             globalS%wavelMRRadSimS(iband))
        if (errorCheck(errS)) return
        call setMRWavelengthGrid(errS, globalS%controlRetrS, globalS%wavelInstrIrrRetrS(iband), globalS%wavelHRRetrS(iband), &
             globalS%wavelMRIrrRetrS(iband))
        if (errorCheck(errS)) return
        call setMRWavelengthGrid(errS, globalS%controlRetrS, globalS%wavelInstrRadRetrS(iband), globalS%wavelHRRetrS(iband), &
             globalS%wavelMRRadRetrS(iband))
        if (errorCheck(errS)) return
      end do ! iband

      call set_nwavelInstr(errS, globalS)
      if (errorCheck(errS)) return
      call set_nwavelHR(errS, globalS)
      if (errorCheck(errS)) return
      call set_nwavelMR(errS, globalS)
      if (errorCheck(errS)) return

      do iband = 1, globalS%numSpectrBands
        call freeMemSolarIrradianceS  (errS,  globalS%solarIrradianceRetrS(iband) )
        if (errorCheck(errS)) return
        call claimMemSolarIrradianceS (errS,  globalS%solarIrradianceRetrS(iband) )
        if (errorCheck(errS)) return
        call freeMemEarthRadianceS  (errS,  globalS%earthRadianceRetrS(iband) )
        if (errorCheck(errS)) return
        call claimMemEarthRadianceS (errS,  globalS%earthRadianceRetrS(iband) )
        if (errorCheck(errS)) return
        call freeMemRRS_RingS   (errS,  globalS%RRS_RingRetrS(iband) )
        if (errorCheck(errS)) return
        call claimMemRRS_RingS  (errS,  globalS%RRS_RingRetrS(iband) )
        if (errorCheck(errS)) return
        call freeMemReflDerivS  (errS,  globalS%reflDerivHRRetrS(iband) )
        if (errorCheck(errS)) return
        call claimMemReflDerivS (errS,  globalS%reflDerivHRRetrS(iband) )
        if (errorCheck(errS)) return
        do iTrace = 1, globalS%nTrace
          call freeMemXsecS  (errS,  globalS%XsecHRRetrS(iband, iTrace) )
          if (errorCheck(errS)) return
          call freeMemXsecLUTS (errS,  globalS%XsecHRLUTRetrS(iband, iTrace) )
          if (errorCheck(errS)) return
          call claimMemXsecS (errS,  globalS%XsecHRRetrS(iband, iTrace) )
          if (errorCheck(errS)) return
          call claimMemXsecLUTS (errS,  globalS%XsecHRLUTRetrS(iband, iTrace) )
          if (errorCheck(errS)) return
        end do
      end do ! iband

      call fillReflectanceSim(errS, globalS%numSpectrBands, globalS%controlSimS, globalS%wavelInstrRadSimS, &
                              globalS%solarIrradianceSimS, globalS%earthRadianceSimS, globalS%retrS)
      if (errorCheck(errS)) return

      ! fill calibration errors ( must come after the measured reflectance is filled in globalS%retrS )
      call fillReflCalibrationError(errS, globalS%numSpectrBands, globalS%wavelInstrRadSimS, globalS%calibErrorReflS, globalS%retrS)
      if (errorCheck(errS)) return

      do iband = 1, globalS%numSpectrBands
        if ( globalS%earthRadStrayLightSimS(iband)%useCharacteristicBias ) then
          call getCharatisticBiasfromFile (errS, globalS%wavelInstrRadSimS(iband), &
               globalS%earthRadStrayLightSimS(iband), globalS%earthRadianceSimS(iband))
          if (errorCheck(errS)) return
        end if
        if ( globalS%earthRadStrayLightRetrS(iband)%useCharacteristicBias ) then
          call getCharatisticBiasfromFile (errS, globalS%wavelInstrRadRetrS(iband), &
               globalS%earthRadStrayLightRetrS(iband), globalS%earthRadianceRetrS(iband))
          if (errorCheck(errS)) return
        end if
      end do ! iband

      if (staticS%operational /= 1) then
        ! add straylight and offsets to the measured spectrum
        call offsetsStraylight(errS, globalS)
        if (errorCheck(errS)) return
      end if

      ! although no simulation is calculated, some arrays need to be allocated if a disamar.sim file
      ! is used as simukated reflectance. Otherwise the simulated results for subcolumns can not be
      ! calculated.
      call fillAltitudeGridCol(errS,  globalS%ncolumn, globalS%numSpectrBands, globalS%nTrace,      &
                            globalS%cloudAerosolRTMgridSimS, globalS%columnSimS,                    &
                            globalS%gasPTSimS, globalS%optPropRTMGridSimS, globalS%reflDerivHRSimS, &
                            globalS%earthRadianceSimS, globalS%retrS)
      if (errorCheck(errS)) return
      call getOptPropAtmSim (errS, globalS%nTrace, globalS%gasPTSimS, globalS%traceGasSimS, &
                             globalS%optPropRTMGridSimS )
      if (errorCheck(errS)) return

  !
  !TBC make a subroutine to calibrate the wavelengths for the solar irradiance using the
  !velocity read from the configuration file, which includes the Doppler shift
  !

      time  = current_time(current_time_values)
      write(errS%temp,'(A,F14.3)') 'time for reading reflectance and preparing retrieval (sec) = ', time - prev_time
      if (staticS%operational == 1) then
        call logDebug(errS%temp)
      else
        call logInfo(errS%temp)
      end if

    else  ! globalS%controlSimS%useReflectanceFromFile

      prev_time = current_time(current_time_values)

      ! write header to additional output that we deal with results for simulation
      if (staticS%operational /= 1) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'RESULTS FOR SIMULATION'
        write(addtionalOutputUnit,*)
      end if

      ! prepare simulation: set up wavelength grids - claim memory - read polarization correction
      ! - get solar irradiance on high resolution wavelength grid - calculate absorption coefficients
      ! only for full simulation/retrieval sequence.

      if (staticS%operational /= 1) then
        call prepareSimulation(errS, staticS, globalS)
        if (errorCheck(errS)) return
      end if

      call setFlagAAI(errS, globalS%numSpectrBands, globalS%surfaceSimS, globalS%controlSimS, &
                      globalS%RRS_RingSimS, globalS%wavelInstrRadSimS)
      if (errorCheck(errS)) return
      ! after testing for AAI test for ozone profile
      if ( .not. globalS%controlSimS%allowNegativeSurfAlbedo ) then
        call setFlagOzoneProfile(errS, globalS%controlSimS, globalS%nTrace, globalS%traceGasSimS)
        if (errorCheck(errS)) return
      end if

      ! Calculate the HR reflectance (sun-normalized radiance) and derivatives for all spectral bands
      ! for the simulated spectrum. Results are stored in globalS%reflDerivHRSimS

      if ( globalS%controlSimS%method == 0 ) then

        call calcReflAndDeriv(errS, globalS%maxFourierTermLUT, globalS%numSpectrBands, globalS%nTrace,                      &
                              globalS%wavelMRRadSimS,  globalS%wavelHRSimS,                                                 &
                              globalS%XsecHRSimS, globalS%RRS_RingSimS, globalS%geometrySimS, globalS%retrS,                &
                              globalS%controlSimS, globalS%cloudAerosolRTMgridSimS, globalS%gasPTSimS,                      &
                              globalS%traceGasSimS, globalS%surfaceSimS,                                                    &
                              globalS%LambCloudSimS, globalS%mieAerSimS, globalS%mieCldSimS, globalS%createLUTSimS,         &
                              globalS%cldAerFractionSimS, globalS%earthRadMulOffsetSimS, globalS%earthRadStrayLightSimS,    &
                              globalS%solarIrradianceSimS, globalS%optPropRTMGridSimS,  globalS%dn_dnodeSimS,               &
                              globalS%reflDerivHRSimS)
        if (errorCheck(errS)) return

      else

        globalS%nwavelRTM(:) = globalS%weakAbsSimS(:)%degreePoly + 1
        globalS%use_abs_opt_thickn_for_AMF(:) =  globalS%weakAbsSimS(:)%useAbsOptThcknForAMF
        call calcReflAndDerivDismas(errS, globalS%numSpectrBands, globalS%nTrace, globalS%nwavelRTM, globalS%RRS_RingSimS,      &
                                    globalS%use_abs_opt_thickn_for_AMF, globalS%wavelHRSimS, globalS%XsecHRSimS,                &
                                    globalS%geometrySimS, globalS%retrS, globalS%controlSimS, globalS%cloudAerosolRTMgridSimS,  &
                                    globalS%gasPTSimS, globalS%traceGasSimS,  globalS%surfaceSimS, globalS%LambCloudSimS,       &
                                    globalS%mieAerSimS, globalS%mieCldSimS, globalS%cldAerFractionSimS,                         &
                                    globalS%earthRadMulOffsetSimS, globalS%earthRadStrayLightSimS, globalS%solarIrradianceSimS, &
                                    globalS%optPropRTMGridSimS, globalS%dn_dnodeSimS, globalS%reflDerivHRSimS)
        if (errorCheck(errS)) return

      end if ! globalS%controlSimS%method == 0

      ! convolute with slit function, add straylight, add noise, add offsets to the simulated spectrum
      call convoluteOffsetsStraylightNoise(errS, globalS)
      if (errorCheck(errS)) return

      ! write ozone profile OMI - OMO3PR input data
      ! call writeOMO3PR_input

      ! write simulated reflectance to file disamar.sim
      open(UNIT=writeIrrRadUnit, ACTION= 'WRITE', STATUS = 'REPLACE', FILE = reflectanceFileName )
      call write_disamar_sim(errS, globalS%numSpectrBands, globalS%wavelInstrIrrSimS, globalS%wavelInstrRadSimS,  &
                             globalS%solarIrradianceSimS, globalS%earthRadianceSimS, globalS%retrS)
      if (errorCheck(errS)) return
      close( writeIrrRadUnit )

      if ( globalS%controlSimS%testChandrasekhar )                                                                   &
        call testChandrasekhar(errS, globalS%numSpectrBands, globalS%wavelInstrRadSimS, globalS%solarIrradianceSimS, &
                               globalS%earthRadianceSimS, globalS%geometrySimS)

      time  = current_time(current_time_values)
      write(errS%temp,'(A,F14.3)') 'time for simulation (sec) = ', time - prev_time
      call logInfo(errS%temp)

    end if ! globalS%controlSimS%useReflectanceFromFile

    if ( globalS%controlSimS%simulationOnly ) then

      call print_results(errS, globalS%numSpectrBands, globalS%nTrace, globalS%controlSimS, globalS%controlRetrS,           &
                         globalS%wavelMRRadSimS, globalS%wavelMRRadRetrS, globalS%wavelInstrIrrSimS,                        &
                         globalS%wavelInstrRadSimS, globalS%wavelInstrIrrRetrS, globalS%wavelInstrRadRetrS,                 &
                         globalS%geometrySimS, globalS%geometryRetrS, globalS%reflDerivHRSimS, globalS%reflDerivHRRetrS,    &
                         globalS%gasPTSimS, globalS%gasPTRetrS, globalS%solarIrradianceSimS, globalS%solarIrradianceRetrS,  &
                         globalS%earthRadianceSimS, globalS%earthRadianceRetrS,                                             &
                         globalS%traceGasSimS, globalS%traceGasRetrS, globalS%surfaceSimS, globalS%surfaceRetrS,            &
                         globalS%LambCloudSimS, globalS%LambCloudRetrS, globalS%cldAerFractionSimS,                         &
                         globalS%cldAerFractionRetrS,  globalS%cloudAerosolRTMgridSimS, globalS%cloudAerosolRTMgridRetrS,   &
                         globalS%earthRadMulOffsetSimS, globalS%earthRadMulOffsetRetrS,                                     &
                         globalS%earthRadStrayLightSimS, globalS%earthRadStrayLightRetrS, globalS%RRS_RingSimS,             &
                         globalS%RRS_RingRetrS, globalS%retrS, globalS%diagnosticS, globalS%optPropRTMGridSimS,             &
                         globalS%optPropRTMGridRetrS, globalS%polCorrectionRetrS, globalS%ncolumn, globalS%columnSimS,      &
                         globalS%columnRetrS)
      if (errorCheck(errS)) return

      call print_asciiHDF(errS, globalS%external_dataS, globalS%numSpectrBands, globalS%nTrace, globalS%controlSimS,        &
                          globalS%controlRetrS, globalS%wavelMRRadSimS, globalS%wavelMRRadRetrS, globalS%wavelInstrIrrSimS, &
                          globalS%wavelInstrRadSimS, globalS%geometrySimS, globalS%geometryRetrS, globalS%reflDerivHRSimS,  &
                          globalS%reflDerivHRRetrS, globalS%gasPTSimS, globalS%gasPTRetrS, globalS%solarIrradianceSimS,     &
                          globalS%solarIrradianceRetrS, globalS%earthRadianceSimS, globalS%earthRadianceRetrS,              &
                          globalS%XsecHRSimS, globalS%XsecHRRetrS, globalS%XsecHRLUTSimS, globalS%XsecHRLUTRetrS,           &
                          globalS%traceGasSimS, globalS%traceGasRetrS,                                                      &
                          globalS%surfaceSimS, globalS%surfaceRetrS, globalS%LambCloudSimS,  globalS%LambCloudRetrS,        &
                          globalS%cldAerFractionSimS, globalS%cldAerFractionRetrS,  globalS%cloudAerosolRTMgridSimS,        &
                          globalS%cloudAerosolRTMgridRetrS, globalS%earthRadMulOffsetSimS, globalS%earthRadMulOffsetRetrS,  &
                          globalS%earthRadStrayLightSimS, globalS%earthRadStrayLightRetrS,                                  &
                          globalS%RRS_RingSimS, globalS%RRS_RingRetrS, globalS%retrS, globalS%diagnosticS, globalS%ncolumn, &
                          globalS%optPropRTMGridSimS, globalS%optPropRTMGridRetrS,  globalS%columnSimS, globalS%columnRetrS)
      if (errorCheck(errS)) return

      ! find out what memory can be deallocted

      if (staticS%operational /= 1) then
        close( errorCovOutUnit )
        close( intermediateFileUnit )
        close( outputFileUnit )
        close( addtionalOutputUnit )
        close( asciiHDFoutputUnit )
      end if

      call mystop(errS, 'simulation completed')
      if (errorCheck(errS)) return

    end if

    call claimMemRetrS_Se(errS, globalS%retrS)
    if (errorCheck(errS)) return

    call calculate_Se (errS,  globalS%numSpectrBands, globalS%wavelInstrRadSimS, globalS%calibErrorReflS, globalS%retrS )
    if (errorCheck(errS)) return

    call fillAltitudeGridCol(errS, globalS%ncolumn, globalS%numSpectrBands, globalS%nTrace, globalS%cloudAerosolRTMgridRetrS, &
                             globalS%columnRetrS, globalS%gasPTRetrS, globalS%optPropRTMGridRetrS, globalS%reflDerivHRRetrS,  &
                             globalS%earthRadianceRetrS, globalS%retrS)
    if (errorCheck(errS)) return

    ! replace XSecLUTData with data read elsewhere. Only done operationally.
    if (staticS%operational == 1) then
      call replaceXSecLUTData(errS, globalS%inputS, globalS)
      if (errorCheck(errS)) return
    end if

    select case ( globalS%controlRetrS%method )

      case(0:1)
        prev_time    = current_time(current_time_values)
        call prepareOptimalEstimation(errS, staticS, globalS)
        if (errorCheck(errS)) return
        call doFullOptimalEstimation(errS, staticS, globalS)
        if (errorCheck(errS)) return

        if ( globalS%controlRetrS%polCorrectionFile .and. &
             staticS%operational /= 1  ) call fillPolCorrection(errS, globalS)

        time    = current_time(current_time_values)
        write(errS%temp,'(A,F14.3)') 'time for retrieval (sec) = ', time - prev_time
        if (staticS%operational == 1) then
            call logDebug(errS%temp)
        else
            call logInfo(errS%temp)
        end if

        prev_time    = current_time(current_time_values)

        if (staticS%operational /= 1) then

        call print_results(errS, globalS%numSpectrBands, globalS%nTrace, globalS%controlSimS, globalS%controlRetrS,          &
                           globalS%wavelMRRadSimS, globalS%wavelMRRadRetrS, globalS%wavelInstrIrrSimS,                       &
                           globalS%wavelInstrRadSimS, globalS%wavelInstrIrrRetrS, globalS%wavelInstrRadRetrS,                &
                           globalS%geometrySimS, globalS%geometryRetrS, globalS%reflDerivHRSimS, globalS%reflDerivHRRetrS,   &
                           globalS%gasPTSimS, globalS%gasPTRetrS, globalS%solarIrradianceSimS, globalS%solarIrradianceRetrS, &
                           globalS%earthRadianceSimS, globalS%earthRadianceRetrS, globalS%traceGasSimS,                      &
                           globalS%traceGasRetrS, globalS%surfaceSimS, globalS%surfaceRetrS, globalS%LambCloudSimS,          &
                           globalS%LambCloudRetrS, globalS%cldAerFractionSimS, globalS%cldAerFractionRetrS,                  &
                           globalS%cloudAerosolRTMgridSimS, globalS%cloudAerosolRTMgridRetrS,                                &
                           globalS%earthRadMulOffsetSimS, globalS%earthRadMulOffsetRetrS,                                    &
                           globalS%earthRadStrayLightSimS, globalS%earthRadStrayLightRetrS, globalS%RRS_RingSimS,            &
                           globalS%RRS_RingRetrS, globalS%retrS, globalS%diagnosticS, globalS%optPropRTMGridSimS,            &
                           globalS%optPropRTMGridRetrS, globalS%polCorrectionRetrS, globalS%ncolumn, globalS%columnSimS,     &
                           globalS%columnRetrS)
        if (errorCheck(errS)) return

        call print_asciiHDF(errS, globalS%external_dataS, globalS%numSpectrBands, globalS%nTrace, globalS%controlSimS,          &
                            globalS%controlRetrS, globalS%wavelMRRadSimS, globalS%wavelMRRadRetrS, globalS%wavelInstrIrrSimS,   &
                            globalS%wavelInstrRadSimS, globalS%geometrySimS, globalS%geometryRetrS, globalS%reflDerivHRSimS,    &
                            globalS%reflDerivHRRetrS, globalS%gasPTSimS, globalS%gasPTRetrS, globalS%solarIrradianceSimS,       &
                            globalS%solarIrradianceRetrS, globalS%earthRadianceSimS, globalS%earthRadianceRetrS,                &
                            globalS%XsecHRSimS, globalS%XsecHRRetrS, globalS%XsecHRLUTSimS, globalS%XsecHRLUTRetrS,             &
                            globalS%traceGasSimS, globalS%traceGasRetrS,                                                        &
                            globalS%surfaceSimS, globalS%surfaceRetrS, globalS%LambCloudSimS, globalS%LambCloudRetrS,           &
                            globalS%cldAerFractionSimS, globalS%cldAerFractionRetrS, globalS%cloudAerosolRTMgridSimS,           &
                            globalS%cloudAerosolRTMgridRetrS, globalS%earthRadMulOffsetSimS, globalS%earthRadMulOffsetRetrS,    &
                            globalS%earthRadStrayLightSimS, globalS%earthRadStrayLightRetrS,                                    &
                            globalS%RRS_RingSimS, globalS%RRS_RingRetrS, globalS%retrS, globalS%diagnosticS, globalS%ncolumn,   &
                            globalS%optPropRTMGridSimS, globalS%optPropRTMGridRetrS, globalS%columnSimS, globalS%columnRetrS)
        if (errorCheck(errS)) return

        time    = current_time(current_time_values)
        write(errS%temp,'(A,F14.3)') 'time for printing results (sec) = ', time - prev_time
        call logInfo(errS%temp)

        end if

      case(2)

        call logDebug('using DOAS method for retrieval')
        prev_time    = current_time(current_time_values)

        call performDOASfit(errS, staticS,                                                                                     &
                            globalS%numSpectrBands, globalS%nTrace, globalS%wavelInstrRadRetrS, globalS%wavelMRRadRetrS,       &
                            globalS%wavelHRRetrS, globalS%geometryRetrS, globalS%XsecHRRetrS, globalS%gasPTSimS,               &
                            globalS%gasPTRetrS, globalS%traceGasSimS, globalS%traceGasRetrS, globalS%solarIrradianceRetrS,     &
                            globalS%earthRadianceRetrS, globalS%surfaceRetrS, globalS%LambCloudRetrS, globalS%mieAerRetrS,     &
                            globalS%cldAerFractionRetrS, globalS%weakAbsRetrS,  globalS%controlRetrS, globalS%retrS,           &
                            globalS%cloudAerosolRTMgridRetrS, globalS%RRS_RingSimS, globalS%RRS_RingRetrS,                     &
                            globalS%optPropRTMGridRetrS, globalS%diagnosticS)
        if (errorCheck(errS)) return

        time    = current_time(current_time_values)
        write(errS%temp,'(A,F14.3)') 'time for DOAS retrieval (sec) = ', time - prev_time
        call logInfo(errS%temp)

      case(3,4)  ! classic DOAS fit or DOMINO

        call logDebug('using DOAS method for slant column fitting')
        prev_time    = current_time(current_time_values)

        call classicDOASfit(errS, staticS,                                                                                  &
                            globalS%numSpectrBands, globalS%nTrace, globalS%wavelInstrRadRetrS, globalS%wavelHRSimS,        &
                            globalS%wavelMRRadRetrS,  globalS%wavelHRRetrS,                                                 &
                            globalS%geometryRetrS, globalS%XsecHRRetrS, globalS%gasPTSimS,                                  &
                            globalS%gasPTRetrS, globalS%traceGasSimS, globalS%traceGasRetrS,  globalS%solarIrradianceRetrS, &
                            globalS%earthRadianceRetrS, globalS%surfaceRetrS, globalS%LambCloudRetrS, globalS%mieAerRetrS,  &
                            globalS%cldAerFractionRetrS, globalS%weakAbsRetrS, globalS%controlSimS, globalS%controlRetrS,   &
                            globalS%retrS,  globalS%cloudAerosolRTMgridSimS, globalS%cloudAerosolRTMgridRetrS,              &
                            globalS%RRS_RingSimS, globalS%RRS_RingRetrS, globalS%optPropRTMGridSimS,                        &
                            globalS%optPropRTMGridRetrS, globalS%diagnosticS, globalS%reflDerivHRSimS)
        if (errorCheck(errS)) return

        time    = current_time(current_time_values)
        write(errS%temp,'(A,F14.3)') 'time for DOAS slant column fitting (sec) = ', time - prev_time
        call logInfo(errS%temp)

      case default

        call logDebug('ERROR in configuration file: incorrect value for retrieval method')
        call logDebug('in section GENERAL in subsection retrievalMethod')
        call logDebugI('values read is ', globalS%controlRetrS%method )
        call logDebug('allowed values are 0, 1, 2, 3 or 4')
        call mystop(errS, 'stopped due to incorrect value for retrieval method')
        if (errorCheck(errS)) return

      end select

! JdH Debug
    !write(intermediateFileUnit, '(A, F10.4)') 'vmrAtSurface', globalS%traceGasRetrS(1)%vmrAtSurface
    !write(intermediateFileUnit, '(A, F10.4)') 'vmrAtSurface_precision', globalS%traceGasRetrS(1)%vmrAtSurface_precision
    !write(intermediateFileUnit, '(A, F10.4)') 'vmrAtCloud', globalS%traceGasRetrS(1)%vmrAtCloud
    !write(intermediateFileUnit, '(A, F10.4)') 'vmrAtCloud_precision', globalS%traceGasRetrS(1)%vmrAtCloud_precision
    !write(intermediateFileUnit, '(A, F10.4)') 'vmrAtTropopause', globalS%traceGasRetrS(1)%vmrAtTropopause
    !write(intermediateFileUnit, '(A, F10.4)') 'vmrAtTropopause_precision', globalS%traceGasRetrS(1)%vmrAtTropopause_precision

    if (staticS%operational /= 1) then
      close( errorCovOutUnit )
      close( intermediateFileUnit )
      close( outputFileUnit )
      close( addtionalOutputUnit )
      close( asciiHDFoutputUnit )
    end if

    call exit('retrieve')

  end subroutine retrieve


  subroutine cleanupretrieval(errS, globalS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(globalType), pointer :: globalS

    integer              :: allocStatus

    call enter('cleanupretrieval')

    allocStatus = 0
    deallocate( globalS%nwavelRTM, STAT = allocStatus )
    !ignore status
    !if ( allocStatus /= 0 ) then
    !  call logDebug('FATAL ERROR: deallocation failed')
    !  call logDebug('for globalS%nwavelRTM in main program')
    !  call mystop(errS, 'stopped because deallocation failed')
    !  if (errorCheck(errS)) return
    !end if
    allocStatus = 0
    deallocate( globalS%use_abs_opt_thickn_for_AMF, STAT = allocStatus )
    !if ( allocStatus /= 0 ) then
    !  call logDebug('FATAL ERROR: deallocation failed')
    !  call logDebug('for globalS%use_abs_opt_thickn_for_AMF in main program')
    !  call mystop(errS, 'stopped because deallocation failed')
    !  if (errorCheck(errS)) return
    !end if

    call deallocatePartsOfStructures(errS, globalS)
    if (errorCheck(errS)) return

    call deallocateStructuresSim(errS,                                                 &
         globalS%wavelHRSimS, globalS%wavelMRIrrSimS, globalS%wavelMRRadSimS,          &
         globalS%wavelInstrIrrSimS, globalS%wavelInstrRadSimS,                         &
         globalS%absLinesSimS, globalS%XsecHRSimS, globalS%XsecHRLUTSimS,              &
         globalS%traceGasSimS, globalS%surfaceSimS, globalS%LambCloudSimS,             &
         globalS%LambAerSimS, globalS%cldAerFractionSimS, globalS%reflDerivHRSimS,     &
         globalS%solarIrradianceSimS, globalS%earthRadianceSimS,                       &
         globalS%earthRadMulOffsetSimS, globalS%earthRadStrayLightSimS,                &
         globalS%calibErrorReflS, globalS%columnSimS,  globalS%createLUTSimS,          &
         globalS%weakAbsSimS, globalS%dn_dnodeSimS, globalS%RRS_RingSimS)
    if (errorCheck(errS)) return

    call deallocateStructuresRetr(errS,                                                               &
         globalS%wavelHRRetrS, globalS%wavelMRIrrRetrS, globalS%wavelMRRadRetrS,                      &
         globalS%wavelInstrIrrRetrS, globalS%wavelInstrRadRetrS,                                      &
         globalS%absLinesRetrS, globalS%XsecHRRetrS, globalS%XsecHRLUTRetrS,                          &
         globalS%traceGasRetrS, globalS%surfaceRetrS, globalS%LambCloudRetrS, globalS%LambAerRetrS,   &
         globalS%cldAerFractionRetrS, globalS%reflDerivHRRetrS, globalS%solarIrradianceRetrS,         &
         globalS%earthRadianceRetrS, globalS%earthRadMulOffsetRetrS, globalS%earthRadStrayLightRetrS, &
         globalS%polCorrectionRetrS, globalS%columnRetrS, globalS%createLUTRetrS,                     &
         globalS%weakAbsRetrS, globalS%dn_dnodeRetrS, globalS%RRS_RingRetrS)
    if (errorCheck(errS)) return

    call exit('cleanupretrieval')

  end subroutine cleanupretrieval


! other modifications:
! if the reflectance is read make two options
!  - read only wavelengths, sun-normalized radiance, and error in radiance
!  - read wavelengths, sun-normalized radiance, and error in radiance, altitude pressure and temperature profiles
!   the altitude pressure and temperature grid specified in the configuration files is then overwritten
!   by the values read. The temperature and pressure read is interpolated to the altitudes specified in the
!   configuration file (retrieval values)

  subroutine setupInstrWavelengthGrid(errS, wavelInstrS)

    ! Set up wavelength grid for the instrument (nominal wavelength associated with
    ! the slit function).

    ! This subroutine allocates and fills the array wavel in the structure  wavelInstrS

     implicit none

    type(errorType), intent(inout) :: errS
     type(wavelInstrType) :: wavelInstrS

     integer :: iwave, counter, ipair, nwavel
     real(8) :: wavel
     logical :: addWavel

    call enter('setupInstrWavelengthGrid')

     ! number of nominal wavelengths for the instrument
     nwavel = 1 + nint( (wavelInstrS%endWavel - wavelInstrS%startWavel) / wavelInstrS%stepWavel )
     wavelInstrS%nwavel = nwavel

     ! determine the lengths of the wavelength array taking into account excluded wavelengths
     counter = 0
     do iwave = 1, nwavel
       wavel = wavelInstrS%startWavel + (iwave - 1) * wavelInstrS%stepWavel
       addWavel = .true.
       do ipair = 1, wavelInstrS%nExclude
         if ( (wavel >= wavelInstrS%excludeStart(ipair)) .and. (wavel <= wavelInstrS%excludeEnd(ipair)) ) &
           addWavel = .false.
       end do ! ipair
       if ( addWavel ) counter = counter + 1
     end do ! iwave
     wavelInstrS%nwavel = counter

     call claimMemWavelInstrS(errS, wavelInstrS)    ! claim memory for wavelInstrS

     ! determine nominal wavelength grid
     counter = 0
     do iwave = 1, nwavel
       wavel = wavelInstrS%startWavel + (iwave - 1) * wavelInstrS%stepWavel
       addWavel = .true.
       do ipair = 1, wavelInstrS%nExclude
         if ( (wavel >= wavelInstrS%excludeStart(ipair)) .and. (wavel <= wavelInstrS%excludeEnd(ipair)) ) &
           addWavel = .false.
       end do ! ipair
       if ( addWavel ) then
         counter = counter + 1
         wavelInstrS%wavelNominal(counter) = wavel
       end if
     end do ! iwave

     ! determine wavelength shift
     do iwave = 1, wavelInstrS%nwavel
       wavelInstrS%wavelShift(iwave) =  polyInt(errS, wavelInstrS%wavelNode, wavelInstrS%APshift, wavelInstrS%wavelNominal(iwave))
     end do

     ! fill (shifted) wavelengths
      wavelInstrS%wavel(:) =  wavelInstrS%wavelNominal(:) +  wavelInstrS%wavelShift(:)

          !createLUTSimS (iFourier, iband)%wavel(:)	  = ????
          !createLUTRetrS(iFourier, iband)%nwavel	  = ????

    call exit('setupInstrWavelengthGrid')

  end subroutine setupInstrWavelengthGrid


  subroutine setupHRWavelengthGrid(errS, controlS, nTrace, wavelInstrS, wavelHRS,  XsecS, absLinesS, &
                                   RRS_RingS,FWHM)

    ! We assume that we can use spline interpolation on the solar spectrum
    ! to get the solar irradiance at the high resolution (HR) wavelength grids
    ! for simulation and retrieval, i.e. the high resolution solar spectrum
    ! is not undersampled.

    ! If line-absorption (e.g. O2 A band, H2O) is involved, the wavelength grid
    ! for the forward calculations (HR grid) depends on the absorption properties
    ! of the molecule considered. To facilitate integration over the instrument
    ! slit function the total wavelength range considered is divided into a number
    ! of intervals on which, say, 3 - 20 Gaussian division points are defined.
    ! For line-absorption the boundaries of these intervals are determined by the
    ! positions of the (strongest) lines.  We use a threshold to eliminate the weaker lines
    ! from the procedure to select the boundaries. For smoothly varying absorption cross
    ! sections, like ozone, and for wavelength ranges where there is no absorption at all
    ! the boundaries of these intervals are equidistant and equal to  the FWHM of the
    ! slit function.
    !
    ! In the configuration file a minimum and a maximum number of Gaussian division pointa
    ! is specified. The maximum number is used for the widest interval between line
    ! positions and the number is scaled with the width between two line position, but at least
    ! the minimum number of division points is used. If no line absorption occurs, the width
    ! is the FHWHM and the maximum number of Gaussian division points is used.

    ! If more than one trace gas with absorption lines is present, the line positions of the next
    ! trace gas are inserted between those already present. After all (stronger) line positions are
    ! known, the procedure of assigning a number of gaussian division points to the intervals
    ! is started.

    ! This subroutine allocates and fills the array wavel in the structures wavelHRS

     implicit none

     type(errorType), intent(inout) :: errS
     type(controlType), intent(in)  :: controlS
     integer               :: nTrace
     type(wavelInstrType)  :: wavelInstrS
     type(wavelHRType)     :: wavelHRS
     type(XsecType)        :: XsecS(1:nTrace)
     type(absLinesType)    :: absLinesS(1:nTrace)
     type(RRS_RingType)    :: RRS_RingS
     real(8)               :: FWHM

     real(8)    :: waveStart, waveEnd
     real(8)    :: threshold
     type boundariesType
       integer          :: numBoundaries
       real(8), pointer :: boundaries(:)  => null()
     end type boundariesType
     type(boundariesType)  :: boundariesTraceS(nTrace)    ! contains boundaries for each trace gas

     real(8), allocatable :: intervalBoundaries(:)
     real(8)              :: maxInterval
     !  x0(i,j) are the division points (x0(i), i = 1:j) and w0 the corresponding weights
     real(8), allocatable :: x0(:,:)
     real(8), allocatable :: w0(:,:)
     real(8)              :: dw, sw, wavel, newWavel

     logical              :: addWavel
     integer              :: nwavel
     real(8), allocatable :: wavelBand(:)        ! (nwavel) full band - not accounting for excluded parts
     real(8), allocatable :: wavelBandWeight(:)  ! (nwavel) weights for full band - not accounting for excluded parts

     integer              :: iwave, nlines, iinterval, index, ipair
     integer              :: iGauss, nGauss, nGaussMax, nGaussMin
     integer              :: iTrace

!    parameters to determine the extension of the wavelength range when RRS is used
     real(8), allocatable :: wavelRRS(:)
     integer              :: numRRSlines


     integer              :: allocStatus
     integer              :: deallocStatus

     logical, parameter   :: verbose = .false.

    call enter('setupHRWavelengthGrid')

    if (staticS%operational == 1 .and. controlS%usePolyExpXsec) then
        call logDebug("Skipping setupHRWavelengthGrid, usePolyExpXsec is set and we are operational")
        goto 9999
    end if

     ! initialize
     absLinesS(:)%nlinePos = 0
! JdH Debug
     waveStart = wavelInstrS%startWavel - 2.0d0 * FWHM
     waveEnd   = wavelInstrS%endWavel   + 2.0d0 * FWHM
!     waveStart = wavelInstrS%startWavel - 2.0d0 * 0.6
!     waveEnd   = wavelInstrS%endWavel   + 2.0d0 * 0.6

!    Replace the extention with the extension needed for RRS
!    only if useRRS is true
     if (RRS_RingS%useRRS .or. RRS_RingS%approximateRRS) then
       numRRSlines = NumberRamanLines()
       allocStatus = 0
       allocate(wavelRRS(numRRSlines), STAT = allocStatus)
       wavelRRS = RamanLinesScatWavel(waveStart)
       waveStart = minval(wavelRRS)
       wavelRRS = RamanLinesScatWavel(waveEnd)
       waveEnd   = maxval(wavelRRS)
       deallocate(wavelRRS, STAT = deallocStatus)
     end if

     ! find the stronger line positions from the HITRAN database
     ! and put those into boundariesTraceSimS and boundariesTraceRetrS
     do iTrace = 1, nTrace

       if ( absLinesS(iTrace)%useHITRAN ) then

         call getLinePositions(errS,  waveStart, waveEnd, XsecS(iTrace), absLinesS(iTrace) )
         if (errorCheck(errS)) return

         if ( absLinesS(iTrace)%nlinePos > 0 ) then

           ! filter on line intensity for simulation grid
           ! first determine the numbere of lines with this threshold
           nlines = 0
           threshold = maxval(absLinesS(iTrace)%S) * absLinesS(iTrace)%thresholdLine
           do iwave = 1, absLinesS(iTrace)%nlinePos
             if ( absLinesS(iTrace)%S(iwave) >  threshold )  nlines = nlines + 1
           end do
           ! the interval (waveStart, first line) and (last line, waveEnd) may be
           ! to large for accurate integration over the slit function as these interval
           ! contains half a line. Hence introduce two virtual lines within this interval.
           boundariesTraceS(iTrace)%numBoundaries = nlines

           allocStatus = 0
           allocate ( boundariesTraceS(iTrace)%boundaries(1:nlines), STAT = allocStatus )
           if ( allocStatus /= 0 ) then
             call logDebug('FATAL ERROR: allocation failed')
             call logDebug('for boundariesTraceS(iTrace)%boundaries')
             call logDebugI('for iTrace = ', iTrace)
             call logDebug('in subroutine setupHRWavelengthGrid')
             call logDebug('in program DISAMAR - file main_DISAMAR.f90')
             call mystop(errS, 'stopped because allocation failed')
             if (errorCheck(errS)) return
           end if

           nlines = 0
           do iwave = 1, absLinesS(iTrace)%nlinePos
             if ( absLinesS(iTrace)%S(iwave) >  threshold )  then
               nlines = nlines + 1
               boundariesTraceS(iTrace)%boundaries(nlines) = absLinesS(iTrace)%linePos(iwave)
             end if
           end do

         else ! absLinesS(iTrace)%nlinePos = 0

           ! no line positions found
           boundariesTraceS(iTrace)%numBoundaries = 0

         end if ! absLinesS(iTrace)%nlinePos > 0

       else ! globalS%absLinesSimS(iTrace)%useHITRAN = .false.

         boundariesTraceS(iTrace)%numBoundaries = 0

       end if ! globalS%absLinesSimS(iTrace)%useHITRAN

     end do ! iTrace loop

     ! starting at waveStart we add a boundary that lies one FWHM further, unless stronger lines
     ! occur in the interval bounded by the current wavelength (lambda) and lambda + FWHM.
     ! In that case stronger line position is used as next boundary instead of  lambda + FWHM.
     ! first we count how much boundarie there are in the end
     nlines = 0
     wavel = waveStart
     do
       nlines = nlines + 1
       newWavel = wavel + FWHM
       do iTrace = 1, nTrace
         do iwave = 1,  boundariesTraceS(iTrace)%numBoundaries
           if ( ( boundariesTraceS(iTrace)%boundaries(iwave) > wavel    ) .and.  &
                ( boundariesTraceS(iTrace)%boundaries(iwave) < newWavel ) ) then
              newWavel = boundariesTraceS(iTrace)%boundaries(iwave)
           end if
         end do ! iwave
       end do ! iTrace
       wavel = newWavel
       if ( wavel > waveEnd ) exit
     end do

     allocStatus = 0
     allocate ( intervalBoundaries(0:nlines), STAT = allocStatus )
     if ( allocStatus /= 0 ) then
       call logDebug('FATAL ERROR: allocation failed')
       call logDebug('for intervalBoundaries')
       call logDebug('in subroutine setupHRWavelengthGrid')
       call logDebug('in program DISAMAR - file main_DISAMAR.f90')
       call mystop(errS, 'stopped because allocation failed')
       if (errorCheck(errS)) return
     end if

     ! fill values
     nlines = 0
     wavel = waveStart
     intervalBoundaries(0) = wavel
     do
       nlines = nlines + 1
       newWavel = wavel + FWHM
       do iTrace = 1, nTrace
         do iwave = 1,  boundariesTraceS(iTrace)%numBoundaries
           if ( ( boundariesTraceS(iTrace)%boundaries(iwave) > wavel    ) .and.  &
                ( boundariesTraceS(iTrace)%boundaries(iwave) < newWavel ) ) then
              newWavel = boundariesTraceS(iTrace)%boundaries(iwave)
           end if
         end do ! iwave
       end do ! iTrace
       wavel = newWavel
       intervalBoundaries(nlines) = wavel
       if ( wavel > waveEnd ) exit
     end do

     ! fill Gausspoints and weights arrays
     if ( sum(absLinesS(:)%nlinePos) > 0 ) then
       ! there are absorption lines in the interval
       nGaussMax = wavelHRS%nGaussMax
       nGaussMin = wavelHRS%nGaussMin
     else
       nGaussMax = wavelHRS%nGaussFWHM
       nGaussMin = wavelHRS%nGaussFWHM
     end if

     ! allocate arrays for gausspoints and weight on (0,1)
     ! they are deallocated just before leaving this subroutine

     allocStatus = 0
     allocate ( x0(nGaussMax,nGaussMax), w0(nGaussMax,nGaussMax), STAT = allocStatus )
     if ( allocStatus /= 0 ) then
       call logDebug('FATAL ERROR: allocation failed')
       call logDebug('for x0 or w0')
       call logDebug('in subroutine setupHRWavelengthGrid')
       call logDebug('in program DISAMAR - file main_DISAMAR.f90')
       call mystop(errS, 'stopped because allocation failed')
       if (errorCheck(errS)) return
     end if
     x0 = 0.0d0
     w0 = 0.0d0

     do iGauss = nGaussMin, nGaussMax
       call GaussDivPoints(errS, 0.0d0, 1.0d0, x0(:,iGauss), w0(:,iGauss), iGauss)
       if (errorCheck(errS)) return
     end do

     ! determine the number of wavelengths for the high resolution simulation grid
     ! use minimal globalS%wavelHRSimS%nGaussMin division points and maximal globalS%wavelHRSimS%nGaussMax on each interval
     ! determine the largest interval
     maxInterval = 0.0d0
     do iinterval = 2, size(intervalBoundaries) - 2
       dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)
       if ( dw >  maxInterval )  maxInterval = dw
     end do

     index = 0
     do iinterval = 1, size(intervalBoundaries) - 1
       dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)      ! width of interval
       sw = intervalBoundaries(iinterval-1)                                      ! start wavelength for interval
       nGauss = max( nGaussMin, nint( nGaussMax * dw / maxInterval ) )
       if (nGauss >  nGaussMax ) nGauss =  nGaussMax
       do iGauss = 1, nGauss
         index = index + 1
       end do
     end do

     nwavel = index

     allocStatus = 0
     allocate ( wavelBand(nwavel), wavelBandWeight(nwavel), STAT = allocStatus )
     if ( allocStatus /= 0 ) then
       call logDebug('FATAL ERROR: allocation failed')
       call logDebug('for wavelBand or wavelBandWeight')
       call logDebug('in subroutine setupHRWavelengthGrid')
       call logDebug('in program DISAMAR - file main_DISAMAR.f90')
       call mystop(errS, 'stopped because allocation failed')
       if (errorCheck(errS)) return
     end if

     ! fill wavelength grid for the entire spectral band
     index = 1
     do iinterval = 1, size(intervalBoundaries) - 1
       dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)
       sw = intervalBoundaries(iinterval-1)
       nGauss = max(  nGaussMin, nint( nGaussMax * dw / maxInterval ) )
       if (nGauss >  nGaussMax ) nGauss = nGaussMax
       do iGauss = 1, nGauss
         wavelBand(index)       = sw + dw * x0(iGauss, nGauss)
         wavelBandWeight(index) = dw * w0(iGauss, nGauss)
         index = index + 1
       end do
     end do

     ! deal with excluded intervals

     ! copy excluded intervals and reduce them with 4 * FWHM (2 left and 2 right)
     wavelHRS%nExclude        = wavelInstrS%nExclude
     wavelHRS%excludeStart(:) = wavelInstrS%excludeStart(:) + 2 * FWHM
     wavelHRS%excludeEnd(:)   = wavelInstrS%excludeEnd(:)   - 2 * FWHM

     ! determine the lengths of the wavelength array taking into account excluded wavelengths
     if ( wavelHRS%nExclude > 0 ) then
       index = 0
       do iwave = 1, nwavel
         wavel = wavelBand(iwave)
         addWavel = .true.
         do ipair = 1, wavelHRS%nExclude
           if ( (wavel >= wavelHRS%excludeStart(ipair)) .and. (wavel <= wavelHRS%excludeEnd(ipair)) ) &
             addWavel = .false.
         end do ! ipair
         if ( addWavel ) index = index + 1
       end do ! iwave

       wavelHRS%nwavel = index

     else

       wavelHRS%nwavel = nwavel

     end if

     ! claim memory space
     call claimMemWavelHRS(errS, wavelHRS)          ! claim memory for high resolution wavelength grid

     ! fill wavelengths

     if ( wavelHRS%nExclude > 0 ) then

       index = 0
       do iwave = 1, nwavel
         wavel = wavelBand(iwave)
         addWavel = .true.
         do ipair = 1, wavelInstrS%nExclude
           if ( (wavel >= wavelHRS%excludeStart(ipair)) .and. (wavel <= wavelHRS%excludeEnd(ipair)) ) &
             addWavel = .false.
         end do ! ipair
         if ( addWavel ) then
           index = index + 1
           wavelHRS%wavel(index)  = wavelBand(iwave)
           wavelHRS%weight(index) = wavelBandWeight(iwave)
         end if
       end do ! iwave

     else

       do iwave = 1, nwavel
         wavelHRS%wavel(iwave)  = wavelBand(iwave)
         wavelHRS%weight(iwave) = wavelBandWeight(iwave)
       end do

     end if

     if ( verbose ) then

       write(intermediateFileUnit, '(A, 2F10.4)') 'wavelength range = ', waveStart, waveEnd

       do iTrace = 1, nTrace
         if ( absLinesS(iTrace)%useHITRAN ) then
           write( intermediateFileUnit, *) 'line positions and intensities'
           do iwave = 1, absLinesS(iTrace)%nlinePos
             write(intermediateFileUnit, '(I4, F15.6, E15.5)') iwave, absLinesS(itrace)%linePos(iwave), &
                                                               absLinesS(itrace)%S(iwave)
           end do
         end if
       end do

       write(intermediateFileUnit, *)
       write(intermediateFileUnit, *) 'sum lines = ', sum(absLinesS(:)%nlinePos)

       write(intermediateFileUnit, *)
       write( intermediateFileUnit, *) ' interval boundaries'
       do iinterval = 0, size(intervalBoundaries) -1
         write(intermediateFileUnit, '(F15.6)') intervalBoundaries(iinterval)
       end do

       write(intermediateFileUnit, *)
       write(intermediateFileUnit,'(A)') 'wavelengths for the high resolution grid'
       do iwave = 1, wavelHRS%nwavel
         write(intermediateFileUnit,'(F12.5)') wavelHRS%wavel(iwave)
       end do

     end if ! verbose

     ! clean up
     do iTrace = 1, nTrace
       if ( absLinesS(iTrace)%useHITRAN ) then
         if ( absLinesS(iTrace)%nlinePos > 0 ) then
           call freeMemAbsLinesS(errS, absLinesS(iTrace))
           if (errorCheck(errS)) return
           deallocStatus  = 0
           deallocate ( boundariesTraceS(iTrace)%boundaries, STAT = deallocStatus )
           if ( deallocStatus /= 0 ) then
             call logDebug('FATAL ERROR: deallocation failed')
             call logDebug('for boundariesTraceS(iTrace)%boundaries')
             call logDebugI('for iTrace = ', iTrace)
             call logDebug('in subroutine setupHRWavelengthGrid')
             call logDebug('in program DISAMAR - file main_DISAMAR.f90')
             call mystop(errS, 'stopped because deallocation failed')
             if (errorCheck(errS)) return
           end if
         end if
       end if
     end do

     deallocStatus  = 0
     deallocate( intervalBoundaries, x0, w0, wavelBand, wavelBandWeight, STAT = deallocStatus )
     if ( deallocStatus /= 0 ) then
       call logDebug('FATAL ERROR: deallocation failed')
       call logDebug('for intervalBoundaries, x0, w0, wavelBand, or wavelBandWeight')
       call logDebug('in subroutine setupHRWavelengthGrid')
       call logDebug('in program DISAMAR - file main_DISAMAR.f90')
       call mystop(errS, 'stopped because deallocation failed')
       if (errorCheck(errS)) return
     end if

9999 continue
    call exit('setupHRWavelengthGrid')

  end subroutine setupHRWavelengthGrid


  subroutine deallocatePartsOfStructures(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    integer :: deallocStatus
    integer :: sumdeallocStatus
    integer :: iband, iTrace, icolumn, iFourier

    ! the following arrays were allocated when reading the configuration file
    do iband = 1, globalS%numSpectrBands

      deallocStatus    = 0
      sumdeallocStatus = 0

      if ( associated(globalS%solarIrradianceSimS(iband)%SNS%SNwavel) ) then
        deallocate( globalS%solarIrradianceSimS(iband)%SNS%SNwavel, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%solarIrradianceSimS(iband)%SNS%SN_ref_values) ) then
        deallocate( globalS%solarIrradianceSimS(iband)%SNS%SN_ref_values, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%solarIrradianceSimS(iband)%SNS%fvalues) ) then
        deallocate( globalS%solarIrradianceSimS(iband)%SNS%fvalues, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadianceSimS(iband)%SNS%SNwavel) ) then
        deallocate( globalS%earthRadianceSimS(iband)%SNS%SNwavel, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadianceSimS(iband)%SNS%SN_ref_values) ) then
        deallocate( globalS%earthRadianceSimS(iband)%SNS%SN_ref_values, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadianceSimS(iband)%SNS%fvalues) ) then
        deallocate( globalS%earthRadianceSimS(iband)%SNS%fvalues, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightSimS(iband)%wavel) ) then
        deallocate( globalS%earthRadStrayLightSimS(iband)%wavel, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightSimS(iband)%radiance) ) then
        deallocate( globalS%earthRadStrayLightSimS(iband)%radiance, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightSimS(iband)%strayLightNodes) ) then
        deallocate( globalS%earthRadStrayLightSimS(iband)%strayLightNodes, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightSimS(iband)%percentStrayLightAP) ) then
        deallocate( globalS%earthRadStrayLightSimS(iband)%percentStrayLightAP, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightSimS(iband)%percentStrayLight) ) then
        deallocate( globalS%earthRadStrayLightSimS(iband)%percentStrayLight, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightSimS(iband)%percentStrayLightPrev) ) then
        deallocate( globalS%earthRadStrayLightSimS(iband)%percentStrayLightPrev, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightSimS(iband)%varStrayLightAP) ) then
        deallocate( globalS%earthRadStrayLightSimS(iband)%varStrayLightAP, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightSimS(iband)%varStrayLight) ) then
        deallocate( globalS%earthRadStrayLightSimS(iband)%varStrayLight, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightSimS(iband)%radianceCB) ) then
        deallocate( globalS%earthRadStrayLightSimS(iband)%radianceCB, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightRetrS(iband)%wavel) ) then
        deallocate( globalS%earthRadStrayLightRetrS(iband)%wavel, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightRetrS(iband)%radiance) ) then
        deallocate( globalS%earthRadStrayLightRetrS(iband)%radiance, STAT = deallocStatus)
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightRetrS(iband)%strayLightNodes) ) then
        deallocate( globalS%earthRadStrayLightRetrS(iband)%strayLightNodes, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightRetrS(iband)%percentStrayLightAP) ) then
        deallocate( globalS%earthRadStrayLightRetrS(iband)%percentStrayLightAP, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightRetrS(iband)%percentStrayLight) ) then
        deallocate( globalS%earthRadStrayLightRetrS(iband)%percentStrayLight, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightRetrS(iband)%percentStrayLightPrev) ) then
        deallocate( globalS%earthRadStrayLightRetrS(iband)%percentStrayLightPrev, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightRetrS(iband)%varStrayLightAP) ) then
        deallocate( globalS%earthRadStrayLightRetrS(iband)%varStrayLightAP, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightRetrS(iband)%varStrayLight) ) then
        deallocate( globalS%earthRadStrayLightRetrS(iband)%varStrayLight, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%earthRadStrayLightRetrS(iband)%radianceCB) ) then
        deallocate( globalS%earthRadStrayLightRetrS(iband)%radianceCB, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%calibErrorReflS(iband)%wavel) ) then
        deallocate( globalS%calibErrorReflS(iband)%wavel, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%calibErrorReflS(iband)%errorMul) ) then
        deallocate( globalS%calibErrorReflS(iband)%errorMul, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%calibErrorReflS(iband)%errorAdd) ) then
        deallocate( globalS%calibErrorReflS(iband)%errorAdd, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated(globalS%calibErrorReflS(iband)%reflAdd) ) then
        deallocate( globalS%calibErrorReflS(iband)%reflAdd, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( sumdeallocStatus /= 0 ) then
        call logDebug('FATAL ERROR: deallocation failed')
        call logDebugI('for iband = ', iband)
        call logDebug('in subroutine deallocatePartsOfStructures')
        call logDebug('in program DISAMAR - file main_DISAMAR.f90')
        call mystop(errS, 'stopped because deallocation failed')
        if (errorCheck(errS)) return
      end if

      call freeMemLambertianS(errS, globalS%surfaceSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemLambertianS(errS, globalS%surfaceRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemLambertianS(errS, globalS%LambCloudSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemLambertianS(errS, globalS%LambCloudRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemLambertianS(errS, globalS%LambAerSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemLambertianS(errS, globalS%LambAerRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemCldAerFractionS(errS, globalS%cldAerFractionSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemCldAerFractionS(errS, globalS%cldAerFractionRetrS(iband))
!      if (errorCheck(errS)) return

    end do ! iband

    call freeMemGeometryS(errS, globalS%geometrySimS)
!    if (errorCheck(errS)) return
    call freeMemGeometryS(errS, globalS%geometryRetrS)
!    if (errorCheck(errS)) return

    call freeMemGasPTS(errS, globalS%gasPTSimS)
!    if (errorCheck(errS)) return
    call freeMemGasPTS(errS, globalS%gasPTRetrS)
!    if (errorCheck(errS)) return

    do iTrace = 1, globalS%nTrace
      call freeMemTraceGasS(errS, globalS%traceGasSimS(itrace))
!      if (errorCheck(errS)) return
      call freeMemTraceGasS(errS, globalS%traceGasRetrS(itrace))
!      if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
      call freeMemWavelInstrS(errS, globalS%wavelInstrIrrSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemWavelInstrS(errS, globalS%wavelInstrIrrRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemWavelInstrS(errS, globalS%wavelInstrRadSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemWavelInstrS(errS, globalS%wavelInstrRadRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemWavelHRS(errS, globalS%wavelHRSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemWavelHRS(errS, globalS%wavelHRRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemWavelHRS(errS, globalS%wavelMRIrrSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemWavelHRS(errS, globalS%wavelMRRadSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemWavelHRS(errS, globalS%wavelMRIrrRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemWavelHRS(errS, globalS%wavelMRRadRetrS(iband))
!      if (errorCheck(errS)) return
      do iTrace = 1, globalS%nTrace
        call freeMemXsecS(errS, globalS%XsecHRSimS(iband,iTrace))
        call freeMemXsecLUTS(errS, globalS%XsecHRLUTSimS(iband,iTrace))
!        if (errorCheck(errS)) return
        call freeMemXsecS(errS, globalS%XsecHRRetrS(iband,iTrace))
        call freeMemXsecLUTS(errS, globalS%XsecHRLUTRetrS(iband,iTrace))
!        if (errorCheck(errS)) return
      end do
      call freeMemReflDerivS(errS, globalS%reflDerivHRSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemReflDerivS(errS, globalS%reflDerivHRRetrS(iband))
!      if (errorCheck(errS)) return

      call freeMemSolarIrradianceS(errS, globalS%solarIrradianceSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemSolarIrradianceS(errS, globalS%solarIrradianceRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemSolarIrradianceS_Slit(errS, globalS%solarIrradianceSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemSolarIrradianceS_Slit(errS, globalS%solarIrradianceRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemEarthRadianceS(errS, globalS%earthRadianceSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemEarthRadianceS(errS, globalS%earthRadianceRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemEarthRadianceS_Slit(errS, globalS%earthRadianceSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemEarthRadianceS_Slit(errS, globalS%earthRadianceRetrS(iband))
!      if (errorCheck(errS)) return
      call freeMemRRS_RingS(errS, globalS%RRS_RingSimS(iband))
!      if (errorCheck(errS)) return
      call freeMemRRS_RingS(errS, globalS%RRS_RingRetrS(iband))
!      if (errorCheck(errS)) return

      do iFourier = 0, globalS%maxFourierTermLUT
        call freeMemCreateLUTS(errS, globalS%createLUTSimS(iFourier, iband))
        call freeMemCreateLUTS(errS, globalS%createLUTRetrS(iFourier, iband))
      end do

      if ( globalS%polCorrectionRetrS(iband)%usePolarizationCorrection ) then
        call freeMemPolCorrectionS(errS, globalS%polCorrectionRetrS(iband))
!     if (errorCheck(errS)) return
      end if

      if ( associated( globalS%earthRadianceSimS(iband)%KHR_ndensCol ) ) then
           deallocate( globalS%earthRadianceSimS(iband)%KHR_ndensCol )
      end if
      if ( associated( globalS%earthRadianceSimS(iband)%K_ndensCol ) ) then
           deallocate( globalS%earthRadianceSimS(iband)%K_ndensCol )
      end if

      if ( associated( globalS%earthRadianceRetrS(iband)%KHR_ndensCol ) ) then
           deallocate( globalS%earthRadianceRetrS(iband)%KHR_ndensCol )
      end if
      if ( associated( globalS%earthRadianceRetrS(iband)%K_ndensCol ) ) then
           deallocate( globalS%earthRadianceRetrS(iband)%K_ndensCol )
      end if

    end do ! iband

    ! globalS%cloudAerosolRTMgridSimS, globalS%cloudAerosolRTMgridRetrS
    call freeMemCloudAerosolRTMgridS(errS, globalS%cloudAerosolRTMgridSimS)
!    if (errorCheck(errS)) return
    call freeMemCloudAerosolRTMgridS(errS, globalS%cloudAerosolRTMgridRetrS)
!    if (errorCheck(errS)) return

    call freeMemOptPropRTMGridS(errS, globalS%optPropRTMGridSimS)
!    if (errorCheck(errS)) return
    call freeMemOptPropRTMGridS(errS, globalS%optPropRTMGridRetrS)
!    if (errorCheck(errS)) return

    call freeMemRetrS_not_Se(errS, globalS%retrS)
!    if (errorCheck(errS)) return
    call freeMemRetrS_Se    (errS, globalS%retrS)
!    if (errorCheck(errS)) return
    call freeMemDiagnosticS(errS, globalS%diagnosticS)
!    if (errorCheck(errS)) return
    do iColumn = 1, globalS%ncolumn
      call freeMemColumnS(errS, globalS%columnSimS (iColumn))
!      if (errorCheck(errS)) return
      call freeMemColumnS(errS, globalS%columnRetrS(iColumn))
!      if (errorCheck(errS)) return
    end do

    do iTrace = 0, globalS%nTrace
      call freeMemdn_dnodeS(errS, globalS%dn_dnodeSimS (iTrace))
!      if (errorCheck(errS)) return
      call freeMemdn_dnodeS(errS, globalS%dn_dnodeRetrS(iTrace))
!      if (errorCheck(errS)) return
    end do ! iTrace

!   MTL assume everything ok
    errS%code = 0

  end subroutine deallocatePartsOfStructures


  subroutine set_ninterval(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    ! Assign values to ninterval in the optPropRTMGrid structures for simulation and retrieval.
    globalS%optPropRTMGridSimS%ninterval     = globalS%cloudAerosolRTMgridSimS%ninterval
    globalS%optPropRTMGridRetrS%ninterval    = globalS%cloudAerosolRTMgridRetrS%ninterval

  end subroutine set_ninterval


  subroutine set_nalt(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    integer :: iTrace

    do iTrace = 1, globalS%nTrace
      globalS%XsecHRSimS (:,iTrace)%nalt     = globalS%traceGasSimS (iTrace)%nalt
      globalS%XsecHRRetrS(:,iTrace)%nalt     = globalS%traceGasRetrS(iTrace)%nalt
    end do

    ! set the number of levels in the diagnostic structure
    ! to distinguish the ozone profile from other fit parameters
    globalS%diagnosticS%nlevel = globalS%traceGasRetrS (1)%nalt + 1

  end subroutine set_nalt


  subroutine set_RTMnlayer(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

      globalS%reflDerivHRSimS %RTMnlayer   = globalS%optPropRTMGridSimS %RTMnlayer
      globalS%reflDerivHRRetrS%RTMnlayer   = globalS%optPropRTMGridRetrS%RTMnlayer
      globalS%earthRadianceSimS %RTMnlayer = globalS%optPropRTMGridSimS %RTMnlayer
      globalS%earthRadianceRetrS%RTMnlayer = globalS%optPropRTMGridRetrS%RTMnlayer
      globalS%weakAbsSimS%RTMnlayer        = globalS%optPropRTMGridRetrS%RTMnlayer
      globalS%weakAbsRetrS%RTMnlayer       = globalS%optPropRTMGridRetrS%RTMnlayer
      globalS%createLUTSimS (:,:)%nlevel   = globalS%optPropRTMGridRetrS%RTMnlayer
      globalS%createLUTRetrS(:,:)%nlevel   = globalS%optPropRTMGridRetrS%RTMnlayer

  end subroutine set_RTMnlayer


  subroutine set_dimSV(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

      globalS%optPropRTMGridSimS %dimSV     = globalS%controlSimS %dimSV
      globalS%optPropRTMGridRetrS%dimSV     = globalS%controlRetrS%dimSV
      globalS%reflDerivHRSimS %dimSV        = globalS%controlSimS %dimSV
      globalS%reflDerivHRRetrS%dimSV        = globalS%controlRetrS%dimSV
      globalS%earthRadianceSimS %dimSV      = globalS%controlSimS %dimSV
      globalS%earthRadianceRetrS%dimSV      = globalS%controlRetrS%dimSV
      globalS%createLUTSimS (:,:)%dimSV     = globalS%controlSimS %dimSV
      globalS%createLUTRetrS(:,:)%dimSV     = globalS%controlRetrS%dimSV

  end subroutine set_dimSV


  subroutine set_nstate(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

      globalS%reflDerivHRSimS %nstate   = globalS%retrS%nstate
      globalS%reflDerivHRRetrS%nstate   = globalS%retrS%nstate
      globalS%earthRadianceSimS %nstate = globalS%retrS%nstate
      globalS%earthRadianceRetrS%nstate = globalS%retrS%nstate
      globalS%diagnosticS%nstate        = globalS%retrS%nstate
      if ( globalS%traceGasRetrS(1)%fitprofile ) then
        globalS%diagnosticS%nother        = globalS%retrS%nstate - globalS%diagnosticS%nlevel
      else
        globalS%diagnosticS%nother        = globalS%retrS%nstate
      end if

  end subroutine set_nstate


  subroutine set_nwavelInstr(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    ! set values for the number of wavelengths on the instrument grid

    integer :: iTrace, iband, icolumn, iFourier

    do iTrace = 1, globalS%nTrace
      do iband = 1, globalS%numSpectrBands
        globalS%XsecHRSimS (iband,iTrace)%nwavel  = globalS%wavelInstrRadSimS(iband)%nwavel
        globalS%XsecHRRetrS(iband,iTrace)%nwavel  = globalS%wavelInstrRadRetrS(iband)%nwavel
      end do
    end do

    do icolumn = 1, globalS%ncolumn
      globalS%columnSimS (icolumn)%nwavel  = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
      globalS%columnRetrS(icolumn)%nwavel  = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
    end do ! icolumn

    do iband = 1, globalS%numSpectrBands
      globalS%solarIrradianceSimS (iband)%nwavel   = globalS%wavelInstrRadSimS(iband)%nwavel
      globalS%solarIrradianceRetrS(iband)%nwavel   = globalS%wavelInstrRadRetrS(iband)%nwavel
      globalS%earthRadianceSimS(iband)%nwavel      = globalS%wavelInstrRadSimS(iband)%nwavel
      globalS%earthRadianceRetrS(iband)%nwavel     = globalS%wavelInstrRadRetrS(iband)%nwavel
      globalS%weakAbsSimS(iband)%nwavelInstr       = globalS%wavelInstrRadSimS(iband)%nwavel
      globalS%weakAbsRetrS(iband)%nwavelInstr      = globalS%wavelInstrRadRetrS(iband)%nwavel
      globalS%RRS_RingSimS(iband)%nwavel           = globalS%wavelInstrRadSimS(iband)%nwavel
      globalS%RRS_RingRetrS(iband)%nwavel          = globalS%wavelInstrRadRetrS(iband)%nwavel
      globalS%createLUTSimS(:,iband)%nwavel        = globalS%wavelInstrRadSimS(iband)%nwavel
      globalS%createLUTRetrS(:,iband)%nwavel       = globalS%wavelInstrRadRetrS(iband)%nwavel
      globalS%earthRadStrayLightSimS(iband)%nwavelCB  = globalS%wavelInstrRadSimS(iband)%nwavel
      globalS%earthRadStrayLightRetrS(iband)%nwavelCB = globalS%wavelInstrRadRetrS(iband)%nwavel
      if (.not. associated(globalS%earthRadStrayLightSimS(iband)%radianceCB) ) then
        allocate(globalS%earthRadStrayLightSimS(iband)%radianceCB(globalS%earthRadStrayLightSimS(iband)%nwavelCB))
      end if
      if (.not. associated(globalS%earthRadStrayLightRetrS(iband)%radianceCB) ) then
        allocate(globalS%earthRadStrayLightRetrS(iband)%radianceCB(globalS%earthRadStrayLightRetrS(iband)%nwavelCB))
      end if
      do iFourier = 0, globalS%maxFourierTermLUT
          globalS%createLUTSimS (iFourier, iband)%nwavel  = globalS%wavelInstrRadSimS(iband)%nwavel
          globalS%createLUTRetrS(iFourier, iband)%nwavel  = globalS%wavelInstrRadSimS(iband)%nwavel
      end do
    end do
    globalS%retrS%nwavelRetr                       = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
    globalS%diagnosticS%nwavelRetr                 = globalS%retrS%nwavelRetr

  end subroutine set_nwavelInstr


  subroutine set_nwavelMR(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    integer :: iband

    do iband = 1, globalS%numSpectrBands
      globalS%solarIrradianceSimS (iband)%nwavelMR = globalS%wavelMRRadSimS (iband)%nwavel
      globalS%solarIrradianceRetrS(iband)%nwavelMR = globalS%wavelMRRadRetrS(iband)%nwavel
    end do

  end subroutine set_nwavelMR

  subroutine set_nwavelHR(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    integer :: iTrace, iband

    ! set vaues for the number of wavelengths on the high resolution grid
    do iTrace = 1, globalS%nTrace
      do iband = 1, globalS%numSpectrBands
        globalS%XsecHRSimS (iband,iTrace)%nwavelHR     = globalS%wavelMRRadSimS (iband)%nwavel
        globalS%XsecHRRetrS(iband,iTrace)%nwavelHR     = globalS%wavelMRRadRetrS(iband)%nwavel
        globalS%XsecHRLUTSimS (iband,iTrace)%nwavelHR  = globalS%wavelMRRadSimS (iband)%nwavel
        globalS%XsecHRLUTRetrS(iband,iTrace)%nwavelHR  = globalS%wavelMRRadRetrS(iband)%nwavel
      end do
    end do
    do iband = 1, globalS%numSpectrBands
      globalS%reflDerivHRSimS (iband)%nwavel       = globalS%wavelMRRadSimS (iband)%nwavel
      globalS%reflDerivHRRetrS(iband)%nwavel       = globalS%wavelMRRadRetrS(iband)%nwavel
      ! exception for solar irradiance. Here we actually need the HR grid in all cases
      globalS%solarIrradianceSimS (iband)%nwavelHR = globalS%wavelHRSimS (iband)%nwavel
      globalS%solarIrradianceRetrS(iband)%nwavelHR = globalS%wavelHRRetrS(iband)%nwavel
      globalS%earthRadianceSimS (iband)%nwavelHR   = globalS%wavelMRRadSimS (iband)%nwavel
      globalS%earthRadianceRetrS(iband)%nwavelHR   = globalS%wavelMRRadRetrS(iband)%nwavel
      globalS%weakAbsSimS(iband)%nwavelHR          = globalS%wavelMRRadSimS (iband)%nwavel
      globalS%weakAbsRetrS(iband)%nwavelHR         = globalS%wavelMRRadRetrS(iband)%nwavel
      globalS%RRS_RingSimS(iband)%nwavelHR         = globalS%wavelMRRadSimS (iband)%nwavel
      globalS%RRS_RingRetrS(iband)%nwavelHR        = globalS%wavelMRRadRetrS(iband)%nwavel
    end do

  end subroutine set_nwavelHR


  subroutine claimRemainingMemory(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    integer iband, iTrace, iFourier

    do iband = 1, globalS%numSpectrBands
      do iTrace = 1, globalS%nTrace
        call claimMemXsecS(errS, globalS%XsecHRSimS(iband,iTrace))
        if (errorCheck(errS)) return
        call claimMemXsecLUTS(errS, globalS%XsecHRLUTSimS(iband,iTrace))
        if (errorCheck(errS)) return
        call claimMemXsecS(errS, globalS%XsecHRRetrS(iband,iTrace))
        if (errorCheck(errS)) return
        call claimMemXsecLUTS(errS, globalS%XsecHRLUTRetrS(iband,iTrace))
        if (errorCheck(errS)) return
      end do
      call claimMemReflDerivS(errS, globalS%reflDerivHRSimS(iband))
      if (errorCheck(errS)) return
      call claimMemReflDerivS(errS, globalS%reflDerivHRRetrS(iband))
      if (errorCheck(errS)) return
      call claimMemSolarIrradianceS(errS, globalS%solarIrradianceSimS(iband))
      if (errorCheck(errS)) return
      call claimMemSolarIrradianceS(errS, globalS%solarIrradianceRetrS(iband))
      if (errorCheck(errS)) return
      call claimMemEarthRadianceS(errS, globalS%earthRadianceSimS(iband))
      if (errorCheck(errS)) return
      call claimMemEarthRadianceS(errS, globalS%earthRadianceRetrS(iband))
      if (errorCheck(errS)) return
      call claimMemRRS_RingS(errS, globalS%RRS_RingSimS(iband))
      if (errorCheck(errS)) return
      call claimMemRRS_RingS(errS, globalS%RRS_RingRetrS(iband))
      if (errorCheck(errS)) return
      do iFourier = 0, globalS%maxFourierTermLUT
        call claimMemCreateLUTS(errS, globalS%createLUTSimS(iFourier, iband) )
        if (errorCheck(errS)) return
        call claimMemCreateLUTS(errS, globalS%createLUTRetrS(iFourier, iband) )
        if (errorCheck(errS)) return
      end do ! iFourier
    end do ! iband
    call claimMemOptPropRTMGridS(errS, globalS%optPropRTMGridSimS)
    if (errorCheck(errS)) return
    call claimMemOptPropRTMGridS(errS, globalS%optPropRTMGridRetrS)
    if (errorCheck(errS)) return
    call claimMemDiagnosticS(errS, globalS%diagnosticS)
    if (errorCheck(errS)) return
    do iTrace = 0, globalS%nTrace
      call claimMemdn_dnodeS(errS, globalS%dn_dnodeSimS (iTrace))
      if (errorCheck(errS)) return
      call claimMemdn_dnodeS(errS, globalS%dn_dnodeRetrS(iTrace))
      if (errorCheck(errS)) return
    end do ! iTrace

  end subroutine claimRemainingMemory


  subroutine offsetsStraylight(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    integer :: iband

    ! add offsets, and stray light to the measured spectrum

    ! fill reference spectrum for simulation and retrieval
    ! use the same reference spectrum in both cases
    do iband = 1, globalS%numSpectrBands
      call fillReferenceSpectrum(errS,  globalS%controlSimS, globalS%wavelMRRadSimS(iband), globalS%wavelInstrRadSimS(iband), &
                                  globalS%earthRadianceSimS(iband), globalS%earthRadianceRetrS(iband) )
      if (errorCheck(errS)) return
      call fillRadianceAtMulOffsetNodes (errS, globalS%wavelInstrRadSimS(iband), globalS%earthRadianceSimS(iband), &
                                         globalS%earthRadMulOffsetSimS(iband) )
      if (errorCheck(errS)) return
      call fillRadianceAtStrayLightNodes (errS,  globalS%wavelInstrRadSimS(iband), globalS%earthRadianceSimS(iband), &
                                           globalS%earthRadStrayLightSimS(iband) )
      if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
        call addMulOffset(errS,  globalS%wavelInstrRadSimS(iband), globalS%earthRadMulOffsetSimS(iband), &
                           globalS%earthRadianceSimS(iband) )
        if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
        call addStraylight(errS,  globalS%wavelInstrRadSimS(iband), globalS%earthRadStrayLightSimS(iband), &
                            globalS%earthRadianceSimS(iband) )
        if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
      call addSimpleOffsets(errS,  globalS%solarIrradianceSimS(iband), globalS%earthRadianceSimS(iband) )
      if (errorCheck(errS)) return
    end do

    ! fill 'measured' reflectance = sun-normalized radiance in the retrieval structure
    call fillReflectanceSim(errS, globalS%numSpectrBands, globalS%controlSimS, globalS%wavelInstrRadSimS, &
                            globalS%solarIrradianceSimS, globalS%earthRadianceSimS, globalS%retrS)
    if (errorCheck(errS)) return

    ! fill calibration error ( must come after the call to fillReflectanceSim )
    call fillReflCalibrationError(errS, globalS%numSpectrBands, globalS%wavelInstrRadSimS, globalS%calibErrorReflS, globalS%retrS)
    if (errorCheck(errS)) return

  end subroutine offsetsStraylight


  subroutine convoluteOffsetsStraylightNoise(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    integer :: iband

    do iband = 1, globalS%numSpectrBands
      call fillRadianceDerivativesHRgrid(errS, globalS%geometrySimS, globalS%controlSimS, globalS%reflDerivHRSimS(iband), &
                                         globalS%solarIrradianceSimS(iband), globalS%earthRadianceSimS(iband))
      if (errorCheck(errS)) return
    end do ! iband

    ! add offsets, stray light, and noise to the simulated spectrum

    do iband = 1, globalS%numSpectrBands
      call addSpecFeaturesIrr(errS,  globalS%wavelMRIrrSimS(iband), globalS%solarIrradianceSimS(iband) )
      if (errorCheck(errS)) return
      call addSpecFeaturesRad(errS,  globalS%wavelMRRadSimS(iband), globalS%earthRadianceSimS(iband) )
      if (errorCheck(errS)) return
    end do ! iband

    do iband = 1, globalS%numSpectrBands
      call integrateSlitFunctionIrr(errS,  globalS%controlSimS, globalS%wavelHRSimS(iband), globalS%wavelInstrIrrSimS(iband), &
                                     globalS%solarIrradianceSimS(iband), globalS%earthRadianceSimS(iband),              &
                                     globalS%RRS_RingSimS(iband) )
      if (errorCheck(errS)) return
      call integrateSlitFunctionRad(errS,  globalS%controlSimS, globalS%wavelHRSimS(iband), globalS%wavelInstrRadSimS(iband),  &
                                     globalS%retrS, globalS%solarIrradianceSimS(iband), globalS%earthRadianceSimS(iband) )
      if (errorCheck(errS)) return
      call calculateRTM_Ring_spectra(errS, globalS%controlSimS, globalS%wavelInstrIrrSimS(iband),                &
                                     globalS%solarIrradianceSimS(iband), globalS%earthRadianceSimS(iband), &
                                     globalS%RRS_RingSimS(iband))
      if (errorCheck(errS)) return
    end do ! iband

    ! fill reference spectrum for simulation and retrieval
    ! use the same reference spectrum in both cases
    do iband = 1, globalS%numSpectrBands
      call fillReferenceSpectrum(errS,  globalS%controlSimS, globalS%wavelMRRadSimS(iband), globalS%wavelInstrRadSimS(iband), &
                                  globalS%earthRadianceSimS(iband), globalS%earthRadianceRetrS(iband) )
      if (errorCheck(errS)) return
      call fillRadianceAtMulOffsetNodes (errS, globalS%wavelInstrRadSimS(iband), globalS%earthRadianceSimS(iband), &
                                         globalS%earthRadMulOffsetSimS(iband) )
      if (errorCheck(errS)) return
      call fillRadianceAtStrayLightNodes (errS,  globalS%wavelInstrRadSimS(iband), globalS%earthRadianceSimS(iband), &
                                           globalS%earthRadStrayLightSimS(iband) )
      if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
        call addMulOffset(errS,  globalS%wavelInstrRadSimS(iband), globalS%earthRadMulOffsetSimS(iband), &
                           globalS%earthRadianceSimS(iband) )
        if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
        call addStraylight(errS,  globalS%wavelInstrRadSimS(iband), globalS%earthRadStrayLightSimS(iband), &
                            globalS%earthRadianceSimS(iband) )
        if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
      call addRingSpec(errS,  globalS%controlSimS, globalS%wavelInstrRadSimS(iband), globalS%RRS_RingSimS(iband), &
                        globalS%solarIrradianceSimS(iband), globalS%earthRadianceSimS(iband) )
      if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
      call specifyNoise(errS,  globalS%wavelInstrRadSimS(iband), globalS%solarIrradianceSimS(iband), &
                        globalS%earthRadianceSimS(iband) )
      if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
      call addSimpleOffsets(errS,  globalS%solarIrradianceSimS(iband), globalS%earthRadianceSimS(iband) )
      if (errorCheck(errS)) return
    end do

    do iband = 1, globalS%numSpectrBands
      call addSmear(errS,  globalS%wavelInstrRadSimS(iband), globalS%solarIrradianceSimS(iband), globalS%earthRadianceSimS(iband) )
      if (errorCheck(errS)) return
    end do

    call ignorePolarizationScrambler(errS, globalS%numSpectrBands, globalS%controlSimS, &
                                     globalS%wavelInstrRadSimS, globalS%earthRadianceSimS)
    if (errorCheck(errS)) return

    ! fill 'measured' reflectance = sun-normalized radiance in the retrieval structure
    call fillReflectanceSim(errS, globalS%numSpectrBands, globalS%controlSimS, globalS%wavelInstrRadSimS, &
                            globalS%solarIrradianceSimS, globalS%earthRadianceSimS, globalS%retrS)
    if (errorCheck(errS)) return

    ! fill calibration error ( must come after the call to fillReflectanceSim )
    call fillReflCalibrationError(errS, globalS%numSpectrBands, globalS%wavelInstrRadSimS, globalS%calibErrorReflS, globalS%retrS)
    if (errorCheck(errS)) return

  end subroutine convoluteOffsetsStraylightNoise


  subroutine writeOMO3PR_input(errS, globalS)
    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    integer :: itrace

    do itrace = 1, globalS%nTrace
      ! only useful for ozone profile
      ! write ASCII that can be read by the OMI ozone profile PGE
      globalS%nameTraceGas = 'O3'
      if ( ( globalS%nameTraceGas == globalS%traceGasSimS(iTrace)%nameTraceGas ) .and.  &
           ( globalS%traceGasRetrS(iTrace)%fitProfile ) .and. (globalS%numSpectrBands == 2) ) then
        call writeAsCIIinputOMO3PR(errS, globalS%numSpectrBands,  globalS%controlSimS, globalS%geometrySimS, globalS%gasPTSimS,   &
                                   globalS%cloudAerosolRTMgridSimS, globalS%optPropRTMGridSimS, globalS%wavelInstrRadSimS,  &
                                   globalS%surfaceSimS, globalS%solarIrradianceSimS, globalS%earthRadianceSimS, globalS%retrS)
        if (errorCheck(errS)) return
      end if
    end do

  end subroutine writeOMO3PR_input


  subroutine prepareSimulation(errS, staticS, globalS)
    type(errorType), intent(inout) :: errS
    type(staticType), intent(in) :: staticS
    type(globalType) :: globalS

    integer :: iband, iTrace
    integer :: current_time_values(8)
    real(8) :: time, prev_time

    call enter('prepareSimulation')

    ! the arrays in the following structures were allocated by the calls to
    ! readConfigFileAndSetFixedValues and setupWavelengthGrids
    ! globalS%geometrySimS, globalS%geometryRetrS
    ! globalS%gasPTSimS,  globalS%gasPTRetrS, globalS%traceGasSimS, globalS%traceGasRetrS
    ! globalS%cloudAerosolRTMgridSimS, globalS%cloudAerosolRTMgridRetrS
    ! globalS%absLinesSimS,   globalS%absLinesRetrS, wavelInstrSimS, wavelInstrRetrS
    ! globalS%wavelHRSimS,  globalS%wavelHRRetrS, globalS%columnSimS (exceptAK), globalS%columnRetrS (exceptAK)

    call fillAltitudeGridCol(errS,  globalS%ncolumn, globalS%numSpectrBands, globalS%nTrace, globalS%cloudAerosolRTMgridSimS,  &
                              globalS%columnSimS, globalS%gasPTSimS, globalS%optPropRTMGridSimS, globalS%reflDerivHRSimS,      &
                              globalS%earthRadianceSimS, globalS%retrS)
    if (errorCheck(errS)) return

    ! get solar irradiance on HR (integration) grid for simulation
    ! high - resolution grid is needed in addition to the instrument grid (in wavelMRRadSimS ) when
    ! slit function is ignored as the HR grid is used for calculating the effective cross sections
    do iband = 1, globalS%numSpectrBands
      call getHRSolarIrradiance(errS, globalS%wavelMRRadSimS(iband), globalS%solarIrradianceSimS(iband), &
                                globalS%RRS_RingSimS(iband), globalS%wavelHRSimS(iband) )
      if (errorCheck(errS)) return
      call integrateSlitFunctionIrr(errS, globalS%controlSimS, globalS%wavelHRSimS(iband), globalS%wavelInstrIrrSimS(iband), &
                                    globalS%solarIrradianceSimS(iband), globalS%earthRadianceSimS(iband),              &
                                    globalS%RRS_RingSimS(iband))
      if (errorCheck(errS)) return
    end do

    ! get absorption coefficients for all wavelengths at the pressures used for the simulation
    do iband = 1, globalS%numSpectrBands
      do iTrace = 1, globalS%nTrace
        ! first calculate expansion coefficients for the absorption cross section for
        ! each individual absorbing gas to be used in the simulation
        if (  globalS%XsecHRLUTSimS(iband,iTrace)%createXsecPolyLUT  ) then
          prev_time = current_time(current_time_values)
          call createXsecLUT (errS, staticS, globalS%controlSimS, globalS%wavelHRSimS(iband),               &
                              globalS%weakAbsRetrS(iband), globalS%gasPTSimS, globalS%traceGasSimS(iTrace), &
                              globalS%XsecHRSimS(iband,iTrace), globalS%XsecHRLUTSimS(iband,iTrace))
          if (errorCheck(errS)) return
          time = current_time(current_time_values)
          write(errS%temp,'(A,F14.3)') 'time for expansion coefficients simulation (sec) = ', time - prev_time
          call logDebug(errS%temp)
          if (errorCheck(errS)) return
        end if ! createXsecPolyLUT

        if (globalS%controlSimS%usePolyExpXsec) then
          prev_time = current_time(current_time_values)
          ! calculate the absorption cross section using the values in the Xsec LUT
          call getAbsorptionXsecUsingLUT(errS, globalS%controlSimS, globalS%wavelMRRadSimS(iband),               &
                                         globalS%gasPTSimS, globalS%traceGasSimS(iTrace),                        &
                                         globalS%XsecHRSimS(iband,iTrace), globalS%XsecHRLUTSimS(iband,iTrace),  &
                                         globalS%wavelInstrRadSimS(iband), globalS%solarIrradianceSimS(iband),   &
                                         globalS%earthRadianceSimS(iband), globalS%wavelHRSimS(iband) )
          if (errorCheck(errS)) return
          time = current_time(current_time_values)
          write(errS%temp,'(A,F14.3)') 'time for using the expansion coefficients (sec) = ', time - prev_time
          call logDebug(errS%temp)
          if (errorCheck(errS)) return
        else
          ! calculate the absorption cross section without using the LUT, which involves reading
          ! absorption data from files
          prev_time = current_time(current_time_values)
          call getAbsorptionXsec(errS, staticS,                                                                     &
                                 globalS%controlSimS, globalS%weakAbsRetrS(iband), globalS%wavelMRRadSimS(iband),   &
                                 globalS%gasPTSimS, globalS%traceGasSimS(iTrace), globalS%XsecHRSimS(iband,iTrace), &
                                 globalS%wavelInstrRadSimS(iband), globalS%solarIrradianceSimS(iband),              &
                                 globalS%earthRadianceSimS(iband), globalS%wavelHRSimS(iband) )
          if (errorCheck(errS)) return
          time = current_time(current_time_values)
          write(errS%temp,'(A,F14.3)') 'time for NOT using the expansion coefficients (sec) = ', time - prev_time
          call logDebug(errS%temp)
          if (errorCheck(errS)) return
        end if ! usePolyExpXsec
      end do
    end do
    call exit('prepareSimulation')
  end subroutine prepareSimulation


  subroutine prepareOptimalEstimation(errS, staticS, globalS)
    type(errorType), intent(inout) :: errS
    type(staticType), pointer :: staticS
    type(globalType), pointer :: globalS

    ! local
    integer :: iTrace, iband
#ifdef WRITE_SPECTRA_TO_STDOUT
    integer :: i, j
#endif
    integer :: current_time_values(8)
    real(8) :: time, prev_time

    call enter('prepareOptimalEstimation')

    if (staticS%operational /= 1) then
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') 'RESULTS FOR RETRIEVAL '
    end if

    ! get solar irradiance on HR retrieval grid
    ! and fill Ring spectra using the slit function for the earth radiance
    do iband = 1, globalS%numSpectrBands
      if (staticS%operational == 1) then
        call getHRSolarIrradianceStatic(errS, staticS, globalS%wavelMRRadRetrS(iband), globalS%solarIrradianceRetrS(iband), &
                                        globalS%RRS_RingRetrS(iband),globalS%wavelHRRetrS(iband) )
      else
        call getHRSolarIrradiance(errS, globalS%wavelMRRadRetrS(iband), globalS%solarIrradianceRetrS(iband), &
                                  globalS%RRS_RingRetrS(iband),globalS%wavelHRRetrS(iband) )
      end if
      if (errorCheck(errS)) return
      call integrateSlitFunctionIrr(errS, globalS%controlRetrS, globalS%wavelHRRetrS(iband), globalS%wavelInstrIrrRetrS(iband), &
                                    globalS%solarIrradianceRetrS(iband), globalS%earthRadianceRetrS(iband),                     &
                                    globalS%RRS_RingRetrS(iband))
      if (errorCheck(errS)) return
    end do

    prev_time = current_time(current_time_values)
    ! get absorption coefficients for all wavelengths at the pressures used for the simulation
    do iband = 1, globalS%numSpectrBands
      do iTrace = 1, globalS%nTrace

        ! calculate expansion coefficients for the absorption cross section for
        ! each individual absorbing gas to be used in the retrieval
        if (  globalS%XsecHRLUTRetrS(iband,iTrace)%createXsecPolyLUT  ) then
          call createXsecLUT (errS, staticS, globalS%controlRetrS, globalS%wavelHRRetrS(iband),               &
                              globalS%weakAbsRetrS(iband), globalS%gasPTRetrS, globalS%traceGasRetrS(iTrace), &
                              globalS%XsecHRRetrS(iband,iTrace), globalS%XsecHRLUTRetrS(iband,iTrace))
          if (errorCheck(errS)) return
        end if ! createXsecPolyLUT

        if (globalS%controlRetrS%usePolyExpXsec ) then
        ! calculate the absorption cross section using the Xsec LUT if usePolyExpXsec is .true.
          call getAbsorptionXsecUsingLUT(errS, globalS%controlRetrS, globalS%wavelMRRadRetrS(iband),               &
                                         globalS%gasPTRetrS, globalS%traceGasRetrS(iTrace),                        &
                                         globalS%XsecHRRetrS(iband,iTrace), globalS%XsecHRLUTRetrS(iband,iTrace),  &
                                         globalS%wavelInstrRadRetrS(iband), globalS%solarIrradianceRetrS(iband),   &
                                         globalS%earthRadianceRetrS(iband), globalS%wavelHRRetrS(iband) )
          time = current_time(current_time_values)
          write(errS%temp,'(A,F14.3)') 'time for NOT calculating absorption coefficients retrieval (sec) = ', time - prev_time
        else
          ! calculate the absorption cross section without using the LUT, which involves reading
          ! absorption data from files
          call getAbsorptionXsec(errS, staticS,                                                                        &
                                 globalS%controlRetrS, globalS%weakAbsRetrS(iband), globalS%wavelMRRadRetrS(iband),    &
                                 globalS%gasPTRetrS, globalS%traceGasRetrS(iTrace), globalS%XsecHRRetrS(iband,iTrace), &
                                 globalS%wavelInstrRadRetrS(iband), globalS%solarIrradianceRetrS(iband),               &
                                 globalS%earthRadianceRetrS(iband), globalS%wavelHRRetrS(iband) )
          if (errorCheck(errS)) return
          time = current_time(current_time_values)
          write(errS%temp,'(A,F14.3)') 'time for calculating absorption coefficients retrieval (sec) = ', time - prev_time
        end if ! usePolyExpXsec

#ifdef WRITE_SPECTRA_TO_STDOUT
        if (globalS%controlRetrS%usePolyExpXsec) then
          write(*, '(A)') 'Using expansion coefficients'
        else
          write(*, '(A)') 'Using HITRAN directly'
        end if
        do i = 1, globalS%XsecHRRetrS(iband,iTrace)%nwavelHR
          do j = 0, globalS%XsecHRRetrS(iband,iTrace)%nalt
            write(*, '(I5,I5,I5,E15.7,E15.7,F14.7,E15.7)') iTrace, i, j, globalS%traceGasRetrS(iTrace)%pressure(j), &
                                                                         globalS%traceGasRetrS(iTrace)%temperature(j), &
                                                                         globalS%wavelHRRetrS(iband)%wavel(i), &
                                                                         globalS%XsecHRRetrS(iband,iTrace)%Xsec(i,j)
          end do
        end do
#endif
      end do
    end do

    if (staticS%operational == 1) then
      call logDebug(errS%temp)
    else
      call logInfo(errS%temp)
    end if

    ! read polarization correction data - it is assumed that the flag is the same for all bands
    if ( globalS%polCorrectionRetrS(1)%usePolarizationCorrection .and. ( staticS%operational /= 1 ) ) &
        call readPolarizationCorrectionFile(errS, globalS%numSpectrBands, globalS%polCorrectionRetrS,        &
                                            globalS%wavelInstrRadRetrS)
    if (errorCheck(errS)) return

    ! fill a-priori state vector, convariances, and the true state vector
    ! and initialize atmospheric - surface - cloud stray light parameters to a-priori values

    call fillAPriori(errS, globalS%numSpectrBands, globalS%nTrace, globalS%traceGasSimS, globalS%traceGasRetrS,      &
                     globalS%surfaceSimS, globalS%surfaceRetrS, globalS%LambCloudSimS, globalS%LambCloudRetrS,       &
                     globalS%cldAerFractionSimS, globalS%cldAerFractionRetrS, globalS%gasPTSimS, globalS%gasPTRetrS, &
                     globalS%earthRadMulOffsetSimS, globalS%earthRadMulOffsetRetrS, globalS%earthRadStrayLightSimS,  &
                     globalS%earthRadStrayLightRetrS, globalS%RRS_RingRetrS, globalS%cloudAerosolRTMgridSimS,        &
                     globalS%cloudAerosolRTMgridRetrS, globalS%retrS)
    if (errorCheck(errS)) return

    ! set initial (starting) values
    globalS%retrS%x               = globalS%retrS%xa
    globalS%retrS%xPrev           = globalS%retrS%xa
    globalS%retrS%dx              = 0.0d0
    globalS%retrS%SInv_lnvmr      = 0.0d0
    globalS%retrS%SInv_vmr        = 0.0d0
    globalS%retrS%SInv_ndens      = 0.0d0

    call exit('prepareOptimalEstimation')

  end subroutine prepareOptimalEstimation


  subroutine doOEIterationStep(errS, staticS, globalS)
    type(errorType), intent(inout) :: errS
    type(staticType), intent(in)   :: staticS
    type(globalType) :: globalS

    integer    :: istate, iband, iTrace, statusSplint, iwave
    real(8)    :: cf                                   ! used in polarization correction
    real(8)    :: distance_xprev_x_adj, distance_xprev_x_not_adj   ! used in reducing step size (boundary conditions)

    call enter('doOEIterationStep')

    if (staticS%operational == 1 .and. staticS%abortFlag == 1) then
        call MyStop(errS, 'Aborting')
        if (errorCheck(errS)) goto 99999
    end if

    ! get absorption coefficients again for all wavelengths at the altitudes for the retrieval grid
    ! when the temperature (profile) changes
    if ( ( globalS%iteration > 1 ) .and. ( globalS%gasPTRetrS%fitTemperatureProfile .or. &
                                           globalS%gasPTRetrS%fitTemperatureOffset ) ) then
      call logDebug('calculate absorption cross sections')
      do iband = 1, globalS%numSpectrBands
        do iTrace = 1, globalS%nTrace
          call getAbsorptionXsec(errS, staticS, &
                                 globalS%controlRetrS, globalS%weakAbsRetrS(iband), globalS%wavelHRRetrS(iband), &
                                 globalS%gasPTRetrS, globalS%traceGasRetrS(iTrace), globalS%XsecHRRetrS(iband,iTrace))
          if (errorCheck(errS)) goto 99999
        end do
      end do
    end if

    ! Calculate the reflectance (sun-normalized radiance) and derivatives for all spectral bands
    ! for the retrieved spectrum. Results are stored in globalS%reflDerivHRRetrS
    if ( globalS%controlRetrS%method == 0 ) then

      call calcReflAndDeriv(errS, globalS%maxFourierTermLUT, globalS%numSpectrBands, globalS%nTrace,                      &
                            globalS%wavelMRRadRetrS, globalS%wavelHRRetrS,                                                &
                            globalS%XsecHRRetrS, globalS%RRS_RingRetrS, globalS%geometryRetrS, globalS%retrS,             &
                            globalS%controlRetrS, globalS%cloudAerosolRTMgridRetrS, globalS%gasPTRetrS,                   &
                            globalS%traceGasRetrS, globalS%surfaceRetrS,                                                  &
                            globalS%LambCloudRetrS, globalS%mieAerRetrS, globalS%mieCldRetrS, globalS%createLUTRetrS,     &
                            globalS%cldAerFractionRetrS, globalS%earthRadMulOffsetRetrS,                                  &
                            globalS%earthRadStrayLightRetrS, globalS%solarIrradianceRetrS, globalS%optPropRTMGridRetrS,   &
                            globalS%dn_dnodeRetrS, globalS%reflDerivHRRetrS)
      if (errorCheck(errS)) goto 99999

    else
      globalS%nwavelRTM(:) = globalS%weakAbsRetrS(:)%degreePoly + 1
      globalS%use_abs_opt_thickn_for_AMF(:) =  globalS%weakAbsRetrS(:)%useAbsOptThcknForAMF
      call calcReflAndDerivDismas(errS, globalS%numSpectrBands, globalS%nTrace, globalS%nwavelRTM, globalS%RRS_RingSimS,      &
                                  globalS%use_abs_opt_thickn_for_AMF,                                                         &
                                  globalS%wavelHRRetrS, globalS%XsecHRRetrS, globalS%geometryRetrS,                           &
                                  globalS%retrS, globalS%controlRetrS, globalS%cloudAerosolRTMgridRetrS, globalS%gasPTRetrS,  &
                                  globalS%traceGasRetrS,  globalS%surfaceRetrS, globalS%LambCloudRetrS, globalS%mieAerRetrS,  &
                                  globalS%mieCldRetrS, globalS%cldAerFractionRetrS, globalS%earthRadMulOffsetRetrS,           &
                                  globalS%earthRadStrayLightRetrS, globalS%solarIrradianceRetrS, globalS%optPropRTMGridRetrS, &
                                  globalS%dn_dnodeRetrS, globalS%reflDerivHRRetrS)
      if (errorCheck(errS)) goto 99999
    end if

    do iband = 1, globalS%numSpectrBands
      call fillRadianceDerivativesHRgrid(errS, globalS%geometryRetrS, globalS%controlRetrS, globalS%reflDerivHRRetrS(iband),  &
                                         globalS%solarIrradianceRetrS(iband), globalS%earthRadianceRetrS(iband))
      if (errorCheck(errS)) goto 99999
    end do ! iband

    do iband = 1, globalS%numSpectrBands
      call integrateSlitFunctionRad(errS, globalS%controlRetrS, globalS%wavelHRRetrS(iband), globalS%wavelInstrRadRetrS(iband), &
                                    globalS%retrS, globalS%solarIrradianceRetrS(iband), globalS%earthRadianceRetrS(iband))
      if (errorCheck(errS)) goto 99999
    end do

    ! perform polarization correction or polarization + RRS correction
      do iband = 1, globalS%numSpectrBands
        if ( globalS%polCorrectionRetrS(iband)%usePolarizationCorrection ) then
          ! corection only for the reflectance, not for the derivatives
          if (staticS%operational == 1)                                                              &
            call interpolatePolCorrLUT(errS, globalS%polCorrectionRetrS(iband),                             &
                                       globalS%geometryRetrS, globalS%external_dataS%latitude,              &
                                       globalS%earthRadianceRetrS(iband), globalS%cloudAerosolRTMgridRetrS, &
                                       globalS%surfaceRetrS(iband), globalS%LambCloudRetrS(iband),          &
                                       globalS%cldAerFractionRetrS(iband), globalS%traceGasRetrS(1),        &
                                       globalS%wavelInstrRadRetrS(iband) )
          do iwave = 1, globalS%wavelInstrRadRetrS(iband)%nwavel
            cf = splint(errS, dble(globalS%polCorrectionRetrS(iband)%wavel), &
                 dble(globalS%polCorrectionRetrS(iband)%correction),         &
                 dble(globalS%polCorrectionRetrS(iband)%SDcorrection),       &
                 globalS%wavelInstrRadRetrS(iband)%wavel(iwave), statusSplint)
            globalS%earthRadianceRetrS(iband)%rad(1,iwave) = &
                globalS%earthRadianceRetrS(iband)%rad(1,iwave) / (1.0d0 - cf/100.0d0)
		! JdH Debug
            !write(intermediateFileUnit, '(f10.4,ES15.5, F10.4)') globalS%wavelInstrRadRetrS(iband)%wavel(iwave), &
            !                                      globalS%earthRadianceRetrS(iband)%rad(1,iwave), cf
          end do ! iwave
        end if ! usePolarizationCorrection
      end do ! iband

    do iband = 1, globalS%numSpectrBands
      if ( globalS%earthRadMulOffsetRetrS(iband)%fitMulOffset ) then
        call fillRadianceAtMulOffsetNodes (errS, globalS%wavelInstrRadRetrS(iband), globalS%earthRadianceRetrS(iband), &
                                           globalS%earthRadMulOffsetRetrS(iband) )
        if (errorCheck(errS)) goto 99999
        call addMulOffset(errS,  globalS%wavelInstrRadRetrS(iband), globalS%earthRadMulOffsetRetrS(iband),  &
                           globalS%earthRadianceRetrS(iband) )
        if (errorCheck(errS)) goto 99999
      else
        ! add a-priori stray light for retrieval only the first time
        if ( globalS%iteration == 1 ) then
          call addMulOffset(errS,  globalS%wavelInstrRadRetrS(iband), globalS%earthRadMulOffsetRetrS(iband), &
                             globalS%earthRadianceRetrS(iband) )
          if (errorCheck(errS)) goto 99999
        end if ! globalS%iteration == 1
      end if ! globalS%earthRadStrayLightRetrS(iband)%fitStrayLight
    end do ! iband


    do iband = 1, globalS%numSpectrBands
      if ( globalS%earthRadStrayLightRetrS(iband)%fitStrayLight ) then
        call fillRadianceAtStrayLightNodes (errS, globalS%wavelInstrRadRetrS(iband), globalS%earthRadianceRetrS(iband), &
                                            globalS%earthRadStrayLightRetrS(iband) )
        if (errorCheck(errS)) goto 99999
        call addStraylight(errS,  globalS%wavelInstrRadRetrS(iband), globalS%earthRadStrayLightRetrS(iband),  &
                            globalS%earthRadianceRetrS(iband) )
        if (errorCheck(errS)) goto 99999
      else
        ! add a-priori stray light for retrieval only the first time
        if ( globalS%iteration == 1 ) then
          call addStraylight(errS,  globalS%wavelInstrRadRetrS(iband), globalS%earthRadStrayLightRetrS(iband), &
                              globalS%earthRadianceRetrS(iband) )
          if (errorCheck(errS)) goto 99999
        end if ! globalS%iteration == 1
      end if ! globalS%earthRadStrayLightRetrS(iband)%fitStrayLight
    end do ! iband

    do iband = 1, globalS%numSpectrBands
      ! add a-priori Ring spectrum for retrieval only the first time
      if ( globalS%iteration == 1 ) then
        call addRingSpec(errS,  globalS%controlRetrS, globalS%wavelInstrRadRetrS(iband), globalS%RRS_RingRetrS(iband), &
                          globalS%solarIrradianceRetrS(iband), globalS%earthRadianceRetrS(iband))
        if (errorCheck(errS)) goto 99999
      end if ! globalS%iteration == 1
    end do ! iband

    do iband = 1, globalS%numSpectrBands
      ! set derivatives for the earth radiance w.r.t. the Ring coefficient
      call setRingDerivatives(errS, globalS%controlRetrS, globalS%nTrace, globalS%wavelInstrRadRetrS(iband), &
                              globalS%RRS_RingRetrS(iband), globalS%retrS,                             &
                              globalS%solarIrradianceRetrS(iband), globalS%earthRadianceRetrS(iband) )
      if (errorCheck(errS)) goto 99999
    end do ! iband

    ! fill modeled reflectance and derivatives in the retrieval structure
    call fillReflectanceRetr(errS, globalS%numSpectrBands, globalS%controlRetrS, globalS%wavelInstrRadRetrS, &
                             globalS%solarIrradianceRetrS, globalS%earthRadianceRetrS, globalS%retrS)
    if (errorCheck(errS)) goto 99999

    ! add a call to calculate the derivative numerically to test the derivatives for the retrieval
    ! print the semi-analytical derivatives and the numerical derivative side by side and their relative difference;
    ! after that stop
    if ( (globalS%iteration == 1) .and. globalS%controlRetrS%testderivatives ) then
      do iband = 1, globalS%numSpectrBands
        call testDerivatives (errS, globalS, iband)
        if (errorCheck(errS)) goto 99999
      end do
      call mystop(errS, 'stopped because test derivatives was selected - a special debugging mode')
      if (errorCheck(errS)) goto 99999
    end if

    ! update dR
    globalS%retrS%dR      = globalS%retrS%reflMeas - globalS%retrS%refl

    ! fill initial residue
    if ( globalS%iteration == 1 ) globalS%retrS%dR_initial = globalS%retrS%dR

    ! calculate new state vector and set convergence flag globalS%retrS%isConverged
    call calculateNewState(errS, globalS%iteration, globalS%retrS)
    if (errorCheck(errS)) goto 99999

    ! store not adjusted state vector in x_not_adj
     globalS%retrS%x_not_adj = globalS%retrS%x

    ! update other fit parameters than stray light
    call updateFitParameters(errS, globalS%numSpectrBands, globalS%nTrace, globalS%retrS, globalS%gasPTRetrS,             &
                             globalS%traceGasRetrS,  globalS%surfaceRetrS, globalS%LambCloudRetrS, globalS%mieAerRetrS,   &
                             globalS%mieCldRetrS,  globalS%cldAerFractionRetrS, globalS%earthRadMulOffsetRetrS,           &
                             globalS%earthRadStrayLightRetrS,  globalS%RRS_RingRetrS, globalS%cloudAerosolRTMgridRetrS,   &
                             globalS%controlRetrS )
    if (errorCheck(errS)) goto 99999

    ! The grid for the subcolumns may change when the cloud altitude changes or the temperature changes.
    if ( globalS%cloudAerosolRTMgridRetrS%fitIntervalDP  .or. &
         globalS%cloudAerosolRTMgridRetrS%fitIntervalTop .or. &
         globalS%cloudAerosolRTMgridRetrS%fitIntervalBot .or. &
         globalS%gasPTRetrS%fitTemperatureProfile        .or. &
         globalS%gasPTRetrS%fitTemperatureOffset ) then

      call fillAltitudeGridCol(errS, globalS%ncolumn, globalS%numSpectrBands, globalS%nTrace, globalS%cloudAerosolRTMgridRetrS, &
                               globalS%columnRetrS, globalS%gasPTRetrS, globalS%optPropRTMGridRetrS, globalS%reflDerivHRRetrS,  &
                               globalS%earthRadianceRetrS, globalS%retrS)
      if (errorCheck(errS)) goto 99999
    end if

    if ( globalS%iteration < 2 ) then
      ! use at least 2 globalS%iterations (0 and 1) if stray light is fitted and if DISMAS is used
      if ( (globalS%controlRetrS%method == 1) .or. (globalS%earthRadStrayLightRetrS(1)%fitStrayLight) ) then
        globalS%retrS%isConverged = .false.
      end if
    end if ! globalS%iteration < 2

    if ( globalS%retrS%isConverged .or. (globalS%iteration == globalS%retrS%maxNumIterations) ) goto 99999

    ! It may happen that the state vector is modified because of boundaries by subroutine
    ! updateFitParameters. That may happen several times and the state vector does not change.
    ! It seems useless to continue in that case and we have convergence at an boundary.

    ! first test whether the state vector is adjusted
    globalS%retrS%stateVectorAdjusted = .false.
    do istate = 1, globalS%retrS%nstate
      if ( abs(globalS%retrS%x_not_adj(istate) - globalS%retrS%x(istate)) > 1.0d-8 ) then
        globalS%retrS%stateVectorAdjusted = .true.
        exit ! exit loop over state vector elements
      end if
    end do

    if ( globalS%retrS%scale_delta_x_when_adj_x .and.  globalS%retrS%stateVectorAdjusted) then
      distance_xprev_x_adj     = 0.0
      distance_xprev_x_not_adj = 0.0
      do istate = 1, globalS%retrS%nstate
        distance_xprev_x_adj = distance_xprev_x_adj  &
                             + ( globalS%retrS%x(istate) - globalS%retrS%xPrev(istate) )**2
        distance_xprev_x_not_adj = distance_xprev_x_not_adj &
                                 + ( globalS%retrS%x_not_adj(istate) - globalS%retrS%xPrev(istate) )**2
      end do
      distance_xprev_x_adj     = sqrt(distance_xprev_x_adj)
      distance_xprev_x_not_adj = sqrt(distance_xprev_x_not_adj)
      ! perform scaling
      globalS%retrS%x(:) = globalS%retrS%xPrev(:) &
                         + distance_xprev_x_adj / distance_xprev_x_not_adj &
                         * (globalS%retrS%x_not_adj(:) - globalS%retrS%xPrev(:))
    end if ! scale_delta_x_when_adj_x

    globalS%retrS%xConvBoundary = dot_product( (globalS%retrS%x - globalS%retrS%xPrev), &
                                                matmul(globalS%retrS%SInv_lnvmr, (globalS%retrS%x - globalS%retrS%xPrev)) ) &
                                / globalS%retrS%nstate

    if (staticS%operational /= 1) write(intermediateFileUnit,'(A, F15.3)') 'xConvBoundary = ', globalS%retrS%xConvBoundary

    if ( (globalS%retrS%xConvBoundary  < globalS%retrS%xConvThreshold) .and. globalS%retrS%stateVectorAdjusted ) then
      if ( globalS%iteration > 1 ) then
        globalS%retrS%isConvergedBoundary = .true.
      else
        globalS%retrS%isConvergedBoundary = .false.
      end if
    else
      globalS%retrS%isConvergedBoundary = .false.
    end if

    if (staticS%operational /= 1) write(intermediateFileUnit,'(A,E15.5,500E17.7)') 'xConv and adj state vector = ', &
        globalS%retrS%xConv, globalS%retrS%x

    ! update values in the retrieval structure
    globalS%retrS%reflPrev = globalS%retrS%refl
    globalS%retrS%xPrev    = globalS%retrS%x
    globalS%retrS%dx       = globalS%retrS%x - globalS%retrS%xa

99999 continue
    call exit('doOEIterationStep')

  end subroutine doOEIterationStep


  subroutine doFullOptimalEstimation(errS, staticS, globalS)

    implicit none

    type(errorType),  intent(inout) :: errS
    type(staticType), intent(inout) :: staticS
    type(globalType) :: globalS

    ! prepare optimal estimation:
    ! fill altitude grid (sub)columns - get HR solar irradiance on retrieval wavelength grid
    ! calculate absorption coefficients for HR wavelength grid at retrieval nodes - read polarization correction
    ! calculate radiance at reference wavelength (for straylight fitting) - fill a-priori
    ! initialize starting values - fill error covariance matrix for noise

    ! local
    integer            :: counter          ! counts the number of trace gas profiles that are fitted
    integer            :: nl               ! number of Leendre coefficients used to estimate x_true
    integer, parameter :: n_threshold = 4  ! number of thresholds for for singular eigenvalues
    integer            :: iband, iTrace
    real(8)            :: threshold_w(n_threshold) ! thresholds for singular eigenvalues when inverting the averaging kernel
    integer            :: iiteration

    call enter('doFullOptimalEstimation')

    ! initialize
    globalS%retrS%isConvergedBoundary = .false.
    globalS%retrS%isConverged = .false.
    globalS%retrS%x_stored = 0.0d0

    call setFlagAAI(errS, globalS%numSpectrBands, globalS%surfaceRetrS, globalS%controlRetrS, &
                    globalS%RRS_RingRetrS, globalS%wavelInstrRadRetrS)
    if (errorCheck(errS)) return
    ! after testing for AAI test for ozone profile
    if ( .not. globalS%controlRetrS%allowNegativeSurfAlbedo ) then
      call setFlagOzoneProfile(errS, globalS%controlRetrS, globalS%nTrace, globalS%traceGasRetrS)
      if (errorCheck(errS)) return
    end if

    do iiteration = 1, globalS%retrS%maxNumIterations
      globalS%iteration = iiteration

      ! write header to additional output that we deal with results for simulation
      if (staticS%operational /= 1) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A,I4)') 'iteration = ', globalS%iteration
        write(addtionalOutputUnit,*)
      end if

      write(errS%temp,'(A,I4)') 'iteration = ',globalS%iteration
      if (staticS%operational == 1) then
        call logDebug(errS%temp)
      else
        call logInfo(errS%temp)
      end if

      globalS%retrS%numIterations = globalS%iteration

      ! perform iteration step:
      ! - calculate the reflectance + derivatives on the HR wavelength grid for the current state vector
      ! - calculate the radiance + derivatives on the instrument wavelength grid (conovlute with slit function)
      ! - calculate stray light + derivatives using the current state vector
      ! - calculate the sun-normalized radiance  + derivatives on the instrument grid
      ! - calculate new state vector and test convergence (if converged set flag)
      ! - update fit parameters so that they become consistent with the new state vector

      call doOEIterationStep(errS, staticS, globalS)
      if (errorCheck(errS)) return

      if ( globalS%retrS%isConverged .or. globalS%retrS%isConvergedBoundary ) exit  ! exit globalS%iteration loop

    end do ! globalS%iteration

    do iband = 1, globalS%numSpectrBands
      call calculateRTM_Ring_spectra(errS, globalS%controlRetrS, globalS%wavelInstrIrrRetrS(iband),          &
                                     globalS%solarIrradianceRetrS(iband), globalS%earthRadianceRetrS(iband), &
                                     globalS%RRS_RingRetrS(iband))
      if (errorCheck(errS)) return
    end do ! iband

    call calculateDiagnostics(errS, globalS%retrS, globalS%diagnosticS)
    if (errorCheck(errS)) return

    call updateCovFitParameters(errS, globalS%numSpectrBands, globalS%nTrace, globalS%retrS, globalS%diagnosticS, &
                                globalS%traceGasRetrS, globalS%surfaceRetrS, globalS%LambCloudRetrS,              &
                                globalS%cldAerFractionRetrS, globalS%gasPTRetrS, globalS%earthRadMulOffsetRetrS,  &
                                globalS%earthRadStrayLightRetrS, globalS%RRS_RingRetrS, globalS%cloudAerosolRTMgridRetrS)
    if (errorCheck(errS)) return

    call calculateAAI(errS, globalS%numSpectrBands, globalS%surfaceRetrS, globalS%controlRetrS, globalS%retrS)
    if (errorCheck(errS)) return

    if ( globalS%controlRetrS%writeSpecFeatIrr .or. &
         globalS%controlRetrS%writeSpecFeatRad .or. &
         globalS%controlRetrS%writeSpecFeatRefl )   &
      call evaluateSpectralFeatures(errS, globalS%numSpectrBands, globalS%nTrace, globalS%controlRetrS,                         &
                                    globalS%wavelInstrRadRetrS, globalS%cloudAerosolRTMgridRetrS, globalS%solarIrradianceRetrS, &
                                    globalS%earthRadianceRetrS,  globalS%retrS, globalS%diagnosticS, globalS%traceGasRetrS)
      if (errorCheck(errS)) return


    ! For more than one trace gas, e.g. NO2 and O2 A band, the dynamic range for the number density
    ! is so large that the calculation of the a-posteriori error covariance matrix using singular value
    ! decomposition can fail and diagonal elements of S become negative. This leads to a domain error
    ! when the square root is taken in order to calculate the error. The proper way to deal with this
    ! is to calculate S_lnvr first and then transform it to S_ndens so that the dynamic range is much smaller.
    ! As a temporary solution, the column properties are calculated for a single trace gas only.
    ! We also demand that the profile is retrieved because strange results occur if the column is fitted
    ! e.g. a-priori error for subcolumn is significantly smaller than the a-posterioir error, perhaps
    ! because Sa is not properly filled then.

    counter = 0
    do iTrace = 1, globalS%nTrace
      if ( globalS%traceGasRetrS(iTrace)%fitProfile ) counter = counter + 1
    end do
    ! calculate column properties and perform tests only if one profile is fitted
    ! because numerical problems arise for more than one profile (e.g. O3 and NO2)
    if ( counter == 1 ) then
      call calculateColumnPropertiesSim (errS, globalS%ncolumn, globalS%nTrace, globalS%optPropRTMGridSimS, globalS%columnSimS)
      if (errorCheck(errS)) return
      call calculateColumnPropertiesRetr(errS, globalS%ncolumn, globalS%nTrace, globalS%gasPTRetrS, globalS%traceGasRetrS, &
                                         globalS%optPropRTMGridRetrS, globalS%retrS, globalS%columnRetrS)
      if (errorCheck(errS)) return
      ! the lines below are used to investigate the smoothing error in profile retrieval
      ! the flag testUseAveragingKernel is specified in the configuration file section ADDITIONAL_OUTPUT
      if ( globalS%controlRetrS%testUseAveragingKernel ) then

        ! determine the smoothing error and the influence of non-profile terms in the averaging kernel
        call test_use_averaging_kernel(errS, globalS)
        if (errorCheck(errS)) return

        ! try to approximate the true profile by approximating it with a linear combination
        ! of Legendre polynomials
        do nl = 4, 8
          ! nl is the number of coefficients in front of the Legendre polynomials
          call estimate_x_true_fit_poly(errS, globalS, nl)
          if (errorCheck(errS)) return
        end do

        ! Try to approximate the true profile using the first few eigenvalues and eigenvectors
        ! of the averaging kernel using singular value decomposition, using different thresholds
        threshold_w(1) = 1.00d0
        threshold_w(2) = 0.10d0
        threshold_w(3) = 0.01d0
        threshold_w(4) = 0.001d0
        do nl = 1, n_threshold
          call x_true_from_few_singular_values(errS, globalS, threshold_w(nl))
          if (errorCheck(errS)) return
        end do

      end if ! globalS%controlRetrS%testUseAveragingKernel
    end if ! counter == 1

    call exit('doFullOptimalEstimation')

  end subroutine doFullOptimalEstimation

  subroutine fillWavelPressureGeometryLUT(errS, globalS)


    type(errorType),       intent(inout) :: errS
    type(globalType)                     :: globalS

    ! local
    integer  :: iFourier, iband, iwave, ipressure, imu

    call enter('fillWavelPressureGeometryLUT')

    do iband = 1, globalS%numSpectrBands
      do iFourier = 0, globalS%maxFourierTermLUT
		  ! wavelength
          do iwave = 1, globalS%createLUTSimS(iFourier,iband)%nwavel
            globalS%createLUTSimS(iFourier,iband)%wavel(iwave) = globalS%wavelInstrRadSimS(iband)%wavel(iwave)
          end do ! iwave
          do iwave = 1, globalS%createLUTRetrS(iFourier,iband)%nwavel
            globalS%createLUTRetrS(iFourier,iband)%wavel(iwave) = globalS%wavelInstrRadRetrS(iband)%wavel(iwave)
          end do ! iwave
          ! pressure
          do ipressure = 0, globalS%createLUTSimS(iFourier,iband)%npressure
            globalS%createLUTSimS(iFourier,iband)%pressure(ipressure) = &
            globalS%cloudAerosolRTMgridSimS%intervalBounds_P(ipressure)
          end do ! ipressure
          do ipressure = 0, globalS%createLUTRetrS(iFourier,iband)%npressure
            globalS%createLUTRetrS(iFourier,iband)%pressure(ipressure) = &
            globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(ipressure)
          end do ! ipressure
          ! geometry (mu = cos(theta) with theta the polar angle
          do imu = 1, globalS%createLUTSimS(iFourier,iband)%nmu
            globalS%createLUTSimS(iFourier,iband)%mu(imu) = &
            globalS%geometrySimS%u(imu)
          end do ! imu
          do imu = 1, globalS%createLUTRetrS(iFourier,iband)%nmu
            globalS%createLUTRetrS(iFourier,iband)%mu(imu) = &
            globalS%geometryRetrS%u(imu)
          end do ! imu
      end do ! iFourier
    end do ! iband

    call exit('fillWavelPressureGeometryLUT')

  end subroutine fillWavelPressureGeometryLUT


  subroutine setFlagAAI(errS, numSpectrBands, surfaceS, controlS, RRS_RingS, wavelInstrS)

    ! Set flag that AAI is calculated when several conditions are met.

    ! The flag is used for
    ! - calculating AAI and its variance
    ! - eliminating the test on the minimum value of the surface albedo
    ! - to control printing of the AAI

    type(errorType),       intent(inout) :: errS
    integer,               intent(in)    :: numSpectrBands
    type(LambertianType),  intent(in)    :: surfaceS(numSpectrBands)
    type(controlType),     intent(inout) :: controlS
    type(RRS_RingType),    intent(in)    :: RRS_RingS(numSpectrBands)
    type(wavelInstrType),  intent(inout) :: wavelInstrS(numSpectrBands)

    ! initialize
    controlS%calculateAAI = .false.

    if ( (wavelInstrS(1)%nwavel == 2) .and. (numSpectrBands == 1) ) then
      if ( ( wavelInstrS(1)%wavel(1) > 330.0 ) .and. ( wavelInstrS(1)%wavel(2) < 410.0 ) ) then
          if (  wavelInstrS(1)%wavel(2) > wavelInstrS(1)%wavel(1) ) then
            if ( surfaceS(1)%fitAlbedo ) then
              controlS%calculateAAI            = .true.
              controlS%allowNegativeSurfAlbedo = .true.
            end if
          end if
      end if
    end if

    if ( controlS%calculateAAI ) then
      if ( RRS_RingS(1)%useRRS .or. RRS_RingS(1)%approximateRRS ) then
        write(errS%temp,*)
        call errorAddLine(errS, errS%temp)
        call logDebug('ERROR in configuration file: RRS and calculate AAI are both .true.')
        call logDebug('Rotational Raman Scattering can not be taken into account when')
        call logDebug('calculating the AAI')
        call logDebug('set useRRS to 0 and approximateRRS to 0')
        call logDebug('in subsections simulation and retrieval')
        call logDebug('in SECTION RRS_RING')
        call MyStop(errS, 'RRS is not compatible with calculation of AAI')
        if (errorCheck(errS)) return
        write(errS%temp,*)
        call errorAddLine(errS, errS%temp)
      end if
    end if

  end subroutine setFlagAAI

  subroutine setFlagOzoneProfile(errS, controlS, nTrace, traceGasS)

    ! Set flag for ozone profile so that a negative surface albedo is allowed

    type(errorType),       intent(inout) :: errS
    integer,               intent(in)    :: nTrace
    type(controlType),     intent(inout) :: controlS
    type(traceGasType),    intent(inout) :: traceGasS(nTrace)

    ! Local
    integer :: iTrace

    ! initialize
    controlS%allowNegativeSurfAlbedo = .false.
    
    ! check that fitprofile is true and trace gas is ozone
    do iTrace = 1, nTrace
      if ( traceGasS(iTrace)%fitprofile .and. &
           trim(traceGasS(iTrace)%nameTraceGas) == 'O3' ) then
           controlS%allowNegativeSurfAlbedo = .true.
      end if
    end do ! iTrace

  end subroutine setFlagOzoneProfile

  subroutine calculateAAI(errS, numSpectrBands, surfaceRetrS, controlRetrS, retrS)

    ! Calculate the absorbing aerosol index (AAI) and its variance

    ! For the calculation of the error we use the SNR for the second wavelength and scale it
    ! using SNR(lambda) = SNR(lambda_0) * sqrt( R(lambda)/R(lambda_0) ) which holds for shot noise.
    ! Here lambda corresponds to index 0 and lambda_0 corresponds to index 1
    implicit none

    type(errorType),       intent(inout) :: errS
    integer,               intent(in)    :: numSpectrBands
    type(LambertianType),  intent(in)    :: surfaceRetrS(numSpectrBands)
    type(controlType),     intent(inout) :: controlRetrS
    type(retrType),        intent(inout) :: retrS

    if ( controlRetrS%calculateAAI ) then
      retrS%AAI = -100.0d0*log10( retrS%reflMeas(1) / retrS%refl(1) )
      retrS%varAAI =  1.0d4 * log(10.0d0)**2 * &
         (   (retrS%reflNoiseError(2) / retrS%reflMeas(2))**2  * ( retrS%reflMeas(2)/retrS%reflMeas(1) ) &
           + surfaceRetrS(1)%varianceAlbedo(1) * (retrS%K_vmr(1,1)/ retrS%refl(1))**2 )
    else
      retrS%AAI    = 0.0d0
      retrS%varAAI = 0.0d0
    end if

  end subroutine calculateAAI


  subroutine fillPolCorrection(errS, globalS)

    ! It is assumed that polarization (and RRS) is taken into account
    ! for the simulation and that in the retrieval those and not taken into
    ! account.

    implicit none

    type(errorType),  intent(inout) :: errS
    type(globalType) :: globalS

    ! local
    integer  :: iband, iwavel
    real(8)  :: reflSim,  reflRetr

    do iband = 1, globalS%numSpectrBands
      ! allocate memory space
      call freeMemPolCorrectionS(errS, globalS%polCorrectionRetrS(iband))
      ! initialize
      globalS%polCorrectionRetrS(iband)%nwavel           = globalS%wavelInstrRadRetrS(iband)%nwavel
      globalS%polCorrectionRetrS(iband)%nsurfaceAlbedo   = 1
      globalS%polCorrectionRetrS(iband)%nmu              = 1
      globalS%polCorrectionRetrS(iband)%nmu0             = 1
      globalS%polCorrectionRetrS(iband)%nozoneColumn     = 1
      globalS%polCorrectionRetrS(iband)%nsurfacePressure = 1
      globalS%polCorrectionRetrS(iband)%nlatitude        = 1
      ! perform allocation
      call claimMemPolCorrectionS(errS, globalS%polCorrectionRetrS(iband))
      if (errorCheck(errS)) return

      do iwavel = 1, globalS%wavelInstrRadRetrS(iband)%nwavel
        reflSim  = globalS%earthRadianceSimS(iband)%rad(1,iwavel) &
                 / globalS%solarIrradianceSimS(iband)%solIrr(iwavel)
        reflRetr = globalS%earthRadianceRetrS(iband)%rad(1,iwavel) &
                 / globalS%solarIrradianceRetrS(iband)%solIrr(iwavel)
         globalS%polCorrectionRetrS(iband)%wavel(iwavel) = globalS%wavelInstrRadRetrS(iband)%wavel(iwavel)
         globalS%polCorrectionRetrS(iband)%correction(iwavel) = 100.0d0 * (reflSim - reflRetr) / reflSim !in percent
      end do
    end do ! iband

  end subroutine fillPolCorrection


  subroutine interpolatePolCorrLUT(errS, polCorrectionS, geometryS, latitude, earthRadianceS,   &
                                   cloudAerosolRTMgridS, surfaceS, LambCloudS, cldAerFractionS, &
                                   traceGasS, wavelInstrS)

    implicit none

    type(errorType),               intent(inout) :: errS
    type(polCorrectionType),       intent(inout) :: polCorrectionS
    type(geometryType),            intent(in)    :: geometryS
    real(8),                       intent(in)    :: latitude
    type(earthRadianceType),       intent(in)    :: earthRadianceS
    type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS
    type(LambertianType),          intent(in)    :: surfaceS
    type(LambertianType),          intent(in)    :: LambCloudS
    type(cldAerFractionType),      intent(in)    :: cldAerFractionS
    type(traceGasType),            intent(in)    :: traceGasS
    type(wavelInstrType),          intent(in)    :: wavelInstrS

    ! local
    integer :: iwavel, status
    real    :: mu, mu0, dphi, lat, O3col, O3colAboveCld, surfAlbedo, cldAlbedo, surfPressure, cldPressure
    real    :: cldFraction, wavel, radCloud, radClear, val0, val1, val2, valClear, valCloud
    ! checks
    if (trim(traceGasS%nameTraceGas) /= 'O3') then
        write(errS%temp,*)
        call errorAddLine(errS, errS%temp)
        write(errS%temp,*) 'ERROR in configuration file: firat trace gas is not O3'
        call errorAddLine(errS, errS%temp)
        write(errS%temp,*) 'polarization + RRS LUT has been made for ozone'
        call errorAddLine(errS, errS%temp)
        write(errS%temp,*) 'calculating the AAI'
        call errorAddLine(errS, errS%temp)
        call MyStop(errS, 'incorrect trace gas')
        if (errorCheck(errS)) return
        write(errS%temp,*)
        call errorAddLine(errS, errS%temp)
    end if

    ! local assignments
    mu            = real(geometryS%uu)
    mu0           = real(geometryS%u0)
    dphi          = real(geometryS%dphiRad)  ! dphi is in radians
    lat           = real(latitude)
    O3col         = real(traceGasS%column)
    O3colAboveCld = real(traceGasS%columnAboveCloud)
    surfPressure  = real(surfaceS%pressure)
    cldPressure   = real(cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%numIntervalFit))

    do iwavel = 1, wavelInstrS%nwavel

      wavel = real(wavelInstrS%wavel(iwavel))

      ! set cloud fraction
      if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
        cldFraction = real(cloudAerosolRTMgridS%cldAerFractionAllBands)
      else
        cldFraction = real(polyInt(errS, cldAerFractionS%wavelCldAerFraction, &
                           cldAerFractionS%cldAerFraction, wavelInstrS%wavel(iwavel)))
      end if

      ! set surface albedo
      if ( cloudAerosolRTMgridS%useAlbedoLambSurfAllBands) then
        surfAlbedo = real(cloudAerosolRTMgridS%albedoLambSurfAllBands)
      else
        surfAlbedo = real(polyInt(errS, surfaceS%wavelAlbedo, surfaceS%albedo, wavelInstrS%wavel(iwavel)))
      end if

      ! set cloud albedo
      if ( cloudAerosolRTMgridS%useAlbedoLambCldAllBands) then
        cldAlbedo = real(cloudAerosolRTMgridS%albedoLambCldAllBands)
      else
        cldAlbedo = real(polyInt(errS, LambCloudS%wavelAlbedo, LambCloudS%albedo, wavelInstrS%wavel(iwavel)))
      end if

      ! radiance for the clear and cloudy part of the pixel
      radCloud   = real(earthRadianceS%rad_cld(1,iwavel))
      radClear   = real(earthRadianceS%rad_clr(1,iwavel))

      ! cloud free part m = 0
      call interpol6D( errS, &
         polCorrectionS%nsurfaceAlbedo, polCorrectionS%nmu, polCorrectionS%nmu0,                 &
         polCorrectionS%nozoneColumn, polCorrectionS%nsurfacePressure, polCorrectionS%nlatitude, &
         polCorrectionS%surfaceAlbedo, polCorrectionS%mu, polCorrectionS%mu0,                    &
         polCorrectionS%ozoneColumn, polCorrectionS%surfacePressure, polCorrectionS%latitude,    &
         polCorrectionS%correctionLUT0(:,:,:,:,:,:,iwavel),                                      &
         surfAlbedo, mu, mu0, O3col, surfPressure, lat,                                          &
         val0, status)

      ! cloud free part m = 1
      call interpol6D( errS, &
         polCorrectionS%nsurfaceAlbedo, polCorrectionS%nmu, polCorrectionS%nmu0,                 &
         polCorrectionS%nozoneColumn, polCorrectionS%nsurfacePressure, polCorrectionS%nlatitude, &
         polCorrectionS%surfaceAlbedo, polCorrectionS%mu, polCorrectionS%mu0,                    &
         polCorrectionS%ozoneColumn, polCorrectionS%surfacePressure, polCorrectionS%latitude,    &
         polCorrectionS%correctionLUT1(:,:,:,:,:,:,iwavel),                                      &
         surfAlbedo, mu, mu0, O3col, surfPressure, lat,                                          &
         val1, status)

      ! cloud free part m = 2
      call interpol6D( errS, &
         polCorrectionS%nsurfaceAlbedo, polCorrectionS%nmu, polCorrectionS%nmu0,                 &
         polCorrectionS%nozoneColumn, polCorrectionS%nsurfacePressure, polCorrectionS%nlatitude, &
         polCorrectionS%surfaceAlbedo, polCorrectionS%mu, polCorrectionS%mu0,                    &
         polCorrectionS%ozoneColumn, polCorrectionS%surfacePressure, polCorrectionS%latitude,    &
         polCorrectionS%correctionLUT2(:,:,:,:,:,:,iwavel),                                      &
         surfAlbedo, mu, mu0, O3col, surfPressure, lat,                                          &
         val2, status)

      valClear = val0 + 2.0d0 * val1 * cos(dphi) + 2.0d0 * val2 * cos(2.0d0 * dphi)


      ! cloudy part m = 0
      call interpol6D( errS, &
         polCorrectionS%nsurfaceAlbedo, polCorrectionS%nmu, polCorrectionS%nmu0,                 &
         polCorrectionS%nozoneColumn, polCorrectionS%nsurfacePressure, polCorrectionS%nlatitude, &
         polCorrectionS%surfaceAlbedo, polCorrectionS%mu, polCorrectionS%mu0,                    &
         polCorrectionS%ozoneColumn, polCorrectionS%surfacePressure, polCorrectionS%latitude,    &
         polCorrectionS%correctionLUT1(:,:,:,:,:,:,iwavel),                                      &
         cldAlbedo, mu, mu0, O3colAboveCld, cldPressure, lat,                                    &
         val0, status)

      ! cloudy part m = 1
      call interpol6D( errS, &
         polCorrectionS%nsurfaceAlbedo, polCorrectionS%nmu, polCorrectionS%nmu0,                 &
         polCorrectionS%nozoneColumn, polCorrectionS%nsurfacePressure, polCorrectionS%nlatitude, &
         polCorrectionS%surfaceAlbedo, polCorrectionS%mu, polCorrectionS%mu0,                    &
         polCorrectionS%ozoneColumn, polCorrectionS%surfacePressure, polCorrectionS%latitude,    &
         polCorrectionS%correctionLUT1(:,:,:,:,:,:,iwavel),                                      &
         cldAlbedo, mu, mu0, O3colAboveCld, cldPressure, lat,                                    &
         val1, status)

      ! cloudy part m = 2
      call interpol6D( errS, &
         polCorrectionS%nsurfaceAlbedo, polCorrectionS%nmu, polCorrectionS%nmu0,                 &
         polCorrectionS%nozoneColumn, polCorrectionS%nsurfacePressure, polCorrectionS%nlatitude, &
         polCorrectionS%surfaceAlbedo, polCorrectionS%mu, polCorrectionS%mu0,                    &
         polCorrectionS%ozoneColumn, polCorrectionS%surfacePressure, polCorrectionS%latitude,    &
         polCorrectionS%correctionLUT2(:,:,:,:,:,:,iwavel),                                      &
         cldAlbedo, mu, mu0, O3colAboveCld, cldPressure, lat,                                    &
         val2, status)

      valCloud = val0 + 2.0d0 * val1 * cos(dphi) + 2.0d0 * val2 * cos(2.0d0 * dphi)

      polCorrectionS%correction(iwavel) = (radClear*valClear + radCloud*valCloud)/(radClear + radCloud)
    end do ! iwavel

  end subroutine interpolatePolCorrLUT

  subroutine testChandrasekhar(errS, numSpectrBands, wavelInstrRadSimS, solarIrradianceSimS, earthRadianceSimS, &
                               geometrySimS)

    ! This subroutine is called when the flag testChandrasekhar is set in the configuration file, section
    ! additional output.

    implicit none

    type(errorType),         intent(inout) :: errS
    integer,                 intent(in)    :: numSpectrBands
    type(wavelInstrType),    intent(in)    :: wavelInstrRadSimS(numSpectrBands)
    type(SolarIrrType),      intent(in)    :: solarIrradianceSimS(numSpectrBands)
    type(earthRadianceType), intent(in)    :: earthRadianceSimS(numSpectrBands)
    type(geometryType),      intent(in)    :: geometrySimS

    ! Note that 3 spectal bands must be present, covering the same wavelength
    ! interval, but differing in the surface albedo. The surface albedo values
    ! must be 0.0, 0.5, and 1.0. They are tested when veryfying the configuration file.

    ! local

    real(8), parameter :: PI = 3.141592653589793d0

    integer   :: iwave
    real(8)   :: sstar( wavelInstrRadSimS(1)%nwavel )    ! spherical albedo for illumination at bottom
                 ! T = [exp(-tau/mu) + td(mu)] [exp(-tau/mu0) + td(mu0)]; assuming tdstar = td
    real(8)   :: T( wavelInstrRadSimS(1)%nwavel )
    real(8)   :: R0, R05, R1
    real(8)   :: R1minR0, R0minR05
    real(8)   :: R025, R050, R075

    ! Calculate  sstar and T
    do iwave = 1, wavelInstrRadSimS(1)%nwavel
      R0  = PI * earthRadianceSimS(1)%rad_meas(1,iwave) / solarIrradianceSimS(1)%solIrr(iwave) / geometrySimS%u0
      R05 = PI * earthRadianceSimS(2)%rad_meas(1,iwave) / solarIrradianceSimS(1)%solIrr(iwave) / geometrySimS%u0
      R1  = PI * earthRadianceSimS(3)%rad_meas(1,iwave) / solarIrradianceSimS(1)%solIrr(iwave) / geometrySimS%u0

      R1minR0  = R1 - R0
      R0minR05 = R0 -R05

      sstar(iwave) = ( 1.0d0 + 2.0d0 * R0minR05/ R1minR0 ) / ( 1.0d0 + R0minR05/ R1minR0 )
      t( iwave )   = sqrt( ( 1.0d0 - sstar(iwave) ) * R1minR0 )
    end do ! iwave

    ! write to additional output
    write(addtionalOutputUnit,*)
    write(addtionalOutputUnit,'(A)') ' wavelength       R0         T         sstar        R025        R050        R05         R075'
    do iwave = 1, wavelInstrRadSimS(1)%nwavel
      R0   = PI * earthRadianceSimS(1)%rad_meas(1,iwave) / solarIrradianceSimS(1)%solIrr(iwave) / geometrySimS%u0
      R05  = PI * earthRadianceSimS(2)%rad_meas(1,iwave) / solarIrradianceSimS(1)%solIrr(iwave) / geometrySimS%u0
      R025 = R0 + 0.25d0 * t( iwave ) * t( iwave ) / ( 1.0d0 - 0.25d0 * sstar(iwave) )
      R050 = R0 + 0.50d0 * t( iwave ) * t( iwave ) / ( 1.0d0 - 0.50d0 * sstar(iwave) )
      R075 = R0 + 0.75d0 * t( iwave ) * t( iwave ) / ( 1.0d0 - 0.75d0 * sstar(iwave) )
      write(addtionalOutputUnit,'(8F12.8)')  wavelInstrRadSimS(1)%wavel(iwave), R0, T( iwave ), sstar(iwave), &
                                        R025, R050, R05, R075
    end do ! iwave

  end subroutine testChandrasekhar


  subroutine test_use_averaging_kernel(errS, globalS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS

    ! test the use of x_retr = x_a + A * ( x_true - x_a )

    ! Note that the operation A * ( x_true - x_a ) involves an numerical integration
    ! because we do not work with properties of individual layers.
    ! Therefore we have to work with the 'column properties'

    ! The length of the arrays are given by globalS%retrS%nstateCol = globalS%nTrace * nGaussCol + nFit_other
    ! where nFit_other is the numer of fit parameters other than those pertaining to profile retrieval
    !       nGaussCol is the number of Gaussian division points for the altitude in the atmosphere

    ! globalS%retrS%A_ndensCol(nstateCol, nstateCol)  is the averaging kernel on column grid
    ! globalS%optPropRTMGridSimS%Colndens(nGaussCol,globalS%nTrace) true (simulated) number density on column grid (molecules cm-3)
    ! globalS%optPropRTMGridRetrS%Colndens(nGaussCol,globalS%nTrace) retrieved number density on column grid (molecules cm-3)
    ! globalS%optPropRTMGridRetrS%ColndensAP(nGaussCol,globalS%nTrace) a priori number density on column grid (molecules cm-3)
    ! globalS%optPropRTMGridRetrS%Colaltitude(nGaussCol) altitudes (km) of column grid
    ! To find the values of x_true and x_a for the other fit parameters (i.e. not pertaining to a trace gas)
    ! use select case and ignore the nodeTrace cases.

    ! local
    integer :: ialt, jalt, startValue, endValue, iTrace, istate
    real(8) :: Colndens_estimated_reduced(globalS%retrS%nGaussCol,globalS%nTrace)  ! number density estimated from
                                                                   ! x_retr = x_a + A*(x_true-x_a)
                                                                   ! only profile part of A
    real(8) :: Colndens_estimated_full   (globalS%retrS%nGaussCol,globalS%nTrace)  ! number density estimated from
                                                                   ! x_retr = x_a + A*(x_true-x_a)
                                                                   ! full averaging kernel used
    real(8) :: diffAP(globalS%retrS%nGaussCol)                             ! x_true - x_a

    logical, parameter :: verbose = .false.

    ! initialize
    Colndens_estimated_reduced = 0.0d0

    do iTrace = 1, globalS%nTrace

      startValue = (iTrace - 1) * globalS%retrS%nGaussCol
      endValue   =  iTrace      * globalS%retrS%nGaussCol

      diffAP(:) = (globalS%optPropRTMGridSimS%Colndens(:,iTrace) - globalS%optPropRTMGridRetrS%ColndensAP(:,iTrace) )

      do ialt = 1, globalS%retrS%nGaussCol

        Colndens_estimated_reduced(ialt, iTrace) = globalS%optPropRTMGridRetrS%ColndensAP(ialt,iTrace) + &
               dot_product( globalS%retrS%A_ndensCol(startValue+ialt, startValue+1 : endValue), diffAP(:) )

        Colndens_estimated_full(ialt, iTrace) = Colndens_estimated_reduced(ialt, iTrace)

        do istate = 1, globalS%retrS%nstate

          select case (globalS%retrS%codeFitParameters(istate))

            case( 'columglobalS%nTrace','surfAlbedo', 'surfEmission', 'LambCldAlbedo', 'straylight', 'aerosolTau',  &
                  'aerosolSSA', 'aerosolAC', 'nodeTemp', 'offsetTemp', 'surfPressure', 'cloudFraction',     &
                  'cloudTau', 'cloudAC', 'intervalDP', 'intervalTop', 'intervalBot' )

              Colndens_estimated_full(ialt, iTrace) = Colndens_estimated_reduced(ialt, iTrace) + &
                globalS%retrS%A_ndensCol(startValue+ialt, istate) * ( globalS%retrS%x(istate) - globalS%retrS%xa(istate) )

          end select

        end do ! istate

      end do ! ialt

      ! write to output
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') 'estimate x_estimated = xa + A * ( x_true - xa ) on column grid'
      write(addtionalOutputUnit,'(A)') 'unit      = molecules cm-3'
      write(addtionalOutputUnit,'(A)') 'trace gas = ', globalS%traceGasRetrS(iTrace)%nameTraceGas
      write(addtionalOutputUnit,'(A)') &
       'altitude(km)   x_retrieved    x_estimated_reduced   x_estimated_full       x_true        xa'
      do ialt = 1, globalS%retrS%nGaussCol
        write(addtionalOutputUnit,'(F10.4, 5ES18.5, F10.7)') globalS%optPropRTMGridRetrS%Colaltitude(ialt), &
                                                       globalS%optPropRTMGridRetrS%Colndens  (ialt,iTrace), &
                                                       Colndens_estimated_reduced    (ialt,iTrace), &
                                                       Colndens_estimated_full       (ialt,iTrace), &
                                                       globalS%optPropRTMGridSimS%Colndens   (ialt,iTrace), &
                                                       globalS%optPropRTMGridRetrS%ColndensAP(ialt,iTrace)
      end do ! ialt

      write(addtionalOutputUnit,'(A)') 'altitude(km)   100(retr - estimFULL)/true     100(retr - estim_reduced)/true'
      do ialt = 1, globalS%retrS%nGaussCol
        write(addtionalOutputUnit,'(F10.4, 2F30.12)') globalS%optPropRTMGridRetrS%Colaltitude(ialt),               &
            100.0d0*(globalS%optPropRTMGridRetrS%Colndens(ialt,iTrace) - Colndens_estimated_full(ialt,iTrace) )    &
                    / globalS%optPropRTMGridSimS%Colndens(ialt,iTrace),                                            &
            100.0d0*(globalS%optPropRTMGridRetrS%Colndens(ialt,iTrace) - Colndens_estimated_reduced(ialt,iTrace) ) &
                    / globalS%optPropRTMGridSimS%Colndens(ialt,iTrace)
      end do ! ialt

      if ( verbose) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'averaging kernel on column grid'
        write(addtionalOutputUnit,*)
        do ialt = 1, globalS%retrS%nstateCol
          write(addtionalOutputUnit,'(200ES15.5)') ( globalS%retrS%A_ndensCol(ialt, jalt), jalt = 1, globalS%retrS%nstateCol )
        end do ! ialt
      end if ! verbose

    end do ! iTrace

  end subroutine test_use_averaging_kernel


  subroutine estimate_x_true_fit_poly(errS, globalS, nl)

    ! use of x_retr = xa + A * ( x_true - xa )

    ! replace x_true / xa by a linear combination of Legendre functions
    ! P(z) = a0*p0(z) + a1*p1(z) + ... with pk Legendre functions

    ! This yields the system of equations
    ! x_retr(i) - xa(i) + sum_j[A(i,j)*xa(j)] = sum_k[ ak * (sum_j[A(i,j)*pk(j)*xa(j))]
    ! which can be written as Ba = b
    ! b(i) = x_retr(i) - xa(i) + sum_j[A(i,j)*xa(j)]
    ! a = (a0, a1, ..., an)
    ! B(i,k) = sum_j[A(i,j)*pk(j)*xa(j))]
    ! k = 1,2,...,n
    ! i,j = 1,2,3,..., globalS%retrS%nGaussCol are indices for the Gaussian altitude grid

    ! We need the least squares solution and write CT C a = CT b
    ! where CT is the transposed of C and C(i,k) = B(i,k) / sig(i)
    ! where sig(i) is the error in the retrieved ozone profile

    ! the system of equations CT C a = CT b is solved for a by using singular value decomposition

    ! globalS%retrS%A_ndensCol(nstateCol, nstateCol)  is the averaging kernel on column grid
    ! globalS%optPropRTMGridSimS%Colndens(nGaussCol,globalS%nTrace) true (simulated) number density on column grid (molecules cm-3)
    ! globalS%optPropRTMGridRetrS%Colndens(nGaussCol,globalS%nTrace) retrieved number density on column grid (molecules cm-3)
    ! globalS%optPropRTMGridRetrS%ColndensAP(nGaussCol,globalS%nTrace) a priori number density on column grid (molecules cm-3)
    ! globalS%optPropRTMGridRetrS%Colaltitude(nGaussCol) altitudes (km) of column grid
    ! To find the values of x_true and x_a for the other fit parameters (i.e. not pertaining to a trace gas)
    ! use select case and ignore the nodeTrace cases.

    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS
    integer, intent(in) :: nl

    ! local
    integer  :: ialt, jalt, k, iTrace
    integer  :: startValue, endValue
    real(8)  :: Colndens_true(globalS%retrS%nGaussCol)         ! number density estimated from
                                                       ! x_retr = x_a + A*(x_true-xa)
    real(8)  :: sig(globalS%retrS%nGaussCol)                   ! error in the retrieved number density
                                                       ! divided by the a priori number density
    real(8)  :: alt(globalS%retrS%nGaussCol)                   ! altitudes where the number density is retrieved
    real(8)  :: altScaled(globalS%retrS%nGaussCol)             ! altitude scaled to (-1,+1)
    real(8)  :: ak(globalS%retrS%nGaussCol,globalS%retrS%nGaussCol)    ! averaging kernel
    real(8)  :: xa(globalS%retrS%nGaussCol)                    ! a priori profile
    real(8)  :: xr(globalS%retrS%nGaussCol)                    ! retrieved profile
    real(8)  :: xt(globalS%retrS%nGaussCol)                    ! true profile

    real(8)  :: b(globalS%retrS%nGaussCol)                     ! x_retr(i) - xa(i) + A * xa
    real(8)  :: C(globalS%retrS%nGaussCol,nl)                  ! sum_j[A(i,j)*pk(j)*xa(j))] / sig(i)
    real(8)  :: CT(nl,globalS%retrS%nGaussCol)                 ! transpose of C
    real(8)  :: D(nl,nl)                               ! D = CT C
    real(8)  :: e(nl)                                  ! e = CT b

    ! arrays for singular value decomposition of the averaging kernel
    real(8)  :: u(nl, nl)
    real(8)  :: v(nl, nl)
    real(8)  :: w(nl)

    ! coefficients
    real(8)  :: a(nl)                   ! coefficients
    real(8)  :: pk(globalS%retrS%nGaussCol,nl)  ! legendre functions

    ! initialize
    Colndens_true = 0.0d0
    alt           = globalS%optPropRTMGridRetrS%Colaltitude
    altScaled     = 2.0d0 * (alt(:) - alt(1)) / ( alt(globalS%retrS%nGaussCol) - alt(1) ) - 1.0d0

    ! calculate Legendre functions
    do ialt = 1, globalS%retrS%nGaussCol
      pk(ialt,:) = fleg(altScaled(ialt),nl)
    end do

    do iTrace = 1, globalS%nTrace

      startValue = (iTrace - 1) * globalS%retrS%nGaussCol
      endValue   =  iTrace      * globalS%retrS%nGaussCol

      ! fill sig
      do ialt = 1, globalS%retrS%nGaussCol
        sig(ialt) = sqrt( globalS%retrS%S_ndensCol(startValue+ialt,startValue+ialt) )
      end do ! ialt

      ! fill the averaging kernel ak for the current trace gas
      ak(:,:) = globalS%retrS%A_ndensCol(startValue+1 : endValue, startValue+1 : endValue)
      xr(:)   = globalS%optPropRTMGridRetrS%Colndens  (:,iTrace)
      xa(:)   = globalS%optPropRTMGridRetrS%ColndensAP(:,iTrace)
      xt(:)   = globalS%optPropRTMGridSimS%Colndens   (:,iTrace)

      b(:) = xr(:) - xa(:) + matmul(ak, xa)
      do ialt = 1, globalS%retrS%nGaussCol
        do k = 1, nl
          C(ialt,k) = 0.0d0
          do jalt = 1, globalS%retrS%nGaussCol
            C(ialt,k) = C(ialt,k) + ak(ialt,jalt) * pk(jalt,k) * xa(jalt)
          end do ! jalt
          ! C(ialt,k) = C(ialt,k) / sig(ialt)
        end do ! k
      end do ! ialt
      CT = transpose(C)
      D  = matmul(cT,C)
      e  = matmul(CT,b)

      ! perform singular value decomposition
      ! ak is overwritten with u
      u = D
      call svdcmp(errS, u,w,v,nl)
      if (errorCheck(errS)) return
      call svbksb(errS, u,w,v,e,a)
      if (errorCheck(errS)) return

      Colndens_true = 0.0d0
      do k = 1, nl
        Colndens_true = Colndens_true + a(k) * pk(:,k) * xa(:)
      end do

      ! write to additional output
      if (staticS%operational /= 1) then
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) ' coefficients for the Legendre functions'
      do k = 1, nl
        write(addtionalOutputUnit,'(I4, ES18.5)') k, a(k)
      end do
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') 'true profile is estimated by expanding the true profile'
      write(addtionalOutputUnit,'(A)') 'in a set of Legendre functions and fitting the coefficients'
      write(addtionalOutputUnit,'(A)') 'demand that x_retr = xa + A * (x_true - x_a)'
      write(addtionalOutputUnit,'(A)') 'trace gas = ', globalS%traceGasRetrS(iTrace)%nameTraceGas
      write(addtionalOutputUnit,'(A)') 'altitude(km)   x_retrieved      x_true_fitted        x_true        xa'
      do ialt = 1, globalS%retrS%nGaussCol
        write(addtionalOutputUnit,'(F10.4, 4ES18.5)') alt(ialt), xr(ialt), Colndens_true(ialt), xt(ialt), xa(ialt)
      end do ! ialt

      write(addtionalOutputUnit,'(A)') 'relative update of xa when fit  is used: 100*(xt_fitted - xa)/xa'
      write(addtionalOutputUnit,'(A)') 'relative update of xa when xr   is used: 100*(xr        - xa)/xa'
      write(addtionalOutputUnit,'(A)') 'relative update of xa should be:         100*(xt        - xa)/xa'
      write(addtionalOutputUnit,'(A)')
      write(addtionalOutputUnit,'(A)') 'altitude(km)  100*(xt_fitted - xa)/xa   100*(xr - xa)/xa   100*(xt - xa)/xa)'
      do ialt = 1, globalS%retrS%nGaussCol
        write(addtionalOutputUnit,'(F10.4, 3F20.12)') alt(ialt),                        &
                                  100.0d0*(Colndens_true(ialt) - xa(ialt) ) / xa(ialt), &
                                  100.0d0*(xr(ialt)            - xa(ialt) ) / xa(ialt), &
                                  100.0d0*(xt(ialt)            - xa(ialt) ) / xa(ialt)
      end do ! ialt
      end if

    end do ! iTrace

  end subroutine estimate_x_true_fit_poly


  subroutine x_true_from_few_singular_values(errS, globalS, threshold_w)

    ! use of x_retr = xa + A * ( x_true - xa )

    ! calculate the the inverse of A keeping only the first nl singular eigenvalues
    ! and solve the system of equations for x_true

    ! singular value decomposition (SVD) is used

    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS
    real(8),    intent(in) :: threshold_w ! threshold for singular eigenvalues

    ! local
    integer  :: ialt, iTrace
    integer  :: startValue, endValue
    real(8)  :: Colndens_true(globalS%retrS%nGaussCol)         ! number density estimated from
                                                       ! x_retr = x_a + A*(x_true-xa)
    real(8)  :: alt(globalS%retrS%nGaussCol)                   ! altitudes where the number density is retrieved
    real(8)  :: ak(globalS%retrS%nGaussCol,globalS%retrS%nGaussCol)    ! averaging kernel
    real(8)  :: xa(globalS%retrS%nGaussCol)                    ! a priori profile
    real(8)  :: xr(globalS%retrS%nGaussCol)                    ! retrieved profile
    real(8)  :: xt(globalS%retrS%nGaussCol)                    ! true profile

    ! arrays for singular value decomposition of the averaging kernel
    real(8)  :: u(globalS%retrS%nGaussCol, globalS%retrS%nGaussCol)
    real(8)  :: v(globalS%retrS%nGaussCol, globalS%retrS%nGaussCol)
    real(8)  :: w(globalS%retrS%nGaussCol)

    ! initialize
    alt           = globalS%optPropRTMGridRetrS%Colaltitude

    do iTrace = 1, globalS%nTrace

      startValue = (iTrace - 1) * globalS%retrS%nGaussCol
      endValue   =  iTrace      * globalS%retrS%nGaussCol

      ! fill the averaging kernel ak for the current trace gas
      ak(:,:) = globalS%retrS%A_ndensCol(startValue+1 : endValue, startValue+1 : endValue)
      xr(:)   = globalS%optPropRTMGridRetrS%Colndens(:,iTrace)
      xa(:)   = globalS%optPropRTMGridRetrS%ColndensAP(:,iTrace)
      xt(:)   = globalS%optPropRTMGridSimS%Colndens(:,iTrace)

      ! perform singular value decomposition
      ! ak is overwritten with u
      u = ak
      call svdcmp(errS, u,w,v,globalS%retrS%nGaussCol)
      if (errorCheck(errS)) return

      where ( abs(w) < threshold_w ) w = 0.0d0

      call svbksb(errS, u,w,v,xr-xa,Colndens_true)
      if (errorCheck(errS)) return
      Colndens_true = Colndens_true + xa

      ! write to additional output
      if (staticS%operational /= 1) then
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) ' singular eigenvalues of the averaging kernel that are used'
      write(addtionalOutputUnit,'(A,F10.5)') 'threshold eigenvalues = ', threshold_w
      do ialt = 1, globalS%retrS%nGaussCol
        if ( w(ialt) > 0.0d0 ) write(addtionalOutputUnit,'(I4, ES18.5)') ialt, w(ialt)
      end do
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') 'x_true = xa + Ainv * (x_retr - x_a)'
      write(addtionalOutputUnit,'(A)') 'trace gas = ', globalS%traceGasRetrS(iTrace)%nameTraceGas
      write(addtionalOutputUnit,'(A)') 'altitude(km)   x_retrieved      x_true_estimated      x_true        xa'
      do ialt = 1, globalS%retrS%nGaussCol
        write(addtionalOutputUnit,'(F10.4, 4ES18.5)') alt(ialt), xr(ialt), Colndens_true(ialt), xt(ialt), xa(ialt)
      end do ! ialt

      write(addtionalOutputUnit,'(A)') 'relative update of xa when SVD  is used: 100*(xt_fitted - xa)/xa'
      write(addtionalOutputUnit,'(A)') 'relative update of xa when xr   is used: 100*(xr        - xa)/xa'
      write(addtionalOutputUnit,'(A)') 'relative update of xa should be:         100*(xt        - xa)/xa'
      write(addtionalOutputUnit,'(A)')
      write(addtionalOutputUnit,'(A)') 'altitude(km)  100*(xt_SVD - xa)/xa   100*(xr - xa)/xa  100*(xt - xa)/xa'
      do ialt = 1, globalS%retrS%nGaussCol
        write(addtionalOutputUnit,'(F10.4, 3F21.12)') alt(ialt),                        &
                                  100.0d0*(Colndens_true(ialt) - xa(ialt) ) / xa(ialt), &
                                  100.0d0*(xr(ialt)            - xa(ialt) ) / xa(ialt), &
                                  100.0d0*(xt(ialt)            - xa(ialt) ) / xa(ialt)
      end do ! ialt
      end if

    end do ! iTrace

  end subroutine x_true_from_few_singular_values


  subroutine numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)

    ! perform numerical differentiation and write result of comparison with
    ! semi-analytical derivatives to additional output

    type(errorType), intent(inout) :: errS
    type(globalType) :: globalS
    integer, intent(in) :: iband, istate
    real(8), intent(in) :: xOriginal, xModified

    ! local
    integer  :: iwave, ibnd
    real(8)  :: radianceOriginal        (globalS%wavelInstrRadRetrS(iband)%nwavel)
    real(8)  :: radianceModified        (globalS%wavelInstrRadRetrS(iband)%nwavel)
    real(8)  :: derivativeSemiAnalytical(globalS%wavelInstrRadRetrS(iband)%nwavel)
    real(8)  :: derivativeNumerical     (globalS%wavelInstrRadRetrS(iband)%nwavel)
    real(8)  :: difference              (globalS%wavelInstrRadRetrS(iband)%nwavel)

    radianceOriginal(:)         = globalS%earthRadianceRetrS(iband)%rad(1,:)
    derivativeSemiAnalytical(:) = globalS%earthRadianceRetrS(iband)%K_lnvmr(:,istate)

    if ( globalS%controlRetrS%method == 0 ) then
      call calcReflAndDeriv(errS, globalS%maxFourierTermLUT, globalS%numSpectrBands, globalS%nTrace,                       &
                            globalS%wavelMRRadRetrS, globalS%wavelHRRetrS,                                                 &
                            globalS%XsecHRRetrS, globalS%RRS_RingRetrS, globalS%geometryRetrS, globalS%retrS,              &
                            globalS%controlRetrS, globalS%cloudAerosolRTMgridRetrS, globalS%gasPTRetrS,                    &
                            globalS%traceGasRetrS, globalS%surfaceRetrS, globalS%LambCloudRetrS,                           &
                            globalS%mieAerRetrS, globalS%mieCldRetrS,  globalS%createLUTRetrS,                             &
                            globalS%cldAerFractionRetrS, globalS%earthRadMulOffsetRetrS,                                   &
                            globalS%earthRadStrayLightRetrS, globalS%solarIrradianceRetrS, globalS%optPropRTMGridRetrS,    &
                            globalS%dn_dnodeRetrS, globalS%reflDerivHRRetrS)
      if (errorCheck(errS)) return

    else

       globalS%nwavelRTM(:) = globalS%weakAbsRetrS(:)%degreePoly + 1
       globalS%use_abs_opt_thickn_for_AMF(:) =  globalS%weakAbsRetrS(:)%useAbsOptThcknForAMF
       call calcReflAndDerivDismas(errS, globalS%numSpectrBands, globalS%nTrace, globalS%nwavelRTM, globalS%RRS_RingRetrS,     &
                                   globalS%use_abs_opt_thickn_for_AMF,                                                         &
                                   globalS%wavelHRRetrS, globalS%XsecHRRetrS, globalS%geometryRetrS,                           &
                                   globalS%retrS, globalS%controlRetrS, globalS%cloudAerosolRTMgridRetrS, globalS%gasPTRetrS,  &
                                   globalS%traceGasRetrS,  globalS%surfaceRetrS, globalS%LambCloudRetrS, globalS%mieAerRetrS,  &
                                   globalS%mieCldRetrS, globalS%cldAerFractionRetrS, globalS%earthRadMulOffsetRetrS,           &
                                   globalS%earthRadStrayLightRetrS, globalS%solarIrradianceRetrS, globalS%optPropRTMGridRetrS, &
                                   globalS%dn_dnodeRetrS, globalS%reflDerivHRRetrS)
       if (errorCheck(errS)) return
    end if

    do ibnd = 1, globalS%numSpectrBands
      call fillRadianceDerivativesHRgrid(errS, globalS%geometryRetrS, globalS%controlRetrS, globalS%reflDerivHRRetrS(ibnd), &
                                         globalS%solarIrradianceRetrS(ibnd), globalS%earthRadianceRetrS(ibnd))
      if (errorCheck(errS)) return
    end do ! iband

    do ibnd = 1, globalS%numSpectrBands
      call integrateSlitFunctionRad(errS, globalS%controlRetrS, globalS%wavelHRRetrS(ibnd), globalS%wavelInstrRadRetrS(ibnd), &
                                    globalS%retrS, globalS%solarIrradianceRetrS(ibnd), globalS%earthRadianceRetrS(ibnd))
      if (errorCheck(errS)) return
    end do

    do ibnd = 1, globalS%numSpectrBands
      if ( globalS%earthRadMulOffsetRetrS(ibnd)%fitMulOffset ) then
        call addMulOffset(errS,  globalS%wavelInstrRadRetrS(ibnd), globalS%earthRadMulOffsetRetrS(ibnd), &
                           globalS%earthRadianceRetrS(ibnd) )
        if (errorCheck(errS)) return
      else
        ! add a-priori multiplicative offsets for retrieval only the first time
        if ( globalS%iteration == 1 ) then
          call addMulOffset(errS,  globalS%wavelInstrRadRetrS(ibnd), globalS%earthRadMulOffsetRetrS(ibnd), &
                             globalS%earthRadianceRetrS(ibnd))
          if (errorCheck(errS)) return
        end if ! globalS%iteration == 1
      end if ! globalS%earthRadMulOffsetRetrS(ibnd)%fitMulOffset
    end do ! ibnd

    do ibnd = 1, globalS%numSpectrBands
      if ( globalS%earthRadStrayLightRetrS(ibnd)%fitStrayLight ) then
        call addStraylight(errS,  globalS%wavelInstrRadRetrS(ibnd), globalS%earthRadStrayLightRetrS(ibnd), &
                            globalS%earthRadianceRetrS(ibnd) )
        if (errorCheck(errS)) return
      else
        ! add a-priori stray light for retrieval only the first time
        if ( globalS%iteration == 1 ) then
          call addStraylight(errS,  globalS%wavelInstrRadRetrS(ibnd), globalS%earthRadStrayLightRetrS(ibnd), &
                              globalS%earthRadianceRetrS(ibnd))
          if (errorCheck(errS)) return
        end if ! globalS%iteration == 1
      end if ! globalS%earthRadStrayLightRetrS(ibnd)%fitStrayLight
    end do ! ibnd

    radianceModified(:) = globalS%earthRadianceRetrS(iband)%rad(1,:)

    derivativeNumerical(:) = ( radianceModified(:) - radianceOriginal ) &
                           / ( xModified - xOriginal )

    difference(:)          = 100.0d0 * ( derivativeNumerical(:) - derivativeSemiAnalytical(:) ) &
                           / derivativeSemiAnalytical(:)

    write(addtionalOutputUnit,*)
    write(addtionalOutputUnit,*) 'results for '//trim( globalS%retrS%codeFitParameters(istate) )
    write(addtionalOutputUnit,'(4(A,I4))') ' iband  = ', iband, ' istate = ', istate, &
                                           ' ialt   = ', globalS%retrS%codeAltitude(istate),  &
                                           ' iTrace = ', globalS%retrS%codeTraceGas(istate)
    write(addtionalOutputUnit,*) 'wavelength   derivSemiAnal   derivativeNum    relDifference(%)'
    do iwave = 1, globalS%wavelInstrRadRetrS(iband)%nwavel
      write(addtionalOutputUnit,'(F12.6, 2ES15.5, F12.4)') globalS%wavelInstrRadRetrS(iband)%wavel(iwave), &
                                                           derivativeSemiAnalytical(iwave),                &
                                                           derivativeNumerical(iwave),                     &
                                                           difference(iwave)
    end do
    write(addtionalOutputUnit,*)
    write(addtionalOutputUnit,*) 'wavelength   radianceOriginal   radianceModified'
    do iwave = 1, globalS%wavelInstrRadRetrS(iband)%nwavel
      write(addtionalOutputUnit,'(F12.6, 2ES15.7)') globalS%wavelInstrRadRetrS(iband)%wavel(iwave), &
                                                                   radianceOriginal(iwave),         &
                                                                   radianceModified(iwave)
    end do

    ! undo changes
    globalS%earthRadianceRetrS(iband)%rad(1,:)     = radianceOriginal(:)
    globalS%earthRadianceRetrS(iband)%K_lnvmr(:,istate) = derivativeSemiAnalytical(:)

  end subroutine numericalDifferentiation


  subroutine testderivatives (errS, globalS, iband)

    ! subroutine testderivatives calculates the derivatives using numerical differentiation.
    ! These derivatives are compared to the derivative than have been calculated using
    ! the semi-analytical approach. The derivatives and their relative difference are written
    ! to additional output. Execution then stops. Only the derivatives for the elements of the
    ! state vector are compared here.

    implicit none

    type(errorType), intent(inout) :: errS
    type(globalType)     :: globalS
    integer, intent (in) :: iband

    ! local
    integer :: istate, iTrace, ialt, imodel, index, status
    real(8) :: xOriginal, xModified
    real(8) :: altitudeTop, altitudeBot, pressureTop, pressureBot, pressureDifference
    real(8) :: altitudeTopOrig, altitudeBotOrig, pressureTopOrig, pressureBotOrig
    real(8) :: lnPressure(0:globalS%gasPTRetrS%npressure)
    real(8) :: SDaltitude(0:globalS%gasPTRetrS%npressure)
    real(8) :: tau, tauOriginal

    do istate = 1, globalS%retrS%nstate

      select case (globalS%retrS%codeFitParameters(istate))

        case( 'nodeTrace' )

          ! natural logarithm of the volume mixing ratio is fitted
          iTrace = globalS%retrS%codeTraceGas(istate)
          ialt   = globalS%retrS%codeAltitude(istate)

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)
          globalS%traceGasRetrS(iTrace)%vmr(ialt) = exp( xModified )
          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%traceGasRetrS(iTrace)%vmr(ialt) = exp( xOriginal )

        case( 'columnTrace' )

          ! natural logarithm of the column is fitted
          iTrace = globalS%retrS%codeTraceGas(istate)

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          ! update the profile by scaling the profile
          globalS%traceGasRetrS(iTrace)%column  = exp(xModified)
          globalS%traceGasRetrS(iTrace)%numDens = globalS%traceGasRetrS(iTrace)%numDensAP &
                                        * exp(xModified) / globalS%traceGasRetrS(iTrace)%columnAP
          globalS%traceGasRetrS(iTrace)%vmr     = globalS%traceGasRetrS(iTrace)%vmrAP     &
                                        * exp(xModified) / globalS%traceGasRetrS(iTrace)%columnAP

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%traceGasRetrS(iTrace)%column  = exp(xOriginal)
          globalS%traceGasRetrS(iTrace)%numDens = globalS%traceGasRetrS(iTrace)%numDensAP &
                                        * exp(xOriginal) / globalS%traceGasRetrS(iTrace)%columnAP
          globalS%traceGasRetrS(iTrace)%vmr     = globalS%traceGasRetrS(iTrace)%vmrAP     &
                                        * exp(xOriginal) / globalS%traceGasRetrS(iTrace)%columnAP

        case('nodeTemp')

          ialt   = globalS%retrS%codeAltitude(istate)

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%gasPTRetrS%temperatureNodes(ialt) = xModified
          globalS%retrS%x(istate)                   = xModified
          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%gasPTRetrS%temperatureNodes(ialt) = xOriginal

        case('offsetTemp')

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%gasPTRetrS%temperatureOffset = xModified
          globalS%retrS%x(istate)              = xModified

          ! change the temperatures profile using the offset
          globalS%gasPTRetrS%temperature(:) = globalS%gasPTRetrS%temperatureAP(:) + globalS%gasPTRetrS%temperatureOffset
          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate)              = xOriginal
          globalS%gasPTRetrS%temperatureOffset = xOriginal
          globalS%gasPTRetrS%temperature(:)    = globalS%gasPTRetrS%temperatureAP(:) + xOriginal

        case('surfPressure')

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%retrS%x(istate)          = xModified
          globalS%surfaceRetrS(1)%pressure = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate)          = xOriginal
          globalS%surfaceRetrS(1)%pressure = xOriginal

        case('surfAlbedo')

          index = globalS%retrS%codeIndexSurfAlb(istate)

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%retrS%x(istate) = xModified
          globalS%surfaceRetrS(iband)%albedo(index) = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate)                   = xOriginal
          globalS%surfaceRetrS(iband)%albedo(index) = xOriginal

        case('surfEmission')

          index = globalS%retrS%codeIndexSurfEmission(istate)

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%retrS%x(istate) = xModified
          globalS%surfaceRetrS(iband)%albedo(index) = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate)                     = xOriginal
          globalS%surfaceRetrS(iband)%emission(index) = xOriginal

        case('LambCldAlbedo')

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%retrS%x(istate) = xModified

          if ( globalS%cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then
            globalS%cloudAerosolRTMgridRetrS%albedoLambCldAllBands = xModified
          else
            index = globalS%retrS%codeIndexLambCldAlb(istate)
            globalS%LambCloudRetrS(iband)%albedo(index) = xModified
          end if ! cloudAerosolRTMgridS%useAlbedoLambCldAllBands

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate)                   = xOriginal

          if ( globalS%cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then
            globalS%cloudAerosolRTMgridRetrS%albedoLambCldAllBands = xOriginal
          else
            index = globalS%retrS%codeIndexLambCldAlb(istate)
            globalS%LambCloudRetrS(iband)%albedo(index) = xOriginal
          end if ! cloudAerosolRTMgridS%useAlbedoLambCldAllBands

        case('mulOffset')

          ! polynomial coefficients are fitted
          index = globalS%retrS%codeIndexMulOffset(istate)

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%earthRadMulOffsetRetrS(iband)%percentOffset(index) = xModified
          globalS%retrS%x(istate) = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate)                                         = xOriginal
          globalS%earthRadMulOffsetRetrS(iband)%percentOffset(index)      = xOriginal

        case('straylight')

          ! polynomial coefficients are fitted
          index = globalS%retrS%codeIndexStraylight(istate)

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%earthRadStrayLightRetrS(iband)%percentStrayLight(index) = xModified
          globalS%retrS%x(istate) = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate)                                         = xOriginal
          globalS%earthRadStrayLightRetrS(iband)%percentStrayLight(index) = xOriginal

        case('aerosolTau')

          ! natural logarithm of the aerosol optical thickness is fitted

          xOriginal = globalS%retrS%x(istate)
          ! ensure that we have a change when the optical thickness is one and xOriginal = 0
          if ( abs(xOriginal) > 1.0d-4 ) then
            xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)
          else
            xModified = globalS%controlRetrS%relChange
          end if

          globalS%cloudAerosolRTMgridRetrS%intervalAerTau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = exp( xModified )

          if ( globalS%cloudAerosolRTMgridRetrS%useMieScatAer ) then
            do imodel = 1, maxNumMieModels
              tauOriginal = globalS%mieAerRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
              if ( tauOriginal > 1.0d-10 ) then
                tau = exp( log(tauOriginal) * (1.0d0 + globalS%controlRetrS%relChange) )
                globalS%mieAerRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = tau
              end if
            end do ! imodel
          end if

          globalS%retrS%x(istate) = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%cloudAerosolRTMgridRetrS%intervalAerTau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = exp( xOriginal )
          if ( globalS%cloudAerosolRTMgridRetrS%useMieScatAer ) then
            do imodel = 1, maxNumMieModels
              tau = globalS%mieAerRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
              if ( tau > 1.0d-10 ) then
                tauOriginal = exp( log(tau) / (1.0d0 + globalS%controlRetrS%relChange) )
                globalS%mieAerRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = tauOriginal
              end if
            end do ! imodel
          end if

        case('aerosolSSA')

          ! single scattering albedo itself is fitted, not the natural logarithm

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%cloudAerosolRTMgridRetrS%intervalAerSSA(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = xModified
          globalS%retrS%x(istate) = xModified
          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%cloudAerosolRTMgridRetrS%intervalAerSSA(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)  = xOriginal

        case('aerosolAC')

          ! angstrom coefficient itself is fitted, not the natural logarithm

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          globalS%cloudAerosolRTMgridRetrS%intervalAerAC(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = xModified
          globalS%retrS%x(istate) = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%cloudAerosolRTMgridRetrS%intervalAerAC(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = xOriginal

        case('cloudTau')

          if ( globalS%cloudAerosolRTMgridRetrS%fitLnCldTau ) then

            ! fit natural logaritm of the cloud optical thickness

            xOriginal = globalS%retrS%x(istate)
            ! ensure that we have a change when the optical thickness is one and xOriginal = 0
            if ( abs(xOriginal) > 1.0d-4 ) then
              xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)
            else
              xModified = globalS%controlRetrS%relChange
            end if

            globalS%cloudAerosolRTMgridRetrS%intervalCldTau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = exp( xModified )

            if ( globalS%cloudAerosolRTMgridRetrS%useMieScatCld ) then
              do imodel = 1, maxNumMieModels
                tauOriginal = globalS%mieCldRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
                if ( tauOriginal > 1.0d-10 ) then
                  tau = exp( log(tauOriginal) * (1.0d0 + globalS%controlRetrS%relChange) )
                  globalS%mieCldRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = tau
                end if
              end do ! imodel
            end if

            globalS%retrS%x(istate) = xModified

            call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
            if (errorCheck(errS)) return

            ! undo changes
            globalS%retrS%x(istate) = xOriginal
            globalS%cloudAerosolRTMgridRetrS%intervalCldTau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = exp( xOriginal )
            if ( globalS%cloudAerosolRTMgridRetrS%useMieScatCld ) then
              do imodel = 1, maxNumMieModels
                tau = globalS%mieCldRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
                if ( tau > 1.0d-10 ) then
                  tauOriginal = exp( log(tau) / (1.0d0 + globalS%controlRetrS%relChange) )
                  globalS%mieCldRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = tauOriginal
                end if ! tau > 1.0d-10
              end do ! imodel
            end if ! globalS%cloudAerosolRTMgridRetrS%useMieScatCld

          else

            ! fit cloud optical thickness
            xOriginal = globalS%retrS%x(istate)
            xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)
            globalS%cloudAerosolRTMgridRetrS%intervalCldTau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = xModified

            if ( globalS%cloudAerosolRTMgridRetrS%useMieScatCld ) then
              do imodel = 1, maxNumMieModels
                tauOriginal = globalS%mieCldRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
                if ( tauOriginal > 1.0d-10 ) then
                  tau = tauOriginal * (1.0d0 + globalS%controlRetrS%relChange)
                  globalS%mieCldRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = tau
                end if
              end do ! imodel
            end if

            globalS%retrS%x(istate) = xModified

            call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
            if (errorCheck(errS)) return

            ! undo changes
            globalS%retrS%x(istate) = xOriginal
            globalS%cloudAerosolRTMgridRetrS%intervalCldTau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = xOriginal
            if ( globalS%cloudAerosolRTMgridRetrS%useMieScatCld ) then
              do imodel = 1, maxNumMieModels
                tau = globalS%mieCldRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
                if ( tau > 1.0d-10 ) then
                  tauOriginal = tau / (1.0d0 + globalS%controlRetrS%relChange)
                  globalS%mieCldRetrS(imodel)%tau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = tauOriginal
                end if ! tau > 1.0d-10
              end do ! imodel
            end if ! globalS%cloudAerosolRTMgridRetrS%useMieScatCld

          end if ! globalS%cloudAerosolRTMgridRetrS%fitLnCldTau


        case('cloudAC')

          ! angstrom coefficient itself is fitted, not the natural logarithm

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)
          globalS%cloudAerosolRTMgridRetrS%intervalCldAC(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = xModified
          globalS%retrS%x(istate) = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%cloudAerosolRTMgridRetrS%intervalCldAC(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = xOriginal

        case('cloudFraction')

          xOriginal = globalS%retrS%x(istate)
          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)
          index = globalS%retrS%codeIndexCloudFraction(istate)

          if ( globalS%cloudAerosolRTMgridRetrS%usecldAerFractionAllBands ) then
            globalS%cloudAerosolRTMgridRetrS%cldAerfractionAllBands = xModified
          else
            globalS%cldAerFractionRetrS(iband)%cldAerFraction(index) = xModified
          end if
          globalS%retrS%x(istate) = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          if ( globalS%cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then
            globalS%cloudAerosolRTMgridRetrS%cldAerFractionAllBands = xOriginal
          else
            globalS%cldAerFractionRetrS(iband)%cldAerFraction(index) = xOriginal
          end if

        case('intervalDP')

          xOriginal = globalS%retrS%x(istate)
          pressureTopOrig = globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
          pressureBotOrig = globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit-1)
          altitudeTopOrig = globalS%cloudAerosolRTMgridRetrS%intervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
          altitudeBotOrig = globalS%cloudAerosolRTMgridRetrS%intervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit-1)

          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          altitudeTop = xModified

          ! use spline interpolation to update the pressure at the top
          lnpressure(:)  = log(globalS%gasPTRetrS%pressure(:))
          call spline(errS, globalS%gasPTRetrS%alt, lnpressure, SDaltitude, status)
          if (errorCheck(errS)) return
          pressureTop = exp( splint(errS, globalS%gasPTRetrS%alt, lnpressure, SDaltitude, altitudeTop, status) )

          pressureDifference = &
                       globalS%cloudAerosolRTMgridRetrS%intervalBoundsAP_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit - 1) &
                     - globalS%cloudAerosolRTMgridRetrS%intervalBoundsAP_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
          pressureBot = pressureTop + pressureDifference

          ! use spline interpolation to update the altitudes
          call spline(errS, lnpressure, globalS%gasPTRetrS%alt, SDaltitude, status)
          if (errorCheck(errS)) return
          altitudeBot = splint(errS, lnpressure, globalS%gasPTRetrS%alt, SDaltitude, log(pressureBot), status)

          ! store updated values
          globalS%cloudAerosolRTMgridRetrS%intervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)       = altitudeTop
          globalS%cloudAerosolRTMgridRetrS%intervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit - 1)   = altitudeBot
          globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)     = pressureTop
          globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit - 1) = pressureBot

          ! update state vector because it has to correspond to the atmospheric parameters
          globalS%retrS%x(istate)   = xModified

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%cloudAerosolRTMgridRetrS%intervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)       = altitudeTopOrig
          globalS%cloudAerosolRTMgridRetrS%intervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit - 1)   = altitudeBotOrig
          globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)     = pressureTopOrig
          globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit - 1) = pressureBotOrig

        case('intervalTop')

          xOriginal = globalS%retrS%x(istate)
          pressureTopOrig = globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)
          altitudeTopOrig = globalS%cloudAerosolRTMgridRetrS%intervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)

          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          altitudeTop = xModified

          ! use spline interpolation to update the pressure at the top
          lnpressure  = log(globalS%gasPTRetrS%pressure)
          call spline(errS, globalS%gasPTRetrS%alt, lnpressure, SDaltitude, status)
          if (errorCheck(errS)) return
          pressureTop = exp( splint(errS, globalS%gasPTRetrS%alt, lnpressure, SDaltitude, altitudeTop, status) )

          globalS%cloudAerosolRTMgridRetrS%intervalBounds  (globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = altitudeTop
          globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = pressureTop

          ! update state vector because it has to correspond to the atmospheric parameters
          globalS%retrS%x(istate) = altitudeTop

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%cloudAerosolRTMgridRetrS%intervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)   = altitudeTopOrig
          globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) = pressureTopOrig

        case('intervalBot')

          xOriginal = globalS%retrS%x(istate)
          pressureBotOrig = globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit-1)
          altitudeBotOrig = globalS%cloudAerosolRTMgridRetrS%intervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit-1)

          xModified = xOriginal * (1.0d0 + globalS%controlRetrS%relChange)

          altitudeBot = xModified

          ! use spline interpolation to update the pressure at the bottom of the interval
          lnpressure  = log(globalS%gasPTRetrS%pressure)
          call spline(errS, globalS%gasPTRetrS%alt, lnpressure, SDaltitude, status)
          if (errorCheck(errS)) return
          pressureBot = exp( splint(errS, globalS%gasPTRetrS%alt, lnpressure, SDaltitude, altitudeBot, status) )

          globalS%cloudAerosolRTMgridRetrS%intervalBounds  (globalS%cloudAerosolRTMgridRetrS%numIntervalFit-1) = altitudeBot
          globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit-1) = pressureBot

          ! update state vector because it has to correspond to the atmospheric parameters
          globalS%retrS%x(istate) = altitudeBot

          call numericalDifferentiation(errS, globalS, iband, istate, xOriginal, xModified)
          if (errorCheck(errS)) return

          ! undo changes
          globalS%retrS%x(istate) = xOriginal
          globalS%cloudAerosolRTMgridRetrS%intervalBounds  (globalS%cloudAerosolRTMgridRetrS%numIntervalFit-1) = altitudeBotOrig
          globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit-1) = pressureBotOrig

        case default
           call logDebug('in subroutine testderivatives in DISAMARmodule')
           write(errS%temp,*) 'incorrect value for globalS%retrS%codeFitParameters(istate) : ', &
                               globalS%retrS%codeFitParameters(istate)
          call errorAddLine(errS, errS%temp)
          call mystop(errS, 'stopped due to incorrect value for globalS%retrS%codeFitParameters(istate)')
          if (errorCheck(errS)) return
      end select

    end do ! istate

  end subroutine testderivatives


  function current_time(current_time_values)

    ! function current_time uses the standard FORTRAN 90 routine
    ! CALL DATE_AND_TIME ([ date] [, time] [, zone] [, values] )
    ! to return the time in seconds since midnight

    ! DATE_AND_TIME returns information in the array values:
    ! value(5) = Hour of the day (0-23)
    ! value(6) = Minutes (0-59)
    ! value(7) = Seconds (0-59)
    ! value(8) = Milliseconds (0-999)

    real(8)              :: current_time              ! in seconds
    integer              :: current_time_values(8)

    CALL DATE_AND_TIME (values = current_time_values)

    current_time = current_time_values(5) * 3600d0 +  &
                   current_time_values(6) *   60d0 +  &
                   current_time_values(7) *    1d0 +  &
                   current_time_values(8) / 1000d0

  end function current_time


  subroutine getAerosol_mid_altitude(aerosol_mid_altitude, lnaerosol_mid_pressure, lnp, z, n)

    ! interpolate the ln(mid pressure (hPa)) to an mid-altitude (in km), as these are the units of the input arrays.

    integer                   :: n
    real(8), intent(inout)    :: aerosol_mid_altitude
    real(8), intent(in)       :: lnaerosol_mid_pressure
    real(8), intent(in)       :: lnp(0:n)
    real(8), intent(in)       :: z(0:n)

    type(errorType)           :: errS
    integer                   :: status_spline, status_alloc
    real(8), allocatable      :: SDz_p(:)

    real(4), parameter        :: fill_value = 9969209968386869046778552952102584320.0

    call enter('getAerosol_mid_altitude')

    aerosol_mid_altitude = fill_value

    call errorInit(errS)
    call errorSetInteractive(errS, .false.)

    allocate(SDz_p(0:n), STAT = status_alloc)
    if (status_alloc /= 0 .and. errorCheck(errS)) then
      goto 99999
    end if

    call spline(errS, lnp, z, SDz_p, status_spline)
    if (status_spline /= 0 .and. errorCheck(errS)) then
      deallocate( SDz_p, STAT = status_alloc )
      goto 99999
    end if

    aerosol_mid_altitude = splint(errS, lnp, z, SDz_p, dble(lnaerosol_mid_pressure), status_spline)
    if (status_spline /= 0 .and. errorCheck(errS)) then
      deallocate( SDz_p, STAT = status_alloc )
      goto 99999
    end if

    deallocate(SDz_p, STAT = status_alloc)

99999 continue
    call exit('getAerosol_mid_altitude')

  end subroutine getAerosol_mid_altitude

end module DISAMARModule
