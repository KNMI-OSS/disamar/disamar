!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

program DISAMAR

! Author: Johan F. de Haan
! The code Deriving Instrument Specifications and Analyzing Methods for Atmospheric Retrievals (DISAMAR)
! was initially based on software developed for ozone profile retrieval for the Ozone Monitoring Instrument.
! It was substantially extended and modified during the CAMELOT project, intended to derive Level 1b
! requirements for the Sentinel 4 and 5 instruments, scheduled for launch around 2019. Futher improvements
! were made during the OnTRAQ project (April 2008 - Nov 2009). Further extensions and improvements have
! been made in 2010 - 20013 to support the TROPOMI / Sentinel 5 precursor project.
!
! DISAMAR is flexible. Many different simulations can be done by changing only the configuration file
! and input files like absorption cross sections. Main elements for version 3.5.2 are:
! - Optimal Extimation is used for retrieval, which provides extensive diagnostic information, while
!   the influence of a-priori information can be made very small by using large a-priori errors.
!   Alternatives are two forms of DOAS retrieval and DISMAS (DIfferential and SMooth Absorption Separated).
! - The model atmosphere used for simulation can differ from the model atmosphere used for retrieval, e.g.
!   the atmosphere used for simulation can contain cloud / aerosol while the atmosphere used for
!   retrieval contains no cloud/aerosol or cloud /aerosol with different properties. Also the solar
!   irradiance spectrum, and the absorption cross sections can differ for simulation and retrieval.
! - The radiative transfer calculations are based on the doubling/adding method. The adding part
!   can be replaced by the LAyer Based Orders of Scattering method (LABOS) which is often faster than
!   the adding method. The reflectance is calculated using integration over the source function.
! - The code has been extended to account for polarized light.
! - Derivatives needed for retrieval are calculated efficiently using reciprocity. These derivatives
!   are calculated using the internal radiation field.
! - In contrast to many other models, here the profiles of the absorbing gases are defined through
!   cubic spline interpolation on the logarithm of the volume mixing ratio (vmr). That means that real
!   profiles are retrieved specified by the nodes used for spline interpolation, and not the mean
!   vmr for specified layers.
! - Clouds can be modeled using a Lambertian cloud or a scattering cloud with a Henyey-Greenstein
!   phase function. It is also possible to use Mie scattering. Clouds can cover a part of the pixel.
! - Aerosol is covers the entire pixel. A Henyey-Greenstein phase function or Mie scattering can be used.
! - For a Henyey-Greenstein phase function the wavelength dependence of cloud or aerosol is restricted to
!   an Angstrom law for the optical thickness. The optical thickness, single scattering albedo, and
!   Angstrom coefficient can be fitted, but only for one atmospheric interval.
! - The reflection properties of the surface are specified through a Lambertian reflector whose albedo
!   can vary with wavelength using a polynomial. One can specify the wavelength and the value of the albedo
!   at one (constant), two (linear), or more (polynomial) values.
! - More than one spectral band can be specified, and the instrument properties can differ for these
!   spectral bands (e.g. the slit function for UV1 and UV2 or a UV window and the O2 A band).
! - The instrument model is fairly simple. For each spectral band  the start wavelength, the end wavelength
!   and the step size (width of the spectral bin or sampling distance) is given. Further, a slit function
!   (currently not depending on the wavelength within a spectral band). Stray light can be fitted as a
!   polynomial in each spectral band. Alternatively, additional and multiplicative offsets can be specified.
!   A reference spectrum and a spectral bin size for that reference spectrum can be used to define the
!   Signal to Noise Ratio (SNR). The reference spectrum is read from file. The SNR can be specified at
!   one or more wavelengths. The SNR used for other wavelength is obtaine through interpolation where
!   the user can choose to account for shot noise. Further, non-diagonal elements of the measurement
!   error covariance matrix can be used to account for calibration errors.
! - The total column can be divided into a number of subcolumns. Properties of these subcolumns (e.g.
!   the amount of trace gas in the subcolumn, the error in this amount, etc.) are calculated
!   for each trace gas.
! - Rotational Raman scattering can be taken into account and Ring spectra can be calculated.
!   Version 3.5.3: added TOMS V8 ozone climatology and removed some bugs
!   Version 3.5.4: added option to use effective cross sections and not convolute (at the end) with the slit function
!                  which does not work very well when RRS is taken into account.
!   Version 3.5.5  - save memory for simulation by allocating Se only for retrieval
!                  - calculate gain for subcolumns
!                  - removed bug in HITRAN module (number of wavenumbers was incorrect)
!                  - added paramaterization of the SNR for Sentinel 5 (based on info from Joerg Langen - ESA)
!                  - added slit functions for Sentinel 5 (based on info from Joerg Langen - ESA)
!                  - repaired bug for using reflectance from file combined with a tabulated slit function
!   Version 3.5.6  - added the option to fit multiplicative offsets for the Earth radiance
!                    where the multiplicative offset is a low order polynomial
!                  - added a noise floor SNRmax in configuration file. When SNR > SNRmax SNR is set to SNRmax.
!                    Note that SNRmax pertains to the actual signal to noise ration whereas the SNR specified
!                    in the configuration file pertains to the SNR for the reference spectrum.
!   Version 3.6.0  - option to use a correction LUT to account for polarization and Rotational Raman Scattering (RRS)
!                    so that for the forward calculations only Rayleigh scattering is needed (ozone profile)


    use DISAMAR_interface

    implicit none

    integer :: status
    type(globalType), pointer :: dynamic_workspace
    character, allocatable :: buffer(:)

    status = disamar_set_log_level(LOG_DEBUG)
    if (status .ne. 0) then
        call disamar_logger('Failed to set log level', LOG_ERROR)
        goto 99999
    end if

    status = disamar_allocate_static_workspace()
    if (status .ne. 0) then
        call disamar_logger('Failed to allocate static workspace', LOG_ERROR)
        goto 99999
    end if

    status = disamar_load_file('Config.in', buffer)
    if (status .ne. 0) then
        call disamar_logger('Failed to read Config.in', LOG_ERROR)
        goto 99999
    end if

    status = disamar_set_static_input_c('config', 0, buffer, 0)
    if (status .ne. 0) then
        call disamar_logger('Failed to set static input', LOG_ERROR)
        goto 99999
    end if

    status = disamar_allocate_dynamic_workspace(dynamic_workspace)
    if (status .ne. 0) then
        call disamar_logger('Failed to allocate dynamic workspace', LOG_ERROR)
        goto 99999
    end if

    status = disamar_retrieval(dynamic_workspace)
    if (status .ne. 0) then
        call disamar_logger('Failed to perform retrieval', LOG_ERROR)
        goto 99999
    end if

    status = disamar_deallocate_dynamic_workspace(dynamic_workspace)
    if (status .ne. 0) then
        call disamar_logger('Failed to deallocate dynamic workspace', LOG_ERROR)
        goto 99999
    end if

    status = disamar_deallocate_static_workspace()
    if (status .ne. 0) then
        call disamar_logger('Failed to deallocate static workspace', LOG_ERROR)
        goto 99999
    end if

99999 continue

end program DISAMAR
