!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

! **********************************************************************
! This module is part of DISAMAR
! The module dismasModule contains subroutines to calculate
! the reflectance and the derivatives w.r.t. the state vector elements
! in an efficient manner when the absorption by trace gases is a smooth
! function of the wavelength (no line absorption) using a DOAS like
! expression for the wavelength.
!
! Author: Johan de Haan, Jan 2010
!
! Modified July 2010: replace the polynomial a0 + a1 * lambda + a2 * lambda**2 + ...
!                     where lambda is the wavelength scaled to the interval (-1,+1)
!                     by a sum over (orthoganal) Legendre functions
!                     which makes it possible to use a larger degree of the polynomial
!
! **********************************************************************

module dismasModule

  use dataStructures
  use mathTools,                only: spline, splint, svdfit, fleg, svdcmp, verfy_elem_differ, &
                                      getSmoothAndDiffXsec, splintLin, polyInt,                &
                                      fitAndEvalLegPolynomial, lnfitAndEvalLegPolynomial

  use radianceIrradianceModule, only: fillAltPresGridRTM, calculate_dn_dnode, calculate_K_cld, calculate_K_clr,&
                                      setPolynomialDerivatives_cld, setPolynomialDerivatives_clr,              &
                                      calculate_wfnode, allocateUD, deallocateUD
  use propAtmosphereModule,     only: getOptPropAtm
  use LabosModule,              only: layerBasedOrdersScattering
  use doasModule,               only: interpolateXsecRTMaltGrid, calcTemperatureNdensRTMgrid

  private
  public  :: calcReflAndDerivDismas

  contains

  subroutine calcReflAndDerivDismas(errS, numSpectrBands, nTrace, nwavelRTM, RRS_RingS,            &
                                    use_abs_opt_thickn_for_AMF,                                    &
                                    wavelHRS, XsecHRS, geometryS, retrS, controlS,                 &
                                    cloudAerosolRTMgridS, gasPTS, traceGasS, surfaceS, LambCloudS, &
                                    mieAerS, mieCldS, cldAerFractionS, mulOffsetS, strayLightS,    &
                                    solarIrrS, optPropRTMGridS, dn_dnodeS, reflDerivHRS)

   ! calcuates the sun-normalized radiance (also called reflectance here) and the derivatives
   ! on a high-resolution spectral grid using a DOAS-like approach and stores the results in reflDerivHRS

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                       intent(in)    :: numSpectrBands                  ! number of spectral bands
    integer,                       intent(in)    :: nTrace                          ! number of trace gases
    integer,                       intent(in)    :: nwavelRTM(numSpectrBands)       ! # wavelengths for RTM calculations
    logical,                       intent(in)    :: use_abs_opt_thickn_for_AMF(numSpectrBands)
    type(RRS_RingType),            intent(in)    :: RRS_RingS(numSpectrBands)       ! Raman / Ring information
    type(wavelHRType),             intent(in)    :: wavelHRS(numSpectrBands)        ! high resolution wavelength grid
    type(XsecType),                intent(in)    :: XsecHRS(numSpectrBands,nTrace)  ! absorption cross section
    type(geometryType),            intent(in)    :: geometryS                       ! geometrical information
    type(retrType),                intent(in)    :: retrS                           ! contains info on the fit parameters
    type(controlType),             intent(inout) :: controlS                        ! control parameters for RTM
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS            ! cloud-aerosol properties
    type(gasPTType),               intent(inout) :: gasPTS                          ! atmospheric pressure and temperature
    type(traceGasType),            intent(inout) :: traceGasS(nTrace)               ! atmospheric trace gas properties
    type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)        ! surface properties
    type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)      ! Lambertian cloud properties
    type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)        ! specification expansion coefficients
    type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)        ! specification expansion coefficients
    type(cldAerFractionType),      intent(inout) :: cldAerFractionS(numSpectrBands) ! cloud / aerosol fraction structure
    type(mulOffsetType),           intent(inout) :: mulOffsetS(numSpectrBands)      ! multiplicative offset
    type(straylightType),          intent(inout) :: strayLightS(numSpectrBands)     ! stray light
    type(SolarIrrType),            intent(inout) :: solarIrrS(numSpectrBands)       ! solar irradiance
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS                 ! optical properties on RTM altitude grid
    type(dn_dnodeType),            intent(inout) :: dn_dnodeS(0:nTrace)             ! derivatives for spline interpolation
    type(reflDerivType),           intent(inout) :: reflDerivHRS(numSpectrBands)    ! reflectance and derivatives

    ! local
    integer :: iband

    ! Use dismas reflectance model: R = Rsmooth * exp(-M*N*sigma) for each spectral band.
    ! Repeat the calculations for each spectral band, because the polynomial fitting for Rsmooth
    ! has to be performed for each band separately.

    do iband = 1, numSpectrBands
        call dismas(errS, iband, nTrace, nwavelRTM(iband), use_abs_opt_thickn_for_AMF(iband),  &
                    RRS_RingS(iband), wavelHRS(iband), XsecHRS(iband,:), geometryS,      &
                    retrS, controlS, cloudAerosolRTMgridS, gasPTS, traceGasS,            &
                    surfaceS(iband),  LambCloudS(iband), mieAerS, mieCldS,               &
                    cldAerFractionS(iband), mulOffsetS(iband), strayLightS(iband),       &
                    solarIrrS(iband), optPropRTMGridS, dn_dnodeS, reflDerivHRS(iband))
        if (errorCheck(errS)) return
    end do

  end subroutine calcReflAndDerivDismas


  subroutine dismas(errS, iband, nTrace, nwavelRTM, use_abs_opt_thickn_for_AMF,     &
                    RRS_RingS, wavelHRS, XsecHRS, geometryS, retrS, controlS,       &
                    cloudAerosolRTMgridS, gasPTS, traceGasS, surfaceS, LambCloudS,  &
                    mieAerS, mieCldS, cldAerFractionS, mulOffsetS, strayLightS,     &
                    solarIrrS, optPropRTMGridS, dn_dnodeS, reflDerivHRS)

    implicit none

    type(errorType),               intent(inout) :: errS
    integer,                       intent(in)    :: iband                     ! number wavelength band
    integer,                       intent(in)    :: nTrace                    ! number of trace gases
    integer,                       intent(in)    :: nwavelRTM                 ! number wavelengths for RTM calculations
    logical,                       intent(in)    :: use_abs_opt_thickn_for_AMF
    type(RRS_RingType),            intent(in)    :: RRS_RingS                 ! Raman / Ring information
    type(wavelHRType),             intent(in)    :: wavelHRS                  ! high resolution wavelength grid
    type(XsecType),                intent(in)    :: XsecHRS(nTrace)           ! altitude dependent absorption cross sections
    type(geometryType),            intent(in)    :: geometryS                 ! geometrical information for simulation and retrieval
    type(retrType),                intent(in)    :: retrS                     ! contains info on the fit parameters
    type(controlType),             intent(inout) :: controlS                  ! control parameters for radiative transfer
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS      ! cloud-aerosol properties and RTM integration grid
    type(gasPTType),               intent(inout) :: gasPTS                    ! atmospheric pressure and temperature
    type(traceGasType),            intent(inout) :: traceGasS(nTrace)         ! atmospheric trace gas properties
    type(LambertianType),          intent(inout) :: surfaceS                  ! surface properties for a spectral band
    type(LambertianType),          intent(inout) :: LambCloudS                ! Lambertian cloud properties
    type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)  ! specification expansion coefficients
    type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)  ! specification expansion coefficients
    type(cldAerFractionType),      intent(inout) :: cldAerFractionS           ! cloud / aerosol fraction structure
    type(mulOffsetType),           intent(inout) :: mulOffsetS                ! multiplicative offset
    type(straylightType),          intent(inout) :: strayLightS               ! stray light
    type(SolarIrrType),            intent(inout) :: solarIrrS                 ! solar irradiance
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS           ! optical properties on RTM grid
    type(dn_dnodeType),            intent(inout) :: dn_dnodeS(0:nTrace)       ! derivatives for spline interpolation
    type(reflDerivType),           intent(inout) :: reflDerivHRS              ! reflectance and derivatives on HR grid

    ! local

    integer             :: iwave, istate, iTrace
    type(wavelHRType)   :: wavelRTMS_clr         ! wavelength grid for RTM calculations
    type(wavelHRType)   :: wavelRTMS_cld         ! wavelength grid for RTM calculations

    type(XsecType)      :: XsecRTMS_clr(nTrace)  ! Absorption cross sections on RTM grid, i.e. wavelengths
                                                 ! where the total differential absorption is zero.
                                                 ! The altitude grid is the retrieval grid from the configuration file.
    type(XsecType)      :: XsecRTMS_cld(nTrace)  ! Absorption cross sections on RTM grid, cloudy part

    type(XsecType)      :: XsecHRRTMaltS(nTrace) ! same as XsecHRS, but interpolated to the RTM altitude grid

    type(reflDerivType) :: reflDerivRTMS         ! reflectance and derivatives on RTM grid cloud free part

    real(8) :: amfAltAveragedHR_clr       (wavelHRS%nwavel,nTrace) ! altitude averaged air mass factor clear part
    real(8) :: amfAltAveragedHR_cld       (wavelHRS%nwavel,nTrace) ! altitude averaged air mass factor cloudy part
    real(8) :: XsecAltAveragedHR_clr      (wavelHRS%nwavel,nTrace) ! altitude averaged Xsec on HR grid clear part
    real(8) :: XsecAltAveragedHR_cld      (wavelHRS%nwavel,nTrace) ! altitude averaged Xsec on HR grid cloudy part
    real(8) :: XsecAltAveragedHRsmooth_clr(wavelHRS%nwavel,nTrace) ! smooth part of XsecAltAveragedHR_clr
    real(8) :: XsecAltAveragedHRsmooth_cld(wavelHRS%nwavel,nTrace) ! smooth part of XsecAltAveragedHR_cld
    real(8) :: XsecAltAveragedHRdiff_clr  (wavelHRS%nwavel,nTrace) ! differential part of XsecAltAveragedHR_clr
    real(8) :: XsecAltAveragedHRdiff_cld  (wavelHRS%nwavel,nTrace) ! differential part of XsecAltAveragedHR_cld
    real(8) :: absOptThickness            (wavelHRS%nwavel)

    real(8) :: slantAbsOptThickness_clr    (wavelHRS%nwavel)       ! slant absorption optical thickness clear part
    real(8) :: slantAbsOptThickness_cld    (wavelHRS%nwavel)       ! slant absorption optical thickness cloudy part
    real(8) :: slantAbsOptThicknessDiff_clr(wavelHRS%nwavel)       ! differential part of slantAbsOptThickness_clr
    real(8) :: slantAbsOptThicknessDiff_cld(wavelHRS%nwavel)       ! differential part of slantAbsOptThickness_cld

    logical, parameter :: verbose = .false.

    ! Determine the altitude grid used for the radiative transfer calculations - RTM altitude grid.
    ! As the altitude of clouds can change, this is repeated for each RTM calculations.

    call fillAltPresGridRTM(errS, cloudAerosolRTMgridS, OptPropRTMGridS)
    if (errorCheck(errS)) return

    if ( controlS%calculateDerivatives ) then
      if ( (retrS%numIterations == 1 ) .OR. cloudAerosolRTMgridS%fitIntervalDP    &
                                       .OR. cloudAerosolRTMgridS%fitIntervalTop   &
                                       .OR. cloudAerosolRTMgridS%fitIntervalBot ) then
        ! calculate the change in a profile if the value at a node changes; index 0 is for air
        call calculate_dn_dnode(errS, gasPTS%npressureNodes, gasPTS%altNodes, optPropRTMGridS%RTMnlayerSub, &
                                optPropRTMGridS%RTMaltitudeSub, dn_dnodeS(0)%dn_dnode, gasPTS%useLinInterp)
        if (errorCheck(errS)) return
        do iTrace = 1, nTrace
          call calculate_dn_dnode(errS, traceGasS(iTrace)%nalt, traceGasS(iTrace)%alt, optPropRTMGridS%RTMnlayerSub, &
                   optPropRTMGridS%RTMaltitudeSub, dn_dnodeS(iTrace)%dn_dnode, traceGasS(iTrace)%useLinInterp)
          if (errorCheck(errS)) return
        end do
      end if
    end if ! controlS%calculateDerivatives

    ! Initialization which includes claiming memory for XsecRTMS, XsecHRRTMaltS, and reflDerivRTMS.
    ! The altitude resolved air mass factors in reflDerivHRS are set to the geometrical
    ! air mass factor for the first iteration.

    call initializeDismas(errS, nTrace, nwavelRTM, geometryS, traceGasS, optPropRTMGridS, &
                          XsecHRS, retrS, wavelRTMS_clr, wavelRTMS_cld,             &
                          XsecRTMS_clr, XsecRTMS_cld, XsecHRRTMaltS, controlS,      &
                          reflDerivRTMS, reflDerivHRS)
    if (errorCheck(errS)) return

    ! Interpolate absorption cross sections on the high resolution wavelength grid from the
    ! retrieval altitude grid to the RTM altitude grid.

    do iTrace = 1, nTrace
      call interpolateXsecRTMaltGrid(errS, traceGasS(iTrace), optPropRTMGridS, XsecHRS(iTrace), XsecHRRTMaltS(iTrace))
      if (errorCheck(errS)) return
    end do ! iTrace

    ! calculate the number density and temperature on RTM altitude grid

    call calcTemperatureNdensRTMgrid(errS, nTrace, gasPTS, traceGasS, optPropRTMGridS)
    if (errorCheck(errS)) return


    ! calculate the altitude averaged absorption cross section on the high resolution wavelength grid

    call calculateAltAveragedXsecHR(errS, nTrace, optPropRTMGridS, wavelHRS,            &
                                    XsecHRRTMaltS, reflDerivHRS,                  &
                                    amfAltAveragedHR_clr, amfAltAveragedHR_cld,   &
                                    XsecAltAveragedHR_clr, XsecAltAveragedHR_cld)
    if (errorCheck(errS)) return

    ! Determine the smooth and and differential part of the absorption cross section on the
    ! high resolution wavelength grid.

    do iTrace = 1, nTrace
      call getSmoothAndDiffXsec(errS, nwavelRTM-1, wavelHRS%nwavel, wavelHRS%wavel, XsecAltAveragedHR_clr(:,iTrace), &
                                XsecAltAveragedHRsmooth_clr(:,iTrace),  XsecAltAveragedHRdiff_clr(:,iTrace))
      if (errorCheck(errS)) return
      call getSmoothAndDiffXsec(errS, nwavelRTM-1, wavelHRS%nwavel, wavelHRS%wavel, XsecAltAveragedHR_cld(:,iTrace), &
                                XsecAltAveragedHRsmooth_cld(:,iTrace),  XsecAltAveragedHRdiff_cld(:,iTrace))
      if (errorCheck(errS)) return
    end do ! iTrace

    ! Calculate the differential slant optical thickness
    if ( use_abs_opt_thickn_for_AMF ) then
      ! regard amf as a function of the absorption optical thickness
      call calcSlantAbsOptThickness(errS, nTrace, traceGasS, wavelHRS, amfAltAveragedHR_clr,  &
                                    XsecAltAveragedHR_clr, XsecAltAveragedHRdiff_clr,   &
                                    slantAbsOptThicknessDiff_clr, absOptThickness)
      if (errorCheck(errS)) return

      call calcSlantAbsOptThickness(errS, nTrace, traceGasS, wavelHRS, amfAltAveragedHR_cld,  &
                                    XsecAltAveragedHR_cld, XsecAltAveragedHRdiff_cld,   &
                                    slantAbsOptThicknessDiff_cld, absOptThickness)
      if (errorCheck(errS)) return
    else
      ! regard amf as a smooth function of the wavelength
      call calcSlantAbsOptThickness_fit(errS, nTrace, nwavelRTM, traceGasS, wavelHRS,               &
                                        amfAltAveragedHR_clr, XsecAltAveragedHR_clr,          &
                                        slantAbsOptThickness_clr, slantAbsOptThicknessDiff_clr)
      if (errorCheck(errS)) return
      call calcSlantAbsOptThickness_fit(errS, nTrace, nwavelRTM, traceGasS, wavelHRS,               &
                                        amfAltAveragedHR_cld, XsecAltAveragedHR_cld,          &
                                        slantAbsOptThickness_cld, slantAbsOptThicknessDiff_cld)
      if (errorCheck(errS)) return
    end if ! use_abs_opt_thickness_for_AMF

    ! Determine the wavelengths for the RTM grid, i.e. the wavelengths where the differential
    ! absorption optical thickness vanishes.

    call setWavelRTM(errS, nwavelRTM, wavelHRS, slantAbsOptThicknessDiff_clr, wavelRTMS_clr )
    if (errorCheck(errS)) return
    call setWavelRTM(errS, nwavelRTM, wavelHRS, slantAbsOptThicknessDiff_cld, wavelRTMS_cld )
    if (errorCheck(errS)) return

    ! Calculate the absorption cross sections at the RTM wavlength grid.

    call setXsecRTM(errS, nTrace, nwavelRTM, wavelHRS, XsecAltAveragedHRsmooth_clr, wavelRTMS_clr, XsecRTMS_clr)
    if (errorCheck(errS)) return
    call setXsecRTM(errS, nTrace, nwavelRTM, wavelHRS, XsecAltAveragedHRsmooth_cld, wavelRTMS_cld, XsecRTMS_cld)
    if (errorCheck(errS)) return

    ! Calculate the reflectance, air mass factors and derivatives on the RTM wavelength grid,
    ! i.e. for smooth part of the reflectance.

    call calcRelfAndDerivRTMgrid(errS, iband, nTrace, nwavelRTM, wavelRTMS_clr, wavelRTMS_cld, RRS_RingS, &
                                 XsecRTMS_clr, XsecRTMS_cld, gasPTS, traceGasS, surfaceS,                 &
                                 LambCloudS, mieAerS, mieCldS, cldAerFractionS, strayLightS,              &
                                 solarIrrS, cloudAerosolRTMgridS, OptPropRTMGridS, retrS,                 &
                                 geometryS, controlS, dn_dnodeS, reflDerivRTMS)
    if (errorCheck(errS)) return

    ! Evaluate smooth reflectance, amf, and derivatives on high resolution wavelength grid and
    ! put the values in reflDerivHRS.

    call evaluateSmoothValuesOnHRgrid(errS, use_abs_opt_thickn_for_AMF, nwavelRTM, wavelRTMS_clr, wavelRTMS_cld, &
                                      wavelHRS, reflDerivRTMS, absOptThickness, reflDerivHRS)
    if (errorCheck(errS)) return

    ! When accounting for differential absorption we can not use the geometrical air mass factor
    ! if there is strong absorption, e.g. for the ozone profile. Therefore we have to repeat the
    ! calculation of the altitude averaged absorption cross section and the differential slant
    ! absorption optical thickness for the first iteration, now using the smooth altitude resolved
    ! air mass factor calculated in subroutine evaluateSmoothValuesOnHRgrid.

    if ( retrS%numIterations == 1 ) then
      call calculateAltAveragedXsecHR(errS, nTrace, optPropRTMGridS, wavelHRS,            &
                                      XsecHRRTMaltS, reflDerivHRS,                  &
                                      amfAltAveragedHR_clr, amfAltAveragedHR_cld,   &
                                      XsecAltAveragedHR_clr, XsecAltAveragedHR_cld)
      if (errorCheck(errS)) return

      do iTrace = 1, nTrace
        call getSmoothAndDiffXsec(errS, nwavelRTM, wavelHRS%nwavel, wavelHRS%wavel, XsecAltAveragedHR_clr(:,iTrace), &
                                  XsecAltAveragedHRsmooth_clr(:,iTrace),  XsecAltAveragedHRdiff_clr(:,iTrace))
        if (errorCheck(errS)) return
        call getSmoothAndDiffXsec(errS, nwavelRTM, wavelHRS%nwavel, wavelHRS%wavel, XsecAltAveragedHR_cld(:,iTrace), &
                                  XsecAltAveragedHRsmooth_cld(:,iTrace),  XsecAltAveragedHRdiff_cld(:,iTrace))
        if (errorCheck(errS)) return
      end do ! iTrace

      if ( use_abs_opt_thickn_for_AMF ) then
        ! regard amf as a function of the absorption optical thickness
        call calcSlantAbsOptThickness(errS, nTrace, traceGasS, wavelHRS, amfAltAveragedHR_clr,  &
                                      XsecAltAveragedHR_clr, XsecAltAveragedHRdiff_clr,   &
                                      slantAbsOptThicknessDiff_clr, absOptThickness)
        if (errorCheck(errS)) return

        call calcSlantAbsOptThickness(errS, nTrace, traceGasS, wavelHRS, amfAltAveragedHR_cld,  &
                                      XsecAltAveragedHR_cld, XsecAltAveragedHRdiff_cld,   &
                                      slantAbsOptThicknessDiff_cld, absOptThickness)
        if (errorCheck(errS)) return
      else
        ! regard amf as a smooth function of the wavelength
        call calcSlantAbsOptThickness_fit(errS, nTrace, nwavelRTM, traceGasS, wavelHRS,              &
                                          amfAltAveragedHR_clr, XsecAltAveragedHR_clr,         &
                                          slantAbsOptThickness_clr, slantAbsOptThicknessDiff_clr)
        if (errorCheck(errS)) return
        call calcSlantAbsOptThickness_fit(errS, nTrace, nwavelRTM, traceGasS, wavelHRS,              &
                                          amfAltAveragedHR_cld, XsecAltAveragedHR_cld,         &
                                          slantAbsOptThickness_cld, slantAbsOptThicknessDiff_cld)
        if (errorCheck(errS)) return
      end if ! use_abs_opt_thickness_for_AMF

    end if ! retrS%numIterations == 1

    ! Deal with the differential absorption.

    call accountForDifferentialAbs(errS, iband, nTrace, wavelHRS, RRS_RingS, controlS, gasPTS, traceGasS, &
                                   optPropRTMGridS, surfaceS, LambCloudS, mieAerS, mieCldS,         &
                                   cldAerFractionS, mulOffsetS, straylightS, cloudAerosolRTMgridS,  &
                                   retrS, XsecHRS,  amfAltAveragedHR_clr, amfAltAveragedHR_cld,     &
                                   XsecAltAveragedHRdiff_clr, XsecAltAveragedHRdiff_cld,            &
                                   slantAbsOptThicknessDiff_clr, slantAbsOptThicknessDiff_cld,      &
                                   dn_dnodeS, reflDerivHRS)
    if (errorCheck(errS)) return

    call finishDismas (errS, nTrace, wavelRTMS_clr, wavelRTMS_cld, XsecRTMS_clr, XsecRTMS_cld, &
                       XsecHRRTMaltS, reflDerivRTMS)
    if (errorCheck(errS)) return

    if ( verbose ) then

      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') ' high-resolution reflectance and derivatives (for ln(vmr)) '
      write(addtionalOutputUnit,'(A)') '      wavel     bsca       babs        refl       derivatives  ....'
      do iwave = 1, wavelHRS%nwavel
        write(addtionalOutputUnit,'(3F12.6,F12.8,100E14.5)') wavelHRS%wavel(iwave),             &
                                                              reflDerivHRS%bsca(iwave),         &
                                                              reflDerivHRS%babs(iwave),         &
                                                              reflDerivHRS%refl(1,iwave),       &
                         ( reflDerivHRS%K_lnvmr(iwave,istate), istate = 1, reflDerivHRS%nstate )
      end do
    end if

    if ( verbose ) then
      do iTrace = 1, nTrace
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*) ' altitude averaged absorption cross sections on HR grid - clear part pixel'
        write(intermediateFileUnit,'(A)') &
          ' wavelength     XsecAltAveragedHR  XsecAltAveragedHRsmooth  XsecAltAveragedHRdiff'

        do iwave = 1, wavelHRS%nwavel
          write(intermediateFileUnit,'(I4,F10.4,ES21.5,2ES23.5)')  iTrace, wavelHRS%wavel(iwave),  &
                                                                XsecAltAveragedHR_clr(iwave,iTrace),  &
                                                                XsecAltAveragedHRsmooth_clr(iwave,iTrace),  &
                                                                XsecAltAveragedHRdiff_clr(iwave,iTrace)
        end do ! iwave

        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*) ' altitude averaged absorption cross sections on HR grid - cloudy part pixel'
        write(intermediateFileUnit,'(A)') &
          ' wavelength     XsecAltAveragedHR  XsecAltAveragedHRsmooth  XsecAltAveragedHRdiff'

        do iwave = 1, wavelHRS%nwavel
          write(intermediateFileUnit,'(I4,F10.4,ES21.5,2ES23.5)') iTrace,  wavelHRS%wavel(iwave),  &
                                                                XsecAltAveragedHR_cld(iwave,iTrace),  &
                                                                XsecAltAveragedHRsmooth_cld(iwave,iTrace),  &
                                                                XsecAltAveragedHRdiff_cld(iwave,iTrace)
        end do ! iwave
      end do ! iTrace
    end if ! verbose


  end subroutine dismas


  subroutine initializeDismas(errS, nTrace, nwavelRTM, geometryS, traceGasS, optPropRTMGridS,   &
                              XsecHRS, retrS, wavelRTMS_clr, wavelRTMS_cld,               &
                              XsecRTMS_clr, XsecRTMS_cld, XsecHRRTMaltS, controlS,        &
                              reflDerivRTMS, reflDerivHRS)

    ! Purpose of this subroutime is to claim memory for the arrays in the structures
    ! wavelRTMS, XsecRTMS, and XsecHRRTMaltS and to initialize them.
    ! Futher, the air mass factors amfAltRTM are initialized with geometrical air mass factors

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                  intent(in)    :: nTrace                 ! number of trace gases
    integer,                  intent(in)    :: nwavelRTM              ! #wavelengths for RTM calculations
    type(geometryType),       intent(in)    :: geometryS              ! info geometry
    type(traceGasType),       intent(in)    :: traceGasS(nTrace)      ! properties of the trace gases
    type(optPropRTMGridType), intent(in)    :: optPropRTMGridS        ! optical properties on RTM grid
    type(XsecType),           intent(in)    :: XsecHRS(nTrace)        ! absorption cross section
    type(retrType),           intent(in)    :: retrS                  ! retrieval structure
    type(wavelHRType),        intent(inout) :: wavelRTMS_clr          ! wavelengths for RTM calculations
    type(wavelHRType),        intent(inout) :: wavelRTMS_cld          ! wavelengths for RTM calculations
    type(XsecType),           intent(inout) :: XsecRTMS_clr(nTrace)   ! Xsec on RTM wavelength grid
    type(XsecType),           intent(inout) :: XsecRTMS_cld(nTrace)   ! Xsec on RTM wavelength grid
    type(XsecType),           intent(inout) :: XsecHRRTMaltS(nTrace)  ! Xsec on HR wavelength and RTM altitude grid
    type(controlType),        intent(inout) :: controlS               ! control parameters for RTM
    type(reflDerivType),      intent(inout) :: reflDerivRTMS          ! reflectance and derivatives on RTM grid
    type(reflDerivType),      intent(inout) :: reflDerivHRS           ! reflectance and derivatives on HR grid

    ! local
    integer :: iTrace
    real(8) :: geometricalAMF
    integer :: allocStatus

    wavelRTMS_clr%nGaussMax = 0
    wavelRTMS_clr%nGaussMin = 0
    wavelRTMS_clr%nwavel    = nwavelRTM
    wavelRTMS_cld%nGaussMax = 0
    wavelRTMS_cld%nGaussMin = 0
    wavelRTMS_cld%nwavel    = nwavelRTM

    ! fill values associated with the cross sections XsecRTMS
    do iTrace = 1, nTrace
      XsecRTMS_clr(iTrace)%XsectionFileName = XsecHRS(iTrace)%XsectionFileName
      XsecRTMS_clr(iTrace)%useHITRAN        = XsecHRS(iTrace)%useHITRAN
      XsecRTMS_clr(iTrace)%factorLM         = XsecHRS(iTrace)%factorLM
      XsecRTMS_clr(iTrace)%gasIndex         = XsecHRS(iTrace)%gasIndex
      XsecRTMS_clr(iTrace)%ISO              = XsecHRS(iTrace)%ISO
      XsecRTMS_clr(iTrace)%thresholdLine    = XsecHRS(iTrace)%thresholdLine
      XsecRTMS_clr(iTrace)%nwavelHR         = XsecHRS(nTrace)%nwavelHR
      XsecRTMS_clr(iTrace)%nwavel           = XsecHRS(nTrace)%nwavel
      XsecRTMS_clr(iTrace)%nalt             = traceGasS(iTrace)%nalt
    end do ! iTrace
    do iTrace = 1, nTrace
      XsecRTMS_cld(iTrace)%XsectionFileName = XsecHRS(iTrace)%XsectionFileName
      XsecRTMS_cld(iTrace)%useHITRAN        = XsecHRS(iTrace)%useHITRAN
      XsecRTMS_cld(iTrace)%factorLM         = XsecHRS(iTrace)%factorLM
      XsecRTMS_cld(iTrace)%gasIndex         = XsecHRS(iTrace)%gasIndex
      XsecRTMS_cld(iTrace)%ISO              = XsecHRS(iTrace)%ISO
      XsecRTMS_cld(iTrace)%thresholdLine    = XsecHRS(iTrace)%thresholdLine
      XsecRTMS_cld(iTrace)%nwavelHR         = XsecHRS(nTrace)%nwavelHR
      XsecRTMS_cld(iTrace)%nwavel           = XsecHRS(nTrace)%nwavel
      XsecRTMS_cld(iTrace)%nalt             = traceGasS(iTrace)%nalt
    end do ! iTrace

    ! fill values associated with the cross sections XsecHRRTMaltS
    do iTrace = 1, nTrace
      XsecHRRTMaltS(iTrace)%XsectionFileName = XsecHRS(iTrace)%XsectionFileName
      XsecHRRTMaltS(iTrace)%useHITRAN        = XsecHRS(iTrace)%useHITRAN
      XsecHRRTMaltS(iTrace)%factorLM         = XsecHRS(iTrace)%factorLM
      XsecHRRTMaltS(iTrace)%gasIndex         = XsecHRS(iTrace)%gasIndex
      XsecHRRTMaltS(iTrace)%ISO              = XsecHRS(iTrace)%ISO
      XsecHRRTMaltS(iTrace)%thresholdLine    = XsecHRS(iTrace)%thresholdLine
      XsecHRRTMaltS(iTrace)%nwavelHR         = XsecHRS(nTrace)%nwavelHR
      XsecHRRTMaltS(iTrace)%nwavel           = XsecHRS(nTrace)%nwavel
      XsecHRRTMaltS(iTrace)%nalt             = optPropRTMGridS%RTMnlayer
    end do ! iTrace

    ! allocate arrays in structures
    call claimMemWavelHRS(errS,  wavelRTMS_clr )
    if (errorCheck(errS)) return
    call claimMemWavelHRS(errS,  wavelRTMS_cld )
    if (errorCheck(errS)) return
    do iTrace = 1, nTrace
      call claimMemXsecS(errS,  XsecRTMS_clr(iTrace) )
      if (errorCheck(errS)) return
      call claimMemXsecS(errS,  XsecRTMS_cld(iTrace) )
      if (errorCheck(errS)) return
      call claimMemXsecS(errS,  XsecHRRTMaltS(iTrace) )
      if (errorCheck(errS)) return
    end do ! iTrace

    ! initialize the air mass factors on the HR grid to the geometrical air mass factor for iteration 1
    if ( retrS%numIterations == 1 ) then
      geometricalAMF = 1.0d0 / geometryS%uu + 1.0d0 / geometryS%u0
      reflDerivHRS%altResAMFabs_clr = geometricalAMF
      reflDerivHRS%altResAMFabs_cld = geometricalAMF
    end if

    ! initialize and claim memory for reflDerivRTMS

    reflDerivRTMS%RTMnlayer  = optPropRTMGridS%RTMnlayer
    reflDerivRTMS%nwavel     = nwavelRTM
    reflDerivRTMS%nstate     = retrS%nstate
    reflDerivRTMS%nstateCol  = retrS%nstateCol
    reflDerivRTMS%nTrace     = nTrace
    reflDerivRTMS%nGaussCol  = retrS%nGaussCol
    reflDerivRTMS%dimSV      = controlS%dimSV

    call claimMemReflDerivS(errS,  reflDerivRTMS )
    if (errorCheck(errS)) return

    ! in claimMemReflDerivS the array reflDerivRTMS%KHR_ndensCol is not allocated
    ! it must have the dimensions (0:reflDerivRTMS%nwavel, reflDerivRTMS%nstateCol)
    allocate ( reflDerivRTMS%KHR_ndensCol(reflDerivRTMS%nwavel, reflDerivRTMS%nstateCol), STAT = allocStatus )
    allocate ( reflDerivRTMS%KHR_clr_ndensCol(reflDerivRTMS%nwavel, reflDerivRTMS%nstateCol), STAT = allocStatus )
    allocate ( reflDerivRTMS%KHR_cld_ndensCol(reflDerivRTMS%nwavel, reflDerivRTMS%nstateCol), STAT = allocStatus )

    if ( allocStatus /= 0 ) then
      call logDebug('Error in dismasModule: subroutine initializeDismas')
      call logDebug('allocation of reflDerivRTMS%KHR_ndensCol failed')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if

  end subroutine initializeDismas


  subroutine calculateAltAveragedXsecHR(errS, nTrace, optPropRTMGridS, wavelHRS,      &
                                        XsecHRRTMaltS, reflDerivHRS,                  &
                                        amfAltAveragedHR_clr, amfAltAveragedHR_cld,   &
                                        XsecAltAveragedHR_clr, XsecAltAveragedHR_cld)

    ! Calculate the monochromatic altitude averaged absorption cross section weighted with the
    ! number density of the trace gas and the altitude resolved air mass factor on the high resolution grid.
    ! the results are stored in the XsecAltAveragedHR.

    ! It is assumed that the monochromatic altitude resolved air mass factors are known.

    implicit none

    type(errorType),          intent(inout) :: errS
    integer,                  intent(in)    :: nTrace                     ! number of trace gases
    type(optPropRTMGridType), intent(in)    :: optPropRTMGridS            ! contains altitude grid and weights
    type(wavelHRType),        intent(in)    :: wavelHRS                   ! instrument wavelength grid
    type(XsecType),           intent(in)    :: XsecHRRTMaltS(nTrace)      ! Xsec on HR wavelength grid and RTM alt grid
    type(reflDerivType),      intent(in)    :: reflDerivHRS               ! reflectance, amf, derivatives on HR grid
    real(8),                  intent(out)   :: amfAltAveragedHR_clr (wavelHRS%nwavel,nTrace)
    real(8),                  intent(out)   :: amfAltAveragedHR_cld (wavelHRS%nwavel,nTrace)
    real(8),                  intent(out)   :: XsecAltAveragedHR_clr(wavelHRS%nwavel,nTrace)
    real(8),                  intent(out)   :: XsecAltAveragedHR_cld(wavelHRS%nwavel,nTrace)

    ! local

    integer :: ialt, iwave, iTrace
    real(8) :: column, amf_clr, amf_cld, s0, s1_clr, s1_cld, s2_clr, s2_cld

    logical, parameter :: verbose = .false.

    ! Calculate the altitude averaged absorption cross section by integration over altitude
    ! weighted with the number density of the trace gas and the altitude resolved air mass factor.

    ! The altitude resolved air mass factor is stored in reflDerivHRS and is initialized with
    ! the geometrical air mass factor in sbroutine initializeDismas for the first iteration.
    ! Therefore the amf is known from previous calls to dismas during the iterations.

    do iTrace = 1, nTrace
      do iwave = 1, wavelHRS%nwavel
        amf_clr  = 0.0d0
        amf_cld  = 0.0d0
        column   = 0.0d0
        XsecAltAveragedHR_clr(iwave,iTrace) = 0.0d0
        XsecAltAveragedHR_cld(iwave,iTrace) = 0.0d0
        do ialt = 0, optPropRTMGridS%RTMnlayer
          s0 = optPropRTMGridS%ndensGas(ialt,itrace) * optPropRTMGridS%RTMweight(ialt)
          column = column + s0
          s1_clr  = s0 * reflDerivHRS%altResAMFabs_clr(iwave, ialt)
          s1_cld  = s0 * reflDerivHRS%altResAMFabs_cld(iwave, ialt)
          amf_clr = amf_clr + s1_clr
          amf_cld = amf_cld + s1_cld
          s2_clr = s1_clr * XsecHRRTMaltS(iTrace)%Xsec(iwave,ialt)
          s2_cld = s1_cld * XsecHRRTMaltS(iTrace)%Xsec(iwave,ialt)
          XsecAltAveragedHR_clr(iwave,iTrace) = XsecAltAveragedHR_clr(iwave,iTrace) + s2_clr
          XsecAltAveragedHR_cld(iwave,iTrace) = XsecAltAveragedHR_cld(iwave,iTrace) + s2_cld
        end do ! ialt
        ! normalize
        XsecAltAveragedHR_clr(iwave,iTrace) = XsecAltAveragedHR_clr(iwave,iTrace) / amf_clr
        XsecAltAveragedHR_cld(iwave,iTrace) = XsecAltAveragedHR_cld(iwave,iTrace) / amf_cld
        amfAltAveragedHR_clr (iwave,iTrace) = amf_clr / column
        amfAltAveragedHR_cld (iwave,iTrace) = amf_cld / column

        if (verbose) then
          write(intermediateFileUnit,'(A, I4, 2E15.5)') 'iTrace XsecAltAveragedHR_clr amfAltAveragedHR_clr', &
             iTrace,  XsecAltAveragedHR_clr(iwave,iTrace),  amfAltAveragedHR_clr (iwave,iTrace)
          write(intermediateFileUnit,'(A, I4, 2E15.5)') 'iTrace XsecAltAveragedHR_cld amfAltAveragedHR_cld', &
             iTrace,  XsecAltAveragedHR_cld(iwave,iTrace),  amfAltAveragedHR_cld (iwave,iTrace)
        end if
      end do ! iwave
    end do !iTrace

  end subroutine calculateAltAveragedXsecHR


  subroutine calcSlantAbsOptThickness(errS, nTrace, traceGasS, wavelHRS, amfAltAveragedHR, &
                                      XsecAltAveragedHR, XsecAltAveragedHRdiff,      &
                                      slantAbsOptThicknessDiff, absOptThickness)

   ! The slant differential absorption optical thickness is calculated on the HR wavelength grid.
   ! - Note that we can not fit a low degree polynomial when M(lamda) is not a smooth function
   !   of the wavelength => use differential cross sections of the trace gases

    implicit none

      type(errorType), intent(inout) :: errS
    integer,               intent(in)    :: nTrace                         ! number of trace gases
    type(traceGasType),    intent(in)    :: traceGasS(nTrace)              ! trace gas properties, e.g. total column
    type(wavelHRType),     intent(in)    :: wavelHRS                       ! HR wavelength grid
    real(8),               intent(in)    :: amfAltAveragedHR     (wavelHRS%nwavel,nTrace)
    real(8),               intent(in)    :: XsecAltAveragedHR    (wavelHRS%nwavel,nTrace)
    real(8),               intent(in)    :: XsecAltAveragedHRdiff(wavelHRS%nwavel,nTrace)
    real(8),               intent(out)   :: slantAbsOptThicknessDiff(wavelHRS%nwavel)
    real(8),               intent(out)   :: absOptThickness         (wavelHRS%nwavel)

    ! local
    integer :: iTrace, iwave

    logical, parameter :: verbose = .false.

    absOptThickness(:)          = 0.0d0
    slantAbsOptThicknessDiff(:) = 0.0d0

    do iTrace = 1, nTrace

        do iwave = 1, wavelHRS%nwavel
          absOptThickness(iwave)      = absOptThickness(iwave) + traceGasS(iTrace)%column       &
                                                               * XsecAltAveragedHR(iwave,iTrace)
          slantAbsOptThicknessDiff(iwave) = slantAbsOptThicknessDiff(iwave)    &
                                          + amfAltAveragedHR(iwave,iTrace)     &
                                          * traceGasS(iTrace)%column           &
                                          * XsecAltAveragedHRdiff(iwave,iTrace)
        end do ! iwave

    end do ! iTrace

    if ( verbose ) then
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*) 'dismas output: differential slant absorption optical thickness'
        write(intermediateFileUnit,*) 'wavelength        M*N*s_diff '
        do iwave = 1, wavelHRS%nwavel
          write(intermediateFileUnit,'(F12.6, 2F15.8)') wavelHRS%wavel(iwave), slantAbsOptThicknessDiff(iwave)
        end do
    end if

  end subroutine calcSlantAbsOptThickness


  subroutine calcSlantAbsOptThickness_fit(errS, nTrace, nwavelRTM, traceGasS, wavelHRS,  &
                                          amfAltAveragedHR, XsecAltAveragedHR,     &
                                          slantAbsOptThickness, slantAbsOptThicknessDiff)

   ! The following steps are taken
   ! - The slant absorption optical thickness is calculated on the HR wavelength grid.
   ! - A low degree polynomial (Legendre polynominals) is fitted to the absorption optical thickness.
   ! - The differential part of the slant absorption optical thickness is calculated.
   ! - NOTE: R = Rs * exp(-tau_slant) => if we use a polynomial for ln(Rs) we should not
   !   fit a polynomial to ln(tau_slant), but to tau_slant itself

    implicit none

      type(errorType), intent(inout) :: errS
    integer,               intent(in)    :: nTrace                         ! number of trace gases
    integer,               intent(in)    :: nwavelRTM                      ! number wavelength for RTM calculations
    type(traceGasType),    intent(in)    :: traceGasS(nTrace)              ! trace gas properties, e.g. total column
    type(wavelHRType),     intent(in)    :: wavelHRS                       ! HR wavelength grid
    real(8),               intent(in)    :: amfAltAveragedHR (wavelHRS%nwavel,nTrace)
    real(8),               intent(in)    :: XsecAltAveragedHR(wavelHRS%nwavel,nTrace)
    real(8),               intent(out)   :: slantAbsOptThickness    (wavelHRS%nwavel)
    real(8),               intent(out)   :: slantAbsOptThicknessDiff(wavelHRS%nwavel)

    ! local
    integer :: iTrace, iwave
    real(8) :: wStart, wEnd

    ! arrays for fitting polynomial to the slant absorption optical thickness

    real(8) :: sig(wavelHRS%nwavel)
    real(8) :: polynomial(wavelHRS%nwavel)
    real(8) :: wavelScaled(wavelHRS%nwavel)

    real(8) :: a(nwavelRTM )
    real(8) :: w(nwavelRTM )
    real(8) :: v(nwavelRTM, nwavelRTM)

    real(8) :: chisq  ! chi**2 of the fit

    logical, parameter :: verbose = .false.

    slantAbsOptThickness(:) = 0.0d0

    do iTrace = 1, nTrace

        do iwave = 1, wavelHRS%nwavel
          slantAbsOptThickness(iwave) = slantAbsOptThickness(iwave) + amfAltAveragedHR(iwave,iTrace)     &
                                                                    * traceGasS(iTrace)%column           &
                                                                    * XsecAltAveragedHR(iwave,iTrace)
        end do ! iwave

    end do ! iTrace

    ! Scale the wavelength grid to (-1, +1) for the high resolution wavelength grid.
    wStart = wavelHRS%wavel(1)
    wEnd   = wavelHRS%wavel( wavelHRS%nwavel )
    wavelScaled(:) = 2.0d0 * ( wavelHRS%wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0

    ! assume constant errors for the least squares polynomial fit
    sig(:) = 1.0d0

    ! Fit low degree Legendre polynomial to the slant absorption optical thickness.

    call svdfit(errS, wavelScaled, slantAbsOptThickness, sig, a, v, w, chisq, fleg)
    if (errorCheck(errS)) return

    ! Evaluate the smooth part of the slant absorption optical thickness
    do iwave = 1, wavelHRS%nwavel
      polynomial(iwave) = dot_product( a, fleg(wavelScaled(iwave),nwavelRTM) )
    end do ! iwave

    ! Calculate the differential part of the  slant absorption optical thhickness.
    slantAbsOptThicknessDiff(:) = slantAbsOptThickness(:) - polynomial(:)

    if ( verbose ) then
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*) 'dismas output: total and differential slant absorption optical thickness'
        write(intermediateFileUnit,*) 'wavelength        M*N*s     M*N*s_diff '
        do iwave = 1, wavelHRS%nwavel
          write(intermediateFileUnit,'(F12.6, 2F15.8)') wavelHRS%wavel(iwave), slantAbsOptThickness(iwave), &
                                                        slantAbsOptThicknessDiff(iwave)
        end do
        write(intermediateFileUnit,*) 'dismas output: wavelength amfAltAveraged   XsecAltAveraged (first trace gas)'
        do iwave = 1, wavelHRS%nwavel
          write(intermediateFileUnit,'(F12.6, F15.8, E15.5)') wavelHRS%wavel(iwave), amfAltAveragedHR(iwave,1), &
                                                        XsecAltAveragedHR(iwave,1)
        end do
    end if

  end subroutine calcSlantAbsOptThickness_fit


  subroutine setWavelRTM(errS, nwavelRTM, wavelHRS, slantAbsOptThicknessDiff, wavelRTMS )

   ! - Wavelengths corresponding to zero values of the differential slant optical thickness
   !   are calculated and stored in wavelSignChanges.
   ! - Based on the value of nwavelRTM a subset of wavelengths is selected from wavelSignChanges.
   !   These are used for the radiative transfer calculations and stored in wavelRTMS.

    implicit none

      type(errorType), intent(inout) :: errS
    integer,               intent(in)    :: nwavelRTM                      ! number wavelength for RTM calculations
    type(wavelHRType),     intent(in)    :: wavelHRS                       ! HR wavelength grid
    real(8),               intent(in)    :: slantAbsOptThicknessDiff(wavelHRS%nwavel)
    type(wavelHRType),     intent(inout) :: wavelRTMS                      ! wavelength grid for RTM calculations

    ! local
    integer :: iwave, iPoly
    real(8) :: step, wStart, wEnd, wMiddle, wInterval
    integer :: indLoc(1)

    ! parameters for finding the zeros of the total differential absorption optical thickness
    integer, parameter :: maxSignChanges = 800
    integer            :: numSignChanges
    real(8)            :: wavelSignChanges( maxSignChanges )

    ! control output
    logical, parameter :: verbose = .false.

    ! find the wavelengths where the differential slant absorption changes sign
    wavelSignChanges = 0.0d0
    numSignChanges   = 1
    do iwave = 2, wavelHRS%nwavel
      if ( slantAbsOptThicknessDiff(iwave-1) * slantAbsOptThicknessDiff(iwave) < 0.0d0 ) then
        ! use inverse linear interpolation to find the wavelength where the zero occurs
        wavelSignChanges(numSignChanges) = wavelHRS%wavel(iwave-1)    &
               + ( wavelHRS%wavel(iwave-1) - wavelHRS%wavel(iwave) )  &
               * slantAbsOptThicknessDiff(iwave-1)                    &
               / ( slantAbsOptThicknessDiff(iwave) - slantAbsOptThicknessDiff(iwave-1))

        numSignChanges = numSignChanges + 1
        if ( numSignChanges > maxSignChanges ) then
          call logDebug('ERROR: dismasModule:setWavelRTM: maxSignChanges is too small')
          call mystop(errS, 'stopped because maxSignChanges is too small')
          if (errorCheck(errS)) return
        end if

      end if
    end do ! iwave

    if ( numSignChanges <= nwavelRTM ) then
      call logDebug('ERROR in dismasModule subroutine setWavelRTM')
      call logDebug('not enough sign changes to approximate the reflectance')
      call logDebugI('with a polynomial of degree: ', nwavelRTM)
      call logDebugI('number of sign changes found = ', numSignChanges)
      call mystop(errS, 'stopped because there are not enough sign changes')
      if (errorCheck(errS)) return
    end if

    ! The wavelengths where the differential absorption optical thickness is zero are now known,
    ! but we do not have to perform radiative transfer calculations for all of them. The minimum number
    ! required is nwavelRTM + 1. Now we select the appropriate wavelengths.

    ! use select case for nwavelRTM
    ! if nwavelRTM = 0 select wavelength closest to the middle of the fit interval
    ! if nwavelRTM = 1 select smallest and largest wavelengths
    ! if nwavelRTM = 2 select smallest and largest wavelengths, and the wavelength closest to the middle
    ! if nwavelRTM > 2 divide the fit interval into nwavelRTM subintervals of equal length and find
    ! the wavelengths closest to the end points of the intervals

    ! fill wavelengths

    if ( numSignChanges == nwavelRTM ) then

      ! there are exactly the right number of sign changes => use all.
      wavelRTMS%wavel(1:nwavelRTM) = wavelSignChanges(1:nwavelRTM)

    else

      ! select wavelengths
      wStart = wavelHRS%wavel(1)
      wEnd   = wavelHRS%wavel( wavelHRS%nwavel )

      select case (nwavelRTM)

        case ( 1 )  ! value close to the middle of the interval

          wMiddle = wStart + 0.5d0* ( wEnd - wStart )
          indLoc = minLoc(abs( wavelSignChanges - wMiddle ) )
          ! subtract one because wavelSignChanges starts at 0 and indLoc assumes it starts at 1
          wavelRTMS%wavel(1) = wavelSignChanges( indLoc(1) )

        case ( 2:100 )

          step =  ( wEnd - wStart ) / (nwavelRTM - 1)
          do iPoly = 1, nwavelRTM
            wInterval = wStart + (iPoly - 1) * step
            indLoc = minLoc(abs( wavelSignChanges - wInterval ) )
            wavelRTMS%wavel(iPoly) =  wavelSignChanges( indLoc(1) )
            ! if wavelength is the same as the previous wavelength take the next
            if ( iPoly > 1 ) then
              if (  abs( wavelRTMS%wavel(iPoly) -  wavelRTMS%wavel(iPoly - 1) )  < 0.1  ) &
               wavelRTMS%wavel(iPoly) =  wavelSignChanges( indLoc(1) + 1)
            end if
          end do

        case default

          call logDebugI('incorrect value for nwavelRTM : ', nwavelRTM)
          call logDebug('in dismasModule : subroutine setWavelRTM ')
          call mystop(errS, 'stopped due to incorrect value for nwavelRTM')
          if (errorCheck(errS)) return

      end select

    end if ! numSignChanges == nwavelRTM

    write(intermediateFileUnit,'(A,40F17.10)') 'wavelength = ', wavelRTMS%wavel

    ! Verify that all wavelengths are different within 0.1 nm.

    if ( .not. verfy_elem_differ(wavelRTMS%wavel, 0.1d0) ) then

      call logDebug('ERROR in dismasModule : subroutine setWavelRTM ')
      call logDebug('selected wavelengths for RTM calculations are (nearly) the same  ')
      write(errS%temp,'(A, 50F12.5)') 'wavelengths : ', wavelRTMS%wavel
      call errorAddLine(errS, errS%temp)
      call mystop(errS, 'stopped due to (nearly) the same wavelengths')
      if (errorCheck(errS)) return

    end if

    if ( verbose ) then
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'DISMAS wavelengths where sign changes'
      write(addtionalOutputUnit,*) 'numSignChanges = ', numSignChanges - 1
      write(addtionalOutputUnit,'(300F12.5)') (wavelSignChanges(iwave), iwave = 1, numSignChanges - 1)
    end if

  end subroutine setWavelRTM


  subroutine setXsecRTM(errS, nTrace, nwavelRTM, wavelHRS, XsecAltAveragedHRsmooth, wavelRTMS, XsecRTMS)

   ! The radiative transfer calculations for the few wavelengths calculated in setWavelRTM
   ! are performed using the smooth part of the altitude averaged absorption cross section.
   ! These absorption cross sections are calculated using interpolation on the
   ! values given on the HR wavelength grid and stored in XsecRTMS. Note that
   ! the altitude dependence of the absorption cross section has vanished.

    implicit none

      type(errorType), intent(inout) :: errS
    integer,               intent(in)    :: nTrace                         ! number of trace gases
    integer,               intent(in)    :: nwavelRTM                      ! # wavelengths for RTM calculations
    type(wavelHRType),     intent(in)    :: wavelHRS                       ! HR wavelength grid
    real(8),               intent(in)    :: XsecAltAveragedHRsmooth(wavelHRS%nwavel,nTrace)
    type(wavelHRType),     intent(in)    :: wavelRTMS                      ! RTM wavelength grid
    type(XsecType),        intent(inout) :: XsecRTMS(nTrace)               ! DOAS absorption cross sections

    ! local
    integer :: iTrace, iwave
    integer :: ialt, status
    real(8) :: SDabs(wavelHRS%nwavel)

    ! control output
    logical, parameter :: verbose = .false.

    do iTrace = 1, nTrace
      call spline(errS, wavelHRS%wavel, XsecAltAveragedHRsmooth(:,iTrace), SDabs, status)
      if (errorCheck(errS)) return
      do iwave = 1, nwavelRTM
        XsecRTMS(iTrace)%Xsec(iwave,0) = splint(errS, wavelHRS%wavel, XsecAltAveragedHRsmooth(:,iTrace), &
                                                SDabs, wavelRTMS%wavel(iwave), status)
        XsecRTMS(iTrace)%dXsecdT(iwave,0) = 0.0d0   !!!!!!! BETTER SOLUTION THAN SETTING TO ZERO JdH debug
      end do ! iwave

      ! No altitude dependence because the altitude averaged absorption cross section is used.
      ! Copy values for altitude index = 0 to other altitudes.
      do ialt = 1, XsecRTMS(iTrace)%nalt
        do iwave = 1,nwavelRTM
          XsecRTMS(iTrace)%Xsec(iwave,ialt) = XsecRTMS(iTrace)%Xsec(iwave,0)
          XsecRTMS(iTrace)%dXsecdT(iwave,ialt) = 0.0d0   !!!!!!! BETTER SOLUTION THAN SETTING TO ZERO JdH debug
        end do ! iwave
      end do ! ialt
    end do ! iTrace

    if ( verbose ) then
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'DISMAS output from setXsecRTM'
      write(addtionalOutputUnit,*) 'nwavelRTM = ', nwavelRTM
      do iTrace = 1, nTrace
        write(addtionalOutputUnit,*) 'iTrace = ', iTrace
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'values of XsecAltAveragedHRsmooth'
        write(addtionalOutputUnit,*) 'wavelength   Xsec'
        do iwave = 1, wavelHRS%nwavel
          write(addtionalOutputUnit,'(F10.5, E15.5)') wavelHRS%wavel(iwave), XsecAltAveragedHRsmooth(iwave,iTrace)
        end do ! iwave
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'values of XsecRTMS(iTrace)%Xsec(iwave,0)'
        write(addtionalOutputUnit,'(12E15.5)') (XsecRTMS(iTrace)%Xsec(iwave,0), iwave = 1, nwavelRTM)
      end do ! iTrace
    end if ! verbose

  end subroutine setXsecRTM


  subroutine  calcRelfAndDerivRTMgrid(errS, iband, nTrace, nwavelRTM, wavelRTMS_clr, wavelRTMS_cld, RRS_RingS, &
                                      XsecRTMS_clr, XsecRTMS_cld, gasPTS, traceGasS, surfaceS, LambCloudS,     &
                                      mieAerS, mieCldS, cldAerFractionS, strayLightS, solarIrrS,               &
                                      cloudAerosolRTMgridS, OptPropRTMGridS, retrS, geometryS, controlS,       &
                                      dn_dnodeS, reflDerivRTMS)

    ! Calculate the reflectance and derivatives with respect to the state vector elements
    ! on the RTM wavelength grid.

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                       intent(in)    :: iband                     ! number wavelength band
    integer,                       intent(in)    :: nTrace                    ! number of trace gases
    integer,                       intent(in)    :: nwavelRTM                 ! number wavelengths for RTM calculations
    type(wavelHRType),             intent(in)    :: wavelRTMS_clr             ! wavelength grid for RTM calculations
    type(wavelHRType),             intent(in)    :: wavelRTMS_cld             ! wavelength grid for RTM calculations
    type(RRS_RingType),            intent(in)    :: RRS_RingS                 ! Raman / Ring info
    type(XsecType),                intent(in)    :: XsecRTMS_clr(nTrace)      ! Absorption cross sections on RTM grid
    type(XsecType),                intent(in)    :: XsecRTMS_cld(nTrace)      ! Absorption cross sections on RTM grid
    type(gasPTType),               intent(in)    :: gasPTS                    ! atmospheric pressure and temperature
    type(traceGasType),            intent(inout) :: traceGasS(nTrace)         ! atmospheric trace gas properties
    type(LambertianType),          intent(inout) :: surfaceS                  ! surface properties for a spectral band
    type(LambertianType),          intent(inout) :: LambCloudS                ! Lambertian cloud properties
    type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)  ! specification expansion coefficients
    type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)  ! specification expansion coefficients
    type(cldAerFractionType),      intent(inout) :: cldAerFractionS           ! cloud / aerosol fraction structure
    type(straylightType),          intent(inout) :: strayLightS               ! stray light
    type(SolarIrrType),            intent(inout) :: solarIrrS                 ! solar irradiance
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS      ! cloud-aerosol properties on RTM altitude grid
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS           ! optical properties on RTM grid
    type(retrType),                intent(in)    :: retrS                     ! contains info on the fit parameters
    type(geometryType),            intent(in)    :: geometryS                 ! geometrical information for simulation and retrieval
    type(controlType),             intent(inout) :: controlS                  ! control parameters for radiative transfer
    type(dn_dnodeType),            intent(inout) :: dn_dnodeS(0:nTrace)       ! account for spline interpolation for derivatives
    type(reflDerivType),           intent(inout) :: reflDerivRTMS             ! reflectance and derivatives on RTM grid

    ! local
    logical :: cloudyPart
    integer :: iwave, istate, iTrace, statusRTM, iSV

    real(8) :: refl_clear(controlS%dimSV)
    real(8) :: refl_Cloud(controlS%dimSV)

    real(8) :: contribRefl_Clear(controlS%dimSV,0:optPropRTMGridS%RTMnlayer)
    real(8) :: contribRefl_Cloud(controlS%dimSV,0:optPropRTMGridS%RTMnlayer)

    type(UDType)  :: UD_Clear(0:optPropRTMGridS%RTMnlayer)
    type(UDType)  :: UD_Cloud(0:optPropRTMGridS%RTMnlayer)

    real(8) :: wfInterfkscaGas_clear(0:optPropRTMGridS%RTMnlayer)
    real(8) :: wfInterfkscaGas_Cloud(0:optPropRTMGridS%RTMnlayer)

    real(8) :: wfInterfkscaAerAbove_clear(0:optPropRTMGridS%RTMnlayer)
    real(8) :: wfInterfkscaAerBelow_clear(0:optPropRTMGridS%RTMnlayer)
    real(8) :: wfInterfkscaAerAbove_Cloud(0:optPropRTMGridS%RTMnlayer)
    real(8) :: wfInterfkscaAerBelow_Cloud(0:optPropRTMGridS%RTMnlayer)

    real(8) :: wfInterfkscaCldAbove_clear(0:optPropRTMGridS%RTMnlayer)
    real(8) :: wfInterfkscaCldBelow_clear(0:optPropRTMGridS%RTMnlayer)
    real(8) :: wfInterfkscaCldAbove_Cloud(0:optPropRTMGridS%RTMnlayer)
    real(8) :: wfInterfkscaCldBelow_Cloud(0:optPropRTMGridS%RTMnlayer)

    real(8) :: wfInterfkabs_clear        (0:optPropRTMGridS%RTMnlayer)
    real(8) :: wfInterfkabs_Cloud        (0:optPropRTMGridS%RTMnlayer)

    real(8) :: wfSurfaceAlbedo_clear
    real(8) :: wfSurfaceAlbedo_Cloud

    real(8) :: wfSurfaceEmission_clear
    real(8) :: wfSurfaceEmission_Cloud

    real(8) :: wfCloudAlbedo

    ! weighting functions for the cloud fraction
    real(8) :: wfCloudFraction
    real(8) :: cf, cfMax, cfMin                   ! cld/aer fraction, maximal and minimal fractions

    real(8) :: surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave

    real(8), parameter :: PI = 3.141592653589793d0

    logical, parameter :: verbose = .false.

    call allocateUD(errS, geometryS%nmutot, controlS%dimSV, optPropRTMGridS%RTMnlayer, UD_Clear, UD_Cloud)
    if (errorCheck(errS)) return

    call fillAltPresGridRTM(errS, cloudAerosolRTMgridS, OptPropRTMGridS)
    if (errorCheck(errS)) return

    if ( controlS%calculateDerivatives ) then
      if ( (retrS%numIterations == 1 ) .OR. cloudAerosolRTMgridS%fitIntervalDP    &
                                       .OR. cloudAerosolRTMgridS%fitIntervalTop   &
                                       .OR. cloudAerosolRTMgridS%fitIntervalBot ) then
        ! calculate the change in a profile if the value at a node changes; index 0 is for air
        call calculate_dn_dnode(errS, gasPTS%npressureNodes, gasPTS%altNodes, optPropRTMGridS%RTMnlayerSub, &
                                optPropRTMGridS%RTMaltitudeSub, dn_dnodeS(0)%dn_dnode, gasPTS%useLinInterp)
        if (errorCheck(errS)) return
        do iTrace = 1, nTrace
          call calculate_dn_dnode(errS, traceGasS(iTrace)%nalt, traceGasS(iTrace)%alt, optPropRTMGridS%RTMnlayerSub, &
                     optPropRTMGridS%RTMaltitudeSub, dn_dnodeS(iTrace)%dn_dnode, traceGasS(iTrace)%useLinInterp)
          if (errorCheck(errS)) return
        end do
      end if
    end if ! controlS%calculateDerivatives

    ! calculate cfMax and cfMin these are used when
    ! the derivatives for the cloudy part of the pixel are calculated
    if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
      cfMax = cloudAerosolRTMgridS%cldAerFractionAllBands
      cfMin = cloudAerosolRTMgridS%cldAerFractionAllBands
    else
      cfMin = maxCldFraction
      cfMax = minCldFraction
      do iwave = 1, nwavelRTM
        cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelRTMS_cld%wavel(iwave))
        if ( cf < cfMin ) cfMin = cf
        if ( cf > cfMax ) cfMax = cf
      end do
    end if

    ! loop over wavelengths

    do iwave = 1, nwavelRTM

      ! initialize
      refl_Clear                  = 0.0d0
      refl_Cloud                  = 0.0d0

      contribRefl_Clear           = 0.0d0
      contribRefl_Cloud           = 0.0d0

      wfInterfkabs_Clear          = 0.0d0
      wfInterfkabs_Cloud          = 0.0d0

      wfInterfkscaGas_Clear       = 0.0d0
      wfInterfkscaGas_Cloud       = 0.0d0

      wfInterfkscaAerAbove_Clear  = 0.0d0
      wfInterfkscaAerBelow_Clear  = 0.0d0
      wfInterfkscaAerAbove_Cloud  = 0.0d0
      wfInterfkscaAerBelow_Cloud  = 0.0d0

      wfInterfkscaCldAbove_Clear  = 0.0d0
      wfInterfkscaCldBelow_Clear  = 0.0d0
      wfInterfkscaCldAbove_Cloud  = 0.0d0
      wfInterfkscaCldBelow_Cloud  = 0.0d0

      wfSurfaceAlbedo_Clear       = 0.0d0
      wfSurfaceAlbedo_Cloud       = 0.0d0

      wfSurfaceEmission_Clear     = 0.0d0
      wfSurfaceEmission_Cloud     = 0.0d0

      wfCloudAlbedo               = 0.0d0

      wfCloudFraction             = 0.0d0

      surfEmission_iwave          = 0.0d0

      ! set cloud fraction
      if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
        cf = cloudAerosolRTMgridS%cldAerFractionAllBands
      else
        cf = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelRTMS_cld%wavel(iwave))
      end if

      ! get optical properties of the atmosphere for the cloud free part at this wavelength
      ! and store them in OptPropRTMGridS
      ! in addition, calculate the albedo at this wavelength: surfAlb_iwave and cloudAlb_iwave

      if ( cf < 1.0d0 ) then  ! only if the cloud fraction is less than 1.0

        cloudyPart = .false.
        call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelRTMS_clr, XsecRTMS_clr, RRS_RingS,  &
                            controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,         &
                            mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                     &
                            surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,                  &
                            reflDerivRTMS%depolarization(iwave))
        if (errorCheck(errS)) return

        call layerBasedOrdersScattering(errS,        &
          optPropRTMGridS,                     &
          controlS,                            &
          geometryS,                           &
          surfAlb_iwave,                       &
          0,                                   &   ! index for Lambertian surface - here ground surface with index 0
          reflDerivRTMS%babs(iwave),           &   ! output
          reflDerivRTMS%bsca(iwave),           &   ! output
          refl_Clear,                          &   ! output
          contribRefl_Clear,                   &   ! output
          UD_Clear,                            &   ! output
          wfInterfkscaGas_Clear,               &   ! output
          wfInterfkscaAerAbove_Clear,          &   ! output
          wfInterfkscaAerBelow_Clear,          &   ! output
          wfInterfkscaCldAbove_Clear,          &   ! output
          wfInterfkscaCldBelow_Clear,          &   ! output
          wfInterfkabs_Clear,                  &   ! output
          wfSurfaceAlbedo_Clear,               &   ! output
          wfSurfaceEmission_Clear,             &   ! output
          statusRTM)
        if (errorCheck(errS)) return

      else

        refl_Clear                 = 1.0d-10
        contribRefl_Clear          = 0.0d0
        wfInterfkscaGas_Clear      = 0.0d0
        wfInterfkscaAerAbove_Clear = 0.0d0
        wfInterfkscaAerBelow_Clear = 0.0d0
        wfInterfkscaCldAbove_Clear = 0.0d0
        wfInterfkscaCldBelow_Clear = 0.0d0
        wfInterfkabs_Clear         = 0.0d0
        wfSurfaceAlbedo_Clear      = 0.0d0
        wfSurfaceEmission_Clear    = 0.0d0

      end if ! cf < 1.0d0

      if ( ( cf > controlS%cloudFracThreshold) .or. (cloudAerosolRTMgridS%fitCldAerFractionAllBands) ) then

        if ( cloudAerosolRTMgridS%useLambertianCloud ) then
          ! caculate the optical properties of the atmosphere for the cloud covered part
          ! note that the properties may differ from those for the clear part because
          ! the wavelength grid may differ
            cloudyPart = .false.  ! there is no scatterimg cioud
            call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelRTMS_cld, XsecRTMS_cld, RRS_RingS, &
                                controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,        &
                                mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                    &
                                surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,                 &
                                reflDerivRTMS%depolarization(iwave))
            if (errorCheck(errS)) return

          call layerBasedOrdersScattering(errS,  &
            optPropRTMGridS,                     &
            controlS,                            &
            geometryS,                           &
            cloudAlb_iwave,                      &
            cloudAerosolRTMgridS%RTMnlevelCloud, &   ! index for Lambertian surface
            reflDerivRTMS%babs(iwave),           &   ! output
            reflDerivRTMS%bsca(iwave),           &   ! output
            refl_Cloud,                          &   ! output
            contribRefl_Cloud,                   &   ! output
            UD_Cloud,                            &   ! output
            wfInterfkscaGas_Cloud,               &   ! output
            wfInterfkscaAerAbove_Cloud,          &   ! output
            wfInterfkscaAerBelow_Cloud,          &   ! output
            wfInterfkscaCldAbove_Cloud,          &   ! output
            wfInterfkscaCldBelow_Cloud,          &   ! output
            wfInterfkabs_Cloud,                  &   ! output
            wfCloudAlbedo,                       &   ! output
            wfSurfaceEmission_Cloud,             &   ! output
            statusRTM)
          if (errorCheck(errS)) return

        else

          ! use a scattering cloud
          cloudyPart = .true.
          call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelRTMS_cld, XsecRTMS_cld, RRS_RingS, &
                              controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,        &
                              mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,                    &
                              surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,                 &
                              reflDerivRTMS%depolarization(iwave))
          if (errorCheck(errS)) return

          call layerBasedOrdersScattering(errS,  &
            optPropRTMGridS,                     &
            controlS,                            &
            geometryS,                           &
            surfAlb_iwave,                       &
            0,                                   &   ! index for Lambertian surface - here ground surface with index 0
            reflDerivRTMS%babs(iwave),           &   ! output
            reflDerivRTMS%bsca(iwave),           &   ! output
            refl_Cloud,                          &   ! output
            contribRefl_Cloud,                   &   ! output
            UD_Cloud,                            &   ! output
            wfInterfkscaGas_Cloud,               &   ! output
            wfInterfkscaAerAbove_Cloud,          &   ! output
            wfInterfkscaAerBelow_Cloud,          &   ! output
            wfInterfkscaCldAbove_Cloud,          &   ! output
            wfInterfkscaCldBelow_Cloud,          &   ! output
            wfInterfkabs_Cloud,                  &   ! output
            wfSurfaceAlbedo_Cloud,               &   ! output
            wfSurfaceEmission_Cloud,             &   ! output
            statusRTM)
          if (errorCheck(errS)) return

          wfCloudAlbedo = 0.0d0  ! no Lambertian cloud in this case

        end if !  cloudAerosolRTMgridS%useLambertianCloud

      else

        refl_Cloud                 = 1.0d-10
        contribRefl_Cloud          = 0.0d0
        wfInterfkscaGas_Cloud      = 0.0d0
        wfInterfkscaAerAbove_Cloud = 0.0d0
        wfInterfkscaAerBelow_Cloud = 0.0d0
        wfInterfkscaCldAbove_Cloud = 0.0d0
        wfInterfkscaCldBelow_Cloud = 0.0d0
        wfInterfkabs_Cloud         = 0.0d0
        wfSurfaceAlbedo_Cloud      = 0.0d0
        wfSurfaceEmission_Cloud    = 0.0d0
        wfCloudAlbedo              = 0.0d0

      end if ! ( cf > controlS%cloudFracThreshold) .or. (cloudAerosolRTMgridS%fitGeomCloudFraction)

      ! In DISMAS we interpolate on the natural logarithm of the reflectance for the clear
      ! and cloudy part. Make sure that they are not negative or zero.
      if ( refl_Clear(1) < 1.0d-8 ) refl_Clear(1) = 1.0d-8
      if ( refl_Cloud(1) < 1.0d-8 ) refl_Cloud(1) = 1.0d-8

      reflDerivRTMS%refl_clr(1,iwave) = refl_Clear(1)                                     &
                                      + surfEmission_iwave * wfSurfaceEmission_Clear * PI &
                                      / geometryS%u0 / solarIrrS%solIrrMR(iwave)
      do iSV = 2, controlS%dimSV
        reflDerivRTMS%refl_clr(iSV,iwave) = refl_Clear(iSV)
      end do

      reflDerivRTMS%refl_cld(1,iwave) = refl_Cloud(1)                                     &
                                      + surfEmission_iwave * wfSurfaceEmission_Cloud * PI &
                                      / geometryS%u0 / solarIrrS%solIrrMR(iwave)
      do iSV = 2, controlS%dimSV
        reflDerivRTMS%refl_cld(iSV,iwave) = refl_Cloud(iSV)
      end do

      reflDerivRTMS%altResAMFabs_clr(iwave,:) = - wfInterfkabs_Clear(:) / refl_Clear(1)
      reflDerivRTMS%altResAMFabs_cld(iwave,:) = - wfInterfkabs_Cloud(:) / refl_Cloud(1)

      wfCloudFraction = reflDerivRTMS%refl_cld(1,iwave) - reflDerivRTMS%refl_clr(1,iwave)

      if ( controlS%calculateDerivatives ) then

        call calculate_K_clr(errS, iband, nTrace, traceGasS, gasPTS,                             &
                             strayLightS, solarIrrS, cloudAerosolRTMgridS,                       &
                             optPropRTMGridS, retrS, geometryS, wavelRTMS_clr, iwave, dn_dnodeS, &
                             wfInterfkabs_Clear, wfInterfkscaGas_Clear,                          &
                             wfInterfkscaAerAbove_Clear, wfInterfkscaAerBelow_Clear,             &
                             wfSurfaceAlbedo_Clear, wfSurfaceEmission_Clear, wfCloudFraction,    &
                             reflDerivRTMS)
        if (errorCheck(errS)) return

        call calculate_K_cld(errS, iband, nTrace, cfMin, cfMax, traceGasS, gasPTS, LambCloudS,   &
                             strayLightS, solarIrrS, cloudAerosolRTMgridS,                       &
                             optPropRTMGridS, retrS, geometryS, wavelRTMS_cld, iwave, dn_dnodeS, &
                             wfInterfkabs_Cloud, wfInterfkscaGas_Cloud,                          &
                             wfInterfkscaAerAbove_Cloud, wfInterfkscaAerBelow_Cloud,             &
                             wfInterfkscaCldAbove_Cloud, wfInterfkscaCldBelow_Cloud,             &
                             wfSurfaceAlbedo_Cloud, wfSurfaceEmission_Cloud, wfCloudAlbedo,      &
                             wfCloudFraction, reflDerivRTMS)
        if (errorCheck(errS)) return

      else

          reflDerivRTMS%K_clr_lnvmr(iwave,:) = 0.0d0
          reflDerivRTMS%K_cld_lnvmr(iwave,:) = 0.0d0
          reflDerivRTMS%K_clr_vmr(iwave,:)   = 0.0d0
          reflDerivRTMS%K_cld_vmr(iwave,:)   = 0.0d0
          reflDerivRTMS%K_clr_ndens(iwave,:) = 0.0d0
          reflDerivRTMS%K_cld_ndens(iwave,:) = 0.0d0

      end if ! controlS%calculateDerivatives

    end do ! iwave

    if ( verbose .or. controlS%writeHighResReflAndDeriv) then
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') ' reflectance and derivatives (for ln(vmr)) on RTM grid - DISMAS'
      write(addtionalOutputUnit,'(A)') '      wavel            bsca           babs      refl_clr    derivatives  ....'
      do iwave = 1, wavelRTMS_clr%nwavel
        write(addtionalOutputUnit,'(3F15.8,F12.8,100E14.5)')  wavelRTMS_clr%wavel(iwave),      &
                                                              reflDerivRTMS%bsca(iwave),       &
                                                              reflDerivRTMS%babs(iwave),       &
                                                              reflDerivRTMS%refl_clr(1,iwave), &
               ( reflDerivRTMS%K_clr_lnvmr(iwave,istate), istate = 1, reflDerivRTMS%nstate )
      end do
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') ' reflectance and derivatives (for ln(vmr)) on RTM grid - DISMAS'
      write(addtionalOutputUnit,'(A)') '      wavel            bsca           babs      refl_cld    derivatives  ....'
      do iwave = 1, wavelRTMS_cld%nwavel
        write(addtionalOutputUnit,'(3F15.8,F12.8,100E14.5)')  wavelRTMS_cld%wavel(iwave),      &
                                                              reflDerivRTMS%bsca(iwave),       &
                                                              reflDerivRTMS%babs(iwave),       &
                                                              reflDerivRTMS%refl_cld(1,iwave), &
               ( reflDerivRTMS%K_cld_lnvmr(iwave,istate), istate = 1, reflDerivRTMS%nstate )
      end do
    end if

    ! clean up
    call deallocateUD(errS, optPropRTMGridS%RTMnlayer, UD_Clear, UD_Cloud)
    if (errorCheck(errS)) return

  end subroutine calcRelfAndDerivRTMgrid


  subroutine evaluateSmoothValuesOnHRgrid(errS, use_abs_opt_thickn_for_AMF, nwavelRTM, wavelRTMS_clr, &
                             wavelRTMS_cld, wavelHRS, reflDerivRTMS, absOptThickness, reflDerivHRS)

    ! Evalueste the smooth parts of the reflectance and derivatives w.r.t. the state vector elements
    ! on the high resolution grid by fitting a low degree polynomial (degree = nwavelRTM) to the values
    ! in reflDerivRTMS and evaluate that polynomial on the high resolution grid. Note that
    ! the differential part is calculated later; this provides only the smooth part.

    implicit none

      type(errorType), intent(inout) :: errS
    logical,                       intent(in)    :: use_abs_opt_thickn_for_AMF
    integer,                       intent(in)    :: nwavelRTM            ! number of wavelengths for RTM calculations
    type(wavelHRType),             intent(in)    :: wavelRTMS_clr        ! wavelength grid for RTM calculations
    type(wavelHRType),             intent(in)    :: wavelRTMS_cld        ! wavelength grid for RTM calculations
    type(wavelHRType),             intent(in)    :: wavelHRS             ! high resolution wavelength grid
    type(reflDerivType),           intent(inout) :: reflDerivRTMS        ! reflectance and derivatives on RTM grid
    real(8),                       intent(in)    :: absOptThickness(wavelHRS%nwavel)
    type(reflDerivType),           intent(inout) :: reflDerivHRS         ! reflectance and derivatives on HR grid

    ! local

    integer :: istate, ialt, iwave
    integer :: status

    ! optical thickness used for interpolating the air mass factor
    real(8) :: absOpticalThicknessRTM_clr(nwavelRTM)
    real(8) :: absOpticalThicknessRTM_cld(nwavelRTM)

    logical, parameter :: verbose = .false.


    ! CLEAR PART PIXEL

    reflDerivHRS%refl_clr(1,1:wavelHRS%nwavel) = lnfitAndEvalLegPolynomial(errS,              &
                                      wavelRTMS_clr%wavel(1:wavelRTMS_clr%nwavel),      &
                                      reflDerivRTMS%refl_clr(1,1:wavelRTMS_clr%nwavel), &
                                      wavelHRS%wavel(1:wavelHRS%nwavel) )

    ! fit the coefficients of the polynomial for the derivatives w.r.t. the state vector elements
    do istate = 1, reflDerivRTMS%nstate

      ! if all value of the derivative are negative we change sign
      ! then we can still fit the logarithm of the values
      if ( all( reflDerivRTMS%K_clr_lnvmr(:,istate) < 0.0d0 ) )  then
        reflDerivRTMS%K_clr_lnvmr(:,istate) = - reflDerivRTMS%K_clr_lnvmr(:,istate)
        reflDerivHRS%K_clr_lnvmr(1:wavelHRS%nwavel,istate)  = - lnfitAndEvalLegPolynomial(errS,            &
                                           wavelRTMS_clr%wavel(1:wavelRTMS_clr%nwavel),              &
                                           reflDerivRTMS%K_clr_lnvmr(1:wavelRTMS_clr%nwavel,istate), &
                                           wavelHRS%wavel(1:wavelHRS%nwavel) )
        reflDerivHRS%K_clr_lnvmr(:,istate) = - reflDerivHRS%K_clr_lnvmr(:,istate)
      else
        if ( any ( reflDerivRTMS%K_clr_lnvmr(:,istate) <= 0.0d0 ) ) then
          reflDerivHRS%K_clr_lnvmr(1:wavelHRS%nwavel,istate) = fitAndEvalLegPolynomial(errS,               &
                                           wavelRTMS_clr%wavel(1:wavelRTMS_clr%nwavel),              &
                                           reflDerivRTMS%K_clr_lnvmr(1:wavelRTMS_clr%nwavel,istate), &
                                           wavelHRS%wavel(1:wavelHRS%nwavel) )
        else
          reflDerivHRS%K_clr_lnvmr(1:wavelHRS%nwavel,istate) = lnfitAndEvalLegPolynomial(errS,             &
                                           wavelRTMS_clr%wavel(1:wavelRTMS_clr%nwavel),              &
                                           reflDerivRTMS%K_clr_lnvmr(1:wavelRTMS_clr%nwavel,istate), &
                                           wavelHRS%wavel(1:wavelHRS%nwavel) )
        end if ! any ( reflDerivRTMS%K_clr_lnvmr(:,istate) <= 0.0d0 )
      end if ! all( reflDerivRTMS%K_clr_lnvmr(:,istate) < 0.0d0 )

      ! K_lnvmr differs from K_vmr and K_ndens by at most a constant (independent of wavelength)
      if ( abs( reflDerivRTMS%K_clr_lnvmr(1,istate) ) > 1.0d-100 ) then
        reflDerivHRS%K_clr_vmr(:,istate)   = reflDerivHRS%K_clr_lnvmr(:,istate)  &
                                           * reflDerivRTMS%K_clr_vmr  (1,istate) &
                                           / reflDerivRTMS%K_clr_lnvmr(1,istate)
        reflDerivHRS%K_clr_ndens(:,istate) = reflDerivHRS%K_clr_lnvmr(:,istate)  &
                                           * reflDerivRTMS%K_clr_ndens(1,istate) &
                                           / reflDerivRTMS%K_clr_lnvmr(1,istate)
      else
        reflDerivHRS%K_clr_vmr(:,istate)   = 0.0d0
        reflDerivHRS%K_clr_ndens(:,istate) = 0.0d0
      end if

    end do  ! istate

    ! fit the derivatives for the column grid

    do istate = 1, reflDerivHRS%nstateCol

      if ( any ( reflDerivRTMS%KHR_clr_ndensCol(:,istate) <= 0.0d0 ) ) then
        reflDerivHRS%KHR_clr_ndensCol(1:wavelHRS%nwavel,istate) = fitAndEvalLegPolynomial(errS,             &
                                       wavelRTMS_clr%wavel(1:wavelRTMS_clr%nwavel),                   &
                                       reflDerivRTMS%KHR_clr_ndensCol(1:wavelRTMS_clr%nwavel,istate), &
                                       wavelHRS%wavel(1:wavelHRS%nwavel) )
      else
        reflDerivHRS%KHR_clr_ndensCol(1:wavelHRS%nwavel,istate) = lnfitAndEvalLegPolynomial(errS,           &
                                       wavelRTMS_clr%wavel(1:wavelRTMS_clr%nwavel),                   &
                                       reflDerivRTMS%KHR_clr_ndensCol(1:wavelRTMS_clr%nwavel,istate), &
                                       wavelHRS%wavel(1:wavelHRS%nwavel) )
      end if

    end do  ! istate


    ! CLOUDY PART PIXEL

    reflDerivHRS%refl_cld(1, 1:wavelHRS%nwavel) = lnfitAndEvalLegPolynomial(errS,               &
                                       wavelRTMS_cld%wavel(1:wavelRTMS_cld%nwavel),       &
                                       reflDerivRTMS%refl_cld(1, 1:wavelRTMS_cld%nwavel), &
                                       wavelHRS%wavel(1:wavelHRS%nwavel) )

    ! fit the coefficients of the polynomial for the derivatives w.r.t. the state vector elements

    do istate = 1, reflDerivRTMS%nstate

      ! if all value of the derivative are negative we change sign
      ! then we can still fit the logarithm of the values
      if ( all( reflDerivRTMS%K_cld_lnvmr(:,istate) < 0.0d0 ) )  then
        reflDerivRTMS%K_cld_lnvmr(:,istate) = - reflDerivRTMS%K_cld_lnvmr(:,istate)
        reflDerivHRS%K_cld_lnvmr(1:wavelHRS%nwavel,istate)  = - lnfitAndEvalLegPolynomial(errS,            &
                                           wavelRTMS_cld%wavel(1:wavelRTMS_cld%nwavel),              &
                                           reflDerivRTMS%K_cld_lnvmr(1:wavelRTMS_cld%nwavel,istate), &
                                           wavelHRS%wavel(1:wavelHRS%nwavel) )
        reflDerivHRS%K_cld_lnvmr(:,istate) = - reflDerivHRS%K_cld_lnvmr(:,istate)
      else
        if ( any ( reflDerivRTMS%K_cld_lnvmr(:,istate) <= 0.0d0 ) ) then
          reflDerivHRS%K_cld_lnvmr(1:wavelHRS%nwavel,istate) = fitAndEvalLegPolynomial(errS,               &
                                           wavelRTMS_cld%wavel(1:wavelRTMS_cld%nwavel),              &
                                           reflDerivRTMS%K_cld_lnvmr(1:wavelRTMS_cld%nwavel,istate), &
                                           wavelHRS%wavel(1:wavelHRS%nwavel) )
        else
          reflDerivHRS%K_cld_lnvmr(1:wavelHRS%nwavel,istate) = lnfitAndEvalLegPolynomial(errS,             &
                                           wavelRTMS_cld%wavel(1:wavelRTMS_cld%nwavel),              &
                                           reflDerivRTMS%K_cld_lnvmr(1:wavelRTMS_cld%nwavel,istate), &
                                           wavelHRS%wavel(1:wavelHRS%nwavel) )
        end if ! any ( reflDerivRTMS%K_cld_lnvmr(:,istate) <= 0.0d0 )
      end if ! all( reflDerivRTMS%K_cld_lnvmr(:,istate) < 0.0d0 )

      ! K_lnvmr differs from K_vmr and K_ndens by at most a constant (independent of wavelength)
      if ( abs( reflDerivRTMS%K_cld_lnvmr(1,istate) ) > 1.0d-100 ) then
        reflDerivHRS%K_cld_vmr(:,istate)   = reflDerivHRS%K_cld_lnvmr(:,istate)  &
                                           * reflDerivRTMS%K_cld_vmr  (1,istate) &
                                           / reflDerivRTMS%K_cld_lnvmr(1,istate)
        reflDerivHRS%K_cld_ndens(:,istate) = reflDerivHRS%K_cld_lnvmr(:,istate)  &
                                           * reflDerivRTMS%K_cld_ndens(1,istate) &
                                           / reflDerivRTMS%K_cld_lnvmr(1,istate)
      else
        reflDerivHRS%K_cld_vmr(:,istate)   = 0.0d0
        reflDerivHRS%K_cld_ndens(:,istate) = 0.0d0
      end if

    end do  ! istate

    ! fit the derivatives for the column grid

    do istate = 1, reflDerivHRS%nstateCol

      if ( any ( reflDerivRTMS%KHR_cld_ndensCol(:,istate) <= 0.0d0 ) ) then
        reflDerivHRS%KHR_cld_ndensCol(1:wavelHRS%nwavel,istate) = fitAndEvalLegPolynomial(errS,             &
                                       wavelRTMS_cld%wavel(1:wavelRTMS_cld%nwavel),                   &
                                       reflDerivRTMS%KHR_cld_ndensCol(1:wavelRTMS_cld%nwavel,istate), &
                                       wavelHRS%wavel(1:wavelHRS%nwavel) )
      else
        reflDerivHRS%KHR_cld_ndensCol(1:wavelHRS%nwavel,istate) = lnfitAndEvalLegPolynomial(errS,           &
                                       wavelRTMS_cld%wavel(1:wavelRTMS_cld%nwavel),                   &
                                       reflDerivRTMS%KHR_cld_ndensCol(1:wavelRTMS_cld%nwavel,istate), &
                                       wavelHRS%wavel(1:wavelHRS%nwavel) )
      end if

    end do  ! istate

    ! reset values for the altitude resolved air mass factor if required
    ! if the amf is 0.0 or negative we can not take the logarithm
    ! 0.0 occuras for a cloud fraction 1.0 and negative values occur for a cloud fraction > 1.0
    ! for altitudes below a Lambertian cloud
    do ialt = 0, reflDerivRTMS%RTMnlayer
      do iwave = 1, nwavelRTM
        if ( reflDerivRTMS%altResAMFabs_clr(iwave,ialt) < minAltResolvedAMF ) &
             reflDerivRTMS%altResAMFabs_clr(iwave,ialt) = minAltResolvedAMF
        if ( reflDerivRTMS%altResAMFabs_clr(iwave,ialt) > maxAltResolvedAMF ) &
             reflDerivRTMS%altResAMFabs_clr(iwave,ialt) = maxAltResolvedAMF
        if ( reflDerivRTMS%altResAMFabs_cld(iwave,ialt) < minAltResolvedAMF ) &
             reflDerivRTMS%altResAMFabs_cld(iwave,ialt) = minAltResolvedAMF
        if ( reflDerivRTMS%altResAMFabs_cld(iwave,ialt) > maxAltResolvedAMF ) &
             reflDerivRTMS%altResAMFabs_cld(iwave,ialt) = maxAltResolvedAMF
      end do ! iwave
    end do ! ialt

! CHANGE CODE
! To calculate the air mass factor on the HR grid we did interpolate on the wavelength
! and evaluate the value at each wavelength grid point.
! However, we know the altitude averaged  absorption cross section
! on the HR wavelength grid (calculated using
! previous estimates of the air mass factor). Hence, we can determine the slant optical thickness
! at the RTM wavelength grid using interpolation. Assume that the altitude resolved air mass
! factor is a smooth function of the slant absorption optical thickness, not smooth in the wavelength.
! Hence we fit a low order polynomial to the altitude resolved amf as a function of the slant absorption
! optical thickness, and evaluate the values on the high resolution wavelength grid.

    if (use_abs_opt_thickn_for_AMF ) then

      ! determine absorption optical thickness at RTM wavelengths
      do iwave = 1, nwavelRTM
        absOpticalThicknessRTM_clr(iwave) = &
               splintLin(errS, wavelHRS%wavel, absOptThickness, wavelRTMS_clr%wavel(iwave), status)
        absOpticalThicknessRTM_cld(iwave) = &
               splintLin(errS, wavelHRS%wavel, absOptThickness, wavelRTMS_cld%wavel(iwave), status)
      end do ! iwave

      do ialt = 0, reflDerivRTMS%RTMnlayer
        do iwave = 1, wavelHRS%nwavel
          reflDerivHRS%altResAMFabs_clr(iwave,ialt)  = splintLin(errS, absOpticalThicknessRTM_clr,  &
                     reflDerivRTMS%altResAMFabs_clr(:,ialt), absOptThickness(iwave), status)
          reflDerivHRS%altResAMFabs_cld(iwave,ialt)  = splintLin(errS, absOpticalThicknessRTM_cld,  &
                     reflDerivRTMS%altResAMFabs_cld(:,ialt), absOptThickness(iwave), status)
        end do ! iwave
      end do  ! ialt

    else

      ! fit the coefficients of the polynomial for the altitude resolved air mass factor
      do ialt = 0, reflDerivRTMS%RTMnlayer
        if ( any ( reflDerivRTMS%altResAMFabs_clr(:,ialt) <= 0.0d0 ) ) then
          reflDerivHRS%altResAMFabs_clr(1:wavelHRS%nwavel,ialt) = fitAndEvalLegPolynomial(errS,       &
                                        wavelRTMS_clr%wavel(1:wavelRTMS_clr%nwavel),                  &
                                        reflDerivRTMS%altResAMFabs_clr(1:wavelRTMS_clr%nwavel,ialt),  &
                                        wavelHRS%wavel(1:wavelHRS%nwavel) )
        else
          reflDerivHRS%altResAMFabs_clr(1:wavelHRS%nwavel,ialt) = lnfitAndEvalLegPolynomial(errS,     &
                                        wavelRTMS_clr%wavel(1:wavelRTMS_clr%nwavel),                  &
                                        reflDerivRTMS%altResAMFabs_clr(1:wavelRTMS_clr%nwavel,ialt),  &
                                        wavelHRS%wavel(1:wavelHRS%nwavel) )
        end if
        if ( any ( reflDerivRTMS%altResAMFabs_cld(:,ialt) <= 0.0d0 ) ) then
          reflDerivHRS%altResAMFabs_cld(1:wavelHRS%nwavel,ialt) = fitAndEvalLegPolynomial(errS,       &
                                        wavelRTMS_cld%wavel(1:wavelRTMS_cld%nwavel),                  &
                                        reflDerivRTMS%altResAMFabs_cld(1:wavelRTMS_cld%nwavel,ialt),  &
                                        wavelHRS%wavel(1:wavelHRS%nwavel) )
        else
          reflDerivHRS%altResAMFabs_cld(1:wavelHRS%nwavel,ialt) = lnfitAndEvalLegPolynomial(errS,     &
                                        wavelRTMS_cld%wavel(1:wavelRTMS_cld%nwavel),                  &
                                        reflDerivRTMS%altResAMFabs_cld(1:wavelRTMS_cld%nwavel,ialt),  &
                                        wavelHRS%wavel(1:wavelHRS%nwavel) )
        end if
      end do  ! ialt

    end if ! use_abs_opt_thickness_for_AMF

    if ( verbose ) then
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'dismas output: smooth reflectance on RTM wavelength grid clear and cloudy part'
      write(intermediateFileUnit,*) 'reflectance = PI * sun-normalized radiance / mu0'
      write(intermediateFileUnit,*) 'wavelength_clr    reflectance_clr    wavelength_cld    reflectance_cld '
      do iwave = 1, wavelRTMS_clr%nwavel
        write(intermediateFileUnit,'(2(F12.6,F12.8))') wavelRTMS_clr%wavel(iwave), reflDerivRTMS%refl_clr(1,iwave), &
                                                       wavelRTMS_cld%wavel(iwave), reflDerivRTMS%refl_cld(1,iwave)
      end do
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'dismas output: smooth reflectance on HR wavelength grid clear and cloudy part'
      write(intermediateFileUnit,*) 'wavelength    reflectance ( = PI * sun-normalized radiance / mu0 )'
      do iwave = 1, wavelHRS%nwavel
        write(intermediateFileUnit,'(F12.6, 3F12.8)') wavelHRS%wavel(iwave), reflDerivHRS%refl_clr(1,iwave), &
                                                      reflDerivHRS%refl_cld(1,iwave)
      end do
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'dismas output: derivatives K_clr_lnvmr on RTM wavelength grid'
      write(intermediateFileUnit,*) 'wavelength    K_clr_lnvmr'
      do iwave = 1, wavelRTMS_clr%nwavel
        write(intermediateFileUnit,'(F12.6, 100ES12.3)') wavelRTMS_clr%wavel(iwave), &
              ( reflDerivRTMS%K_clr_lnvmr(iwave,istate), istate = 1, reflDerivRTMS%nstate )
      end do
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'dismas output: derivatives K_cld_lnvmr on RTM wavelength grid'
      write(intermediateFileUnit,*) 'wavelength    K_cld_lnvmr'
      do iwave = 1, wavelRTMS_cld%nwavel
        write(intermediateFileUnit,'(F12.6, 100ES12.3)') wavelRTMS_cld%wavel(iwave), &
              ( reflDerivRTMS%K_cld_lnvmr(iwave,istate), istate = 1, reflDerivRTMS%nstate )
      end do
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'dismas output: smooth derivatives K_clr_lnvmr on HR wavelength grid'
      write(intermediateFileUnit,*) 'wavelength    K_clr_lnvmr'
      do iwave = 1, wavelHRS%nwavel
        write(intermediateFileUnit,'(F12.6, 100ES12.3)') wavelHRS%wavel(iwave), &
            ( reflDerivHRS%K_clr_lnvmr(iwave,istate), istate = 1, reflDerivRTMS%nstate )
      end do
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'dismas output: smooth derivatives K_cld_lnvmr on HR wavelength grid'
      write(intermediateFileUnit,*) 'wavelength    K_cld_lnvmr'
      do iwave = 1, wavelHRS%nwavel
        write(intermediateFileUnit,'(F12.6, 100ES12.3)') wavelHRS%wavel(iwave), &
            ( reflDerivHRS%K_cld_lnvmr(iwave,istate), istate = 1, reflDerivRTMS%nstate )
      end do
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'dismas output: smooth altitude resolved amf on HR wavelength grid'
      write(intermediateFileUnit,*) 'wavelength    amf_clr'
      do iwave = 1, wavelHRS%nwavel
        write(intermediateFileUnit,'(F12.6, 100ES12.3)') wavelHRS%wavel(iwave), &
            ( reflDerivHRS%altResAMFabs_clr(iwave,ialt), ialt = 0, reflDerivRTMS%RTMnlayer )
      end do
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'dismas output: smooth altitude resolved amf on HR wavelength grid'
      write(intermediateFileUnit,*) 'wavelength    amf_cld'
      do iwave = 1, wavelHRS%nwavel
        write(intermediateFileUnit,'(F12.6, 100ES12.3)') wavelHRS%wavel(iwave), &
            ( reflDerivHRS%altResAMFabs_cld(iwave,ialt), ialt = 0, reflDerivRTMS%RTMnlayer )
      end do
    end if

  end subroutine evaluateSmoothValuesOnHRgrid


  subroutine accountForDifferentialAbs(errS, iband, nTrace, wavelHRS, RRS_RingS, controlS, gasPTS, traceGasS, &
                                       optPropRTMGridS, surfaceS, LambCloudS, mieAerS, mieCldS,         &
                                       cldAerFractionS, mulOffsetS, straylightS, cloudAerosolRTMgridS,  &
                                       retrS, XsecHRS, amfAltAveragedHR_clr, amfAltAveragedHR_cld,      &
                                       XsecAltAveragedHRdiff_clr, XsecAltAveragedHRdiff_cld,            &
                                       slantAbsOptThicknessDiff_clr, slantAbsOptThicknessDiff_cld,      &
                                       dn_dnodeS, reflDerivHRS)

    ! Background: We use the approximation R = Rs * exp( sum(-M*N*sig_diff) ) = Rs * exp( - tau_slant_diff )
    !             where the sum is over the different trace gases, M is the altitude averaged air mass factor,
    !             N is the column of the trace gas, sig_diff is the differential absorption cross section,
    !             Rs is the smooth reflectance that is currently stored in reflDerivHRS.
    !
    !             This expression is used separately for the clear and cloudy part of the pixel.
    !
    !             In addition the derivatives are calculated.

    implicit none

      type(errorType), intent(inout) :: errS
    integer,                       intent(in)    :: iband                     ! number spectral band
    integer,                       intent(in)    :: nTrace                    ! number of trace gases
    type(wavelHRType),             intent(in)    :: wavelHRS                  ! high resolution wavelength grid
    type(RRS_RingType),            intent(in)    :: RRS_RingS                 ! Raman / Ring info
    type(controlType),             intent(inout) :: controlS                  ! control parameters for radiative transfer
    type(gasPTType),               intent(in)    :: gasPTS                    ! atmospheric pressure and temperature
    type(traceGasType),            intent(in)    :: traceGasS(nTrace)         ! atmospheric trace gas properties
    type(LambertianType),          intent(in)    :: surfaceS                  ! surface properties for a spectral band
    type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)  ! specification expansion coefficients
    type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)  ! specification expansion coefficients
    type(LambertianType),          intent(in)    :: LambCloudS                ! Lambertian cloud properties
    type(cldAerFractionType),      intent(in)    :: cldAerFractionS           ! cloud / aerosol fraction structure
    type(mulOffsetType),           intent(in)    :: mulOffsetS                ! multiplicative offset
    type(straylightType),          intent(in)    :: strayLightS               ! stray light
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS      ! cloud-aerosol properties
    type(retrType),                intent(in)    :: retrS                     ! contains info on the fit parameters
    type(XsecType),                intent(in)    :: XsecHRS(nTrace)           ! altitude dependent absorption cross sections
    real(8),                       intent(in)    :: amfAltAveragedHR_clr        (wavelHRS%nwavel,nTrace)
    real(8),                       intent(in)    :: amfAltAveragedHR_cld        (wavelHRS%nwavel,nTrace)
    real(8),                       intent(in)    :: XsecAltAveragedHRdiff_clr   (wavelHRS%nwavel,nTrace)
    real(8),                       intent(in)    :: XsecAltAveragedHRdiff_cld   (wavelHRS%nwavel,nTrace)
    real(8),                       intent(in)    :: slantAbsOptThicknessDiff_clr(wavelHRS%nwavel)
    real(8),                       intent(in)    :: slantAbsOptThicknessDiff_cld(wavelHRS%nwavel)
    type(dn_dnodeType),            intent(in)    :: dn_dnodeS(0:nTrace)    ! derivatives for interpolation
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS        ! optical properties on RTM grid
    type(reflDerivType),           intent(inout) :: reflDerivHRS ! reflectance and derivatives on HR grid

    ! local

    integer  :: iwave, iTrace, istate, indexTop, indexBot
    real(8)  :: cf(wavelHRS%nwavel) ! cloud fraction may be wavelength dependent
    real(8)  :: Msigma_clr(wavelHRS%nwavel), Msigma_cld(wavelHRS%nwavel)
    real(8)  :: refl_smooth(wavelHRS%nwavel)
    real(8)  :: refl_smooth_clr(wavelHRS%nwavel), refl_smooth_cld(wavelHRS%nwavel)
    real(8)  :: sum_ndens_top_sigma_d      ! sum over number density times the differential absorption
                                           ! for the trace gases at the top of the fit interval
    real(8)  :: sum_ndens_bot_sigma_d      ! sum over number density times the differential absorption
                                           ! for the trace gases at the bottom of the fit interval
    real(8)  :: m_cld_top , m_clr_top  ! altitude resolved air mass factors at the top of the fit interval
    real(8)  :: m_cld_bot , m_clr_bot  ! altitude resolved air mass factors at the bottom of the fit interval
    real(8)  :: surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave


    ! parameters for profile retrieval
    ! weighting functions for the profile
    logical  :: first_time_profile
    logical  :: cloudyPart
    integer  :: ialt, nAlt, status, startIndex, endIndex
    real(8)  :: wfInterfkabs_clr(0:optPropRTMGridS%RTMnlayer)
    real(8)  :: wfInterfkabs_cld(0:optPropRTMGridS%RTMnlayer)
    real(8)  :: wfnode_clr_lnvmr, wfnode_clr_vmr, wfnode_clr_ndens
    real(8)  :: wfnode_cld_lnvmr, wfnode_cld_vmr, wfnode_cld_ndens

    real(8)  :: wfkabs_clr_int(0:optPropRTMGridS%RTMnlayerSub)   ! wfInterfkabs interpolated to sublayer grid
    real(8)  :: wfkabs_cld_int(0:optPropRTMGridS%RTMnlayerSub)   ! wfInterfkabs interpolated to sublayer grid

    ! temporary arrays for Lambertian cloud
    integer              :: length
    integer              :: deallocStatus, sumdeallocStatus
    integer              :: allocStatus, sumallocStatus
    real(8), allocatable :: RTMalt(:)            ! altitudes on RTM grid for the fit interval or above cloud
    real(8), allocatable :: wfk_Lambcld(:)       ! temporary array for spline interpolation


    logical, parameter :: verbose = .false.

    ! Store smooth parts for printing.
    refl_smooth_clr(:) = reflDerivHRS%refl_clr(1,:)
    refl_smooth_cld(:) = reflDerivHRS%refl_cld(1,:)


    ! Simply multiply with exp( -slantAbsOptThicknessDiff) to account for
    ! differential absorption. Note that in principle we also need
    ! d exp( -slantAbsOptThicknessDiff) / dxi to calculate the proper derivatives.
    ! Generally, we ignore this extra term for the derivative, but account for it in
    ! special cases.
    reflDerivHRS%refl_clr(1,:) = reflDerivHRS%refl_clr(1,:) * exp( -slantAbsOptThicknessDiff_clr(:) )
    reflDerivHRS%refl_cld(1,:) = reflDerivHRS%refl_cld(1,:) * exp( -slantAbsOptThicknessDiff_cld(:) )

    do istate = 1, retrS%nstate
      reflDerivHRS%K_clr_lnvmr(:,istate) = &
           reflDerivHRS%K_clr_lnvmr(:,istate) * exp( -slantAbsOptThicknessDiff_clr(:) )
      reflDerivHRS%K_cld_lnvmr(:,istate) = &
           reflDerivHRS%K_cld_lnvmr(:,istate) * exp( -slantAbsOptThicknessDiff_cld(:) )

      reflDerivHRS%K_clr_vmr(:,istate) = &
           reflDerivHRS%K_clr_vmr(:,istate) * exp( -slantAbsOptThicknessDiff_clr(:) )
      reflDerivHRS%K_cld_vmr(:,istate) = &
           reflDerivHRS%K_cld_vmr(:,istate) * exp( -slantAbsOptThicknessDiff_cld(:) )

      reflDerivHRS%K_clr_ndens(:,istate) = &
           reflDerivHRS%K_clr_ndens(:,istate) * exp( -slantAbsOptThicknessDiff_clr(:) )
      reflDerivHRS%K_cld_ndens(:,istate) = &
           reflDerivHRS%K_cld_ndens(:,istate) * exp( -slantAbsOptThicknessDiff_cld(:) )
    end do ! istate

    ! derivatives for the column
    do istate = 1, reflDerivHRS%nstateCol
      reflDerivHRS%KHR_clr_ndensCol(:,istate) = &
           reflDerivHRS%KHR_clr_ndensCol(:,istate) * exp( -slantAbsOptThicknessDiff_clr(:) )
      reflDerivHRS%KHR_cld_ndensCol(:,istate) = &
           reflDerivHRS%KHR_cld_ndensCol(:,istate) * exp( -slantAbsOptThicknessDiff_cld(:) )
    end do ! istate

    if ( verbose ) then
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'before istate loop'
      write(intermediateFileUnit,*) 'dismas output: derivatives on HR wavelength grid'
      write(intermediateFileUnit,*) 'wavelength    K_clr_lnvmr K_cld_lnvmr'
      do iwave = 1, wavelHRS%nwavel
        write(intermediateFileUnit,'(F12.6, 200ES17.8)') wavelHRS%wavel(iwave), &
              ( reflDerivHRS%K_clr_lnvmr(iwave,istate), istate = 1, reflDerivHRS%nstate ), &
              ( reflDerivHRS%K_cld_lnvmr(iwave,istate), istate = 1, reflDerivHRS%nstate )
      end do

    end if ! verbose

    ! prepare for a Lambertian cloud
    length = optPropRTMGridS%RTMnlayer - optPropRTMGridS%indexRTMFitIntTop + 1

    allocStatus    = 0
    sumallocStatus = 0

    allocate( RTMalt(length), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus
    allocate( wfk_Lambcld(length), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    if ( sumallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: allocation failed')
      call logDebug('forRTMalt or wfk_Lambcld')
      call logDebug('in subroutine accountForDifferentialAbs')
      call logDebug('in module dismasModule')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if

    startIndex = optPropRTMGridS%indexRTMFitIntTop
    endIndex   = optPropRTMGridS%RTMnlayer
    RTMalt = optPropRTMGridS%RTMaltitude(startIndex:endIndex)

   ! initialize for profile retrieval
   first_time_profile = .true.

    do istate = 1, retrS%nstate

      select case (retrS%codeFitParameters(istate))

        case( 'nodeTrace' )

          do iwave = 1,wavelHRS%nwavel

            if ( first_time_profile ) then
              cloudyPart = .false.
              call getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelHRS, XsecHRS, RRS_RingS,     &
                                  controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,  &
                                  mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,              &
                                  surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave,           &
                                  reflDerivHRS%depolarization(iwave))
              if (errorCheck(errS)) return
              first_time_profile = .false.
            end if ! first_time_profile

            ! interpolate the altitude resolved air mass factor m(z) to the RTM sublayer
            ! altitude grid

            ! cloudy part
            if ( cloudAerosolRTMgridS%useLambertianCloud ) then

              ! interpolation for wfkabs
              wfk_Lambcld = - refl_smooth_cld(iwave) * reflDerivHRS%altResAMFabs_cld(iwave, startIndex:endIndex)

              wfkabs_cld_int = 0.0d0
              do ialt = optPropRTMGridS%indexRTMSubFitIntTop, optPropRTMGridS%RTMnlayerSub
                wfkabs_cld_int(ialt) =  splintLin(errS, RTMalt, wfk_Lambcld, optPropRTMGridS%RTMaltitudeSub(ialt), status)
              end do

            else  ! cloudAerosolRTMgridS%useLambertianCloud

              wfInterfkabs_cld(:) = - refl_smooth_cld(iwave) * reflDerivHRS%altResAMFabs_cld(iwave,:)

              do ialt = 0, optPropRTMGridS%RTMnlayerSub
                wfkabs_cld_int(ialt) =  splintLin(errS, optPropRTMGridS%RTMaltitude, wfInterfkabs_cld, &
                                                  optPropRTMGridS%RTMaltitudeSub(ialt), status)
              end do

            end if ! cloudAerosolRTMgridS%useLambertianCloud

            ! clear part
            wfInterfkabs_clr(:) = - refl_smooth_clr(iwave) * reflDerivHRS%altResAMFabs_clr(iwave,:)

            do ialt = 0, optPropRTMGridS%RTMnlayerSub
               wfkabs_clr_int(ialt) =  splintLin(errS, optPropRTMGridS%RTMaltitude, wfInterfkabs_clr, &
                                                 optPropRTMGridS%RTMaltitudeSub(ialt), status)
            end do

            iTrace = retrS%codeTraceGas(istate)
            ialt = retrS%codeAltitude(istate)
            nAlt = traceGasS(iTrace)%nalt

            call calculate_wfnode(errS, iTrace, iAlt, nAlt, optPropRTMGridS, dn_dnodeS(iTrace)%dn_dnode,  &
                                  wfkabs_clr_int, wfnode_clr_lnvmr, wfnode_clr_vmr, wfnode_clr_ndens)
            if (errorCheck(errS)) return
            call calculate_wfnode(errS, iTrace, iAlt, nAlt, optPropRTMGridS, dn_dnodeS(iTrace)%dn_dnode,  &
                                  wfkabs_cld_int, wfnode_cld_lnvmr, wfnode_cld_vmr, wfnode_cld_ndens)
            if (errorCheck(errS)) return

            ! assign values note that the full absorption cross section and not the differential
            ! absorption cross section is used here
            reflDerivHRS%K_clr_lnvmr(iwave,istate) = wfnode_clr_lnvmr
            reflDerivHRS%K_clr_vmr  (iwave,istate) = wfnode_clr_vmr
            reflDerivHRS%K_clr_ndens(iwave,istate) = wfnode_clr_ndens
            reflDerivHRS%K_cld_lnvmr(iwave,istate) = wfnode_cld_lnvmr
            reflDerivHRS%K_cld_vmr  (iwave,istate) = wfnode_cld_vmr
            reflDerivHRS%K_cld_ndens(iwave,istate) = wfnode_cld_ndens

          end do ! iwave

        case('columnTrace')

          iTrace = retrS%codeTraceGas(istate)

          Msigma_cld(:) = amfAltAveragedHR_cld(:,iTrace)*XsecAltAveragedHRdiff_cld(:,iTrace)
          Msigma_clr(:) = amfAltAveragedHR_clr(:,iTrace)*XsecAltAveragedHRdiff_clr(:,iTrace)
          reflDerivHRS%K_clr_vmr(:,istate) = reflDerivHRS%K_clr_vmr(:,istate) &
                                           - reflDerivHRS%refl_clr(1,:) * Msigma_clr(:)
          reflDerivHRS%K_cld_vmr(:,istate) = reflDerivHRS%K_cld_vmr(:,istate) &
                                           - reflDerivHRS%refl_cld(1,:) * Msigma_cld(:)

          reflDerivHRS%K_clr_ndens(:,istate) = reflDerivHRS%K_clr_vmr(:,istate)
          reflDerivHRS%K_cld_ndens(:,istate) = reflDerivHRS%K_cld_vmr(:,istate)
         ! reflDerivHRS%K_clr_lnvmr(:,istate) = reflDerivHRS%K_clr_vmr(:,istate) * traceGasS(iTrace)%column
         ! reflDerivHRS%K_cld_lnvmr(:,istate) = reflDerivHRS%K_cld_vmr(:,istate) * traceGasS(iTrace)%column
          reflDerivHRS%K_clr_lnvmr(:,istate) = reflDerivHRS%K_clr_vmr(:,istate)
          reflDerivHRS%K_cld_lnvmr(:,istate) = reflDerivHRS%K_cld_vmr(:,istate)

        case('nodeTemp', 'offsetTemp', 'surfPressure', 'surfAlbedo', 'surfEmission', 'LambCldAlbedo', &
             'mulOffset', 'straylight', 'intervalTop', 'aerosolTau', 'aerosolSSA', 'aerosolAC', 'cloudTau', &
             'cloudAC', 'cloudFraction', 'intervalBot' )

        case('intervalDP')

          indexTop = optPropRTMGridS%indexRTMFitIntTop
          indexBot = optPropRTMGridS%indexRTMFitIntBot

          if ( cloudAerosolRTMgridS%useLambertianCloud ) then

            ! Lambertian cloud

            do iwave = 1,wavelHRS%nwavel

              sum_ndens_top_sigma_d = 0.0d0
              do iTrace = 1, nTrace
                sum_ndens_top_sigma_d = sum_ndens_top_sigma_d + XsecAltAveragedHRdiff_cld(iwave, iTrace) &
                                                              * optPropRTMGridS%ndensGas(indexTop,iTrace)
              end do ! iTrace

              m_cld_top = reflDerivHRS%altResAMFabs_cld(iwave, indexTop)
              m_clr_top = reflDerivHRS%altResAMFabs_clr(iwave, indexTop)

              ! dz in km while  sum_ndens_sigma_d in cm-1 => factor 1.0d5
              reflDerivHRS%K_clr_lnvmr(iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate) &
                    + 1.0d5 * reflDerivHRS%refl_clr(1,iwave) * m_clr_top * sum_ndens_top_sigma_d
              reflDerivHRS%K_cld_lnvmr(iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate) &
                    + 1.0d5 * reflDerivHRS%refl_cld(1,iwave) * m_cld_top * sum_ndens_top_sigma_d

              ! the altitude is fitted, not the logarithm of the altitude
              ! and the values for lnvmr, vmr, and ndens are the same
              reflDerivHRS%K_clr_vmr  (iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate)
              reflDerivHRS%K_cld_vmr  (iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate)
              reflDerivHRS%K_clr_ndens(iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate)
              reflDerivHRS%K_cld_ndens(iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate)

            end do ! iwave

          else

            ! scattering cloud or aerosol layer

            do iwave = 1,wavelHRS%nwavel

              sum_ndens_top_sigma_d = 0.0d0
              sum_ndens_bot_sigma_d = 0.0d0
              do iTrace = 1, nTrace
                sum_ndens_top_sigma_d = sum_ndens_top_sigma_d + XsecAltAveragedHRdiff_cld(iwave, iTrace) &
                                                              * optPropRTMGridS%ndensGas(indexTop,iTrace)
                sum_ndens_bot_sigma_d = sum_ndens_bot_sigma_d + XsecAltAveragedHRdiff_cld(iwave, iTrace) &
                                                              * optPropRTMGridS%ndensGas(indexBot,iTrace)
              end do ! iTrace

              m_cld_top = reflDerivHRS%altResAMFabs_cld(iwave, indexTop)
              m_cld_bot = reflDerivHRS%altResAMFabs_cld(iwave, indexBot)
              m_clr_top = reflDerivHRS%altResAMFabs_clr(iwave, indexTop)
              m_clr_bot = reflDerivHRS%altResAMFabs_clr(iwave, indexBot)

              ! dz in km while  sum_ndens_sigma_d in cm-1 => factor 1.0d5
              reflDerivHRS%K_clr_lnvmr(iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate)   &
                    + 1.0d5 * reflDerivHRS%refl_clr(1,iwave)                                    &
                    * ( m_clr_top * sum_ndens_top_sigma_d - m_clr_bot * sum_ndens_bot_sigma_d )
              reflDerivHRS%K_cld_lnvmr(iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate)   &
                    + 1.0d5 * reflDerivHRS%refl_cld(1,iwave)                                    &
                    * ( m_cld_top * sum_ndens_top_sigma_d - m_cld_bot * sum_ndens_bot_sigma_d )

              reflDerivHRS%K_clr_vmr  (iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate)
              reflDerivHRS%K_cld_vmr  (iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate)
              reflDerivHRS%K_clr_ndens(iwave,istate) = reflDerivHRS%K_clr_lnvmr(iwave,istate)
              reflDerivHRS%K_cld_ndens(iwave,istate) = reflDerivHRS%K_cld_lnvmr(iwave,istate)

            end do ! iwave

          end if ! cloudAerosolRTMgridS%useLambertianCloud

          case('lnpolyCoef', 'diffRingCoef', 'RingCoef')
            ! do nothing here
            ! needed if DOAS is used in the retrieval or Ring spectra

        case default

          call logDebug('in subroutine accountForDifferentialAbs in module dismasModule')
          call logDebug('incorrect value for retrS%codeFitParameters(istate) : '// retrS%codeFitParameters(istate))
          call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
          if (errorCheck(errS)) return

      end select

    end do ! istate loop


    ! complete the calculation of derivatives for polynomials
    call setPolynomialDerivatives_clr(errS, iband, nTrace, wavelHRS, retrS, cloudAerosolRTMgridS, &
                                      surfaceS, LambCloudS, cldAerFractionS, mulOffsetS, strayLightS, reflDerivHRS)
    if (errorCheck(errS)) return
    call setPolynomialDerivatives_cld(errS, iband, nTrace, wavelHRS, retrS, cloudAerosolRTMgridS, &
                                      surfaceS, LambCloudS, cldAerFractionS, mulOffsetS, strayLightS, reflDerivHRS)
    if (errorCheck(errS)) return

    ! set cloud fraction
    do iwave = 1, wavelHRS%nwavel
      if ( cloudAerosolRTMgridS%useCldAerFractionAllBands) then
        cf(iwave) = cloudAerosolRTMgridS%cldAerFractionAllBands
      else
        cf(iwave) = polyInt(errS, cldAerFractionS%wavelCldAerFraction, cldAerFractionS%cldAerFraction, wavelHRS%wavel(iwave))
      end if
    end do ! iwave

    ! combine clear and cloudy parts for the reflectance
    ! allow a cloud fraction larger than 1.0, but ignore the clear part in that case

    where ( cf < 1.0d0 )
      refl_smooth(:)         = cf(:) * refl_smooth_cld(:) + (1.0d0 - cf(:)) * refl_smooth_clr(:)
      reflDerivHRS%refl(1,:) = cf(:) * reflDerivHRS%refl_cld(1,:) + (1.0d0 - cf(:)) * reflDerivHRS%refl_clr(1,:)
    elsewhere
      refl_smooth(:)         = cf(:) * refl_smooth_cld(:)
      reflDerivHRS%refl(1,:) = cf(:) * reflDerivHRS%refl_cld(1,:)
    end where

    ! combine clear and cloudy parts for the derivatives

    do istate = 1, retrS%nstate

      where ( cf < 1.0d0 )
        reflDerivHRS%K_lnvmr(:,istate) = cf(:) * reflDerivHRS%K_cld_lnvmr(:,istate)  &
                                       + (1.0d0 - cf(:)) * reflDerivHRS%K_clr_lnvmr(:,istate)
        reflDerivHRS%K_vmr  (:,istate) = cf(:) * reflDerivHRS%K_cld_vmr(:,istate)    &
                                       + (1.0d0 - cf(:)) * reflDerivHRS%K_clr_vmr(:,istate)
        reflDerivHRS%K_ndens(:,istate) = cf(:) * reflDerivHRS%K_cld_ndens(:,istate)  &
                                       + (1.0d0 - cf(:)) * reflDerivHRS%K_clr_ndens(:,istate)
      elsewhere
        reflDerivHRS%K_lnvmr(:,istate) = cf(:) * reflDerivHRS%K_cld_lnvmr(:,istate)
        reflDerivHRS%K_vmr  (:,istate) = cf(:) * reflDerivHRS%K_cld_vmr(:,istate)
        reflDerivHRS%K_ndens(:,istate) = cf(:) * reflDerivHRS%K_cld_ndens(:,istate)
      end where

    end do ! istate

    ! derivatives for the column
    do istate = 1, reflDerivHRS%nstateCol

      where ( cf < 1.0d0 )
        reflDerivHRS%KHR_ndensCol(:,istate) = cf(:) * reflDerivHRS%KHR_cld_ndensCol(:,istate)  &
                                  + (1.0d0 - cf(:)) * reflDerivHRS%KHR_clr_ndensCol(:,istate)
      elsewhere
          reflDerivHRS%KHR_ndensCol(:,istate) = cf(:) * reflDerivHRS%KHR_cld_ndensCol(:,istate)
      end where
    end do ! istateCol

    ! clean up allocated arrays for Lambertian cloud
    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( RTMalt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus
    deallocate( wfk_Lambcld, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if ( sumdeallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: deallocation failed')
      call logDebug('forRTMalt, wfk_Lambcld or SDwfk_Lambcld')
      call logDebug('in subroutine accountForDifferentialAbs')
      call logDebug('in module dismasModule')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

    if ( verbose ) then
      write(intermediateFileUnit,*) 'DISMAS output : smooth and total reflectance on HR grid'
      do iwave = 1,wavelHRS%nwavel
        write(intermediateFileUnit,'(F12.6,2F15.10)') wavelHRS%wavel(iwave), &
                              refl_smooth(iwave), reflDerivHRS%refl(1,iwave)
      end do ! iwave

      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'dismas output: derivatives K_lnvmr on HR wavelength grid'
      write(intermediateFileUnit,*) 'wavelength    K_lnvmr'
      do iwave = 1, wavelHRS%nwavel
        write(intermediateFileUnit,'(F12.6, 100ES17.8)') wavelHRS%wavel(iwave), &
            ( reflDerivHRS%K_lnvmr(iwave,istate), istate = 1, reflDerivHRS%nstate )
      end do

    end if ! verbose

  end subroutine accountForDifferentialAbs


  subroutine finishDismas(errS, nTrace, wavelRTMS_clr, wavelRTMS_cld, XsecRTMS_clr, XsecRTMS_cld, &
                          XsecHRRTMaltS,  reflDerivRTMS)

    implicit none

      type(errorType), intent(inout) :: errS
    integer,             intent(in)    :: nTrace                 ! number of trace gases
    type(wavelHRType),   intent(inout) :: wavelRTMS_clr          ! wavelength grid for RTM calculations
    type(wavelHRType),   intent(inout) :: wavelRTMS_cld          ! wavelength grid for RTM calculations
    type(XsecType),      intent(inout) :: XsecRTMS_clr(nTrace)
    type(XsecType),      intent(inout) :: XsecRTMS_cld(nTrace)
    type(XsecType),      intent(inout) :: XsecHRRTMaltS(nTrace)
    type(reflDerivType), intent(inout) :: reflDerivRTMS

    ! local
    integer :: iTrace

    ! clean up

    call freeMemWavelHRS(errS,  wavelRTMS_clr )
    if (errorCheck(errS)) return
    call freeMemWavelHRS(errS,  wavelRTMS_cld )
    if (errorCheck(errS)) return
    call freeMemReflDerivS(errS,  reflDerivRTMS )
    if (errorCheck(errS)) return

    do iTrace = 1, nTrace
      call freeMemXsecS(errS,  XsecRTMS_clr(iTrace) )
      if (errorCheck(errS)) return
      call freeMemXsecS(errS,  XsecRTMS_cld(iTrace) )
      if (errorCheck(errS)) return
      call freeMemXsecS(errS,  XsecHRRTMaltS(iTrace) )
      if (errorCheck(errS)) return
    end do ! iTrace

  end subroutine finishDismas


end module dismasModule
