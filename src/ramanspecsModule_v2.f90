!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

MODULE ramansspecs


! VERSION 2.0, May 05, 2003
! AUTHOR: Johan F. de Haan

! This module specifies parameters for Rotational Raman
! scattering in the atmosphere by O2 and N2.
! Based on molecular parameters provided by Kelly Chance.
! VERSION 2 is changed to support object oriented programming.

! Comparisons for the Raman cross sections and resulting Ring spectra
! have been made with results calculated with a code of Kelly Chance and
! the agreement is very good.

! INTERFACE description
! The data structures have been made private and access is only possible
! through the following functions

! FUNCTION RayXsec(wavelength)  
!          Returns the Rayleigh scattering cross section in cm^2 per molecule, wavelength in nm
!
! FUNCTION RayXsec_Chance_Spurr(wavelength)
!          Gives the Rayleigh scattering cross section in m^2, wavelength in nm
!          See Applied Optics, 36, 5224-5230, 1997
!
! FUNCTION CabannesXsec(temperature, wavelength)
!          Returns the Cabannes scattering cross section in m^2 , wavelength in nm
!          temperature in Kelvin.

! FUNCTION NumberRamanLines()
!          Returns the number of Raman lines and may be used to allocate an array.

! FUNCTION RamanLinesIncWavel(wavelength)
!          Returns the ARRAY RamanLinesIncWavel(1:NumberRamanLines()) containing the
!          wavelengths of the Raman lines in nm, The argument wavelength is the
!          wavelength of the incident light in nm. 
!          The first part are lines for N2, the second parts are those of O2.

! FUNCTION RamanLinesScatWavel(wavelengthPrime)
!          Returns the ARRAY RamanLinesScatWavel(1:NumberRamanLines()) containing the
!          wavelengths of the Raman lines in nm, The argument wavelengthPrime is the
!          wavelength of the scattered light in nm. 
!          The first part are lines for N2, the second parts are those of O2.

! FUNCTION RamanXsecIncWavel(temperature, wavelength)
!          Returns the ARRAY RamanXsecIncWavel(1:NumberRamanLines())
!          containing the scatering cross sections in m^2, 
!          Here wavelength is the wavelength of the incident light
!          in nm and the temperature is the temperature of the scattering
!          gas molecules (N2 and O2) in Kelvin. The cross sections correspond
!          to the wavelengths calculated with FUNCTION RamanLinesIncWavel(wavelength).

! FUNCTION RamanXsecScatWavel(temperature, wavelengthPrime)
!          Returns the ARRAY RamanXsecScatWavel(1:NumberRamanLines())
!          containing the scatering cross sections in m^2.
!          Here wavelengthPrime is the wavelength of the scattered light 
!          in nm and the temperature is the temperature of the scattering
!          gas molecules (N2 and O2) in Kelvin. It gives the contributions
!          for scattered light that emerges at one wavelength, namely wavelengthPrime,
!          which originates from scattering at different wavelengths.
!          The cross sections correspomd with the wavelengths calculated with
!          FUNCTION RamanLinesScatWavel(wavelength)

! FUNCTION ZRayleigh(scat_angle, wavelength)
!          Returns the Rayleigh phase function. scat_angle in degrees, wavelength in nm.

! FUNCTION ZCabannes(scat_angle, wavelength)
!          Returns the Cabannes phase function. scat_angle in degrees, wavelength in nm.

! FUNCTION ZRaman(Scat_angle)
!          Returns the Raman phase function. scat_angle in degrees.
!          ZRaman does not depend on the wavelength and is the same for each Raman line.
!
! other functions and subroutines that are available (errS, wavelength in nm, temperature in Kelvin)
! SUBROUTINE fracPopulation(temperature)  calculates the fractional population of levels
! FUNCTION RefrIndexSTP(wavelength)       refractive index of dry air at STP
! FUNCTION RelAnisoPol(wavelength)        square of the rel. anisotropy of the polarizability
! FUNCTION gammaO2(wavelength)            anisotropy of the polarizability of O2 in m^2
! FUNCTION gammaN2(wavelength)            anisotropy of the polarizability of N2 in m^2
! SUBROUTINE ConvoluteSpecRaman(wavelengthGrid, nSpec, spec, specConv, status) to convolute spectra

! EXAMPLE for the use of this module
!
!     PROGRAM UseRamanSpecModule
!
!       USE RAMANSPECS
!
!       IMPLICIT NONE
!
!       INTEGER                 :: k
!       REAL(8), ALLOCATABLE    :: Raman_wavelengths (:), RamanCrossSec(:)
!       REAL(8)                 :: temperature, wavelength, CabannesCrossSec
!
!       temperature = 250.0          ! temperature of the scattering gas in Kelvin
!       wavelength  = 340.0          ! in nm
!  
!       ALLOCATE (Raman_wavelengths(NumberRamanLines()), RamanCrossSec(NumberRamanLines()))
!
!       !put wavelengths of Raman lines in the array Raman_wavelengths 
!       Raman_wavelengths = RamanLinesIncWavel(wavelength)
!
!       !put cross sections of Raman lines in the array RamanCrossSec
!       RamanCrossSec     = RamanXsecIncWavel(temperature, wavelength)
!
!       !the Cabannes scattering cross section is a scalar
!       CabannesCrossSec  = CabannesXsec(temperature, wavelength)
!       ...........
!
!       DEALLOCATE (Raman_wavelengths, RamanCrossSec)
!
!     END PROGRAM UseRamanSpecModule


! Changes with respect to previous versions
! March, 16, 2003: FUNCTION RamanXsec(k, wavelength) modified to bring it in line
!                  with Eq. (10) in Chance, K.V. and Spurr, J.D., Ring effect studies..., 
!                  Applied Optics, Vol. 36, 5224-5230, 1997. Note that
!                  wavelengthPrime instead wavelength is used when calculating f.
!                  The resulting Raman cross sections change up to a few percent.
!                  This change agrees with Penny et al. J. Opt. Soc. Am. 64, 712-716, 1974,
!                  but seems to disagree with the last sentences of Sec. 2  in
!                  Joiner et al. Applied Optics, 34, 4513-4525, 1995. I do not understand
!                  the argument given by Joiner et al. that wavelengthPrime should not
!                  be used when calculating the relative strength of the lines
!                  for backscattered light.
!
! April, 20, 2003  Second part of DATA N2tran%g changed. Note that in N2tran%J near
!                  the middle two successive values of J are �even� and gJ has the
!                  value 6 for these entries. That pattern change was ignored previously.
!                  Comparison with the cross section calculated with Ringomi.f (code
!                  by Kelly Chance) agrees with the cross sections here in at least
!                  five decimal places.

  use dataStructures
  use mathTools,  only: splintLin, splint, spline, indexx

  ! default is private
  private 
  public  :: RayXsec, CabannesXsec, CabannesAlbedo, NumberRamanLines, RamanLinesIncWavel, RamanLinesScatWavel
  public  :: RamanXsecIncWavel, RamanXsecScatWavel, TotalRamanXsecScatWavel
  public  :: ConvoluteSpecRaman, ConvoluteSpecRamanMS, depolarization_factor_air


  integer, PARAMETER, PRIVATE       :: n_statesN2= 31, n_statesO2= 54
  integer, PARAMETER, PRIVATE       :: n_transN2 = 48, n_transO2 = 185
  REAL(8), PARAMETER, PRIVATE       :: PI=3.141592653589793238d0


  TYPE Energy_state_N2
    integer            J(n_statesN2)   ! rotational quantum number
    integer            g(n_statesN2)   ! weight due to nuclear spin
    REAL(8)            E(n_statesN2)   ! energy of state in cm-1
  END TYPE Energy_state_N2

  TYPE Energy_state_O2
    integer            N(n_statesO2)   ! vibrational quantum number
    integer            J(n_statesO2)   ! rotational quantum number
    integer            g(n_statesO2)   ! weight due to nuclear spin
    REAL(8)            E(n_statesO2)   ! energy of state in cm-1
  END TYPE Energy_state_O2

  ! Jprime is not used in the calculations, but may be used for
  ! checking purposes
  TYPE Transition_N2
    integer            J(n_transN2)      ! rot. quantum number initial state
    integer            Jprime(n_transN2) ! rot. quantum number final state
    integer            g(n_transN2)      ! weight due to nuclear spin
    REAL(8)            E(n_transN2)      ! energy of initial state cm-1
    REAL(8)            DE(n_transN2)     ! transition energy cm-1
    REAL(8)            CPT(n_transN2)    ! Placzek-Teller coefficient
    REAL(8)            f(n_transN2)      ! fractional population of the levels
  END TYPE Transition_N2

  ! N, Nprime, and Jprime are not used in the calculations, but they
  ! may be used for checking purposes
  ! No g added because it is assumed to be one everywhere
  TYPE Transition_O2
    integer            N(n_transO2)     ! vibr. quantum number initial state
    integer            J(n_transO2)     ! rot.  quantum number initial state
    integer            Nprime(n_transO2)! vibr. quantum number final state
    integer            Jprime(n_transO2)! rot.  quantum number final state
    integer            g(n_transO2)     ! weight due to nuclear spin
    REAL(8)            E(n_transO2)     ! energy of initial state cm-1
    REAL(8)            DE(n_transO2)    ! transition energy cm-1
    REAL(8)            CPT(n_transO2)   ! Placzek-Teller coefficient
    REAL(8)            f(n_transO2)     ! fractional population of the levels
  END TYPE Transition_O2

  TYPE (Energy_state_N2), PRIVATE   :: N2state
  TYPE (Energy_state_O2), PRIVATE   :: O2state
  TYPE (Transition_N2)  , PRIVATE   :: N2tran
  TYPE (Transition_O2)  , PRIVATE   :: O2tran
  
  DATA N2state%J / 0, 1, 2, 3, 4, 5, 6, 7, 8,       &
         9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, &
        20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 /

  DATA N2state%g / 6, 3, 6, 3, 6, 3, 6, 3, 6,       &
                   3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, &
                   6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6 /
  
  DATA N2state%E  / 0.00, 3.9791,   11.9373,   23.8741,   39.7892,&
              59.6821,   83.5521,  111.3983,  143.2197,  179.0154,&
             218.7839,  262.5240,  310.2341,  361.9126,  417.5576,&
             477.1673,  540.7395,  608.2722,  679.7628,  755.2090,&
             834.6081,  917.9574, 1005.2540, 1096.4948, 1191.6766,&
            1290.7963, 1393.8503, 1500.8350, 1611.7467, 1726.5816,&
            1845.3358 /

  DATA O2state%N  / 1, 1, 1, 3, 3, 3, 5, 5, 5, 7, 7, 7, 9, 9,     &
     9, 11, 11, 11, 13, 13, 13, 15, 15, 15, 17, 17, 17, 19, 19,   &
    19, 21, 21, 21, 23, 23, 23, 25, 25, 25, 27, 27, 27, 29, 29,   &
    29, 31, 31, 31, 33, 33, 33, 35, 35, 35 /

  DATA O2state%J  / 0,   2,   1,   2,   4,   3,   4,   6,   5,   8, &
                    6,   7,  10,   8,   9,  12,  10,  11,  14,  12, &
                   13,  16,  14,  15,  18,  16,  17,  20,  18,  19, &
                   22,  20,  21,  24,  22,  23,  26,  24,  25,  28, &
                   26,  27,  30,  28,  29,  32,  30,  31,  34,  32, &
                   33,  36,  34,  35 /

! The code of K. Chance seems to suggest gJ = 1, even if J is even
! Now gj = 0 for J even and gJ = 1 for J odd (Nuclear spin O2 = 1, N2 = 0
!   DATA O2state%g  / 0,   0,   1,   0,   0,   1,   0,   0,   1,   0, &
!                     0,   1,   0,   0,   1,   0,   0,   1,   0,   0, &
!                     1,   0,   0,   1,   0,   0,   1,   0,   0,   1, &
!                     0,   0,   1,   0,   0,   1,   0,   0,   1,   0, &
!                     0,   1,   0,   0,   1,   0,   0,   1,   0,   0, &
!                     1,   0,   0,   1 /
! The code of K. Chance seems to suggest gJ = 1, even if J is even
! Chance noted that only odd values of N (not J) have to be taken into account
! in reply to a question on this issue. (see DATA O2tran%N)
  DATA O2state%g  / 1,   1,   1,   1,   1,   1,   1,   1,   1,   1, &
                    1,   1,   1,   1,   1,   1,   1,   1,   1,   1, &
                    1,   1,   1,   1,   1,   1,   1,   1,   1,   1, &
                    1,   1,   1,   1,   1,   1,   1,   1,   1,   1, &
                    1,   1,   1,   1,   1,   1,   1,   1,   1,   1, &
                    1,   1,   1,   1 /

  DATA O2state%E /                                                &
      0.0000,    2.0843,    3.9611,   16.2529,  16.3876,  18.3372,&
     42.2001,   42.2240,   44.2117,   79.5646,  79.6070,  81.5805,&
    128.3978,  128.4921,  130.4376,  188.7135, 188.8532, 190.7749,&
    260.5011,  260.6826,  262.5829,  343.7484, 343.9697, 345.8500,&
    438.4418,  438.7015,  440.5620,  544.5658, 544.8628, 546.7050,&
    662.1030,  662.4368,  664.2610,  791.0344, 791.4045, 793.2100,&
    931.3390,  931.7450,  933.5330, 1082.9941, 1083.4356,         &
   1085.2060, 1245.9750, 1246.4518, 1248.2040, 1420.2552,         &
   1420.7672, 1422.5020, 1605.8064, 1606.3533, 1608.0710,         &
   1802.5983, 1803.1802, 1804.8810 /

  DATA N2tran%J      / 25, 24, 23, 22, 21, 20, 19, 18, 17, 16,   &
                       15, 14, 13, 12, 11, 10,  9,  8,  7,  6,   &
                        5,  4,  3,  2,  0,  1,  2,  3,  4,  5,   &
                        6,  7,  8,  9, 10, 11, 12, 13, 14, 15,   &
                       16, 17, 18, 19, 20, 21, 22, 23 /

  DATA N2tran%Jprime / 23, 22, 21, 20, 19, 18, 17, 16, 15, 14,   &
                       13, 12, 11, 10,  9,  8,  7,  6,  5,  4,   &
                        3,  2,  1,  0,  2,  3,  4,  5,  6,  7,   &
                        8,  9, 10, 11, 12, 13, 14, 15, 16, 17,   &
                       18, 19, 20, 21, 22, 23, 24, 25 /

  DATA N2tran%g /       3,  6,  3,  6,  3,  6,  3,  6,  3,  6,   &
                        3,  6,  3,  6,  3,  6,  3,  6,  3,  6,   &
                        3,  6,  3,  6,  6,  3,  6,  3,  6,  3,   &
                        6,  3,  6,  3,  6,  3,  6,  3,  6,  3,   &
                        6,  3,  6,  3,  6,  3,  6,  3 /

  DATA N2tran%E /                                                &
         1290.7963, 1191.6766, 1096.4948, 1005.2540, 917.9574,   &
          834.6081,  755.2090,  679.7628,  608.2722, 540.7395,   &
          477.1673,  417.5576,  361.9126,  310.2341, 262.5240,   &
          218.7839,  179.0154,  143.2197,  111.3983,  83.5521,   &
           59.6821,   39.7892,   23.8741,   11.9373,   0.0000,   &
            3.9791,   11.9373,   23.8741,   39.7892,  59.6821,   &
           83.5521,  111.3983,  143.2197,  179.0154, 218.7839,   &
          262.5240,  310.2341,  361.9126,  417.5576, 477.1673,   &
          540.7395,  608.2722,  679.7628,  755.2090, 834.6081,   &
          917.9574, 1005.2540, 1096.4948 /

  DATA N2tran%DE /                                               &
         -194.3015, -186.4226, -178.5374, -170.6459, -162.7484,  &
         -154.8453, -146.9368, -139.0233, -131.1049, -123.1819,  &
         -115.2547, -107.3235,  -99.3886,  -91.4502,  -83.5086,  &
          -75.5642,  -67.6171,  -59.6676,  -51.7162,  -43.7629,  &
          -35.8080,  -27.8519,  -19.8950,  -11.9373,   11.9373,  &
           19.8950,   27.8519,   35.8080,   43.7629,   51.7162,  &
           59.6676,   67.6171,   75.5642,   83.5086,   91.4502,  &
           99.3886,  107.3235,  115.2547,  123.1819,  131.1049,  &
          139.0233,  146.9368,  154.8453,  162.7484,  170.6459,  &
          178.5374,  186.4226,  194.3015 /

  DATA N2tran%CPT /                                              &
          3.601d-1,  3.595d-1,  3.589d-1,  3.581d-1,  3.573d-1,  &
          3.565d-1,  3.555d-1,  3.544d-1,  3.532d-1,  3.519d-1,  &
          3.504d-1,  3.487d-1,  3.467d-1,  3.443d-1,  3.416d-1,  &
          3.383d-1,  3.344d-1,  3.294d-1,  3.231d-1,  3.147d-1,  &
          3.030d-1,  2.857d-1,  2.571d-1,  2.000d-1,  1.000d+0,  &
          6.000d-1,  5.143d-1,  4.762d-1,  4.545d-1,  4.406d-1,  &
          4.308d-1,  4.235d-1,  4.180d-1,  4.135d-1,  4.099d-1,  &
          4.070d-1,  4.044d-1,  4.023d-1,  4.004d-1,  3.988d-1,  &
          3.974d-1,  3.961d-1,  3.950d-1,  3.940d-1,  3.931d-1,  &
          3.922d-1,  3.915d-1,  3.908d-1 /

  DATA O2tran%N /       33, 33, 33, 31, 31, 31, 29, 29, 29, 27,  &
                        27, 27, 25, 25, 25, 23, 23, 23, 21, 21,  &
                        21, 19, 19, 19, 19, 19, 17, 17, 17, 17,  &
                        17, 15, 15, 15, 15, 15, 13, 13, 13, 13,  &
                        13, 11, 11, 11, 11, 11,  9,  9,  9,  9,  &
                         9,  5,  7,  7,  7,  7,  7,  5,  5,  5,  &
                         5,  5,  5,  3,  3,  3,  3,  3,  3, 19,  &
                        17, 15,  1,  3, 13, 11,  9,  7,  5,  5,  &
                         7,  3,  9, 11, 13, 15,  1, 17, 19,  3,  &
                         3, 19, 17,  1, 15, 13, 11,  9,  3,  7,  &
                         5,  5,  7,  9, 11, 13,  1,  3, 15, 17,  &
                        19,  1,  1,  1,  1,  1,  1,  3,  3,  3,  &
                         3,  3,  3,  5,  5,  5,  5,  5,  1,  7,  &
                         7,  7,  7,  7,  9,  9,  9,  9,  9, 11,  &
                        11, 11, 11, 11, 13, 13, 13, 13, 13, 15,  &
                        15, 15, 15, 15, 17, 17, 17, 17, 17, 19,  &
                        19, 19, 19, 19, 21, 21, 21, 23, 23, 23,  &
                        25, 25, 25, 27, 27, 27, 29, 29, 29, 31,  &
                        31, 31, 33, 33, 33 /

  DATA O2tran%J /       32, 33, 34, 30, 31, 32, 28, 29, 30, 26,  &
                        27, 28, 24, 25, 26, 22, 23, 24, 20, 21,  &
                        22, 19, 18, 19, 20, 18, 17, 16, 17, 18,  &
                        16, 15, 14, 15, 16, 14, 13, 12, 13, 14,  &
                        12, 11, 10, 11, 12, 10,  9,  8,  9, 10,  &
                         8,  4,  7,  6,  7,  8,  6,  5,  4,  5,  &
                         6,  4,  4,  2,  3,  3,  4,  2,  2, 19,  &
                        17, 15,  2,  3, 13, 11,  9,  7,  5,  5,  &
                         7,  3,  9, 11, 13, 15,  1, 17, 19,  4,  &
                         2, 18, 16,  2, 14, 12, 10,  8,  4,  6,  &
                         6,  4,  8, 10, 12, 14,  0,  2, 16, 18,  &
                        20,  1,  2,  2,  1,  0,  2,  3,  4,  4,  &
                         3,  2,  4,  5,  6,  5,  4,  6,  2,  7,  &
                         8,  7,  6,  8,  9, 10,  9,  8, 10, 11,  &
                        12, 11, 10, 12, 13, 14, 13, 12, 14, 15,  &
                        16, 15, 14, 16, 17, 18, 17, 16, 18, 19,  &
                        20, 19, 18, 20, 22, 21, 20, 24, 23, 22,  &
                        26, 25, 24, 28, 27, 26, 30, 29, 28, 32,  &
                        31, 30, 34, 33, 32 /

  DATA O2tran%Nprime /  31, 31, 31, 29, 29, 29, 27, 27, 27, 25,  &
                        25, 25, 23, 23, 23, 21, 21, 21, 19, 19,  &
                        19, 17, 17, 17, 17, 17, 15, 15, 15, 15,  &
                        15, 13, 13, 13, 13, 13, 11, 11, 11, 11,  &
                        11,  9,  9,  9,  9,  9,  7,  7,  7,  7,  &
                         7,  1,  5,  5,  5,  5,  5,  3,  3,  3,  &
                         3,  3,  3,  1,  1,  1,  1,  1,  1, 19,  &
                        17, 15,  1,  3, 13, 11,  9,  7,  5,  5,  &
                         7,  3,  9, 11, 13, 15,  1, 17, 19,  3,  &
                         3, 19, 17,  1, 15, 13, 11,  9,  3,  7,  &
                         5,  5,  7,  9, 11, 13,  1,  3, 15, 17,  &
                        19,  3,  3,  3,  3,  3,  3,  5,  5,  5,  &
                         5,  5,  5,  7,  7,  7,  7,  7,  5,  9,  &
                         9,  9,  9,  9, 11, 11, 11, 11, 11, 13,  &
                        13, 13, 13, 13, 15, 15, 15, 15, 15, 17,  &
                        17, 17, 17, 17, 19, 19, 19, 19, 19, 21,  &
                        21, 21, 21, 21, 23, 23, 23, 25, 25, 25,  &
                        27, 27, 27, 29, 29, 29, 31, 31, 31, 33,  &
                        33, 33, 35, 35, 35 /
 
  DATA O2tran%Jprime /  30, 31, 32, 28, 29, 30, 26, 27, 28, 24,  &
                        25, 26, 22, 23, 24, 20, 21, 22, 18, 19,  &
                        20, 18, 16, 17, 18, 17, 16, 14, 15, 16,  &
                        15, 14, 12, 13, 14, 13, 12, 10, 11, 12,  &
                        11, 10,  9,  9, 10,  9,  8,  6,  7,  8,  &
                         7,  2,  6,  4,  5,  6,  5,  4,  2,  3,  &
                         4,  4,  3,  0,  2,  1,  2,  2,  1, 20,  &
                        18, 16,  0,  2, 14, 12, 10,  8,  4,  6,  &
                         6,  4,  8, 10, 12, 14,  2, 16, 18,  2,  &
                         4, 19, 17,  1, 15, 13, 11,  9,  3,  7,  &
                         5,  5,  7,  9, 11, 13,  2,  3, 15, 17,  &
                        19,  2,  2,  4,  3,  2,  3,  4,  4,  6,  &
                         5,  4,  5,  6,  8,  7,  6,  7,  4,  8,  &
                        10,  9,  8,  9, 10, 12, 11, 10, 11, 12,  &
                        14, 13, 12, 13, 14, 16, 15, 14, 15, 16,  &
                        18, 17, 16, 17, 18, 20, 19, 18, 19, 20,  &
                        22, 21, 20, 21, 24, 23, 22, 26, 25, 24,  &
                        28, 27, 26, 30, 29, 28, 32, 31, 30, 34,  &
                        33, 32, 36, 35, 34 /


  DATA O2tran%E/                                                 &
         1606.3533, 1608.0710, 1605.8064, 1420.7672, 1422.5020,  &
         1420.2552, 1246.4518, 1248.2040, 1245.9750, 1083.4356,  &
         1085.2060, 1082.9941,  931.7450,  933.5330,  931.3390,  &
          791.4045,  793.2100,  791.0344,  662.4368,  664.2610,  &
          662.1030,  546.7050,  544.8628,  546.7050,  544.5658,  &
          544.8628,  440.5620,  438.7015,  440.5620,  438.4418,  &
          438.7015,  345.8500,  343.9697,  345.8500,  343.7484,  &
          343.9697,  262.5829,  260.6826,  262.5829,  260.5011,  &
          260.6826,  190.7749,  188.8532,  190.7749,  188.7135,  &
          188.8532,  130.4376,  128.4921,  130.4376,  128.3978,  &
          128.4921,   42.2001,   81.5805,   79.6070,   81.5805,  &
           79.5646,   79.6070,   44.2117,   42.2001,   44.2117,  &
           42.2240,   42.2001,   42.2001,   16.2529,   18.3372,  &
           18.3372,   16.3876,   16.2529,   16.2529,  546.7050,  &
          440.5620,  345.8500,    2.0843,   18.3372,  262.5829,  &
          190.7749,  130.4376,   81.5805,   44.2117,   44.2117,  &
           81.5805,   18.3372,  130.4376,  190.7749,  262.5829,  &
          345.8500,    3.9611,  440.5620,  546.7050,   16.3876,  &
           16.2529,  544.8628,  438.7015,    2.0843,  343.9697,  &
          260.6826,  188.8532,  128.4921,   16.3876,   79.6070,  &
           42.2240,   42.2001,   79.5646,  128.3978,  188.7135,  &
          260.5011,    0.0000,   16.2529,  343.7484,  438.4418,  &
          544.5658,    3.9611,    2.0843,    2.0843,    3.9611,  &
            0.0000,    2.0843,   18.3372,   16.3876,   16.3876,  &
           18.3372,   16.2529,   16.3876,   44.2117,   42.2240,  &
           44.2117,   42.2001,   42.2240,    2.0843,   81.5805,  &
           79.5646,   81.5805,   79.6070,   79.5646,  130.4376,  &
          128.3978,  130.4376,  128.4921,  128.3978,  190.7749,  &
          188.7135,  190.7749,  188.8532,  188.7135,  262.5829,  &
          260.5011,  262.5829,  260.6826,  260.5011,  345.8500,  &
          343.7484,  345.8500,  343.9697,  343.7484,  440.5620,  &
          438.4418,  440.5620,  438.7015,  438.4418,  546.7050,  &
          544.5658,  546.7050,  544.8628,  544.5658,  662.1030,  &
          664.2610,  662.4368,  791.0344,  793.2100,  791.4045,  &
          931.3390,  933.5330,  931.7450, 1082.9941, 1085.2060,  &
         1083.4356, 1245.9750, 1248.2040, 1246.4518, 1420.2552,  &
         1422.5020, 1420.7672, 1605.8064, 1608.0710, 1606.3533 /

  DATA O2tran%DE/                                                &
         -185.5861, -185.5690, -185.5512, -174.3154, -174.2980,  &
         -174.2802, -163.0162, -162.9980, -162.9809, -151.6906,  &
         -151.6730, -151.6551, -140.3405, -140.3230, -140.3046,  &
         -128.9677, -128.9490, -128.9314, -117.5740, -117.5560,  &
         -117.5372, -108.2632, -106.1613, -106.1430, -106.1240,  &
         -104.3008,  -96.8136,  -94.7318,  -94.7120,  -94.6934,  &
          -92.8515,  -85.3489,  -83.2871,  -83.2671,  -83.2473,  &
          -81.3868,  -73.8694,  -71.8294,  -71.8080,  -71.7876,  &
          -69.9077,  -62.3771,  -60.3611,  -60.3373,  -60.3157,  &
          -58.4156,  -50.8730,  -48.8851,  -48.8571,  -48.8332,  &
          -46.9116,  -40.1158,  -39.3565,  -37.4069,  -37.3688,  &
          -37.3406,  -35.3953,  -27.8241,  -25.9472,  -25.8745,  &
          -25.8364,  -25.8125,  -23.8629,  -16.2529,  -16.2529,  &
          -14.3761,  -14.3033,  -14.1686,  -12.2918,   -2.1392,  &
           -2.1202,   -2.1016,   -2.0843,   -2.0843,   -2.0818,  &
           -2.0614,   -2.0398,   -2.0159,   -2.0116,   -1.9877,  &
           -1.9735,   -1.9496,   -1.9455,   -1.9217,   -1.9003,  &
           -1.8803,   -1.8768,   -1.8605,   -1.8422,   -0.1347,  &
            0.1347,    1.8422,    1.8605,    1.8768,    1.8803,  &
            1.9003,    1.9217,    1.9455,    1.9496,    1.9735,  &
            1.9877,    2.0116,    2.0159,    2.0398,    2.0614,  &
            2.0818,    2.0843,    2.0843,    2.1016,    2.1202,  &
            2.1392,   12.2918,   14.1686,   14.3033,   14.3761,  &
           16.2529,   16.2529,   23.8629,   25.8125,   25.8364,  &
           25.8745,   25.9472,   27.8241,   35.3953,   37.3406,  &
           37.3688,   37.4069,   39.3565,   40.1158,   46.9116,  &
           48.8332,   48.8571,   48.8851,   50.8730,   58.4156,  &
           60.3157,   60.3373,   60.3611,   62.3771,   69.9077,  &
           71.7876,   71.8080,   71.8294,   73.8694,   81.3868,  &
           83.2473,   83.2671,   83.2871,   85.3489,   92.8515,  &
           94.6934,   94.7120,   94.7318,   96.8136,  104.3008,  &
          106.1240,  106.1430,  106.1613,  108.2632,  115.7318,  &
          117.5372,  117.5560,  117.5740,  119.6952,  128.9314,  &
          128.9490,  128.9677,  140.3046,  140.3230,  140.3405,  &
          151.6551,  151.6730,  151.6906,  162.9809,  162.9980,  &
          163.0162,  174.2802,  174.2980,  174.3154,  185.5512,  &
          185.5690,  185.5861,  196.7919,  196.8100, 196.8269 /

  DATA O2tran%CPT/                                               &
              3.630d-1, 3.630d-1, 3.637d-1, 3.622d-1, 3.622d-1,  &
              3.630d-1, 3.613d-1, 3.613d-1, 3.622d-1, 3.602d-1,  &
              3.602d-1, 3.612d-1, 3.589d-1, 3.589d-1, 3.601d-1,  &
              3.574d-1, 3.574d-1, 3.589d-1, 3.556d-1, 3.556d-1,  &
              3.573d-1, 2.079d-3, 3.533d-1, 3.534d-1, 3.555d-1,  &
              2.191d-3, 2.597d-3, 3.505d-1, 3.506d-1, 3.532d-1,  &
              2.755d-3, 3.337d-3, 3.468d-1, 3.471d-1, 3.504d-1,  &
              3.567d-3, 4.444d-3, 3.418d-1, 3.422d-1, 3.467d-1,  &
              4.800d-3, 6.211d-3, 3.348d-1, 3.354d-1, 3.416d-1,  &
              6.803d-3, 9.288d-3, 3.220d-1, 3.251d-1, 3.344d-1,  &
              1.127d-2, 1.062d-3, 1.387d-2, 3.013d-1, 3.077d-1,  &
              3.223d-1, 1.979d-2, 2.613d-2, 2.544d-1, 2.727d-1,  &
              3.020d-1, 1.476d-3, 4.342d-2, 9.234d-2, 6.596d-2,  &
              1.714d-1, 2.571d-1, 1.843d-2, 1.615d-1, 1.970d-3,  &
              2.445d-3, 3.116d-3, 1.077d-1, 7.690d-2, 4.105d-3,  &
              5.652d-3, 8.271d-3, 1.222d-2, 2.842d-2, 2.207d-2,  &
              1.470d-2, 5.132d-2, 8.256d-3, 5.647d-3, 4.103d-3,  &
              3.115d-3, 2.308d-1, 2.445d-3, 1.970d-3, 2.116d-3,  &
              3.810d-3, 2.076d-3, 2.593d-3, 1.385d-1, 3.329d-3,  &
              4.431d-3, 6.184d-3, 9.227d-3, 3.991d-2, 1.696d-2,  &
              1.867d-2, 3.474d-2, 1.078d-2, 7.483d-3, 5.200d-3,  &
              3.822d-3, 5.383d-1, 1.077d-1, 2.927d-3, 2.313d-3,  &
              1.874d-3, 2.692d-1, 1.843d-2, 4.628d-1, 4.000d-1,  &
              4.617d-1, 9.234d-2, 5.583d-2, 1.476d-3, 4.362d-1,  &
              4.286d-1, 4.579d-1, 3.193d-2, 2.339d-2, 4.214d-1,  &
              4.196d-1, 4.352d-1, 1.600d-2, 1.911d-3, 1.278d-2,  &
              4.130d-1, 4.118d-1, 4.210d-1, 1.038d-2, 7.519d-3,  &
              4.067d-1, 4.060d-1, 4.135d-1, 6.803d-3, 5.217d-3,  &
              4.021d-1, 4.017d-1, 4.070d-1, 4.800d-3, 3.831d-3,  &
              3.987d-1, 3.985d-1, 4.023d-1, 3.567d-3, 2.933d-3,  &
              3.961d-1, 3.959d-1, 3.988d-1, 2.755d-3, 2.317d-3,  &
              3.939d-1, 3.938d-1, 3.961d-1, 2.191d-3, 1.876d-3,  &
              3.922d-1, 3.921d-1, 3.940d-1, 1.785d-3, 3.908d-1,  &
              3.907d-1, 3.922d-1, 3.895d-1, 3.895d-1, 3.908d-1,  &
              3.885d-1, 3.885d-1, 3.896d-1, 3.876d-1, 3.876d-1,  &
              3.885d-1, 3.868d-1, 3.868d-1, 3.876d-1, 3.861d-1,  &
              3.861d-1, 3.868d-1, 3.855d-1, 3.855d-1, 3.861d-1 /

CONTAINS

  FUNCTION RayXsec(wavelength)

  ! Gives the Rayleigh scattering cross section in cm^2, wavelength in nm
  ! Based on the paper: "On Rayleigh Optical Depth Calculations" by
  ! BARRY A. BODHAINE, NORMAN B. WOOD, ELLSWORTH G. DUTTON and JAMES R. SLUSSER
  ! JOURNAL OF ATMOSPHERIC AND OCEANIC TECHNOLOGY, Vol.16, pp. 1854-1861, 1999.
  ! - no correction for water vapor is applied - therfore we have dry air here

    implicit none

    real(8), intent(in) :: wavelength         
    real(8)             :: RayXsec

    ! local
    real(8)             :: King_factor_air
    ! fractions are expressed as parts per volume by percent
    real(8), parameter  :: fraction_N2  = 78.084d0
    real(8), parameter  :: fraction_O2  = 20.946d0
    real(8), parameter  :: fraction_Ar  =  0.934d0
    real(8), parameter  :: fraction_CO2 =  0.036d0   ! for 360 ppmv CO2
    real(8)             :: sig, Xsec
    real(8)             :: refrIndex
    real(8), parameter  :: numdens_ref = 2.5468993d19  ! number of air molecules cm-3 at 288.15K and 1013.25 hPa
    real(8), parameter  :: PI = 3.141592653589793d0

    sig = 1.0d3/wavelength                  ! sig is wavenumber in micrometer^-1     


    ! Based on Jackson (1975) Eq. 9.112 and the Clausius Mossotti Eq. 4.70 relation
    !
    !           24 * PI*PI*PI       (n**2 -1)**2   (6.0 + 3* depol)
    ! RayXsec = -----------------   -----------    ------------------
    !           lambda**4 * N**2    (n**2 +2)**2   (6.0 - 7* depol)
    !
    ! where n is the index of refraction of air, N is the number density of molecules
    ! and depol is the depolarization factor. The term (6.0 + 3* depol) / (6.0 - 7* depol)
    ! is also called the King factor or depolarization term.
    !
    ! The index of refraction is taken from Peck and Reeder (1972):
    ! Peck, E. R., and K. Reeder, 1972: Dispersion of air. J. Opt. Soc. Amer., 62, 958�962.
    !
    !                                 2480990.0            17455.7
    ! n = 1 + 1.0E-8 * [ 8060.51 + ---------------- + ------------------ ]
    !                              132.274 - sig**2   39.32957 - sig**2 
    !
    ! with sig in the inverse wavelength in micrometer-1
    !
    ! When the wavelength (lamdba) is given in cm the Rayleigh cross section is given in cm2 per molecule.

    refrIndex = 8060.51d0 +  2480990.0d0 / (132.274d0 - sig**2) + 17455.7d0 / (39.32957d0 - sig**2)
    refrIndex = 1.0d0 + refrIndex * 1.0d-8

    Xsec = 24.0d0 * PI**3 / (1.0d-7*wavelength)**4 / numdens_ref**2
    Xsec = Xsec * (refrIndex**2 - 1.0d0) **2 / (refrIndex**2 + 2.0d0) **2

    ! correction for depolarization

    King_factor_air = fraction_N2  * KingFactor_N2 (wavelength) &
                    + fraction_O2  * KingFactor_O2 (wavelength) &
                    + fraction_Ar  * 1.00d0                     &
                    + fraction_CO2 * 1.15d0

    King_factor_air = King_factor_air / ( fraction_N2 + fraction_O2 + fraction_Ar + fraction_CO2)

    RayXsec = Xsec * King_factor_air

  END FUNCTION RayXsec


  FUNCTION depolarization_factor_air(wavelength)

    ! returns the depolarization factor for dry air
    ! wavelength in nm

    implicit none

    real(8), intent(in) :: wavelength         
    real(8)             :: depolarization_factor_air

    ! local
    real(8)             :: King_factor_air
    ! fractions are expressed as parts per volume by percent
    real(8), parameter  :: fraction_N2  = 78.084d0
    real(8), parameter  :: fraction_O2  = 20.946d0
    real(8), parameter  :: fraction_Ar  =  0.934d0
    real(8), parameter  :: fraction_CO2 =  0.036d0   ! for 360 ppmv CO2


    King_factor_air = fraction_N2  * KingFactor_N2 (wavelength) &
                    + fraction_O2  * KingFactor_O2 (wavelength) &
                    + fraction_Ar  * 1.00d0                     &
                    + fraction_CO2 * 1.15d0

    ! normalize by the sum of the fractions
    King_factor_air = King_factor_air / ( fraction_N2 + fraction_O2 + fraction_Ar + fraction_CO2)

    ! King factor K = (6+3d)/(6-7d) where d is the depolarization factor
    ! Hence d = 6 * (K-1) / (3+7K)

    depolarization_factor_air = 6.0d0 * (King_factor_air - 1.0d0) / (3.0d0 + 7.0d0 * King_factor_air )

  END FUNCTION depolarization_factor_air

  FUNCTION RayXsec_Chance_Spurr(wavelength)
  ! Gives the Rayleigh scattering cross section in m^2, wavelength in nm

    implicit none

    real(8), intent(in) :: wavelength         
    real(8)             :: RayXsec_Chance_Spurr

    ! local
    real(8)  :: sig

    sig = 1.d3/wavelength                  ! sig is wavenumber in micrometer^-1     

    RayXsec_Chance_Spurr= 3.9993d-20 * sig**4 / (1d0 - 1.069d-2 * sig**2 - 6.681d-5 * sig**4)

  END FUNCTION RayXsec_Chance_Spurr


  FUNCTION KingFactor_N2 (wavelength)

   implicit none

   real(8), intent(in) :: wavelength      ! in nm
   real(8)             :: KingFactor_N2

   ! local
   real(8) :: sig   ! inverse wavelength in micrometer-1

   sig = 1.d3/wavelength 

   KingFactor_N2 = 1.034d0 + 3.17d-4 * sig**2

  END FUNCTION KingFactor_N2


  FUNCTION KingFactor_O2 (wavelength)

   implicit none

   real(8), intent(in) :: wavelength      ! in nm
   real(8)             :: KingFactor_O2

   ! local
   real(8) :: sig   ! inverse wavelength in micrometer-1

   sig = 1.d3/wavelength 

   KingFactor_O2 = 1.096d0 + 1.385d-3 * sig**2 + 1.448d-4 * sig**4

  END FUNCTION KingFactor_O2


  FUNCTION CabannesXsec(wavelength, T)
  ! CabannesXsec is the scattering cross section for the Cabannes line in cm^2
  ! CabannesXsec is calculated by subtracting the Raman Xsec from the Rayleigh Xsec
  ! wavelength pertains to the wavelength of the incident light, in nm

  ! USES FUNCTION RayXsec and FUNCTION RamanXsecIncWavel

    implicit none

    real(8),  intent(in) :: wavelength, T
    real(8)              :: CabannesXsec

    ! RayXsec is in cm^2, RamanXsecIncWavel in m^2
    CabannesXsec = RayXsec(wavelength) &
                 - SUM(RamanXsecIncWavel(T, wavelength)) * 1.0d4
 
  END FUNCTION CabannesXsec


  FUNCTION CabannesAlbedo(errS, wavelength, T)
  ! Parameterization of the single scattering albedo, a, for Cabannes scattering
  ! 
  ! wavelength pertains to the wavelength of the incident light, in nm
  ! T is the temperature in Kelvin

  ! a = sum_k ( sum_m [ c(k,m) * T**m ) ] * (wavelength - 385.0)**k )

    implicit none

    type(errorType), intent(inout) :: errS
    real(8),  intent(in) :: wavelength, T
    real(8)              :: CabannesAlbedo

    ! local
    integer :: k, m
    real(8) :: c(0:5, 0:4)

    data c(0,0:4) /  0.96330156E+00,  0.87474561E-05, -0.41166504E-07,  0.92160982E-10, -0.66167126E-13 /
    data c(1,0:4) /  0.24198304E-04, -0.96852250E-08,  0.38455551E-10, -0.77820548E-13,  0.57813801E-16 /
    data c(2,0:4) / -0.10495012E-06,  0.44353220E-10, -0.17254553E-12,  0.34212841E-15, -0.25381174E-18 /
    data c(3,0:4) /  0.43297268E-09, -0.19574488E-12,  0.74732242E-15, -0.14618116E-17,  0.10886200E-20 /
    data c(4,0:4) / -0.28125279E-11,  0.14109315E-14, -0.52470681E-17,  0.10052633E-19, -0.75285270E-23 /
    data c(5,0:4) /  0.12664408E-13, -0.66182294E-17,  0.24372774E-19, -0.46322999E-22,  0.34767486E-25 /

    if ( ( wavelength < 265.0d0 ) .or. ( wavelength > 505.0d0 )  ) then
      call logDebug('wavelength outeside allowed range: 265 - 505 nm')
      call logDebugD('wavelength = ', wavelength)
      call logDebug('in function CabannesAlbedo')
      call logDebug('in module ramanspecsModule_v2')
      call logDebug('in function CabannesAlbedo')
      call mystop(errS, 'wavelength out of range')
      if (errorCheck(errS)) return
    end if

    !Initialize
    CabannesAlbedo = 0.0d0
    do k = 0, 5
      do m = 0, 4
        CabannesAlbedo = CabannesAlbedo + ( c(k,m) * T**m) * (wavelength - 385.0d0)**k
      end do ! m
    end do ! k
 
  END FUNCTION CabannesAlbedo


  FUNCTION NumberRamanLines()
  ! NumberRamanLines returns the nuber of Raman lines (O2 + N2)

    integer           :: NumberRamanLines

    NumberRamanLines = n_transN2 + n_transO2
 
  END FUNCTION NumberRamanLines


  FUNCTION RamanLinesIncWavel(wavelength)
  ! RamanLinesIncWavel returns an array of wavelengths for Raman scattering
  ! FUNCTION NumberRamanLines() may be used to get the size of the array
  ! RamanLinesIncWavel(k) for k = 1, n_transN2 contains the Raman wavelengths in nm for N2
  ! RamanLinesIncWavel(k) for k = n_transN2 + 1, n_transN2  + n_transO2 those for O2
  ! wavelength pertains to the wavelength of the incident light in nm

    implicit none

    integer    :: k
    REAL(8)    :: wavelength, RamanLinesIncWavel(n_transN2 + n_transO2)

    DO k = 1,n_transN2
      RamanLinesIncWavel(k) = 1.d7/(1.d7/wavelength - N2tran%DE(k))
    END DO
    DO k = 1, n_transO2
      RamanLinesIncWavel(k + n_transN2) = 1.d7/(1.d7/wavelength - O2tran%DE(k))
    END DO
  END FUNCTION RamanLinesIncWavel


  FUNCTION RamanLinesScatWavel(wavelengthPrime)
  ! Ramanlines returns an array of wavelengths for Raman scattering
  ! FUNCTION NumberRamanLines() may be used to get the size of the array
  ! RamanLines(k) for k = 1, n_transN2 contains the Raman wavelengths in nm for N2
  ! RamanLines(k) for k = n_transN2 + 1, n_transN2  + n_transO2 those for O2
  ! wavelengthPrime pertains to the wavelength of the scattered light in nm

    implicit none

    integer    :: k
    REAL(8)    :: wavelengthPrime, RamanLinesScatWavel(n_transN2 + n_transO2)

    DO k = 1,n_transN2
      RamanLinesScatWavel(k) = 1.d7/(1.d7/wavelengthPrime + N2tran%DE(k))
    END DO
    DO k = 1, n_transO2
      RamanLinesScatWavel(k + n_transN2) = 1.d7/(1.d7/wavelengthPrime + O2tran%DE(k))
    END DO
  END FUNCTION RamanLinesScatWavel


  FUNCTION RamanXsecIncWavel(temperature, wavelength)
  ! RamanXsecIncWavel returns an array of cross sections for Raman scattering
  ! FUNCTION NumberRamanLines() may be used to get the size of the array
  ! RamanXsec(k) = 1, n_trans, where n_trans = n_transN2 + n_transO2 (first N2 then O2)
  ! wavelength pertains to the wavelength of the incident light in nm
  ! wavelengthPrime pertains to the wavelength of the scattered light in nm

  ! USES SUBROUTINE fracPopulation

    implicit none

    integer    :: k
    REAL(8)    :: wavelengthPrime, factor, f, temperature, wavelength
    REAL(8)    :: RamanXsecIncWavel(n_transN2 + n_transO2)

    ! wavelength is in nm  instead of m,   gives scaling of 10^36
    ! cross section in m^2 instead of cm^2 gives scaling of 10^(-4)

    factor = 1.d32 * 256d0*PI**5/27d0

    CALL fracPopulation(temperature)           ! fills N2tran%f and O2tran%f

    DO k = 1,n_transN2
      wavelengthPrime = 1.d7/(1.d7/wavelength - N2tran%DE(k))
      f = 0.79d0 / (wavelengthPrime**4)
      RamanXsecIncWavel(k) =                                          &
                   f*factor*gammaN2(wavelength)**2 * N2tran%f(k)*N2tran%CPT(k)
    END DO
    DO k = 1, n_transO2
      wavelengthPrime = 1.d7/(1.d7/wavelength - O2tran%DE(k))
      f = 0.21d0 / (wavelengthPrime**4)
      RamanXsecIncWavel(k + n_transN2) =                              &
                   f*factor*gammaO2(wavelength)**2 * O2tran%f(k)*O2tran%CPT(k)
    END DO
  END FUNCTION RamanXsecIncWavel


  FUNCTION RamanXsecScatWavel(temperature, wavelengthPrime)
  ! RamanXsecScatWavel returns an array of cross sections for Raman scattering
  ! wavelengthPrime is now the wavelength of the scattered light in nm
  ! FUNCTION NumberRamanLines may be used to get the size of the array
  ! wavelength is the wavelength of the incident light
  ! USES SUBROUTINE fracPopulation

    implicit none

    integer    :: k
    REAL(8)    :: wavelengthPrime, factor, f, temperature, wavelength
    REAL(8)    :: RamanXsecScatWavel(n_transN2 + n_transO2)

    ! wavelength is in nm  instead of m,   gives scaling of 10^36
    ! cross section in m^2 instead of cm^2 gives scaling of 10^(-4)

    factor = 1.d32 * 256d0*PI**5/27d0

    CALL fracPopulation(temperature)           ! fills N2tran%f and O2tran%f

! N2tran%DE  and O2tran%DE contain the energy differences in cm-1 for the molecules
! Therefore wavelength = 1.d7/(1.d7/wavelengthPrime + N2tran%DE(k)) is the wavelength
! of the incident light

    DO k = 1,n_transN2
      wavelength = 1.d7/(1.d7/wavelengthPrime + N2tran%DE(k))
      f = 0.79d0 / (wavelengthPrime**4)
      RamanXsecScatWavel(k) =                                         &
                  f*factor*gammaN2(wavelength)**2 * N2tran%f(k)*N2tran%CPT(k)
    END DO
    DO k = 1, n_transO2
      wavelength = 1.d7/(1.d7/wavelengthPrime + O2tran%DE(k))
      f = 0.21d0 / (wavelengthPrime**4)
      RamanXsecScatWavel(k + n_transN2) =                             &
                  f*factor*gammaO2(wavelength)**2 * O2tran%f(k)*O2tran%CPT(k)
    END DO
  END FUNCTION RamanXsecScatWavel


  FUNCTION TotalRamanXsecScatWavel(temperature, wavelengthPrime)
  ! RamanXsecScatWavel returns the total Raman cross section for a specific
  ! scattering wavelength (wavelengthPrime) in nm and temperature (in Kelvin)
  ! The cross section is given in m2.
  ! USES SUBROUTINE fracPopulation

    implicit none

    REAL(8)    :: wavelengthPrime, temperature
    REAL(8)    :: XsecScatWavel(n_transN2 + n_transO2)
    REAL(8)    :: TotalRamanXsecScatWavel

    XsecScatWavel = RamanXsecScatWavel(temperature, wavelengthPrime)
    TotalRamanXsecScatWavel = sum(XsecScatWavel)
  END FUNCTION TotalRamanXsecScatWavel


  FUNCTION ZRayleigh( scat_angle, wavelength )
  ! ZRayleigh is the phase function for Rayleigh scattering (no wavelength shifts)
  ! scat_angle is the scattering angle in degrees
  ! wavelength in nm
  ! ZRay is normalized so that the integration over 4PI steradians yields 4PI

  ! USES FUNCTION RelAnisoPol

    implicit none

    real(8),  intent(in) :: scat_angle, wavelength

    real(8)  :: ZRayleigh

    ! local
    real(8)  :: CosSA, y

    CosSA = COS (scat_angle * PI/180d0)
    y     = RelAnisoPol(wavelength)
    ZRayleigh  = (3d0/20d0) * ((45d0 + 13d0*y) + (45d0 + y)*CosSA**2) / (9d0 + 2d0*y) 

  END FUNCTION ZRayleigh


  FUNCTION ZCabannes( scat_angle, wavelength )
  ! ZCabannes is the phase function for Cabannes scattering
  ! scat_angle is the scattering angle in degrees
  ! wavelength in nm
  ! ZCabannes is normalized so that the integration over 4PI steradians yields 4PI

  ! USES FUNCTION RelAnisoPol

    implicit none

    real(8), intent(in) :: scat_angle, wavelength
    real(8)  :: ZCabannes

    ! local
    REAL(8)  :: CosSA, y

    CosSA     = COS (Scat_angle * PI/180d0)
    y         = RelAnisoPol( wavelength )
    ZCabannes = (3d0/40d0) *  ((180d0 + 13d0*y) + (180d0 + y) * CosSA**2) / (18d0 + y)   

  END FUNCTION ZCabannes


  FUNCTION ZRaman(Scat_angle)
  ! ZRaman is the phase function for the rotational Raman lines
  ! Scat_angle is the scattering angle in degrees
  ! Note that ZRaman is no function of the relative ansisotropy in the polarizability,
  ! in contrast to ZCabannes and, therefore, does not depend on the wavelength.
  ! ZR is normalized so that the integration over 4PI steradians yields 4PI

    implicit none

    REAL(8) :: ZRaman, Scat_angle

    ZRaman    = (3d0/40d0) *  (13d0 + COS (Scat_angle *PI/180d0)**2)  

  END FUNCTION ZRaman


  SUBROUTINE fracPopulation(temperature)
  ! calculates the fractional population for all transitions (not states)
  ! is used by function RamanXsec

    implicit none

    REAL(8), PARAMETER :: c2 = 1.438769               ! c2 = hc/k with c in cm/sec
    REAL(8)            :: temperature, sumN2, sumO2

    sumN2 = SUM((2*N2state%J + 1)* N2state%g * exp(-N2state%E * c2/temperature))
    sumO2 = SUM((2*O2state%J + 1)* O2state%g * exp(-O2state%E * c2/temperature))

    O2tran%g = 1 ! According to Chance this is correct, because N, not J is the
                 ! relevant quantity

    N2tran%f = (2*N2tran%J + 1)* N2tran%g * exp(-N2tran%E * c2/temperature)/sumN2
    O2tran%f = (2*O2tran%J + 1)* O2tran%g * exp(-O2tran%E * c2/temperature)/sumO2

  END SUBROUTINE fracPopulation


  FUNCTION RefrIndexSTP(wavelength)
  ! Gives the refractive index of dry air at STP(273.15K and 1 atm), wavelength in nm
  ! Should not be used for wavelengths outside 200 - 1000 nm

    implicit none

    REAL(8)  :: RefrIndexSTP, wavelength, sig, y

    sig = 1.d3/wavelength     
    y= 0.7041d0 + 315.9d0/(157.39d0 - sig**2) +  8.4127d0/(50.429d0 - sig**2)
    RefrIndexSTP = 1.d0 + y * 1.d-4                  

  END FUNCTION RefrIndexSTP


  FUNCTION RelAnisoPol(wavelength)
  ! RelAnisoPol is the square of the relative anisotropy in the polarizability:
  ! RelAnisoPol = (gamma/<alpha>)**2, with gamma the anisotropy of the polarizability
  ! and <alpha> the average polarizability
  ! wavelength in nm
  ! Should not be used for wavelengths outside 200 - 1000 nm

  ! USES FUNCTION RefrIndexSTP and FUNCTION RayXsec

    implicit none

    real(8),    intent(in) :: wavelength

    REAL(8)  :: RelAnisoPol

    ! local
    real(8)  :: y, nSTP

    nSTP = 2.686763d25                            !Loschmidt's number [m^-3]

    y= 3.0d0 * nSTP **2 * (wavelength * 1d-9)**4 * RayXsec_Chance_Spurr(wavelength) / &
       (32.d0 * PI**3 * (RefrIndexSTP(wavelength) **2 - 1.0d0) **2)
    RelAnisoPol = 4.5d0 * (y - 1.0d0)               

  END FUNCTION RelAnisoPol


  FUNCTION gammaO2(wavelength)
  ! gamma is the anisotropy of the polarizability of O2 in m^2, wavelength in nm
  ! Should not be used for wavelengths outside 200 - 1000 nm
  ! This is an empirical formulae (no deeper physical meaning)

    implicit none

    REAL(8)  :: gammaO2, wavelength, sig

    sig = 1.d3/wavelength     
    gammaO2= 0.07149d-28 + 45.9364d-28/( 48.2716d0 - sig**2)     

  END FUNCTION gammaO2


  FUNCTION gammaN2(wavelength)
  ! gamma the anisotropy of the polarizability of N2 in m^2, wavelength in nm
  ! Should not be used for wavelengths outside 200 - 1000 nm
  ! This is an empirical formulae (no deeper physical meaning)

    implicit none

    REAL(8)  :: gammaN2, wavelength, sig

    sig = 1.d3/wavelength     
    gammaN2= -6.01466d-29 + 2385.57d-29/( 186.099d0 - sig**2)   

  END FUNCTION gammaN2


  subroutine ConvoluteSpecRaman(errS, wavelengthGrid, temperature, spec, specConv)

    ! Subroutine ConvoluteSpecRaman convolutes a spectrum with rotational Raman lines
    ! the Raman lines used pertian to air (79% N2 and 21% O2) of a certain temperature.
    ! as the temperature sensitivity is small, one usually takes 250 K as the average temperature
    ! of the atmosphere.
    ! Input:  wavelengthGrid   = array with wavelength in nm between 200 and 1000 nm
    !         spec             = spectrum corresponding to wavelengthGrid
    ! Output: SpecConv         = spectrum after convolution with the Raman lines

    ! Procedure:
    ! 1)   The input spectrum is interpolated to a high-resolution equidistant spectral grid with a step size of 0.01 nm
    !      to ensure that linear interpolation is sufficiently accrate when the convolution is performed
    ! 2)   routines from the module RamanModule are used to obtain the number of Raman lines, the
    !      values of the wavelengths for the lines if the scattered light emerges at a specified wavelength, and
    !      the Raman scattering cross sections at these wavelengths
    ! 3)   The values of the spectrum Spec at the Raman wavelengths are obtained from linear interpolation
    !      on the high-resolution spectrum calculated in step 1).
    ! 4)   The inner product of the Raman cross sections and the values of the spectrum at the Raman lines is calculated
    !      and the result is devided by the sum of the Raman cross sections, i.e. convolution is performed.
    ! 
    !      NOTE: The convoluted values of the first and last wavelength will be less accurate because extrapolation
    !      is needed to perform the convolution. All values outside the interval (wavelengthGrid(1), wavelengthGrid(last))
    !      are set to the value at the end point of the interval. This is assumed to be more accurate than using another
    !      form of estrapolation.
    !      WARNING: Only values that are 1.5 - 2.0 nm from the endpoints of the interval specified by  wavelengthGrid
    !      will be accurate.

    implicit none

     ! input and output
    type(errorType), intent(inout) :: errS
    real(8)   , intent(in)   :: wavelengthGrid(:)    ! wavelength grid for spectrum
    real(8)   , intent(in)   :: temperature          ! temperature of the scattering (Kelvin)
    real(8)   , intent(in)   :: spec(:)              ! (nwave) spectrum that will be convoluted
    real(8)   , intent(out)  :: specConv(:)          ! (nwave) spectra convoluted with Raman lines

    ! allocatable array for second order derivatives needed for spline interpolation
    real(8),    allocatable  :: specDer2(:)          ! second order derivatives

    ! allocatable arrays for the high-resolution spectrum and step size
    real(8),    allocatable  :: waveHR(:)            ! (nwavelHR) high-resolution wavelengths
    real(8),    allocatable  :: specHR(:)            ! (nwavelHR) high-resolution version of spec
    real(8),    parameter    :: wstep = 0.01d0       ! wavelength step HR spectrum

    ! allocatable arrays for the Raman lines
    real(8),    allocatable  :: Dlambda(:)           ! wavelength shifts
    real(8),    allocatable  :: XsecRaman(:)         ! Raman scattering cross-sections
    real(8),    allocatable  :: specIntRaman(:)      ! interpolated at Raman lines

    real(8),    allocatable  :: x(:), weight(:)      ! used for linear interpolation
    integer   , allocatable  :: m(:), j(:)           ! used for linear interpolation

    real(8)                  :: w, wclose
    real(8)                  :: beginInterval, endInterval
    integer                  :: iwave

    integer                  :: nw                   ! number of wavelengths in spec
    integer                  :: nHR                  ! number of wavelengths HR spectrum
    integer                  :: nRamanLines          ! number of Raman lines
    integer                  :: indMin(1)            ! index minimum and maximum value

    integer                  :: status_spline
    integer                  :: status_splint
    integer                  :: allocationError, sumAllocationError

    ! if verbose = .true. print result intermediate output file
    logical, parameter           :: verbose = .false.

    ! create space for arrays associated with Raman lines

    nRamanLines = NumberRamanLines()                  !  from RamanModule

    allocationError    = 0
    sumAllocationError = 0

    allocate( Dlambda(nRamanLines), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    allocate( specIntRaman(nRamanLines), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    allocate( XsecRaman(nRamanLines), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    allocate( x(nRamanLines), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    allocate( weight(nRamanLines), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    allocate( m(nRamanLines), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    allocate( j(nRamanLines), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    if (sumAllocationError /= 0) then
      call logDebug('ERROR: allocation failed in subroutime ConvoluteSpecRaman')
      call logDebug('for allocate (Dlambda(nRamanLines), specIntRaman(nRamanLines),...')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if

    ! determine wavelength interval for high-resolution spectrum

    nw = size(wavelengthGrid)
    beginInterval = wavelengthGrid(1)
    endInterval   = wavelengthGrid(nw)

    ! extend wavelength interval to account for Raman shifts

    Dlambda  = RamanLinesScatWavel(beginInterval) - beginInterval ! wavelength shifts
    beginInterval = beginInterval + minval(Dlambda)  ! extend interval at the beginning
    Dlambda  = RamanLinesScatWavel(endInterval) - endInterval ! wavelength shifts
    endInterval = endInterval + maxval(Dlambda) ! extend interval at the end

    ! create space for HR spectrum
    ! and for second order derivative that is needed for spline interpolation

    nHR = 4 + nint( (endInterval - beginInterval) / wstep )

    allocationError    = 0
    sumAllocationError = 0

    allocate( waveHR(nHR), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    allocate( specHR(nHR), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    allocate( specDer2(nw), STAT = allocationError )
    sumAllocationError = sumAllocationError + allocationError

    if (sumAllocationError /= 0) then
      call logDebug('ERROR: allocation failed in subroutime ConvoluteSpecRaman')
      call logDebug('for allocation of waveHR, specHR, or specDer2')
      call mystop(errS, 'stopped because allocation failed')
      if (errorCheck(errS)) return
    end if

    ! fill HR wavelengths
    !     start at beginInterval - wstep  and stop at endInterval + wstep
    !     to account for round-off errors when an index is calculated
    waveHR(:) = 0.0d0  ! initalization
    do iwave = 1, nHR
      waveHR(iwave) = beginInterval + (iwave - 2) * wstep
    end do

    ! prepare for spline interpolation
    call spline(errS, wavelengthGrid, spec, specDer2, status_spline)
    if (errorCheck(errS)) return

    if (status_spline /= 0) then
      call logDebug('ERROR: spline preparation failed in subroutime ConvoluteSpecRaman')
      call mystop(errS, 'stopped because spline preparation failed')
      if (errorCheck(errS)) return
    end if

    ! perform spline interpolation

    do iwave = 1, nHR
      specHR(iwave) = splint(errS, wavelengthGrid, spec, specDer2, waveHR(iwave), status_splint)
      ! prevent extrapolation using spline
      if ( waveHR(iwave) < wavelengthGrid(1)  ) specHR(iwave) = spec( 1)
      if ( waveHR(iwave) > wavelengthGrid(nw) ) specHR(iwave) = spec(nw)

      if (status_splint /= 0) then
      call logDebug('ERROR: splint failed in subroutime ConvoluteSpecRaman')
      call mystop(errS, 'stopped because splint failed')
      if (errorCheck(errS)) return
      end if

    end do ! wavelength loop

    ! convolute with Raman lines

    do iwave = 1, nw

       w = wavelengthGrid(iwave)

       !  Put wavelengths of Raman lines in Dlambda (scattered light has wavelength w)
       Dlambda  = RamanLinesScatWavel(w)

       ! We work with ph s-1 cm-2 nm -1 as unit for the irradiance, but the
       ! scattering cross sections given by RamanXsecScatWavel pertain to energy
       ! in  W m-2 nm-1. A photon with a smaller wavelength has more energy.
       ! Therefore the blue-shifted lines contribute more
       ! to the scattering, namely a fraction w/Dlamba.

       XsecRaman = RamanXsecScatWavel(temperature, w) * w / Dlambda     ! from RamanModule

       Dlambda  = Dlambda - w

       !  Use linear interpolation for the Raman shifted values
       !  note that x, m, j, and weight are arrays

       ! find wavelength on HR grid closest to w
       indMin = minloc(abs(waveHR - w))
       wclose = waveHR(indMin(1))     

       x = (Dlambda + w - wclose)/ wstep  ! shift in units wstep w.r.t. wclose
       m = floor (x)                      ! floor returns the greatest integer less than
                                          ! or equal to its argument
       j = indMin(1) + m                  ! j is an index array such that waveHR(j) 
                                          ! and waveHR(j+1) bracket the shifted wavelenghts
       weight = 1.0d0 - x + m             ! interpolation weights

! JdH Debug
       !if ( abs(w - 760.8d0) < 0.05) then
       !  write(19,*) 'w = ', w, 'wclose = ', wclose
       !  write(19,*) 'j = ', j
       !  write(19,*) 'm = ',  m
       !  write(19,'(A,500F15.4)') 'Dlambda = ',  Dlambda
       !  write(19,'(A,500E15.4)') 'xSec    = ',  XsecRaman * 1.0d4 / RayXsec(w)
       !  write(19,'(A,500F15.4)') 'x*wstep = ',  x*wstep + wclose
       !  write(19,'(A,500F15.4)') 'waveHR  = ',  (waveHR(j(i))  , i = 1, nRamanLines)
       !  write(19,'(A,500F15.4)') 'waveHR  = ',  (waveHR(j(i)+1), i = 1, nRamanLines)
       !  stop
       !end if
       !
       ! array specIntRaman receives interpolated values at Raman wavelengths
       specIntRaman  = weight * specHR(j) + (1.d0 - weight) * specHR(j+1)

       ! perform convolution
       ! the convoluted spectrum is proportional to the Raman cross section which is given
       ! in m2 (by RamanXsecScatWavel) but we want it in cm2 => multply it with 1.0d4
       specConv(iwave) = sum(XsecRaman*specIntRaman)

       ! divide the spectrum by the cross section for Raman scattering
       specConv(iwave) = specConv(iwave) / sum(XsecRaman)

    end do ! iwave

    ! clean up
    sumAllocationError = 0
    allocationError    = 0
    deallocate(Dlambda, specIntRaman, XsecRaman, x, weight, m, j, stat = allocationError)
    sumAllocationError = sumAllocationError + allocationError
    deallocate(waveHR, specHR, specDer2, stat = allocationError)
    sumAllocationError = sumAllocationError + allocationError

    if (sumAllocationError /= 0) then
      call logDebug('ERROR: deallocation failed in subroutime ConvoluteSpecRaman')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

    if (verbose) then
      write(intermediateFileUnit,*) ''
      write(intermediateFileUnit,*) 'convolution with the rotational Raman lines '
      write(intermediateFileUnit,*) ''
      write(intermediateFileUnit,*) 'spectrum before convolution'
      write(intermediateFileUnit,*) 'wavelength(nm)      spec'
      do iwave = 1, nw
        write(intermediateFileUnit,'(F10.4, 40E15.6)') wavelengthGrid(iwave), spec(iwave)
      end do
      write(intermediateFileUnit,*) ''
      write(intermediateFileUnit,*) 'spectra after convolution'
      write(intermediateFileUnit,*) 'wavelength(nm)      spec1       spec2      etc. '
      do iwave = 1, nw
        write(intermediateFileUnit,'(F10.4, ES15.6)') wavelengthGrid(iwave), specConv(iwave)
      end do
    end if

  end subroutine ConvoluteSpecRaman


  subroutine ConvoluteSpecRamanMS(errS, fraction, wavelengthGrid, nspec, temperature, spec, specConv, status)

    ! convolution for more than one spectrum

    ! Subroutine ConvoluteSpecRaman convolutes a spectrum with rotational Raman lines
    ! the Raman lines used pertian to air (79% N2 and 21% O2) of a certain temperature.
    ! as the temperature sensitivity is small, one usually takes 250 K as the average temperature
    ! of the atmosphere.
    ! Input:  wavelengthGrid   = array with wavelength in nm between 200 and 1000 nm
    !         nSpec            = number of spectra to be convolutes
    !         spec             = 2D array of spectrum values corresponding to wavelengthGrid
    !                            first index gives index the wavelength, second index the spectrumNumber
    ! Output: SpecConv         = 2D array of spectra after convolution with the Raman lines

    ! Procedure:
    ! 1)   The input spectrum is interpolated to a high-resolution equidistant spectral grid with a step size of 0.01 nm
    !      to ensure that linear interpolation is sufficiently accrate when the convolution is performed
    ! 2)   routines from the module RamanModule are used to obtain the number of Raman lines, the
    !      values of the wavelengths for the lines if the scattered light emerges at a specified wavelength, and
    !      the Raman scattering cross sections at these wavelengths
    ! 3)   The values of the spectrum Spec at the Raman wavelengths are obtained from linear interpolation
    !      on the high-resolution spectrum calculated in step 1).
    ! 4)   The inner product of the Raman cross sections and the values of the spectrum at the Raman lines is calculated
    !      and the result is devided by the sum of the Raman cross sections, i.e. convolution is performed.
    ! 5)   Steps 2) - 4) are repeated for each wavelength in the array wavelengthGrid
    ! 
    !      NOTE: The convoluted values of the first and last wavelength will be less accurate because extrapolation
    !      is needed to perform the convolution. All values outside the interval (wavelengthGrid(1), wavelengthGrid(last))
    !      are set to the value at the end point of the interval. This is assumed to be more accurate than using another
    !      form of estrapolation.
    !      WARNING: Only values that are 1.5 - 2.0 nm from the endpoints of the interval specified by  wavelengthGrid
    !      will be accurate.

    implicit none

    ! input and output
    type(errorType), intent(inout) :: errS
    real(8), intent(in)   :: fraction             ! use only the strongest Raman lines i.e. if fration = 0.10 use only
                                                  ! the 10% strongest lines
    real(8), intent(in)   :: wavelengthGrid(:)    ! wavelength grid for spectrum
    integer, intent(in)   :: nspec                ! first numSpecConv of the spectra in spec will be convoluted
    real(8), intent(in)   :: temperature          ! temperature of the scattering (Kelvin)
    real(8), intent(in)   :: spec(:,:)            ! (nwave, nspec ) spectra that will be convoluted
    real(8), intent(out)  :: specConv(:,:)        ! (nwave, nspec ) spectra convoluted with Raman lines
    integer, intent(out)  :: status               ! status
    
    ! allocatable array for second order derivatives needed for spline interpolation
    real(8),    allocatable  :: specDer2(:)          ! second order derivatives

    ! allocatable arrays for the high-resolution spectrum and step size
    real(8),    allocatable  :: waveHR(:)            ! (nwavelHR) high-resolution wavelengths
    real(8),    allocatable  :: specHR(:,:)          ! (nwavelHR, nspec) high-resolution version of spec
! JdH Debug reduce wstep; wstep = 0.05 is inaccurate
!   real(8),    parameter    :: wstep = 0.05d0       ! wavelength step HR spectrum
    real(8),    parameter    :: wstep = 0.01d0       ! wavelength step HR spectrum

    ! allocatable arrays for the Raman lines
    real(8),    allocatable  :: Dlambda(:)            ! wavelength shifts
    real(8),    allocatable  :: XsecRaman(:)          ! Raman scattering cross-sections
    real(8),    allocatable  :: Dlambda_filtered(:)   ! wavelength shifts after filtering
    real(8),    allocatable  :: XsecRaman_filtered(:) ! Raman scattering cross-sections
    real(8),    allocatable  :: specIntRaman(:)       ! interpolated at Raman lines

    real(8),    allocatable  :: x(:), weight(:)     ! used for linear interpolation
    integer,    allocatable  :: m(:), j(:)          ! used for linear interpolation
    integer,    allocatable  :: indexArr(:)         ! used for sorting

    real(8)                  :: w, wclose
    real(8)                  :: beginInterval, endInterval
    integer                  :: iwave, iSpec, i, ii

    integer                  :: nw                   ! number of wavelengths in spec
    integer                  :: nHR                  ! number of wavelengths HR spectrum
    integer                  :: nRamanLines          ! number of Raman lines
    integer                  :: nRamanLines_filtered ! number of Raman lines after filtering
    integer                  :: indMin(1)            ! index minimum and maximum value

    integer                  :: status_spline
    integer                  :: status_splint
    integer                  :: allocationError

    ! if verbose = .true. print result to fort.19
    logical, parameter           :: verbose = .false.

    ! assume correct processing
    status = 0

    ! determine wavelength interval for high-resolution spectrum
    nw = size(wavelengthGrid)
    beginInterval = wavelengthGrid(1)
    endInterval   = wavelengthGrid(nw)

    ! create space for arrays associated with Raman lines
    nRamanLines = NumberRamanLines()
    allocate (Dlambda(nRamanLines), XsecRaman(nRamanLines), indexArr(nRamanLines), &
              STAT = allocationError)
    if (allocationError /= 0) then
      call logDebug('allocation failed in subroutine ConvoluteSpecRamanMS')
      call mystop(errS, 'allocation failed in subroutine ConvoluteSpecRamanMS')
      if (errorCheck(errS)) return
    end if

    ! calculate Raman scattering cross sections for the mid-wavelength
    w = 0.5d0 * (endInterval - beginInterval)
    Dlambda   = RamanLinesScatWavel(w)
    XsecRaman = RamanXsecScatWavel(temperature, w) * w / Dlambda
    ! sort the Raman cross sections
    ! XsecRaman(indexArr(i)) decreases with i
    call indexx(errS, XsecRaman, indexArr)
    if (errorCheck(errS)) return


    nRamanLines_filtered = fraction * NumberRamanLines()
    allocate (Dlambda_filtered(nRamanLines_filtered), specIntRaman(nRamanLines_filtered),     &
              XsecRaman_filtered(nRamanLines_filtered), x(nRamanLines_filtered),              &
              weight(nRamanLines_filtered), m(nRamanLines_filtered), j(nRamanLines_filtered), &
              stat = allocationError)

    if (allocationError /= 0) then
      call logDebug('allocation failed in subroutine ConvoluteSpecRamanMS')
      call mystop(errS, 'allocation failed in subroutine ConvoluteSpecRamanMS')
      if (errorCheck(errS)) return
    end if

    ! extend wavelength interval to account for Raman shifts

    Dlambda  = RamanLinesScatWavel(beginInterval) - beginInterval ! wavelength shifts
    beginInterval = beginInterval + minval(Dlambda)  ! extend interval at the beginning
    Dlambda  = RamanLinesScatWavel(endInterval) - endInterval ! wavelength shifts
    endInterval = endInterval + maxval(Dlambda) ! extend interval at the end

    ! create space for HR spectrum
    ! and for second order derivative that is needed for spline interpolation

    nHR = 3 + (endInterval - beginInterval) / wstep

    allocate(waveHR(nHR), specHR(nHR,nSpec), specDer2(nw), stat = allocationError)

    if (allocationError /= 0) then
      call logDebug('allocation failed in subroutine ConvoluteSpecRamanMS')
      call mystop(errS, 'allocation failed in subroutine ConvoluteSpecRamanMS')
      if (errorCheck(errS)) return
    end if

    ! fill HR wavelengths
    !     start at beginInterval - wstep  and stop at endInterval + wstep
    !     to account for round-off errors when an index is calculated
    waveHR(:) = 0.0d0  ! initalization
    do iwave = 1, nHR
      waveHR(iwave) = beginInterval +(iwave-2) * wstep
    end do

    do iSpec = 1, nSpec
      ! prepare for spline interpolation
      call spline(errS, wavelengthGrid, spec(:,iSpec), specDer2, status_spline)
      if (errorCheck(errS)) return

      if (status_spline /= 0) then
        call logDebug('spline failed in subroutine ConvoluteSpecRamanMS')
        call mystop(errS, 'spline failed in subroutine ConvoluteSpecRamanMS')
        if (errorCheck(errS)) return
      end if

      ! perform spline interpolation

      do iwave = 1, nHR
        specHR(iwave,iSpec) = splint(errS, wavelengthGrid, spec(:,iSpec), specDer2, waveHR(iwave), status_splint)
        ! prevent extrapolation
        if ( waveHR(iwave) < wavelengthGrid(1)  ) specHR(iwave,iSpec) = spec( 1,iSpec)
        if ( waveHR(iwave) > wavelengthGrid(nw) ) specHR(iwave,iSpec) = spec(nw,iSpec)

        if (status_splint /= 0) then
          call logDebug('splint failed in subroutine ConvoluteSpecRamanMS')
          call mystop(errS, 'splint failed in subroutine ConvoluteSpecRamanMS')
          if (errorCheck(errS)) return
        end if

      end do ! wavelength loop
    end do ! spectrum loop

    ! convolute with Raman lines

    do iwave = 1, nw

       w = wavelengthGrid(iwave)

       !  Put wavelengths of Raman lines in Dlambda (scattered light has wavelength w)
       Dlambda  = RamanLinesScatWavel(w)

       ! We work with ph s-1 cm-2 nm -1 as unit for the irradiance, but the
       ! scattering cross sections given by RamanXsecScatWavel pertain to energy
       ! in  W m-2 nm-1. A photon with a smaller wavelength has more energy.
       ! Therefore the blue-shifted lines contribute more
       ! to the scattering, namely a fraction w/Dlamba.

       XsecRaman = RamanXsecScatWavel(temperature, w) * w / Dlambda

       ! select strongest lines
       do i = 1, nRamanLines_filtered
         ii = nRamanLines + 1 - i
         Dlambda_filtered(i)   = Dlambda(indexArr(ii))
         XsecRaman_filtered(i) = XsecRaman(indexArr(ii))
       end do

       Dlambda_filtered  = Dlambda_filtered - w

       !  Use linear interpolation for the Raman shifted values
       !  note that x, m, j, and weight are arrays

       ! find wavelength on HR grid closest to w
       indMin = minloc(abs(waveHR -w))
       wclose = waveHR(indMin(1))     

       x = (Dlambda_filtered + w - wclose)/ wstep  ! shift in units wstep w.r.t. wclose
       m = floor (x)                               ! floor returns the greatest integer less than
                                                   ! or equal to its argument
       j = indMin(1) + m                           ! j is an index array such that specHR(j) 
                                                   ! and specHR(j+1) bracket the shifted wavelenghts
       weight = 1.0d0 - x + m                      ! interpolation weights

       do iSpec = 1, nSpec
         ! array specIntRaman receives interpolated values at Raman wavelengths
         specIntRaman  = weight * specHR(j, iSpec) + (1.d0 - weight) * specHR(j+1, iSpec)

         ! perform convolution
         ! the convoluted spectrum is proportional to the Raman cross section which is given
         ! in m2 (by RamanXsecScatWavel) but we want it in cm2 => multply it with 1.0d4
         specConv(iwave,iSpec)    = sum(XsecRaman_filtered*specIntRaman) * 1.0d4
       end do ! spectrum loop

    end do

    ! clean up

    deallocate(waveHR, specHR, specDer2,                  &
               Dlambda, specIntRaman, XsecRaman,          &
               x, weight, m, j, Dlambda_filtered,         &
               XsecRaman_filtered, indexArr, stat = allocationError)

    if (allocationError /= 0) then
      call logDebug('deallocation failed in subroutine ConvoluteSpecRamanMS')
      call mystop(errS, 'deallocation failed in subroutine ConvoluteSpecRamanMS')
      if (errorCheck(errS)) return
    end if

    if (verbose) then
      write(intermediateFileUnit,*) ''
      write(intermediateFileUnit,*) 'convolution with the rotational Raman lines '
      write(intermediateFileUnit,*) ''
      write(intermediateFileUnit,*) 'spectra before convolution'
      write(intermediateFileUnit,*) 'wavelength(nm)      spec1       spec2      etc. '
      do iwave = 1, nw
        write(intermediateFileUnit,'(F10.4, 40E15.6)') wavelengthGrid(iwave), (spec(iwave, iSpec), iSpec = 1, nSpec)
      end do
      write(intermediateFileUnit,*) ''
      write(intermediateFileUnit,*) 'spectra after convolution'
      write(intermediateFileUnit,*) 'wavelength(nm)      spec1       spec2      etc. '
      do iwave = 1, nw
        write(intermediateFileUnit,'(F10.4, 40E15.6)') wavelengthGrid(iwave), (specConv(iwave, iSpec), iSpec = 1, nSpec)
      end do
    end if

  end subroutine ConvoluteSpecRamanMS


END MODULE ramansspecs


