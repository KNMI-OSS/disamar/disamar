!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

  module writeModule

    ! This module contains subroutines to write
    ! - overall results of the retrieval: print_results
    ! - specific output for the retrieval of a trace gas profile (e.g. averaging kernel is useful only if a profile is retrieved)
    ! - cloud parameters to file so that they can be read in for a second retrieval

  use dataStructures
  use mathTools,        only: splintLin, spline, splint, gaussDivPoints, getSmoothAndDiffXsec, polyInt
  use asciiHDFtools

  ! default is private
  private
  public  :: print_results, print_AsciiHDF, writeAsCIIinputOMO3PR, writeContribRad_Refl, writeAltResolvedAMF
  public  :: writeTestDataHR, writeTestDataInstr, write_disamar_sim

  contains

    subroutine print_results(errS, numSpectrBands, nTrace, controlSimS, controlRetrS,                   &
                             wavelHRSimS, wavelHRRetrS, wavelInstrIrrSimS, wavelInstrRadSimS,           &
                             wavelInstrIrrRetrS, wavelInstrRadRetrS, geometrySimS, geometryRetrS,       &
                             reflDerivHRSimS, reflDerivHRRetrS, gasPTSimS, gasPTRetrS,                  &
                             solarIrradianceSimS, solarIrradianceRetrS, earthRadianceSimS,              &
                             earthRadianceRetrS, traceGasSimS, traceGasRetrS, surfaceSimS,              &
                             surfaceRetrS, LambCloudSimS, LambCloudRetrS, cldAerFractionSimS,           &
                             cldAerFractionRetrS, cloudAerosolRTMgridSimS, cloudAerosolRTMgridRetrS,    &
                             mulOffsetSimS, mulOffsetRetrS, strayLightSimS, strayLightRetrS,            &
                             RRS_RingSimS, RRS_RingRetrS, retrS, diagnosticS, optPropRTMGridSimS,       &
                             optPropRTMGridRetrS, polCorrectionRetrS, ncolumn, columnSimS, columnRetrS)

      ! print results to output file

      implicit none

      type(errorType)              ,intent(inout) :: errS
      integer                      ,intent(in)    :: numSpectrBands
      integer                      ,intent(in)    :: nTrace
      type(controlType)            ,intent(in)    :: controlSimS
      type(controlType)            ,intent(in)    :: controlRetrS
      type(wavelHRType)            ,intent(in)    :: wavelHRSimS(numSpectrBands)
      type(wavelHRType)            ,intent(in)    :: wavelHRRetrS(numSpectrBands)
      type(wavelInstrType)         ,intent(in)    :: wavelInstrIrrSimS(numSpectrBands)
      type(wavelInstrType)         ,intent(in)    :: wavelInstrRadSimS(numSpectrBands)
      type(wavelInstrType)         ,intent(in)    :: wavelInstrIrrRetrS(numSpectrBands)
      type(wavelInstrType)         ,intent(in)    :: wavelInstrRadRetrS(numSpectrBands)
      type(geometryType)           ,intent(in)    :: geometrySimS                       ! geo information for simulation
      type(geometryType)           ,intent(in)    :: geometryRetrS                      ! geo information for retrieval
      type(reflDerivType)          ,intent(in)    :: reflDerivHRSimS(numSpectrBands)
      type(reflDerivType)          ,intent(in)    :: reflDerivHRRetrS(numSpectrBands)
      type(gasPTType)              ,intent(in)    :: gasPTSimS, gasPTRetrS              ! pressure and temperature
      type(solarIrrType)           ,intent(in)    :: solarIrradianceSimS(numSpectrBands)
      type(solarIrrType)           ,intent(in)    :: solarIrradianceRetrS(numSpectrBands)
      type(earthRadianceType)      ,intent(in)    :: earthRadianceSimS(numSpectrBands)
      type(earthRadianceType)      ,intent(in)    :: earthRadianceRetrS(numSpectrBands)
      type(traceGasType)           ,intent(in)    :: traceGasSimS(nTrace)                ! trace gas properties
      type(traceGasType)           ,intent(in)    :: traceGasRetrS(nTrace)               ! trace gas properties
      type(LambertianType)         ,intent(in)    :: surfaceSimS(numSpectrBands)         ! surface properties for simulation
      type(LambertianType)         ,intent(in)    :: surfaceRetrS(numSpectrBands)        ! surface properties for retrieval
      type(LambertianType)         ,intent(in)    :: LambCloudSimS(numSpectrBands)       ! Lambertian cloud properties
      type(LambertianType)         ,intent(in)    :: LambCloudRetrS(numSpectrBands)      ! Lambertian cloud properties
      type(cldAerFractionType)     ,intent(in)    :: cldAerFractionSimS(numSpectrBands)  ! cloud/aerosol fraction properties
      type(cldAerFractionType)     ,intent(in)    :: cldAerFractionRetrS(numSpectrBands) ! cloud/aerosol fraction properties
      type(cloudAerosolRTMgridType),intent(in)    :: cloudAerosolRTMgridSimS             ! cld/aer properties sim
      type(cloudAerosolRTMgridType),intent(in)    :: cloudAerosolRTMgridRetrS            ! cld/aer properties retr
      type(mulOffsetType)          ,intent(in)    :: mulOffsetSimS(numSpectrBands)       ! multiplicative offset radiance sim
      type(mulOffsetType)          ,intent(in)    :: mulOffsetRetrS(numSpectrBands)      ! multiplicative offset radiance retr
      type(straylightType)         ,intent(in)    :: strayLightSimS(numSpectrBands)      ! earth radiance stray light simulation
      type(straylightType)         ,intent(in)    :: strayLightRetrS(numSpectrBands)     ! earth radiance stray light retrieval
      type(RRS_RingType)           ,intent(in)    :: RRS_RingSimS(numSpectrBands)        ! RRS
      type(RRS_RingType)           ,intent(in)    :: RRS_RingRetrS(numSpectrBands)       ! RRS
      type(retrType)               ,intent(inout) :: retrS                               ! retrieval structure
      type(diagnosticType)         ,intent(in)    :: diagnosticS                         ! diagnostic information
      integer                      ,intent(in)    :: ncolumn                             ! # columns with subcolumns
      type(optPropRTMGridType)     ,intent(in)    :: optPropRTMGridSimS, optPropRTMGridRetrS
      type(polCorrectionType)      ,intent(inout) :: polCorrectionRetrS(numSpectrBands)
      type(columnType)             ,intent(in)    :: columnSimS(ncolumn), columnRetrS(ncolumn)

      ! local
      integer    :: iwave, istate, jstate, iband, index, iTrace, jTrace, ialt, jalt
      integer    :: startvalue
      character(LEN=80) :: string

      logical    :: first_time_profile, first_time_temp, corr_flag

      real(8)    :: factor, sumAKx

      real(8), parameter :: DUToCm2 = 2.68668d16
      real(8), parameter :: PI = 3.141592653589793d0

      ! write additional output for simulation

      ! write test data + header data for high spectral resolution:

      if ( controlSimS%writeHighResolutionRefl .and. (.not. controlSimS%useReflectanceFromFile) ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'REFLECTANCE ON HR GRID FOR SIMULATION'
        call writeTestDataHR(errS, numSpectrBands, geometrySimS, surfaceSimS, cloudAerosolRTMgridSimS, &
                             wavelHRSimS, solarIrradianceSimS, earthRadianceSimS)
        if (errorCheck(errS)) return
      end if

      ! write test data + header data for instrument spectral resolution:

      if ( controlSimS%writeInstrResolutionRefl .and. (.not. controlSimS%useReflectanceFromFile) ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'REFLECTANCE ON INSTRUMENT GRID FOR SIMULATION'
        call writeTestDataInstr(errS, numSpectrBands, geometrySimS, surfaceSimS, cloudAerosolRTMgridSimS, &
                                solarIrradianceSimS, wavelInstrIrrSimS, earthRadianceSimS)
        if (errorCheck(errS)) return
      end if

      if ( controlSimS%writeSNR .and. (.not. controlSimS%useReflectanceFromFile) ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'SIGNAL TO NOISE RATIO'
        call writeSNR(errS, wavelInstrRadSimS, earthRadianceSimS)
        if (errorCheck(errS)) return
      end if

      if ( controlSimS%writeRing_spectra .and. (.not. controlSimS%useReflectanceFromFile) ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'RING SPECTRA for simulation'
        write(addtionalOutputUnit,*) 'Ring spectra based on radiative transfer calculations'
        write(addtionalOutputUnit,*) 'are calculated when in SECTION RRS_RING useRRS = 1'
        call writeRingSpec(errS, wavelInstrRadSimS, RRS_RingSimS)
        if (errorCheck(errS)) return
      end if

      if ( controlSimS%writeDiffRing_spectra .and. (.not. controlSimS%useReflectanceFromFile) ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'DIFFERENTIAL RING SPECTRA'
        write(addtionalOutputUnit,*) 'Differential Ring spectra based on radiative transfer calculations'
        write(addtionalOutputUnit,*) 'are calculated when in SECTION RRS_RING useRRS = 1'
        call writeDiffRingSpec(errS, wavelInstrRadSimS, RRS_RingSimS)
        if (errorCheck(errS)) return
      end if

      if ( controlSimS%writeFilling_in_spectra .and. (.not. controlSimS%useReflectanceFromFile) ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'RRS FILLING IN SPECTRA'
        write(addtionalOutputUnit,*) 'filling in spectra based on radiative transfer calculations'
        write(addtionalOutputUnit,*) 'these spectra are calculated when in SECTION RRS_RING useRRS = 1'
        call writeFillingInSpec(errS, wavelInstrRadSimS, wavelHRSimS, RRS_RingSimS)
        if (errorCheck(errS)) return
      end if

      if ( controlSimS%writeContributionRadiance .and. (.not. controlSimS%useReflectanceFromFile) ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'CONTRIBUTION FOR SIMULATION'
        call writeContribRad_Refl(errS, geometrySimS, wavelHRSimS, wavelInstrRadSimS, optPropRTMGridSimS, &
                                  solarIrradianceSimS, earthRadianceSimS)
        if (errorCheck(errS)) return
      end if

      if ( (.not. controlSimS%singleScatteringOnly) .and. (controlSimS%method == 0 ) ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'INTERNAL FIELD FOR SIMULATION'
        call writeInternalField(errS, controlSimS, geometrySimS, wavelHRSimS, wavelInstrRadSimS, &
                                optPropRTMGridSimS, solarIrradianceSimS, earthRadianceSimS)
        if (errorCheck(errS)) return
      end if

      if ( controlSimS%writeAltResolvedAMF .and. (.not. controlSimS%useReflectanceFromFile) ) then
        do iband = 1, numSpectrBands
          write(addtionalOutputUnit,*)
          write(addtionalOutputUnit,*) 'AMF FOR SIMULATION'
          call writeAltResolvedAMF(errS, iband, wavelHRSimS(iband), cloudAerosolRTMgridSimS, &
                                   optPropRTMGridSimS, reflDerivHRSimS(iband))
          if (errorCheck(errS)) return
        end do
      end if

      if ( controlSimS%simulationOnly ) return   ! STOP HERE for simulation

      ! write additional output for retrieval

      if ( controlRetrS%writeHighResolutionRefl ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'REFLECTANCE ON HR GRID FOR RETRIEVAL'
        call writeTestDataHR(errS, numSpectrBands, geometryRetrS, surfaceRetrS, cloudAerosolRTMgridRetrS, &
                             wavelHRRetrS, solarIrradianceRetrS, earthRadianceRetrS)
        if (errorCheck(errS)) return
      end if

      ! write test data + header data for instrument spectral resolution:

      if ( controlRetrS%writeInstrResolutionRefl ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'REFLECTANCE ON INSTRUMENT GRID FOR RETRIEVAL'
        call writeTestDataInstr(errS, numSpectrBands, geometryRetrS, surfaceRetrS, cloudAerosolRTMgridRetrS, &
                                solarIrradianceRetrS, wavelInstrRadRetrS, earthRadianceRetrS)
        if (errorCheck(errS)) return
      end if


      if ( controlRetrS%writeContributionRadiance) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'CONTRIBUTION FOR RETRIEVAL'
        call writeContribRad_Refl(errS, geometryRetrS, wavelHRRetrS, wavelInstrRadRetrS, optPropRTMGridRetrS, &
                                  solarIrradianceRetrS, earthRadianceRetrS)
        if (errorCheck(errS)) return
      end if

      if ( (.not. controlSimS%singleScatteringOnly) .and. (controlSimS%method == 0 ) ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'INTERNAL FIELD FOR RETRIEVAL'
        call writeInternalField(errS, controlRetrS, geometryRetrS, wavelHRRetrS, wavelInstrRadRetrS, &
                                optPropRTMGridRetrS, solarIrradianceRetrS, earthRadianceRetrS)
        if (errorCheck(errS)) return
      end if

      if ( controlRetrS%writeAltResolvedAMF) then
        do iband = 1, numSpectrBands
          write(addtionalOutputUnit,*)
          write(addtionalOutputUnit,*) 'AMF FOR RETRIEVAL'
          call writeAltResolvedAMF(errS, iband, wavelHRRetrS(iband), cloudAerosolRTMgridSimS,  &
                                   optPropRTMGridRetrS, reflDerivHRRetrS(iband))
          if (errorCheck(errS)) return
        end do
      end if

      if ( controlRetrS%writeRing_spectra ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'RING SPECTRA for retrieval'
        write(addtionalOutputUnit,*) 'Ring spectra based on radiative transfer calculations'
        write(addtionalOutputUnit,*) 'are calculated when in SECTION RRS_RING useRRS = 1'
        call writeRingSpec(errS, wavelInstrRadRetrS, RRS_RingRetrS)
        if (errorCheck(errS)) return
      end if

      if ( controlRetrS%writeDiffRing_spectra ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'DIFFERENTIAL RING SPECTRA for retrieval'
        write(addtionalOutputUnit,*) 'Differential Ring spectra based on radiative transfer calculations'
        write(addtionalOutputUnit,*) 'are calculated when in SECTION RRS_RING useRRS = 1'
        call writeDiffRingSpec(errS, wavelInstrRadRetrS, RRS_RingRetrS)
        if (errorCheck(errS)) return
      end if

      if ( controlRetrS%writeFilling_in_spectra ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'RRS FILLING IN SPECTRA for retrieval'
        write(addtionalOutputUnit,*) 'filling in spectra based on radiative transfer calculations'
        write(addtionalOutputUnit,*) 'these spectra are calculated when in SECTION RRS_RING useRRS = 1'
        call writeFillingInSpec(errS, wavelInstrRadRetrS, wavelHRRetrS, RRS_RingRetrS)
        if (errorCheck(errS)) return
      end if

      ! if polCorrectionFile is .true. write files with polarization correction data
      ! These are ascii text files and are used to test polarization (and RRS) correction
      ! There are restrictions for writing polarization correction data, see verify module
      ! around line 1435.
      if ( controlRetrS%polCorrectionFile ) then
          call writePolarizationCorrectionFile (errS, numSpectrBands, polCorrectionRetrS)
      end if ! controlRetrS%polCorrectionFile

      write(outputFileUnit,*)
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'WAVELENGT GRIDS, RADIANCE, AND IRRADIANCE'
      do iband = 1, numSpectrBands
        write(outputFileUnit,'(A120)') "when 'M' is appended it means monochromatic (ir)radiance"// &
                                       " otherwise the (ir)radiance is convoluted with the slit function"
        if (controlSimS%ignoreSlit .and. controlRetrS%ignoreSlit ) then
          write(outputFileUnit,'(A120)') '   nominal  w_solIrrSim   solIrrSimM  w_earthRadSim earthRadSimM'// &
                                         '  w_solIrrRetr solIrrRetrM  w_earthRadRetr earthRadRetrM'
        end if
        if (.not. controlSimS%ignoreSlit .and. controlRetrS%ignoreSlit ) then
          write(outputFileUnit,'(A120)') '   nominal  w_solIrrSim    solIrrSim  w_earthRadSim  earthRadSim'// &
                                         '  w_solIrrRetr solIrrRetrM  w_earthRadRetr earthRadRetrM'
        end if
        if ( controlSimS%ignoreSlit .and. .not. controlRetrS%ignoreSlit ) then
          write(outputFileUnit,'(A120)') '   nominal  w_solIrrSim   solIrrSimM  w_earthRadSim earthRadSimM'// &
                                         '  w_solIrrRetr  solIrrRetr  w_earthRadRetr  earthRadRetr'
        end if
        if (.not. controlSimS%ignoreSlit .and. .not. controlRetrS%ignoreSlit ) then
          write(outputFileUnit,'(A120)') '   nominal  w_solIrrSim    solIrrSim  w_earthRadSim  earthRadSim'// &
                                         '  w_solIrrRetr  solIrrRetr  w_earthRadRetr  earthRadRetr'
        end if
        do iwave = 1, wavelInstrRadRetrS(iband)%nwavel
          write(outputFileUnit,'(F12.6, 4(F12.6, ES15.6) )')  &
             wavelInstrIrrSimS(iband)%wavelNominal(iwave),    &
             wavelInstrIrrSimS(iband)%wavel(iwave),           &
             solarIrradianceSimS(iband)%solIrrMeas(iwave),    &
             wavelInstrRadSimS(iband)%wavel(iwave),           &
             earthRadianceSimS(iband)%rad_meas(1,iwave),      &
             wavelInstrIrrRetrS(iband)%wavel(iwave),          &
             solarIrradianceRetrS(iband)%solIrr(iwave),       &
             wavelInstrRadRetrS(iband)%wavel(iwave),          &
             earthRadianceRetrS(iband)%rad(1,iwave)
        end do ! iwave
      end do ! iband

      write(outputFileUnit,*)
      write(outputFileUnit,*) 'RESULTS OF OPTIMAL ESTIMATION'
      write(outputFileUnit,*)
      write(outputFileUnit,'(A, 2F10.5)') 'DFS      = ', diagnosticS%DFS
      write(outputFileUnit,'(A,  F10.5)') 'DFSTemp  = ', diagnosticS%DFSTemperature
      write(outputFileUnit,'(A, 5A10)')   'Trace gas  ', traceGasRetrS%nameTraceGas
      write(outputFileUnit,'(A, 5F10.5)') 'DFStrace = ', diagnosticS%DFStrace

      write(outputFileUnit,*)
      write(outputFileUnit,'(A, F10.5)') 'information content lnvmr = ', diagnosticS%infoContent_lnvmr
      write(outputFileUnit,'(A, F10.5)') 'information content   vmr = ', diagnosticS%infoContent_vmr
      write(outputFileUnit,'(A, F10.5)') 'information content ndens = ', diagnosticS%infoContent_ndens
      write(outputFileUnit,*)
      write(outputFileUnit,'(A, F10.5)') 'root mean square error    = ', retrS%rmse

      if ( retrS%isConverged ) then
        write(outputFileUnit,'(A,2F12.5,I4)') ' solution has converged: xConvThreshold xConv  nIter = ', &
                        retrS%xConvThreshold, retrS%xConv, retrS%numIterations
      else
        if ( retrS%isConvergedBoundary ) then
          write(outputFileUnit,'(A)') ' stopped because state vector does not change due to boundaries'
          write(outputFileUnit,'(A,2E15.5,I4)') 'chi2 xConv nIter = ', &
                                                retrS%chi2, retrS%xConv, retrS%numIterations
        else
          write(outputFileUnit,'(A,2E15.5,I4)') ' solution has NOT converged: xConvThreshold xConv nIter = ', &
                                                retrS%xConvThreshold, retrS%xConv, retrS%numIterations
        end if
      end if

      write(outputFileUnit,*)
      write(outputFileUnit,*) 'ACCURACY OF THE FITTED SUN-NORMALIZED RADIANCE (residue)'
      write(outputFileUnit,*)

      write(outputFileUnit,*)
      write(outputFileUnit,'(A)') 'Here reflectance is used to denote the sun-normalized radiance'
      write(outputFileUnit,'(A)') &
        '  wavelRadRetr     actual S/N     measured refl      fitted refl     rel diff(%) rel diff initial(%)'
      do iwave = 1, retrS%nwavelRetr
        write(outputFileUnit,'(A, F12.4, F12.2, 2F18.8, 2F15.6)')  'data',                   &
                                        retrS%wavelRetr(iwave),                              &
                                        retrS%reflMeas(iwave)/retrS%reflNoiseError(iwave),   &
                                        retrS%reflMeas(iwave), retrS%refl(iwave),            &
                              100.0d0 * retrS%dR(iwave)/retrS%reflMeas(iwave),               &
                              100.0d0 * retrS%dR_initial(iwave)/retrS%reflMeas(iwave)
      end do

      if ( controlRetrS%calculateAAI ) then
        write(outputFileUnit,*)
        write(outputFileUnit, '(A,2F12.4)') 'AAI and error AAI (1 sigma)= ', retrS%AAI, sqrt(retrS%varAAI)
      end if

      ! loop over al the state vector elements and print the retrieved values

      ! initialize
      first_time_profile = .true.
      first_time_temp    = .true.

      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case('nodeTrace')

            if ( first_time_profile ) &
              call print_profile(errS, nTrace, traceGasSimS, traceGasRetrS, retrS, &
                                 diagnosticS, ncolumn, optPropRTMGridRetrS,  &
                                 columnSimS, columnRetrS, controlRetrS)
              if (errorCheck(errS)) return

            first_time_profile = .false.

          case('columnTrace')

            iTrace = retrS%codeTraceGas(istate)
            if ( traceGasRetrS(iTrace)%unitFlag ) then
              factor = 1.0d0 / DUToCm2
              write(outputFileUnit,*)
              write(outputFileUnit,'(2A)') traceGasRetrS(iTrace)%nameTraceGas, '  column in Dobson Units'
            else
              factor = 1.0d0
              write(outputFileUnit,*)
              write(outputFileUnit,'(2A)') traceGasRetrS(iTrace)%nameTraceGas, '  column in molecules cm-2'
            end if

            write(outputFileUnit,'(A,4ES12.4,F9.3)') 'a-priori true and RETRIEVED TRACE GAS COLUMN and bias(abs and %) = ', &
                                                       factor * traceGasRetrS(iTrace)%columnAP,                             &
                                                       factor * traceGasSimS(iTrace)%column,                                &
                                                       factor * traceGasRetrS(iTrace)%column,                               &
                         factor * (traceGasRetrS(iTrace)%column - traceGasSimS(iTrace)%column) ,                            &
                    100*( traceGasRetrS(iTrace)%column - traceGasSimS(iTrace)%column ) / traceGasSimS(iTrace)%column
            write(outputFileUnit,'(A,2ES12.4,F9.3)') 'a-priori. a-posteriori, and rel. error (%) trace gas column  = ', &
                                                   factor * sqrt(traceGasRetrS(iTrace)%covColumnAP ),                   &
                                                   factor * sqrt(traceGasRetrS(iTrace)%covColumn),                      &
                                                   100.0d0* sqrt(traceGasRetrS(iTrace)%covColumn) /                     &
                                                                 traceGasRetrS(iTrace)%column

          case('nodeTemp')

            if ( first_time_temp ) then
              write(outputFileUnit,*)
              write(outputFileUnit,*) 'TEMPERATURE PROFILE'
              write(outputFileUnit,'(A)') &
                 ' altitude  pressure  a-priori   true    retrieved  AP error  error'
              do ialt = 0, gasPTRetrS%npressureNodes
                write(outputFileUnit,'(7F10.3)')  gasPTRetrS%altNodes(ialt), gasPTRetrS%pressureNodes(ialt), &
                gasPTRetrS%temperatureNodesAP(ialt), gasPTRetrS%temperature_trueNodes(ialt),                 &
                gasPTRetrS%temperatureNodes(ialt),                                                           &
                sqrt(gasPTRetrS%covTempNodesAP(ialt,ialt)), sqrt(gasPTRetrS%covTempNodes(ialt,ialt))
              end do ! ialt

              ! averaging kernel
              startvalue = 0
              do jstate = 1, retrS%nstate
                if(   (retrS%codeFitParameters(jstate) == 'nodeTemp' ) .and.  &
                      (retrS%codeAltitude(jstate)      == 0           ) ) then
                  startvalue = istate
                end if
              end do

              write(outputFileUnit,*)
              write(outputFileUnit,*) 'transposed averaging kernel for the temperature : A_temp'
              write(outputFileUnit,*) 'columns give the averaging kernel for a certain pressure level'
              write(outputFileUnit,*) 'structure is AK(p1), pressures, AK(p2), pressures,..'
              write(outputFileUnit,*) 'this structure makes plotting in EXCEL easy'
              write(outputFileUnit,'(200(A7,F10.4))') &
                ('      p =',gasPTRetrS%pressureNodes(ialt), '      p =',gasPTRetrS%pressureNodes(ialt), &
                 ialt = 0, gasPTRetrS%npressureNodes )
              do ialt = 0, gasPTRetrS%npressureNodes
                write(outputFileUnit,'(200E17.8)')  &
                  (diagnosticS%A_lnvmr(jalt+startValue,ialt+startValue), gasPTRetrS%pressureNodes(ialt), &
                   jalt = 0, gasPTRetrS%npressureNodes )
              end do

              write(outputFileUnit,*)
              write(outputFileUnit,*) 'averaging kernel dx/dT_i with T_i the temperature nodes'

              do jstate = 1, retrS%nstate

                select case (retrS%codeFitParameters(jstate))

                  case('columnTrace')

                    jTrace = retrS%codeTraceGas(jstate)
                    if ( traceGasRetrS(jTrace)%unitFlag ) then
                      factor = 1.0d0 / DUToCm2
                      write(outputFileUnit,*)
                      write(outputFileUnit,'(2A)') traceGasRetrS(jTrace)%nameTraceGas, '  column in Dobson Units'
                    else
                      factor = 1.0d0
                      write(outputFileUnit,*)
                      write(outputFileUnit,'(2A)') traceGasRetrS(jTrace)%nameTraceGas, '  column in molecules cm-2'
                    end if
                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( factor * diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('surfPressure')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('surfAlbedo')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('LambCldAlbedo')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('aerosolTau')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('aerosolSSA')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('aerosolAC')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('cloudFraction')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('cloudTau')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('cloudAC')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('intervalDP')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )
                    ! test x_retr = x_a + A(x_true - x_a)
                    ! here x_true is the temperature for the simulation
                    !      x_retr is the retrieved altitude
                    !      x_a is the a-priori temperature
                    write(outputFileUnit,'(A, E17.8)') 'retrieved altitude (km) = ', &
                      cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit)
                    write(outputFileUnit,'(A, E17.8)') '     true altitude (km) = ', &
                      cloudAerosolRTMgridSimS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit)
                    sumAKx = 0.0d0
                    do ialt = 0, gasPTRetrS%npressureNodes
                      sumAKx = sumAKx + diagnosticS%A_vmr(jstate, ialt+startValue) &
                             * (gasPTRetrS%temperature_trueNodes(ialt) - gasPTRetrS%temperatureNodesAP(ialt) )
                    end do
                    write(outputFileUnit,'(A, E17.8)') ' altitude from AKdx(km) = ', &
                      cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit) &
                      + sumAKx

                  case('intervalTop')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )

                  case('intervalBot')

                    write(outputFileUnit,'(A15, 200E12.4)') trim(retrS%codeFitParameters(jstate)), &
                        ( diagnosticS%A_vmr(jstate, ialt+startValue), ialt = 0, gasPTRetrS%npressureNodes )


                end select

              end do

            end if

            first_time_temp = .false.

          case('offsetTemp')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved TEMPERATURE OFFSET                 = ', &
                 gasPTRetrS%temperatureOffsetAP, gasPTSimS%temperatureOffset, gasPTRetrS%temperatureOffset
            write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error temperature offset             = ', &
                                                  sqrt(gasPTRetrS%varianceTempOffsetAP), &
                                                  sqrt(gasPTRetrS%varianceTempOffset)
          case('surfPressure')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved SURFACE PRESSURE                   = ', &
                 surfaceRetrS(1)%pressureAP, surfaceSimS(1)%pressure, surfaceRetrS(1)%pressure
            write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error surface pressure               = ', &
                                                  sqrt(surfaceRetrS(1)%varPressureAP), &
                                                  sqrt(surfaceRetrS(1)%varPressure)

          case('surfAlbedo')

            if ( cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then

              ! the surface albedo is the same for all wavelengths and all spectral bands

              write(outputFileUnit,*)
              write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved SURFACE ALBEDO                     = ', &
                 cloudAerosolRTMgridRetrS%albedoLambSurfAllBandsAP, &
                 cloudAerosolRTMgridSimS%albedoLambSurfAllBands,    &
                 cloudAerosolRTMgridRetrS%albedoLambSurfAllBands
              write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error surface albedo                 = ', &
                sqrt(cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP), sqrt(cloudAerosolRTMgridRetrS%varAlbedoLambSurfAll)

            else


              iband = retrS%codeSpecBand(istate)
              index = retrS%codeIndexSurfAlb(istate)

              if ( index == 1) then

                write(outputFileUnit,*)
                write(outputFileUnit,'(A,50F12.6)') 'wavelengths for surface albedo in simulation [nm]              = ', &
                                                     surfaceSimS(iband)%wavelAlbedo
                write(outputFileUnit,'(A,50F12.6)') 'true surface albedo                                            = ', &
                                                     surfaceSimS(iband)%albedoAP
                write(outputFileUnit,'(A,50F12.6)') 'wavelengths for retrieved surface albedo [nm]                  = ', &
                                                     surfaceRetrS(iband)%wavelAlbedo
                write(outputFileUnit,'(A,50F12.6)') 'a-priori for retrieved surface albedo                          = ', &
                                                     surfaceRetrS(iband)%albedoAP
                write(outputFileUnit,'(A,50F12.6)') 'RETRIEVED SURFACE ALBEDO                                       = ', &
                                                     surfaceRetrS(iband)%albedo
                write(outputFileUnit,'(A,50F12.6)') 'a-priori error surface albedo (1 std dev)                      = ', &
                                                     sqrt(surfaceRetrS(iband)%varianceAlbedoAP)
                write(outputFileUnit,'(A,50F12.6)') 'a-posteriori error surface albedo (1 std dev)                  = ', &
                                                     sqrt(surfaceRetrS(iband)%varianceAlbedo)
              end if ! index == 1
            end if ! cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands

          case('surfEmission')

            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexSurfEmission(istate)

            if ( index == 1) then

              write(outputFileUnit,*)
              write(outputFileUnit,'(A,50F12.6)') 'wavelengths for surface emission in simulation [nm]            = ', &
                                                   surfaceSimS(iband)%wavelEmission
              write(outputFileUnit,'(A,50E12.3)') 'true surface emission                                          = ', &
                                                   surfaceSimS(iband)%emission
              write(outputFileUnit,'(A,50F12.6)') 'wavelengths for retrieved surface emission [nm]                = ', &
                                                   surfaceRetrS(iband)%wavelEmission
              write(outputFileUnit,'(A,50E12.3)') 'a-priori for retrieved surface emission                        = ', &
                                                   surfaceRetrS(iband)%emissionAP
              write(outputFileUnit,'(A,50E12.3)') 'RETRIEVED SURFACE EMISSIOM                                     = ', &
                                                   surfaceRetrS(iband)%emission
              write(outputFileUnit,'(A,50E12.3)') 'a-priori error surface emission (1 std dev)                    = ', &
                                                   sqrt(surfaceRetrS(iband)%varianceEmissionAP)
              write(outputFileUnit,'(A,50E12.3)') 'a-posteriori error surface emission (1 std dev)                = ', &
                                                   sqrt(surfaceRetrS(iband)%varianceEmission)
            end if ! index == 1

          case('LambCldAlbedo')

            if ( cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then

              ! the cloud albedo is the same for all wavelengths and all spectral bands

              write(outputFileUnit,*)
              write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved LAMBERTIAN CLOUD ALBEDO            = ', &
                 cloudAerosolRTMgridRetrS%albedoLambCldAllBandsAP, &
                 cloudAerosolRTMgridSimS%albedoLambCldAllBands,    &
                 cloudAerosolRTMgridRetrS%albedoLambCldAllBands
              write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error lambertian cloud albedo        = ', &
                sqrt(cloudAerosolRTMgridRetrS%varAlbedoLambCldAllAP), sqrt(cloudAerosolRTMgridRetrS%varAlbedoLambCldAll)

            else

              ! cloud albedo can differ for spectral bands and can be a function of the wavelength

              iband = retrS%codeSpecBand(istate)
              index = retrS%codeIndexLambCldAlb(istate)

              if ( index == 1) then

                write(outputFileUnit,*)
                write(outputFileUnit,'(A,50F12.6)') 'wavelengths for cloud albedo in simulation [nm]                = ', &
                                                     LambCloudSimS(iband)%wavelAlbedo
                write(outputFileUnit,'(A,50F12.6)') 'true cloud albedo                                              = ', &
                                                     LambCloudSimS(iband)%albedoAP
                write(outputFileUnit,'(A,50F12.6)') 'wavelengths for retrieved cloud albedo [nm]                    = ', &
                                                     LambCloudRetrS(iband)%wavelAlbedo
                write(outputFileUnit,'(A,50F12.6)') 'a-priori for retrieved cloud albedo                            = ', &
                                                     LambCloudRetrS(iband)%albedoAP
                write(outputFileUnit,'(A,50F12.6)') 'RETRIEVED CLOUD ALBEDO                                         = ', &
                                                     LambCloudRetrS(iband)%albedo
                write(outputFileUnit,'(A,50F12.6)') 'a-priori error cloud albedo (1 std dev)                        = ', &
                                                     sqrt(LambCloudRetrS(iband)%varianceAlbedoAP)
                write(outputFileUnit,'(A,50F12.6)') 'a-posteriori error cloud albedo (1 std dev)                    = ', &
                                                     sqrt(LambCloudRetrS(iband)%varianceAlbedo)
              end if ! index == 1

            end if ! cloudAerosolRTMgridS%useAlbedoLambCldAllBands

          case('mulOffset')

            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexMulOffset(istate)

            if ( index == 1) then

              write(outputFileUnit,*)
              write(outputFileUnit,'(A,50F12.6)') 'wavelengths for multiplicative offset in simulation [nm]       = ', &
                                                   mulOffsetSimS(iband)%wavel
              write(outputFileUnit,'(A,50F12.6)') 'percent multiplicative offset in simulation                    = ', &
                                                   mulOffsetSimS(iband)%percentOffset
              write(outputFileUnit,'(A,50F12.6)') 'wavelengths for multiplicative offset in retrieval [nm]        = ', &
                                                   mulOffsetRetrS(iband)%wavel
              write(outputFileUnit,'(A,50F12.6)') 'a-priori percent multiplicative offset in retrieval            = ', &
                                                   mulOffsetRetrS(iband)%percentOffsetAP
              write(outputFileUnit,'(A,50F12.6)') 'RETRIEVED PERCENT MULTIPLICATIVE OFFSET                        = ', &
                                                   mulOffsetRetrS(iband)%percentOffset
              write(outputFileUnit,'(A,50F12.6)') 'a-priori error percent multiplicative offset (1 std dev)       = ', &
                                                   sqrt(mulOffsetRetrS(iband)%varMulOffsetAP)
              write(outputFileUnit,'(A,50F12.6)') 'a-posteriori error percent multiplicative offset (1 std dev)   = ', &
                                                   sqrt(mulOffsetRetrS(iband)%varMulOffset)
            end if


          case('straylight')

            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexStraylight(istate)

            if ( index == 1) then

              write(outputFileUnit,*)
              write(outputFileUnit,'(A,50F12.6)') 'wavelengths for straylight in simulation [nm]                  = ', &
                                                   strayLightSimS(iband)%wavel
              write(outputFileUnit,'(A,50F12.6)') 'percent straylight in simulation                               = ', &
                                                   strayLightSimS(iband)%percentStrayLight
              write(outputFileUnit,'(A,50F12.6)') 'wavelengths for straylight in retrieval [nm]                   = ', &
                                                   strayLightRetrS(iband)%wavel
              write(outputFileUnit,'(A,50F12.6)') 'a-priori percent straylight in retrieval                       = ', &
                                                   strayLightRetrS(iband)%percentStrayLightAP
              write(outputFileUnit,'(A,50F12.6)') 'RETRIEVED PERCENT STRAY LIGHT                                  = ', &
                                                   strayLightRetrS(iband)%percentStrayLight
              write(outputFileUnit,'(A,50F12.6)') 'a-priori error percent stray light (1 std dev)                 = ', &
                                                   sqrt(strayLightRetrS(iband)%varStrayLightAP)
              write(outputFileUnit,'(A,50F12.6)') 'a-posteriori error percent stray light (1 std dev)             = ', &
                                                   sqrt(strayLightRetrS(iband)%varStrayLight)
            end if

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)
            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.8)')   'a-priori true and retrieved DIFFERENTAL RING COEFFICIENT       = ', &
                 RRS_RingRetrS(iband)%ringCoeffAP, RRS_RingSimS(iband)%ringCoeff, RRS_RingRetrS(iband)%ringCoeff
            write(outputFileUnit,'(A, 2F12.8)')   'a-priori and a-posteriori error differential Ring coefficient  = ', &
                sqrt(RRS_RingRetrS(iband)%varRingCoeffAP), sqrt(RRS_RingRetrS(iband)%varRingCoeff)

          case('RingCoef')

            iband = retrS%codeSpecBand(istate)
            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved RING COEFFICIENT                   = ', &
                 RRS_RingRetrS(iband)%ringCoeffAP, RRS_RingSimS(iband)%ringCoeff, RRS_RingRetrS(iband)%ringCoeff
            write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error Ring coefficient               = ', &
                sqrt(RRS_RingRetrS(iband)%varRingCoeffAP), sqrt(RRS_RingRetrS(iband)%varRingCoeff)

          case('aerosolTau')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved AEROSOL TAU                        = ', &
                 cloudAerosolRTMgridRetrS%intervalAerTauAP(cloudAerosolRTMgridSimS%numIntervalFit), &
                 cloudAerosolRTMgridSimS %intervalAerTau(cloudAerosolRTMgridSimS%numIntervalFit),   &
                 cloudAerosolRTMgridRetrS%intervalAerTau(cloudAerosolRTMgridSimS%numIntervalFit)
            write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error aerosol tau                    = ', &
                sqrt(cloudAerosolRTMgridRetrS%varAerTauAP), sqrt(cloudAerosolRTMgridRetrS%varAerTau)

          case('aerosolSSA')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved AEROSOL SSA                        = ', &
                 cloudAerosolRTMgridRetrS%intervalAerSSAAP(cloudAerosolRTMgridSimS%numIntervalFit), &
                 cloudAerosolRTMgridSimS %intervalAerSSA(cloudAerosolRTMgridSimS%numIntervalFit),   &
                 cloudAerosolRTMgridRetrS%intervalAerSSA(cloudAerosolRTMgridSimS%numIntervalFit)
            write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error aerosol ssa                    = ', &
                sqrt(cloudAerosolRTMgridRetrS%varAerSSAAP), sqrt(cloudAerosolRTMgridRetrS%varAerSSA)

          case('aerosolAC')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved AEROSOL ANGSTROM COEFFICIENT       = ', &
                 cloudAerosolRTMgridRetrS%intervalAerACAP(cloudAerosolRTMgridSimS%numIntervalFit), &
                 cloudAerosolRTMgridSimS %intervalAerAC(cloudAerosolRTMgridSimS%numIntervalFit),   &
                 cloudAerosolRTMgridRetrS%intervalAerAC(cloudAerosolRTMgridSimS%numIntervalFit)
            write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error aerosol angstrom coefficient   = ', &
                sqrt(cloudAerosolRTMgridRetrS%varAerACAP), sqrt(cloudAerosolRTMgridRetrS%varAerAC)

          case('cloudFraction')

            if ( cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then

              ! the cloud/aerosol fraction is the same for all wavelengths and all spectral bands

              write(outputFileUnit,*)
              write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved CLOUD / AEROSOL FRACTION           = ', &
                                                                 cloudAerosolRTMgridRetrS%cldAerFractionAllBandsAP,    &
                                                                 cloudAerosolRTMgridSimS %cldAerFractionAllBands,      &
                                                                 cloudAerosolRTMgridRetrS%cldAerFractionAllBands
              write(outputFileUnit,'(A, 2F12.6)') 'a-priori and a-posteriori error cloud fraction                 = ', &
                                                  sqrt(cloudAerosolRTMgridRetrS%varCldAerFractionAllBandsAP),          &
                                                  sqrt(cloudAerosolRTMgridRetrS%varCldAerFractionAllBands)

            else
              ! the cloud fraction can differ for spectral bands and can be a function of the wavelength

              iband = retrS%codeSpecBand(istate)
              index = retrS%codeIndexCloudFraction(istate)

              if ( index == 1) then

                write(outputFileUnit,*)
                write(outputFileUnit,'(A,50F12.6)') 'wavelengths for cld/aer fraction in simulation [nm]            = ', &
                                                     cldAerFractionSimS(iband)%wavelCldAerFraction
                write(outputFileUnit,'(A,50F12.6)') 'true cld/aer fraction                                          = ', &
                                                     cldAerFractionSimS(iband)%cldAerFractionAP
                write(outputFileUnit,'(A,50F12.6)') 'wavelengths for retrieved cloud albedo [nm]                    = ', &
                                                     cldAerFractionRetrS(iband)%wavelCldAerFraction
                write(outputFileUnit,'(A,50F12.6)') 'a-priori for retrieved cld/aer fraction                        = ', &
                                                     cldAerFractionRetrS(iband)%cldAerFractionAP
                write(outputFileUnit,'(A,50F12.6)') 'RETRIEVED CLOUD / AEROSOL FRACTION                             = ', &
                                                     cldAerFractionRetrS(iband)%cldAerFraction
                write(outputFileUnit,'(A,50F12.6)') 'a-priori error cld/aer fraction (1 std dev)                    = ', &
                                                     sqrt(cldAerFractionRetrS(iband)%varianceCldAerFractionAP)
                write(outputFileUnit,'(A,50F12.6)') 'a-posteriori error cld/aer fraction (1 std dev)                = ', &
                                                     sqrt(cldAerFractionRetrS(iband)%varianceCldAerFraction)
              end if ! index == 1

            end if ! cloudAerosolRTMgridRetrS%useCloudFractionAllBands

          case('cloudTau')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved CLOUD TAU                          = ', &
                 cloudAerosolRTMgridRetrS%intervalCldTauAP(cloudAerosolRTMgridSimS%numIntervalFit), &
                 cloudAerosolRTMgridSimS %intervalCldTau(cloudAerosolRTMgridSimS%numIntervalFit),   &
                 cloudAerosolRTMgridRetrS%intervalCldTau(cloudAerosolRTMgridSimS%numIntervalFit)
            write(outputFileUnit,'(A, 2F12.6)') 'a-priori and a-posteriori error cloud tau                      = ', &
                                                sqrt(cloudAerosolRTMgridRetrS%varCldTauAP), &
                                                sqrt(cloudAerosolRTMgridRetrS%varCldTau)

          case('cloudAC')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)')   'a-priori true and retrieved CLOUD ANGSTROM COEFFICIENT         = ', &
                 cloudAerosolRTMgridRetrS%intervalCldACAP(cloudAerosolRTMgridSimS%numIntervalFit), &
                 cloudAerosolRTMgridSimS %intervalCldAC(cloudAerosolRTMgridSimS%numIntervalFit),   &
                 cloudAerosolRTMgridRetrS%intervalCldAC(cloudAerosolRTMgridSimS%numIntervalFit)
            write(outputFileUnit,'(A, 2F12.6)')   'a-priori and a-posteriori error cloud angstrom coefficient     = ', &
                sqrt(cloudAerosolRTMgridRetrS%varCldACAP), sqrt(cloudAerosolRTMgridRetrS%varCldAC)

          case('intervalDP')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A)') 'pressure difference of fit interval is fixed'
            write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved TOP ALTITUDE for fit interval [km] = ', &
                                 cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit), &
                                 cloudAerosolRTMgridSimS %intervalBounds(cloudAerosolRTMgridSimS%numIntervalFit),    &
                                 cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit)
            write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved BOTTOM ALTITUDE fit interval [km]  = ', &
                             cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit - 1), &
                             cloudAerosolRTMgridSimS %intervalBounds  (cloudAerosolRTMgridSimS %numIntervalFit - 1), &
                             cloudAerosolRTMgridRetrS%intervalBounds  (cloudAerosolRTMgridRetrS%numIntervalFit - 1)
            write(outputFileUnit,'(A, 2F12.6)') 'a-priori and a-posteriori error TOP ALTITUDE interval [km]     = ', &
                        sqrt(cloudAerosolRTMgridRetrS %varIntervalBoundsAP(cloudAerosolRTMgridSimS%numIntervalFit)), &
                        sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds  (cloudAerosolRTMgridRetrS%numIntervalFit))
            write(outputFileUnit,'(A,15F12.6)') 'interval boundaries [km]                                       = ', &
                        cloudAerosolRTMgridRetrS%intervalBounds(:)
            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved TOP PRESSURE for fit interval[hPa] = ', &
                               cloudAerosolRTMgridRetrS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%numIntervalFit), &
                               cloudAerosolRTMgridSimS %intervalBounds_P(cloudAerosolRTMgridSimS%numIntervalFit),    &
                               cloudAerosolRTMgridRetrS%intervalBounds_P(cloudAerosolRTMgridRetrS%numIntervalFit)
            write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved BOTTOM PRESSURE fit interval [hPa] = ', &
                           cloudAerosolRTMgridRetrS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%numIntervalFit - 1), &
                           cloudAerosolRTMgridSimS %intervalBounds_P  (cloudAerosolRTMgridSimS %numIntervalFit - 1), &
                           cloudAerosolRTMgridRetrS%intervalBounds_P  (cloudAerosolRTMgridRetrS%numIntervalFit - 1)
            write(outputFileUnit,'(A, 2F12.6)') 'a-priori and a-posteriori error TOP PRESSURE [hPa]             = ', &
                       sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP_P(cloudAerosolRTMgridSimS%numIntervalFit)), &
                       sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds_P  (cloudAerosolRTMgridRetrS%numIntervalFit))
            write(outputFileUnit,'(A,15F12.6)') 'interval boundaries [hPa]                                      = ', &
                        cloudAerosolRTMgridRetrS%intervalBounds_P(:)


          case('intervalTop')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved TOP ALTITUDE for fit interval [km] = ', &
                                 cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit), &
                                 cloudAerosolRTMgridSimS %intervalBounds(cloudAerosolRTMgridSimS%numIntervalFit),    &
                                 cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit)
            write(outputFileUnit,'(A, 2F12.6)') 'a-priori and a-posteriori error top ALTITUDE interbal [km]     = ', &
                        sqrt(cloudAerosolRTMgridRetrS %varIntervalBoundsAP(cloudAerosolRTMgridSimS%numIntervalFit)), &
                        sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds  (cloudAerosolRTMgridRetrS%numIntervalFit))
            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved TOP PRESSURE for fit interval[hPa] = ', &
                               cloudAerosolRTMgridRetrS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%numIntervalFit), &
                               cloudAerosolRTMgridSimS %intervalBounds_P  (cloudAerosolRTMgridSimS%numIntervalFit),  &
                               cloudAerosolRTMgridRetrS%intervalBounds_P  (cloudAerosolRTMgridRetrS%numIntervalFit)
            write(outputFileUnit,'(A, 2F12.6)') 'a-priori and a-posteriori error TOP PRESSURE [hPa]             = ', &
                       sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP_P(cloudAerosolRTMgridSimS%numIntervalFit)), &
                       sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds_P  (cloudAerosolRTMgridRetrS%numIntervalFit))

          case('intervalBot')

            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved BOTTOM ALTITUDE fit interval [km]  = ', &
                             cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit - 1), &
                             cloudAerosolRTMgridSimS %intervalBounds  (cloudAerosolRTMgridSimS %numIntervalFit - 1), &
                             cloudAerosolRTMgridRetrS%intervalBounds  (cloudAerosolRTMgridRetrS%numIntervalFit - 1)
            write(outputFileUnit,'(A, 2F12.6)') 'a-priori and a-posteriori error top altitude interval [km]     = ', &
                    sqrt(cloudAerosolRTMgridRetrS %varIntervalBoundsAP(cloudAerosolRTMgridSimS%numIntervalFit - 1)), &
                    sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds  (cloudAerosolRTMgridRetrS%numIntervalFit - 1))
            write(outputFileUnit,*)
            write(outputFileUnit,'(A, 3F12.6)') 'a-priori true and retrieved BOTTOM PRESSURE fit interval [hPa] = ', &
                           cloudAerosolRTMgridRetrS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%numIntervalFit - 1), &
                           cloudAerosolRTMgridSimS %intervalBounds_P  (cloudAerosolRTMgridSimS %numIntervalFit - 1), &
                           cloudAerosolRTMgridRetrS%intervalBounds_P  (cloudAerosolRTMgridRetrS%numIntervalFit - 1)
            write(outputFileUnit,'(A, 2F12.6)') 'a-priori and a-posteriori error TOP PRESSURE [hPa]             = ', &
                  sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP_P(cloudAerosolRTMgridSimS %numIntervalFit - 1)), &
                  sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds_P  (cloudAerosolRTMgridRetrS%numIntervalFit - 1))

          case default
            call logDebug('incorrect value for retrS%codeFitParameters(counter) : '// retrS%codeFitParameters(istate))
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return
        end select

      end do ! istate

      string    = 'full a priori covariance matrix Sa_lnvmr'
      corr_flag = .false.
      call print_covariance_correlation(errS, string, numSpectrBands, retrS%nstate, retrS, retrS%Sa_lnvmr, corr_flag,  &
                          cloudAerosolRTMgridRetrS, surfaceRetrS, cldAerFractionRetrS, LambCloudRetrS)
      if (errorCheck(errS)) return

      string    = 'full a priori covariance matrix Sa_vmr'
      corr_flag = .false.
      call print_covariance_correlation(errS, string, numSpectrBands, retrS%nstate, retrS, retrS%Sa_vmr, corr_flag,  &
                          cloudAerosolRTMgridRetrS, surfaceRetrS, cldAerFractionRetrS, LambCloudRetrS)
      if (errorCheck(errS)) return

      string    = 'full a posteriori covariance matrix S_lnvmr'
      corr_flag = .false.
      call print_covariance_correlation(errS, string, numSpectrBands, retrS%nstate, retrS, diagnosticS%S_lnvmr, corr_flag,  &
                          cloudAerosolRTMgridRetrS, surfaceRetrS, cldAerFractionRetrS, LambCloudRetrS)
      if (errorCheck(errS)) return

      string    = 'full a posteriori covariance matrix S_vmr'
      corr_flag = .false.
      call print_covariance_correlation(errS, string, numSpectrBands, retrS%nstate, retrS, diagnosticS%S_vmr, corr_flag,  &
                          cloudAerosolRTMgridRetrS, surfaceRetrS, cldAerFractionRetrS, LambCloudRetrS)
      if (errorCheck(errS)) return

      string    = 'noise part of a posteriori covariance matrix S_vmr'
      corr_flag = .false.
      call print_covariance_correlation(errS, string, numSpectrBands, retrS%nstate, retrS, diagnosticS%Snoise_vmr, corr_flag,  &
                          cloudAerosolRTMgridRetrS, surfaceRetrS, cldAerFractionRetrS, LambCloudRetrS)
      if (errorCheck(errS)) return

      string    = 'correlation for a posteriori covariance matrix S_lnvmr'
      corr_flag = .true.
      call print_covariance_correlation(errS, string, numSpectrBands, retrS%nstate, retrS, diagnosticS%S_lnvmr, corr_flag,  &
                          cloudAerosolRTMgridRetrS, surfaceRetrS, cldAerFractionRetrS, LambCloudRetrS)
      if (errorCheck(errS)) return

      string    = 'full averaging kernel A_lnvmr'
      corr_flag = .false.
      call print_covariance_correlation(errS, string, numSpectrBands, retrS%nstate, retrS, diagnosticS%A_lnvmr, corr_flag,  &
                          cloudAerosolRTMgridRetrS, surfaceRetrS, cldAerFractionRetrS, LambCloudRetrS)
      if (errorCheck(errS)) return

      string    = 'full averaging kernel A_vmr'
      corr_flag = .false.
      call print_covariance_correlation(errS, string, numSpectrBands, retrS%nstate, retrS, diagnosticS%A_vmr, corr_flag,  &
                          cloudAerosolRTMgridRetrS, surfaceRetrS, cldAerFractionRetrS, LambCloudRetrS)
      if (errorCheck(errS)) return

      write(outputFileUnit,*)
      write(outputFileUnit,*) 'Transposed gain matrix G for number density: G_ndens'
      !write(outputFileUnit,*) 'number density in molecules cm-3'
      !write(outputFileUnit,'(20X,100(13X,A))') (trim(traceGasRetrS(iTrace)%nameTraceGas), iTrace = 1, nTrace)
      write(outputFileUnit,'(  A,100(7X,A))') 'wavelength(nm)       ', &
                           (trim(retrS%codeFitParameters(istate)), istate = 1, retrS%nstate)
      do iwave = 1, retrS%nwavelRetr
        write(outputFileUnit,'(F14.8, 6X,100ES17.7)') retrS%wavelRetr(iwave), &
             (diagnosticS%G_ndens(istate,iwave), istate = 1, retrS%nstate)
      end do

      write(outputFileUnit,*)
      write(outputFileUnit,'(A)') 'Transposed gain matrix G for number density multiplied by sun-norm radiance: G_ndens * R'
      write(outputFileUnit,'(A)') 'so that the error is sum_wavel[G(lambda_i) * R(lambda_i) * eps(lambda_i) / R(lambda_i)] '
      write(outputFileUnit,'(A)') 'where eps(lambda_i) / R(lambda_i)] is the relative error for the sun-norm radiance'
      !write(outputFileUnit,'(A)') 'number density in molecules cm-3'
      !write(outputFileUnit,'(20X,100(13X,A))') (trim(traceGasRetrS(iTrace)%nameTraceGas), iTrace = 1, nTrace)
      write(outputFileUnit,'(  A,100(7X,A))') 'wavelength(nm)       ', &
                           (trim(retrS%codeFitParameters(istate)), istate = 1, retrS%nstate)
      do iwave = 1, retrS%nwavelRetr
        write(outputFileUnit,'(F14.8, 6X,100ES17.7)') retrS%wavelRetr(iwave), &
             (diagnosticS%G_ndens(istate,iwave)*retrS%refl(iwave), istate = 1, retrS%nstate)
      end do

    end subroutine print_results


    subroutine print_covariance_correlation(errS, string, numSpectrBands, nstate, retrS, covar, corr_flag,  &
                          cloudAerosolRTMgridRetrS, surfaceRetrS, cldAerFractionRetrS, LambCloudRetrS)

      ! write full errror covariance matrix or correlation matrix

      type(errorType), intent(inout) :: errS
      character(LEN = 80),           intent(in) :: string
      integer,                       intent(in) :: numSpectrBands
      integer,                       intent(in) :: nstate
      type(retrType),                intent(in) :: retrS
      real(8),                       intent(in) :: covar(nstate,nstate)
      logical,                       intent(in) :: corr_flag
      type(cloudAerosolRTMgridType), intent(in) :: cloudAerosolRTMgridRetrS
      type(LambertianType),          intent(in) :: surfaceRetrS(numSpectrBands)
      type(cldAerFractionType),      intent(in) :: cldAerFractionRetrS(numSpectrBands)
      type(LambertianType),          intent(in) :: LambCloudRetrS(numSpectrBands)


      ! local
      integer    :: istate, jstate, iband, index

      write(outputFileUnit,*)
      write(outputFileUnit,*) trim(string)

      if ( corr_flag ) then

        do istate = 1, nstate

          iband = retrS%codeSpecBand(istate)

          select case (retrS%codeFitParameters(istate))

            case('surfAlbedo')
              index = retrS%codeIndexSurfAlb(istate)
              if ( cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands) then
                write(outputFileUnit,'(A15, 8X, 500E17.8)') retrS%codeFitParameters(istate), &
                (covar(istate,jstate)/sqrt(covar(istate,istate))/sqrt(covar(jstate,jstate)), jstate = 1, nstate)
              else
                write(outputFileUnit,'(A15, F8.2, 500E17.8)') retrS%codeFitParameters(istate), &
                                                       surfaceRetrS(iband)%wavelAlbedo(index), &
                (covar(istate,jstate)/sqrt(covar(istate,istate))/sqrt(covar(jstate,jstate)), jstate = 1, nstate)
              end if

            case('cloudFraction')
              if ( cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then
                write(outputFileUnit,'(A15,   8X, 500E17.8)') retrS%codeFitParameters(istate), &
                (covar(istate,jstate)/sqrt(covar(istate,istate))/sqrt(covar(jstate,jstate)), jstate = 1, nstate)
              else
                index = retrS%codeIndexCloudFraction(istate)
                write(outputFileUnit,'(A15, F8.2, 500E17.8)') retrS%codeFitParameters(istate), &
                                        cldAerFractionRetrS(iband)%wavelCldAerFraction(index), &
                (covar(istate,jstate)/sqrt(covar(istate,istate))/sqrt(covar(jstate,jstate)), jstate = 1, nstate)
              end if
            case('LambCldAlbedo')
              if ( cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then
                write(outputFileUnit,'(A15,   8X, 500E17.8)') retrS%codeFitParameters(istate), &
                (covar(istate,jstate)/sqrt(covar(istate,istate))/sqrt(covar(jstate,jstate)), jstate = 1, nstate)
              else
                index = retrS%codeIndexLambCldAlb(istate)
                write(outputFileUnit,'(A15, F8.2, 500E17.8)') retrS%codeFitParameters(istate), &
                                                     LambCloudRetrS(iband)%wavelAlbedo(index), &
                (covar(istate,jstate)/sqrt(covar(istate,istate))/sqrt(covar(jstate,jstate)), jstate = 1, nstate)
              end if
            case default
              write(outputFileUnit,'(A15, 8X, 500E17.8)') retrS%codeFitParameters(istate), &
                (covar(istate,jstate)/sqrt(covar(istate,istate))/sqrt(covar(jstate,jstate)), jstate = 1, nstate)

          end select ! retrS%codeFitParameters(istate)

        end do

      else

        do istate = 1, nstate

          iband = retrS%codeSpecBand(istate)

          select case (retrS%codeFitParameters(istate))

            case('surfAlbedo')
              index = retrS%codeIndexSurfAlb(istate)
              if ( cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands) then
                write(outputFileUnit,'(A15, 8X, 500E17.8)') retrS%codeFitParameters(istate), &
                                                 (covar(istate,jstate), jstate = 1, nstate)
              else
                write(outputFileUnit,'(A15, F8.2, 500E17.8)') retrS%codeFitParameters(istate), &
                                                       surfaceRetrS(iband)%wavelAlbedo(index), &
                                                   (covar(istate,jstate), jstate = 1, nstate)
              end if

            case('cloudFraction')
              if ( cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then
                write(outputFileUnit,'(A15,   8X, 500E17.8)') retrS%codeFitParameters(istate), &
                                                   (covar(istate,jstate), jstate = 1, nstate)
              else
                index = retrS%codeIndexCloudFraction(istate)
                write(outputFileUnit,'(A15, F8.2, 500E17.8)') retrS%codeFitParameters(istate), &
                                        cldAerFractionRetrS(iband)%wavelCldAerFraction(index), &
                                                   (covar(istate,jstate), jstate = 1, nstate)
              end if
            case('LambCldAlbedo')
              if ( cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then
                write(outputFileUnit,'(A15,   8X, 500E17.8)') retrS%codeFitParameters(istate), &
                                                   (covar(istate,jstate), jstate = 1, nstate)
              else
                index = retrS%codeIndexLambCldAlb(istate)
                write(outputFileUnit,'(A15, F8.2, 500E17.8)') retrS%codeFitParameters(istate), &
                                                     LambCloudRetrS(iband)%wavelAlbedo(index), &
                                                     (covar(istate,jstate), jstate = 1, nstate)
              end if
            case default
              write(outputFileUnit,'(A15, 8X, 500E17.8)') retrS%codeFitParameters(istate), &
                                               (covar(istate,jstate), jstate = 1, nstate)

          end select ! retrS%codeFitParameters(istate)

        end do

      end if ! corr_flag

    end subroutine print_covariance_correlation


    subroutine print_asciiHDF(errS, external_dataS, numSpectrBands, nTrace, controlSimS, controlRetrS,   &
                              wavelHRSimS, wavelHRRetrS, wavelInstrIrrSimS, wavelInstrRadSimS,           &
                              geometrySimS, geometryRetrS, reflDerivHRSimS, reflDerivHRRetrS,            &
                              gasPTSimS, gasPTRetrS, solarIrradianceSimS, solarIrradianceRetrS,          &
                              earthRadianceSimS, earthRadianceRetrS, XsecHRSimS, XsecHRRetrS,            &
                              XsecHRLUTSimS, XsecHRLUTRetrS,                                             &
                              traceGasSimS, traceGasRetrS, surfaceSimS, surfaceRetrS, LambCloudSimS,     &
                              LambCloudRetrS, cldAerFractionSimS, cldAerFractionRetrS,                   &
                              cloudAerosolRTMgridSimS, cloudAerosolRTMgridRetrS,                         &
                              mulOffsetSimS, mulOffsetRetrS, strayLightSimS, strayLightRetrS,            &
                              RRS_RingSimS, RRS_RingRetrS, retrS, diagnosticS, ncolumn,                  &
                              optPropRTMGridSimS, optPropRTMGridRetrS, columnSimS, columnRetrS)

      ! print HDF-like output using ascii

      implicit none

      type(errorType)              ,intent(inout) :: errS
      integer                      ,intent(in) :: numSpectrBands
      integer                      ,intent(in) :: nTrace
      type(external_data)          ,intent(in) :: external_dataS                     ! external data not used by DISAMAR
      type(controlType)            ,intent(in) :: controlSimS
      type(controlType)            ,intent(in) :: controlRetrS
      type(wavelHRType)            ,intent(in) :: wavelHRSimS(numSpectrBands)
      type(wavelHRType)            ,intent(in) :: wavelHRRetrS(numSpectrBands)
      type(wavelInstrType)         ,intent(in) :: wavelInstrIrrSimS(numSpectrBands)
      type(wavelInstrType)         ,intent(in) :: wavelInstrRadSimS(numSpectrBands)
      type(geometryType)           ,intent(in) :: geometrySimS                       ! geo information for simulation
      type(geometryType)           ,intent(in) :: geometryRetrS                      ! geo information for retrieval
      type(reflDerivType)          ,intent(in) :: reflDerivHRSimS(numSpectrBands)
      type(reflDerivType)          ,intent(in) :: reflDerivHRRetrS(numSpectrBands)
      type(gasPTType)              ,intent(in) :: gasPTSimS, gasPTRetrS              ! pressure and temperature
      type(solarIrrType)           ,intent(in) :: solarIrradianceSimS(numSpectrBands)
      type(solarIrrType)           ,intent(in) :: solarIrradianceRetrS(numSpectrBands)
      type(earthRadianceType)      ,intent(inout) :: earthRadianceSimS(numSpectrBands)
      type(earthRadianceType)      ,intent(inout) :: earthRadianceRetrS(numSpectrBands)
      type(XsecType)               ,intent(in) :: XsecHRSimS(numSpectrBands, nTrace)  ! absorption cross sections
      type(XsecType)               ,intent(in) :: XsecHRRetrS(numSpectrBands, nTrace) ! absorption cross sections
      type(XsecLUTType)            ,intent(in) :: XsecHRLUTSimS(numSpectrBands, nTrace)  ! LUT for expansion coefficients
      type(XsecLUTType)            ,intent(in) :: XsecHRLUTRetrS(numSpectrBands, nTrace) ! LUT for expansion coefficients
      type(traceGasType)           ,intent(inout) :: traceGasSimS(nTrace)             ! trace gas properties
      type(traceGasType)           ,intent(inout) :: traceGasRetrS(nTrace)            ! trace gas properties
      type(LambertianType)         ,intent(in) :: surfaceSimS(numSpectrBands)         ! surface properties simulation
      type(LambertianType)         ,intent(in) :: surfaceRetrS(numSpectrBands)        ! surface properties retrieval
      type(LambertianType)         ,intent(in) :: LambCloudSimS(numSpectrBands)       ! Lambertian cloud properties sim
      type(LambertianType)         ,intent(in) :: LambCloudRetrS(numSpectrBands)      ! Lambertian cloud properties retr
      type(cldAerFractionType)     ,intent(in) :: cldAerFractionSimS(numSpectrBands)  ! cld/aer fraction properties sim
      type(cldAerFractionType)     ,intent(in) :: cldAerFractionRetrS(numSpectrBands) ! cld/aer fraction properties retr
      type(cloudAerosolRTMgridType),intent(in) :: cloudAerosolRTMgridSimS             ! cld/aer properties simulation
      type(cloudAerosolRTMgridType),intent(in) :: cloudAerosolRTMgridRetrS            ! cld/aer properties retrieval
      type(mulOffsetType)          ,intent(in) :: mulOffsetSimS(numSpectrBands)       ! multiplicative offset radiance sim
      type(mulOffsetType)          ,intent(in) :: mulOffsetRetrS(numSpectrBands)      ! multiplicative offset radiance retr
      type(straylightType)         ,intent(in) :: strayLightSimS(numSpectrBands)      ! earth radiance stray light sim
      type(straylightType)         ,intent(in) :: strayLightRetrS(numSpectrBands)     ! earth radiance stray light retr
      type(RRS_RingType)           ,intent(in) :: RRS_RingSimS(numSpectrBands)        ! Ring spectra for simulation
      type(RRS_RingType)           ,intent(in) :: RRS_RingRetrS(numSpectrBands)       ! Ring spectra for retrieval
      type(retrType)               ,intent(in) :: retrS                               ! retrieval structure
      type(diagnosticType)         ,intent(in) :: diagnosticS                         ! diagnostic information
      integer                      ,intent(in) :: ncolumn                             ! # columns with subcolumns
      type(optPropRTMGridType)     ,intent(in) :: optPropRTMGridSimS, optPropRTMGridRetrS
      type(columnType)             ,intent(in) :: columnSimS(ncolumn), columnRetrS(ncolumn)

      ! local
      integer    :: istate, jstate, iband, index, itrace, ialt, iwave
      integer    :: isurfAlb, isurfEmission, icldAlb, imulOffset, istrayLight, icloudFraction
      integer    :: counterSurfaceAlbedo, counterSurfaceEmission, counterCloudAlbedo
      integer    :: counterMulOffset, counterStrayLight, countercolumnfit
      integer    :: counterWavelSimHR, counterWavelRetrHR, counterWavelSimInstr , counterWavelRetrInstr
      integer    :: counterAltSim, counterAltRetr, counterStateVectorSim, counterStateVectorRetr
      integer    :: counterCloudFraction, counterPressureSim, counterPressureRetr
      integer    :: counterAltWeightSim, counterAltWeightRetr
      integer    :: nsurfaceAlbedo, nsurfaceEmission, ncloudAlbedo, nmulOffset, nstrayLight, nCldAerFraction

      logical    :: first_time_profile, first_time_temp
      logical    :: additional_output_sim, additional_output_retr

      real(8)    :: factor
      real(8)    :: wavelengths(retrS%nstate)
      real(8)    :: correlation(retrS%nstate,retrS%nstate)
      real(8)    :: columns(3,retrS%ncolumnfit), precisionColumns(3,retrS%ncolumnfit)
      real(8)    :: temperature(gasPTRetrS%npressureNodes + 1,3)
      real(8)    :: temperature_precision(gasPTRetrS%npressureNodes + 1,3)
      real(8)    :: surfaceAlbedo    (3, sum(surfaceRetrS(:)%nAlbedo) )
      real(8)    :: precisionSurfAlb (3, sum(surfaceRetrS(:)%nAlbedo) )
      real(8)    :: wavelengthSurfAlb(   sum(surfaceRetrS(:)%nAlbedo) )
      real(8)    :: surfaceEmission       (3, sum(surfaceRetrS(:)%nEmission) )
      real(8)    :: precisionSurfEmission (3, sum(surfaceRetrS(:)%nEmission) )
      real(8)    :: wavelengthSurfEmission(   sum(surfaceRetrS(:)%nEmission) )
      real(8)    :: cloudAlbedo       (3, sum(LambCloudRetrS(:)%nAlbedo) )
      real(8)    :: precisionCloudAlb (3, sum(LambCloudRetrS(:)%nAlbedo) )
      real(8)    :: wavelengthCloudAlb(   sum(LambCloudRetrS(:)%nAlbedo) )
      real(8)    :: cldAerFraction          (3, sum(cldAerFractionRetrS(:)%nCldAerfraction) )
      real(8)    :: precisionCldAerFraction (3, sum(cldAerFractionRetrS(:)%nCldAerfraction) )
      real(8)    :: wavelengthCldAerFraction(   sum(cldAerFractionRetrS(:)%nCldAerfraction) )
      real(8)    :: mulOffset           (3, sum(mulOffsetRetrS(:)%nwavel) )
      real(8)    :: precisionMulOffset  (3, sum(mulOffsetRetrS(:)%nwavel) )
      real(8)    :: wavelengthMulOffset (   sum(mulOffsetRetrS(:)%nwavel) )
      real(8)    :: strayLight          (3, sum(strayLightRetrS(:)%nwavel) )
      real(8)    :: precisionStrayLight (3, sum(strayLightRetrS(:)%nwavel) )
      real(8)    :: wavelengthStrayLight(   sum(strayLightRetrS(:)%nwavel) )

      real(8), allocatable  :: sun_norm_radiance(:)
      real(8), allocatable  :: degree_lin_polarization(:)
      real(8), allocatable  :: x_array(:)
      real(8)               :: allBands(3), precisionAllBands(3), AAI(1), precisionAAI(1)
      real(8)               :: x_real(1)
      integer               :: x_int(1)

      character(LEN=50) :: legend(3), legend_precision(3)
      character(LEN=50) :: nameTraceGas(retrS%ncolumnfit), unit(retrS%ncolumnfit)
      character(LEN=50) :: nameTraceGas_state_vector(retrS%nstate)
      character(LEN=50) :: unit_state_vector(retrS%nstate)
      character(LEN=50) :: unit_covariance(retrS%nstate)

      character(LEN=1)  :: char_index

      character(LEN=50) :: groupName, name, keyword, namePressureGrid
      character(LEN=50) :: keywordList1, keywordList2, keywordList3, keywordList4
      character(LEN=50) :: keyword1, keyword2, keyword3, keyword4
      character(LEN=50) :: string_1, string_2, string_3, string_4
      character(LEN=50) :: unit_abs_Xsec



      real(8), parameter :: PI = 3.141592653589793d0
      real(8), parameter :: DUToCm2 = 2.68668d16

      integer, parameter :: maxNumAttribStateVector = 0

      integer  :: allocStatus, deallocStatus
      integer  :: sumallocStatus

      call initializeAsciiHDF
        call beginAttributes
          keyword = 'version_number_DISAMAR'
          call addAttribute(errS, keyword, DISAMAR_version_number)
          if (errorCheck(errS)) return

          if ( .not. controlSimS%simulationOnly ) then
            keyword = 'solar_zenith_angle_Retr'
            call addAttribute(errS, keyword, geometryRetrS%sza)
            if (errorCheck(errS)) return
            keyword = 'viewing_zenith_angle_Retr'
            call addAttribute(errS, keyword, geometryRetrS%vza)
            if (errorCheck(errS)) return
            keyword = 'azimuth_difference_Retr'
            call addAttribute(errS, keyword, geometryRetrS%dphi)
            if (errorCheck(errS)) return
            keyword = 'dimSVRetr'
            call addAttribute(errS, keyword, controlRetrS%dimSV)
            if (errorCheck(errS)) return
            keyword = 'number of iterations'
            call addAttribute(errS, keyword, retrS%numIterations)
            if (errorCheck(errS)) return
            keyword = 'solution_has_converged'
            if ( retrS%isConverged ) then
              call addAttribute(errS, keyword, .true.)
              if (errorCheck(errS)) return
            else
              call addAttribute(errS, keyword, .false.)
              if (errorCheck(errS)) return
            end if
            keyword = 'stopped_at_boundary'
            if ( retrS%isConvergedBoundary ) then
              call addAttribute(errS, keyword, .true.)
              if (errorCheck(errS)) return
            else
              call addAttribute(errS, keyword, .false.)
              if (errorCheck(errS)) return
            end if
            keyword = 'stateVectorConvThreshold'
            call addAttribute(errS, keyword, retrS%xConvThreshold)
            if (errorCheck(errS)) return
            keyword = 'stateVectorConv'
            call addAttribute(errS, keyword, retrS%xConv)
            if (errorCheck(errS)) return
            keyword = 'chi2_reflectance'
            call addAttribute(errS, keyword, retrS%chi2R)
            if (errorCheck(errS)) return
            keyword = 'chi2_state_vector'
            call addAttribute(errS, keyword, retrS%chi2x)
            if (errorCheck(errS)) return
            keyword = 'chi2'
            call addAttribute(errS, keyword, retrS%chi2)
            if (errorCheck(errS)) return
            keyword = 'rmse'
            call addAttribute(errS, keyword, retrS%rmse)
            if (errorCheck(errS)) return
            keyword = 'DFS'
            call addAttribute(errS, keyword, diagnosticS%DFS)
            if (errorCheck(errS)) return
            keyword = 'DFS_temperature'
            call addAttribute(errS, keyword, diagnosticS%DFSTemperature)
            if (errorCheck(errS)) return
          end if ! .not. controlSimS%simulationOnly
        call endAttributes

      ! test whether additional output is desired

      if ( controlSimS%writeHighResolutionRefl    .or.   &
           controlSimS%writeInstrResolutionRefl   .or.   &
           controlSimS%writeHighResReflAndDeriv   .or.   &
           controlSimS%writeInstrResReflAndDeriv  .or.   &
           controlSimS%writeSNR                   .or.   &
           controlSimS%writeContributionRadiance  .or.   &
           controlSimS%writeAltResolvedAMF        .or.   &
           controlSimS%writeAbsorptionXsec        .or.   &
           controlSimS%writeInternalFieldUpHR     .or.   &
           controlSimS%writeInternalFieldUp       .or.   &
           controlSimS%writeInternalFieldDownHR   .or.   &
           controlSimS%writeInternalFieldDown     .or.   &
           controlSimS%writeRing_spectra          .or.   &
           controlSimS%writeDiffRing_spectra      .or.   &
           controlSimS%writeFilling_in_spectra ) then

        additional_output_sim = .true.
      else
        additional_output_sim = .false.
      end if

      if ( controlRetrS%writeHighResolutionRefl    .or.   &
           controlRetrS%writeInstrResolutionRefl   .or.   &
           controlRetrS%writeHighResReflAndDeriv   .or.   &
           controlRetrS%writeInstrResReflAndDeriv  .or.   &
           controlRetrS%writeContributionRadiance  .or.   &
           controlRetrS%writeAltResolvedAMF        .or.   &
           controlRetrS%writeAbsorptionXsec        .or.   &
           controlRetrS%writeInternalFieldUpHR     .or.   &
           controlRetrS%writeInternalFieldUp       .or.   &
           controlRetrS%writeInternalFieldDownHR   .or.   &
           controlRetrS%writeInternalFieldDown     .or.   &
           controlRetrS%writeRing_spectra          .or.   &
           controlRetrS%writeDiffRing_spectra      .or.   &
           controlRetrS%writeFilling_in_spectra ) then

        additional_output_retr = .true.
      else
        additional_output_retr = .false.
      end if

      if ( additional_output_sim .and. controlSimS%useReflectanceFromFile ) &
           additional_output_sim = .false.

      if ( additional_output_retr .and. controlSimS%simulationOnly ) &
           additional_output_retr = .false.

      ! initialization
      counterWavelSimHR      = 0
      counterWavelRetrHR     = 0
      counterWavelSimInstr   = 0
      counterWavelRetrInstr  = 0
      counterAltSim          = 0
      counterAltRetr         = 0
      counterPressureSim     = 0
      counterPressureRetr    = 0
      counterAltWeightSim    = 0
      counterAltWeightRetr   = 0
      counterStateVectorSim  = 0
      counterStateVectorRetr = 0
      counterCloudFraction   = 0
      counterMulOffset       = 0
      counterStrayLight      = 0
      countercolumnfit       = 0
      counterSurfaceAlbedo   = 0
      counterSurfaceEmission = 0
      counterCloudAlbedo     = 0

        groupName = 'specifications_sim'
        call beginGroup(errS, groupName)
        if (errorCheck(errS)) return

          name = 'solar_zenith_angle'
          x_real(1)    = geometrySimS%sza
          keyword1     = 'unit'
          string_1     = 'degrees'
          call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          name = 'solar_azimuth_angle'
          x_real(1)    = geometrySimS%s_azimuth
          keyword1     = 'unit'
          string_1     = 'degrees'
          call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          name = 'viewing_nadir_angle'
          x_real(1)    = geometrySimS%vza
          keyword1     = 'unit'
          string_1     = 'degrees'
          call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          name = 'viewing_azimuth_angle'
          x_real(1)    = geometrySimS%v_azimuth
          keyword1     = 'unit'
          string_1     = 'degrees'
          call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          name = 'azimuth_difference_used_in_RTM'
          x_real(1)    = geometrySimS%dphi
          keyword1     = 'unit'
          string_1     = 'degrees'
          call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          name = 'surface_pressure'
          x_real(1)    = surfaceSimS(1)%pressure
          keyword1     = 'unit'
          string_1     = 'hPa'
          call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          name = 'top_of_atmosphere_pressure'
          x_real(1) = cloudAerosolRTMgridSimS %intervalBounds_P(cloudAerosolRTMgridSimS%ninterval)
          keyword1     = 'unit'
          string_1     = 'hPa'
          call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          name = 'dimension_Stokes_vector'
          x_int(1) =  controlSimS%dimSV
          keyword1     = 'unit'
          string_1     = '  '
          call writeInt(errS, name, x1D = x_int,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          if ( cloudAerosolRTMgridSimS%cloudPresent) then
            x_int(1) = 1
          else
            x_int(1) = 0
          end if
          name = 'cloud_present'
          keyword1     = 'unit'
          string_1     = '  '
          call writeInt(errS, name, x1D = x_int,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          ! write temperature profile
          ! first write pressure levels where temperature is specified
          name = 'pressure_levels_for_temperature'
          keyword1     = 'unit'
          string_1     = 'hPa'
          call writeReal(errS, name, x1D = gasPTSimS%pressure,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return
          name = 'altitude_levels_for_temperature'
          keyword1     = 'unit'
          string_1     = 'km'
          call writeReal(errS, name, x1D = gasPTSimS%alt,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return
          name = 'temperature'
          keyword1     = 'unit'
          string_1     = 'Kelvin'
          call writeReal(errS, name, x1D = gasPTSimS%temperature,  keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return

          do iband = 1, numSpectrBands

            name = 'use_rotational_Raman_scattering_in_band_'//achar(48 + iband)
            if ( RRS_RingSimS(iband)%useRRS ) then
              x_int(1) = 1
            else
              x_int(1) = 0
            end if
            keyword1     = 'unit'
            string_1     = '  '
            call writeInt(errS, name, x1D = x_int,  keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return

            ! write slit index solar irradiance
            name = 'index_slit_function_solar_irradiance_band_'//achar(48 + iband)
            keyword1 = 'unit'
            string_1 = '  '
            x_int(1) = solarIrradianceSimS(iband)%slitFunctionSpecsS%slitIndex
            call writeInt(errS, name, x1D = x_int,  keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return

            ! write slit index earth radiance
            name = 'index_slit_function_earth_radiance_band_'//achar(48 + iband)
            keyword1 = 'unit'
            string_1 = '  '
            x_int(1) = earthRadianceSimS(iband)%slitFunctionSpecsS%slitIndex
            call writeInt(errS, name, x1D = x_int,  keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return

            ! write FWHM solar irradiance
            name = 'FWHM_solar_irradiance_band_'//achar(48 + iband)
            keyword1  = 'unit'
            string_1  = 'nm'
            x_real(1) = solarIrradianceSimS(iband)%slitFunctionSpecsS%FWHM
            call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return

            ! write FWHM earth radiance
            name = 'FWHM_earth_radiance_band_'//achar(48 + iband)
            keyword1  = 'unit'
            string_1  = 'nm'
            x_real(1) = earthRadianceSimS(iband)%slitFunctionSpecsS%FWHM
            call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return

            ! calculate surface albedo on instrument grid
            allocate(x_array( wavelInstrRadSimS(iband)%nwavel ) )

            ! calculate surface albedo on instrument grid
            do iwave = 1, wavelInstrRadSimS(iband)%nwavel
              if ( cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then
                x_array(iwave) = cloudAerosolRTMgridSimS%albedoLambSurfAllBands
              else
                x_array(iwave) = polyInt(errS, surfaceSimS(iband)%wavelAlbedo, surfaceSimS(iband)%albedo, &
                                         wavelInstrRadSimS(iband)%wavel(iwave) )
              end if
            end do ! iwave
            name = 'surface_albedo_sim_band_'//achar(48 + iband)
            keyword1     = 'unit'
            string_1     = '  '
            keyword2     = 'wavelength'
            string_2     = '/specifications_sim/wavelength_radiance_band_'//achar(48 + iband)
            call writeReal(errS, name, x1D = x_array(:),                               &
                           keyword1 = keyword1, string_1 = string_1,             &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return

            if ( cloudAerosolRTMgridSimS%cloudPresent) then

              ! calculate cloud fraction on instrument grid
              do iwave = 1, wavelInstrRadSimS(iband)%nwavel
                if ( cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then
                  x_array(iwave) = cloudAerosolRTMgridSimS%cldAerFractionAllBands
                else
                  x_array(iwave) = polyInt(errS, cldAerFractionSimS(iband)%wavelCldAerFraction,  &
                         cldAerFractionSimS(iband)%cldAerFraction, wavelInstrRadSimS(iband)%wavel(iwave) )
                end if
              end do ! iwave
              name = 'cloud_fraction_sim_band_'//achar(48 + iband)
              keyword1     = 'unit'
              string_1     = '  '
              keyword2     = 'wavelength'
              string_2     = '/specifications_sim/wavelength_radiance_band_'//achar(48 + iband)
              call writeReal(errS, name, x1D = x_array(:),                               &
                             keyword1 = keyword1, string_1 = string_1,             &
                             keyword2 = keyword2, string_2 = string_2)
              if (errorCheck(errS)) return

              if ( cloudAerosolRTMgridSimS%useLambertianCloud ) then

                ! write cloud pressure which is the same for all bands
                if ( iband == 1) then
                  name = 'pressure_Lambertian_cloud'
                  x_real(1) = cloudAerosolRTMgridSimS %intervalBounds_P(cloudAerosolRTMgridSimS%numIntervalFit)
                  keyword1     = 'unit'
                  string_1     = 'hPa'
                  call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
                  if (errorCheck(errS)) return
                end if

                ! calculate cloud albedo on instrument grid
                do iwave = 1, wavelInstrRadSimS(iband)%nwavel
                  if ( cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then
                    x_array(iwave) = cloudAerosolRTMgridSimS%albedoLambCldAllBands
                  else
                    x_array(iwave) = polyInt(errS, LambCloudSimS(iband)%wavelAlbedo,  &
                           LambCloudSimS(iband)%albedo, wavelInstrRadSimS(iband)%wavel(iwave) )
                  end if
                end do ! iwave
                name = 'cloud_albedo_sim_band_'//achar(48 + iband)
                keyword1     = 'unit'
                string_1     = '  '
                keyword2     = 'wavelength'
                string_2     = '/specifications_sim/wavelength_radiance_band_'//achar(48 + iband)
                call writeReal(errS, name, x1D = x_array(:),                               &
                               keyword1 = keyword1, string_1 = string_1,             &
                               keyword2 = keyword2, string_2 = string_2)
                if (errorCheck(errS)) return
              else
                ! write cloud pressure levels which are the same for all spectral bands
                if ( iband == 1 ) then
                  name = 'pressure_top_scattering_cloud'
                  x_real(1) = cloudAerosolRTMgridSimS %intervalBounds_P(cloudAerosolRTMgridSimS%numIntervalFit)
                  keyword1     = 'unit'
                  string_1     = 'hPa'
                  call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
                  if (errorCheck(errS)) return
                  name = 'pressure_base_scattering_cloud'
                  x_real(1) = cloudAerosolRTMgridSimS %intervalBounds_P(cloudAerosolRTMgridSimS%numIntervalFit - 1)
                  keyword1     = 'unit'
                  string_1     = 'hPa'
                  call writeReal(errS, name, x1D = x_real,  keyword1 = keyword1, string_1 = string_1)
                  if (errorCheck(errS)) return
                end if
               if ( cloudAerosolRTMgridSimS%useHGScatCld ) then
                   ! write parameters for HG scattering
                 end if
                 if ( cloudAerosolRTMgridSimS%useHGScatCld ) then
                   ! write parameters for Mie scattering
                 end if
              end if ! cloudAerosolRTMgridSimS%useLambertianCloud

            end if ! cloudAerosolRTMgridSimS%cloudPresent

            deallocate( x_array )

          end do ! iband

          name = 'wavelength_irradiance'
          call writeInstrWavelGrid(errS,  numSpectrBands, wavelInstrIrrSimS, name )
          if (errorCheck(errS)) return
          name = 'wavelength_radiance'
          call writeInstrWavelGrid(errS,  numSpectrBands, wavelInstrRadSimS, name )
          if (errorCheck(errS)) return

        call endGroup

        groupName = 'radiance_and_irradiance'
        call beginGroup(errS, groupName)
        if (errorCheck(errS)) return

          ! write instrument resolution solar irradiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'solar_irradiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_nm_per_s'
            keyword2     = 'wavelength'
            string_2     = '/specifications_sim/wavelength_irradiance_band_'//char_index
            keyword3     = 'remark'
            string_3     = 'shot noise and offsets have been added'
            call writeReal(errS, name, x1D = solarIrradianceSimS(iband)%solIrrMeas(:), &
                           keyword1 = keyword1, string_1 = string_1,             &
                           keyword2 = keyword2, string_2 = string_2,             &
                           keyword3 = keyword3, string_3 = string_3)
            if (errorCheck(errS)) return
          end do ! iband

          ! write instrument resolution earth radiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'earth_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons per cm2 per nm per sr'
            keyword2     = 'wavelength'
            string_2     = '/specifications_sim/wavelength_radiance_band_'//char_index
            keyword3     = 'remark'
            string_3     = 'shot noise and offsets have been added'
            call writeReal(errS, name, x1D = earthRadianceSimS(iband)%rad_meas(1,:),  &
                           keyword1 = keyword1, string_1 = string_1,            &
                           keyword2 = keyword2, string_2 = string_2,            &
                           keyword3 = keyword3, string_3 = string_3)
            if (errorCheck(errS)) return
          end do ! iband

          ! write instrument resolution radRing spectrum.
          do iband = 1, numSpectrBands
            if (associated(RRS_RingSimS(iband)%radRing)) then
              char_index = achar(48 + iband)
              name = 'rad_ring_band_'//char_index
              keyword1     = 'unit'
              string_1     = 'photons per cm2 per nm per sr'
              keyword2     = 'wavelength'
              string_2     = '/specifications_sim/wavelength_radiance_band_'//char_index
              keyword3     = 'remark'
              string_3     = 'radRing (IR) is the solar irradiance convoluted with the RRS lines and slit function'
              call writeReal(errS, name, x1D = RRS_RingSimS(iband)%radRing(:),  &
                             keyword1 = keyword1, string_1 = string_1,          &
                             keyword2 = keyword2, string_2 = string_2,          &
                             keyword3 = keyword3, string_3 = string_3)
              if (errorCheck(errS)) return
            endif
          end do ! iband

          ! write instrument resolution differential Ring spectrum.
          do iband = 1, numSpectrBands
            if (associated(RRS_RingSimS(iband)%diffRing)) then
              char_index = achar(48 + iband)
              name = 'diff_ring_band_'//char_index
              keyword1     = 'unit'
              string_1     = 'photons per cm2 per nm per sr'
              keyword2     = 'wavelength'
              string_2     = '/specifications_sim/wavelength_radiance_band_'//char_index
              keyword3     = 'remark'
              string_3     = &
             'diffRing (IR) is the solar irradiance convoluted with RRS lines and slit function, with a polynomial subtracted'
              call writeReal(errS, name, x1D = RRS_RingSimS(iband)%diffRing(:),  &
                             keyword1 = keyword1, string_1 = string_1,            &
                             keyword2 = keyword2, string_2 = string_2,            &
                             keyword3 = keyword3, string_3 = string_3)
              if (errorCheck(errS)) return
            endif
          end do ! iband

        call endGroup

      if ( additional_output_sim ) then

        groupName = 'additional_output_sim'
        call beginGroup(errS, groupName)
        if (errorCheck(errS)) return

        ! write additinal output for simulation
        if ( controlSimS%writeHighResolutionRefl) then
          if ( counterWavelSimHR < 1 ) then
            call writeHRwavelGrid(errS,  numSpectrBands, wavelHRSimS)
            if (errorCheck(errS)) return
            counterWavelSimHR = 1
          end if

          ! write high resolution solar irradiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_solar_irradiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_nm_per_s'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_sim/HR_wavelength_band_'//char_index
            call writeReal(errS, name, x1D = solarIrradianceSimS(iband)%solIrrMR(:), &
                           keyword1 = keyword1, string_1 = string_1,           &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband
          ! write high resolution earth radiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_earth_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_nm_per_s_per_sr'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_sim/HR_wavelength_band_'//char_index
            call writeReal(errS, name, x1D = earthRadianceSimS(iband)%rad_HR(1,:),   &
                           keyword1 = keyword1, string_1 = string_1,           &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband
          ! write high resolution sun_normalized_radiance simulation
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_sun_normalized_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'per_sr'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_sim/HR_wavelength_band_'//char_index
            allocate( sun_norm_radiance(earthRadianceSimS(iband)%nwavelHR), STAT = allocStatus )
            if ( allocStatus /= 0 ) then
              call logDebug('FATAL ERROR: allocation failed')
              call logDebug('for sun_norm_radiance - high resolution - simulation')
              call logDebugI('for iband = ', iband)
              call logDebug('in subroutine print_AsciiHDF')
              call logDebug('in module writeModule')
              call mystop(errS, 'stopped because allocation failed')
              if (errorCheck(errS)) return
            end if
            sun_norm_radiance(:) = earthRadianceSimS(iband)%rad_HR(1,:) &
                                 / solarIrradianceSimS(iband)%solIrrMR(:)
            call writeReal(errS, name, x1D = sun_norm_radiance, keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
            deallocate( sun_norm_radiance, STAT = deallocStatus )
            if ( deallocStatus /= 0 ) then
              call logDebug('FATAL ERROR: deallocation failed')
              call logDebug('for sun_norm_radiance - high resolution - simulation')
              call logDebugI('for iband = ', iband)
              call logDebug('in subroutine print_AsciiHDF')
              call logDebug('in module writeModule')
              call mystop(errS, 'stopped because deallocation failed')
              if (errorCheck(errS)) return
            end if
          end do ! iband

          ! write degree of linear polarization on high resolution wavelength grid - simulation
          if ( controlSimS%dimSV > 2 ) then
            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'HR_degree_lin_polarization_band_'//char_index
              keyword1     = 'unit'
              string_1     = 'percent'
              keyword2     = 'wavelength'
              string_2     = '/additional_output_sim/HR_wavelength_band_'//char_index
              keyword3     = 'remark'
              string_3     = 'degree_lin_polarization = 100 * sqrt(Q*Q+U*U) / I'
              allocate( degree_lin_polarization(earthRadianceSimS(iband)%nwavelHR), STAT = allocStatus )
              if ( allocStatus /= 0 ) then
                call logDebug('FATAL ERROR: allocation failed')
                call logDebug('for degree_lin_polarization - high resolution - simulation')
                call logDebugI('for iband = ', iband)
                call logDebug('in subroutine print_AsciiHDF')
                call logDebug('in module writeModule')
                call mystop(errS, 'stopped because allocation failed')
                if (errorCheck(errS)) return
              end if
              degree_lin_polarization(:) = 100.0d0 * sqrt(   earthRadianceSimS(iband)%rad_HR(2,:)**2    &
                                                           + earthRadianceSimS(iband)%rad_HR(3,:)**2  ) &
                                                           / earthRadianceSimS(iband)%rad_HR(1,:)
              call writeReal(errS, name, x1D = degree_lin_polarization, keyword1 = keyword1, string_1 = string_1, &
                             keyword2 = keyword2, string_2 = string_2,                                      &
                             keyword3 = keyword3, string_3 = string_3)
              if (errorCheck(errS)) return
              deallocate( degree_lin_polarization, STAT = deallocStatus )
              if ( deallocStatus /= 0 ) then
                call logDebug('FATAL ERROR: deallocation failed')
                call logDebug('for degree_lin_polarization - high resolution - simulation')
                call logDebugI('for iband = ', iband)
                call logDebug('in subroutine print_AsciiHDF')
                call logDebug('in module writeModule')
                call mystop(errS, 'stopped because deallocation failed')
                if (errorCheck(errS)) return
              end if
            end do ! iband
          end if ! controlSimS%dimSV > 2
        end if ! controlSimS%writeHighResolutionRefl

        if ( controlSimS%writeInstrResolutionRefl ) then

          ! write instrument resolution solar irradiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'solar_irradiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_nm_per_s'
            keyword2     = 'wavelength'
            string_2     = '/specifications_sim/wavelength_irradiance_band_'//char_index
            keyword3     = 'remark'
            string_3     = 'shot noise and offsets have been added'
            call writeReal(errS, name, x1D = solarIrradianceSimS(iband)%solIrrMeas(:), &
                           keyword1 = keyword1, string_1 = string_1,             &
                           keyword2 = keyword2, string_2 = string_2,             &
                           keyword3 = keyword3, string_3 = string_3)
            if (errorCheck(errS)) return
          end do ! iband

          ! write instrument resolution earth radiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'earth_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons per cm2 per nm per sr'
            keyword2     = 'wavelength'
            string_2     = '/specifications_sim/wavelength_radiance_band_'//char_index
            keyword3     = 'remark'
            string_3     = 'shot noise and offsets have been added'
            call writeReal(errS, name, x1D = earthRadianceSimS(iband)%rad_meas(1,:),  &
                           keyword1 = keyword1, string_1 = string_1,            &
                           keyword2 = keyword2, string_2 = string_2,            &
                           keyword3 = keyword3, string_3 = string_3)
            if (errorCheck(errS)) return
          end do ! iband

          ! write instrument resolution sun_normalized_radiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'sun_normalized_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'per_sr'
            keyword2     = 'wavelength'
            string_2     = '/specifications_sim/wavelength_radiance_band_'//char_index
            keyword3     = 'remark'
            string_3     = 'shot noise and offsets have been added'
            allocate( sun_norm_radiance(earthRadianceSimS(iband)%nwavel), STAT = allocStatus )
            if ( allocStatus /= 0 ) then
              call logDebug('FATAL ERROR: allocation failed')
              call logDebug('for sun_norm_radiance - instrument resolution - simulation')
              call logDebugI('for iband = ', iband)
              call logDebug('in subroutine print_AsciiHDF')
              call logDebug('in module writeModule')
              call mystop(errS, 'stopped because allocation failed')
              if (errorCheck(errS)) return
            end if
            sun_norm_radiance = earthRadianceSimS(iband)%rad_meas(1,:) / solarIrradianceSimS(iband)%solIrrMeas
            call writeReal(errS, name, x1D = sun_norm_radiance, keyword1 = keyword1, string_1 = string_1,       &
                           keyword2 = keyword2, string_2 = string_2,                                      &
                           keyword3 = keyword3, string_3 = string_3)
            if (errorCheck(errS)) return
            deallocate( sun_norm_radiance, STAT = deallocStatus )
            if ( deallocStatus /= 0 ) then
              call logDebug('FATAL ERROR: deallocation failed')
              call logDebug('for sun_norm_radiance - instrument resolution - simulation')
              call logDebugI('for iband = ', iband)
              call logDebug('in subroutine print_AsciiHDF')
              call logDebug('in module writeModule')
              call mystop(errS, 'stopped because deallocation failed')
              if (errorCheck(errS)) return
            end if
          end do ! iband

          if ( controlSimS%dimSV > 2 ) then
            ! write instrument resolution degree linear polarization
            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'degree_lin_polarization_band_'//char_index
              keyword1     = 'unit'
              string_1     = 'percent'
              keyword2     = 'wavelength'
              string_2     = '/specifications_sim/wavelength_radiance_band_'//char_index
              keyword3     = 'remark'
              string_3     = 'shot noise and offsets have NOT been added'
              keyword4     = 'remark'
              string_4     = 'degree_lin_polarization = 100 * sqrt(Q*Q+U*U) / I'
              allocate( degree_lin_polarization(earthRadianceSimS(iband)%nwavel), STAT = allocStatus )
              if ( allocStatus /= 0 ) then
                call logDebug('FATAL ERROR: allocation failed')
                call logDebug('for degree_lin_polarization - instrument resolution - simulation')
                call logDebugI('for iband = ', iband)
                call logDebug('in subroutine print_AsciiHDF')
                call logDebug('in module writeModule')
                call mystop(errS, 'stopped because allocation failed')
                if (errorCheck(errS)) return
              end if
              degree_lin_polarization(:) = 100.0d0 * sqrt(   earthRadianceSimS(iband)%rad(2,:)**2    &
                                                           + earthRadianceSimS(iband)%rad(3,:)**2  ) &
                                                           / earthRadianceSimS(iband)%rad(1,:)
              call writeReal(errS, name, x1D = degree_lin_polarization, keyword1 = keyword1, string_1 = string_1, &
                             keyword2 = keyword2, string_2 = string_2,                                      &
                             keyword3 = keyword3, string_3 = string_3,                                      &
                             keyword4 = keyword4, string_4 = string_4)
              if (errorCheck(errS)) return
              deallocate( degree_lin_polarization, STAT = deallocStatus )
              if ( deallocStatus /= 0 ) then
                call logDebug('FATAL ERROR: deallocation failed')
                call logDebug('for degree_lin_polarization - instrument resolution - simulation')
                call logDebugI('for iband = ', iband)
                call logDebug('in subroutine print_AsciiHDF')
                call logDebug('in module writeModule')
                call mystop(errS, 'stopped because deallocation failed')
                if (errorCheck(errS)) return
              end if
            end do ! iband

          end if ! controlSimS%dimSV > 2

        end if ! controlSimS%writeInstrResolutionRefl

        if ( controlSimS%writeSNR) then

          ! write SNR for the reference spectrum
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'SNR_refSpec_'//char_index
            keyword1     = 'unit'
            string_1     = '-'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_sim/wavelength_band_'//char_index
            call writeReal(errS, name, x1D = earthRadianceSimS(iband)%SNrefSpec(:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband

          ! write SNR for the radiance spectrum
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'SNR_radiance_'//char_index
            keyword1     = 'unit'
            string_1     = '-'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_sim/wavelength_band_'//char_index
            call writeReal(errS, name, x1D = earthRadianceSimS(iband)%SN(:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband

          ! write SNR for the reflectance spectrum
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'SNR_reflectance_'//char_index
            keyword1     = 'unit'
            string_1     = '-'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_sim/wavelength_band_'//char_index
            call writeReal(errS, name, x1D = earthRadianceSimS(iband)%SNrefl(:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlSimS%writeSNR


        if ( controlSimS%writeContributionRadiance) then

          ! write altitudes
          if ( counterAltSim < 1 ) then
            name = 'altitudes'
            keyword1     = 'unit'
            string_1     = 'km'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMaltitude, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterAltSim = counterAltSim + 1
          end if

          ! write pressure levels
          if ( counterPressureSim < 1 ) then
            name = 'pressures'
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMpressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterPressureSim = counterPressureSim + 1
          end if

          ! write path radiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'earth_path_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_nm_per_sr_per_sec_per_km'
            keyword2     = 'altitudes'
            string_2     = '/additional_output_sim/altitudes'
            keyword3     = 'pressures'
            string_3     = '/additional_output_sim/pressures'
            keyword4     = 'wavelength'
            string_4     = '/additional_output_sim/wavelength_band_'//char_index
            call writeReal(errS, name, x2D = earthRadianceSimS(iband)%contribRad(1,:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlSimS%writeContributionRadiance

        if ( controlSimS%writeAltResolvedAMF) then

          if ( counterWavelSimHR < 1 ) then
            call writeHRwavelGrid(errS,  numSpectrBands, wavelHRSimS)
            if (errorCheck(errS)) return
            counterWavelSimHR = 1
          end if

          ! write altitudes
          if ( counterAltSim < 1 ) then
            name = 'altitudes'
            keyword1     = 'unit'
            string_1     = 'km'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMaltitude, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterAltSim = counterAltSim + 1
          end if

          ! write weights corresponding to altitudes
          if ( counterAltWeightSim < 1 ) then
            name = 'weights_for_integration_over_altitudes'
            keyword1     = 'unit'
            string_1     = '-'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMweight, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterAltWeightSim = counterAltWeightSim + 1
          end if

          if ( counterPressureSim < 1 ) then
            name = 'pressures'
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMpressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterPressureSim = counterPressureSim + 1
          end if

          ! write reflectance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_reflectance_clear_band_'//char_index
            keyword1     = 'unit'
            string_1     = ' '
            keyword2     = 'wavelength'
            string_2     = '/additional_output_sim/HR_wavelength_band_'//char_index
            call writeReal(errS, name, x1D = reflDerivHRSimS(iband)%refl_clr(1,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
            if ( cloudAerosolRTMgridSimS%cloudPresent ) then
              char_index = achar(48 + iband)
              name = 'HR_reflectance_cloud_band_'//char_index
              keyword1     = 'unit'
              string_1     = ' '
              keyword2     = 'wavelength'
              string_2     = '/additional_output_sim/HR_wavelength_band_'//char_index
              call writeReal(errS, name, x1D = reflDerivHRSimS(iband)%refl_cld(1,:), &
                             keyword1 = keyword1, string_1 = string_1, &
                             keyword2 = keyword2, string_2 = string_2)
              if (errorCheck(errS)) return
            end if ! cloudAerosolRTMgridSimS%cloudPresent
          end do ! iband

          ! write altitude resolved air mass factor for absorption
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_alt_resolved_AMF_abs_band_clear_'//char_index
            keyword1     = 'unit'
            string_1     = ' '
            keyword2     = 'altitudes'
            string_2     = '/additional_output_sim/altitudes'
            keyword3     = 'pressures'
            string_3     = '/additional_output_sim/pressures'
            keyword4     = 'wavelength'
            string_4     = '/additional_output_sim/HR_wavelength_band_'//char_index
            call writeReal(errS, name, x2D = reflDerivHRSimS(iband)%altResAMFabs_clr(:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband
          if ( cloudAerosolRTMgridSimS%cloudPresent ) then
            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'HR_alt_resolved_AMF_abs_band_cloud_'//char_index
              keyword1     = 'unit'
              string_1     = ' '
              keyword2     = 'altitudes'
              string_2     = '/additional_output_sim/altitudes'
              keyword3     = 'pressures'
              string_3     = '/additional_output_sim/pressures'
              keyword4     = 'wavelength'
              string_4     = '/additional_output_sim/HR_wavelength_band_'//char_index
              call writeReal(errS, name, x2D = reflDerivHRSimS(iband)%altResAMFabs_cld(:,:), &
                             keyword1 = keyword1, string_1 = string_1, &
                             keyword2 = keyword2, string_2 = string_2, &
                             keyword3 = keyword3, string_3 = string_3, &
                             keyword4 = keyword4, string_4 = string_4)
              if (errorCheck(errS)) return
             end do ! iband
          end if ! cloudAerosolRTMgridSimS%cloudPresent

          ! write altitude resolved air mass factor for aerosol scattering
          if ( cloudAerosolRTMgridSimS%aerosolPresent ) then
            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'HR_alt_resolved_AMF_sca_aer_band_clear_'//char_index
              keyword1     = 'unit'
              string_1     = ' '
              keyword2     = 'altitudes'
              string_2     = '/additional_output_sim/altitudes'
              keyword3     = 'pressures'
              string_3     = '/additional_output_sim/pressures'
              keyword4     = 'wavelength'
              string_4     = '/additional_output_sim/HR_wavelength_band_'//char_index
              call writeReal(errS, name, x2D = reflDerivHRSimS(iband)%altResAMFscaAer_clr(:,:), &
                             keyword1 = keyword1, string_1 = string_1, &
                             keyword2 = keyword2, string_2 = string_2, &
                             keyword3 = keyword3, string_3 = string_3, &
                             keyword4 = keyword4, string_4 = string_4)
              if (errorCheck(errS)) return
            end do ! iband

            if ( cloudAerosolRTMgridSimS%cloudPresent ) then
              do iband = 1, numSpectrBands
                char_index = achar(48 + iband)
                name = 'HR_alt_resolved_AMF_sca_aer_band_cloud_'//char_index
                keyword1     = 'unit'
                string_1     = ' '
                keyword2     = 'altitudes'
                string_2     = '/additional_output_sim/altitudes'
                keyword3     = 'pressures'
                string_3     = '/additional_output_sim/pressures'
                keyword4     = 'wavelength'
                string_4     = '/additional_output_sim/HR_wavelength_band_'//char_index
                call writeReal(errS, name, x2D = reflDerivHRSimS(iband)%altResAMFscaAer_cld(:,:), &
                               keyword1 = keyword1, string_1 = string_1, &
                               keyword2 = keyword2, string_2 = string_2, &
                               keyword3 = keyword3, string_3 = string_3, &
                               keyword4 = keyword4, string_4 = string_4)
                if (errorCheck(errS)) return
              end do ! iband
            end if ! cloudAerosolRTMgridSimS%cloudPresent
          end if ! cloudAerosolRTMgridSimS%aerosolPresent

        end if ! controlSimS%writeAltResolvedAMF

        if ( controlSimS%writeAbsorptionXsec ) then

          if ( counterWavelSimHR < 1 ) then
            call writeHRwavelGrid(errS,  numSpectrBands, wavelHRSimS)
            if (errorCheck(errS)) return
            counterWavelSimHR = 1
          end if

          ! write the pressure grids for the different absorbing gases
          ! and the absorption cross section
          do itrace = 1, nTrace

            ! write pressure grid for the trace gas involved
            namePressureGrid = 'pressuresGrid_for_'//trim(traceGasSimS(itrace)%nameTraceGas )
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, namePressureGrid, x1D = traceGasSimS(itrace)%pressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return

            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'absorptionXsecBand_'//char_index//'_for_'//trim(traceGasSimS(itrace)%nameTraceGas )
              keyword1     = 'unit'
              if ( trim(traceGasSimS(itrace)%nameTraceGas) == 'O2-O2' ) then
                unit_abs_Xsec = 'cm^5 / molecule^2'
              else
                unit_abs_Xsec = 'cm^2 / molecule'
              end if
              string_1     = unit_abs_Xsec
              keyword2     = 'pressureGrid'
              string_2     = '/additional_output_sim/'//trim(namePressureGrid)
              keyword3     = 'wavelength'
              string_3     = '/additional_output_sim/HR_wavelength_band_'//char_index
              call writeReal(errS, name, x2D = XsecHRSimS(iband,itrace)%Xsec(:,:), &
                             keyword1 = keyword1, string_1 = string_1,  &
                             keyword2 = keyword2, string_2 = string_2,  &
                             keyword3 = keyword3, string_3 = string_3)
              if (errorCheck(errS)) return
            end do ! iband
          end do ! itrace

        end if ! controlSimS%writeAbsorptionXsec

        if ( controlSimS%writeHighResReflAndDeriv) then

          if ( counterWavelSimHR < 1 ) then
            call writeHRwavelGrid(errS,  numSpectrBands, wavelHRSimS)
            if (errorCheck(errS)) return
            counterWavelSimHR = 1
          end if

          if ( counterStateVectorSim < 1 ) then
            ! write identifying labels for the state vecotor elements
            name = 'state_vector_elements'
            call writeString(errS, name, x1D = retrS%codeFitParameters(1:retrS%nstate) )
            if (errorCheck(errS)) return
            counterStateVectorSim = counterStateVectorSim + 1
          end if

          ! write derivatives
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_derivatives_sim_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'depends on state vector element'
            keyword2     = 'legend'
            string_2     = '/additional_output_sim/state_vector_elements'
            keyword3     = 'wavelengths'
            string_3     = '/additional_output_sim/HR_wavelength_band_'//char_index
            keyword4     = 'remark'
            string_4     = 'dR/dx_i; R reflectance x_i state vector element'
            call writeReal(errS, name, x2D = reflDerivHRSimS(iband)%K_vmr(:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlSimS%writeHighResReflAndDeriv

        if ( controlSimS%writeInstrResReflAndDeriv) then

          if ( counterStateVectorSim < 1 ) then
            ! write identifying labels for the state vecotor elements
            name = 'state_vector_elements'
            call writeString(errS, name, x1D = retrS%codeFitParameters(1:retrS%nstate) )
            if (errorCheck(errS)) return
            counterStateVectorSim = counterStateVectorSim + 1
          end if

          ! write derivatives for radiance on instrument spectral grid
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'derivatives_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'depends on state vector element'
            keyword2     = 'legend'
            string_2     = '/additional_output_sim/state_vector_elements'
            keyword3     = 'wavelengths'
            string_3     = '/additional_output_sim/wavelength_band_'//char_index
            keyword4     = 'remark'
            string_4     = 'dI/dx_i; I radiance x_i state vector element'
            call writeReal(errS, name, x2D = earthRadianceSimS(iband)%K_lnvmr(:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

          ! write derivatives for reflectance (not sun-normalized) on instrument spectral grid
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'derivatives_reflectance_sim_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'depends on state vector element'
            keyword2     = 'legend'
            string_2     = '/additional_output_sim/state_vector_elements'
            keyword3     = 'wavelengths'
            string_3     = '/additional_output_sim/wavelength_band_'//char_index
            keyword4     = 'remark'
            string_4     = 'dR/dx_i; R reflectance x_i state vector element'

            ! overwrite the values in earthRadianceSimS(iband)%K_lnvmr(:,:)
            do istate = 1, retrS%nstate
              earthRadianceSimS(iband)%K_lnvmr(:,istate) = PI  / geometrySimS%u0 &
               * earthRadianceSimS(iband)%K_lnvmr(:,istate) / solarIrradianceSimS(iband)%solIrr(:)
            end do

            call writeReal(errS, name, x2D = earthRadianceSimS(iband)%K_lnvmr(:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlSimS%writeInstrResReflAndDeriv

        ! write high resolution internal radiation field up
        if (controlSimS%writeInternalFieldUpHR) then

          if ( counterWavelSimHR < 1 ) then
            call writeHRwavelGrid(errS,  numSpectrBands, wavelHRSimS)
            if (errorCheck(errS)) return
            counterWavelSimHR = 1
          end if

          ! write altitudes
          if ( counterAltSim < 1 ) then
            name = 'altitudes'
            keyword1     = 'unit'
            string_1     = 'km'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMaltitude, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterAltSim = counterAltSim + 1
          end if

          if ( counterPressureSim < 1 ) then
            name = 'pressures'
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMpressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterPressureSim = counterPressureSim + 1
          end if

          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_internal_radiation_up_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_sec_per_nm_per_sr'
            keyword2     = 'altitudes'
            string_2     = '/additional_output_sim/altitudes'
            keyword3     = 'pressures'
            string_3     = '/additional_output_sim/pressures'
            keyword4     = 'wavelength'
            string_4     = '/additional_output_sim/HR_wavelength_band_'//char_index
            call writeReal(errS, name, x3D = earthRadianceSimS(iband)%intFieldUpHR(:,:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlSimS%writeInternalFieldUpHR

        ! write instrument resolution internal radiation field up
        if (controlSimS%writeInternalFieldUp) then

          ! write altitudes
          if ( counterAltSim < 1 ) then
            name = 'altitudes'
            keyword1     = 'unit'
            string_1     = 'km'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMaltitude, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterAltSim = counterAltSim + 1
          end if

          if ( counterPressureSim < 1 ) then
            name = 'pressures'
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMpressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterPressureSim = counterPressureSim + 1
          end if

          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'internal_radiation_up_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_sec_per_nm_per_sr'
            keyword2     = 'altitudes'
            string_2     = '/additional_output_sim/altitudes'
            keyword3     = 'pressures'
            string_3     = '/additional_output_sim/pressures'
            keyword4     = 'wavelength'
            string_4     = '/additional_output_sim/wavelength_band_'//char_index
            call writeReal(errS, name, x3D = earthRadianceSimS(iband)%intFieldUp(:,:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlSimS%writeInternalFieldUp

        ! write high resolution internal radiation field down
        if (controlSimS%writeInternalFieldDownHR) then

          if ( counterWavelSimHR < 1 ) then
            call writeHRwavelGrid(errS,  numSpectrBands, wavelHRSimS)
            if (errorCheck(errS)) return
            counterWavelSimHR = 1
          end if

          ! write altitudes
          if ( counterAltSim < 1 ) then
            name = 'altitudes'
            keyword1     = 'unit'
            string_1     = 'km'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMaltitude, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterAltSim = counterAltSim + 1
          end if

          if ( counterPressureSim < 1 ) then
            name = 'pressures'
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMpressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterPressureSim = counterPressureSim + 1
          end if

          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_internal_radiation_down_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_sec_per_nm_per_sr'
            keyword2     = 'altitudes'
            string_2     = '/additional_output_sim/altitudes'
            keyword3     = 'pressures'
            string_3     = '/additional_output_sim/pressures'
            keyword4     = 'wavelength'
            string_4     = '/additional_output_sim/HR_wavelength_band_'//char_index
            call writeReal(errS, name, x3D = earthRadianceSimS(iband)%intFieldDownHR(:,:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlSimS%writeInternalFieldUpHR

        ! write instrument resolution internal radiation field down
        if (controlSimS%writeInternalFieldDown) then

          ! write altitudes
          if ( counterAltSim < 1 ) then
            name = 'altitudes'
            keyword1     = 'unit'
            string_1     = 'km'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMaltitude, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterAltSim = counterAltSim + 1
          end if

          if ( counterPressureSim < 1 ) then
            name = 'pressures'
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, name, x1D = optPropRTMGridSimS%RTMpressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterPressureSim = counterPressureSim + 1
          end if

          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'internal_radiation_down_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_sec_per_nm_per_sr'
            keyword2     = 'altitudes'
            string_2     = '/additional_output_sim/altitudes'
            keyword3     = 'pressures'
            string_3     = '/additional_output_sim/pressures'
            keyword4     = 'wavelength'
            string_4     = '/additional_output_sim/wavelength_band_'//char_index
            call writeReal(errS, name, x3D = earthRadianceSimS(iband)%intFieldDown(:,:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlSimS%writeInternalFieldUp

        call endGroup

      end if ! additional_output_sim

      ! write absorption cross section LUT
      do iTrace = 1, nTrace
        do iband = 1, numSpectrBands
          if ( XsecHRLUTSimS(iband, iTrace)%createXsecPolyLUT ) then

            call writeXsecHRLUT(errS, iband, iTrace, wavelHRSimS(iband), XsecHRLUTSimS(iband, iTrace), &
                                traceGasSimS(iTrace) )

          end if ! createXsecPolyLUT
        end do ! iband
      end do ! iTrace

      ! stop writing to asciiHDF when simulationOnly = .true.
      if ( controlSimS%simulationOnly ) then
        call endAsciiHDF(errS)
        if (errorCheck(errS)) return
        return
      end if

      if ( additional_output_retr ) then

        groupName = 'additional_output_retr'
        call beginGroup(errS, groupName)
        if (errorCheck(errS)) return

        if ( controlRetrS%writeContributionRadiance) then

          ! write altitudes
          if ( counterAltRetr < 1 ) then
            name = 'altitudes'
            keyword1     = 'unit'
            string_1     = 'km'
            call writeReal(errS, name, x1D = optPropRTMGridRetrS%RTMaltitude, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            name = 'pressures'
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, name, x1D = optPropRTMGridRetrS%RTMpressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterAltRetr = counterAltRetr + 1
          end if

          ! write path radiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'earth_path_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_nm_per_sr_per_km'
            keyword2     = 'altitudes'
            string_2     = '/additional_output_retr/altitudes'
            keyword3     = 'wavelength'
            string_3     = '/additional_output_retr/wavelength_band_'//char_index
            call writeReal(errS, name, x2D = earthRadianceRetrS(iband)%contribRad(1,:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlRetrS%writeContributionRadiance

        if ( controlRetrS%writeAltResolvedAMF) then

          if ( counterWavelRetrHR < 1 ) then
            call writeHRwavelGrid(errS,  numSpectrBands, wavelHRRetrS)
            if (errorCheck(errS)) return
            counterWavelRetrHR = 1
          end if

          ! write altitudes
          if ( counterAltRetr < 1 ) then
            name = 'altitudes'
            keyword1     = 'unit'
            string_1     = 'km'
            call writeReal(errS, name, x1D = optPropRTMGridRetrS%RTMaltitude, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            name = 'pressures'
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, name, x1D = optPropRTMGridRetrS%RTMpressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return
            counterAltRetr = counterAltRetr + 1
          end if

          ! write altitude resolved air mass factor for absorption
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_alt_resolved_AMF_abs_band_clear_'//char_index
            keyword1     = 'unit'
            string_1     = ' '
            keyword2     = 'altitudes'
            string_2     = '/additional_output_retr/altitudes'
            keyword3     = 'pressures'
            string_3     = '/additional_output_retr/pressures'
            keyword4     = 'wavelength'
            string_4     = '/additional_output_retr/HR_wavelength_band_'//char_index
            call writeReal(errS, name, x2D = reflDerivHRRetrS(iband)%altResAMFabs_clr(:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

          if ( cloudAerosolRTMgridRetrS%cloudPresent ) then
            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'HR_alt_resolved_AMF_abs_band_cloud_'//char_index
              keyword1     = 'unit'
              string_1     = ' '
              keyword2     = 'altitudes'
              string_2     = '/additional_output_retr/altitudes'
              keyword3     = 'pressures'
              string_3     = '/additional_output_retr/pressures'
              keyword4     = 'wavelength'
              string_4     = '/additional_output_retr/HR_wavelength_band_'//char_index
              call writeReal(errS, name, x2D = reflDerivHRRetrS(iband)%altResAMFabs_cld(:,:), &
                             keyword1 = keyword1, string_1 = string_1, &
                             keyword2 = keyword2, string_2 = string_2, &
                             keyword3 = keyword3, string_3 = string_3, &
                             keyword4 = keyword4, string_4 = string_4)
              if (errorCheck(errS)) return
             end do ! iband
          end if ! cloudAerosolRTMgridRetrS%cloudPresent

          ! write altitude resolved air mass factor for aerosol scattering
          if ( cloudAerosolRTMgridRetrS%aerosolPresent ) then
            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'HR_alt_resolved_AMF_sca_aer_band_clear_'//char_index
              keyword1     = 'unit'
              string_1     = ' '
              keyword2     = 'altitudes'
              string_2     = '/additional_output_retr/altitudes'
              keyword3     = 'pressures'
              string_3     = '/additional_output_retr/pressures'
              keyword4     = 'wavelength'
              string_4     = '/additional_output_retr/HR_wavelength_band_'//char_index
              call writeReal(errS, name, x2D = reflDerivHRRetrS(iband)%altResAMFscaAer_clr(:,:), &
                             keyword1 = keyword1, string_1 = string_1, &
                             keyword2 = keyword2, string_2 = string_2, &
                             keyword3 = keyword3, string_3 = string_3, &
                             keyword4 = keyword4, string_4 = string_4)
              if (errorCheck(errS)) return
            end do ! iband

            if ( cloudAerosolRTMgridRetrS%cloudPresent ) then
              do iband = 1, numSpectrBands
                char_index = achar(48 + iband)
                name = 'HR_alt_resolved_AMF_sca_aer_band_cloud_'//char_index
                keyword1     = 'unit'
                string_1     = ' '
                keyword2     = 'altitudes'
                string_2     = '/additional_output_retr/altitudes'
                keyword3     = 'pressures'
                string_3     = '/additional_output_retr/pressures'
                keyword4     = 'wavelength'
                string_4     = '/additional_output_retr/HR_wavelength_band_'//char_index
                call writeReal(errS, name, x2D = reflDerivHRRetrS(iband)%altResAMFscaAer_cld(:,:), &
                               keyword1 = keyword1, string_1 = string_1, &
                               keyword2 = keyword2, string_2 = string_2, &
                               keyword3 = keyword3, string_3 = string_3, &
                               keyword4 = keyword4, string_4 = string_4)
                if (errorCheck(errS)) return
              end do ! iband
            end if ! cloudAerosolRTMgridSimS%cloudPresent
          end if ! cloudAerosolRTMgridSimS%aerosolPresent

        end if ! controlRetrS%writeAltResolvedAMF

        if ( controlRetrS%writeAbsorptionXsec ) then

          if ( counterWavelRetrHR < 1 ) then
            call writeHRwavelGrid(errS,  numSpectrBands, wavelHRRetrS)
            if (errorCheck(errS)) return
            counterWavelRetrHR = 1
          end if

          ! write the pressure grids for the different absorbing gases
          ! and the absorption cross section
          do itrace = 1, nTrace

            ! write pressure grid for the trace gas involved
            namePressureGrid = 'pressuresGrid_for_'//trim(traceGasRetrS(itrace)%nameTraceGas )
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, namePressureGrid, x1D = traceGasRetrS(itrace)%pressure, &
                           keyword1 = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return

            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'absorptionXsecBand_'//char_index//'_for_'//trim(traceGasRetrS(itrace)%nameTraceGas )
              keyword1     = 'unit'
              if ( trim(traceGasRetrS(itrace)%nameTraceGas) == 'O2-O2' ) then
                unit_abs_Xsec = 'cm^5 / molecule^2'
              else
                unit_abs_Xsec = 'cm^2 / molecule'
              end if
              string_1     = unit_abs_Xsec
              keyword2     = 'pressureGrid'
              string_2     = '/additional_output_retr/'//trim(namePressureGrid)
              keyword3     = 'wavelength'
              string_3     = '/additional_output_retr/HR_wavelength_band_'//char_index
              call writeReal(errS, name, x2D = XsecHRRetrS(iband,itrace)%Xsec(:,:), &
                             keyword1 = keyword1, string_1 = string_1,  &
                             keyword2 = keyword2, string_2 = string_2,  &
                             keyword3 = keyword3, string_3 = string_3)
              if (errorCheck(errS)) return
            end do ! iband
          end do ! itrace

        end if ! controlRetrS%writeAbsorptionXsec

        if ( controlRetrS%writeHighResReflAndDeriv) then
          if ( counterWavelRetrHR < 1 ) then
            call writeHRwavelGrid(errS,  numSpectrBands, wavelHRRetrS)
            if (errorCheck(errS)) return
            counterWavelRetrHR = 1
          end if

          ! write high resolution solar irradiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_solar_irradiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_nm_per_s'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_retr/HR_wavelength_band_'//char_index
            call writeReal(errS, name, x1D = solarIrradianceRetrS(iband)%solIrrMR(:), &
                           keyword1 = keyword1, string_1 = string_1,            &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband

          ! write high resolution earth radiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_earth_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_nm_per_s_per_sr'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_retr/HR_wavelength_band_'//char_index
            call writeReal(errS, name, x1D = earthRadianceRetrS(iband)%rad_HR(1,:), &
                           keyword1 = keyword1, string_1 = string_1,          &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband

          ! write high resolution sun_normalized_radiance for retrieval
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_sun_normalized_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'per_sr'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_retr/HR_wavelength_band_'//char_index
            allocate( sun_norm_radiance(earthRadianceRetrS(iband)%nwavelHR), STAT = allocStatus  )
            if ( allocStatus /= 0 ) then
              call logDebug('FATAL ERROR: allocation failed')
              call logDebug('for sun_norm_radiance - high resolution - retrieval')
              call logDebugI('for iband = ', iband)
              call logDebug('in subroutine print_AsciiHDF')
              call logDebug('in module writeModule')
              call mystop(errS, 'stopped because allocation failed')
              if (errorCheck(errS)) return
            end if
            sun_norm_radiance(:) = earthRadianceRetrS(iband)%rad_HR(1,:)   &
                                 / solarIrradianceRetrS(iband)%solIrrMR(:)
            call writeReal(errS, name, x1D = sun_norm_radiance,                  &
                           keyword1 = keyword1, string_1 = string_1,       &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
            deallocate( sun_norm_radiance, STAT = deallocStatus )
            if ( deallocStatus /= 0 ) then
              call logDebug('FATAL ERROR: deallocation failed')
              call logDebug('for sun_norm_radiance - high resolution - retrieval')
              call logDebugI('for iband = ', iband)
              call logDebug('in subroutine print_AsciiHDF')
              call logDebug('in module writeModule')
              call mystop(errS, 'stopped because deallocation failed')
              if (errorCheck(errS)) return
            end if
          end do ! iband

          if ( (controlRetrS%dimSV > 2) .and. (controlRetrS%method == 0) ) then
            ! write degree of linear polarization on high resolution wavelength grid for retrieval
            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'HR_degree_lin_polarization_band_'//char_index
              keyword1     = 'unit'
              string_1     = 'percent'
              keyword2     = 'wavelength'
              string_2     = '/additional_output_retr/HR_wavelength_band_'//char_index
              keyword3     = 'remark'
              string_3     = 'degree_lin_polarization = 100 * sqrt(Q*Q+U*U) / I'
              allocate( degree_lin_polarization(earthRadianceRetrS(iband)%nwavelHR), STAT = allocStatus  )
              if ( allocStatus /= 0 ) then
                call logDebug('FATAL ERROR: allocation failed')
                call logDebug('for degree_lin_polarization - high resolution - retrieval')
                call logDebugI('for iband = ', iband)
                call logDebug('in subroutine print_AsciiHDF')
                call logDebug('in module writeModule')
                call mystop(errS, 'stopped because allocation failed')
                if (errorCheck(errS)) return
              end if
              degree_lin_polarization(:) = 100.0d0 * sqrt(   earthRadianceRetrS(iband)%rad_HR(2,:)**2    &
                                                           + earthRadianceRetrS(iband)%rad_HR(3,:)**2  ) &
                                                           / earthRadianceRetrS(iband)%rad_HR(1,:)
              call writeReal(errS, name, x1D = degree_lin_polarization,         &
                             keyword1 = keyword1, string_1 = string_1,    &
                             keyword2 = keyword2, string_2 = string_2,    &
                             keyword3 = keyword3, string_3 = string_3)
              if (errorCheck(errS)) return
              deallocate( degree_lin_polarization, STAT = deallocStatus )
              if ( deallocStatus /= 0 ) then
                call logDebug('FATAL ERROR: deallocation failed')
                call logDebug('for degree_lin_polarization - high resolution - retrieval')
                call logDebugI('for iband = ', iband)
                call logDebug('in subroutine print_AsciiHDF')
                call logDebug('in module writeModule')
                call mystop(errS, 'stopped because deallocation failed')
                if (errorCheck(errS)) return
              end if
            end do ! iband
          end if ! controlRetrS%dimSV > 2

          if ( counterStateVectorRetr < 1 ) then
            ! write identifying labels for the state vecotor elements
            name = 'state_vector_elements'
            call writeString(errS, name, x1D = retrS%codeFitParameters(1:retrS%nstate) )
            if (errorCheck(errS)) return
            counterStateVectorRetr = counterStateVectorRetr + 1
          end if

          ! write derivatives
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'HR_derivatives_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'depends on state vector element'
            keyword2     = 'legend'
            string_2     = '/additional_output_retr/state_vector_elements'
            keyword3     = 'wavelengths'
            string_3     = '/additional_output_retr/HR_wavelength_band_'//char_index
            keyword4     = 'remark'
            string_4     = 'dR/dx_i; R reflectance x_i state vector element'
            call writeReal(errS, name, x2D = reflDerivHRRetrS(iband)%K_vmr(:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlRetrS%writeHighResReflAndDeriv

        if ( controlRetrS%writeInstrResReflAndDeriv) then

          ! write instrument resolution solar irradiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'solar_irradiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons_per_cm2_per_nm_per_s'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_retr/wavelength_band_'//char_index
            call writeReal(errS, name, x1D = solarIrradianceRetrS(iband)%solIrr(:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband

          ! write instrument resolution earth radiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'earth_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'photons per cm2 per nm per sr'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_retr/wavelength_band_'//char_index
            call writeReal(errS, name, x1D = earthRadianceRetrS(iband)%rad(1,:), &
                           keyword1 = keyword1, string_1 = string_1,       &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
          end do ! iband

          ! write instrument resolution sun_normalized_radiance
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'sun_normalized_radiance_band_'//char_index
            keyword1     = 'unit'
            string_1     = 'per_sr'
            keyword2     = 'wavelength'
            string_2     = '/additional_output_retr/wavelength_band_'//char_index
            allocate( sun_norm_radiance(earthRadianceRetrS(iband)%nwavel), STAT = allocStatus )
            if ( allocStatus /= 0 ) then
              call logDebug('FATAL ERROR: allocation failed')
              call logDebug('for sun_norm_radiance - instrument resolution - retrieval')
              call logDebugI('for iband = ', iband)
              call logDebug('in subroutine print_AsciiHDF')
              call logDebug('in module writeModule')
              call mystop(errS, 'stopped because allocation failed')
              if (errorCheck(errS)) return
            end if
            sun_norm_radiance = earthRadianceRetrS(iband)%rad(1,:) / solarIrradianceRetrS(iband)%solIrr
            call writeReal(errS, name, x1D = sun_norm_radiance,               &
                           keyword1 = keyword1, string_1 = string_1,   &
                           keyword2 = keyword2, string_2 = string_2)
            if (errorCheck(errS)) return
            deallocate( sun_norm_radiance, STAT = deallocStatus )
            if ( deallocStatus /= 0 ) then
              call logDebug('FATAL ERROR: deallocation failed')
              call logDebug('for sun_norm_radiance - instrument resolution - retrieval')
              call logDebugI('for iband = ', iband)
              call logDebug('in subroutine print_AsciiHDF')
              call logDebug('in module writeModule')
              call mystop(errS, 'stopped because deallocation failed')
              if (errorCheck(errS)) return
            end if
          end do ! iband

          if ( (controlRetrS%dimSV > 2) .and. (controlRetrS%method == 0) ) then
            ! write instrument resolution linear polarization
            do iband = 1, numSpectrBands
              char_index = achar(48 + iband)
              name = 'degree_lin_polarization_band_'//char_index
              keyword1     = 'unit'
              string_1     = 'percent'
              keyword2     = 'wavelength'
              string_2     = '/additional_output_retr/wavelength_band_'//char_index
              keyword3     = 'remark'
              string_3     = 'shot noise and offsets have NOT been added'
              keyword3     = 'remark'
              string_3     = 'degree_lin_polarization = 100 * sqrt(Q*Q+U*U) / I'
              allocate( degree_lin_polarization(earthRadianceRetrS(iband)%nwavel), STAT = allocStatus )
              if ( allocStatus /= 0 ) then
                call logDebug('FATAL ERROR: allocation failed')
                call logDebug('for degree_lin_polarization - instrument resolution - retrieval')
                call logDebugI('for iband = ', iband)
                call logDebug('in subroutine print_AsciiHDF')
                call logDebug('in module writeModule')
                call mystop(errS, 'stopped because allocation failed')
                if (errorCheck(errS)) return
              end if
              degree_lin_polarization(:) = 100.0d0 * sqrt(   earthRadianceRetrS(iband)%rad(2,:)**2    &
                                                           + earthRadianceRetrS(iband)%rad(3,:)**2  ) &
                                                           / earthRadianceRetrS(iband)%rad(1,:)
              call writeReal(errS, name, x1D = degree_lin_polarization,        &
                             keyword1 = keyword1, string_1 = string_1,   &
                             keyword2 = keyword2, string_2 = string_2,   &
                             keyword3 = keyword3, string_3 = string_3,   &
                             keyword4 = keyword4, string_4 = string_4)
              if (errorCheck(errS)) return
              deallocate( degree_lin_polarization, STAT = deallocStatus )
              if ( deallocStatus /= 0 ) then
                call logDebug('FATAL ERROR: deallocation failed')
                call logDebug('for degree_lin_polarization - instrument resolution - retrieval')
                call logDebugI('for iband = ', iband)
                call logDebug('in subroutine print_AsciiHDF')
                call logDebug('in module writeModule')
                call mystop(errS, 'stopped because deallocation failed')
                if (errorCheck(errS)) return
              end if
            end do ! iband
          end if ! controlRetrS%dimSV > 2

          if ( counterStateVectorRetr < 1 ) then
            ! write identifying labels for the state vecotor elements
            name = 'state_vector_elements'
            call writeString(errS, name, x1D = retrS%codeFitParameters(1:retrS%nstate) )
            if (errorCheck(errS)) return
            counterStateVectorRetr = counterStateVectorRetr + 1
          end if

          ! write derivatives on instrument spectral grid
          do iband = 1, numSpectrBands
            char_index = achar(48 + iband)
            name = 'derivatives_band_'//char_index
            keyword1     = 'unit'
            string_1     = ' '
            keyword2     = 'legend'
            string_2     = '/additional_output_retr/state_vector_elements'
            keyword3     = 'wavelengths'
            string_3     = '/additional_output_retr/wavelength_band_'//char_index
            keyword4     = 'remark'
            string_4     = 'dI/dx_i; I radiance x_i state vector element'
            call writeReal(errS, name, x2D = earthRadianceRetrS(iband)%K_vmr(:,:), &
                           keyword1 = keyword1, string_1 = string_1, &
                           keyword2 = keyword2, string_2 = string_2, &
                           keyword3 = keyword3, string_3 = string_3, &
                           keyword4 = keyword4, string_4 = string_4)
            if (errorCheck(errS)) return
          end do ! iband

        end if ! controlRetrS%writeInstrResReflAndDeriv

        call endGroup

      end if ! additional output for retrieval

      ! fill identifying wavelengths and units array so that they can be used as attribute

      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case('nodeTrace')
            iTrace = retrS%codeTraceGas(istate)
            wavelengths(istate) = 0.0d0
            unit_state_vector(istate) = 'molecules_per_cm3'
            unit_covariance(istate)   = 'molecules2_per_cm6'
            nameTraceGas_state_vector(istate) = traceGasRetrS(iTrace)%nameTraceGas

          case('columnTrace')
            wavelengths(istate) = 0.0d0
            iTrace = retrS%codeTraceGas(istate)
            if ( traceGasRetrS(iTrace)%unitFlag ) then
              unit_state_vector(istate) = 'Dobson_Units'
              unit_covariance(istate)   = 'Dobson_Units2'
            else
              unit_state_vector(istate) = 'molecules_per_cm2'
              unit_covariance(istate)   = 'molecules2_per_cm4'
            end if
            nameTraceGas_state_vector(istate) = traceGasRetrS(iTrace)%nameTraceGas

          case('nodeTemp', 'offsetTemp')
            wavelengths(istate) = 0.0d0
            unit_state_vector(istate) = 'Kelvin'
            unit_covariance(istate)   = 'Kelvin2'
            nameTraceGas_state_vector(istate) = ' '

          case('surfPressure')
            wavelengths(istate) = 0.0d0
            unit_state_vector(istate) = 'hPa'
            unit_covariance(istate)   = 'hPa^2 '
            nameTraceGas_state_vector(istate) = ' '

          case('surfAlbedo')
            if ( cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then
              wavelengths(istate) =  0.0d0
            else
              iband = retrS%codeSpecBand(istate)
              index = retrS%codeIndexSurfAlb(istate)
              wavelengths(istate) =  surfaceRetrS(iband)%wavelAlbedo(index)
            end if
            unit_state_vector(istate) = ' '
            unit_covariance(istate)   = ' '
            nameTraceGas_state_vector(istate) = ' '

          case('surfEmission')
            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexSurfEmission(istate)
            wavelengths(istate) =  surfaceRetrS(iband)%wavelEmission(index)
            unit_state_vector(istate) = '10^12 ph/cm2/s/nm/sr'
            unit_covariance(istate)   = '(10^12 ph/cm2/s/nm/sr)**2'
            nameTraceGas_state_vector(istate) = ' '

          case('LambCldAlbedo')
            if ( cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then
              wavelengths(istate) =  0.0d0
            else
              iband = retrS%codeSpecBand(istate)
              index = retrS%codeIndexLambCldAlb(istate)
              wavelengths(istate) = LambCloudRetrS(iband)%wavelAlbedo(index)
            end if
            unit_state_vector(istate) = ' '
            unit_covariance(istate)   = ' '
            nameTraceGas_state_vector(istate) = ' '

          case('mulOffset')
            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexMulOffset(istate)
            wavelengths(istate) = mulOffsetRetrS(iband)%wavel(index)
            unit_state_vector(istate) = 'percent'
            unit_covariance(istate)   = 'percent2'
            nameTraceGas_state_vector(istate) = ' '

          case('straylight')
            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexStraylight(istate)
            wavelengths(istate) = strayLightRetrS(iband)%wavel(index)
            unit_state_vector(istate) = 'percent'
            unit_covariance(istate)   = 'percent2'
            nameTraceGas_state_vector(istate) = ' '

          case('diffRingCoef', 'RingCoef', 'aerosolTau', 'aerosolSSA', 'aerosolAC', 'cloudTau', 'cloudAC')
            wavelengths(istate) = 0.0d0
            unit_state_vector(istate) = ' '
            unit_covariance(istate)   = ' '
            nameTraceGas_state_vector(istate) = ' '

          case('cloudFraction')
            if ( cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then
              wavelengths(istate) = 0.0d0
            else
              iband = retrS%codeSpecBand(istate)
              index = retrS%codeIndexCloudFraction(istate)
              wavelengths(istate) = cldAerFractionRetrS(iband)%wavelCldAerFraction(index)
            end if
            unit_state_vector(istate) = ' '
            unit_covariance(istate)   = ' '
            nameTraceGas_state_vector(istate) = ' '

          case('intervalDP', 'intervalTop', 'intervalBot')
            wavelengths(istate) = 0.0d0
            unit_state_vector(istate) = 'km or hPa'
            unit_covariance(istate)   = 'km2 or hPa2'
            nameTraceGas_state_vector(istate) = ' '

          case default
            call logDebug('incorrect value for retrS%codeFitParameters(counter) : '// retrS%codeFitParameters(istate))
            call logDebug('in subroutine print_AsciiHDF')
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return
        end select

      end do ! istate

      ! fill correlation coefficient matrix for S_lnvmr
      do jstate = 1, retrS%nstate
        do istate = 1, retrS%nstate
          correlation(istate,jstate) = diagnosticS%S_lnvmr(istate,jstate) &
            / sqrt( diagnosticS%S_lnvmr(istate,istate) * diagnosticS%S_lnvmr(jstate,jstate) )
        end do ! istate
      end do ! jstate

      ! fill legend and legend precision
      legend(1) = 'true'
      legend(2) = 'a_priori'
      legend(3) = 'retrieved'
      legend_precision(1) = 'a_priori_precision'
      legend_precision(2) = 'precision'
      legend_precision(3) = 'bias'

      ! allocate arrays
      allocStatus    = 0
      sumallocStatus = 0

      ! initialize counters
      nsurfaceAlbedo         = sum( surfaceRetrS(:)%nAlbedo )
      nsurfaceEmission       = sum( surfaceRetrS(:)%nEmission )
      ncloudAlbedo           = sum( LambCloudRetrS(:)%nAlbedo )
      ncldaerFraction        = sum( cldAerFractionRetrS(:)%nCldAerFraction )
      nmulOffset             = sum( mulOffsetRetrS(:)%nwavel )
      nstrayLight            = sum( strayLightRetrS(:)%nwavel )

      if ( nsurfaceAlbedo   > numSpectrBands * maxNumWavelAlbedo         ) &
           nsurfaceAlbedo   = numSpectrBands * maxNumWavelAlbedo
      if ( nsurfaceEmission > numSpectrBands * maxNumWavelEmission       ) &
           nsurfaceEmission = numSpectrBands * maxNumWavelEmission
      if ( ncloudAlbedo     > numSpectrBands * maxNumWavelAlbedo         ) &
           ncloudAlbedo     = numSpectrBands * maxNumWavelAlbedo
      if ( ncldaerFraction  > numSpectrBands * maxNumWavelCldAerFraction ) &
           ncldaerFraction  = numSpectrBands * maxNumWavelCldAerFraction
      if ( nstrayLight  > numSpectrBands * maxNumWavelStrayLight ) &
           nstrayLight  = numSpectrBands * maxNumWavelStrayLight

      if ( nsurfaceAlbedo   < 1 ) nsurfaceAlbedo   = 1
      if ( nsurfaceEmission < 1 ) nsurfaceEmission = 1
      if ( ncloudAlbedo     < 1 ) ncloudAlbedo     = 1
      if ( ncldaerFraction  < 1 ) ncldaerFraction  = 1
      if ( nstrayLight      < 1 ) nstrayLight      = 1

      groupName = 'sun_normalized_radiance'
      call beginGroup(errS, groupName)
      if (errorCheck(errS)) return
        keyword1 = 'unit'
        string_1 = 'nm'
        name = 'wavelengths'
        call writeReal(errS, name, x1D=retrS%wavelRetr, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = ' '
        name = 'SNR_measured_spectrum'
        call writeReal(errS, name, x1D=retrS%reflMeas/retrS%reflNoiseError, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'per_sr'
        name = 'measured_spectrum'
        call writeReal(errS, name, x1D=retrS%reflMeas, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'per_sr'
        name = 'fitted_spectrum'
        call writeReal(errS, name, x1D=retrS%refl, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'per_sr'
        name = 'assumed_noise'
        call writeReal(errS, name, x1D=retrS%reflNoiseError, keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'percent'
        name = 'initial_residue'
        call writeReal(errS, name, x1D=1.0D2*retrS%dR_initial(:)/retrS%reflMeas(:), keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
        string_1 = 'percent'
        name = 'final_residue'
        call writeReal(errS, name, x1D=1.0D2*retrS%dR(:)/retrS%reflMeas(:), keyword1=keyword1,  string_1 = string_1)
        if (errorCheck(errS)) return
      call endGroup

      if ( .not. external_dataS%missing ) then
        groupName = 'external_data'
        call beginGroup(errS, groupName)
        if (errorCheck(errS)) return
          name = 'latitude'
          keyword1     = 'unit'
          string_1     = 'degree'
          call writeReal(errS, name, x=external_dataS%latitude, keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return
          name = 'longitude'
          keyword1     = 'unit'
          string_1     = 'degree'
          call writeReal(errS, name, x=external_dataS%longitude, keyword1 = keyword1, string_1 = string_1)
          if (errorCheck(errS)) return
          name = 'useSurfAlbedoDataBase'
          if ( external_dataS%useSurfAlbedoDataBase ) then
            call writeInt(errS, name, x=1)
            if (errorCheck(errS)) return
          else
            call writeInt(errS, name, x=0)
            if (errorCheck(errS)) return
          end if
          name = 'year'
          call writeInt(errS, name, x=external_dataS%year)
          if (errorCheck(errS)) return
          name = 'month'
          call writeInt(errS, name, x=external_dataS%month)
          if (errorCheck(errS)) return
          name = 'day'
          call writeInt(errS, name, x=external_dataS%day)
          if (errorCheck(errS)) return
          name = 'hour'
          call writeInt(errS, name, x=external_dataS%hour)
          if (errorCheck(errS)) return
          name = 'minute'
          call writeInt(errS, name, x=external_dataS%minute)
          if (errorCheck(errS)) return
          name = 'second'
          call writeReal(errS, name, x=external_dataS%second)
          if (errorCheck(errS)) return
          name = 'XtrackNumber'
          call writeInt(errS, name, x=external_dataS%XtrackNumber)
          if (errorCheck(errS)) return
          name = 'AtrackNumber'
          call writeInt(errS, name, x=external_dataS%AtrackNumber)
          if (errorCheck(errS)) return
          name = 'orbitNumber'
          call writeInt(errS, name, x=external_dataS%orbitNumber)
          if (errorCheck(errS)) return
          name = 'originalIrradianceFileName'
          call writeString(errS, name, x_long=external_dataS%originalIrradianceFileName)
          if (errorCheck(errS)) return
          name = 'originalRadianceUVFileName'
          call writeString(errS, name, x_long=external_dataS%originalRadianceUVFileName)
          if (errorCheck(errS)) return
          name = 'originalRadianceVISFileName'
          call writeString(errS, name, x_long=external_dataS%originalRadianceVISFileName)
          if (errorCheck(errS)) return
        call endGroup
      end if ! external_dataS%missing

      groupName = 'parameters'
      call beginGroup(errS, groupName)
      if (errorCheck(errS)) return

      ! initialize
      first_time_profile = .true.
      first_time_temp     = .true.
      countercolumnfit = 0

      ! write AAI when apropriate
      if ( controlRetrS%calculateAAI ) then
        AAI(1)          = retrS%AAI
        name            = 'AAI'
        call writeReal(errS, name, x1D = AAI)
        if (errorCheck(errS)) return
        precisionAAI(1) = sqrt(retrS%varAAI)
        name            = 'precisionAAI'
        call writeReal(errS, name, x1D = precisionAAI)
        if (errorCheck(errS)) return
      end if

      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case('nodeTrace')  ! group for profiles is done later

          case('columnTrace')

            countercolumnfit = countercolumnfit + 1
            iTrace = retrS%codeTraceGas(istate)

            keyword = 'unit'

            if ( traceGasRetrS(iTrace)%unitFlag ) then
              factor = 1.0d0 / DUToCm2
              unit(countercolumnfit)  = 'Dobson Units'
            else
              factor = 1.0d0
              unit(countercolumnfit)  = 'molecules_per_cm2'
            end if
            nameTraceGas(countercolumnfit) = traceGasRetrS(iTrace)%nameTraceGas
            columns(1,countercolumnfit)    = factor * traceGasSimS(iTrace)%column      ! true column
            columns(2,countercolumnfit)    = factor * traceGasRetrS(iTrace)%columnAP   ! a-priori column
            columns(3,countercolumnfit)    = factor * traceGasRetrS(iTrace)%column     ! retrieved column

            precisionColumns(1,countercolumnfit) = factor * sqrt(traceGasRetrS(iTrace)%covColumnAP)  ! a-priori precision
            precisionColumns(2,countercolumnfit) = factor * sqrt(traceGasRetrS(iTrace)%covColumn)    ! a-posteriori precision
            precisionColumns(3,countercolumnfit) = columns(3,countercolumnfit) - columns(1,countercolumnfit)             ! bias

            if( countercolumnfit == retrS%ncolumnfit ) then
              keyword1     = 'unit'
              name = 'total_columns'
              keywordList1 = 'name_trace_gas'
              keywordList2 = 'unit'
              keywordList3 = 'legend'
              call writeReal(errS, name, x2D = columns, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit,         &
                             keywordList3 = keywordList3, stringList_3 = legend )
              if (errorCheck(errS)) return
              name = 'precision_bias_columns'
              call writeReal(errS, name, x2D = precisionColumns, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = nameTraceGas, &
                             keywordList2 = keywordList2, stringList_2 = unit,         &
                             keywordList3 = keywordList3, stringList_3 = legend_precision )
              if (errorCheck(errS)) return
            end if

          case('nodeTemp')

            if ( first_time_temp ) then

              temperature(1:gasPTRetrS%npressureNodes+1,1) = &
                       gasPTRetrS%temperature_trueNodes(0:gasPTRetrS%npressureNodes)
              temperature(1:gasPTRetrS%npressureNodes+1,2) = &
                       gasPTRetrS%temperatureNodesAP(0:gasPTRetrS%npressureNodes)
              temperature(1:gasPTRetrS%npressureNodes+1,3) = &
                       gasPTRetrS%temperatureNodes(0:gasPTRetrS%npressureNodes)

              do ialt = 1, gasPTRetrS%npressureNodes + 1
                temperature_precision(ialt,1) = sqrt( gasPTRetrS%covTempNodesAP(ialt - 1, ialt - 1) )
                temperature_precision(ialt,2) = sqrt( gasPTRetrS%covTempNodes  (ialt - 1, ialt - 1) )
              end do
              temperature_precision(:,3) = temperature(:,3) - temperature(:,1)

              name         = 'temperature'
              keyword1     = 'unit'
              keywordList1 = 'legend'
              call writeReal(errS, name, x2D = temperature, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = legend )
              if (errorCheck(errS)) return

              name         = 'precision_bias_temperature'
              call writeReal(errS, name, x2D = temperature_precision, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = legend_precision )
              if (errorCheck(errS)) return
            end if

            first_time_temp = .false.

          case('offsetTemp')

            allBands(1) = gasPTSimS%temperatureOffset
            allBands(2) = gasPTRetrS%temperatureOffsetAP
            allBands(3) = gasPTRetrS%temperatureOffset

            precisionAllBands(1) = sqrt(gasPTRetrS%varianceTempOffsetAP) ! a-priori precision
            precisionAllBands(2) = sqrt(gasPTRetrS%varianceTempOffset)   ! precision
            precisionAllBands(3) = allBands(3) - allBands(1)             ! bias

            name         = 'temperature_offset'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_temperature_offset'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('surfPressure')

            allBands(1) = surfaceSimS(1) %pressure
            allBands(2) = surfaceRetrS(1)%pressureAP
            allBands(3) = surfaceRetrS(1)%pressure

            precisionAllBands(1) = sqrt(surfaceRetrS(1)%varPressureAP)   ! a-priori precision
            precisionAllBands(2) = sqrt(surfaceRetrS(1)%varPressure  )   ! precision
            precisionAllBands(3) = allBands(3) - allBands(1)          ! bias

            name         = 'surface_pressure'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_surface_pressure'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('surfAlbedo')

            if ( cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then

              ! one surface albedo for all wavelengths and all spectral bands

              allBands(1) = cloudAerosolRTMgridSimS%albedoLambSurfAllBands     ! true surface albedo
              allBands(2) = cloudAerosolRTMgridRetrS%albedoLambSurfAllBandsAP  ! a-priori surface albedo
              allBands(3) = cloudAerosolRTMgridRetrS%albedoLambSurfAllBands    ! a-posteriori surface albedo

              precisionAllBands(1) = sqrt(cloudAerosolRTMgridRetrS%varAlbedoLambSurfAllAP) ! a-priori precision
              precisionAllBands(2) = sqrt(cloudAerosolRTMgridRetrS%varAlbedoLambSurfAll)   ! precision
              precisionAllBands(3) = allBands(3) - allBands(1)                             ! bias

              name = 'surface_albedo'
              keyword1     = 'unit'
              keywordList1 = 'legend'
              call writeReal(errS, name, x1D = allBands, &
                            keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                            keywordList1 = keywordList1, stringList_1 = legend )
              if (errorCheck(errS)) return
              name = 'precision_bias_surface_albedo'
              call writeReal(errS, name, x1D = precisionAllBands, &
                            keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                            keywordList1 = keywordList1, stringList_1 = legend_precision )
              if (errorCheck(errS)) return


            else

              ! surface albedo can differ for spectral bands and can be a function of the wavelength

              counterSurfaceAlbedo = counterSurfaceAlbedo + 1

              isurfAlb = 0
              do iband = 1, retrS%codeSpecBand(istate) - 1
                isurfAlb = isurfAlb + surfaceRetrS(iband)%nAlbedo
              end do
              iband    = retrS%codeSpecBand(istate)
              index    = retrS%codeIndexSurfAlb(istate)
              isurfAlb = index + isurfAlb

              wavelengthSurfAlb(isurfAlb) = surfaceRetrS(iband)%wavelAlbedo(index)

              ! interpolate surface albedo for simulation to the retrieval wavelength
              surfaceAlbedo(1,isurfAlb) = &  ! true surface albedo (interpolated)
                polyInt(errS, surfaceSimS(iband)%wavelAlbedo, surfaceSimS(iband)%albedo, surfaceRetrS(iband)%wavelAlbedo(index))
              surfaceAlbedo(2,isurfAlb) = surfaceRetrS(iband)%albedoAP(index)    ! a-priori surface albedo
              surfaceAlbedo(3,isurfAlb) = surfaceRetrS(iband)%albedo(index)      ! retrieved surface albedo

              precisionSurfAlb(1,isurfAlb) = sqrt(surfaceRetrS(iband)%varianceAlbedoAP(index))     ! a-priori precision
              precisionSurfAlb(2,isurfAlb) = sqrt(surfaceRetrS(iband)%varianceAlbedo(index))       ! a-posteriori precision
              precisionSurfAlb(3,isurfAlb) = surfaceAlbedo(3,isurfAlb) - surfaceAlbedo(1,isurfAlb) ! bias


              if( counterSurfaceAlbedo == nsurfaceAlbedo ) then
                name = 'surface_albedo'
                keyword1     = 'unit'
                keywordList1 = 'wavelengths'
                keywordList2 = 'legend'
                call writeReal(errS, name, x2D = surfaceAlbedo, &
                               keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                               keywordList1 = keywordList1, real8List_1  = wavelengthSurfAlb, &
                               keywordList2 = keywordList2, stringList_2 = legend )
                if (errorCheck(errS)) return
                name = 'precision_bias_surface_albedo'
                call writeReal(errS, name, x2D = precisionSurfAlb, &
                               keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                               keywordList1 = keywordList1, real8List_1  = wavelengthSurfAlb, &
                               keywordList2 = keywordList2, stringList_2 = legend_precision )
                if (errorCheck(errS)) return
              end if
            end if !

          case('surfEmission')

            ! surface emission can differ for spectral bands and can be a function of the wavelength

            counterSurfaceEmission = counterSurfaceEmission + 1

            isurfEmission = 0
            do iband = 1, retrS%codeSpecBand(istate) - 1
              isurfEmission = isurfEmission + surfaceRetrS(iband)%nEmission
            end do
            iband    = retrS%codeSpecBand(istate)
            index    = retrS%codeIndexSurfEmission(istate)
            isurfEmission = index + isurfEmission

            wavelengthSurfEmission(isurfEmission) = surfaceRetrS(iband)%wavelEmission(index)

            ! interpolate surface emission for simulation to the retrieval wavelength
            surfaceEmission(1,isurfEmission) =    &  ! true surface emission (interpolated)
              polyInt(errS, surfaceSimS(iband)%wavelEmission, surfaceSimS(iband)%emission, &
                      surfaceRetrS(iband)%wavelEmission(index))
            surfaceEmission(2,isurfEmission) = surfaceRetrS(iband)%emissionAP(index)    ! a-priori surface emission
            surfaceEmission(3,isurfEmission) = surfaceRetrS(iband)%emission(index)      ! retrieved surface emission

            precisionSurfEmission(1,isurfEmission) = &
                        sqrt(surfaceRetrS(iband)%varianceEmissionAP(index))  ! a-priori precision
            precisionSurfEmission(2,isurfEmission) = &
                        sqrt(surfaceRetrS(iband)%varianceEmission(index))    ! a-posteriori precision
            precisionSurfEmission(3,isurfEmission) = &
                        surfaceEmission(3,isurfEmission) - surfaceEmission(1,isurfEmission) ! bias


            if( counterSurfaceEmission == nsurfaceEmission ) then
              name = 'surface_emission'
              keyword1     = 'unit'
              keywordList1 = 'wavelengths'
              keywordList2 = 'legend'
              call writeReal(errS, name, x2D = surfaceEmission, &
                             keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                             keywordList1 = keywordList1, real8List_1  = wavelengthSurfEmission, &
                             keywordList2 = keywordList2, stringList_2 = legend )
              if (errorCheck(errS)) return
              name = 'precision_bias_surface_emission'
              call writeReal(errS, name, x2D = precisionSurfEmission, &
                             keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                             keywordList1 = keywordList1, real8List_1  = wavelengthSurfEmission, &
                             keywordList2 = keywordList2, stringList_2 = legend_precision )
              if (errorCheck(errS)) return
            end if


          case('LambCldAlbedo')

            if ( cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then

              ! one cloud albedo for all wavelengths and all spectral bands

              allBands(1) = cloudAerosolRTMgridSimS%albedoLambCldAllBands     ! true cloud albedo
              allBands(2) = cloudAerosolRTMgridRetrS%albedoLambCldAllBandsAP  ! a-priori cloud albedo
              allBands(3) = cloudAerosolRTMgridRetrS%albedoLambCldAllBands    ! a-posteriori cloud albedo

              precisionAllBands(1) = sqrt(cloudAerosolRTMgridRetrS%varAlbedoLambCldAllAP) ! a-priori precision
              precisionAllBands(2) = sqrt(cloudAerosolRTMgridRetrS%varAlbedoLambCldAll)   ! precision
              precisionAllBands(3) = allBands(3) - allBands(1)                            ! bias

              name = 'cloud_albedo'
              keyword1     = 'unit'
              keywordList1 = 'legend'
              call writeReal(errS, name, x1D = allBands, &
                            keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                            keywordList1 = keywordList1, stringList_1 = legend )
              if (errorCheck(errS)) return
              name = 'precision_bias_cloud_albedo'
              call writeReal(errS, name, x1D = precisionAllBands, &
                            keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                            keywordList1 = keywordList1, stringList_1 = legend_precision )
              if (errorCheck(errS)) return

            else

              ! cloud albedo can differ for spectral bands and can be a function of the wavelength

              counterCloudAlbedo = counterCloudAlbedo + 1

              icldAlb = 0
              do iband = 1, retrS%codeSpecBand(istate) - 1
                icldAlb = icldAlb + iband * LambCloudRetrS(iband)%nAlbedo
              end do
              iband    = retrS%codeSpecBand(istate)
              index    = retrS%codeIndexLambCldAlb(istate)
              icldAlb = index + icldAlb

              wavelengthCloudAlb(icldAlb) = LambCloudRetrS(iband)%wavelAlbedo(index)

              ! interpolate Lambertian cloud albedo for simulation to the retrieval wavelength
              cloudAlbedo(1,icldAlb) = polyInt(errS, LambCloudSimS(iband)%wavelAlbedo, &  ! true cloud albedo (interpolated)
                LambCloudSimS(iband)%albedo, LambCloudRetrS(iband)%wavelAlbedo(index))
              cloudAlbedo(2,icldAlb) = LambCloudRetrS(iband)%albedoAP(index)    ! a-priori cloud albedo
              cloudAlbedo(3,icldAlb) = LambCloudRetrS(iband)%albedo(index)      ! retrieved cloud albedo

              precisionCloudAlb(1,icldAlb) = sqrt(LambCloudRetrS(iband)%varianceAlbedoAP(index)) ! a-priori precision
              precisionCloudAlb(2,icldAlb) = sqrt(LambCloudRetrS(iband)%varianceAlbedo(index))   ! a-posteriori precision
              precisionCloudAlb(3,icldAlb) = cloudAlbedo(3,icldAlb) - cloudAlbedo(1,icldAlb)   ! bias


              if( counterCloudAlbedo == ncloudAlbedo ) then
                name = 'cloud_albedo'
                keyword1     = 'unit'
                keywordList1 = 'wavelengths'
                keywordList2 = 'legend'
                call writeReal(errS, name, x2D = cloudAlbedo, &
                               keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                               keywordList1 = keywordList1, real8List_1  = wavelengthCloudAlb, &
                               keywordList2 = keywordList2, stringList_2 = legend )
                if (errorCheck(errS)) return
                name = 'precision_bias_cloud_albedo'
                call writeReal(errS, name, x2D = precisionCloudAlb, &
                               keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                               keywordList1 = keywordList1, real8List_1  = wavelengthCloudAlb, &
                               keywordList2 = keywordList2, stringList_2 = legend_precision )
                if (errorCheck(errS)) return
              end if

            end if ! cloudAerosolRTMgridS%useAlbedoLambCldAllBands

          case('mulOffset')

            counterMulOffset = counterMulOffset + 1

            imulOffset = 0
            do iband = 1, retrS%codeSpecBand(istate) - 1
              imulOffset = imulOffset + iband * mulOffsetRetrS(iband)%nwavel
            end do
            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexMulOffset(istate)
            imulOffset = index + imulOffset

            wavelengthMulOffset(imulOffset) = mulOffsetRetrS(iband)%wavel(index)

            ! interpolate multiplicative offset for simulation to the retrieval wavelength
            mulOffset(1,imulOffset) = polyInt(errS, mulOffsetSimS(iband)%wavel, &      ! true offset (interpolated)
                         mulOffsetSimS(iband)%percentOffset, mulOffsetRetrS(iband)%wavel(index))
            mulOffset(2,imulOffset) = mulOffsetRetrS(iband)%percentOffsetAP(index)    ! a-priori offset
            mulOffset(3,imulOffset) = mulOffsetRetrS(iband)%percentOffset(index)      ! retrieved offset

            precisionMulOffset(1,imulOffset) = sqrt(mulOffsetRetrS(iband)%varMulOffsetAP(index))   ! a-priori precision
            precisionMulOffset(2,imulOffset) = sqrt(mulOffsetRetrS(iband)%varMulOffset(index))     ! precision
            precisionMulOffset(3,imulOffset) = mulOffset(3,imulOffset) - mulOffset(1,imulOffset)   ! bias


            if( counterMulOffset == nmulOffset ) then
              name = 'multiplicative offset radiance'
              keyword1     = 'unit'
              keywordList1 = 'wavelengths'
              keywordList2 = 'legend'
              call writeReal(errS, name, x2D = mulOffset, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, real8List_1  = wavelengthMulOffset, &
                             keywordList2 = keywordList2, stringList_2 = legend )
              if (errorCheck(errS)) return
              name = 'precision_bias_stray_light'
              call writeReal(errS, name, x2D = precisionMulOffset, &
                             keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                             keywordList1 = keywordList1, real8List_1  = wavelengthMulOffset, &
                             keywordList2 = keywordList2, stringList_2 = legend_precision )
              if (errorCheck(errS)) return
            end if

          case('straylight')

            counterStrayLight = counterStrayLight + 1

            istrayLight = 0
            do iband = 1, retrS%codeSpecBand(istate) - 1
              istrayLight = istrayLight + iband * strayLightRetrS(iband)%nwavel
            end do
            iband = retrS%codeSpecBand(istate)
            index = retrS%codeIndexStraylight(istate)
            istrayLight = index + istrayLight

            wavelengthStrayLight(istrayLight) = strayLightRetrS(iband)%wavel(index)

            ! interpolate stray light for simulation to the retrieval wavelength
            strayLight(1,istrayLight) = polyInt(errS, strayLightSimS(iband)%wavel, &      ! true stray light (interpolated)
              strayLightSimS(iband)%percentStrayLight, strayLightRetrS(iband)%wavel(index))
            strayLight(2,istrayLight) = strayLightRetrS(iband)%percentStrayLightAP(index)    ! a-priori stray light
            strayLight(3,istrayLight) = strayLightRetrS(iband)%percentStrayLight(index)      ! retrieved stray light

            precisionStrayLight(1,istrayLight) = sqrt(strayLightRetrS(iband)%varStrayLightAP(index))  ! a-priori precision
            precisionStrayLight(2,istrayLight) = sqrt(strayLightRetrS(iband)%varStrayLight(index))     ! precision
            precisionStrayLight(3,istrayLight) = strayLight(3,istrayLight) - strayLight(1,istrayLight) ! bias


            if( counterStrayLight == nstrayLight ) then
              name = 'stray_light'
              keyword1     = 'unit'
              keywordList1 = 'wavelengths'
              keywordList2 = 'legend'
              call writeReal(errS, name, x2D = strayLight, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, real8List_1  = wavelengthStrayLight, &
                             keywordList2 = keywordList2, stringList_2 = legend )
              if (errorCheck(errS)) return
              name = 'precision_bias_stray_light'
              call writeReal(errS, name, x2D = precisionStrayLight, &
                             keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                             keywordList1 = keywordList1, real8List_1  = wavelengthStrayLight, &
                             keywordList2 = keywordList2, stringList_2 = legend_precision )
              if (errorCheck(errS)) return
            end if

          case('diffRingCoef')

            iband = retrS%codeSpecBand(istate)

            allBands(1) = RRS_RingSimS(iband)%ringCoeff
            allBands(2) = RRS_RingRetrS(iband)%ringCoeffAP
            allBands(3) = RRS_RingRetrS(iband)%ringCoeff

            precisionAllBands(1) = sqrt(RRS_RingRetrS(iband)%varRingCoeffAP)   ! a-priori prescision
            precisionAllBands(2) = sqrt(RRS_RingRetrS(iband)%varRingCoeff)     ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            char_index = achar(48 + iband)
            name         = 'diffRingCoef_band_'//char_index
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return

            name         = 'precision_bias_aerosol_tau'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('RingCoef')

            iband = retrS%codeSpecBand(istate)

            allBands(1) = RRS_RingSimS(iband)%ringCoeff
            allBands(2) = RRS_RingRetrS(iband)%ringCoeffAP
            allBands(3) = RRS_RingRetrS(iband)%ringCoeff

            precisionAllBands(1) = sqrt(RRS_RingRetrS(iband)%varRingCoeffAP)   ! a-priori prescision
            precisionAllBands(2) = sqrt(RRS_RingRetrS(iband)%varRingCoeff)     ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            char_index = achar(48 + iband)
            name         = 'RingCoef_band_'//char_index
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return

            name         = 'precision_bias_aerosol_tau'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('aerosolTau')

            allBands(1) = cloudAerosolRTMgridSimS %intervalAerTau(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalAerTauAP(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalAerTau(cloudAerosolRTMgridSimS%numIntervalFit)

            precisionAllBands(1) = sqrt(cloudAerosolRTMgridRetrS%varAerTauAP)  ! a-priori prescision
            precisionAllBands(2) = sqrt(cloudAerosolRTMgridRetrS%varAerTau)    ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'aerosol_tau'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return

            name         = 'precision_bias_aerosol_tau'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('aerosolSSA')

            allBands(1) = cloudAerosolRTMgridSimS %intervalAerSSA(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalAerSSAAP(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalAerSSA(cloudAerosolRTMgridSimS%numIntervalFit)

            precisionAllBands(1) = sqrt(cloudAerosolRTMgridRetrS%varAerSSAAP) ! a-priori prescision
            precisionAllBands(2) = sqrt(cloudAerosolRTMgridRetrS%varAerSSA)   ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                  ! bias

            name         = 'aerosol_SSA'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_aerosol_SSA'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('aerosolAC')

            allBands(1) = cloudAerosolRTMgridSimS %intervalAerAC  (cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalAerACAP(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalAerAC  (cloudAerosolRTMgridSimS%numIntervalFit)

            precisionAllBands(1) = sqrt(cloudAerosolRTMgridRetrS%varAerACAP)  ! a-priori prescision
            precisionAllBands(2) = sqrt(cloudAerosolRTMgridRetrS%varAerAC)    ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                  ! bias

            name         = 'aerosol_angstrom_coefficient'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return

            name         = 'precision_bias_aerosol_angstrom_coefficient'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('cloudFraction')

            if ( cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then

              ! one cloud fraction for all wavelengths and all spectral bands

              allBands(1) = cloudAerosolRTMgridSimS %cldAerFractionAllBands
              allBands(2) = cloudAerosolRTMgridRetrS%cldAerFractionAllBandsAP
              allBands(3) = cloudAerosolRTMgridRetrS%cldAerFractionAllBands

              precisionAllBands(1) = sqrt(cloudAerosolRTMgridRetrS%varCldAerFractionAllBandsAP) ! a-priori precision
              precisionAllBands(2) = sqrt(cloudAerosolRTMgridRetrS%varCldAerFractionAllBands)   ! precision
              precisionAllBands(3) = allBands(3) - allBands(1)                                  ! bias

              name         = 'cloud_fraction'
              keyword1     = 'unit'
              keywordList1 = 'legend'
              call writeReal(errS, name, x1D = allBands, &
                             keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = legend )
              if (errorCheck(errS)) return
              name         = 'precision_bias_cloud_fraction'
              call writeReal(errS, name, x1D = precisionAllBands, &
                             keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                             keywordList1 = keywordList1, stringList_1 = legend_precision )
              if (errorCheck(errS)) return

            else

              ! cloud fraction can differ for spectral bands and can be a function of the wavelength

              counterCloudFraction = counterCloudFraction + 1

              icloudFraction = 0
              do iband = 1, retrS%codeSpecBand(istate) - 1
                icloudFraction = icloudFraction + iband * cldAerFractionRetrS(iband)%nCldAerFraction
              end do
              iband    = retrS%codeSpecBand(istate)
              index    = retrS%codeIndexCloudFraction(istate)
              icloudFraction = index + icloudFraction

              wavelengthCldAerFraction(icloudFraction) = cldAerFractionRetrS(iband)%wavelCldAerFraction(index)

              ! interpolate cloud fraction for simulation to the retrieval wavelength
              cldAerFraction(1,icloudFraction) = polyInt(errS, cldAerFractionSimS(iband)%wavelCldAerFraction, &
                cldAerFractionSimS(iband)%cldAerFraction, cldAerFractionRetrS(iband)%wavelCldAerFraction(index))
              cldAerFraction(2,icloudFraction) = cldAerFractionRetrS(iband)%cldAerFractionAP(index) ! a-priori
              cldAerFraction(3,icloudFraction) = cldAerFractionRetrS(iband)%cldAerFraction(index)   ! retrieved

              precisionCldAerFraction(1,icloudFraction) = sqrt(cldAerFractionRetrS(iband)%varianceCldAerFractionAP(index))
              precisionCldAerFraction(2,icloudFraction) = sqrt(cldAerFractionRetrS(iband)%varianceCldAerFraction(index))
              precisionCldAerFraction(3,icloudFraction) = cldAerFraction(3,icloudFraction) - cldAerFraction(1,icloudFraction)

              if( counterCloudFraction == nCldAerFraction ) then
                name = 'cloud_aerosol_fraction'
                keyword1     = 'unit'
                keywordList1 = 'wavelengths'
                keywordList2 = 'legend'
                call writeReal(errS, name, x2D = cldAerFraction, &
                               keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                               keywordList1 = keywordList1, real8List_1  = wavelengthCldAerFraction, &
                               keywordList2 = keywordList2, stringList_2 = legend )
                if (errorCheck(errS)) return
                name = 'precision_bias_cloud_aerosol_fraction'
                call writeReal(errS, name, x2D = precisionCldAerFraction, &
                               keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                               keywordList1 = keywordList1, real8List_1  = wavelengthCldAerFraction, &
                               keywordList2 = keywordList2, stringList_2 = legend_precision )
                if (errorCheck(errS)) return
              end if

            end if ! cloudAerosolRTMgridS%useCloudFractionAllBands

          case('cloudTau')

            allBands(1) = cloudAerosolRTMgridSimS %intervalCldTau(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalCldTauAP(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalCldTau(cloudAerosolRTMgridSimS%numIntervalFit)

            precisionAllBands(1) = sqrt(cloudAerosolRTMgridRetrS%varCldTauAP) ! a-priori precision
            precisionAllBands(2) = sqrt(cloudAerosolRTMgridRetrS%varCldTau)   ! precision
            precisionAllBands(3) = allBands(3) - allBands(1)                  ! bias

            name         = 'cloud_Tau'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_cloud_Tau'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('cloudAC')

            allBands(1) = cloudAerosolRTMgridSimS %intervalCldAC  (cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalCldACAP(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalCldAC  (cloudAerosolRTMgridSimS%numIntervalFit)

            precisionAllBands(1) = sqrt(cloudAerosolRTMgridRetrS%varCldACAP)  ! a-priori prescision
            precisionAllBands(2) = sqrt(cloudAerosolRTMgridRetrS%varCldAC)    ! prescision
            precisionAllBands(3) = allBands(3) - allBands(1)                  ! bias

            name         = 'cloud_angstrom_coefficient'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return

            name         = 'precision_bias_cloud_angstrom_coefficient'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1,     string_1     = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('intervalDP')

            allBands(1) = cloudAerosolRTMgridSimS %intervalBounds(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit)

            precisionAllBands(1) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP(cloudAerosolRTMgridSimS%numIntervalFit))
            precisionAllBands(2) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds  (cloudAerosolRTMgridSimS%numIntervalFit))
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'fit_interval_top_altitude'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_cloud_top_altitude'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

            allBands(1) = cloudAerosolRTMgridSimS %intervalBounds(cloudAerosolRTMgridSimS%numIntervalFit - 1)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit - 1)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit - 1)

            precisionAllBands(1) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP(cloudAerosolRTMgridSimS%numIntervalFit - 1))
            precisionAllBands(2) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds  (cloudAerosolRTMgridSimS%numIntervalFit - 1))
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'fit_interval_base_altitude'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_cloud_base_altitude'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

            allBands(1) = cloudAerosolRTMgridSimS %intervalBounds_P(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%numIntervalFit)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalBounds_P(cloudAerosolRTMgridRetrS%numIntervalFit)

            precisionAllBands(1) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP_P(cloudAerosolRTMgridSimS%numIntervalFit))
            precisionAllBands(2) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds_P  (cloudAerosolRTMgridSimS%numIntervalFit))
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'fit_interval_top_pressure'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_cloud_top_pressure'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

            allBands(1) = cloudAerosolRTMgridSimS %intervalBounds_P  (cloudAerosolRTMgridSimS %numIntervalFit - 1)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%numIntervalFit - 1)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalBounds_P  (cloudAerosolRTMgridRetrS%numIntervalFit - 1)

            name         = 'fit_interval_base_pressure'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_cloud_base_pressure'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

            name = 'interval_boundaries_retr_km'
            keyword1     = 'unit'
            string_1     = 'km'
            call writeReal(errS, name, x1D = cloudAerosolRTMgridRetrS %intervalBounds, &
                           keyword1     = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return

            name = 'interval_boundaries_retr_hPa'
            keyword1     = 'unit'
            string_1     = 'hPa'
            call writeReal(errS, name, x1D = cloudAerosolRTMgridRetrS %intervalBounds_P, &
                           keyword1     = keyword1, string_1 = string_1)
            if (errorCheck(errS)) return

          case('intervalTop')

            allBands(1) = cloudAerosolRTMgridSimS %intervalBounds(cloudAerosolRTMgridSimS%numIntervalFit)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit)

            precisionAllBands(1) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP(cloudAerosolRTMgridSimS%numIntervalFit))
            precisionAllBands(2) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds  (cloudAerosolRTMgridSimS%numIntervalFit))
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'fit_interval_top_altitude'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_cloud_top_altitude'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

            allBands(1) = cloudAerosolRTMgridSimS %intervalBounds_P  (cloudAerosolRTMgridSimS %numIntervalFit)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%numIntervalFit)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalBounds_P  (cloudAerosolRTMgridRetrS%numIntervalFit)

            precisionAllBands(1) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP_P(cloudAerosolRTMgridSimS%numIntervalFit))
            precisionAllBands(2) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds_P  (cloudAerosolRTMgridSimS%numIntervalFit))
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'fit_interval_top_pressure'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_cloud_top_pressure'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

          case('intervalBot')

            allBands(1) = cloudAerosolRTMgridSimS %intervalBounds(cloudAerosolRTMgridSimS%numIntervalFit - 1)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalBoundsAP(cloudAerosolRTMgridRetrS%numIntervalFit - 1)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalBounds(cloudAerosolRTMgridRetrS%numIntervalFit - 1)

            precisionAllBands(1) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP(cloudAerosolRTMgridSimS%numIntervalFit - 1))
            precisionAllBands(2) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds  (cloudAerosolRTMgridSimS%numIntervalFit - 1))
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'fit_interval_base_altitude'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_cloud_base_altitude'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return

            allBands(1) = cloudAerosolRTMgridSimS %intervalBounds_P  (cloudAerosolRTMgridSimS %numIntervalFit - 1)
            allBands(2) = cloudAerosolRTMgridRetrS%intervalBoundsAP_P(cloudAerosolRTMgridRetrS%numIntervalFit - 1)
            allBands(3) = cloudAerosolRTMgridRetrS%intervalBounds_P  (cloudAerosolRTMgridRetrS%numIntervalFit - 1)

            precisionAllBands(1) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBoundsAP_P(cloudAerosolRTMgridSimS%numIntervalFit - 1))
            precisionAllBands(2) = &
              sqrt(cloudAerosolRTMgridRetrS%varIntervalBounds_P  (cloudAerosolRTMgridSimS%numIntervalFit - 1))
            precisionAllBands(3) = allBands(3) - allBands(1)                   ! bias

            name         = 'fit_interval_base_pressure'
            keyword1     = 'unit'
            keywordList1 = 'legend'
            call writeReal(errS, name, x1D = allBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend )
            if (errorCheck(errS)) return
            name         = 'precision_bias_cloud_base_pressure'
            call writeReal(errS, name, x1D = precisionAllBands, &
                           keyword1     = keyword1, string_1 = unit_state_vector(istate), &
                           keywordList1 = keywordList1, stringList_1 = legend_precision )
            if (errorCheck(errS)) return


          case default
            call logDebug('incorrect value for retrS%codeFitParameters(counter) : '// retrS%codeFitParameters(istate))
            call logDebug('in subroutine print_AsciiHDF')
            call mystop(errS, 'stopped due to incorrect value for retrS%codeFitParameters(counter)')
            if (errorCheck(errS)) return
        end select

      end do ! istate

      call endGroup

      ! write retrieved trace gas profile
      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case('nodeTrace')

            if ( first_time_profile ) &
              call print_profile_asciiHDF(errS, nTrace, controlSimS, traceGasSimS, traceGasRetrS, retrS,       &
                                          diagnosticS, ncolumn, optPropRTMGridSimS, optPropRTMGridRetrS, &
                                          columnSimS, columnRetrS, controlRetrS)
              if (errorCheck(errS)) return

            first_time_profile = .false.

        end select

      end do ! istate


      groupName = 'error_coveriance_matrices'

      call beginGroup(errS, groupName)
      if (errorCheck(errS)) return

        keywordList1 = 'state_vector_elements'
        keywordList2 = 'unit'
        keywordList3 = 'wavelength'
        keywordList4 = 'nameTraceGas'

        if ( retrS%nstate < maxNumAttribStateVector ) then

          name = 'a_priori_relative_error_covariance'
          call writeReal(errS, name, x2D = retrS%Sa_lnvmr, &
                         keywordList1 = keywordList1, stringList_1 = retrS%codeFitParameters(1:retrS%nstate), &
                         keywordList2 = keywordList2, stringList_2 = unit_covariance, &
                         keywordList3 = keywordList3, real8List_3  = wavelengths, &
                         keywordList4 = keywordList4, stringList_4 = nameTraceGas_state_vector)
          if (errorCheck(errS)) return
          name = 'relative_error_covariance'
          call writeReal(errS, name, x2D = diagnosticS%S_lnvmr, &
                         keywordList1 = keywordList1, stringList_1 = retrS%codeFitParameters(1:retrS%nstate), &
                         keywordList2 = keywordList2, stringList_2 = unit_covariance, &
                         keywordList3 = keywordList3, real8List_3  = wavelengths, &
                         keywordList4 = keywordList4, stringList_4 = nameTraceGas_state_vector)
          if (errorCheck(errS)) return
          name = 'noise_part_relative_error_covariance'
          call writeReal(errS, name, x2D = diagnosticS%Snoise_lnvmr, &
                         keywordList1 = keywordList1, stringList_1 = retrS%codeFitParameters(1:retrS%nstate), &
                         keywordList2 = keywordList2, stringList_2 = unit_covariance, &
                         keywordList3 = keywordList3, real8List_3  = wavelengths, &
                         keywordList4 = keywordList4, stringList_4 = nameTraceGas_state_vector)
          if (errorCheck(errS)) return
          name = 'correlation_coefficients'
          call writeReal(errS, name, x2D = correlation, &
                         keywordList1 = keywordList1, stringList_1 = retrS%codeFitParameters(1:retrS%nstate), &
                         keywordList3 = keywordList3, real8List_3  = wavelengths, &
                         keywordList4 = keywordList4, stringList_4 = nameTraceGas_state_vector)
          if (errorCheck(errS)) return

        else

          name = keywordList1
          call writeString(errS, name, x1D = retrS%codeFitParameters(1:retrS%nstate) )
          if (errorCheck(errS)) return
          name = keywordList2
          call writeString(errS, name, x1D = unit_covariance, keyword1=keyword1, string_1=string_1 )
          if (errorCheck(errS)) return
          name = keywordList3
          call writeReal  (errS, name, x1D = wavelengths )
          if (errorCheck(errS)) return
          name = keywordList4
          call writeString(errS, name, x1D = nameTraceGas_state_vector )
          if (errorCheck(errS)) return

          string_1 = '/error_coveriance_matrices/'//trim(keywordList1)
          string_2 = '/error_coveriance_matrices/'//trim(keywordList2)
          string_3 = '/error_coveriance_matrices/'//trim(keywordList3)
          string_4 = '/error_coveriance_matrices/'//trim(keywordList4)

          name = 'a_priori_relative_error_covariance'
          call writeReal(errS, name, x2D = retrS%Sa_lnvmr, &
                         keyword1 = keywordList1, string_1 = string_1, &
                         keyword2 = keywordList2, string_2 = string_2, &
                         keyword3 = keywordList3, string_3 = string_3, &
                         keyword4 = keywordList4, string_4 = string_4)
          if (errorCheck(errS)) return
          name = 'relative_error_covariance'
          call writeReal(errS, name, x2D = diagnosticS%S_lnvmr, &
                         keyword1 = keywordList1, string_1 = string_1, &
                         keyword2 = keywordList2, string_2 = string_2, &
                         keyword3 = keywordList3, string_3 = string_3, &
                         keyword4 = keywordList4, string_4 = string_4)
          if (errorCheck(errS)) return
          name = 'noise_part_relative_error_covariance'
          call writeReal(errS, name, x2D = diagnosticS%Snoise_lnvmr, &
                         keyword1 = keywordList1, string_1 = string_1, &
                         keyword2 = keywordList2, string_2 = string_2, &
                         keyword3 = keywordList3, string_3 = string_3, &
                         keyword4 = keywordList4, string_4 = string_4)
          if (errorCheck(errS)) return
          name = 'correlation_coefficients'
          call writeReal(errS, name, x2D = correlation, &
                         keyword1 = keywordList1, string_1 = string_1, &
                         keyword3 = keywordList3, string_3 = string_3, &
                         keyword4 = keywordList4, string_4 = string_4)
          if (errorCheck(errS)) return

        end if

      call endGroup

      groupName = 'state_vector_during_iterations'

      call beginGroup(errS, groupName)
      if (errorCheck(errS)) return

        keywordList1 = 'name_state_vector_elements'

        if ( retrS%nstate < maxNumAttribStateVector ) then

          name = 'state_vector'
          call writeReal(errS, name, x2D = retrS%x_stored, &
                         keywordList1 = keywordList1, stringList_1 = retrS%codeFitParameters(1:retrS%nstate) )
          if (errorCheck(errS)) return

        else

          name = keywordList1
          call writeString(errS, name, x1D = retrS%codeFitParameters(1:retrS%nstate) )
          if (errorCheck(errS)) return

          name = 'state_vector'
          string_1 = '/state_vector_during_iterations/'//trim(keywordList1)
          call writeReal(errS, name, x2D = retrS%x_stored)
          if (errorCheck(errS)) return

        end if

      call endGroup

      call endAsciiHDF(errS)
      if (errorCheck(errS)) return

    end subroutine print_AsciiHDF


    subroutine print_profile(errS, nTrace, traceGasSimS, traceGasRetrS, retrS,   &
                             diagnosticS, ncolumn, optPropRTMGridRetrS, &
                             columnSimS, columnRetrS, controlRetrS)

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                  intent(in) :: nTrace                 ! number of trace gases
      type(traceGasType),       intent(in) :: traceGasSimS(nTrace)   ! trace gas properties for simulation
      type(traceGasType),       intent(in) :: traceGasRetrS(nTrace)  ! trace gas properties for retrieval
      type(retrType),           intent(in) :: retrS                  ! parameters for iteratively solving the retrieval
      type(diagnosticType),     intent(in) :: diagnosticS            ! diagnostic information
      integer,                  intent(in) :: ncolumn
      type(optPropRTMGridType), intent(in) :: optPropRTMGridRetrS
      type(columnType),         intent(in) :: columnSimS(ncolumn), columnRetrS(ncolumn)
      type(controlType),        intent(in) :: controlRetrS

      ! local
      integer            :: iwave, ialt, jalt, iTrace, istate
      integer            :: startValue, counter
      real(8), parameter :: DUToCm2 = 2.68668d16
      
      write(outputFileUnit,*)
      write(outputFileUnit,*) 'RESULTS FOR TRACE GAS PROFILE AND SUBCOLUMNS'
      do iTrace = 1, nTrace

        startvalue = 0
        do istate = 1, retrS%nstate
          if(   (retrS%codeFitParameters(istate) == 'nodeTrace' ) .and.  &
                (retrS%codeTraceGas(istate)      == iTrace      ) .and.  &
                (retrS%codeAltitude(istate)      == 0           ) ) then
            startvalue = istate
          end if
        end do
        if ( startvalue == 0 ) cycle  ! go to next value of iTrace

        write(outputFileUnit,*)
        write(outputFileUnit,'(2A)') 'retrieved volume mixing ratio (ppmv) for trace gas ', &
                                     trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A)') '      alt   pressure   a-priori   retrieved      true     diff retr(%) diff ap(%)'
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(f10.3,7ES12.4)') traceGasRetrS(iTrace)%alt(ialt),        &
                                                  traceGasRetrS(iTrace)%pressure(ialt),   &
                                                  traceGasRetrS(iTrace)%vmrAP(ialt),      &
                                                  traceGasRetrS(iTrace)%vmr(ialt),        &
                                                  traceGasSimS (iTrace)%vmr(ialt),        &
               100.0d0*(traceGasRetrS(iTrace)%vmr(ialt)- traceGasSimS(iTrace)%vmr(ialt))  &
                 /traceGasSimS(iTrace)%vmr(ialt),                                         &
               100.0d0*(traceGasRetrS(iTrace)%vmrAP(ialt)-traceGasSimS(iTrace)%vmr(ialt)) &
                 /traceGasSimS(iTrace)%vmr(ialt)
        end do ! ialt

        write(outputFileUnit,*)
        write(outputFileUnit,'(2A)') 'retrieved number density (molecules/cm3) for trace gas ', &
                                     trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A)') '      alt    pressure    a-priori    retrieved     alt        true     '// &
                                    'diff retr(%) diff ap(%)'
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(f10.3,8ES12.4)') traceGasRetrS(iTrace)%alt(ialt),                &
                                                  traceGasRetrS(iTrace)%pressure(ialt),           &
                                                  traceGasRetrS(iTrace)%numDensAP(ialt),          &
                                                  traceGasRetrS(iTrace)%numDens(ialt),            &
                                                  traceGasSimS (iTrace)%alt(ialt),                &
                                                  traceGasSimS (iTrace)%numDens(ialt),            &
               100.0d0*(traceGasRetrS(iTrace)%numDens(ialt)- traceGasSimS(iTrace)%numDens(ialt))  &
                 /traceGasSimS(iTrace)%numDens(ialt),                                             &
               100.0d0*(traceGasRetrS(iTrace)%numDensAP(ialt)-traceGasSimS(iTrace)%numDens(ialt)) &
                 /traceGasSimS(iTrace)%numDens(ialt)
        end do ! ialt

      end do ! iTrace

      write(outputFileUnit,*)
      write(outputFileUnit,'(A)') 'total column molecules/cm2: name     a-priori    true        retrieved   diff true and retr (%)'
      do iTrace = 1, nTrace
        if ( traceGasRetrS(iTrace)%fitProfile ) then
          write(outputFileUnit,'(30X,A5,4E12.4)') traceGasRetrS(iTrace)%nameTraceGas, traceGasRetrS(iTrace)%columnAP,  &
                                             traceGasSimS(iTrace)%column, traceGasRetrS(iTrace)%column,           &
            100.0d0 * (traceGasRetrS(iTrace)%column - traceGasSimS(iTrace)%column) / traceGasSimS(iTrace)%column
        end if
      end do
      write(outputFileUnit,'(A)') 'total column Dobson Units: name     a-priori    true        retrieved   diff true and retr (%)'
      do iTrace = 1, nTrace
        if ( traceGasRetrS(iTrace)%fitProfile ) then
          write(outputFileUnit,'(30X,A5,4F12.4)') traceGasRetrS(iTrace)%nameTraceGas,     &
                                                  traceGasRetrS(iTrace)%columnAP/DUToCm2, &
                                                  traceGasSimS(iTrace)%column/DUToCm2,    &
                                                  traceGasRetrS(iTrace)%column/DUToCm2,   &
            100.0d0 * (traceGasRetrS(iTrace)%column - traceGasSimS(iTrace)%column) / traceGasSimS(iTrace)%column
        end if
      end do

      do iTrace = 1, nTrace
        startvalue = 0
        do istate = 1, retrS%nstate
          if(   (retrS%codeFitParameters(istate) == 'nodeTrace' ) .and.  &
                (retrS%codeTraceGas(istate)      == iTrace      ) .and.  &
                (retrS%codeAltitude(istate)      == 0           ) ) then
            startvalue = istate
          end if
        end do
        if ( startvalue == 0 ) cycle  ! go to next value of iTrace

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'transposed averaging kernel for ln(vmr): A_lnvmr'
        write(outputFileUnit,*) 'columns give the averaging kernel for a certain pressure level'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.2)') 'pressure(km)', &
               (traceGasRetrS(iTrace)%pressure(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        write(outputFileUnit,'(A,100(A9,F8.2))') '            ', &
                            (' lnvmr z=',traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(F8.3, 4X,100E17.8)') traceGasRetrS(iTrace)%alt(ialt), &
                 (diagnosticS%A_lnvmr(jalt+startValue,ialt+startValue), jalt = 0, traceGasRetrS(iTrace)%nprof)
        end do

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'transposed averaging kernel for vmr: A_vmr'
        write(outputFileUnit,*)  trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.2)') 'pressure(km)', &
               (traceGasRetrS(iTrace)%pressure(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        write(outputFileUnit,'(A,100(A9,F8.2))') '            ', &
                            (' vmr   z=',traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(F8.3, 4X,100E17.8)') traceGasRetrS(iTrace)%alt(ialt), &
                    (diagnosticS%A_vmr(jalt+startValue,ialt+startValue), jalt = 0, traceGasRetrS(iTrace)%nprof)
        end do ! ialt

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'transposed averaging kernel for number density): A_ndens'
        write(outputFileUnit,*)  trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.2)') 'pressure(km)', &
               (traceGasRetrS(iTrace)%pressure(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        write(outputFileUnit,'(A,100(A9,F8.2))') '            ', &
                            (' ndens z=',traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(F8.3, 4X,100E17.8)') traceGasRetrS(iTrace)%alt(ialt), &
                  (diagnosticS%A_ndens(jalt+startValue,ialt+startValue), jalt = 0,  traceGasRetrS(iTrace)%nprof)
        end do ! ialt
      end do ! iTrace

      write(outputFileUnit,*) 'relative error based on the diagonal elements of the covariance '
      write(outputFileUnit,*) 'as ln(VMR) is fitted the relative error in vmr is sqrt(diag(S)) '
      do iTrace = 1, nTrace
        startvalue = 0
        do istate = 1, retrS%nstate
          if(   (retrS%codeFitParameters(istate) == 'nodeTrace' ) .and.  &
                (retrS%codeTraceGas(istate)      == iTrace      ) .and.  &
                (retrS%codeAltitude(istate)      == 0           ) ) then
            startvalue = istate
          end if
        end do
        if ( startvalue == 0 ) cycle  ! go to next value of iTrace

        write(outputFileUnit,*)  traceGasRetrS(iTrace)%nameTraceGas
        write(outputFileUnit,*) '   alt  pressure      a-priori   a-posteriori   noise part a-posteriori'
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(f8.3,f8.2,3F14.5)') traceGasRetrS(iTrace)%alt(ialt), &
                                                     traceGasRetrS(iTrace)%pressure(ialt), &
                                                     sqrt(retrS%Sa_lnvmr(ialt+startValue, ialt+startValue)),       &
                                                     sqrt(diagnosticS%S_lnvmr (ialt+startValue, ialt+startValue)), &
                                                     sqrt(diagnosticS%Snoise_lnvmr (ialt+startValue, ialt+startValue))
        end do ! ialt
      end do ! iTrace

      ! a-priori covariance matrices
      do iTrace = 1, nTrace
        startvalue = 0
        do istate = 1, retrS%nstate
          if(   (retrS%codeFitParameters(istate) == 'nodeTrace' ) .and.  &
                (retrS%codeTraceGas(istate)      == iTrace      ) .and.  &
                (retrS%codeAltitude(istate)      == 0           ) ) then
            startvalue = istate
          end if
        end do
        if ( startvalue == 0 ) cycle  ! go to next value of iTrace

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'a-priori covariance matrix for ln(vmr) : Sa_lnvmr'
        write(outputFileUnit,*)  trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.2)') 'altitude(km)', &
                          (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        write(outputFileUnit,'(A,100(A7,F10.3))') '            ', &
                          (' p=',traceGasRetrS(iTrace)%pressure(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(F8.3, 4X,100E17.8)') traceGasRetrS(iTrace)%pressure(ialt), &
                     (retrS%Sa_lnvmr(ialt+startValue,jalt+startValue), jalt = 0, traceGasRetrS(iTrace)%nprof)
        end do ! ialt

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'a-priori covariance matrix for vmr : Sa_vmr (ppmv**2)'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.2)') 'altitude(km)', &
                          (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        write(outputFileUnit,'(A,100(A7,F10.3))') '            ', &
                          (' p=',traceGasRetrS(iTrace)%pressure(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(F8.3, 4X,100E17.8)') traceGasRetrS(iTrace)%pressure(ialt), &
                     (retrS%Sa_vmr(ialt+startValue,jalt+startValue), jalt = 0, traceGasRetrS(iTrace)%nprof)
        end do ! ialt

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'a-priori covariance matrix for number density : Sa_ndens (mol**2 cm**-6)'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.2)') 'altitude(km)', &
                          (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        write(outputFileUnit,'(A,100(A7,F10.3))') '            ', &
                          (' p=',traceGasRetrS(iTrace)%pressure(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(F8.3, 4X,100E17.8)') traceGasRetrS(iTrace)%pressure(ialt), &
                     (retrS%Sa_ndens(ialt+startValue,jalt+startValue), jalt = 0, traceGasRetrS(iTrace)%nprof)
        end do
      end do ! iTrace

      ! a-posteriori covariance matrices
      do iTrace = 1, nTrace
        startvalue = 0
        do istate = 1, retrS%nstate
          if(   (retrS%codeFitParameters(istate) == 'nodeTrace' ) .and.  &
                (retrS%codeTraceGas(istate)      == iTrace      ) .and.  &
                (retrS%codeAltitude(istate)      == 0           ) ) then
            startvalue = istate
          end if
        end do
        if ( startvalue == 0 ) cycle  ! go to next value of iTrace

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'a-posteriori covariance matrix for ln(vmr) : S_lnvmr'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.2)') 'altitude(km)', &
                          (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        write(outputFileUnit,'(A,100(A7,F10.3))') '            ', &
                          (' p=',traceGasRetrS(iTrace)%pressure(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(F8.2, 4X,100E17.8)') traceGasRetrS(iTrace)%pressure(ialt), &
               (diagnosticS%S_lnvmr(ialt+startValue,jalt+startValue), jalt = 0, traceGasRetrS(iTrace)%nprof)
        end do

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'a-posteriori covariance matrix for vmr : S_vmr (ppmv**2)'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.2)') 'altitude(km)', &
                          (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        write(outputFileUnit,'(A,100(A7,F10.3))') '            ', &
                          (' p=',traceGasRetrS(iTrace)%pressure(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(F8.2, 4X,100E17.8)') traceGasRetrS(iTrace)%pressure(ialt), &
               (diagnosticS%S_vmr(ialt+startValue,jalt+startValue), jalt = 0, traceGasRetrS(iTrace)%nprof)
        end do

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'a-posteriori covariance matrix for number density : S_ndens (mol**2 cm**-6)'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.2)') 'altitude(km)', &
                          (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        write(outputFileUnit,'(A,100(A7,F10.3))') '            ', &
                          (' p=',traceGasRetrS(iTrace)%pressure(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          write(outputFileUnit,'(F8.2, 4X,100E17.8)') traceGasRetrS(iTrace)%pressure(ialt), &
               (diagnosticS%S_ndens(ialt+startValue,jalt+startValue), jalt = 0, traceGasRetrS(iTrace)%nprof)
        end do
      end do ! iTrace

      ! transposed gain matrices
      do iTrace = 1, nTrace
        startvalue = 0
        do istate = 1, retrS%nstate
          if(   (retrS%codeFitParameters(istate) == 'nodeTrace' ) .and.  &
                (retrS%codeTraceGas(istate)      == iTrace      ) .and.  &
                (retrS%codeAltitude(istate)      == 0           ) ) then
            startvalue = istate
          end if
        end do
        if ( startvalue == 0 ) cycle  ! go to next value of iTrace

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'Transposed gain matrix G for ln(vmr) * refl : G_lnvmr * refl'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.4)') 'wavelength(nm)', &
                             (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do iwave = 1, retrS%nwavelRetr
          write(outputFileUnit,'(F8.2, 6X,100E17.8)') retrS%wavelRetr(iwave), &
               (diagnosticS%G_lnvmr(ialt+startValue,iwave) * retrS%refl(iwave), ialt = 0, traceGasRetrS(iTrace)%nprof)
        end do

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'Transposed gain matrix G for ln(vmr): G_lnvmr'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.4)') 'wavelength(nm)', &
                             (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do iwave = 1, retrS%nwavelRetr
          write(outputFileUnit,'(F8.2, 6X,100E17.8)') retrS%wavelRetr(iwave), &
               (diagnosticS%G_lnvmr(ialt+startValue,iwave), ialt = 0, traceGasRetrS(iTrace)%nprof)
        end do

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'Transposed gain matrix G for vmr: G_vmr  (vmr in ppmv)'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.4)') 'wavelength(nm)', &
                             (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do iwave = 1, retrS%nwavelRetr
          write(outputFileUnit,'(F8.2, 6X,100E17.8)') retrS%wavelRetr(iwave), &
               (diagnosticS%G_vmr(ialt+startValue,iwave), ialt = 0, traceGasRetrS(iTrace)%nprof)
        end do

        write(outputFileUnit,*)
        write(outputFileUnit,*) 'Transposed gain matrix G for number density: G_ndens'
        write(outputFileUnit,*) 'number density in molecules cm-3'
        write(outputFileUnit,*) trim(traceGasRetrS(iTrace)%nameTraceGas)
        write(outputFileUnit,'(A,100F17.4)') 'wavelength(nm)', &
                             (traceGasRetrS(iTrace)%alt(ialt), ialt = 0, traceGasRetrS(iTrace)%nprof)
        do iwave = 1, retrS%nwavelRetr
          write(outputFileUnit,'(F8.2, 6X,100E17.8)') retrS%wavelRetr(iwave), &
               (diagnosticS%G_ndens(ialt+startValue,iwave), ialt = 0, traceGasRetrS(iTrace)%nprof)
        end do

      end do ! iTrace

      ! write (sub) column properties for the trace gass whose profile is determined
      ! currently the (sub) column properties can not be calculated accurately for
      ! more than one trace gas because of the large dynamic range

      ! determine how much profiles are fitted
      counter = 0
      do iTrace = 1, nTrace
        if ( traceGasRetrS(iTrace)%fitProfile ) then
          counter = counter + 1
        end if
      end do

      ! print column properties only if one profile is fitted
      if ( counter == 1 ) then
        do iTrace = 1, nTrace
          if ( traceGasRetrS(iTrace)%fitProfile ) then
            call writeColumnProperties(errS, ncolumn, nTrace, traceGasSimS, traceGasRetrS, &
                 optPropRTMGridRetrS, columnSimS, columnRetrS, controlRetrS, retrS)
            if (errorCheck(errS)) return
          end if
        end do ! iTrace
      end if ! counter == 1

    end subroutine print_profile


    subroutine print_profile_asciiHDF(errS, nTrace, controlSimS, traceGasSimS, traceGasRetrS, retrS,        &
                                      diagnosticS, ncolumn, optPropRTMGridSimS, optPropRTMGridRetrS,  &
                                      columnSimS, columnRetrS, controlRetrS)

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                  intent(in) :: nTrace                 ! number of trace gases
      type(controlType),        intent(in) :: controlSimS            ! control parameters
      type(traceGasType),       intent(inout) :: traceGasSimS(nTrace)   ! trace gas properties for simulation
      type(traceGasType),       intent(inout) :: traceGasRetrS(nTrace)  ! trace gas properties for retrieval
      type(retrType),           intent(in) :: retrS                  ! parameters for iteratively solving the retrieval
      type(diagnosticType),     intent(in) :: diagnosticS            ! diagnostic information
      integer,                  intent(in) :: ncolumn
      type(optPropRTMGridType), intent(in) :: optPropRTMGridSimS, optPropRTMGridRetrS
      type(columnType),         intent(in) :: columnSimS(ncolumn), columnRetrS(ncolumn)
      type(controlType),        intent(in) :: controlRetrS

      ! local
      integer    :: iwave, ialt, jalt, iTrace, istate
      integer    :: startValue, status
      integer    :: nalt, nwavel

      character(LEN=50) :: groupName, name, keyword
      character(LEN=50) :: keywordList1
      character(LEN=50) :: keyword1
      character(LEN=50) :: string_1
      character(LEN=50) :: legend(3), legend_precision(4)

      real(8)              :: column(3)
      real(8)              :: factor

      real(8), allocatable :: profile(:,:)
      real(8), allocatable :: profile_precision_bias(:,:)

      real(8), allocatable :: SDlnprofSim(:)     ! spline derivatives
      real(8), allocatable :: lnprofSim  (:)     ! interpolated true profile on simulation grid
      real(8), allocatable :: altSim(:)          ! altitude grid for simulation
      real(8), allocatable :: profSimRetrGrid(:) ! profile interpolated to retrieval grid

      real(8), allocatable :: avaragingKernel_relative(:,:)
      real(8), allocatable :: avaragingKernel_vmr(:,:)
      real(8), allocatable :: avaragingKernel_ndens(:,:)
      real(8), allocatable :: gain_relative(:,:)
      real(8), allocatable :: gain_vmr(:,:)
      real(8), allocatable :: gain_ndens(:,:)
      real(8), allocatable :: error_covariance_relative(:,:)
      real(8), allocatable :: error_covariance_vmr(:,:)
      real(8), allocatable :: error_covariance_ndens(:,:)
      real(8), allocatable :: error_covariance_relativeAP(:,:)
      real(8), allocatable :: error_covariance_vmrAP(:,:)
      real(8), allocatable :: error_covariance_ndensAP(:,:)

      real(8), parameter   :: DUToCm2 = 2.68668d16

      integer :: allocStatus, deallocStatus
      integer :: sumallocStatus, sumdeallocStatus

      ! fill legend and legend precision
      legend(1) = 'true'
      legend(2) = 'a_priori'
      legend(3) = 'retrieved'
      legend_precision(1) = 'a_priori_precision'
      legend_precision(2) = 'precision'
      legend_precision(3) = 'precision_noise'
      legend_precision(4) = 'bias'

      ! The first trace gas can be used for profile retrieval
      do iTrace = 1, 1

        allocStatus    = 0
        sumallocStatus = 0

        ! alocate arrays for interpolation of the true profile to the altitude grid for retrieval
        allocate(     SDlnprofSim(0:traceGasSimS(iTrace) %nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate(       lnprofSim(0:traceGasSimS(iTrace) %nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate(          altSim(0:traceGasSimS(iTrace) %nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( profSimRetrGrid(0:traceGasRetrS(iTrace)%nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        ! alocate arrays for the profiles and the error in the  profiles
        allocate( profile(3, traceGasRetrS(iTrace)%nprof+1), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( profile_precision_bias(4, traceGasRetrS(iTrace)%nprof+1), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus

        ! alocate arrays for the averaging kernel, gain, and error covariance
        if ( trim(traceGasRetrS(iTrace)%nameTraceGas) == 'trop_O3' ) then
          nalt = traceGasRetrS(iTrace)%nprof + 1
        else
          nalt = traceGasRetrS(iTrace)%nalt + 1
        end if
        nwavel = retrS%nwavelRetr + 1
        allocate( avaragingKernel_relative(nalt, nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( avaragingKernel_vmr(nalt, nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( avaragingKernel_ndens(nalt, nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( gain_relative(nalt, nwavel), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( gain_vmr(nalt, nwavel), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( gain_ndens(nalt, nwavel), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( error_covariance_relativeAP(nalt, nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( error_covariance_vmrAP(nalt, nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( error_covariance_ndensAP(nalt, nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( error_covariance_relative(nalt, nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( error_covariance_vmr(nalt, nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate( error_covariance_ndens(nalt, nalt), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus

        if ( sumallocStatus /= 0 ) then
          call logDebug('FATAL ERROR: allocation failed')
          call logDebugI('for iTrace = ', iTrace)
          call logDebug('in subroutine print_profile_asciiHDF')
          call logDebug('in module writeModule')
          call mystop(errS, 'stopped because allocation failed')
          if (errorCheck(errS)) return
        end if

        ! determine location where the profile begins in the state vector
        startvalue = 0
        do istate = 1, retrS%nstate
          if(   (retrS%codeFitParameters(istate) == 'nodeTrace' ) .and.  &
                (retrS%codeTraceGas(istate)      == iTrace      ) .and.  &
                (retrS%codeAltitude(istate)      == 0           ) ) then
            startvalue = istate
          end if
        end do
        if ( startvalue == 0 ) cycle  ! go to next value of iTrace

        ! altitude grid for simulation can differ from the altitude grid for retrieval
        ! interpolate the true profile to the retrieval grid using interpolatio on lnvmr

        lnprofSim = log( traceGasSimS(iTrace)%vmr )
        altSim    = traceGasSimS(iTrace)%alt
        if (errorCheck(errS)) return
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          profSimRetrGrid(ialt) = exp( splintLin(errS, altSim, lnprofSim, traceGasRetrS(iTrace)%alt(ialt), status) )
        end do

        groupName = 'profile_'//trim(traceGasRetrS(iTrace)%nameTraceGas)
        call beginGroup(errS, groupName)
        if (errorCheck(errS)) return

        ! write attributes
        keyword = 'name_trace_gas'
        call beginAttributes
        call addAttribute(errS, keyword, traceGasRetrS(iTrace)%nameTraceGas)
        if (errorCheck(errS)) return
        call endAttributes

        ! write DFS for trace gas
        name = 'DFS'
        call writeReal(errS, name, x = diagnosticS%DFStrace(iTrace) )
        if (errorCheck(errS)) return

        name = 'correlation_length'
        keyword1 = 'unit'
        string_1 = 'km'
        if ( traceGasRetrS(iTrace)%useAPCorrLength ) then
          call writeReal(errS, name, x = traceGasRetrS(iTrace)%APcorrLength, keyword1=keyword1, string_1=string_1 )
          if (errorCheck(errS)) return
        else
          call writeReal(errS, name, x = 0.0d0, keyword1=keyword1, string_1=string_1 )
          if (errorCheck(errS)) return
        end if

        ! write altitudes
        name = 'altitude'
        keyword1 = 'unit'
        string_1 = 'km'
        call writeReal(errS, name, x1D = traceGasRetrS(iTrace)%alt, keyword1=keyword1, string_1=string_1 )
        if (errorCheck(errS)) return

        ! write pressures
        name = 'pressure'
        keyword1 = 'unit'
        string_1 = 'hPa'
        call writeReal(errS, name, x1D = traceGasRetrS(iTrace)%pressure, keyword1=keyword1, string_1=string_1 )
        if (errorCheck(errS)) return

        ! write temperature
        name = 'temperature'
        keyword1 = 'unit'
        string_1 = 'K'
        call writeReal(errS, name, x1D = traceGasRetrS(iTrace)%temperature, keyword1=keyword1, string_1=string_1 )
        if (errorCheck(errS)) return

        ! write true, a-priori and retrieved profiles
        do ialt = 0, traceGasRetrS(iTrace)%nprof
          profile(1,ialt+1) = profSimRetrGrid(ialt)                 ! true
          profile(2,ialt+1) = traceGasRetrS(iTrace)%vmrAP(ialt)     ! AP
          profile(3,ialt+1) = traceGasRetrS(iTrace)%vmr(ialt)       ! retrieved
          profile_precision_bias(1,ialt+1) = 1.0d2 * sqrt( traceGasRetrS(iTrace)%covVmrAP(ialt,ialt) ) &
                                           / profile(2,ialt+1)      ! a-priori precision
          profile_precision_bias(2,ialt+1) = 1.0d2 * sqrt( traceGasRetrS(iTrace)%covVmr(ialt,ialt) ) &
                                           / profile(3,ialt+1)      ! a-posteriori precision
          profile_precision_bias(3,ialt+1) = 1.0d2 * &              ! noise precision
             sqrt( diagnosticS%Snoise_lnvmr (ialt+startValue, ialt+startValue) )
          profile_precision_bias(4,ialt+1) = 1.0d2 * ( profile(3,ialt+1) - profile(1,ialt+1) ) / profile(1,ialt+1)
        end do

        name = 'profiles'
        keyword1 = 'unit'
        string_1 = 'ppmv'
        keywordList1 = 'legend'
        call writeReal(errS, name, x2D = profile, &
                       keyword1     = keyword1,     string_1     = string_1, &
                       keywordList1 = keywordList1, stringList_1 = legend )
        if (errorCheck(errS)) return

        name = 'precision_bias_profile'
        keyword1 = 'unit'
        string_1 = 'percent'
        keywordList1 = 'legend'
        call writeReal(errS, name, x2D = profile_precision_bias, &
                       keyword1     = keyword1,     string_1     = string_1, &
                       keywordList1 = keywordList1, stringList_1 = legend_precision )
        if (errorCheck(errS)) return

        ! write total column
        keyword1 = 'unit'
        if ( traceGasRetrS(iTrace)%unitFlag ) then
          factor = 1.0d0 / DUToCm2
          string_1  = 'Dobson Units'
        else
          factor = 1.0d0
          string_1  = 'molecules_per_cm2'
        end if
        column(1) = factor * traceGasSimS(iTrace)%column      ! true column
        column(2) = factor * traceGasRetrS(iTrace)%columnAP   ! a-priori column
        column(3) = factor * traceGasRetrS(iTrace)%column     ! retrieved column

        name = 'total_column'
        call writeReal(errS, name, x1D = column, &
                       keyword1     = keyword1,     string_1     = string_1, &
                       keywordList1 = keywordList1, stringList_1 = legend )
        if (errorCheck(errS)) return

        ! write averaging kernel
        do jalt = 0, traceGasRetrS(iTrace)%nprof
          do ialt = 0, traceGasRetrS(iTrace)%nprof
            avaragingKernel_relative(ialt+1,jalt+1) = diagnosticS%A_lnvmr(ialt+startValue,jalt+startValue)
            avaragingKernel_vmr     (ialt+1,jalt+1) = diagnosticS%A_vmr  (ialt+startValue,jalt+startValue)
            avaragingKernel_ndens   (ialt+1,jalt+1) = diagnosticS%A_ndens(ialt+startValue,jalt+startValue)
          end do
        end do
        name = 'averaging_kernel_relative'
        call writeReal(errS, name, x2D = avaragingKernel_relative)
        if (errorCheck(errS)) return
        name = 'avaragingKernel_vmr'
        call writeReal(errS, name, x2D = avaragingKernel_vmr)
        if (errorCheck(errS)) return
        name = 'avaragingKernel_ndens'
        call writeReal(errS, name, x2D = avaragingKernel_ndens)
        if (errorCheck(errS)) return

        ! write gain
        do iwave = 1, diagnosticS%nwavelRetr
          do jalt = 0, traceGasRetrS(iTrace)%nprof
            gain_relative(jalt+1, iwave+1) = diagnosticS%G_lnvmr(jalt+startValue, iwave)
            gain_vmr     (jalt+1, iwave+1) = diagnosticS%G_vmr  (jalt+startValue, iwave)
            gain_ndens   (jalt+1, iwave+1) = diagnosticS%G_ndens(jalt+startValue, iwave)
          end do
        end do
        name = 'gain_relative'
        call writeReal(errS, name, x2D = gain_relative)
        if (errorCheck(errS)) return
        name = 'gain_vmr'
        call writeReal(errS, name, x2D = gain_vmr)
        if (errorCheck(errS)) return
        name = 'gain_ndens'
        call writeReal(errS, name, x2D = gain_ndens)
        if (errorCheck(errS)) return

        ! write a-priori error covariance
        do jalt = 0, traceGasRetrS(iTrace)%nprof
          do ialt = 0, traceGasRetrS(iTrace)%nprof
            error_covariance_relativeAP(ialt+1,jalt+1) = retrS%Sa_lnvmr(ialt+startValue,jalt+startValue)
            error_covariance_vmrAP     (ialt+1,jalt+1) = retrS%Sa_vmr  (ialt+startValue,jalt+startValue)
            error_covariance_ndensAP   (ialt+1,jalt+1) = retrS%Sa_ndens(ialt+startValue,jalt+startValue)
          end do
        end do
        name = 'error_covariance_relativeAP'
        call writeReal(errS, name, x2D = error_covariance_relativeAP)
        if (errorCheck(errS)) return
        name = 'error_covariance_vmrAP'
        call writeReal(errS, name, x2D = error_covariance_vmrAP)
        if (errorCheck(errS)) return
        name = 'error_covariance_ndensAP'
        call writeReal(errS, name, x2D = error_covariance_ndensAP)
        if (errorCheck(errS)) return

        ! write a-posteriori error covariance
        do jalt = 0, traceGasRetrS(iTrace)%nprof
          do ialt = 0, traceGasRetrS(iTrace)%nprof
            error_covariance_relative(ialt+1,jalt+1) = diagnosticS%S_lnvmr(ialt+startValue,jalt+startValue)
            error_covariance_vmr     (ialt+1,jalt+1) = diagnosticS%S_vmr  (ialt+startValue,jalt+startValue)
            error_covariance_ndens   (ialt+1,jalt+1) = diagnosticS%S_ndens(ialt+startValue,jalt+startValue)
          end do
        end do
        name = 'error_covariance_relative'
        call writeReal(errS, name, x2D = error_covariance_relative)
        if (errorCheck(errS)) return
        name = 'error_covariance_vmr'
        call writeReal(errS, name, x2D = error_covariance_vmr)
        if (errorCheck(errS)) return
        name = 'error_covariance_ndens'
        call writeReal(errS, name, x2D = error_covariance_ndens)
        if (errorCheck(errS)) return

        call endGroup

        if ( traceGasRetrS(iTrace)%fitProfile ) then

          groupName = 'subcolumns_'//trim(traceGasRetrS(iTrace)%nameTraceGas)
          call beginGroup(errS, groupName)
          if (errorCheck(errS)) return

          call writeColumnProperties_asciiHDF(errS, ncolumn, nTrace, 1, controlSimS, traceGasRetrS, &
                  optPropRTMGridSimS, optPropRTMGridRetrS, columnSimS, columnRetrS, controlRetrS)
          if (errorCheck(errS)) return

          call endGroup

        end if

        deallocStatus    = 0
        sumdeallocStatus = 0

        deallocate( SDlnprofSim, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( lnprofSim, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( altSim, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( profSimRetrGrid, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( profile, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( profile_precision_bias, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( avaragingKernel_relative, STAT = deallocStatus)
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( avaragingKernel_vmr, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( avaragingKernel_ndens, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( gain_relative, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( gain_vmr, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( gain_ndens, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( error_covariance_relativeAP, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( error_covariance_vmrAP, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( error_covariance_ndensAP, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( error_covariance_relative, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( error_covariance_vmr, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate( error_covariance_ndens, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus

        if ( sumdeallocStatus /= 0 ) then
          call logDebug('FATAL ERROR: deallocation failed')
          call logDebugI('for iTrace = ', iTrace)
          call logDebug('in subroutine print_profile_asciiHDF')
          call logDebug('in module writeModule')
          call mystop(errS, 'stopped because deallocation failed')
          if (errorCheck(errS)) return
        end if

      end do ! iTrace

    end subroutine print_profile_asciiHDF


    subroutine writeHRwavelGrid(errS,  numSpectrBands, wavelHRS )

      ! write high resolution wavelength grid and corresponding weights for Gaussian integration

      implicit none

      type(errorType),   intent(inout) :: errS
      integer,           intent(in)    :: numSpectrBands
      type(wavelHRType), intent(in)    :: wavelHRS(numSpectrBands)

      ! local
      integer           :: iband
      character(LEN=1)  :: char_index
      character(LEN=50) :: name
      character(LEN=50) :: keyword1
      character(LEN=50) :: string_1

      do iband = 1, numSpectrBands
        char_index = achar(48 + iband)
        name = 'HR_wavelength_band_'//char_index
        keyword1     = 'unit'
        string_1     = 'nm'
        call writeReal(errS, name, x1D = wavelHRS(iband)%wavel(:), &
                       keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return
      end do ! iband
      do iband = 1, numSpectrBands
        char_index = achar(48 + iband)
        name = 'HR_weights_band_'//char_index
        keyword1     = 'unit'
        string_1     = 'per nm'
        call writeReal(errS, name, x1D = wavelHRS(iband)%weight(:), &
                       keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return
      end do ! iband

    end subroutine writeHRwavelGrid



    subroutine writeInstrWavelGrid(errS,  numSpectrBands, wavelInstrS, name )

      ! write high resolution wavelength grid and corresponding weights for Gaussian integration

      implicit none

      type(errorType), intent(inout) :: errS
      integer,              intent(in) :: numSpectrBands
      type(wavelInstrType), intent(in) :: wavelInstrS(numSpectrBands)
      character(LEN=50),    intent(in) :: name

      ! local
      integer           :: iband
      character(LEN=1)  :: char_index
      character(LEN=50) :: string
      character(LEN=50) :: keyword1
      character(LEN=50) :: string_1

      do iband = 1, numSpectrBands
        char_index = achar(48 + iband)
        string = trim(name)//'_band_'//char_index
        keyword1     = 'unit'
        string_1     = 'nm'
        call writeReal(errS, string, x1D = wavelInstrS(iband)%wavel(:), &
                       keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return
      end do ! iband

    end subroutine writeInstrWavelGrid

    subroutine writeXsecHRLUT(errS, iband, iTrace, wavelHRS, XsecHRLUTS, traceGasS )

      ! write high resolution wavelength grid, corresponding weights for Gaussian integration,
      ! and (parameters for) absorption cross section LUT for a specific spectral band and a specific
      ! trace gas

      implicit none

      type(errorType),    intent(inout) :: errS
      integer,            intent(in)    :: iband, iTrace
      type(wavelHRType),  intent(in)    :: wavelHRS
      type(XsecLUTType),  intent(in)    :: XsecHRLUTS
      type(traceGasType), intent(inout) :: traceGasS                      ! trace gas properties


      ! local
      character(LEN=1)  :: char_index, char_index_band, char_index_trace
      character(LEN=50) :: name, groupName
      character(LEN=50) :: keyword1
      character(LEN=50) :: string_1

      char_index_band   = achar(48 + iband)
      char_index_trace  = achar(48 + iTrace)
      groupName = 'XsecLUT_band'//char_index_band//'_traceGas_'//char_index_trace

      call beginGroup(errS, groupName)

        char_index = achar(48 + iband)
        name = 'HR_wavelength_band_'//char_index
        keyword1     = 'unit'
        string_1     = 'nm'
        call writeReal(errS, name, x1D = wavelHRS%wavel(:), &
                       keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return

        char_index = achar(48 + iband)
        name = 'HR_weights_band_'//char_index
        keyword1     = 'unit'
        string_1     = 'per nm'
        call writeReal(errS, name, x1D = wavelHRS%weight(:), &
                       keyword1 = keyword1, string_1 = string_1)
        if (errorCheck(errS)) return

        name = 'absorbing_gas'
        call writeString(errS, name, x = traceGasS%nameTraceGas)

        name = 'Tmin'
        call writeReal(errS, name, x = XsecHRLUTS%Tmin_LUT )
        name = 'Tmax'
        call writeReal(errS, name, x = XsecHRLUTS%Tmax_LUT )
        name = 'pmin'
        call writeReal(errS, name, x = XsecHRLUTS%pmin_LUT )
        name = 'pmax'
        call writeReal(errS, name, x = XsecHRLUTS%pmax_LUT )
        name = 'coeff_lnTlnp_LUT'
        call writeReal(errS, name, x3D = XsecHRLUTS%coeff_lnTlnp_LUT )

      call endGroup

    end subroutine writeXsecHRLUT

    subroutine writeASCIIinputOMO3PR(errS, numSpectrBands, controlS, geometryS, gasPTS, &
                                     cloudAerosolRTMgridS, OptPropRTMGridS, wavelInstrS, &
                                     surfaceS, solarIrradianceS, earthRadianceS, retrS)

    ! Write an ASCII text file that can be used as input fot the operatinal
    ! OMI ozone profile algorithm. This can be used for testing purposes.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in) :: numSpectrBands                   ! number of spectral bands
      type(controlType),             intent(in) :: controlS                         ! control parameters
      type(geometryType),            intent(in) :: geometryS                        ! geometrical information
      type(gasPTType),               intent(in) :: gasPTS                           ! atmospheric pressure and temperature
      type(cloudAerosolRTMgridType), intent(in) :: cloudAerosolRTMgridS             ! cloud-aerosol properties
      type(optPropRTMGridType),      intent(in) :: optPropRTMGridS                  ! optical properties on RTM grid
      type(wavelInstrType),          intent(in) :: wavelInstrS(numSpectrBands)      ! wavelength grid for measurement
      type(LambertianType),          intent(in) :: surfaceS(numSpectrBands)         ! surface properties
      type(solarIrrType),            intent(in) :: solarIrradianceS(numSpectrBands) ! solar irradiance
      type(earthRadianceType),       intent(in) :: earthRadianceS(numSpectrBands)   ! earth radiance
      type(retrType),                intent(in) :: retrS                            ! contains reflectance

      ! local
      integer         :: OpenError
      integer         :: iband

      integer         :: NumberOfMeasurements, numStreamsLIDORT
      integer         :: year, month, day

      real(8)         :: DeltaLambda_UV1, DeltaLambda_UV2, Border_UV1_UV2
      logical         :: replaceCabRamanByRayOpera ! use Rayleigh in OPERA
      logical         :: replaceCabRamanByRaySpec  ! use Rayleigh when simulating spectra
      logical         :: useSingleScattering, useDeltaM

      real(8)         :: layerColumnO3(0:gasPTS%npressureNodes),  layerColumnNO2(0:gasPTS%npressureNodes)
      real(8)         :: layerColumnSO2(0:gasPTS%npressureNodes), layerColumnAir(0:gasPTS%npressureNodes)

      real(8)         :: lat, lon
      real(8)         :: SolarZenithAngleGround
      real(8)         :: cloudPressure, cloudAltitude
      integer         :: nXtrackUV1, nXtrackUV2
      integer         :: wavelStartRefl
      integer         :: statusSplint


      open(UNIT= ASCIIinputOMO3PRUnit, ACTION= 'WRITE', STATUS = 'replace', IOSTAT= OpenError, &
           FILE = controlS%ASCIIinputOMO3PR_FileName)
      if (OpenError /= 0) then
         write(errS%temp,*) 'writeASCIIinputOMO3PR: Error in opening file for writing OMO3PR text input: ', &
                      controlS%ASCIIinputOMO3PR_FileName
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped because file could not be opened')
         if (errorCheck(errS)) return
      end if

      call writeHeader

      ! set OPF parameter values
      DeltaLambda_UV1           = 0.90d0
      DeltaLambda_UV2           = 0.60d0
      Border_UV1_UV2            = 310.0d0
      numStreamsLIDORT          = 6
      replaceCabRamanByRayOpera = .true.
      replaceCabRamanByRaySpec  = .true.
      useSingleScattering       = .true.
      useDeltaM                 = .false.
      year                      = 2005
      month                     = 6
      day                       = 15

      call writeOPFparam (errS,          &
           DeltaLambda_UV1,              &
           DeltaLambda_UV2,              &
           Border_UV1_UV2,               &
           numStreamsLIDORT,             &
           replaceCabRamanByRayOpera,    &
           replaceCabRamanByRaySpec,     &
           useSingleScattering,          &
           controlS%useSphericalCorr,    &
           useDeltaM,                    &
           Year,                         &
           Month,                        &
           Day                           &
            )
      if (errorCheck(errS)) return

       NumberOfMeasurements = 1
       layerColumnO3  = 0.0d0
       layerColumnNO2 = 0.0d0
       layerColumnSO2 = 0.0d0
       layerColumnAir = 0.0d0

       cloudAltitude = OptPropRTMGridS%upperBoundFitInterval
       cloudPressure = splintLin(errS, gasPTS%alt, gasPTS%pressure, cloudAltitude, statusSplint)

       call writeTempPresProfile (errS, &
            gasPTS,                     &
            cloudPressure,              &
            NumberOfMeasurements        &
           )
       if (errorCheck(errS)) return

       Lat = 45.0d0
       Lon = 20.0d0
       nXtrackUV1  = 15
       nXtrackUV2  = 30
       SolarZenithAngleGround =  geometryS%sza

       wavelStartRefl = 0

       do iband = 1, numSpectrBands

         if (iband == 1) then
           call writeSimulatedSpectra1 (errS,                   &
                wavelInstrS(iband)%nwavel,                      &
                wavelInstrS(iband)%wavel,                       &
                Lat,                                            &
                Lon,                                            &
                surfaceS(iband)%albedo(1),                      &
                cloudAerosolRTMgridS%albedoLambCldAllBands,     &
                cloudAerosolRTMgridS%cldAerFractionAllBands,    &
                cloudPressure,                                  &
                gasPTS%alt(0),                                  &
                SolarZenithAngleGround,                         &
                geometryS%vza,                                  &
                geometryS%dphi,                                 &
                nXtrackUV1,                                     &
                nXtrackUV2,                                     &
                solarIrradianceS(iband)%solIrrMeas,             &
                earthRadianceS(iband)%rad_meas,                 &
                retrS%reflMeas,                                 &
                retrS%reflNoiseError                            &
               )
           if (errorCheck(errS)) return

         else

           call writeSimulatedSpectra2 (errS,        &
                wavelInstrS(iband)%nwavel,           &
                wavelInstrS(iband)%wavel,            &
                wavelStartRefl,                      &
                solarIrradianceS(iband)%solIrrMeas,  &
                earthRadianceS(iband)%rad_meas,      &
                retrS%reflMeas,                      &
                retrS%reflNoiseError                 &
                )
           if (errorCheck(errS)) return

         end if

         wavelStartRefl = wavelStartRefl + wavelInstrS(iband)%nwavel + 2

       end do

       close(UNIT= ASCIIinputOMO3PRUnit)


    end subroutine writeAsCIIinputOMO3PR


    subroutine writeHeader

      ! write header information at the beginning of the file
      ! to identify the test case involved

      implicit none

      write(ASCIIinputOMO3PRUnit, *) '#=========================================='
      write(ASCIIinputOMO3PRUnit, *) '#ASCII test data for OMI ozone profile PGE '
      write(ASCIIinputOMO3PRUnit, *) '#Data generated with LABOS '
      write(ASCIIinputOMO3PRUnit, *) '#Version number = 1.0'
      write(ASCIIinputOMO3PRUnit, *) '#Data generated by Johan F. de Haan : haandej@knmi.nl  '
      write(ASCIIinputOMO3PRUnit, *) '#=========================================='
      write(ASCIIinputOMO3PRUnit, *) ''

    end subroutine writeHeader


    subroutine writeOPFparam (errS, &
      DeltaLambda_UV1,           &
      DeltaLambda_UV2,           &
      Border_UV1_UV2,            &
      numStreamsLIDORT,          &
      replaceCabRamanByRayOpera, &
      replaceCabRamanByRaySpec,  &
      useSingleScattering,       &
      usePseudoSpherical,        &
      useDeltaM,                 &
      Year,                      &
      Month,                     &
      Day                        &
     )

      implicit none

      type(errorType), intent(inout) :: errS
      real(8),    intent(in) :: DeltaLambda_UV1    ! interval used for convolution Chebyshev with slit
      real(8),    intent(in) :: DeltaLambda_UV2    ! interval used for convolution Chebyshev with slit
      real(8),    intent(in) :: Border_UV1_UV2
      integer,    intent(in) :: numStreamsLIDORT
      logical,    intent(in) :: usePseudoSpherical, useDeltaM
      logical,    intent(in) :: useSingleScattering
      logical,    intent(in) :: replaceCabRamanByRayOpera ! use Rayleigh in OPERA
      logical,    intent(in) :: replaceCabRamanByRaySpec  ! use Rayleigh when simulating spectra
      integer,    intent(in) :: Year, Month, Day

      ! local
      integer :: IntusePseudoSpherical, IntuseDeltaM
      integer :: IntuseSingleScattering
      integer :: IntreplaceCabRamanByRayOpera
      integer :: IntreplaceCabRamanByRaySpec
      integer :: IntUsePolarizationCorrection

      ! cast Booleans to integers
      if (usePseudoSpherical) then
        IntusePseudoSpherical = 1
      else
        IntusePseudoSpherical = 0
      end if
      if (useDeltaM) then
        IntuseDeltaM = 1
      else
        IntuseDeltaM = 0
      end if
      if (useSingleScattering) then
        IntuseSingleScattering = 1
      else
        IntuseSingleScattering = 0
      end if
      if (replaceCabRamanByRayOpera) then
        IntreplaceCabRamanByRayOpera = 1
      else
        IntreplaceCabRamanByRayOpera = 0
      end if
      if (replaceCabRamanByRaySpec) then
        IntreplaceCabRamanByRaySpec = 1
      else
        IntreplaceCabRamanByRaySpec = 0
      end if
      IntUsePolarizationCorrection = 0


      write(ASCIIinputOMO3PRUnit, *)  'start OPF parameters'
      write(ASCIIinputOMO3PRUnit, '(I10, A25)') numStreamsLIDORT,      '    # numStreamsLIDORT    '
      write(ASCIIinputOMO3PRUnit, '(I10, A25)') IntuseSingleScattering,'    # useSingleScattering '
      write(ASCIIinputOMO3PRUnit, '(I10, A25)') IntuseDeltaM,          '    # useDeltaM           '
      write(ASCIIinputOMO3PRUnit, '(I10, A25)') IntusePseudoSpherical, '    # usePseudoSpherical  '
      write(ASCIIinputOMO3PRUnit, '(I10, A48)') IntreplaceCabRamanByRayOpera,  '    # replaceCabannesRamanByRayleigh '
      write(ASCIIinputOMO3PRUnit, '(A1, I9, A56)') '#', IntreplaceCabRamanByRaySpec,   '    # replaceCabannesRamanByRayleigh '
      write(ASCIIinputOMO3PRUnit, '(I10, A43)') IntUsePolarizationCorrection,  '    # usePolarizationCorrection (in opera) '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A25)') DeltaLambda_UV1,     '    # DeltaLambda_UV1     '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A25)') DeltaLambda_UV2,     '    # DeltaLambda_UV2     '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A25)') Border_UV1_UV2,      '    # Border_UV1_UV2      '
      write(ASCIIinputOMO3PRUnit, '(I4,A1,I2.2,A1,I2.2, A25)') Year,'-', Month, '-', Day, '    # UTCfirstMeasurement '
      write(ASCIIinputOMO3PRUnit, *)  'end OPF parameters'
      write(ASCIIinputOMO3PRUnit, *)  ' '

    end subroutine writeOPFparam


    subroutine writeTempPresProfile (errS, &
               gasPTS,                     &
               cloudPressure,              &
               NumberOfMeasurements        &
               )


      implicit none

      type(errorType), intent(inout) :: errS
      type(gasPTType),              intent(in) :: gasPTS
      real(8),                      intent(in) :: cloudPressure
      integer,                      intent(in) :: NumberOfMeasurements

      ! local
      integer :: i, statusSpline, statusSplint
      integer, parameter :: nlevelOMI = 18
      real(8) :: pressureOMI (0:nlevelOMI), temperatureOMI(0:nlevelOMI), altitudeOMI(0:nlevelOMI)
      real(8) :: temperatureOMIav(0:nlevelOMI)
      real(8) :: SDaltitude(0:gasPTS%npressure)    ! spline derivatives for altitude
      real(8) :: SDtemperature(0:gasPTS%npressure) ! spline derivatives for temperature

      integer    :: indexClosestPressureLevel(1)

      ! basic OMI pressure grid
      pressureOMI( 0)=  gasPTS%pressure(0)
      pressureOMI( 1)=  700.0d0
      pressureOMI( 2)=  500.0d0
      pressureOMI( 3)=  300.0d0
      pressureOMI( 4)=  200.0d0
      pressureOMI( 5)=  150.0d0
      pressureOMI( 6)=  100.0d0
      pressureOMI( 7)=   70.0d0
      pressureOMI( 8)=   50.0d0
      pressureOMI( 9)=   30.0d0
      pressureOMI(10)=   20.0d0
      pressureOMI(11)=   10.0d0
      pressureOMI(12)=    7.0d0
      pressureOMI(13)=    5.0d0
      pressureOMI(14)=    3.0d0
      pressureOMI(15)=    2.0d0
      pressureOMI(16)=    1.0d0
      pressureOMI(17)=    0.5d0
      pressureOMI(18)=    0.3d0

      ! shift closest pressure level to cloud pressure level
      indexClosestPressureLevel = minLoc(abs(pressureOMI - cloudPressure))
      ! arrays start at 0 and minLoc assumes that the start at 1 => subtract 1
      indexClosestPressureLevel(1) = indexClosestPressureLevel(1) - 1
      ! do not shift surface pressure
      if( indexClosestPressureLevel(1) == 0 ) indexClosestPressureLevel(1) = 1
      pressureOMI( indexClosestPressureLevel(1) ) = cloudPressure

      call spline(errS, gasPTS%pressure, gasPTS%temperature, SDtemperature, statusSpline)
      if (errorCheck(errS)) return
      call spline(errS, gasPTS%pressure, gasPTS%alt,         SDaltitude,    statusSpline)
      if (errorCheck(errS)) return

      do i = 0, nlevelOMI
        temperatureOMI(i) = splint(errS, gasPTS%pressure, gasPTS%temperature, SDtemperature, pressureOMI(i), statusSplint)
        altitudeOMI(i)    = splint(errS, gasPTS%pressure, gasPTS%alt,         SDaltitude,    pressureOMI(i), statusSplint)
      end do

      do i = 0, nlevelOMI - 1
        temperatureOMIav(i) = 0.5 * (temperatureOMI(i) + temperatureOMI(i+1) )
      end do
      temperatureOMIav(nlevelOMI) = 0.0d0

      write(ASCIIinputOMO3PRUnit, *)  ''
      write(ASCIIinputOMO3PRUnit, *)  '# pressure and temperature profiles + column densities of air and O3'
      write(ASCIIinputOMO3PRUnit, *)  '# P and alt are given for the the atmospheric levels'
      write(ASCIIinputOMO3PRUnit, *)  '# T (average of layer) , n_air, n_O3, n_NO2, n_SO2, and b_aer'
      write(ASCIIinputOMO3PRUnit, *)  '#  are quantities for the layers (b_aer is given for the reference: 400 nm)'
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit, '(A97)')  '#   P(hPa)      T(K)     alt(km)'
      write(ASCIIinputOMO3PRUnit, *)  'start profiles'
      do i = 0, nlevelOMI
        write(ASCIIinputOMO3PRUnit, '(37ES12.4)') pressureOMI(i), temperatureOMIav(i), altitudeOMI(i)
      end do
      write(ASCIIinputOMO3PRUnit, *)  'end profiles'
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit, '(I10, A25)') NumberOfMeasurements,  '    # Number of pixels    '
      write(ASCIIinputOMO3PRUnit, *)  ' '

    end subroutine writeTempPresProfile


    subroutine writeSimulatedSpectra1 (errS,     &
      NWavelengths,             &
      MeasWavelengths,          &
      Lat,                      &
      Lon,                      &
      SurfaceAlbedo,            &
      CloudAlbedo,              &
      CloudFraction,            &
      CloudPressure,            &
      TerrainHeight,            &
      SolarZenithAngleGround,   &
      ViewingZenithAngleGround, &
      AzimuthGround,            &
      nXtrackUV1,               &
      nXtrackUV2,               &
      SunIrrad,                 &
      EarthRad,                 &
      reflect,                  &
      Reflect_Precision         &
     )

      implicit none

      type(errorType), intent(inout) :: errS
      integer, intent(in)    :: NWavelengths
      integer, intent(in)    :: nXtrackUV1, nXtrackUV2
      real(8), intent(in)    :: Lat, Lon
      real(8), intent(in)    :: SurfaceAlbedo, CloudAlbedo, CloudFraction
      real(8), intent(in)    :: CloudPressure, TerrainHeight
      real(8), intent(in)    :: SolarZenithAngleGround
      real(8), intent(in)    :: ViewingZenithAngleGround
      real(8), intent(in)    :: AzimuthGround
      real(8), intent(in)    :: MeasWavelengths(0:NWavelengths)
      real(8), intent(in)    :: SunIrrad(0:NWavelengths)
      real(8), intent(in)    :: EarthRad(0:NWavelengths)
      real(8), intent(in)    :: Reflect(0:NWavelengths)
      real(8), intent(in)    :: Reflect_Precision(0:NWavelengths)

      ! local
      integer    :: i

      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit, *)  'start pixel'
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit, '(I10, A23)') NWavelengths,     '  # NWavelengths     '
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit, '(I10, A23)') nXtrackUV1,       '  # nXtrackUV1       '
      write(ASCIIinputOMO3PRUnit, '(I10, A23)') nXtrackUV2,       '  # nXtrackUV2       '
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A23)') Lat,            '  # Latitude         '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A23)') Lon,            '  # Longitude        '
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A23)') SurfaceAlbedo,  '  # SurfaceAlbedo    '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A23)') CloudAlbedo,    '  # CloudAlbedo      '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A23)') CloudFraction,  '  # CloudFraction    '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A23)') CloudPressure,  '  # CloudPressure    '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A23)') TerrainHeight*1000.0d0,  '  # TerrainHeight(m) '
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A40)') SolarZenithAngleGround,     '  # SolarZenithAngle at Ground        '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A40)') ViewingZenithAngleGround,   '  # ViewingZenithAngle at Ground      '
      write(ASCIIinputOMO3PRUnit, '(F10.4, A40)') AzimuthGround,              '  # Azimuth difference at Ground      '
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit,*) '# wavelength in nm'
      write(ASCIIinputOMO3PRUnit,*) '# solar irradiance in ph/cm2/s/nm'
      write(ASCIIinputOMO3PRUnit,*) '# earth radiance in ph/cm2/s/nm/sr'
      write(ASCIIinputOMO3PRUnit,*) '# reflectance here is sun-normalized radiance = earth rad / solar irr'
      write(ASCIIinputOMO3PRUnit, *)  ' '
      write(ASCIIinputOMO3PRUnit,*) '# wavelength   solar irr     earth rad   reflectance  refl_precision'
      write(ASCIIinputOMO3PRUnit,*) 'start spectra'
      do i = 0, NWavelengths
        write (ASCIIinputOMO3PRUnit, '(f12.5, 4ES14.5)') MeasWavelengths(i), SunIrrad(i), EarthRad(i), reflect(i), &
                                       Reflect_Precision(i)
      end do

      return

    end subroutine writeSimulatedSpectra1


    subroutine writeSimulatedSpectra2 (errS,     &
      NWavelengths,             &
      MeasWavelengths,          &
      wavelStartRefl,           &
      SunIrrad,                 &
      EarthRad,                 &
      reflect,                  &
      Reflect_Precision         &
     )

      implicit none

      type(errorType), intent(inout) :: errS
      integer,    intent(in) :: NWavelengths
      real(8),    intent(in) :: MeasWavelengths(0:NWavelengths)
      integer,    intent(in) :: wavelStartRefl
      real(8),    intent(in) :: SunIrrad(0:NWavelengths)
      real(8),    intent(in) :: EarthRad(0:NWavelengths)
      real(8),    intent(in) :: Reflect(:)
      real(8),    intent(in) :: Reflect_Precision(:)

      ! local
      integer    :: i

      do i = 0, NWavelengths
          write (ASCIIinputOMO3PRUnit, '(f12.5, 4ES14.5)') MeasWavelengths(i), SunIrrad(i), EarthRad(i), &
           reflect(i + wavelStartRefl), Reflect_Precision(i + wavelStartRefl)
      end do
      write(ASCIIinputOMO3PRUnit, *)  'end spectra'
      write(ASCIIinputOMO3PRUnit, *)  'end pixel'

    end subroutine writeSimulatedSpectra2


    subroutine writeSNR(errS, wavelInstrS, earthRadianceS)

      ! write signal no noise ratio to additional output

      implicit none

      type(errorType), intent(inout) :: errS
      type(wavelInstrType),      intent(in) :: wavelInstrS(:)      ! wavelength grid for (simulated) measurements
      type(earthRadianceType),   intent(in) :: earthRadianceS(:)   ! contains SNR information

      integer    :: iwave, iband, nband

      nband = size( wavelInstrS )

      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'Instrument wavelength grid: signal to noise ratio'
      do iband = 1, nband
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'wavelength      SNR_reference  SNR_radiance  SNR_reflectance'
        do iwave = 1,  wavelInstrS(iband)%nwavel
          write(addtionalOutputUnit,'(F12.6, 3F15.4)') wavelInstrS(iband)%wavel(iwave), &
                                                earthRadianceS(iband)%SNrefspec(iwave), &
                                                earthRadianceS(iband)%SN       (iwave), &
                                                earthRadianceS(iband)%SNrefl   (iwave)

        end do ! iwave
        write(addtionalOutputUnit,*)
      end do ! iband

    end subroutine writeSNR


    subroutine writeRingSpec(errS, wavelInstrS, RRS_RingS)

      ! write Ring spectra to additional output

      implicit none

      type(errorType), intent(inout) :: errS
      type(wavelInstrType), intent(in) :: wavelInstrS(:)    ! wavelength grid for (simulated) measurements
      type(RRS_RingType),   intent(in) :: RRS_RingS(:)      ! RRS

      integer    :: iwave, iband, nband
      real(8)    :: sumradRing, sumRTMradRing

      nband = size( wavelInstrS )

      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'Ring spectra on instrument wavelength grid'
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'There are two types of Ring spectra'
      write(addtionalOutputUnit,*) '- calculated using radiative transfer'
      write(addtionalOutputUnit,*) '- calculated directly from the solar irradiance'
      write(addtionalOutputUnit,*) '- the second type is also called Fraunhofer Ring spectra'
      write(addtionalOutputUnit,*) 'to get values for the first type'
      write(addtionalOutputUnit,*) 'the flag useRRS or the flag approximateRRS should be 1'
      write(addtionalOutputUnit,*) 'in the section RRS_RING in the configuration file'

      do iband = 1, nband
        sumradRing    = sum(RRS_RingS(iband)%radRing(:))
        sumRTMradRing = sum(RRS_RingS(iband)%RTMradRing(:))
        if ( RRS_RingS(iband)%useRRS .or. RRS_RingS(iband)%approximateRRS) then
          write(addtionalOutputUnit,*)
          write(addtionalOutputUnit,*) 'Ring spectrum / elastic and inelastic radiance calculated using radiative transfer'
          write(addtionalOutputUnit,*) 'radiance at TOA is in in photons/s/cm^2/nm/sr'
          write(addtionalOutputUnit,*) 'RTMRing(IR/F) is the Ring spectrum calculated with radiative transfer'
          write(addtionalOutputUnit,'(A)') ' wavelength    RTMelastic         RTMinelastic(IR)   ' &
                                           //'      RTMRing(IR/F)        fractionRRS'
          do iwave = 1,  wavelInstrS(iband)%nwavel
            write(addtionalOutputUnit,'(F12.6, 5ES20.8)') wavelInstrS(iband)%wavel(iwave),     &
                                                          RRS_RingS(iband)%RTMradElas(iwave),  &
                                                          RRS_RingS(iband)%RTMradRing(iwave),  &
                                                          RRS_RingS(iband)%RTMRing(iwave),     &
                                                          RRS_RingS(iband)%RTMradRing(iwave) / &
                     ( RRS_RingS(iband)%RTMradRing(iwave) + RRS_RingS(iband)%RTMradElas(iwave) )
          end do ! iwave
        end if

        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'Fraunhofer Ring spectra'
        write(addtionalOutputUnit,*) 'radRing (IR) is the solar irradiance convoluted with the RRS lines and slit function'
        write(addtionalOutputUnit,*) 'Ring is the inelastic part of the sun-normalized radiance; i.e.radRing/solarirr'
        write(addtionalOutputUnit,*) '     which is divided by the cross section for Raman scattering'
        write(addtionalOutputUnit,*) ' wavelength       radRing         Ring(IR/F)'
        do iwave = 1,  wavelInstrS(iband)%nwavel
          write(addtionalOutputUnit,'(F12.6, ES20.8, F16.8)') wavelInstrS(iband)%wavel(iwave), &
                                                          RRS_RingS(iband)%radRing(iwave), &
                                                          RRS_RingS(iband)%Ring(iwave)

        end do ! iwave
        write(addtionalOutputUnit,*)
      end do ! iband

    end subroutine writeRingSpec


    subroutine writeDiffRingSpec(errS, wavelInstrS, RRS_RingS)

      ! write Ring spectra to additional output

      implicit none

      type(errorType), intent(inout) :: errS
      type(wavelInstrType), intent(in) :: wavelInstrS(:)    ! wavelength grid for (simulated) measurements
      type(RRS_RingType),   intent(in) :: RRS_RingS(:)      ! RRS

      integer    :: iwave, iband, nband
      real(8)    :: sumdiffRing, sumRTMdiffRing

      nband = size( wavelInstrS )

      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'Differential Ring spectra on instrument wavelength grid'
      write(addtionalOutputUnit,*)
      do iband = 1, nband
        sumdiffRing    = sum(abs(RRS_RingS(iband)%diffRing(:)))
        sumRTMdiffRing = sum(abs(RRS_RingS(iband)%RTMdiffRing(:)))
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'Degree of polynomial used is: ', RRS_RingS(iband)%degreePoly
        write(addtionalOutputUnit,*) 'spectra without RTM in front are Fraunhofer Ring spectra'
        write(addtionalOutputUnit,*) 'spectra with RTM in front are calculated using radiative transfer'
        write(addtionalOutputUnit,*) ' wavelength         diffRing      diffRing_scaled        RTMdiffRing'
        do iwave = 1,  wavelInstrS(iband)%nwavel
          write(addtionalOutputUnit,'(F12.6, 3ES20.8)') wavelInstrS(iband)%wavel(iwave), &
                                                       RRS_RingS(iband)%diffRing(iwave), &
                        sumRTMdiffRing * RRS_RingS(iband)%diffRing(iwave) / sumdiffRing, &
                                                       RRS_RingS(iband)%RTMdiffRing(iwave)

        end do ! iwave
        write(addtionalOutputUnit,*)
      end do ! iband

    end subroutine writeDiffRingSpec


    subroutine writeFillingInSpec(errS, wavelInstrS, wavelHRS, RRS_RingS)

      ! write Ring spectra to additional output

      implicit none

      type(errorType), intent(inout) :: errS
      type(wavelInstrType), intent(in) :: wavelInstrS(:)    ! wavelength grid for (simulated) measurements
      type(wavelHRType)   , intent(in) :: wavelHRS(:)       ! wavelength grid for (simulated) measurements
      type(RRS_RingType),   intent(in) :: RRS_RingS(:)      ! RRS

      integer    :: iwave, iband, nband

      nband = size( wavelInstrS )

      write(addtionalOutputUnit,*)
      do iband = 1, nband
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'filling in spectra on instrument wavelength grid'
        write(addtionalOutputUnit,*) 'following the definition of Joiner: (Iinelas - Ielastic)/ Ielas'
        write(addtionalOutputUnit,*) ' wavelength         filling in(%)'
        do iwave = 1,  wavelInstrS(iband)%nwavel
          write(addtionalOutputUnit,'(F12.6, F20.8)') wavelInstrS(iband)%wavel(iwave), &
                                                      RRS_RingS(iband)%RTM_FI(iwave)

        end do ! iwave
      end do ! iband

      do iband = 1, nband
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'filling in spectra on high-resolution wavelength grid'
        write(addtionalOutputUnit,*) 'following the definition of Joiner: (Iinelas - Ielastic)/ Ielas'
        write(addtionalOutputUnit,*) ' wavelength         filling in(%)'
        do iwave = 1,  wavelHRS(iband)%nwavel
          write(addtionalOutputUnit,'(F12.6, F20.8)') wavelHRS(iband)%wavel(iwave), &
                                                      RRS_RingS(iband)%RTM_FIHR(iwave)

        end do ! iwave
      end do ! iband

    end subroutine writeFillingInSpec

    subroutine writeContribRad_Refl(errS, geometryS, wavelHRS, wavelInstrS, optPropRTMGridS, &
                                    solarIrradianceS, earthRadianceS)

      ! write contribution function to output for the spectral high resolution wavelength grid
      ! and the instrument wavelength grid

      implicit none

      type(errorType), intent(inout) :: errS
      type(geometryType),        intent(in) :: geometryS           ! geometrical information
      type(wavelHRType),         intent(in) :: wavelHRS(:)         ! high resolution wavelength grid for simulation
      type(wavelInstrType),      intent(in) :: wavelInstrS(:)      ! wavelength grid for (simulated) measurements
      type(optPropRTMGridType),  intent(in) :: optPropRTMGridS     ! optical properties on RTM altitude grid
      type(solarIrrType),        intent(in) :: solarIrradianceS(:) ! solar irradiance on HR and instrument grid
      type(earthRadianceType),   intent(in) :: earthRadianceS(:)   ! radiance and derivatives on HR and instrument grid

      ! local
      integer             :: iwave, iband, nlevel, nband
      real(8), parameter  :: PI = 3.141592653589793d0

      nlevel = optPropRTMGridS%RTMnlayer
      nband = size( wavelHRS )

      ! Instrument spectral resolution - contribution to the radiance
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'Instrument wavelength grid: altitude resolved path radiance'
      write(addtionalOutputUnit,'(A,100F15.2)') 'altitude(km)', optPropRTMGridS%RTMaltitude
      do iband = 1, nband
        do iwave = 1,  wavelInstrS(iband)%nwavel
          write(addtionalOutputUnit,'(F12.6,100ES15.5)') wavelInstrS(iband)%wavel(iwave), &
                                                          earthRadianceS(iband)%contribRad(1,iwave,:)
        end do
      end do

      ! High spectral resolution - contribution to the radiance
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'High resolution wavelength grid: altitude resolved path radiance'
      write(addtionalOutputUnit,'(A,100F15.2)') 'altitude(km)', optPropRTMGridS%RTMaltitude
      do iband = 1, nband
        do iwave = 1,  wavelHRS(iband)%nwavel
          write(addtionalOutputUnit,'(F12.6,100ES15.5)') wavelHRS(iband)%wavel(iwave), &
                                                         earthRadianceS(iband)%contribRadHR(1,iwave,:)
        end do
      end do

      ! Instrument spectral resolution - contribution to the sun-normalized radiance
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'Instrument wavelength grid: contribution to the sun-normalized radiance'
      write(addtionalOutputUnit,'(A,100F15.4)') 'altitude(km)', optPropRTMGridS%RTMaltitude
      do iband = 1, nband
        do iwave = 1,  wavelInstrS(iband)%nwavel
          write(addtionalOutputUnit,'(F12.6,100ES15.5)') wavelInstrS(iband)%wavel(iwave), &
           PI * earthRadianceS(iband)%contribRad(1,iwave,:) / geometryS%u0 / solarIrradianceS(iband)%solIrr(iwave)
        end do
      end do

      ! High spectral resolution - contribution to the sun-normalized radiance
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'High resolution wavelength grid: contribution to the sun-normalized radiance'
      write(addtionalOutputUnit,'(A,100F15.2)') 'altitude(km)', optPropRTMGridS%RTMaltitude
      do iband = 1, nband
        do iwave = 1,  wavelHRS(iband)%nwavel
          write(addtionalOutputUnit,'(F12.6,100ES15.5)') wavelHRS(iband)%wavel(iwave), &
           PI * earthRadianceS(iband)%contribRadHR(1,iwave,:) / geometryS%u0 / solarIrradianceS(iband)%solIrrMR(iwave)
        end do
      end do

    end subroutine writeContribRad_Refl


    subroutine writeInternalField(errS, controlS, geometryS, wavelHRS, wavelInstrS, optPropRTMGridS, &
                                  solarIrradianceS, earthRadianceS)

      ! write contribution function to output for the spectral high resolution wavelength grid
      ! and the instrument wavelength grid

      implicit none

      type(errorType), intent(inout) :: errS
      type(controlType),         intent(in) :: controlS            ! control structure
      type(geometryType),        intent(in) :: geometryS           ! geometrical information
      type(wavelHRType),         intent(in) :: wavelHRS(:)         ! high resolution wavelength grid for simulation
      type(wavelInstrType),      intent(in) :: wavelInstrS(:)      ! wavelength grid for (simulated) measurements
      type(optPropRTMGridType),  intent(in) :: optPropRTMGridS     ! optical properties on RTM altitude grid
      type(solarIrrType),        intent(in) :: solarIrradianceS(:) ! solar irradiance on HR and instrument grid
      type(earthRadianceType),   intent(in) :: earthRadianceS(:)   ! radiance and derivatives on HR and instrument grid

      ! local
      integer             :: iwave, iband, nlevel, nband
      real(8), parameter  :: PI = 3.141592653589793d0

      nlevel = optPropRTMGridS%RTMnlayer
      nband = size( wavelHRS )

      if ( controlS%writeInternalFieldUp ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'Upwelling radiance [photons/s/cm^2/nm/sr] on instrument wavelength grid'
        write(addtionalOutputUnit,'(A)') 'instrumental offsets, spectral features, and stray light are ignored'
        write(addtionalOutputUnit,'(A)') 'angles are given in degrees'
        write(addtionalOutputUnit,'(A,F8.3,A,F8.3)') 'SZA = ', geometryS%sza, ' VZA = ', geometryS%vza
        write(addtionalOutputUnit,'(A,F8.3)') 'azimuth difference = ', geometryS%dphi
        write(addtionalOutputUnit,'(A,100F15.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F15.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iband = 1, nband
          do iwave = 1,  wavelInstrS(iband)%nwavel
            write(addtionalOutputUnit,'(F13.6,100ES15.5)') wavelInstrS(iband)%wavel(iwave), &
                                                           earthRadianceS(iband)%intFieldUp(1,iwave,:)
          end do
        end do
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'sun-normalized upwelling radiation [1/sr] on instrument wavelength grid'
        write(addtionalOutputUnit,'(A,100F15.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F15.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iband = 1, nband
          do iwave = 1,  wavelInstrS(iband)%nwavel
            write(addtionalOutputUnit,'(F13.6,100ES15.5)') wavelInstrS(iband)%wavel(iwave), &
               earthRadianceS(iband)%intFieldUp(1,iwave,:) / solarIrradianceS(iband)%solIrr(iwave)
          end do
        end do
      end if

      if ( controlS%writeInternalFieldUpHR ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'Upwelling radiance [photons/s/cm^2/nm/sr] on high resolution wavelength grid'
        write(addtionalOutputUnit,'(A)') 'instrumental offsets, spectral features, and stray light are ignored'
        write(addtionalOutputUnit,'(A)') 'angles are given in degrees'
        write(addtionalOutputUnit,'(A,F8.3,A,F8.3)') 'SZA = ', geometryS%sza, ' VZA = ', geometryS%vza
        write(addtionalOutputUnit,'(A,F8.3)') 'azimuth difference = ', geometryS%dphi
        write(addtionalOutputUnit,'(A,100F15.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F15.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iband = 1, nband
          do iwave = 1,  wavelHRS(iband)%nwavel
            write(addtionalOutputUnit,'(F13.6,100ES15.5)') wavelHRS(iband)%wavel(iwave), &
                                                           earthRadianceS(iband)%intFieldUpHR(1,iwave,:)
          end do
        end do
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'sun-normalized upwelling radiation [1/sr] on high resolution wavelength grid'
        write(addtionalOutputUnit,'(A,100F15.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F15.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iband = 1, nband
          do iwave = 1,  wavelHRS(iband)%nwavel
            write(addtionalOutputUnit,'(F13.6,100ES15.5)') wavelHRS(iband)%wavel(iwave), &
               earthRadianceS(iband)%intFieldUpHR(1,iwave,:) / solarIrradianceS(iband)%solIrrMR(iwave)
          end do
        end do
      end if

      if ( controlS%writeInternalFieldDown ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'Downwelling diffuse radiance [photons/s/cm^2/nm/sr] on instrument wavelength grid'
        write(addtionalOutputUnit,'(A)') 'instrumental offsets, spectral features, and stray light are ignored'
        write(addtionalOutputUnit,'(A)') 'angles are given in degrees'
        write(addtionalOutputUnit,'(A,F8.3,A,F8.3)') 'SZA = ', geometryS%sza, ' VZA = ', geometryS%vza
        write(addtionalOutputUnit,'(A,F8.3)') 'azimuth difference = ', geometryS%dphi
        write(addtionalOutputUnit,'(A,100F15.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F15.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iband = 1, nband
          do iwave = 1,  wavelInstrS(iband)%nwavel
            write(addtionalOutputUnit,'(F13.6,100ES15.5)') wavelInstrS(iband)%wavel(iwave), &
                                                           earthRadianceS(iband)%intFieldDown(1,iwave,:)
          end do
        end do
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'sun-normalized downwelling radiation [1/sr] on instrument wavelength grid'
        write(addtionalOutputUnit,'(A,100F15.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F15.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iband = 1, nband
          do iwave = 1,  wavelInstrS(iband)%nwavel
            write(addtionalOutputUnit,'(F13.6,100ES15.5)') wavelInstrS(iband)%wavel(iwave), &
               earthRadianceS(iband)%intFieldDown(1,iwave,:) / solarIrradianceS(iband)%solIrr(iwave)
          end do
        end do
      end if

      if ( controlS%writeInternalFieldDownHR ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'Downwelling diffuse radiance [photons/s/cm^2/nm/sr] on high resolution wavelength grid'
        write(addtionalOutputUnit,'(A)') 'instrumental offsets, spectral features, and stray light are ignored'
        write(addtionalOutputUnit,'(A)') 'angles are given in degrees'
        write(addtionalOutputUnit,'(A,F8.3,A,F8.3)') 'SZA = ', geometryS%sza, ' VZA = ', geometryS%vza
        write(addtionalOutputUnit,'(A,F8.3)') 'azimuth difference = ', geometryS%dphi
        write(addtionalOutputUnit,'(A,100F15.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F15.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iband = 1, nband
          do iwave = 1,  wavelHRS(iband)%nwavel
            write(addtionalOutputUnit,'(F13.6,100ES15.5)') wavelHRS(iband)%wavel(iwave), &
                                                           earthRadianceS(iband)%intFieldDownHR(1,iwave,:)
          end do
        end do
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A)') 'sun-normalized downwelling diffuse radiation [1/sr] on high resolution wavelength grid'
        write(addtionalOutputUnit,'(A,100F15.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F15.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iband = 1, nband
          do iwave = 1,  wavelHRS(iband)%nwavel
            write(addtionalOutputUnit,'(F13.6,100ES15.5)') wavelHRS(iband)%wavel(iwave), &
               earthRadianceS(iband)%intFieldDownHR(1,iwave,:) / solarIrradianceS(iband)%solIrrMR(iwave)
          end do
        end do
      end if


    end subroutine writeInternalField


    subroutine writeAltResolvedAMF(errS, iband, wavelHRS, cloudAerosolRTMgridS, optPropRTMGridS, reflDerivHRS)

      ! write the altitude resolved air mass factor to output for the spectral high resolution wavelength grid
      ! and the instrument wavelength grid

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in) :: iband
      type(wavelHRType),             intent(in) :: wavelHRS              ! high resolution wavelength grid for simulation
      type(cloudAerosolRTMgridType), intent(in) :: cloudAerosolRTMgridS  ! cloud-aerosol properties
      type(optPropRTMGridType),      intent(in) :: optPropRTMGridS       ! optical properties on RTM altitude grid
      type(reflDerivType),           intent(in) :: reflDerivHRS          ! contains altitude resolved AMF

      ! local
      integer               :: iwave, ilevel, nlevel
      integer, parameter    :: degreePoly = 4
      real(8)               :: amfSmooth(0:wavelHRS%nwavel, 0:optPropRTMGridS%RTMnlayer)
      real(8)               :: amfDiff  (0:wavelHRS%nwavel, 0:optPropRTMGridS%RTMnlayer)

      logical, parameter    :: writeSmoothDiffAMF = .false.

      nlevel = optPropRTMGridS%RTMnlayer

      ! High spectral resolution - altitude resolved air mass factor

      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,*) 'spectral band nr = ', iband
      write(addtionalOutputUnit,*) 'High resolution wavelength grid: altitude resolved AMF abs clear part'
      write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
      write(addtionalOutputUnit,'(A,100F10.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
      do iwave = 1,  wavelHRS%nwavel
        write(addtionalOutputUnit,'(F13.6,100F10.6)') wavelHRS%wavel(iwave), &
                                                      reflDerivHRS%altResAMFabs_clr(iwave,:)
      end do

      if ( cloudAerosolRTMgridS%cloudPresent ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'spectral band nr = ', iband
        write(addtionalOutputUnit,*) 'High resolution wavelength grid: altitude resolved AMF abs cloudy part'
        write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F10.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iwave = 1,  wavelHRS%nwavel
          write(addtionalOutputUnit,'(F13.6,100F10.6)') wavelHRS%wavel(iwave), &
                                                      reflDerivHRS%altResAMFabs_cld(iwave,:)
        end do
      end if ! cloudAerosolRTMgridS%cloudPresent

      if ( cloudAerosolRTMgridS%aerosolPresent ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'spectral band nr = ', iband
        write(addtionalOutputUnit,*) 'High resolution wavelength grid: altitude resolved AMF sca aer clear part'
        write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
        write(addtionalOutputUnit,'(A,100F10.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
        do iwave = 1,  wavelHRS%nwavel
          write(addtionalOutputUnit,'(F13.6,100F10.6)') wavelHRS%wavel(iwave), &
                                                      reflDerivHRS%altResAMFscaAer_clr(iwave,:)
        end do

        if ( cloudAerosolRTMgridS%cloudPresent ) then
          write(addtionalOutputUnit,*)
          write(addtionalOutputUnit,*) 'spectral band nr = ', iband
          write(addtionalOutputUnit,*) 'High resolution wavelength grid: altitude resolved AMF sca aer cloudy part'
          write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km) ', optPropRTMGridS%RTMaltitude
          write(addtionalOutputUnit,'(A,100F10.3)') 'pressure(hPa)', optPropRTMGridS%RTMpressure
          do iwave = 1,  wavelHRS%nwavel
            write(addtionalOutputUnit,'(F13.6,100F10.6)') wavelHRS%wavel(iwave), &
                                                        reflDerivHRS%altResAMFscaAer_cld(iwave,:)
          end do
        end if ! cloudAerosolRTMgridS%cloudPresent

      end if ! cloudAerosolRTMgridS%aerosolPresent

      if ( writeSmoothDiffAMF ) then

        ! calculate smooth differential part of the air mass factor by fitting a polynomial of degree degreePoly
        ! and subtracting it from the altitude resolved amf, using the routine for the absorption cross sections
        do ilevel = 0, optPropRTMGridS%RTMnlayer
          call getSmoothAndDiffXsec(errS, degreePoly, wavelHRS%nwavel, wavelHRS%wavel(:),          &
                reflDerivHRS%altResAMFabs_clr(:,ilevel), amfSmooth(:,ilevel), amfDiff(:,ilevel))
          if (errorCheck(errS)) return
        end do ! ilevel

        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'spectral band nr = ', iband
        write(addtionalOutputUnit,*) 'smooth part of the altitude resolved AMF - clear part'
        write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km)', optPropRTMGridS%RTMaltitude
        do iwave = 1,  wavelHRS%nwavel
          write(addtionalOutputUnit,'(F12.6,100F10.6)') wavelHRS%wavel(iwave), amfSmooth(iwave,:)
        end do ! iwave

        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'spectral band nr = ', iband
        write(addtionalOutputUnit,*) 'differential part of the altitude resolved AMF - clear part'
        write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km)', optPropRTMGridS%RTMaltitude
        do iwave = 1,  wavelHRS%nwavel
          write(addtionalOutputUnit,'(F12.6,100F10.6)') wavelHRS%wavel(iwave), amfDiff(iwave,:)
        end do ! iwave

        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'spectral band nr = ', iband
        write(addtionalOutputUnit,*) 'differential amf in percent: 100 * amfDiff / amfSmooth - clear part'
        write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km)', optPropRTMGridS%RTMaltitude
        do iwave = 1,  wavelHRS%nwavel
          write(addtionalOutputUnit,'(F12.6,100F10.6)') wavelHRS%wavel(iwave), &
                                                        100 * amfDiff(iwave,:) / amfSmooth(iwave,:)
        end do ! iwave

        ! calculate smooth differential part of the air mass factor by fitting a polynomial of degree degreePoly
        ! and subtracting it from the altitude resolved amf, using the routine for the absorption cross sections
        do ilevel = 0, optPropRTMGridS%RTMnlayer
          call getSmoothAndDiffXsec(errS, degreePoly, wavelHRS%nwavel, wavelHRS%wavel(:),          &
                reflDerivHRS%altResAMFabs_cld(:,ilevel), amfSmooth(:,ilevel), amfDiff(:,ilevel))
          if (errorCheck(errS)) return
        end do ! ilevel

        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'spectral band nr = ', iband
        write(addtionalOutputUnit,*) 'smooth part of the altitude resolved AMF - cloudy part'
        write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km)', optPropRTMGridS%RTMaltitude
        do iwave = 1,  wavelHRS%nwavel
          write(addtionalOutputUnit,'(F12.6,100F10.6)') wavelHRS%wavel(iwave), amfSmooth(iwave,:)
        end do ! iwave

        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'spectral band nr = ', iband
        write(addtionalOutputUnit,*) 'differential part of the altitude resolved AMF - cloudy part'
        write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km)', optPropRTMGridS%RTMaltitude
        do iwave = 1,  wavelHRS%nwavel
          write(addtionalOutputUnit,'(F12.6,100F10.6)') wavelHRS%wavel(iwave), amfDiff(iwave,:)
        end do ! iwave

        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'spectral band nr = ', iband
        write(addtionalOutputUnit,*) 'differential amf in percent: 100 * amfDiff / amfSmooth - cloudy part'
        write(addtionalOutputUnit,'(A,100F10.3)') 'altitude(km)', optPropRTMGridS%RTMaltitude
        do iwave = 1,  wavelHRS%nwavel
          write(addtionalOutputUnit,'(F12.6,100F10.6)') wavelHRS%wavel(iwave), &
                                                        100 * amfDiff(iwave,:) / amfSmooth(iwave,:)
        end do ! iwave

      end if ! writeSmoothDiffAMF

    end subroutine writeAltResolvedAMF


    subroutine writeColumnProperties(errS, ncolumn, nTrace, traceGasSimS, traceGasRetrS, &
             optPropRTMGridRetrS, columnSimS, columnRetrS, controlRetrS, retrS)

      ! write properties of the columns and subcolumns to output

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                  intent(in) :: ncolumn
      integer,                  intent(in) :: nTrace
      type(traceGasType),       intent(in) :: traceGasSimS(nTrace), traceGasRetrS(nTrace)
      type(optPropRTMGridType), intent(in) :: optPropRTMGridRetrS
      type(columnType),         intent(in) :: columnSimS(ncolumn), columnRetrS(ncolumn)
      type(controlType),        intent(in) :: controlRetrS
      type(retrType),           intent(in) :: retrS


      ! local
      integer    :: icolumn, isubcolumn, iTrace, iGauss, iwave

      real(8), parameter :: DUToCm2 = 2.68668d16
      real(8)            :: gain_norm(100)

      write(outputFileUnit,*)
      write(outputFileUnit,*) '(SUB)COLUMN PROPERTIES'
      do iTrace = 1, nTrace
        if ( traceGasRetrS(iTrace)%fitProfile ) then
          write(outputFileUnit,*)
          write(outputFileUnit,'(2A)') 'results for trace gas: ', traceGasSimS(iTrace)%nameTraceGas
          write(outputFileUnit,*)
          write(outputFileUnit,'(A)') 'columns are given in DU while bias and precision are given in percent'
          write(outputFileUnit,'(A)') &
            ' z_bot(km)  z_top(km) columnRetr  columnAP   columnSim    bias     precision precisionAP noise_precision'
          do icolumn = 1, ncolumn
            do isubcolumn = columnRetrS(icolumn)%nsubColumn, 1, -1
              write(outputFileUnit,'(2F10.2,3ES11.3,4F11.4)')                                           &
                columnRetrS(icolumn)%altBound(isubcolumn-1), columnRetrS(icolumn)%altBound(isubcolumn), &
                columnRetrS(icolumn)%subColumn(isubcolumn)%column(iTrace) / DUToCm2,                    &
                columnRetrS(icolumn)%subColumn(isubcolumn)%columnAP(iTrace) / DUToCm2,                  &
                columnSimS (icolumn)%subColumn(isubcolumn)%column(iTrace) / DUToCm2,                    &
                100.0d0 * ( columnRetrS(icolumn)%subColumn(isubcolumn)%column(iTrace) -                 &
                            columnSimS (icolumn)%subColumn(isubcolumn)%column(iTrace) )                 &
                          / columnSimS (icolumn)%subColumn(isubcolumn)%column(iTrace),                  &
                columnRetrS(icolumn)%subColumn(isubcolumn)%errorCol(iTrace),                            &
                columnRetrS(icolumn)%subColumn(isubcolumn)%errorColAP(iTrace),                          &
                columnRetrS(icolumn)%subColumn(isubcolumn)%noiseErrorCol(iTrace)
            end do
          end do ! icolumn
        end if ! traceGasRetrS(iTrace)%fitProfile
      end do ! iTrace

      write(outputFileUnit,*)
      do iTrace =  1, nTrace
        if ( traceGasRetrS(iTrace)%fitProfile ) then
          do icolumn = 1, ncolumn
            write(outputFileUnit,'(A, I3)') 'Transposed GAIN matrices for column ', icolumn
            write(outputFileUnit,'(A, 30(3X,F4.1,A,F4.1))') '     wavel  ',  &
              (columnRetrS(icolumn)%altBound(isubcolumn-1),'-', columnRetrS(icolumn)%altBound(isubcolumn), &
               isubcolumn = 1, columnRetrS(icolumn)%nsubColumn  )

            do iwave = 1, retrS%nwavelRetr
              write(outputFileUnit,'(F12.6, 150ES12.3)') retrS%wavelRetr(iwave), &
                  (columnRetrS(icolumn)%Gain(isubcolumn, iwave, iTrace), isubcolumn = 1, columnRetrS(icolumn)%nsubColumn )
            end do ! iwave
          end do ! icolumn

          ! normalized gain: G_i'= G_i * R / N_i  with
          !   G_i the gain for subcolumn i  ( molecules cm-2 )
          !   R   then sun-normalized radiance
          !   N_i then trace gas column for subcolumn i ( molecules cm-2 )
          !      sum_k [G_i'(k) eps'(k)] = delta_N_i'
          !   with
          !   eps'(k) = 100 * eps(k) / R(k) is the relative bias in the sun-normalized radiance (%)
          !   delta_N_i'= 100 * delta_N_i / N_i is the relative bias for the number density
          !                                     of subcolumn i (%)
          do icolumn = 1, ncolumn
            write(outputFileUnit,'(A, I3)') 'Transposed NORMALIZED GAIN matrices for column ', icolumn
            write(outputFileUnit,'(A, 30(3X,F4.1,A,F4.1))') '     wavel  ',  &
              (columnRetrS(icolumn)%altBound(isubcolumn-1),'-', columnRetrS(icolumn)%altBound(isubcolumn), &
               isubcolumn = 1, columnRetrS(icolumn)%nsubColumn  )

            do iwave = 1, retrS%nwavelRetr
              do isubcolumn = 1, columnRetrS(icolumn)%nsubColumn
                gain_norm(isubcolumn) = columnRetrS(icolumn)%Gain(isubcolumn, iwave, iTrace) &
                                      * retrS%refl(iwave)                                    &
                                      / columnRetrS(icolumn)%subColumn(isubcolumn)%column(iTrace)
              end do ! isubcolumn
              write(outputFileUnit,'(F12.6, 150ES12.3)') retrS%wavelRetr(iwave), &
                  (gain_norm(isubcolumn), isubcolumn = 1, columnRetrS(icolumn)%nsubColumn )
            end do ! iwave
          end do ! icolumn

        end if
      end do ! iTrace

      write(outputFileUnit,*)
      write(outputFileUnit,*) 'SUBCOLUMN AVERAGING KERNEL for WT x'
      do iTrace = 1, nTrace
        if ( traceGasRetrS(iTrace)%fitProfile ) then
          write(outputFileUnit,*)
          write(outputFileUnit,'(2A)') 'results for trace gas: ', traceGasSimS(iTrace)%nameTraceGas
          write(outputFileUnit,*)
          write(outputFileUnit,'(A,30(2X,F4.1,A,F4.1,2X,A))') 'numdens(mol cm-3) alt(km)', &
         ((columnRetrS(icolumn)%altBound(isubcolumn-1),'-', columnRetrS(icolumn)%altBound(isubcolumn),'   alt(km) ', &
            isubcolumn = 1, columnRetrS(icolumn)%nsubColumn  ) , icolumn = 1, ncolumn )

          do iGauss = 1, optPropRTMGridRetrS%nGaussCol
            write(outputFileUnit,'(150ES12.3)')  optPropRTMGridRetrS%Colndens(iGauss,iTrace), &
                                                 optPropRTMGridRetrS%Colaltitude(iGauss),     &
                              ((columnRetrS(icolumn)%subColumn(isubcolumn)%AK(iGauss,iTrace), &
                                optPropRTMGridRetrS%Colaltitude(iGauss) ,                     &
                                isubcolumn = 1, columnRetrS(icolumn)%nsubColumn ) ,           &
                                icolumn = 1, ncolumn )
          end do ! iGauss
        end if
      end do ! iTrace

      write(outputFileUnit,*)
      write(outputFileUnit,'(A)') 'SUBCOLUMN AVERAGING KERNEL MULIPLIED BY NUMBER DENSITY AND DIVIDED BY SUBCOLUMN'
      do iTrace = 1, nTrace
        if ( traceGasRetrS(iTrace)%fitProfile ) then
          write(outputFileUnit,*)
          write(outputFileUnit,'(2A)') 'results for trace gas: ', traceGasSimS(iTrace)%nameTraceGas
          write(outputFileUnit,*)
          write(outputFileUnit,'(A,30(2X,F4.1,A,F4.1,2X,A))') 'numdens(mol cm-3) alt(km)', &
         ((columnRetrS(icolumn)%altBound(isubcolumn-1),'-', columnRetrS(icolumn)%altBound(isubcolumn),'   alt(km) ', &
            isubcolumn = 1, columnRetrS(icolumn)%nsubColumn  ) , icolumn = 1, ncolumn )

          do iGauss = 1, optPropRTMGridRetrS%nGaussCol
            write(outputFileUnit,'(150ES12.3)')  optPropRTMGridRetrS%Colndens(iGauss,iTrace), &
                                                 optPropRTMGridRetrS%Colaltitude(iGauss),     &
                            ((columnRetrS(icolumn)%subColumn(isubcolumn)%AKx(iGauss,iTrace),  &
                                optPropRTMGridRetrS%Colaltitude(iGauss),                      &
                                isubcolumn = 1, columnRetrS(icolumn)%nsubColumn ) ,           &
                                icolumn = 1, ncolumn )
          end do ! iGauss
        end if
      end do ! iTrace

    end subroutine writeColumnProperties


    subroutine writeColumnProperties_asciiHDF(errS, ncolumn, nTrace, iTrace, controlSimS, traceGasRetrS, &
                       optPropRTMGridSimS, optPropRTMGridRetrS, columnSimS, columnRetrS, controlRetrS)

      ! write properties of the columns and subcolumns to output

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                  intent(in) :: ncolumn
      integer,                  intent(in) :: nTrace, iTrace
      type(controlType),        intent(in) :: controlSimS
      type(traceGasType),       intent(in) :: traceGasRetrS(nTrace)
      type(optPropRTMGridType), intent(in) :: optPropRTMGridSimS, optPropRTMGridRetrS
      type(columnType),         intent(in) :: columnSimS(ncolumn), columnRetrS(ncolumn)
      type(controlType),        intent(in) :: controlRetrS

      ! local
      character(LEN=50) :: name
      character(LEN=50) :: keywordList1, keywordList2
      character(LEN=50) :: keyword1, keyword2, keyword3, keyword4
      character(LEN=50) :: string_1, string_2, string_3, string_4
      character(LEN=50) :: legend(3), legend_precision(4)

      integer    :: icolumn, isubcolumn
      character(LEN=50)  :: columName(ncolumn), precisionBiasName(ncolumn), meanVMRName(ncolumn)
      character(LEN=50)  :: averagingKernelName(ncolumn)
      character(LEN=50)  :: unit, unit2
      character(LEN=1)   :: char_index
      real(8), parameter :: DUToCm2 = 2.68668d16
      real(8)            :: factor


      real(8), allocatable :: columnBoundaries(:)
      real(8), allocatable :: columnValues(:,:)
      real(8), allocatable :: meanVMR(:,:)
      real(8), allocatable :: precisionBiasValues(:,:)
      real(8), allocatable :: averagingKernel(:,:)       ! (nsubColumn,nGaussColumn)

      integer :: allocStatus, deallocStatus
      integer :: sumallocStatus, sumdeallocStatus

      ! fill legend and legend precision
      legend(1) = 'true'
      legend(2) = 'a_priori'
      legend(3) = 'retrieved'
      legend_precision(1) = 'a_priori_precision'
      legend_precision(2) = 'precision'
      legend_precision(3) = 'precision_noise'
      legend_precision(4) = 'bias'

      if ( traceGasRetrS(iTrace)%unitFlag ) then
        factor = 1.0d0 / DUToCm2
        unit  = 'Dobson Units'
        unit2 = 'DU2'
      else
        factor = 1.0d0
        unit = 'molecules_per_cm2'
        unit2 = 'molecules2_per_cm4'
      end if

      do icolumn = 1, ncolumn

        char_index = achar(48 + icolumn)
        columName(icolumn) = 'column_'//char_index
        meanVMRName(icolumn) = 'average_VMR_'//char_index
        precisionBiasName(icolumn) = 'precision_bias_column_'//char_index
        averagingKernelName(icolumn) = 'averaging_kernel_'//char_index

        sumallocStatus = 0

        allocate ( columnBoundaries(0:columnRetrS(icolumn)%nsubColumn), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate ( columnValues(3, columnRetrS(icolumn)%nsubColumn), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate ( meanVMR(3, columnRetrS(icolumn)%nsubColumn), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate ( precisionBiasValues(4, columnRetrS(icolumn)%nsubColumn), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
        allocate ( averagingKernel(columnRetrS(icolumn)%nsubColumn, columnRetrS(icolumn)%nGaussCol), &
                   STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus

        if ( sumallocStatus /= 0 ) then
          call logDebug('FATAL ERROR: allocation failed')
          call logDebugI('for icolumn = ', icolumn)
          call logDebugI('for iTrace  = ', iTrace)
          call logDebug('in subroutine writeColumnProperties_asciiHDF')
          call logDebug('in module writeModule')
          call mystop(errS, 'stopped because allocation failed')
          if (errorCheck(errS)) return
        end if

        do isubcolumn = 0, columnRetrS(icolumn)%nsubColumn
          columnBoundaries(isubcolumn) = columnRetrS(icolumn)%altBound(isubcolumn)
        end do

        do isubcolumn = 1, columnRetrS(icolumn)%nsubColumn
            columnValues(1, isubcolumn) = factor * columnSimS (icolumn)%subColumn(isubcolumn)%column  (iTrace)   ! true
            columnValues(2, isubcolumn) = factor * columnRetrS(icolumn)%subColumn(isubcolumn)%columnAP(iTrace)   ! a-priori
            columnValues(3, isubcolumn) = factor * columnRetrS(icolumn)%subColumn(isubcolumn)%column  (iTrace)   ! retrieved
            precisionBiasValues(1, isubcolumn) = columnRetrS(icolumn)%subColumn(isubcolumn)%errorColAP(iTrace)   ! AP error
            meanVMR(1, isubcolumn) = columnSimS (icolumn)%subColumn(isubcolumn)%meanVmr  (iTrace)
            meanVMR(2, isubcolumn) = columnRetrS(icolumn)%subColumn(isubcolumn)%meanVmrAP(iTrace)
            meanVMR(3, isubcolumn) = columnRetrS(icolumn)%subColumn(isubcolumn)%meanVmr  (iTrace)
            precisionBiasValues(2, isubcolumn) = columnRetrS(icolumn)%subColumn(isubcolumn)%errorCol(iTrace)      ! error
            precisionBiasValues(3, isubcolumn) = columnRetrS(icolumn)%subColumn(isubcolumn)%noiseErrorCol(iTrace) ! noise
            precisionBiasValues(4, isubcolumn) = ( columnValues(3, isubcolumn) - columnValues(1, isubcolumn) ) &  ! bias
                                               * 1.0d2 / columnValues(1, isubcolumn)
            averagingKernel(isubcolumn,:) = columnRetrS(icolumn)%subColumn(isubcolumn)%AK(:,iTrace)
        end do

        keyword1     = 'unit'
        keywordList1 = 'boundaries_subcolumns_km'
        keywordList2 = 'legend'
        call writeReal(errS, columName(icolumn), x2D = columnValues, &
                       keyword1     = keyword1, string_1 = unit, &
                       keywordList1 = keywordList1, real8List_1 = columnBoundaries, &
                       keywordList2 = keywordList2, stringList_2 = legend )
        if (errorCheck(errS)) return

        keyword1     = 'unit'
        string_1     = 'percent'
        keywordList1 = 'boundaries_subcolumns_km'
        keywordList2 = 'legend'
        call writeReal(errS, precisionBiasName(icolumn), x2D = precisionBiasValues, &
                       keyword1     = keyword1, string_1 = string_1, &
                       keywordList1 = keywordList1, real8List_1 = columnBoundaries, &
                       keywordList2 = keywordList2, stringList_2 = legend_precision )
        if (errorCheck(errS)) return

        keyword1     = 'unit'
        string_1     = 'ppmv'
        keywordList2 = 'legend'
        call writeReal(errS, meanVMRName(icolumn), x2D = meanVMR, &
                       keyword1     = keyword1, string_1 = string_1, &
                       keywordList1 = keywordList1, real8List_1 = columnBoundaries)
        if (errorCheck(errS)) return

        name = 'averaging_kernel'
        keyword1 = 'apply_averaging_kernel'
        string_1 = 'x(i)=xa(i)+sum_j[w(j)*A(i,j)*(n_true(j)-n_AP(j))]'
        keyword2 = 'unit'
        string_2 = ' '
        keyword3 = 'n'
        string_3 = 'number density in molecules cm-3'
        keyword4 = 'w'
        string_4 = 'gaussian_weights'
        call writeReal(errS, averagingKernelName(icolumn), x2D = averagingKernel, &
                       keyword1 = keyword1, string_1 = string_1,            &
                       keyword2 = keyword2, string_2 = string_2,            &
                       keyword3 = keyword3, string_3 = string_3,            &
                       keyword4 = keyword4, string_4 = string_4)
        if (errorCheck(errS)) return

        keyword1 = 'unit'
        name = 'error_covariance_'//char_index
        call writeReal(errS, name, x2D = factor * factor * columnRetrS(icolumn)%S(:,:,iTrace), &
                       keyword1 = keyword1, string_1=unit2 )
        if (errorCheck(errS)) return

        name = 'error_covariance_AP_'//char_index
        call writeReal(errS, name, x2D = factor * factor * columnRetrS(icolumn)%Sa(:,:,iTrace), &
                       keyword1 = keyword1, string_1=unit2 )
        if (errorCheck(errS)) return

        name = 'error_covariance_noise_'//char_index
        call writeReal(errS,  name, x2D = factor * factor * columnRetrS(icolumn)%Snoise(:,:,iTrace), &
                       keyword1 = keyword1, string_1=unit2 )
        if (errorCheck(errS)) return


        deallocStatus    = 0
        sumdeallocStatus = 0

        deallocate ( columnBoundaries, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate ( columnValues, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate ( meanVMR, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate ( precisionBiasValues, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        deallocate ( averagingKernel, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus

        if ( sumdeallocStatus /= 0 ) then
          call logDebug('FATAL ERROR: deallocation failed')
          call logDebugI('for icolumn = ', icolumn)
          call logDebugI('for iTrace  = ', iTrace)
          call logDebug('in subroutine writeColumnProperties_asciiHDF')
          call logDebug('in module writeModule')
          call mystop(errS, 'stopped because deallocation failed')
          if (errorCheck(errS)) return
        end if

      end do ! icolumn

      name = 'gaussian_altitudes'
      keyword1 = 'apply_averaging_kernel'
      string_1 = 'x(i)=xa(i)+sum_j[w(j)*A(i,j)*(n_true(j)-n_AP(j))]'
      keyword2 = 'unit'
      string_2 = ' '
      keyword3 = 'n'
      string_3 = 'number density in molecules cm-3'
      keyword4 = 'w'
      string_4 = 'gaussian_weights'
      call writeReal(errS, name, x1D = optPropRTMGridRetrS%Colaltitude,  &
                     keyword1 = keyword1, string_1 = string_1,     &
                     keyword2 = keyword2, string_2 = string_2,     &
                     keyword3 = keyword3, string_3 = string_3,     &
                     keyword4 = keyword4, string_4 = string_4)
      if (errorCheck(errS)) return

      name = 'gaussian_weights'
      keyword1 = 'apply_averaging_kernel'
      string_1 = 'x(i)=xa(i)+sum_j[w(j)*A(i,j)*(n_true(j)-n_AP(j))]'
      keyword2 = 'unit'
      string_2 = ' '
      keyword3 = 'n'
      string_3 = 'number density in molecules cm-3'
      keyword4 = 'w'
      string_4 = 'gaussian_weights'
      call writeReal(errS, name, x1D = 1.0d5*optPropRTMGridRetrS%Colweight/DUToCm2,  &
                     keyword1 = keyword1, string_1 = string_1,                 &
                     keyword2 = keyword2, string_2 = string_2,                 &
                     keyword3 = keyword3, string_3 = string_3,                 &
                     keyword4 = keyword4, string_4 = string_4)
      if (errorCheck(errS)) return

      name = 'a_priori_number_density'
      keyword1 = 'apply_averaging_kernel'
      string_1 = 'x(i)=xa(i)+sum_j[w(j)*A(i,j)*(n_true(j)-n_AP(j))]'
      keyword2 = 'unit'
      string_2 = ' '
      keyword3 = 'n'
      string_3 = 'number density in molecules cm-3'
      keyword4 = 'w'
      string_4 = 'gaussian_weights'
      call writeReal(errS, name, x1D = optPropRTMGridRetrS%ColndensAP(:,iTrace), &
                     keyword1 = keyword1, string_1 = string_1,             &
                     keyword2 = keyword2, string_2 = string_2,             &
                     keyword3 = keyword3, string_3 = string_3,             &
                     keyword4 = keyword4, string_4 = string_4)
      if (errorCheck(errS)) return


      name = 'number_density_retrieved'
      keyword1 = 'apply_averaging_kernel'
      string_1 = 'x(i)=xa(i)+sum_j[w(j)*A(i,j)*(n_true(j)-n_AP(j))]'
      keyword2 = 'unit'
      string_2 = ' '
      keyword3 = 'n'
      string_3 = 'number density in molecules cm-3'
      keyword4 = 'w'
      string_4 = 'gaussian_weights'
      call writeReal(errS, name, x1D = optPropRTMGridRetrS%Colndens(:,iTrace), &
                     keyword1 = keyword1, string_1 = string_1,           &
                     keyword2 = keyword2, string_2 = string_2,           &
                     keyword3 = keyword3, string_3 = string_3,           &
                     keyword4 = keyword4, string_4 = string_4)
      if (errorCheck(errS)) return

      if ( .not. controlSimS%useReflectanceFromFile ) then
        name = 'number_density_true'
        keyword1 = 'apply_averaging_kernel'
        string_1 = 'x(i)=xa(i)+sum_j[w(j)*A(i,j)*(n_true(j)-n_AP(j))]'
        keyword2 = 'unit'
        string_2 = ' '
        keyword3 = 'n'
        string_3 = 'number density in molecules cm-3'
        keyword4 = 'w'
        string_4 = 'gaussian_weights'
        call writeReal(errS, name, x1D = optPropRTMGridSimS%Colndens(:,iTrace),  &
                       keyword1 = keyword1, string_1 = string_1,           &
                       keyword2 = keyword2, string_2 = string_2,           &
                       keyword3 = keyword3, string_3 = string_3,           &
                       keyword4 = keyword4, string_4 = string_4)
        if (errorCheck(errS)) return
      end if ! .not. controlSimS%useReflectanceFromFile

    end subroutine writeColumnProperties_asciiHDF


    subroutine writeTestDataHR(errS, numSpectrBands, geometryS, surfaceS, cloudAerosolRTMgridS, &
                               wavelHRS, solarIrradianceS, earthRadianceS)

      ! write high resolution test data to additional output file
      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in) :: numSpectrBands
      type(geometryType),            intent(in) :: geometryS
      type(LambertianType),          intent(in) :: surfaceS(numSpectrBands)
      type(cloudAerosolRTMgridType), intent(in) :: cloudAerosolRTMgridS
      type(wavelHRType),             intent(in) :: wavelHRS(numSpectrBands)
      type(solarIrrType),            intent(in) :: solarIrradianceS(numSpectrBands)
      type(earthRadianceType),       intent(in) :: earthRadianceS(numSpectrBands)

      ! local
      integer      :: iband, iwave

      write(addtionalOutputUnit,'(2A)')       'DISAMARversionNumber ', DISAMAR_version_number
      write(addtionalOutputUnit,'(A,F12.7)')  'solarZenithAngle     ', geometryS%sza
      write(addtionalOutputUnit,'(A,F12.7)')  'viewingZenithAngle   ', geometryS%vza
      write(addtionalOutputUnit,'(A,F12.7)')  'azimuthDifference    ', geometryS%dphi
      write(addtionalOutputUnit,'(A,F12.7)')  'surfacePressure(hPa) ', surfaceS(1)%pressure
      write(addtionalOutputUnit,'(A,F12.7)')  'cloudAlbedo          ', cloudAerosolRTMgridS%albedoLambCldAllBands
      do iband = 1, numSpectrBands
        do iwave = 1, surfaceS(iband)%nwavelAlbedo
           write(addtionalOutputUnit,'(A,2F12.7)') 'surfaceAlbedo        ', surfaceS(iband)%wavelAlbedo(iwave), &
                                                                            surfaceS(iband)%albedo(iwave)
        end do ! iwave
      end do ! iband
      write(addtionalOutputUnit,'(A,F12.7)')  'cldAerFraction       ', cloudAerosolRTMgridS%cldAerFractionAllBands
      write(addtionalOutputUnit,'(A,F12.7)')  'cloudAltitude (km)   ', &
                         cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit)
      write(addtionalOutputUnit,*)
      write(addtionalOutputUnit,'(A)') '  wavelength (nm) solIrradiance  earthRadiance     weight'
      do iband = 1, numSpectrBands
        do iwave = 1, wavelHRS(iband)%nwavel
          write(addtionalOutputUnit,'(F15.8,2ES15.6, F15.10)') wavelHRS(iband)%wavel(iwave), &
                                                    solarIrradianceS(iband)%solIrrMR(iwave), &
                                                      earthRadianceS(iband)%rad_HR(1,iwave), &
                                                              wavelHRS(iband)%weight(iwave)
        end do ! iwave
        if ( earthRadianceS(iband)%dimSV > 1 ) then
          write(addtionalOutputUnit,'(2A)') 'wavelength (nm)   degree of linear polarization (%)'
          do iwave = 1, wavelHRS(iband)%nwavel
            write(addtionalOutputUnit,'(F15.8, F20.12)') wavelHRS(iband)%wavel(iwave),  &
            100.0d0 * sqrt(   earthRadianceS(iband)%rad_HR(2,iwave)**2   &
                            + earthRadianceS(iband)%rad_HR(3,iwave)**2 ) &
                         / earthRadianceS(iband)%rad_HR(1,iwave)
          end do
        end if
      end do ! iband
      write(addtionalOutputUnit,*)

    end subroutine writeTestDataHR


    subroutine writeTestDataInstr(errS, numSpectrBands, geometryS, surfaceS, cloudAerosolRTMgridS, &
                                  solarIrradianceS, wavelInstrRadS, earthRadianceS)

      ! write test data to additional output file for instrument wavelength grid
      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in) :: numSpectrBands
      type(geometryType),            intent(in) :: geometryS
      type(LambertianType),          intent(in) :: surfaceS(numSpectrBands)
      type(cloudAerosolRTMgridType), intent(in) :: cloudAerosolRTMgridS
      type(wavelInstrType),          intent(in) :: wavelInstrRadS(numSpectrBands)
      type(solarIrrType),            intent(in) :: solarIrradianceS(numSpectrBands)
      type(earthRadianceType),       intent(in) :: earthRadianceS(numSpectrBands)

      ! local
      integer             :: iband, iwave
      real(8), parameter  :: PI = 3.141592653589793d0


      write(addtionalOutputUnit,'(2A)')       'DISAMARversionNumber ', DISAMAR_version_number
      write(addtionalOutputUnit,'(A,F12.7)')  'solarZenithAngle     ', geometryS%sza
      write(addtionalOutputUnit,'(A,F12.7)')  'viewingZenithAngle   ', geometryS%vza
      write(addtionalOutputUnit,'(A,F12.7)')  'azimuthDifference    ', geometryS%dphi
      write(addtionalOutputUnit,'(A,F12.7)')  'surfacePressure(hPa) ', surfaceS(1)%pressure
      write(addtionalOutputUnit,'(A,F12.7)')  'cloudAlbedo          ', cloudAerosolRTMgridS%albedoLambCldAllBands
      do iband = 1, numSpectrBands
        do iwave = 1, surfaceS(iband)%nwavelAlbedo
          write(addtionalOutputUnit,'(A,2F12.7)') 'surfaceAlbedo        ', surfaceS(iband)%wavelAlbedo(iwave), &
                                                                           surfaceS(iband)%albedo(iwave)
        end do ! iwave
      end do ! iband
      write(addtionalOutputUnit,'(A,F12.7)')  'cldAerFraction       ', cloudAerosolRTMgridS%cldAerFractionAllBands
      write(addtionalOutputUnit,'(A,F12.7)')  'cloudAltitude (km)   ', &
                         cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit)
      write(addtionalOutputUnit,*)
      do iband = 1, numSpectrBands
        write(addtionalOutputUnit,*) 'wavelength grid is the grid for the earth radiance'
        write(addtionalOutputUnit,'(2A)') 'wavelength (nm)    reflectance      sun_normalized_radiance  ', &
                                          '  radiance (ph/cm2/s/sr/nm)     solar irradiance (ph/cm2/s/sr/nm)'
        do iwave = 1, wavelInstrRadS(iband)%nwavel
! JdH Debug
! selected wavelenghts for comparison RT codes
         !if ( ( abs( wavelInstrRadS(iband)%wavel(iwave) - 758.0 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 759.0 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 759.3 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 759.5 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 760.0 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 760.1 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 761.0 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 761.4 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 762.0 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 763.0 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 764.0 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 764.3 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 765.0 ) < 0.001 ) .or. &
         !     ( abs( wavelInstrRadS(iband)%wavel(iwave) - 766.0 ) < 0.001 ) ) then
         ! write(addtionalOutputUnit,'(F15.8, 2F20.12, 2E30.7)') wavelInstrRadS(iband)%wavel(iwave),  &
         !    PI * earthRadianceS(iband)%rad_meas(1,iwave) / solarIrradianceS(iband)%solIrrMeas(iwave) / geometryS%u0, &
         !    earthRadianceS(iband)%rad_meas(1,iwave) / solarIrradianceS(iband)%solIrrMeas(iwave),   &
         !    earthRadianceS(iband)%rad_meas(1,iwave),                                               &
         !    solarIrradianceS(iband)%solIrrMeas(iwave)
         ! end if 
          write(addtionalOutputUnit,'(F15.8, 2F20.12, 2E30.7)') wavelInstrRadS(iband)%wavel(iwave),  &
             PI * earthRadianceS(iband)%rad_meas(1,iwave) / solarIrradianceS(iband)%solIrrMeas(iwave) / geometryS%u0, &
             earthRadianceS(iband)%rad_meas(1,iwave) / solarIrradianceS(iband)%solIrrMeas(iwave),   &
             earthRadianceS(iband)%rad_meas(1,iwave),                                               &
             solarIrradianceS(iband)%solIrrMeas(iwave)
        end do
        if ( earthRadianceS(iband)%dimSV > 1 ) then
          write(addtionalOutputUnit,'(2A)') 'wavelength (nm)   degree of linear polarization (%)   P=-100 Q/I'
          do iwave = 1, wavelInstrRadS(iband)%nwavel
            write(addtionalOutputUnit,'(F15.8, 2F20.12)') wavelInstrRadS(iband)%wavel(iwave),  &
            100.0d0 * sqrt(   earthRadianceS(iband)%rad(2,iwave)**2   &
                            + earthRadianceS(iband)%rad(3,iwave)**2 ) &
                          / earthRadianceS(iband)%rad(1,iwave),       &
            -100.0d0 * earthRadianceS(iband)%rad(2,iwave) / earthRadianceS(iband)%rad(1,iwave)
          end do
        end if
      end do ! iband
      write(addtionalOutputUnit,*)

    end subroutine writeTestDataInstr


    subroutine write_disamar_sim(errS, numSpectrBands,  wavelInstrIrrSimS, wavelInstrRadSimS, solarIrradianceSimS, &
                                 earthRadianceSimS, retrS)

      ! write_disamar_sim writes the simulated reflectance and its error on the instrument spectral grid
      ! in a format that can be read by disamar. By running disamar two times:
      !   first with simulationOnly = 1 and useReflFromFile = 0
      !   first with simulationOnly = 0 and useReflFromFile = 1 and using
      !       externalReflectanceFileName = disamar.sim
      ! it is possible to do retrievals in two steps.
      ! Repeated retrievals can be done using different retrieval settings by repeating the second step.
      ! In addition, this makes it possible to perform calculations with different settings
      ! for the parameters that are currently the same for simulation and retrieval, such as
      ! correction for a spherical geometry.

      implicit none

      type(errorType),         intent(inout) :: errS
      integer,                 intent(in) :: numSpectrBands
      type(wavelInstrType),    intent(in) :: wavelInstrIrrSimS(numSpectrBands)
      type(wavelInstrType),    intent(in) :: wavelInstrRadSimS(numSpectrBands)
      type(solarIrrType),      intent(in) :: solarIrradianceSimS(numSpectrBands)  ! solar irradiance on HR and instrument grid
      type(earthRadianceType), intent(in) :: earthRadianceSimS(numSpectrBands)    ! radiance and derivatives on HR and instr grid
      type(retrType),          intent(in) :: retrS                                ! fit parameters for iterative solution

      ! local

      logical, parameter :: old_version = .false.
      integer            :: iband, iwave, index

      if ( old_version ) then

        ! write header
        write( writeIrrRadUnit,'(A)') '# ========================================='
        write( writeIrrRadUnit,'(A)') '# Reflectance file that can be read by DISAMAR'
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') "# A '# ' in the first two columns is comment "
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') '# The values read are (three values per line separated by blanks)'
        write( writeIrrRadUnit,'(A)') '#    wavelength (in nm)'
        write( writeIrrRadUnit,'(A)') '#    SNR ( signal to noise ratio = reflectance / precision_reflectance )'
        write( writeIrrRadUnit,'(A)') '#    reflectance ( = sun-normalized radiance here )'
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') '# SNR is used instead of precision because the values can directly'
        write( writeIrrRadUnit,'(A)') '# be copied from from the disamar.out text file.'
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') "# The values have to be preceded with 'data  ' at the beginning of the line"
        write( writeIrrRadUnit,'(A)') "# Noise errors are specified in this file through SNR values"
        write( writeIrrRadUnit,'(A)') "# Calibration errors are taken from the configuration file "
        write( writeIrrRadUnit,'(A)') '# ========================================='

        index = 1  ! wavelength counter
        do iband = 1, numSpectrBands
          write( writeIrrRadUnit,'(A)') 'start_fit_window'
          do iwave = 1, wavelInstrRadSimS(iband)%nwavel
            write( writeIrrRadUnit, '(A, 2F15.6, F15.10)') 'data', wavelInstrRadSimS(iband)%wavel(iwave),  &
                                                      retrS%reflMeas(index) / retrS%reflNoiseError(index),   &
                                                      retrS%reflMeas(index)
            index = index + 1
          end do ! iwave
          write( writeIrrRadUnit,'(A)') 'end_fit_window'
        end do ! iband
        write( writeIrrRadUnit,'(A)') 'end_file'

      else

        ! write header
        write( writeIrrRadUnit,'(A)') '# ========================================================================='
        write( writeIrrRadUnit,'(A)') '# Radiance and irradiance data that can be read by DISAMAR'
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') "# A '# ' in the first two columns is comment (note the blank after #)"
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') '# The values read are (three values per line separated by blanks)'
        write( writeIrrRadUnit,'(A)') '#    wavelength (in nm)'
        write( writeIrrRadUnit,'(A)') '#    SNR ( signal to noise ratio )'
        write( writeIrrRadUnit,'(A)') '#    radiance (ph/s/nm/cm2/sr) or irradiance (ph/s/nm/cm2)'
        write( writeIrrRadUnit,'(A)') "# The values have to be preceded with 'irr  ' or 'rad  '"
        write( writeIrrRadUnit,'(A)') "# at the beginning of the line for the irradiance or radiance, respectively"
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') '# It is assumed that the wavelengths are ascending for each channel.'
        write( writeIrrRadUnit,'(A)') '# Here channal means a part of the (measured) spectrum that contains'
        write( writeIrrRadUnit,'(A)') '# one or more fit windows. The wavelengths for the channel are read.'
        write( writeIrrRadUnit,'(A)') '# Next, the wavelengths for the fit window(s) are selected.'
        write( writeIrrRadUnit,'(A)') '# For instance the channel can be 370-510 nm which contains the fit windows'
        write( writeIrrRadUnit,'(A)') '# 420-450 nm for NO2 and 460-490 nm for O2-O2. The fit windows are'
        write( writeIrrRadUnit,'(A)') '# specified in the configuration file.'
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') '# Data can be written for more than one channel using the keywords'
        write( writeIrrRadUnit,'(A)') "# 'start_channel' and 'end_channel' as delimiters"
        write( writeIrrRadUnit,'(A)') "# channels can partially overlap, e.g. (300, 312) and (308, 330)"
        write( writeIrrRadUnit,'(A)') "# but a fit windoe can not have wavelengths in the overlap region"
        write( writeIrrRadUnit,'(A)') "# because it is not clear what channel should be used."
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') "# the reading of the file stops when the string 'end_file' is encountered"
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') "# The wavelength grids for irradiance and radiane may differ. "
        write( writeIrrRadUnit,'(A)') "# High sampling interpolation is then used to obtain the solar irradiance "
        write( writeIrrRadUnit,'(A)') "# on the Earth radiance wavelength grid "
        write( writeIrrRadUnit,'(A)') '# '
        write( writeIrrRadUnit,'(A)') "# Noise errors are specified in this file through SNR values"
        write( writeIrrRadUnit,'(A)') "# Calibration errors are taken from the configuration file "
        write( writeIrrRadUnit,'(A)') '# =========================================================================='

        do iband = 1, numSpectrBands
          write( writeIrrRadUnit,'(A)') 'start_channel_irr'
          do iwave = 1, wavelInstrIrrSimS(iband)%nwavel
            write( writeIrrRadUnit, '(A, 2F15.8, ES18.9)') 'irr ', wavelInstrIrrSimS(iband)%wavel(iwave),  &
                        solarIrradianceSimS(iband)%SN(iwave),  solarIrradianceSimS(iband)%solIrrMeas(iwave)
          end do ! iwave
          write( writeIrrRadUnit,'(A)') 'end_channel_irr'
          write( writeIrrRadUnit,'(A)') 'start_channel_rad'
          do iwave = 1, wavelInstrRadSimS(iband)%nwavel
            write( writeIrrRadUnit, '(A, 2F15.8, ES18.9)') 'rad ', wavelInstrRadSimS(iband)%wavel(iwave),  &
                        earthRadianceSimS(iband)%SN(iwave),  earthRadianceSimS(iband)%rad_meas(1,iwave)
          end do ! iwave
          write( writeIrrRadUnit,'(A)') 'end_channel_rad'
        end do ! iband
        write( writeIrrRadUnit,'(A)') 'end_file'

      end if

    end subroutine write_disamar_sim


    subroutine writePolarizationCorrectionFile ( errS, numSpectrBands, polCorrectionRetrS )


      implicit none

      type(errorType),         intent(inout) :: errS
      integer,                 intent(in)    :: numSpectrBands
      type(polCorrectionType), intent(in)    :: polCorrectionRetrS(numSpectrBands)

      ! local
      integer   :: iband, iwave, OpenError


      open(UNIT= polarizationCorrUnit, ACTION= 'WRITE', STATUS = 'replace', IOSTAT= OpenError, &
           FILE = polCorrectionRetrS(1)%polarizationCorrectionFileName)
      if (OpenError /= 0) then
         write(errS%temp,*) 'writePolarizationCorrectionFile: Error in opening polarization correction file: ', &
                      trim(polCorrectionRetrS(1)%polarizationCorrectionFileName)
         call errorAddLine(errS, errS%temp)
         call mystop(errS, 'stopped because file could not be opened')
         if (errorCheck(errS)) return
      end if

              ! write header
        write( polarizationCorrUnit,'(A)') '# ========================================================================='
        write( polarizationCorrUnit,'(A)') '# Polarization correction (+ RRS correction ) data that can be read by DISAMAR'
        write( polarizationCorrUnit,'(A)') '# '
        write( polarizationCorrUnit,'(A)') "# A '# ' in the first two columns is comment (note the blank after #)"
        write( polarizationCorrUnit,'(A)') '# '
        write( polarizationCorrUnit,'(A)') '# The values read are (two values per line separated by blanks)'
        write( polarizationCorrUnit,'(A)') '#    wavelength (in nm)'
        write( polarizationCorrUnit,'(A)') '#    correction in percent'
        write( polarizationCorrUnit,'(A)') "# The values have to be preceded with 'corr ' at the beginning of the line"
        write( polarizationCorrUnit,'(A)') '# '
        write( polarizationCorrUnit,'(A)') '# It is assumed that the wavelengths are ascending for each spectral band.'
        write( polarizationCorrUnit,'(A)') '# Data can be written for more than one channel using the keywords'
        write( polarizationCorrUnit,'(A)') "# 'start_channel' and 'end_channel' as delimiters"
        write( polarizationCorrUnit,'(A)') "# channels can partially overlap, e.g. (300, 312) and (308, 330)"
        write( polarizationCorrUnit,'(A)') '# '
        write( polarizationCorrUnit,'(A)') "# the reading of the file stops when the string 'end_file' is encountered"
        write( polarizationCorrUnit,'(A)') '# '
        write( polarizationCorrUnit,'(A)') '# Applying the correction means multiplying the Earth radiance with the'
        write( polarizationCorrUnit,'(A)') '# following correction factor'
        write( polarizationCorrUnit,'(A)') "# L_corrected(:) = L_uncorrected(:) / (1.0d0 - correction(:)/100.0d0)"
        write( polarizationCorrUnit,'(A)') '# '
        write( polarizationCorrUnit,'(A)') '# =========================================================================='

        do iband = 1, numSpectrBands
          write( polarizationCorrUnit,'(A)') 'start_channel'
          do iwave = 1, polCorrectionRetrS(iband)%nwavel
            write( polarizationCorrUnit, '(A, 2F15.8)') 'corr ', polCorrectionRetrS(iband)%wavel(iwave),  &
                                                                 polCorrectionRetrS(iband)%correction(iwave)
          end do ! iwave
          write( polarizationCorrUnit,'(A)') 'end_channel'
        end do ! iband
        write( polarizationCorrUnit,'(A)') 'end_file'

      close(UNIT= polarizationCorrUnit)

    end subroutine writePolarizationCorrectionFile


  end module writeModule
