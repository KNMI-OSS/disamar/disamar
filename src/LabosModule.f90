!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module LabosModule

! The module 'LabosModule' uses LAyer Based Orders of Scattering
! (LABOS) to calculate the reflectance and Jacobians
! with respect to atmospheric/surface parameters.
! Weighting functions at the interfaces of layers are calculated,
! not weghting functions for the layers. Further, number densities
! at an altitude grid are used as input, not optical properties of
! a stack of layers.

! Author: Johan F. de Haan, KNMI KS/AK
! Version number: 0.5  April, 2007

! Version 0.6 Nov 4 2007
!
! Version 0.7 March 2010 added single scattering
!
! Version 0.8 Dec 2010 added polarization
! Version 0.9 April 2011 correction for the derivatives in case of polarized light. 
! Version 1.0 October 2011 removed bugs when calculating the Fourier coefficients ZmPlus and ZmMin
!                          by comparing the results with the adding code of Victor Dolman.
!                          There remain differences if circular polarization is used (dimSV = 4).
!                          It is not clear which code (if any) is correct.
! Version 1.1 Jan 2012  added calculation of the weighting function for surface emission, wfEmission
! version 1.2 June 2014: improved error handling (by Mark)
! ----------------------------------------------------------------------------------

  use dataStructures
  use addingTools, only : adding, fillsurface
  use mathTools,   only : GaussDivPoints, LU_decomposition, locate, solve_lin_system_LU_based

  implicit none

! set default to private, i.e. everything is private to the module, except
! that what is made explicitly public
  private
  public layerBasedOrdersScattering, singleScattering, fillPlmVector, fillZplusZmin, fillAttenuation
  public CalcRTlayers, fillsurface, allocCoef, deallocCoef, semul, esmul, smul
  public layerBasedOrdersScattering_fc, apply_q3, apply_q4


  contains

  subroutine layerBasedOrdersScattering(errS,   &
      optPropRTMGridS,        & ! optical properties at RTM grids
      controlS,               & ! control parameters
      geometryS,              & ! parameters for the geometry
      albedo,                 & ! albedo of the Lambertian surface at the level RTMnlevelCloud
                                ! which is the surface albedo if RTMnlevelCloud = 0
      RTMnlevelCloud,         & ! optPropRTMGridS%RTMaltitude(RTMnlevelCloud) is index for Lambertian cloud
      babs,                   & ! absorption optical thickness
      bsca,                   & ! scattering optical thickness
      refl,                   & ! reflectance
      contribRefl,            & ! path radiance profile
      UD,                     & ! internal radiation field at interfaces between homogeneous layers
      wfInterfkscaGas,        & ! weighting function for scattering by molecules
      wfInterfkscaAerAbove,   & ! weighting function for scattering by aerosol; limit for approach from above
      wfInterfkscaAerBelow,   & ! weighting function for scattering by aerosol; limit for approach from below
      wfInterfkscaCldAbove,   & ! weighting function for scattering by cloud; limit for approach from above
      wfInterfkscaCldBelow,   & ! weighting function for scattering by cloud; limit for approach from below
      wfInterfkabs,           & ! weighting function for absorption (the same for different types of absorption)
      wfAlbedo,               & ! weighting function for the (surface/cloud) albedo
      wfEmission,             & ! weighting function for surface emission, dR/dEmission
      status)

      implicit none

      !INPUT
      type(errorType),             intent(inout) :: errS
      type(optPropRTMGridType),    intent(in)    :: optPropRTMGridS
      type(controlType),           intent(in)    :: controlS
      type(geometryType),          intent(in)    :: geometryS

      integer,                     intent(in)    :: RTMnlevelCloud

      real(8),                     intent(in)    :: albedo

      ! OUTPUT
      real(8),  intent(out) :: bsca, babs  ! scattering and absorption (vertical) optical thickness

      real(8),  intent(out) :: refl(controlS%dimSV)

      ! contribution function or altitude resolved path radiance, such that the integral over
      ! the altitude gives the sun-normalized reflectance except for the direct radiance that
      ! has been reflected by the surface (or Lambertian cloud surface).

      real(8),      intent(out) :: contribRefl(controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)
      type(UDType)              :: UD(0:optPropRTMGridS%RTMnlayer)
      

      ! Weighting functions w.r.t. atmospheric parameters at the interfaces between the layers
      ! As the aerosol and cloud properties can be discontineous between layers, the weighting
      ! function can be discontineous. This happens if the phase function differs between layers.
      ! Therefore, we calculate the limits when the interface is approached from above and below.
      ! Note that for absorption and extinction the weighting functions for trace gas,
      ! aerosol and cloud are all the same. Absorption here can also be read as extinction.
      real(8),  intent(out) :: wfInterfkscaGas(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkscaAerAbove(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkscaAerBelow(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkscaCldAbove(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkscaCldBelow(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkabs(0:optPropRTMGridS%RTMnlayer)

      ! Weighting function for Lambertian ground surface albedo or Lamvertian cloud albedo
      real(8),  intent(out) :: wfAlbedo

      ! Weighting function for isotropic emission at the ground surface
      real(8),  intent(out) :: wfEmission

      integer,  intent(out) :: status

      ! local parameter for the maximum number of orders of scattering
      integer  :: numOrdersMax

      ! attenuation between the interfaces used also for spherical correction
      real(8)  :: atten (geometryS%nmutot, 0:optPropRTMGridS%RTMnlayer, 0:optPropRTMGridS%RTMnlayer)

      ! RT_fc has the following structure

      ! | R(g1,g1)  ....  R(g1,gn)  R(g1,v)  R(g1,s) |
      ! | R(g2,g1)  ....  R(g2,gn)  R(g2,v)  R(g2,s) |
      ! | ........  ....  ........  .......  ........|
      ! | R(gn,g1)  ....  R(gn,gn)  R(gn,v)  R(gn,s) |
      ! | R( v,g1)  ....  R( v,gn)  R( v,v)  R( v,s) |
      ! | R( s,g1)  ....  R( s,gn)  R( s,v)  R( s,s) |

      ! where g1 ... gn are the Gaussian division points, v is the viewing and s the solar directions

      ! For U and D we have the following structure

      ! | D(g1,v) D(g1,s) |
      ! | D(g2,v) D(g2,s) |
      ! | ....... ....... |
      ! | D(gn,v) D(gn,s) |
      ! | D( v,v) D( v,s) |
      ! | D( s,v) D( s,s) |

      ! if dimSV > 1 each element of these matrices becomes a (dimsv*dimsv) submatrix
      ! a so-called supermatrx and a column becomes a supervector
      
      ! reflection and transmission properties for individual atmospheric layers
      ! (surface is layer with index 0; Lambertian cloud has index RTMnlevelCloud)
      type(RTType):: RT_fc(0:optPropRTMGridS%RTMnlayer)

      ! Fourier coefficients global internal fields at the interfaces for the axes
      type(UDType) :: UD_fc(0:optPropRTMGridS%RTMnlayer)

      ! storage for individual orders of scattering for the layers
      type(UDType) :: UDorde_fc(0:optPropRTMGridS%RTMnlayer)

      ! local internal fields (equivalent of source function, but for the layers)
      ! these are the result of scattering by a certain layer at the interfaces of that layer
      type(UDLocalType) :: UDLocal_fc(0:optPropRTMGridS%RTMnlayer)

      ! sum of the orders of scattering for the layers
      type(UDLocalType) :: UDsumLocal_fc(0:optPropRTMGridS%RTMnlayer)

      ! fourier coefficients of the reflectance
      real(8) :: refl_fc(controlS%dimSV)

      ! Fourier coefficients of the weighting functions
      real(8) :: wfInterfkscaGas_fc(0:optPropRTMGridS%RTMnlayer)
      real(8) :: wfInterfkscaAerAbove_fc(0:optPropRTMGridS%RTMnlayer)
      real(8) :: wfInterfkscaAerBelow_fc(0:optPropRTMGridS%RTMnlayer)
      real(8) :: wfInterfkscaCldAbove_fc(0:optPropRTMGridS%RTMnlayer)
      real(8) :: wfInterfkscaCldBelow_fc(0:optPropRTMGridS%RTMnlayer)
      real(8) :: wfInterfkabs_fc(0:optPropRTMGridS%RTMnlayer)
      real(8) :: wfAlbedo_fc
      real(8) :: wfEmission_fc

      ! contribution to reflectance of a thin layer: dR/dz
      ! note that this differs from the change in reflectance if a thin layer is added
      ! to the atmosphere at an altitude z, because the scattering is non-linear
      ! in the amount of scattering and absorbing material
      ! The integral from 0 to TOA of the contribution function gives the (normalized) path radiance
      ! The reflectance is the path radiance plus the direct surface contribution exp(-tau/mu)U(0,mu,mu0,dphi)

      ! Only the derivative dR/dEmission is calculated, not the contribution to the radiance of the 
      ! surface emission itself. The emission emerging from the top of the atmosphere is given by
      ! E * wfEmission / PI where E is the isotropic emission at the surface per unit area.

      real(8) :: contribRefl_fc(controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)

      real(8) :: cos_m_dphi         ! cosine( m * azimuth diff)
      real(8) :: sin_m_dphi         ! sine  ( m * azimuth diff)
      real(8) :: cs_m_dphi          ! cosine or sine  ( m * azimuth diff)
      real(8) :: factor 
      integer :: iFourier
      integer :: ilevel, nmutot, nmuextra, nGauss, iSV, imu, imu0, ind

      integer :: RTMnlayer, maxExpCoef, FourierMax
      integer :: dimSV, dimSV_fc ! dimension Stokes vector for the Fourier coefficients

      type(fc_coefficients), pointer  :: fcCoef(:) 

      ! Assume proper processing
      status = 0

      nmuextra   = 2
      nGauss     = geometryS%nGauss
      nmutot     = geometryS%nmutot
      RTMnlayer  = optPropRTMGridS%RTMnlayer
      maxExpCoef = optPropRTMGridS%maxExpCoef
      dimSV      = controlS%dimSV

      ! initialize output to zero
      ! later the Fourier terms are calculated and
      ! added to these quantities

      refl                    = 0.0d0
      contribRefl             = 0.0d0
      wfAlbedo                = 0.0d0
      wfEmission              = 0.0d0
      wfInterfkscaGas         = 0.0d0
      wfInterfkscaAerAbove    = 0.0d0
      wfInterfkscaAerBelow    = 0.0d0
      wfInterfkscaCldAbove    = 0.0d0
      wfInterfkscaCldBelow    = 0.0d0
      wfInterfkabs            = 0.0d0
      do ilevel = 0, RTMnlayer
        UD(ilevel)%E(:)       = 0.0d0
        UD(ilevel)%U(:,:)     = 0.0d0
        UD(ilevel)%D(:,:)     = 0.0d0        
      end do

      if ( ( (1.0d0 - geometryS%uu) < 1.0d-5 ) .or. ( (1.0d0 - geometryS%u0) < 1.0d-5 ) ) then 
        if ( controlS%dimSV == 1 ) then
          FourierMax = 0
        else
          FourierMax = 2    ! azimuth dependence due to reference plane Stokes parameters 
        end if
      else
        FourierMax = maxExpCoef
      end if

      bsca = sum(optPropRTMGridS%opticalThicknLay(:) * optPropRTMGridS%ssaLay(:) )
      babs = sum(optPropRTMGridS%opticalThicknLay(:) * (1.0d0 - optPropRTMGridS%ssaLay(:)) )

      ! set numOrdersMax - preliminary solution better determination of convergence has to be developed
      if ( controlS%numOrdersMax == 0 ) then
        numOrdersMax = int(bsca + 15.0d0)
      else
        numOrdersMax = controlS%numOrdersMax
      end if

      call fillAttenuation (errS, optPropRTMGridS, controlS, geometryS, nmutot, RTMnlayer, atten)
      if (errorCheck(errS)) return

      if ( controlS%aerosolLayerHeight ) then
        ! allocate the elements of the structure 'fcCoef'
        dimSV_fc = 1
        call allocCoef(errS, fcCoef, nmutot, dimSV_fc, maxExpCoef)
                 
        ! Claim memory for values pertaining to the interfaces, layers and  atmosphere
        call allocateReflTransInternalField(errS, RTMnlayer, dimSV_fc, nmutot, nmuextra, RT_fc,  &
                                            UDorde_fc, UDLocal_fc, UDsumLocal_fc, UD_fc)
      end if ! controlS%aerosolLayerHeight
  
      do iFourier = 0, FourierMax  ! start Fourier loop

        if ( .not. controlS%aerosolLayerHeight ) then


          if ( iFourier > controlS%fourierFloorScalar ) then
            dimSV_fc = 1
          else
            dimSV_fc = dimSV
          end if

          ! allocate the elements of the structure 'fcCoef'
          call allocCoef(errS, fcCoef, nmutot, dimSV_fc, maxExpCoef)
          if (errorCheck(errS)) return
                 
          ! Claim memory for values pertaining to the interfaces, layers and  atmosphere
          call allocateReflTransInternalField(errS, RTMnlayer, dimSV_fc, nmutot, nmuextra, RT_fc,  &
                                              UDorde_fc, UDLocal_fc, UDsumLocal_fc, UD_fc)
          if (errorCheck(errS)) return

        end if ! .not. controlS%aerosolLayerHeight

        ! pre-calculate generalized spherical functions (for polarized light)
        ! the structure fcCoef is defined and declared in the module declaration part
        ! NOTE that the calculation of these functions could be taken outside the wavelength loop
        ! and moved to the reading of the configuration file where the grid for the
        ! direction cosines is defined and the maximum number of expansion coefficients are calculated.
        ! Do this only if it saves a significant amount of time (use profiling).

        call fillPlmVector(errS, fcCoef, iFourier, dimSV_fc, nmutot, maxExpCoef, geometryS)
        if (errorCheck(errS)) return

        ! calculate the optical properties of the individual layers
        ! and fill structure RTlayers

        call CalcRTlayers(errS, fcCoef, iFourier, maxExpCoef, RTMnlevelCloud, RTMnlayer, dimSV, dimSV_fc, nmutot, &
                          nGauss, controlS, geometryS, optPropRTMGridS, RT_fc)
        if (errorCheck(errS)) return

        call fillsurface(errS, iFourier, dimSV_fc, nmutot, albedo, geometryS, RT_fc(RTMnlevelCloud)%R,     &
             RT_fc(RTMnlevelCloud)%T, RT_fc(RTMnlevelCloud)%Rst, RT_fc(RTMnlevelCloud)%Tst)
        if (errorCheck(errS)) return

        ! calculate the internal radiation field at the interfaces between the layers
        ! using successive orders of scattering  or the adding method

        if ( controlS%useAdding ) then

          call adding(errS, optPropRTMGridS, iFourier, controlS, RTMnlevelCloud, RTMnlayer,    &
                      atten, dimSV_fc, nmutot, nGauss, RT_fc, geometryS, UDsumLocal_fc, UD_fc)
          if (errorCheck(errS)) return

          do ilevel = 0, RTMnlayer
            UDLocal_fc(ilevel)%U    = 0.0d0
            UDLocal_fc(ilevel)%D    = 0.0d0
            UDorde_fc(ilevel)%E     = 0.0d0
            UDorde_fc(ilevel)%U     = 0.0d0
            UDorde_fc(ilevel)%D     = 0.0d0
          end do

        else

          call ordersScat(errS, controlS, geometryS, numOrdersMax, RTMnlevelCloud, &
                          RTMnlayer, atten, dimSV_fc, nmutot, nmuextra, nGauss,    &
                          RT_fc, UDsumLocal_fc, UDLocal_fc, UDorde_fc, UD_fc)
          if (errorCheck(errS)) return

        end if

       ! calculate Fourier coefficient of the weighting functions

        call CalcWeightingFunctionsInterface (errS, fcCoef, optPropRTMGridS, controlS, &
                                              iFourier, RTMnlayer, RTMnlevelCloud,     &
                                              dimSV, dimSV_fc, nmutot, nGauss,         &
                                              geometryS, UDsumLocal_fc, UD_fc,         &
                                              wfInterfkscaGas_fc,                      &
                                              wfInterfkscaAerAbove_fc,                 &
                                              wfInterfkscaAerBelow_fc,                 &
                                              wfInterfkscaCldAbove_fc,                 &
                                              wfInterfkscaCldBelow_fc,                 &
                                              wfInterfkabs_fc,                         &
                                              wfAlbedo_fc)
        if (errorCheck(errS)) return

       ! Calculate Fourier coefficient of the reflectance by numerical integration
       ! over the source function.

        refl_fc(:)          = 0.0d0
        contribrefl_fc(:,:) = 0.0d0

        call CalcReflectance (errS, fcCoef, optPropRTMGridS, controlS,   &
                              RTMnlayer, RTMnlevelCloud,           &
                              iFourier, dimSV, dimSV_fc, nmutot,   &
                              nGauss, geometryS, UD_fc,            &
                              contribrefl_fc, refl_fc)
        if (errorCheck(errS)) return

        ! calculate the Fourier coefficient of the surface emission
        call CalcDerivdRdEmission(errS, dimSV_fc, nGauss, iFourier, RTMnlayer, geometryS, UD_fc, wfEmission_fc)
        if (errorCheck(errS)) return

        ! sum over Fourier terms

        cos_m_dphi = cos(iFourier * geometryS%dphiRad )
        sin_m_dphi = sin(iFourier * geometryS%dphiRad )

        factor = 2.0d0
        if (iFourier == 0) factor = 1.0d0


        wfAlbedo               = wfAlbedo                + factor * wfAlbedo_fc                * cos_m_dphi
        wfEmission             = wfEmission              + factor * wfEmission_fc              * cos_m_dphi
        wfInterfkscaAerAbove(:)= wfInterfkscaAerAbove(:) + factor * wfInterfkscaAerAbove_fc(:) * cos_m_dphi
        wfInterfkscaAerBelow(:)= wfInterfkscaAerBelow(:) + factor * wfInterfkscaAerBelow_fc(:) * cos_m_dphi
        wfInterfkscaCldAbove(:)= wfInterfkscaCldAbove(:) + factor * wfInterfkscaCldAbove_fc(:) * cos_m_dphi
        wfInterfkscaCldBelow(:)= wfInterfkscaCldBelow(:) + factor * wfInterfkscaCldBelow_fc(:) * cos_m_dphi
        wfInterfkscaGas(:)     = wfInterfkscaGas(:)      + factor * wfInterfkscaGas_fc(:)      * cos_m_dphi
        wfInterfkabs(:)        = wfInterfkabs(:)         + factor * wfInterfkabs_fc(:)         * cos_m_dphi

        do iSV = 1, dimSV_fc
          if ( iSV > 2 ) then
            cs_m_dphi = sin_m_dphi
          else
            cs_m_dphi = cos_m_dphi
          end if
          refl(iSV)          = refl(iSV)          + factor * refl_fc(iSV)           * cs_m_dphi
          contribrefl(iSV,:) = contribrefl(iSV,:) + factor * contribrefl_fc(iSV, :) * cs_m_dphi
        end do

        ! sum over Fourier terms of the internal radiation field
        if ( iFourier == 0 ) then
          do ilevel = 0, RTMnlayer
            UD(ilevel)%E(:) = UD_fc(ilevel)%E(:)
          end do ! ilevel
        end if ! iFourier == 0
        
        do iSV = 1, dimSV_fc
          if ( iSV > 2 ) then
            cs_m_dphi = sin_m_dphi
          else
            cs_m_dphi = cos_m_dphi
          end if
          do ilevel = 0, RTMnlayer
            do imu0 = 1, 2
              do imu = 1, nmutot
                  ind = 1 + (imu -1) * dimSV_fc
                  UD(ilevel)%D(ind,imu0) = UD(ilevel)%D(ind,imu0) + factor * UD_fc(ilevel)%D(ind,imu0) * cs_m_dphi
                  UD(ilevel)%U(ind,imu0) = UD(ilevel)%U(ind,imu0) + factor * UD_fc(ilevel)%U(ind,imu0) * cs_m_dphi
              end do ! imu
            end do ! imu0
          end do ! ilevel
        end do ! iSV

        ! clean up
        if ( .not. controlS%aerosolLayerHeight ) then
          call deallocCoef(errS, fcCoef, maxExpCoef)
          if (errorCheck(errS)) return
          call deallocateReflTransIntField(errS, RTMnlayer, RT_fc, UDorde_fc, UDLocal_fc, UDsumLocal_fc, UD_fc)
          if (errorCheck(errS)) return
        end if ! .not. controlS%aerosolLayerHeight
     
      end do  ! Fourier loop

      if ( controlS%aerosolLayerHeight ) then
        ! clean up
        call deallocCoef(errS, fcCoef, maxExpCoef)
        if (errorCheck(errS)) return
        call deallocateReflTransIntField(errS, RTMnlayer, RT_fc, UDorde_fc, UDLocal_fc, UDsumLocal_fc, UD_fc)
        if (errorCheck(errS)) return
      end if ! controlS%aerosolLayerHeight
    
  end subroutine layerBasedOrdersScattering


  subroutine layerBasedOrdersScattering_fc(errS,   &
      iFourier, maxFourierTermLUT,  & ! Fourier index 0, 1, 2, ...
      dimSV,                   & ! maximum dimension Stokes vector for the Fourier coefficients
      dimSV_fc,                & ! dimension Stokes vector for the current Fourier coefficient
      optPropRTMGridS,         & ! optical properties at RTM grids
      RRS_RingS,               & ! info on RRS
      controlS, createLUTS,    & ! control parameters
      geometryS,               & ! parameters for the geometry
      iwave,                   & ! wavelength index
      wavelMRS,                & ! wavelength structure; either HR or instr grid
      albedo,                  & ! albedo of the Lambertian surface at the level RTMnlevelCloud
                                 ! which is the surface albedo if RTMnlevelCloud = 0
      RTMnlevelCloud,          & ! optPropRTMGridS%RTMaltitude(RTMnlevelCloud) is index for Lambertian cloud
      babs,                    & ! absorption optical thickness
      bsca,                    & ! scattering optical thickness
      refl_fc,                 & ! Fourier coefficient reflectance
      contribRefl_fc,          & ! Fourier coefficient path radiance profile
      UD_fc,                   & ! Fourier coefficient internal radiation field at interfaces between homogeneous layers
      wfInterfkscaGas_fc,      & ! weighting function for scattering by molecules
      wfInterfkscaAerAbove_fc, & ! weighting function for scattering by aerosol; limit for approach from above
      wfInterfkscaAerBelow_fc, & ! weighting function for scattering by aerosol; limit for approach from below
      wfInterfkscaCldAbove_fc, & ! weighting function for scattering by cloud; limit for approach from above
      wfInterfkscaCldBelow_fc, & ! weighting function for scattering by cloud; limit for approach from below
      wfInterfkabs_fc,         & ! weighting function for absorption (the same for different types of absorption)
      wfAlbedo_fc,             & ! weighting function for the (surface/cloud) albedo
      wfEmission_fc,           & ! weighting function for surface emission, dR/dEmission
      status)

      implicit none

      !INPUT
      type(errorType), intent(inout) :: errS
      integer,                     intent(in)   :: iFourier, maxFourierTermLUT, dimSV, dimSV_fc
      type(optPropRTMGridType),    intent(in)   :: optPropRTMGridS
      type(RRS_RingType),          intent(in)   :: RRS_RingS
      type(controlType),           intent(in)   :: controlS
      type(createLUTType),         intent(inout):: createLUTS
      type(geometryType),          intent(in)   :: geometryS

      integer,                     intent(in)   :: RTMnlevelCloud, iwave
      type(wavelHRType),           intent(in)   :: wavelMRS   ! instr - high resolution wavelength grid

      real(8),                     intent(in)   :: albedo

      ! OUTPUT
      real(8),  intent(out) :: bsca, babs  ! scattering and absorption (vertical) optical thickness

      real(8),  intent(out) :: refl_fc(dimSV_fc)

      ! contribution function or altitude resolved path radiance, such that the integral over
      ! the altitude gives the sun-normalized reflectance except for the direct radiance that
      ! has been reflected by the surface (or Lambertian cloud surface).

      real(8),      intent(out) :: contribRefl_fc(dimSV_fc, 0:optPropRTMGridS%RTMnlayer)
      type(UDType)              :: UD_fc(0:optPropRTMGridS%RTMnlayer)
      

      ! Weighting functions w.r.t. atmospheric parameters at the interfaces between the layers
      ! As the aerosol and cloud properties can be discontineous between layers, the weighting
      ! function can be discontineous. This happens if the phase function differs between layers.
      ! Therefore, we calculate the limits when the interface is approached from above and below.
      ! Note that for absorption and extinction the weighting functions for trace gas,
      ! aerosol and cloud are all the same. Absorption here can also be read as extinction.
      real(8),  intent(out) :: wfInterfkscaGas_fc(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkscaAerAbove_fc(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkscaAerBelow_fc(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkscaCldAbove_fc(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkscaCldBelow_fc(0:optPropRTMGridS%RTMnlayer)
      real(8),  intent(out) :: wfInterfkabs_fc(0:optPropRTMGridS%RTMnlayer)

      ! Weighting function for Lambertian ground surface albedo or Lamvertian cloud albedo
      real(8),  intent(out) :: wfAlbedo_fc

      ! Weighting function for isotropic emission at the ground surface
      real(8),  intent(out) :: wfEmission_fc

      integer,  intent(out) :: status

      ! local parameter for the maximum number of orders of scattering
      integer  :: numOrdersMax

      ! attenuation between the interfaces used also for spherical correction
      real(8)  :: atten (geometryS%nmutot, 0:optPropRTMGridS%RTMnlayer, 0:optPropRTMGridS%RTMnlayer)

      ! RT_fc has the following structure

      ! | R(g1,g1)  ....  R(g1,gn)  R(g1,v)  R(g1,s) |
      ! | R(g2,g1)  ....  R(g2,gn)  R(g2,v)  R(g2,s) |
      ! | ........  ....  ........  .......  ........|
      ! | R(gn,g1)  ....  R(gn,gn)  R(gn,v)  R(gn,s) |
      ! | R( v,g1)  ....  R( v,gn)  R( v,v)  R( v,s) |
      ! | R( s,g1)  ....  R( s,gn)  R( s,v)  R( s,s) |

      ! where g1 ... gn are the Gaussian division points, v is the viewing and s the solar directions

      ! For U and D we have the following structure

      ! | D(g1,v) D(g1,s) |
      ! | D(g2,v) D(g2,s) |
      ! | ....... ....... |
      ! | D(gn,v) D(gn,s) |
      ! | D( v,v) D( v,s) |
      ! | D( s,v) D( s,s) |

      ! if dimSV > 1 each element of these matrices becomes a (dimsv*dimsv) submatrix
      ! a so-called supermatrx and a column becomes a supervector
      
      ! reflection and transmission properties for individual atmospheric layers
      ! (surface is layer with index 0; Lambertian cloud has index RTMnlevelCloud)
      type(RTType):: RT_fc(0:optPropRTMGridS%RTMnlayer)

      ! storage for individual orders of scattering for the layers
      type(UDType) :: UDorde_fc(0:optPropRTMGridS%RTMnlayer)

      ! local internal fields (equivalent of source function, but for the layers)
      ! these are the result of scattering by a certain layer at the interfaces of that layer
      type(UDLocalType) :: UDLocal_fc(0:optPropRTMGridS%RTMnlayer)

      ! sum of the orders of scattering for the layers
      type(UDLocalType) :: UDsumLocal_fc(0:optPropRTMGridS%RTMnlayer)

      ! contribution to reflectance of a thin layer: dR/dz
      ! note that this differs from the change in reflectance if a thin layer is added
      ! to the atmosphere at an altitude z, because the scattering is non-linear
      ! in the amount of scattering and absorbing material
      ! The integral from 0 to TOA of the contribution function gives the (normalized) path radiance
      ! The reflectance is the path radiance plus the direct surface contribution exp(-tau/mu)U(0,mu,mu0,dphi)

      ! Only the derivative dR/dEmission is calculated, not the contribution to the radiance of the 
      ! surface emission itself. The emission emerging from the top of the atmosphere is given by
      ! E * wfEmission / PI where E is the isotropic emission at the surface per unit area.

      integer :: ilevel, nmutot, nmuextra, nGauss

      integer :: RTMnlayer, maxExpCoef
      type(fc_coefficients), pointer :: fcCoef(:)

      ! Assume proper processing
      status = 0

      nmuextra   = 2
      nGauss     = geometryS%nGauss
      nmutot     = geometryS%nmutot
      RTMnlayer  = optPropRTMGridS%RTMnlayer
      maxExpCoef = optPropRTMGridS%maxExpCoef

      ! initialize output to zero
      ! later the Fourier terms are calculated and
      ! added to these quantities

      refl_fc                    = 0.0d0
      contribRefl_fc             = 0.0d0
      wfAlbedo_fc                = 0.0d0
      wfEmission_fc              = 0.0d0
      wfInterfkscaGas_fc         = 0.0d0
      wfInterfkscaAerAbove_fc    = 0.0d0
      wfInterfkscaAerBelow_fc    = 0.0d0
      wfInterfkscaCldAbove_fc    = 0.0d0
      wfInterfkscaCldBelow_fc    = 0.0d0
      wfInterfkabs_fc            = 0.0d0
      do ilevel = 0, RTMnlayer
        UD_fc(ilevel)%E(:)       = 0.0d0
        UD_fc(ilevel)%U(:,:)     = 0.0d0
        UD_fc(ilevel)%D(:,:)     = 0.0d0        
      end do

      bsca = sum(optPropRTMGridS%opticalThicknLay(:) * optPropRTMGridS%ssaLay(:) )
      babs = sum(optPropRTMGridS%opticalThicknLay(:) * (1.0d0 - optPropRTMGridS%ssaLay(:)) )

      ! set numOrdersMax - preliminary solution better determination of convergence has to be developed
      if ( controlS%numOrdersMax == 0 ) then
        numOrdersMax = int(bsca + 15.0d0)
      else
        numOrdersMax = controlS%numOrdersMax
      end if

      call fillAttenuation (errS, optPropRTMGridS, controlS, geometryS, nmutot, RTMnlayer, atten)
      if (errorCheck(errS)) return

      ! allocate the elements of the structure 'fcCoef' declared in the header of this module
      call allocCoef(errS, fcCoef, nmutot, dimSV_fc, maxExpCoef)
      if (errorCheck(errS)) return
               
      ! Claim memory for values pertaining to the interfaces, layers and  atmosphere
      call allocateReflTransInternalField(errS, RTMnlayer, dimSV_fc, nmutot, nmuextra, RT_fc,  &
                                          UDorde_fc, UDLocal_fc, UDsumLocal_fc)
      if (errorCheck(errS)) return

        ! pre-calculate generalized spherical functions (for polarized light)
        ! the structure fcCoef is defined and declared in the module declaration part
        ! NOTE that the calculation of these functions could be taken outside the wavelength loop
        ! and moved to the reading of the configuration file where the grid for the
        ! direction cosines is defined and the maximum number of expansion coefficients are calculated.
        ! Do this only if it saves a significant amount of time (use profiling).

        call fillPlmVector(errS, fcCoef, iFourier, dimSV_fc, nmutot, optPropRTMGridS%maxExpCoef, geometryS)
        if (errorCheck(errS)) return

        ! calculate the optical properties of the individual layers
        ! and fill structure RTlayers

        call CalcRTlayers(errS, fcCoef, iFourier, maxExpCoef, RTMnlevelCloud, RTMnlayer, dimSV, dimSV_fc, nmutot, &
                          nGauss, controlS, geometryS, optPropRTMGridS, RT_fc)
        if (errorCheck(errS)) return

        call fillsurface(errS, iFourier, dimSV_fc, nmutot, albedo, geometryS, RT_fc(RTMnlevelCloud)%R,  &
             RT_fc(RTMnlevelCloud)%T, RT_fc(RTMnlevelCloud)%Rst, RT_fc(RTMnlevelCloud)%Tst)
        if (errorCheck(errS)) return

        ! calculate the internal radiation field at the interfaces between the layers
        ! using successive orders of scattering  or the adding method

        if ( controlS%useAdding ) then

          call adding(errS, optPropRTMGridS, iFourier, controlS, RTMnlevelCloud, RTMnlayer, atten, &
                      dimSV_fc, nmutot, nGauss, RT_fc, geometryS, UDsumLocal_fc, UD_fc,            &
                      iwave, wavelMRS, RRS_RingS, createLUTS )
          if (errorCheck(errS)) return

          do ilevel = 0, RTMnlayer
            UDLocal_fc(ilevel)%U    = 0.0d0
            UDLocal_fc(ilevel)%D    = 0.0d0
            UDorde_fc(ilevel)%E     = 0.0d0
            UDorde_fc(ilevel)%U     = 0.0d0
            UDorde_fc(ilevel)%D     = 0.0d0
          end do

        else

          call ordersScat(errS, controlS, geometryS, numOrdersMax, RTMnlevelCloud, &
                          RTMnlayer, atten, dimSV_fc, nmutot, nmuextra, nGauss,    &
                          RT_fc, UDsumLocal_fc, UDLocal_fc, UDorde_fc, UD_fc)
          if (errorCheck(errS)) return

        end if

       ! calculate Fourier coefficient of the weighting functions

        call CalcWeightingFunctionsInterface (errS, fcCoef, optPropRTMGridS, controlS, &
                                              iFourier, RTMnlayer, RTMnlevelCloud,     &
                                              dimSV, dimSV_fc, nmutot, nGauss,         &
                                              geometryS, UDsumLocal_fc, UD_fc,         &
                                              wfInterfkscaGas_fc,                      &
                                              wfInterfkscaAerAbove_fc,                 &
                                              wfInterfkscaAerBelow_fc,                 &
                                              wfInterfkscaCldAbove_fc,                 &
                                              wfInterfkscaCldBelow_fc,                 &
                                              wfInterfkabs_fc,                         &
                                              wfAlbedo_fc)
        if (errorCheck(errS)) return

       ! Calculate Fourier coefficient of the reflectance by numerical integration
       ! over the source function.

        call CalcReflectance (errS, fcCoef, optPropRTMGridS, controlS, &
                              RTMnlayer, RTMnlevelCloud,               &
                              iFourier, dimSV, dimSV_fc, nmutot,       &
                              nGauss, geometryS, UD_fc,                &
                              contribrefl_fc, refl_fc)
        if (errorCheck(errS)) return

        ! calculate the Fourier coefficient of the surface emission
        call CalcDerivdRdEmission(errS, dimSV_fc, nGauss, iFourier, RTMnlayer, geometryS, UD_fc, wfEmission_fc)
        if (errorCheck(errS)) return


        ! clean up
        call deallocCoef(errS, fcCoef, maxExpCoef)
        if (errorCheck(errS)) return
        call deallocateReflTransIntField(errS, RTMnlayer, RT_fc, UDorde_fc, UDLocal_fc, UDsumLocal_fc)
        if (errorCheck(errS)) return
         
  end subroutine layerBasedOrdersScattering_fc


  subroutine allocCoef(errS, fcCoef, nmutot, dimSV_fc, maxExpCoef)

    ! claim memory for the generalized spherical function
    implicit none

    type(errorType), intent(inout)  :: errS
    type(fc_coefficients), pointer  :: fcCoef(:)
    integer, intent(in) :: nmutot, dimSV_fc, maxExpCoef

    ! local
    integer :: i
    integer :: allocStatus
    integer :: sumallocStatus

    allocStatus    = 0
    sumallocStatus = 0

    allocate( fcCoef(0:maxExpCoef), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    do i = 0, maxExpCoef
      allocate(fcCoef(i)%PlmPlus(dimSV_fc*nmutot), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate(fcCoef(i)%PlmMin (dimSV_fc*nmutot), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus
    end do

    ! initialize to zero
    do i = 0, maxExpCoef
      fcCoef(i)%PlmPlus = 0.0d0
      fcCoef(i)%PlmMin  = 0.0d0
    end do

    if (sumallocStatus /= 0) then 
      print *, 'allocCoef in LabosModule has nonzero sumallocStatus. allocation Failed'
      call mystop(errS, 'allocCoef in LabosModule has nonzero sumallocStatus. allocation Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine allocCoef


  subroutine deallocCoef(errS, fcCoef, maxExpCoef)

    ! free memory for support for the Fourier coefficients

    implicit none

      type(errorType), intent(inout) :: errS
    type(fc_coefficients), pointer  :: fcCoef(:)
    integer, intent(in) :: maxExpCoef

    integer             :: i

    integer             :: deallocStatus
    integer             :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    do i = 0, maxExpCoef
      deallocate(fcCoef(i)%PlmPlus, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus

      deallocate(fcCoef(i)%PlmMin, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end do

    deallocate(fcCoef, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if ( sumdeallocStatus /= 0 ) then
      call logDebug('FATAL ERROR: deallocation failed')
      call logDebug('in subroutine deallocCoef')
      call logDebug('in module LabosModule')
      call mystop(errS, 'stopped because deallocation failed')
      if (errorCheck(errS)) return
    end if

  end subroutine deallocCoef


  function Rsingle(dimSV_fc, nmutot, a, E, Zmin, DmuPlus)

    implicit none

    integer, intent(in) :: dimSV_fc, nmutot 
    real(8), intent(in) :: a
    real(8), intent(in) :: E(dimSV_fc*nmutot), DmuPlus(nmutot, nmutot)
    real(8), intent(in) :: Zmin(dimSV_fc*nmutot,dimSV_fc*nmutot)
    real(8)             :: Rsingle(dimSV_fc*nmutot,dimSV_fc*nmutot)

    ! local
    integer :: imu0, imu, iSV, jSV, ind, ind0
    real(8) :: EER(dimSV_fc * nmutot, dimSV_fc * nmutot)

    do imu0 = 1, dimSV_fc * nmutot
      do imu = 1 , dimSV_fc * nmutot
        EER(imu,imu0) = E(imu)*E(imu0)
      end do
    end do

    do imu0 = 1, nmutot
      do jSV = 1, dimSV_fc
        ind0 = jSV + (imu0 - 1) * dimSV_fc
        do imu = 1 , nmutot
          do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            Rsingle(ind,ind0) =  a * Zmin(ind,ind0) * (1d0 - EER(ind,ind0)) * DmuPlus(imu, imu0)
          end do
        end do
      end do
    end do

  end function Rsingle


  function Tsingle(dimSV_fc, nmutot, a, b, E, Zplus, Dmumin, geometryS)

    implicit none

    integer,            intent(in) :: dimSV_fc, nmutot
    real(8),            intent(in) :: a, b
    real(8),            intent(in) :: E(dimSV_fc*nmutot), Dmumin(nmutot,nmutot)
    real(8),            intent(in) :: Zplus(dimSV_fc*nmutot,dimSV_fc*nmutot)
    type(geometryType), intent(in) :: geometryS
    real(8)                        :: Tsingle(dimSV_fc*nmutot,dimSV_fc*nmutot)

    ! local
    integer    :: imu0, imu, iSV, jSV, ind, ind0
    real(8)    :: u, u0
    real(8)    :: EET(nmutot, nmutot)

    do imu0 = 1, nmutot
      ind0 = 1 + (imu0 - 1) * dimSV_fc
      u0 = geometryS%u(imu0)
      do imu = 1 , nmutot
        ind = 1 + (imu - 1) * dimSV_fc
        u = geometryS%u(imu)
        if ( abs(u - u0) < 1.0d-6) then
          EET(imu,imu0) = b * E(ind)
        else
          EET(imu,imu0) = E(ind) - E(ind0)
        end if
      end do
    end do

    do imu0 = 1, nmutot
      do jSV = 1, dimSV_fc
        ind0 = jSV + (imu0 - 1) * dimSV_fc
        do imu = 1 , nmutot
          do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            Tsingle(ind,ind0) =  a * Zplus(ind,ind0) * EET(imu,imu0) * Dmumin(imu,imu0)
          end do
        end do
      end do
    end do

  end function Tsingle


  subroutine allocateReflTransInternalField(errS, RTMnlayer, dimSV_fc, nmutot, nmuextra, RT_fc,  &
                                            UDorde_fc, UDLocal_fc, UDsumLocal_fc, UD_fc)

    ! allocate space for the refllection and transmission properties of the individual layers
    ! and for the internal field

      implicit none

      type(errorType), intent(inout) :: errS
      integer, intent(in)    :: RTMnlayer, dimSV_fc, nmutot, nmuextra
      type(RTType)           :: RT_fc(0:RTMnlayer)        ! refl transm individual layers
      type(UDType)           :: UDorde_fc(0:RTMnlayer)
      type(UDLocalType)      :: UDLocal_fc(0:RTMnlayer)
      type(UDLocalType)      :: UDsumLocal_fc(0:RTMnlayer)
      type(UDType), optional :: UD_fc(0:RTMnlayer)

      ! local
      integer :: ilayer

      do ilayer = 0, RTMnlayer
        allocate (RT_fc(ilayer)%R(dimSV_fc*nmutot,dimSV_fc*nmutot) )
        allocate (RT_fc(ilayer)%T(dimSV_fc*nmutot,dimSV_fc*nmutot) )
        allocate (RT_fc(ilayer)%Rst(dimSV_fc*nmutot,dimSV_fc*nmutot) )
        allocate (RT_fc(ilayer)%Tst(dimSV_fc*nmutot,dimSV_fc*nmutot) )

        allocate (UDorde_fc(ilayer)%E(dimSV_fc*nmutot) )
        allocate (UDorde_fc(ilayer)%D(dimSV_fc*nmutot,nmuextra) )
        allocate (UDorde_fc(ilayer)%U(dimSV_fc*nmutot,nmuextra) )

        allocate (UDLocal_fc(ilayer)%U(dimSV_fc*nmutot,nmuextra) )
        allocate (UDLocal_fc(ilayer)%D(dimSV_fc*nmutot,nmuextra) )

        allocate (UDsumLocal_fc(ilayer)%U(dimSV_fc*nmutot,nmuextra) )
        allocate (UDsumLocal_fc(ilayer)%D(dimSV_fc*nmutot,nmuextra) )
      end do

      if ( present(UD_fc) ) then
        do ilayer = 0, RTMnlayer
          allocate (UD_fc(ilayer)%E(dimSV_fc*nmutot) )
          allocate (UD_fc(ilayer)%D(dimSV_fc*nmutot,nmuextra) )
          allocate (UD_fc(ilayer)%U(dimSV_fc*nmutot,nmuextra) )
        end do
      end if ! present(UD_fc)

    end subroutine allocateReflTransInternalField


    subroutine deallocateReflTransIntField(errS, RTMnlayer, RT_fc, UDorde_fc, UDLocal_fc, UDsumLocal_fc, UD_fc)

      ! deallocate space for the individual layers and for the partial atmospheres

      implicit none

      type(errorType), intent(inout) :: errS
      integer, intent(in)    :: RTMnlayer
      type(RTType)           :: RT_fc(0:RTMnlayer)     ! refl transm individual layers
      type(UDType)           :: UDorde_fc(0:RTMnlayer)
      type(UDLocalType)      :: UDLocal_fc(0:RTMnlayer)
      type(UDLocalType)      :: UDsumLocal_fc(0:RTMnlayer)
      type(UDType), optional :: UD_fc(0:RTMnlayer)

      integer  :: ilayer

      do ilayer = 0, RTMnlayer
        deallocate (RT_fc(ilayer)%R)
        deallocate (RT_fc(ilayer)%T)
        deallocate (RT_fc(ilayer)%Rst)
        deallocate (RT_fc(ilayer)%Tst)

        deallocate (UDorde_fc(ilayer)%E )
        deallocate (UDorde_fc(ilayer)%D )
        deallocate (UDorde_fc(ilayer)%U )

        deallocate (UDLocal_fc(ilayer)%U )
        deallocate (UDLocal_fc(ilayer)%D )

        deallocate (UDsumLocal_fc(ilayer)%U )
        deallocate (UDsumLocal_fc(ilayer)%D )
      end do

      if ( present(UD_fc) ) then
        do ilayer = 0, RTMnlayer
          deallocate (UD_fc(ilayer)%E )
          deallocate (UD_fc(ilayer)%D )
          deallocate (UD_fc(ilayer)%U )
        end do
      end if ! present(UD_fc)

    end subroutine deallocateReflTransIntField


    subroutine fillAttenuation(errS, optPropRTMGridS, controlS, geometryS, nmutot, RTMnlayer, atten)

      ! The array atten(imu, levelFrom, levelTo) is filled with the attenuation for light
      ! traveling directly from levelFrom to levelTo and arrives there with the direction cosine
      ! geometry%u(imu). For a spherical shell atmsphere
      !    atten(imu, levelFrom, levelTo) is NOT equal to atten(imu, levelTo, levelFrom)

      implicit none

      type(errorType), intent(inout) :: errS
      type(optPropRTMGridType), intent(in)  :: optPropRTMGridS
      type(controlType),        intent(in)  :: controlS
      type(geometryType),       intent(in)  :: geometryS
      integer,                  intent(in)  :: nmutot, RTMnlayer
      real(8),                  intent(out) :: atten(nmutot, 0:RTMnlayer, 0:RTMnlayer)

      ! local
      real(8), parameter :: REarth = 6371.0d0  ! mean radius of the Earth in km

      real(8)    :: attenLay(nmutot, RTMnlayer)
      real(8)    :: u(nmutot)
      real(8)    :: sumkext, sin2theta, sqrx_sin2theta
      real(8)    :: x(0:optPropRTMGridS%RTMnlayerSub)
      real(8)    :: x2(0:optPropRTMGridS%RTMnlayerSub)
      real(8)    :: numerator(0:optPropRTMGridS%RTMnlayerSub)
      integer    :: ilFrom, ilTo, ilayer, imu, index

      logical, parameter :: verbose = .false.

      u = geometryS%u

      ! initialize
      atten         = 1.0d0

      ! first fill values for the slant attenuation of the layers in a plane parallel atmosphere
      do ilayer = 1, RTMnlayer
        attenLay(:, ilayer) = exp(- optPropRTMGridS%opticalThicknLay(ilayer) / u(:) )
      end do

      ! fill slant optical distances between the layers for a plane-parallel atmosphere
      do ilTo = 1, RTMnlayer
        do ilFrom = ilTo, 1, -1
          atten(:,ilFrom-1,ilTo)  = atten(:,ilFrom,ilTo) * attenLay(:, ilFrom)
        end do
      end do
      do ilTo = 0, RTMnlayer
        do ilFrom = ilTo, RTMnlayer
          atten(:,ilFrom, ilTo) = atten(:,ilTo, ilFrom)
        end do
      end do

      if ( controlS%useSphericalCorr ) then

        ! modify the attenuation from the top of the atmosphere to the top
        ! of the layers

        x = optPropRTMGridS%RTMaltitudeSub + REarth
        x2 = x**2
        numerator = optPropRTMGridS%RTMweightSub * x &
                  * ( optPropRTMGridS%kextSubGas + optPropRTMGridS%kextSubAer + optPropRTMGridS%kextSubCld )

        ! slant optical distances for the levels with standard geometry at level ilTo
        do imu = 1, nmutot
          sin2theta = 1.0d0 - u(imu)**2
          do ilTo = RTMnlayer - 1, 0, -1
            sqrx_sin2theta = sin2theta * (REarth + optPropRTMGridS%RTMaltitude(ilTo))**2
            sumkext  = 0.0d0
            do index = ilTo*(optPropRTMGridS%nGaussLay + 1), optPropRTMGridS%RTMnlayerSub
              sumkext = sumkext + numerator(index) / sqrt( abs(x2(index) - sqrx_sin2theta) )
            end do
            atten(imu, RTMnlayer, ilTo) = exp(-sumkext)
          end do ! ilTo loop
        end do  ! imu loop

      end if ! usePseudoSpherical


      if (verbose) then
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*) 'slant attenuation'
        do imu = 1, nmutot
          write(intermediateFileUnit,*)
          write(intermediateFileUnit,*) ' imu = ', imu
          write(intermediateFileUnit,'(A,1000I11)') 'From/To = ', (ilTo, ilTo = 0, RTMnlayer)
          do ilFrom = 0, RTMnlayer
             write(intermediateFileUnit,'(I10,1000E11.4)')  ilFrom, (atten(imu, ilFrom, ilTo), ilTo = 0, RTMnlayer)
          end do
        end do

      end if ! verbose

    end subroutine fillAttenuation


    subroutine fillPlmVector(errS, fcCoef, iFourier, dimSV_fc, nmutot, maxExpCoef, geometryS)

      ! This subroutine fills the structure fcCoef declared in the declaration part of
      ! this module (LabosModule). The coefficients differ depending on the Fourier index (iFourier),
      ! the dimension of the Stokes vector for this Fourier index (dimSV_fc), and the polar angles
      ! involved (stored in geometryS).
      !
      ! To be more specific the values of the generalized spherical functions
      ! P_l_m0, P_l_m-2 and P_l_m2 are calculated (if dimSV_fc =1 only P_l_m0 is calculated).
      ! Here P_l_m0 is the associated Legendre function (with a normalization that differs
      ! from the normal normalization). P_l_m-2 and P_l_m2 are a generalization of the associated
      ! Legendre functions and account for the rotation of the reference plane from the scattering
      ! plane to the local meridional planes. The functions are used in the Fourier series expansion
      ! of the phase matrix and pertain to the Stokes parameters Q and U.
      !
      ! The Fourier coefficients of the phase matrix are calculated as super matrices. Therfore,
      ! P_l_m0, P_l_m-2 and P_l_m2 are multiplied with special weights (sqrt(2*w1*ui) if ui is a
      ! Gaussian division point or 1 if ui is not a Gaussian division point). These weights are introduced
      ! so that integrations over polar angles can be replaced by matrix multiplications.
      !
      ! Results for positive and negative values of u are stored in different arrays:
      ! PlmPlus and PlmMin.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,               intent(in)    :: iFourier , dimSV_fc, nmutot, maxExpCoef
      type(geometryType),    intent(in)    :: geometryS
      type(fc_coefficients), intent(inout) :: fcCoef(0:maxExpCoef)

      ! local
      integer :: l,m, imu, iSV, ind, indsm    ! indices for the recurrence relations
      real(8) :: fact

      ! local precalculated values
      real(8) :: u(nmutot)           ! u direction cosines
      real(8) :: oneMinusuu(nmutot)  ! (1.0-u**2)
      real(8) :: squu(nmutot)        ! sqrt[(1.0-u**2)]
      real(8) :: sql4(0:maxExpCoef)  ! sqrt(l**2 -4)      for l>2  and 0 otherwise
      real(8) :: sqlm(0:maxExpCoef)  ! sqrt(l**2 -m**2))  for l>m  and 0 otherwise

      ! initial values ( index l == m == iFourier)
      real(8) :: pmm_00(nmutot)      ! P_l_m_n with l = m and n = 0
      real(8) :: pmm_p2(nmutot)      ! P_l_m_n with l = m and n = +2
      real(8) :: pmm_m2(nmutot)      ! P_l_m_n with l = m and n = -2

      ! values of the generalized spherical functions used in the recurrence relations
      ! _lp1_  means the value for l+1, _l_  means the value for l, _lm1_  means the value for l-1
      real(8) :: p_lp1_m_00(nmutot)      ! P_l_m_n with l = l+1 and n = 0
      real(8) :: p_l___m_00(nmutot)      ! P_l_m_n with l = l   and n = 0
      real(8) :: p_lm1_m_00(nmutot)      ! P_l_m_n with l = l-1 and n = 0

      real(8) :: p_lp1_m_p2(nmutot)      ! P_l_m_n with l = l+1 and n = +2
      real(8) :: p_l___m_p2(nmutot)      ! P_l_m_n with l = l   and n = +2
      real(8) :: p_lm1_m_p2(nmutot)      ! P_l_m_n with l = l-1 and n = +2

      real(8) :: p_lp1_m_m2(nmutot)      ! P_l_m_n with l = l+1 and n = -2
      real(8) :: p_l___m_m2(nmutot)      ! P_l_m_n with l = l   and n = -2
      real(8) :: p_lm1_m_m2(nmutot)      ! P_l_m_n with l = l-1 and n = -2

      real(8) :: smFormPlus(dimSV_fc * nmutot)
      real(8) :: smFormMin(dimSV_fc * nmutot)

      ! coeffcients in the recurrence relations
      ! the general shape is a * P_lp1 = b * P_l + c * P_lm1
      ! we distinguish a00, ap2, am2, for n = 0, +2, -2, respectively
      ! and use a similar notation for b and c
      ! the coefficients are arrays with dimension nmutot, but only b depends on u
      real(8) :: a, b(nmutot), c

      ! control ouput
      logical, parameter :: verbose = .false.  ! if .true. write to intermediate file

      ! initialize ( dimensions of PlmPlus and PlmMin is dimSV_fc * nmutot )
      do l = 0, maxExpCoef
        fcCoef(l)%PlmPlus = 0.0d0
        fcCoef(l)%PlmMin  = 0.0d0
      end do

      ! pre-calculated values as function of u
      u            = geometryS%u
      oneMinusuu   = (1.0d0 - u**2)
      squu         = sqrt(oneMinusuu)

      ! pre-calculated values as function of the index for the recurrence relations
      sql4 = 0.0d0
      do l = 3, maxExpCoef
        sql4(l) = sqrt(dble(l**2 - 4.0d0))
      end do
      sqlm = 0.0d0
      do l = iFourier + 1, maxExpCoef
        sqlm(l) = sqrt( dble(l**2 - iFourier**2) )
      end do

      ! P00 Lgendre functions for (1,1) and (4,4) element

      select case(iFourier)

        case(0)  ! l = m = 0

          ! starting values for recursion (values for l = 0)
          p_lm1_m_00 =  0.0d0
          p_l___m_00 =  1.0d0

        case(1)

          ! starting values for recursion (values for l = 1)
          p_lm1_m_00 =  0.0d0
          p_l___m_00 =  squu / sqrt(2.0d0)

        case(2)

          ! starting values for recursion (values for l = 2)
          p_lm1_m_00 =  0.0d0
          p_l___m_00 =  0.25d0 * sqrt(6.0d0) * oneMinusuu

        case default

          ! iFourier > 2
          ! pmm_00: calculate the square of pmm_00 and take the square root
          ! use f(m) = (1-u**2)**m * 4**(-m) * (2m)! / (m! * m!) as starting value
          ! and f(m) = f(m-1) * (1-u**2) * (m-0.5) / m  as recurrence relation
          ! and use sqrt(f(m)) to calculate the final coefficient
          pmm_00 =   0.375d0 * oneMinusuu * oneMinusuu
          do m = 3, iFourier
            pmm_00 = pmm_00 * oneMinusuu * (m - 0.5d0 ) / dble(m)
          end do
          pmm_00 = sqrt(pmm_00)

          ! starting values for recursion for l (values for l = iFourier)
          p_lm1_m_00 =  0.0d0
          p_l___m_00 =  pmm_00

      end select

      ! store starting value they are the same for PlmPlus and PlmMin
      fcCoef(iFourier)%PlmPlus(1:nmutot) = p_l___m_00
      fcCoef(iFourier)%PlmMin (1:nmutot) = p_l___m_00

      ! apply recurrence relations for u
      do l = iFourier, maxExpCoef - 1

        ! use the generic expression
        ! a * P_lp1 = b * P_l + c * P_lm1

        a =   sqlm(l+1)
        b =  (2.0d0 * l + 1.0d0) * u
        c = - sqlm(l)
        p_lp1_m_00 = ( b * p_l___m_00 + c * p_lm1_m_00 ) / a

        fcCoef(l+1)%PlmPlus(1:nmutot) = p_lp1_m_00

        p_lm1_m_00 = p_l___m_00
        p_l___m_00 = p_lp1_m_00

      end do ! iFourier

      ! initialize the starting values for -u
      p_lm1_m_00 = 0.0d0
      p_l___m_00 = fcCoef(iFourier)%PlmMin (1:nmutot)

      ! apply recurrence relations for -u
      do l = iFourier, maxExpCoef - 1

        ! use the generic expression
        ! a * P_lp1 = b * P_l + c * P_lm1

        a =   sqlm(l+1)
        b = -(2.0d0 * l + 1.0d0) * u
        c = - sqlm(l)
        p_lp1_m_00 = ( b * p_l___m_00 + c * p_lm1_m_00 ) / a

        fcCoef(l+1)%PlmMin(1:nmutot) = p_lp1_m_00

        p_lm1_m_00 = p_l___m_00
        p_l___m_00 = p_lp1_m_00

      end do ! iFourier

      if ( dimSV_fc == 1 ) then

        if (verbose ) then
          write(intermediateFileUnit, *) 'PlmPlus and PlmMin from fillPlmVector'
          do l = iFourier, maxExpCoef
            write(intermediateFileUnit,'(2(A20, I4))') ' iFourier = ',iFourier, 'iCoef = ', l
            write(intermediateFileUnit,*) 'mu     PlmPlus     PlmMin'
            do imu = 1, nmutot
              write(intermediateFileUnit,'(3F12.8)') u(imu), fcCoef(l)%PlmPlus(imu), fcCoef(l)%PlmMin(imu)
            end do
          end do
        end if ! verbose
      
        ! multiply with Gaussian weights
        do l = iFourier, maxExpCoef
          do imu = 1, nmutot
            fcCoef(l)%PlmPlus(imu) = fcCoef(l)%PlmPlus(imu) *  geometryS%w(imu)
            fcCoef(l)%PlmMin (imu) = fcCoef(l)%PlmMin (imu) *  geometryS%w(imu)
          end do ! imu
        end do ! l

        return

      end if

      if ( dimSV_fc == 4 ) then
        do l = iFourier, maxExpCoef
          fcCoef(l)%PlmPlus(3 * nmutot + 1: 4 * nmutot) = fcCoef(l)%PlmPlus(1:nmutot)
          fcCoef(l)%PlmMin (3 * nmutot + 1: 4 * nmutot) = fcCoef(l)%PlmMin (1:nmutot)
        end do
      end if

      ! consider Pm,2 and Pm,-2 first for +u

      select case(iFourier)

        case(0)

          ! starting values for recursion (values for l = 2)
          p_lm1_m_p2 =  0.0d0
          p_l___m_p2 = -0.25d0 * sqrt(6.0d0) * oneMinusuu
          p_lm1_m_m2 =  0.0d0
          p_l___m_m2 = -0.25d0 * sqrt(6.0d0) * oneMinusuu

          ! store starting values
          fcCoef(2)%PlmPlus(  nmutot+1 : 2*nmutot) = p_l___m_m2
          fcCoef(2)%PlmPlus(2*nmutot+1 : 3*nmutot) = p_l___m_p2

        case(1)

          ! starting values for recursion (values for l = 2)
          p_lm1_m_p2 =  0.0d0
          p_l___m_p2 =  0.5d0 * squu * (1.0d0 + u)
          p_lm1_m_m2 =  0.0d0
          p_l___m_m2 = -0.5d0 * squu * (1.0d0 - u)

          ! store starting values
          fcCoef(2)%PlmPlus(  nmutot+1 : 2*nmutot) = p_l___m_m2
          fcCoef(2)%PlmPlus(2*nmutot+1 : 3*nmutot) = p_l___m_p2

        case(2)

          ! starting values for recursion (values for l = 2)
          p_lm1_m_p2 =  0.0d0
          p_l___m_p2 = -0.25d0 * (1.0d0 + u)**2
          p_lm1_m_m2 =  0.0d0
          p_l___m_m2 = -0.25d0 * (1.0d0 - u)**2

          ! store starting values
          fcCoef(2)%PlmPlus(  nmutot+1 : 2*nmutot) = p_l___m_m2
          fcCoef(2)%PlmPlus(2*nmutot+1 : 3*nmutot) = p_l___m_p2

        case default

          ! iFourier > 2

          ! pmm_p2 and pmm_m2: first calculate the square of pmm_p2 and then take minus the square root
          ! start with f(m) = - (1-u)**(m-2) * (1+u)**(m+2) * 4**(-m) * (2m)! / [(m-2)! * (m+2)!]
          ! use recurrence relation f(m) = f(m-1) * (1-u**2) * m * (m-0.5) / (m-2) / (m+2)
          ! and finally use - sqrt(f(m)) for the coefficient
          pmm_p2 = (1 + u)**4 / 16.0d0      ! square of pmm_p2 for m = 2
          pmm_m2 = (1 - u)**4 / 16.0d0      ! square of pmm_m2 for m = 2
          do m = 3, iFourier
            fact = m * (m - 0.5d0 ) / dble(m-2) / dble(m+2)
            pmm_p2 = pmm_p2 * oneMinusuu * fact
            pmm_m2 = pmm_m2 * oneMinusuu * fact
          end do
          pmm_p2 = - sqrt(pmm_p2)
          pmm_m2 = - sqrt(pmm_m2)

          ! starting values for recursion (values for l = iFourier)
          p_lm1_m_p2 =  0.0d0
          p_l___m_p2 =  pmm_p2

          p_lm1_m_m2 =  0.0d0
          p_l___m_m2 =  pmm_m2

          ! store starting values
          fcCoef(iFourier)%PlmPlus(  nmutot+1 : 2*nmutot) = p_l___m_m2
          fcCoef(iFourier)%PlmPlus(2*nmutot+1 : 3*nmutot) = p_l___m_p2

      end select

      ! apply recurrence relations for +u
      do l = iFourier, maxExpCoef - 1

        if ( l < 2 ) cycle  ! go to next value of l

        ! use the generic expression
        ! a * P_lp1 = b * P_l + c * P_lm1

        a =   dble(l) * sql4(l+1) * sqlm(l+1)
        c = - (l+1.0d0) * sql4(l) * sqlm(l)

        b =  (2.0d0 * l + 1.0d0) * ( dble(l) *(l+1.0d0) * u + 2.0d0 * iFourier )
        p_lp1_m_m2 = ( b * p_l___m_m2 + c * p_lm1_m_m2 ) / a

        b =  (2.0d0 * l + 1.0d0) * ( dble(l) *(l+1.0d0) * u - 2.0d0 * iFourier )
        p_lp1_m_p2 = ( b * p_l___m_p2 + c * p_lm1_m_p2 ) / a

        fcCoef(l+1)%PlmPlus(  nmutot+1:2*nmutot) = p_lp1_m_m2
        fcCoef(l+1)%PlmPlus(2*nmutot+1:3*nmutot) = p_lp1_m_p2

        p_lm1_m_m2 = p_l___m_m2
        p_l___m_m2 = p_lp1_m_m2
        p_lm1_m_p2 = p_l___m_p2
        p_l___m_p2 = p_lp1_m_p2

      end do ! iFourier


      ! consider Pm,2 and Pm,-2 now for -u
      select case(iFourier)

        case(0)  ! l = m = 0

          ! starting values for recursion (values for l = 2)
          p_lm1_m_p2 =  0.0d0
          p_l___m_p2 = -0.25d0 * sqrt(6.0d0) * oneMinusuu
          p_lm1_m_m2 =  0.0d0
          p_l___m_m2 = -0.25d0 * sqrt(6.0d0) * oneMinusuu

          ! store starting values
          fcCoef(2)%PlmMin(  nmutot+1 : 2*nmutot) = p_l___m_m2
          fcCoef(2)%PlmMin(2*nmutot+1 : 3*nmutot) = p_l___m_p2

        case(1)

          ! starting values for recursion (values for l = 2)
          p_lm1_m_p2 =  0.0d0
          p_l___m_p2 =  0.5d0 * squu * (1.0d0 - u)
          p_lm1_m_m2 =  0.0d0
          p_l___m_m2 = -0.5d0 * squu * (1.0d0 + u)

          ! store starting values
          fcCoef(2)%PlmMin(  nmutot+1 : 2*nmutot) = p_l___m_m2
          fcCoef(2)%PlmMin(2*nmutot+1 : 3*nmutot) = p_l___m_p2

        case(2)

          ! starting values for recursion (values for l = 2)
          p_lm1_m_p2 =  0.0d0
          p_l___m_p2 = -0.25d0 * (1.0d0 - u)**2
          p_lm1_m_m2 =  0.0d0
          p_l___m_m2 = -0.25d0 * (1.0d0 + u)**2

          ! store starting values
          fcCoef(2)%PlmMin(  nmutot+1 : 2*nmutot) = p_l___m_m2
          fcCoef(2)%PlmMin(2*nmutot+1 : 3*nmutot) = p_l___m_p2

        case default

          ! iFourier > 2

          ! pmm_p2 and pmm_m2: first calculate the square of pmm_p2 and then take minus the square root
          ! start with f(m) = - (1+u)**(m-2) * (1-u)**(m+2) * 4**(-m) * (2m)! / [(m-2)! * (m+2)!]
          ! use recurrence relation f(m) = f(m-1) * (1-u**2) * m * (m-0.5) / (m-2) / (m+2)
          ! and finally use - sqrt(f(m)) for the coefficient
          pmm_p2 = (1.0d0 - u)**4 / 16.0d0      ! square of pmm_p2 for m = 2
          pmm_m2 = (1.0d0 + u)**4 / 16.0d0      ! square of pmm_m2 for m = 2
          do m = 3, iFourier
            fact = dble(m) * (m - 0.5d0 ) / dble(m-2) / dble(m+2)
            pmm_p2 = pmm_p2 * oneMinusuu * fact
            pmm_m2 = pmm_m2 * oneMinusuu * fact
          end do
          pmm_p2 = - sqrt(pmm_p2)
          pmm_m2 = - sqrt(pmm_m2)

          ! starting values for recursion (values for l = iFourier)
          p_lm1_m_p2 =  0.0d0
          p_l___m_p2 =  pmm_p2

          p_lm1_m_m2 =  0.0d0
          p_l___m_m2 =  pmm_m2

          ! store starting values
          fcCoef(iFourier)%PlmMin(  nmutot+1 : 2*nmutot) = p_l___m_m2
          fcCoef(iFourier)%PlmMin(2*nmutot+1 : 3*nmutot) = p_l___m_p2

      end select

      ! apply recurrence relations for -u
      do l = iFourier, maxExpCoef - 1

        if ( l < 2 ) cycle  ! go to next value of l

        ! use the generic expression
        ! a * P_lp1 = b * P_l + c * P_lm1

        a =   dble(l) * sql4(l+1) * sqlm(l+1)
        c = - (l+1.0d0) * sql4(l) * sqlm(l)

        b =  (2.0d0 * l + 1.0d0) * ( -dble(l) *(l+1.0d0) * u + 2.0d0 * iFourier )
        p_lp1_m_m2 = ( b * p_l___m_m2 + c * p_lm1_m_m2 ) / a

        b =  (2.0d0 * l + 1.0d0) * ( -dble(l) *(l+1.0d0) * u - 2.0d0 * iFourier )
        p_lp1_m_p2 = ( b * p_l___m_p2 + c * p_lm1_m_p2 ) / a

        fcCoef(l+1)%PlmMin(  nmutot+1:2*nmutot) = p_lp1_m_m2
        fcCoef(l+1)%PlmMin(2*nmutot+1:3*nmutot) = p_lp1_m_p2

        p_lm1_m_m2 = p_l___m_m2
        p_l___m_m2 = p_lp1_m_m2
        p_lm1_m_p2 = p_l___m_p2
        p_l___m_p2 = p_lp1_m_p2

      end do ! iFourier

      ! re-organize the values to make them suited for the supermatrices
      ! we now have (sv1-g1, sv1-g2, ..., sv1-gn, sv1-mu, sv1-mu0, sv2-g1, sv2-g2, ..., sv2-gn,sv2-mu,sv2-mu0, ....
      ! where sv1 stands for I, sv2 for Q, sv3 for U, and sv4 for V.
      !       g1 .. gn are the Gaussian division point for mu, mu is the viewing cosine, mu the solar cosine.
      ! Howver, we need 
      ! (g1-sv1, g1-sv2, g1-sv3, g1-sv4, g2-sv1, ....., gn-sv4, mu-sv1, ..., mu0-sv4)
      ! Further we have to multiply them with Gaussian weights
      do l = iFourier, maxExpCoef
        do imu = 1, nmutot
          do iSV = 1, dimSV_fc
            ind   = imu + (iSV-1) * nmutot
            indsm = iSV + (imu-1) * dimSV_fc
            smFormPlus(indsm) = fcCoef(l)%PlmPlus(ind)
            smFormMin (indsm) = fcCoef(l)%PlmMin (ind)
          end do ! iSV
        end do ! nmutot
        fcCoef(l)%PlmPlus = smFormPlus
        fcCoef(l)%PlmMin  = smFormMin
      end do ! 

      if (verbose) then
        write(intermediateFileUnit, *) ''
        write(intermediateFileUnit, *) 'Generalized spherical functions Plmn for m = ', iFourier
        write(intermediateFileUnit, *) ' ordered as  n = 0     n = -2     n = +2  '
        write(intermediateFileUnit, '(10X, 200F15.10)') ((u(imu), iSV = 1, dimSV_fc), imu = 1, nmutot)
        do l = 0, maxExpCoef
              write(intermediateFileUnit, '(I10, 200E15.6)') l, fcCoef(l)%PlmPlus(:)
        end do ! loop over l
        write(intermediateFileUnit, '(10X, 200F15.10)') ((-u(imu), iSV = 1, dimSV_fc), imu = 1, nmutot)
        do l = 0, maxExpCoef
              write(intermediateFileUnit, '(I10, 200E15.6)') l, fcCoef(l)%PlmMin(:)
        end do ! loop over l
      end if ! verbose     

      ! multiply with Gaussian weights
      do l = iFourier, maxExpCoef
        do imu = 1, nmutot
          do iSV = 1, dimSV_fc
            indsm = iSV + (imu-1) * dimSV_fc
            fcCoef(l)%PlmPlus(indsm) = fcCoef(l)%PlmPlus(indsm) * geometryS%w(imu)
            fcCoef(l)%PlmMin (indsm) = fcCoef(l)%PlmMin (indsm) * geometryS%w(imu)
          end do ! iSV
        end do ! nmutot
      end do ! 

    end subroutine fillPlmVector


    subroutine fillZplusZmin(errS, fcCoef, iFourier, maxExpCoef, dimSV, dimSV_fc, nmutot, phasefCoef,  &
                             geometryS, Zplus, Zmin, string)

      ! fill the Fourier coefficients of the phase function or phase matrix
      ! using Zplus = D1 * sum[ (D2 * Plm(+u) * D1) * (D2* Sl * D2) * (D1 * Plm(+u) * D2) ] * D1 and
      !       Zmin  = D1 * sum[ (D2 * Plm(-u) * D1) * (D2* Sl * D2) * (D1 * Plm(+u) * D2) ] * D1

      ! the sum starts at the Fourier index iFourier and runs to maxExpCoef
      ! Assuming that there are 4 Stokes vectors ( dimSV_fc = 4) we have

      !       |  1    0    0   0  |              |  1   0     0   0  |
      !       |  0    1    1   0  |              |  0  1/2   1/2  0  |
      ! D1 =  |  0    1   -1   0  |       D2 =   |  0  1/2  -1/2  0  |
      !       |  0    0    0   1  |              |  0   0     0   1  |

      ! D1 * D2 = 1  and D2 * D1 = 1 where 1 is the unit matrix

      !                       | Plm_0(u)    0         0       0     |
      !                       |    0     Plm_-2(u)    0       0     |
      ! (D2 * Plm(u) * D1) =  |    0        0      Plm_+2(u)  0     |  = (D1 * Plm(u) * D2)
      !                       |    0        0         0    Plm_0(u) |

      ! and the expansion coefficients (D2 * Plm(u) * D2) are given by (index l omitted)

      !                  | alpha1       beta1/2             beta1/2           0     |
      !                  | beta1/2  (alpha2+alpha3)/4  (alpha2-alpha3)/4   beta2/2  |
      ! (D2* Sl * D2) =  | beta1/2  (alpha2-alpha3)/4  (alpha2+alpha3)/4  -beta2/2  |
      !                  |    0        -beta2/2             beta2/2        alpha4   |

      ! note that the factor (-1)**iFourier is omitted because the imaginary parts have been ignored
      ! when calculating the generalized spherical functions Plm_0(u), Plm_-2(u), and Plm_+2(u).

      ! The values of the expansion coefficients are stored in phasefCoef
      ! as a (dimSV, dimSV) sub matrix and for dimSV = 4 we have (index l omitted)

      !      | alpha1  beta1    0       0    |
      !      | beta1  alpha2    0       0    |
      ! S =  |    0      0    alpha3  beta2  |
      !      |    0      0   -beta2   alpha4 |

      ! For dimSV_fc = 1 (no polarization) this reduces to
      !  Zplus = sum[ Plm_0(+u) * alpha1 * Plm_0(+u)]
      !  Zplus = sum[ Plm_0(-u) * alpha1 * Plm_0(+u)]


      implicit none

      ! the values of D2 * Plm(u) * D1 are stored in the one dimensional arrays
      !    fcCoef(l)%PlmPlus(dimSV_fc*nmutot)  and
      !    fcCoef(l)%PlmPlus(dimSV_fc*nmutot)
      ! where l = 0, 1, ...maxExpCoef

      type(errorType), intent(inout) :: errS
      integer,               intent(in)  :: iFourier, maxExpCoef, dimSV, dimSV_fc, nmutot
      real(8),               intent(in)  :: phasefCoef(dimSV, dimSV, 0:maxExpCoef)
      type(fc_coefficients), intent(in)  :: fcCoef(0:maxExpCoef)
      type(geometryType),    intent(in)  :: geometryS
      real(8),               intent(out) :: Zplus(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8),               intent(out) :: Zmin (dimSV_fc*nmutot,dimSV_fc*nmutot)
      character(LEN= 50), optional, intent(in)  :: string

      ! local
      integer :: iCoef, imu, imu0, iSV, jSV, ind, ind0
      real(8) :: D1(dimSV_fc, dimSV_fc)
      real(8) :: D2_S_D2(dimSV_fc, dimSV_fc)
      real(8) :: weight

      logical, parameter :: verbose = .false.


      ! initialize
      Zplus = 0.0d0
      Zmin  = 0.0d0

      select case (dimSV_fc)

       case(1)

          do iCoef = iFourier, maxExpCoef
           do imu0 = 1, nmutot
             do imu  = 1, nmutot
               Zplus(imu,imu0) = Zplus(imu,imu0) + phasefCoef( 1, 1, iCoef)  &
                                                 * fcCoef(iCoef)%PlmPlus(imu) * fcCoef(iCoef)%PlmPlus(imu0)
               Zmin (imu,imu0) = Zmin(imu,imu0)  + phasefCoef( 1, 1, iCoef)  &
                                                 * fcCoef(iCoef)%PlmMin(imu)  * fcCoef(iCoef)%PlmPlus(imu0)
             end do ! imu
           end do ! imuo
         end do ! iCoef

       case(3)

         do iCoef = iFourier, maxExpCoef
           D2_S_D2( 1, 1) =   phasefCoef( 1, 1, iCoef)
           D2_S_D2( 2, 1) =   0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 3, 1) =   0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 1, 2) =   0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 1, 3) =   0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 2, 2) =   0.25d0 * ( phasefCoef( 2, 2, iCoef) + phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 3, 3) =   0.25d0 * ( phasefCoef( 2, 2, iCoef) + phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 3, 2) =   0.25d0 * ( phasefCoef( 2, 2, iCoef) - phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 2, 3) =   0.25d0 * ( phasefCoef( 2, 2, iCoef) - phasefCoef( 3, 3, iCoef) )
           do imu0 = 1, nmutot
             do jSV = 1, dimSV_fc
               ind0 = jSV + (imu0 - 1) * dimSV_fc
               do imu  = 1, nmutot
                 do iSV = 1, dimSV_fc
                   ind = iSV + (imu - 1) * dimSV_fc
                   Zplus(ind,ind0) = Zplus(ind,ind0) + D2_S_D2( iSV, jSV)  &
                                           * fcCoef(iCoef)%PlmPlus(ind) * fcCoef(iCoef)%PlmPlus(ind0)
                   Zmin (ind,ind0) = Zmin(ind,ind0)  + D2_S_D2( iSV, jSV)  &
                                           * fcCoef(iCoef)%PlmMin(ind)  * fcCoef(iCoef)%PlmPlus(ind0)
                end do ! iSV
              end do ! imu
             end do ! jSV
           end do ! imu0
         end do ! iCoef         

       case(4)

         do iCoef = iFourier, maxExpCoef
           D2_S_D2( 1, 1) =  phasefCoef( 1, 1, iCoef)
           D2_S_D2( 2, 1) =  0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 3, 1) =  0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 4, 1) =  0.0d0
           D2_S_D2( 1, 2) =  0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 1, 3) =  0.50d0 * phasefCoef( 2, 1, iCoef)
           D2_S_D2( 1, 4) =  0.0d0
           D2_S_D2( 2, 2) =  0.25d0 * ( phasefCoef( 2, 2, iCoef) + phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 3, 3) =  0.25d0 * ( phasefCoef( 2, 2, iCoef) + phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 3, 2) =  0.25d0 * ( phasefCoef( 2, 2, iCoef) - phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 2, 3) =  0.25d0 * ( phasefCoef( 2, 2, iCoef) - phasefCoef( 3, 3, iCoef) )
           D2_S_D2( 4, 1) =  0.0d0
           D2_S_D2( 4, 2) = -0.50d0 * phasefCoef( 3, 4, iCoef) ! phasefCoef( 3, 4, iCoef) = beta2
           D2_S_D2( 4, 3) =  0.50d0 * phasefCoef( 3, 4, iCoef)
           D2_S_D2( 2, 4) =  0.50d0 * phasefCoef( 3, 4, iCoef)
           D2_S_D2( 3, 4) = -0.50d0 * phasefCoef( 3, 4, iCoef)
           D2_S_D2( 4, 4) = phasefCoef( 4, 4, iCoef)
           do imu0 = 1, nmutot
             do jSV = 1, dimSV_fc
               ind0 = jSV + (imu0 - 1) * dimSV_fc
               do imu  = 1, nmutot
                 do iSV = 1, dimSV_fc
                   ind = iSV + (imu - 1) * dimSV_fc
                   Zplus(ind,ind0) = Zplus(ind,ind0) + D2_S_D2( iSV, jSV)  &
                                   * fcCoef(iCoef)%PlmPlus(ind) * fcCoef(iCoef)%PlmPlus(ind0)
                   Zmin (ind,ind0) = Zmin(ind,ind0)  + D2_S_D2( iSV, jSV)  &
                                   * fcCoef(iCoef)%PlmMin(ind)  * fcCoef(iCoef)%PlmPlus(ind0)
                end do ! iSV
              end do ! imu
             end do ! jSV
           end do ! imu0
         end do ! iCoef         

       case default

         call logDebug('ERROR: incorrect value for dimSV_fc in fillZplusZmin')
         call logDebug('in subrutine fillZplusZmin in LabosModule')
         call logDebug('dimSV_fc can be 1, 3, or 4')
         call logDebugI('value provided is ', dimSV_fc)
         call mystop(errS, 'stopped because an incorrect value for dimSV_fc is used')
         if (errorCheck(errS)) return
      end select

      if ( dimSV_fc >= 3) then
        ! pre- and post multiply with D1
        ! fill D1
        D1 = 0.0d0
        do iSV = 1, dimSV_fc
          D1(iSV,iSV) = 1.0d0
        end do
        D1(3,3) = -1.0d0
        D1(2,3) =  1.0d0
        D1(3,2) =  1.0d0

        do imu0 = 1, nmutot
          ind0 = 1 + (imu0 - 1) * dimSV_fc
          do imu  = 1, nmutot
            ind = 1 + (imu - 1) * dimSV_fc
            Zplus(ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1) = &
              matmul( D1, matmul( Zplus(ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1), D1) )
            Zmin(ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1)  = &
              matmul( D1, matmul( Zmin (ind:ind+dimSV_fc-1,ind0:ind0+dimSV_fc-1), D1) )
          end do ! imu
        end do ! imu0
        
      end if

      if ( verbose ) then

        ! remove Gaussian weights
        do imu0 = 1, nmutot
          do jSV = 1, dimSV_fc
            ind0 = jSV + (imu0 - 1) * dimSV_fc
            do imu  = 1, nmutot
              weight = geometryS%w(imu) * geometryS%w(imu0)
              do iSV = 1, dimSV_fc
                ind = iSV + (imu - 1) * dimSV_fc
                Zplus(ind,ind0) = Zplus(ind,ind0) / weight
                Zmin (ind,ind0) = Zmin(ind,ind0)  / weight
             end do ! iSV
           end do ! imu
          end do ! jSV
        end do ! imu0

        write(intermediateFileUnit, '(A, I4)') 'output from fillZplusZmin: Zplus for m = ', iFourier
        if ( present(string) ) write(intermediateFileUnit, '(2A)') 'further info: ', trim(string)
!         do imu  = 1, nmutot
         do imu  = nmutot-1, nmutot
           do iSV = 1, dimSV_fc
             ind = iSV + (imu - 1) * dimSV_fc
!             write(intermediateFileUnit, '(100F15.10)') (Zplus(ind,ind0), ind0 = 1, dimSV_fc*nmutot)
             write(intermediateFileUnit, '(100(1pe13.5))') (Zplus(ind,ind0), ind0 = dimSV_fc*(nmutot-2)+1, dimSV_fc*nmutot)
          end do ! iSV
        end do ! imu

        write(intermediateFileUnit, '(A, I4)') 'output from fillZplusZmin: Zmin for m = ', iFourier
!         do imu  = 1, nmutot
         do imu  = nmutot-1, nmutot
           do iSV = 1, dimSV_fc
             ind = iSV + (imu - 1) * dimSV_fc
!             write(intermediateFileUnit, '(100F15.10)') (Zmin(ind,ind0), ind0 = 1, dimSV_fc*nmutot)
             write(intermediateFileUnit, '(100(1pe13.5))') (Zmin(ind,ind0), ind0 = dimSV_fc*(nmutot-2)+1, dimSV_fc*nmutot)
          end do ! iSV
        end do ! imu

        ! restore Gaussian weights
        do imu0 = 1, nmutot
          do jSV = 1, dimSV_fc
            ind0 = jSV + (imu0 - 1) * dimSV_fc
            do imu  = 1, nmutot
              weight = geometryS%w(imu) * geometryS%w(imu0)
              do iSV = 1, dimSV_fc
                ind = iSV + (imu - 1) * dimSV_fc
                Zplus(ind,ind0) = Zplus(ind,ind0) * weight
                Zmin (ind,ind0) = Zmin(ind,ind0)  * weight
             end do ! iSV
           end do ! imu
          end do ! jSV
        end do ! imu0

      end if

    end subroutine fillZplusZmin


    subroutine CalcRTlayers(errS, fcCoef, iFourier, maxExpCoef, RTMnlevelCloud, RTMnlayer, dimSV, dimSV_fc, nmutot, &
                            nGauss,  controlS, geometryS, optPropRTMGridS, RT_fc)

    ! Single scattering is used if the effective single scattering albedo times the
    ! optical thickness of the layer is less than 'thresholdDoubl'. Otherwise
    ! doubling is used and the start optical thickness is determined by 'thresholdDoubl'

    ! The optical properties of the layers: optical thickness, single scattering
    ! albedo and expansion coefficients, are taken from 'optPropRTMGridS'


      implicit none

      type(errorType), intent(inout) :: errS
      integer,                  intent(in)    :: iFourier, maxExpCoef
      integer,                  intent(in)    :: RTMnlevelCloud, RTMnlayer
      integer,                  intent(in)    :: dimSV, dimSV_fc, nmutot, nGauss
      type(controlType),        intent(in)    :: controlS
      type(geometrytype),       intent(in)    :: geometryS
      type(optPropRTMGridType), intent(in)    :: optPropRTMGridS
      type(fc_coefficients),    intent(in)    :: fcCoef(0:maxExpCoef)
      type(RTType),             intent(inout) :: RT_fc(0:RTMnlayer)


      ! local
      integer, parameter :: ndouble_max = 60

      integer :: ilayer, iCoef, imu, imu0, l, ndouble, iSV, ind, ind0
      real(8) :: a, b, beta(0:maxExpCoef)
      real(8) :: u, u0
      real(8) :: bstart                ! start optical thickness doubling
      real(8) :: aeff                  ! effective single scattering albedo
      real(8) :: beta_eff(0:maxExpCoef), max_beta_eff

      real(8) :: E(dimSV_fc*nmutot)
      real(8) :: DmuPlus(nmutot,nmutot),DmuMin(nmutot,nmutot)
      real(8) :: Zplus(dimSV_fc*nmutot,dimSV_fc*nmutot),Zmin(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: R(dimSV_fc*nmutot,dimSV_fc*nmutot), T(dimSV_fc*nmutot,dimSV_fc*nmutot)

      logical :: doubling

      character(LEN=50)  :: string

      logical, parameter :: verbose = .false.  ! if .true. write to intermediate file

      ! initialize
      do ilayer = 1, RTMnlayer 
        RT_fc(ilayer)%R   = 0.0d0
        RT_fc(ilayer)%T   = 0.0d0
        RT_fc(ilayer)%Rst = 0.0d0
        RT_fc(ilayer)%Tst = 0.0d0
      end do ! loop over layers

      ! pre-calculate matrices used for single scattering (outside layers loop)
      do imu0 = 1, nmutot
        u0 = geometryS%u(imu0)
        do imu = 1 , nmutot
          u = geometryS%u(imu)
          DmuPlus(imu,imu0) = 0.25d0/(u + u0)
          if ( abs(u - u0) < 1.0d-6) then
            DmuMin(imu,imu0) = 0.25d0/u/u0
          else
            DmuMin(imu,imu0) = 0.25d0/(u - u0)
          end if
        end do
      end do

      do ilayer = RTMnlevelCloud + 1, RTMnlayer

        if ( iFourier <= optPropRTMGridS%maxExpCoefLay(ilayer) ) then

          b = optPropRTMGridS%opticalThicknLay(ilayer)
          a = optPropRTMGridS%ssaLay(ilayer)

          ! starting optical thickness is based on the (1,1) element of the phase matrix
          beta(:)  = optPropRTMGridS%phasefCoefLay(1,1,:,ilayer)

          doubling = .false.

          ! determine effective expansion coefficient for the current Fourier term
          ! based on the (1,1) elements of the phase matrix (beta)
          beta_eff = beta
          do iCoef = iFourier, optPropRTMGridS%maxExpCoefLay(ilayer)
            beta_eff(iCoef) = abs(beta_eff(iCoef))/(2*iCoef+1)
          end do
          max_beta_eff = 0.0d0
          do iCoef = iFourier, optPropRTMGridS%maxExpCoefLay(ilayer)
            if ( beta_eff(iCoef) > max_beta_eff ) max_beta_eff = beta_eff(iCoef)
          end do 
          aeff    = a * max_beta_eff

          ! determine optical thickness where doubling is started
          if (aeff*b > controlS%thresholdDoubl) then
            doubling = .true.
            bstart = b
            ndouble = 0
            do l = 1, ndouble_max
              bstart = bstart/2.0d0
              ndouble = ndouble + 1
              if (aeff*bstart < controlS%thresholdDoubl) exit
            end do
          end if ! aeff*b > controlS%thresholdDoubl

          string = ' CalcRTlayers'
          call fillZplusZmin(errS, fcCoef, iFourier, optPropRTMGridS%maxExpCoefLay(ilayer), dimSV, dimSV_fc, nmutot, &
                             optPropRTMGridS%phasefCoefLay(:,:,:,ilayer), geometryS, Zplus, Zmin, string)
          if (errorCheck(errS)) return

          if (doubling) then

            ! Renormalize the fourier coefficients of the phase function for iFourier = 0.
            ! Note that the reflectance is calculated using integration of the source function
            ! (see subroutine CalcReflectance) and there normalization is not applied.

            if ( ( iFourier == 0 ) .and. controlS%renormPhaseFunction ) then
              call renorm(errS, dimSV_fc, nmutot, nGauss, geometryS, Zplus, Zmin)
              if (errorCheck(errS)) return
            end if

            do imu = 1, nmutot
              do iSV = 1, dimSV_fc
                E( iSV + (imu-1) * dimSV_fc ) = exp(-bstart/geometryS%u(imu))
              end do
            end do

            R = Rsingle(dimSV_fc, nmutot, a, E, Zmin, DmuPlus)

            T = Tsingle(dimSV_fc, nmutot, a, bstart, E, Zplus, DmuMin, geometryS)
           
            call double(errS, ndouble, dimSV_fc, nmutot, nGauss, controlS%thresholdMul, geometryS, &
                        bstart, E, R, T)
            if (errorCheck(errS)) return

          else ! doubling = .false.

            do imu = 1, nmutot
              do iSV = 1, dimSV_fc
                E( iSV + (imu-1) * dimSV_fc ) = exp(-b/geometryS%u(imu))
              end do
            end do

            R = Rsingle(dimSV_fc, nmutot, a, E, Zmin, DmuPlus)
            T = Tsingle(dimSV_fc, nmutot, a, b, E, Zplus, DmuMin, geometryS )

          end if ! doubling

          ! fill data structure

          RT_fc(ilayer)%R   = R
          RT_fc(ilayer)%T   = T
          RT_fc(ilayer)%Rst = transform_top_bottom(dimSV_fc, nmutot, R)
          RT_fc(ilayer)%Tst = transform_top_bottom(dimSV_fc, nmutot, T)

        end if !  iFourier <= optPropRTMGridS%maxExpCoefLay(ilayer)

      end do ! loop over layers

      if (verbose) then
        write(intermediateFileUnit, *) ''
        write(intermediateFileUnit, *) 'RT properties for the individual layers: R, T, Rst, and  T*'
        write(intermediateFileUnit, *) ''
        do ilayer = 1, RTMnlayer
          write(intermediateFileUnit,'(3(A20, I4))') 'ilayer = ',ilayer, ' iFourier = ', iFourier, 'ndouble = ', ndouble
          write(intermediateFileUnit,'(3(A20, F10.6))') '     a = ', optPropRTMGridS%ssaLay(ilayer), &
                                     '     b = ', optPropRTMGridS%opticalThicknLay(ilayer)
          write(intermediateFileUnit,*) 'R'
          do ind = 1, dimSV_fc * nmutot
            write(intermediateFileUnit,'(30F10.5)') (RT_fc(ilayer)%R(ind,ind0), ind0 = 1, dimSV_fc * nmutot)
          end do
          write(intermediateFileUnit,*) 'T'
          do ind = 1, dimSV_fc * nmutot
            write(intermediateFileUnit,'(30F10.5)') (RT_fc(ilayer)%T(ind,ind0), ind0 = 1, dimSV_fc * nmutot)
          end do
        end do ! loop over layers
      end if

    end subroutine CalcRTlayers


    function transform_top_bottom(dimSV_fc, nmutot, x)

      ! Transform supermatrix X from values for illumination at the top
      ! to values for illumination at the bottom for homogeneous layers.

      implicit none

      integer, intent(in) :: dimSV_fc, nmutot
      real(8), intent(in) :: x(dimSV_fc * nmutot, dimSV_fc * nmutot)
      real(8)             :: transform_top_bottom(dimSV_fc * nmutot, dimSV_fc * nmutot)

      ! local
      integer :: imu, imu0, iSV, jSV, ind, ind0, start

      transform_top_bottom = x

      if ( dimSV_fc < 3 ) return   ! do nothing

      do imu0 = 1, nmutot
        start = dimSV_fc * (imu0 - 1)
        do jSV = 3, dimSV_fc
          ind0 = start + jSV
          do ind = 1, dimSV_fc * nmutot
            transform_top_bottom(ind,ind0) = -x(ind,ind0)
          end do ! ind0
        end do ! iSV
      end do ! imu0

      do ind0 = 1, dimSV_fc * nmutot
        do imu = 1, nmutot
          start = dimSV_fc * (imu - 1)
          do iSV = 3, dimSV_fc
            ind = start + iSV
            transform_top_bottom(ind,ind0) = -transform_top_bottom(ind,ind0)
          end do ! iSV
        end do ! imu
      end do ! ind0

    end function transform_top_bottom


    function apply_q3(dimSV_fc, nmutot, x)

    ! change the sign of Stokes parameter U for the internal upward radiance x)

      implicit none

      integer, intent(in) :: dimSV_fc, nmutot
      real(8), intent(in) :: x(dimSV_fc *nmutot, 2)
      real(8)             :: apply_q3(dimSV_fc *nmutot, 2)

      ! local
      integer :: ind, ind0, imu

      apply_q3 = x

      if ( dimSV_fc < 3 ) return   ! do nothing

      do ind0 = 1, 2
        do imu = 1, nmutot
          ind = dimSV_fc * (imu - 1) + 3
          apply_q3(ind,ind0) = -x(ind,ind0)
        end do ! imu
      end do ! ind0      

    end function apply_q3


    function apply_q4(dimSV_fc, nmutot, x)

    ! change the sign of Stokes parameter V for the internal upward radiance x)

      implicit none

      integer, intent(in) :: dimSV_fc, nmutot
      real(8), intent(in) :: x(dimSV_fc *nmutot, 2)
      real(8)             :: apply_q4(dimSV_fc *nmutot, 2)

      ! local
      integer :: ind, ind0, imu

      apply_q4 = x

      if ( dimSV_fc < 4 ) return   ! do nothing

      do ind0 = 1, 2
        do imu = 1, nmutot
          ind = dimSV_fc * (imu - 1) + 4
          apply_q4(ind,ind0) = -x(ind,ind0)
        end do ! imu
      end do ! ind0      

    end function apply_q4


    subroutine renorm(errS, dimSV_fc, nmutot, nGauss, geometryS, Zplus, Zmin)

      ! renormalization of the first Fourier coefficient of the phase function
      ! demand that after numerical integration sum [w(i) * (Zplus(i,j) + Zmin(i,j)) ] = 2.0d0
      ! due to imperfect numerical integration this condition is not fulfilled, especially
      ! for strong forward scattering by cloud or aerosol particles
      ! if i <= nGauss the diagonal elements of Zplus are adapted so that the condition is fullfilled
      ! for i > nGauss changing the diagonal elements does not help to improve the integration
      ! instead values for combined gaussian - non-gaussian points have to be adapted. 
      ! here the values closest to the forward direction (diagonal) are modified weighted 
      ! with the distance to the diagonal
      !   - code adapted from the renormalization code written by V. Dolman (VU) mid 1980

      type(errorType), intent(inout) :: errS
      integer,            intent(in)    :: dimSV_fc, nmutot, nGauss
      type(geometryType), intent(in)    :: geometryS
      real(8),            intent(inout) :: Zplus(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8),            intent(inout) :: Zmin (dimSV_fc*nmutot,dimSV_fc*nmutot)


      ! local
      integer     :: imu, imu0, low, high, ind, ind0
      real(8)     :: weight_low, weight_high
      real(8)     :: integral
      ! fraction change and maximal fractions change for gaussian points and for additional points
      real(8)     :: frac, frac_max_gauss, frac_max_additional, frac_high, frac_low

      ! only the (1,1) element is renormalized because the polarization is very weak
      ! for nearly forward scattering
      real(8)     :: Zp(nmutot,nmutot), Zm(nmutot,nmutot)

      logical, parameter  :: verbose = .false.

      ! transfer values from Zplus to zp and from Zmin to Zm
      ! where Zp and Zm pertain only to the (1,1) element 
      do imu0 = 1, nmutot
        ind0 = 1 + (imu0 - 1) * dimSV_fc
        do imu = 1, nmutot
          ind   = 1 + (imu - 1) * dimSV_fc
          Zp(imu,imu0) = Zplus(ind, ind0)
          Zm(imu,imu0) = Zmin (ind, ind0)
        end do ! imu
      end do ! imu0

      ! remove the radiative transfer weights
      do imu0 = 1, nmutot
        do imu = 1, nmutot
          Zp(imu,imu0) = Zp(imu,imu0) / ( geometryS%w(imu) * geometryS%w(imu0) )
          Zm(imu,imu0) = Zm(imu,imu0) / ( geometryS%w(imu) * geometryS%w(imu0) )
        end do
      end do

      ! process gaussian points
      frac_max_gauss = 0.0d0
      do imu0 = 1, nGauss
        integral = 0.0d0
        do imu = 1, nGauss
          integral = integral + geometryS%wg(imu) * ( Zp(imu,imu0) + Zm(imu,imu0) )
        end do
        frac = (2.0d0 - integral) / ( Zp(imu0,imu0) *  geometryS%wg(imu0) )
        Zp(imu0,imu0) = (1.0d0 + frac) * Zp(imu0,imu0)
        if ( abs(frac) > frac_max_gauss ) frac_max_gauss = abs(frac)
      end do  

      ! process gaussian/additional points
      frac_low            = 0.0d0
      frac_high           = 0.0d0
      frac_max_additional = 0.0d0
      do imu0 = nGauss + 1, nmutot
        integral = 0.0d0
        do imu = 1, nGauss
          integral = integral + geometryS%wg(imu) * ( Zp(imu,imu0) + Zm(imu,imu0) )
        end do
        ! additional mu lies between gaussian division points
        if ( (geometryS%u(imu0) > geometryS%ug(1)) .and. (geometryS%u(imu0) < geometryS%ug(nGauss)) ) then
          low  = max(min(locate(geometryS%ug,geometryS%u(imu0),nGauss),nGauss-1),1)
          high = low + 1
          weight_low    = (geometryS%u(imu0)  - geometryS%ug(low) ) / (geometryS%ug(high) - geometryS%ug(low))
          weight_high   = (geometryS%ug(high) - geometryS%u(imu0) ) / (geometryS%ug(high) - geometryS%ug(low))
          frac_low      = weight_low  * (2.0d0 - integral) / ( Zp(imu0,low ) *  geometryS%wg(low) )
          frac_high     = weight_high * (2.0d0 - integral) / ( Zp(imu0,high) *  geometryS%wg(high) )
          Zp(imu0,low)  = (1.0d0 + frac_low ) * Zp(imu0,low)
          Zp(imu0,high) = (1.0d0 + frac_high) * Zp(imu0,high)
          Zp(low,imu0)  = Zp(imu0,low)
          Zp(high,imu0) = Zp(imu0,high)
          if ( abs(frac_low)  > frac_max_additional ) frac_max_additional = abs(frac_low)
          if ( abs(frac_high) > frac_max_additional ) frac_max_additional = abs(frac_high)
        end if 
        ! additional mu is smaller than first gaussian point
        if ( geometryS%u(imu0) < geometryS%ug(1) ) then
          low  = 1
          frac_low      = (2.0d0 - integral) / ( Zp(imu0,low ) *  geometryS%wg(low) )
          Zp(imu0,low)  = (1.0d0 + frac_low ) * Zp(imu0,low)
          Zp(low,imu0)  = Zp(imu0,low)
          if ( abs(frac_low)  > frac_max_additional ) frac_max_additional = abs(frac_low)
        end if 
        ! additional mu is larger than last gaussian point
        if ( geometryS%u(imu0) > geometryS%ug(nGauss) ) then
          high  = nGauss
          frac_high      = (2.0d0 - integral) / ( Zp(imu0,high ) *  geometryS%wg(high) )
          Zp(imu0,high)  = (1.0d0 + frac_high ) * Zp(imu0,high)
          Zp(high,imu0)  = Zp(imu0,high)
          if ( abs(frac_high)  > frac_max_additional ) frac_max_additional = abs(frac_high)
        end if 
      end do

      ! multiply with the radiative transfer weights
      do imu0 = 1, nmutot
        do imu = 1, nmutot
          Zp(imu,imu0) = Zp(imu,imu0) * geometryS%w(imu) * geometryS%w(imu0)
          Zm(imu,imu0) = Zm(imu,imu0) * geometryS%w(imu) * geometryS%w(imu0)
        end do
      end do

      ! put values in Zplus and Zmin
      do imu0 = 1, nmutot
        ind0 = 1 + (imu0 - 1) * dimSV_fc
        do imu = 1, nmutot
          ind   = 1 + (imu - 1) * dimSV_fc
          Zplus(ind, ind0) = Zp(imu,imu0)
          Zmin (ind, ind0) = Zm(imu,imu0)
        end do ! imu
      end do ! imu0

      if ( verbose ) then
        write (intermediateFileUnit,'(A,2ES15.5)') 'renorm: frac_max_gauss and frac_max_additional : ', &
              frac_max_gauss, frac_max_additional
      end if

    end subroutine renorm


    subroutine double(errS, ndouble, dimSV_fc, nmutot, nGauss, thresholdMul, geometryS, b, E, R, T)

      implicit none

      ! perform ndouble doubling steps

      type(errorType), intent(inout) :: errS
      integer,            intent(in)     :: ndouble
      integer,            intent(in)     :: dimSV_fc, nmutot, nGauss
      real(8),            intent(in)     :: thresholdMul
      type(geometryType), intent(in)     :: geometryS
      real(8),            intent(inout)  :: b
      real(8),            intent(inout)  :: E(dimSV_fc*nmutot)
      real(8),            intent(inout)  :: R(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8),            intent(inout)  :: T(dimSV_fc*nmutot,dimSV_fc*nmutot)

      ! local
      real(8) :: Rst(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Tst(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Q  (dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: U  (dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: D  (dimSV_fc*nmutot,dimSV_fc*nmutot)

      integer :: idouble, imu, iSV, ind

      do idouble = 1, ndouble 
        Rst  = transform_top_bottom(dimSV_fc, nmutot, R)
        Tst  = transform_top_bottom(dimSV_fc, nmutot, T)
        Q    = Qseries(errS, dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Rst, R)
        D    = T + semul(dimSV_fc*nmutot, Q, E) + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Q, T) 
        U    = semul(dimSV_fc*nmutot, R, E) + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, R, D)
        R    = R + esmul(dimSV_fc*nmutot, E, U) + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, Tst, U)
        T    = esmul(dimSV_fc*nmutot, E, D) + semul(dimSV_fc*nmutot, T, E) &
             + smul(dimSV_fc*nmutot, dimSV_fc*nGauss, thresholdMul, T, D)
        b = 2.0d0 * b
        if ( b < 0.001 ) then
          ! re-calculate E to avoid loss of significant digits
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu-1) * dimSV_fc
              E(ind) = exp(-b/geometryS%u(imu))
            end do
          end do
        else
          E = E * E
        end if
      end do

    end subroutine double


    subroutine ordersScat(errS, controlS, geometryS, numOrdersMax, startLevel,  &
                          endLevel, atten, dimSV_fc, nmutot, nmuextra, nGauss,  &
                          RT_fc, UDsumLocal_fc, UDLocal_fc, UDorde_fc, UD_fc)                          

      ! This routine calculates the Fourier coefficients of the
      ! internal radiation field at the interfaces, UDClear_fc, UDCloud_fc,
      ! for the clear and cloudy part of the pixel, respectively.
      ! The method used is successive orders of scattering, but between layers and
      ! not between volume elements. The reflection and transmission properties
      ! of the individual layers are stored in RTclear_fc and RTCloud_fc (the only 
      ! difference between these is the reflectance at the lower layer).
      ! Once order j is known for the atmsophere, the next order is calculated in two
      ! steps. First, the light scattered by a layer is calculated using the incident
      ! light of order j and the known reflection and transmission properties. This
      ! scattered light is then known at the boundaries of the layers where scattering
      ! took place. Second, the scattered light of order j+1 is then calculated for
      ! all of the interfaces by transporting this light to the interfaces at a distance
      ! from the layer where scattering took place.
      ! After a number of scatterings the next order of scattering resembles the previous
      ! order, except that it is somewhat smaller. Here we assume that we can sum the
      ! contributions of the remaining orders of scattering as a geometric series, once
      ! a maximum number of scattering calculations has been performed. Typically, for
      ! ozone profile calculations, a maximum number of 5 orders gives errors of a few
      ! tenths of a percent or less. Thresholds are used to exit earlier if there is
      ! much absorption by ozone. The reflecting surface is considered to be a layer.

      implicit none

      type(errorType), intent(inout) :: errS
      type(controlType),  intent(in) :: controlS
      type(geometryType), intent(in) :: geometryS
      integer,            intent(in) :: numOrdersMax, startLevel, endLevel
      integer,            intent(in) :: dimSV_fc, nmutot, nmuextra, nGauss
      real(8),            intent(in) :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(RTType),       intent(in) :: RT_fc(0:endLevel)

      ! local internal fields
      ! it contains the internal field at the interfaces of a layer if scattering occurs
      ! in that layer (UDLocal and UDorde are stored outside this routine to keep
      ! from allocating and deallocating memory space in one routine). 
      type(UDLocalType),  intent(inout)  :: UDLocal_fc(0:endLevel)

      ! the first order is used for pseudo-spherical correction ( used for d2R/dkabs/dz )
      type(UDLocalType),  intent(inout)  :: UDsumLocal_fc(0:endLevel)

      ! internal fields at the interfaces for the successive orders of scattering
      type(UDType),       intent(inout)  :: UDorde_fc(0:endLevel)

      ! internal fields summed over the successive orders of scattering
      type(UDType),       intent(inout)  :: UD_fc(0:endLevel)

      integer :: ilevel, imu, imu0
      integer :: ind, ind0, iSV
      integer :: numOrders
      real(8) :: sumIntField(nmuextra), sumIntField_prev(nmuextra)
      real(8) :: eigenvalue(nmuextra)
      real(8) :: maxValue

      logical, parameter :: verbose = .false.

      ! initialize
      do ilevel = 0, endLevel
        UDsumLocal_fc(ilevel)%U = 0.0d0
        UDsumLocal_fc(ilevel)%D = 0.0d0
        UDLocal_fc(ilevel)%U    = 0.0d0
        UDLocal_fc(ilevel)%D    = 0.0d0
        UDorde_fc(ilevel)%E     = 0.0d0
        UDorde_fc(ilevel)%U     = 0.0d0
        UDorde_fc(ilevel)%D     = 0.0d0
        UD_fc(ilevel)%E         = 0.0d0
        UD_fc(ilevel)%U         = 0.0d0
        UD_fc(ilevel)%D         = 0.0d0
      end do

      ! copy attenuation to UDorder and UD
      do ilevel = startLevel, endLevel
        do imu = 1, nmutot
          do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            UDorde_fc(ilevel)%E(ind) = atten(imu,endLevel,ilevel)
            UD_fc(ilevel)%E(ind)     = atten(imu,endLevel,ilevel)
          end do ! imu
        end do ! iSV
      end do

      ! initialize with single scattering by the layers

      do ilevel = startLevel, endLevel-1
        do imu0 = 1, 2
          ind0 = 1 + (imu0 -1) * dimSV_fc
          do ind = 1, dimSV_fc * nmutot
            UDLocal_fc(ilevel)%D(ind,imu0) = RT_fc(ilevel+1)%T(ind, dimSV_fc * nGauss + ind0) &
                                           * atten(nGauss+imu0, endLevel, ilevel+1)
          end do ! ind
        end do ! imu0
      end do ! ilevel
      UDLocal_fc(endLevel)%D  = 0.0d0  ! no diffuse light incident at the top
      do ilevel = startLevel, endLevel
        do imu0 = 1, 2
          ind0 = 1 + (imu0 -1) * dimSV_fc
          do ind = 1, dimSV_fc * nmutot
            UDLocal_fc(ilevel)%U(ind,imu0) = RT_fc(ilevel)%R(ind, dimSV_fc * nGauss + ind0) &
                                           * atten(nGauss+imu0, endLevel, ilevel)
          end do ! ind
        end do ! imu0
      end do ! ilevel

      ! initialize UDsumLocal using the first order
      do ilevel = startLevel, endLevel
           UDsumLocal_fc(ilevel)%U   = UDLocal_fc(ilevel)%U
           UDsumLocal_fc(ilevel)%D   = UDLocal_fc(ilevel)%D
      end do ! ilevel      

      call transportToOtherLevels(errS, startLevel, endLevel, dimSV_fc, nmutot, &
                                  atten, UDLocal_fc, UDorde_fc)
      if (errorCheck(errS)) return

      ! copy first order to total
      do ilevel = startLevel, endLevel
        UD_fc(ilevel)%D  = UDorde_fc(ilevel)%D
        UD_fc(ilevel)%U  = UDorde_fc(ilevel)%U
      end do

      numorders = 1

      ! test convergence after one order of scattering
      maxValue = 0.0d0
      do imu0 = 1, 2
        do imu = dimSV_fc * nGauss + 1, dimSV_fc * nmutot, dimSV_fc
          if ( abs(UDorde_fc(endLevel)%U(imu, imu0)) > maxValue ) &
            maxValue = abs(UDorde_fc(endLevel)%U(imu, imu0))
        end do ! imu
      end do ! imu0
      if ( maxValue  < controlS%thresholdConv_first) then

        if ( verbose )  write(intermediateFileUnit,'(A, I4)') 'numorders= ', numorders

      else ! higher orders of scattering
        do ! loop over orders of scattering

          numorders = numorders + 1 

          do ilevel = startLevel, endLevel - 1
            do imu0 = 1, 2
              do imu = 1, dimSV_fc * nmutot
                UDLocal_fc(ilevel)%D(imu,imu0) = &
                dot_product(RT_fc(ilevel+1)%Rst(imu,1:dimSV_fc*nGauss),        &
                            UDorde_fc(ilevel  )%U(1:dimSV_fc*nGauss,imu0) ) +  &
                dot_product(RT_fc(ilevel+1)%T  (imu,1:dimSV_fc*nGauss),        &
                            UDorde_fc(ilevel+1)%D(1:dimSV_fc*nGauss,imu0) )
              end do ! imu
            end do ! imu0
          end do ! ilevel
          UDLocal_fc(endLevel)%D = 0.0d0  ! at the top of the atmosphere no diffuse downward light

          ! UDLocal_fc(startLevel)%U is a special case
          do imu0 = 1, 2
            do imu = 1, dimSV_fc * nmutot
              UDLocal_fc(startLevel)%U(imu,imu0) = &
              dot_product(RT_fc(startLevel)%R(imu,1:dimSV_fc * nGauss), &
                          UDorde_fc(startLevel)%D(1:dimSV_fc * nGauss,imu0))
            end do ! imu
          end do ! imu0

          do ilevel = startLevel + 1, endLevel
            do imu0 = 1, 2
              do imu = 1, dimSV_fc * nmutot
                UDLocal_fc(ilevel)%U(imu,imu0) = &
                dot_product(RT_fc(ilevel)%R(imu,1:dimSV_fc * nGauss),          &
                            UDorde_fc(ilevel)%D(1:dimSV_fc * nGauss,imu0) ) +  &
                dot_product(RT_fc(ilevel)%Tst(imu,1:dimSV_fc *nGauss),         &
                            UDorde_fc(ilevel-1)%U(1:dimSV_fc * nGauss,imu0) )
              end do ! imu
            end do ! imu0
          end do ! ilevel


          call transportToOtherLevels(errS, startLevel, endLevel, dimSV_fc, nmutot, &
                                      atten, UDLocal_fc, UDorde_fc)
          if (errorCheck(errS)) return
                                      
          ! test convergence (only for the (1,1) element)
          maxValue = 0.0d0
          do imu0 = 1, 2
            do imu = dimSV_fc * nGauss + 1, dimSV_fc * nmutot, dimSV_fc
              if ( abs(UDorde_fc(endLevel)%U(imu, imu0)) > maxValue ) &
                maxValue = abs(UDorde_fc(endLevel)%U(imu, imu0))
            end do ! imu
          end do ! imu0

          if ( (maxValue  < controlS%thresholdConv_mult) .or. (numorders == numOrdersMax) ) then
             if ( verbose )  write(intermediateFileUnit,*) 'numorders= ', numorders
            exit ! exit loop over orders of scattering
          end if

          ! calculate the sum of the internal field (used to calculate the eigenvalue)
          ! use the absolute value as negative values can occur for Fourier terms larger than 0.
          ! correct for the weights used for the Gaussian integrations
          sumIntField = 0.0d0
          do imu0 = 1,2
            do ilevel = startLevel, endLevel
              do imu = nGauss + 1, nmutot
                ind = 1 + (imu - 1) * dimSV_fc
                sumIntField(imu0) = sumIntField(imu0)                                    &
                                  + abs(UDorde_fc(ilevel)%U(ind, imu0))/geometryS%w(imu) &
                                  + abs(UDorde_fc(ilevel)%D(ind, imu0))/geometryS%w(imu)
              end do ! imu
            end do ! ilevel
          end do ! imu0

          ! if numorders = numOrdersMax, approximate the sum of the
          ! remaining orders using the eigenvalue (geometric series assumed)

          ! criteria for stopping the calculation of new orders summing the eometrical series
          ! - estimate the relative contribution of the geometric series to the total
          ! - estimate the accuracy of the eigenvalue by comparing it to the previous eigenvalue
          ! - estimate the error for the summed contribution in comparision to total
          ! - truncate the calculation if the error is less than a threshold
          ! - not yet implemented

          if (numorders == numOrdersMax) then

            do imu0 = 1, 2
              if (sumIntField_prev(imu0) > 1.0d-10) then
                eigenvalue(imu0) = sumIntField(imu0) / sumIntField_prev(imu0)
              else
                eigenvalue(imu0) = 0.0d0
              end if
              do ilevel = startLevel, endLevel
                do imu = 1, dimSV_fc * nmutot
                  UD_fc(ilevel)%U(imu, imu0) = UD_fc(ilevel)%U(imu, imu0) &
                       + UDorde_fc(ilevel)%U(imu, imu0) / (1.0d0 - eigenvalue(imu0))
                  UD_fc(ilevel)%D(imu, imu0) = UD_fc(ilevel)%D(imu, imu0) &
                       + UDorde_fc(ilevel)%D(imu, imu0) / (1.0d0 - eigenvalue(imu0))
                  UDsumLocal_fc(ilevel)%U(imu, imu0)   = UDsumLocal_fc(ilevel)%U(imu, imu0) &
                       + UDLocal_fc(ilevel)%U(imu, imu0) / (1.0d0 - eigenvalue(imu0))
                  UDsumLocal_fc(ilevel)%D(imu, imu0)   = UDsumLocal_fc(ilevel)%D(imu, imu0) &
                       + UDLocal_fc(ilevel)%D(imu, imu0) / (1.0d0 - eigenvalue(imu0))
                end do ! imu
              end do ! ilevel
            end do ! imu0

            if (verbose) then
              write(intermediateFileUnit,*) 'maximum number of orders used : ', numorders
            end if

            exit ! exit loop over orders of scattering

          else  ! numorders == numOrdersMax

          ! add the results for U and D to the total
            do ilevel = startLevel, endLevel
              UD_fc(ilevel)%U           = UD_fc(ilevel)%U         + UDorde_fc(ilevel)%U
              UD_fc(ilevel)%D           = UD_fc(ilevel)%D         + UDorde_fc(ilevel)%D
              UDsumLocal_fc(ilevel)%U   = UDsumLocal_fc(ilevel)%U + UDLocal_fc(ilevel)%U
              UDsumLocal_fc(ilevel)%D   = UDsumLocal_fc(ilevel)%D + UDLocal_fc(ilevel)%D
            end do
            
          end if ! numorders == numOrdersMax

          ! update previous sum
          sumIntField_prev = sumIntField

        end do ! loop orders of scattering
      end if ! higher orders of scattering

    end subroutine ordersScat


    subroutine transportToOtherLevels(errS, startLevel, endLevel, dimSV_fc, nmutot, &
                                      atten, UDLocal_fc, UDorde_fc)

      ! Subroutine transportToOtherLevels calculates the radiation fields (U and D) at the interfaces
      ! between the layers for the next order of scattering using the local radiation fields
      ! calculated in subroutine ordersScat. The light from the local radation field arrives at the
      ! other levels after attenuation. Here the contribution from these attenuated local fields 
      ! are summed to give the radiation field for the next order of scattering.

      implicit none
                                      
      type(errorType),     intent(inout) :: errS
      integer,             intent(in)    :: startLevel, endLevel
      integer,             intent(in)    :: dimSV_fc, nmutot
      real(8),             intent(in)    :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(UDLocalType),   intent(in)    :: UDLocal_fc(0:endLevel)
      type(UDType),        intent(inout) :: UDorde_fc(0:endLevel)

      !local

      integer :: ilevel, imu, imu0, iSV
      integer :: ind

      ! For a plane-parallel layer we can use one loop over the layers
      ! because the light at the next level is the attenuated light of the previous
      ! level plus the local light at this level. For a spherical atmosphere we can not
      ! do that because the attenuation depends on original and final level.

      UDorde_fc(startlevel)%U = UDLocal_fc(startlevel)%U
      do ilevel = startLevel + 1, endLevel
        do imu0 = 1, 2
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              UDorde_fc(ilevel)%U(ind,imu0) = UDLocal_fc(ilevel)%U(ind,imu0)  &
                 + atten(imu, ilevel-1, ilevel) * UDorde_fc (ilevel-1)%U(ind,imu0)
            end do ! iSV
          end do ! imu
        end do ! imu0
      end do ! ilevel

      ! Downward (diffuse) traveling light vanishes at the top of the atmosphere
      UDorde_fc(endLevel)%D = 0.0d0

      do ilevel = endLevel-1, startLevel, -1
        do imu0 = 1, 2
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              UDorde_fc(ilevel)%D(ind,imu0) = UDLocal_fc(ilevel)%D(ind,imu0)  &
                 + atten(imu, ilevel+1, ilevel) * UDorde_fc (ilevel+1)%D(ind,imu0)
            end do ! iSV
          end do ! imu
        end do ! imu0
      end do ! ilevel

    end subroutine transportToOtherLevels


    subroutine CalcReflectance (errS, fcCoef, optPropRTMGridS, controlS,  &
                                RTMnlayer, RTMnlevelCloud,          &
                                iFourier, dimSV, dimSV_fc,          &
                                nmutot, nGauss, geometryS,          &
                                UD_fc, contribrefl_fc, refl_fc)

      ! In CalcReflectance the reflectance (not sun-normalized raiance)
      ! is calculated. There are two options:
      ! - simply use the radiation at the top of the atmosphere
      ! - calculate the source vector from the internal radiation field
      !   and integrate over altitude using the attenuated source funtion and
      !   add the radiation coming from the Lambertian cloud or Lambertian surface.
      ! The contribution vector is also returned (also called path radiance) which
      ! is the attenuated source function.

      implicit none

      type(errorType), intent(inout) :: errS
      type(optPropRTMGridType), intent(in)  :: optPropRTMGridS
      type(fc_coefficients),    intent(in)  :: fcCoef(0:optPropRTMGridS%maxExpCoef)
      type(controlType),        intent(in)  :: controlS 
      integer,                  intent(in)  :: RTMnlayer, RTMnlevelCloud
      integer,                  intent(in)  :: iFourier
      integer,                  intent(in)  :: dimSV, dimSV_fc, nmutot, nGauss
      type(geometryType),       intent(in)  :: geometryS
      type(UDType),             intent(in)  :: UD_fc(0:RTMnlayer)
      real(8),                  intent(out) :: contribrefl_fc(dimSV_fc, 0:RTMnlayer)
      real(8),                  intent(out) :: refl_fc(dimSV_fc)

      ! local

      real(8) :: Zplus(dimSV_fc*nmutot,dimSV_fc*nmutot), Zmin(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Zplusst(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: Pplusst(dimSV_fc*nmutot,dimSV_fc*nmutot), Pmin(dimSV_fc*nmutot,dimSV_fc*nmutot)
      real(8) :: PminED(dimSV_fc), PplusstU(dimSV_fc)

      integer :: ilevel, maxExpCoefLevel
      integer :: is, imu, imu0, iSV, jSV, ind, ind0
      real(8) :: sumRefl(dimSV_fc)

      character(LEN=50) :: string

      logical, parameter :: verbose = .false.

      if( controlS%integrateSourceFunction ) then

        ! first calculate the contribution to the reflectance

        ! dR/dz = ksca(z) * E * [ P- * (E + D) +  E * P+ * U ]
        ! representing the ilimination of a thin layer at the top and the bottom,
        ! scattering by that layer, and direct attenuation to the top
        ! if the direct attenuation is omitted it is the source function

        ! contribution vanishes below the cloud
        if (RTMnlevelCloud > 0) then
          contribrefl_fc(:, 0:RTMnlevelCloud - 1) = 0.0d0
        end if

        is = 2
        sumRefl = 0.0d0
        do ilevel = RTMnlevelCloud, RTMnlayer

          if ( ilevel == 0 )         maxExpCoefLevel = optPropRTMGridS%maxExpCoefLay(1)
          if ( ilevel == RTMnlayer ) maxExpCoefLevel = optPropRTMGridS%maxExpCoefLay(RTMnlayer)
          if ( ( ilevel > 0 ) .and. ( ilevel < RTMnlayer ) ) &
             maxExpCoefLevel = max( optPropRTMGridS%maxExpCoefLay(ilevel), optPropRTMGridS%maxExpCoefLay(ilevel+1) )

          if ( iFourier <= maxExpCoefLevel ) then
            string = trim(' calcReflectance')
            call fillZplusZmin(errS, fcCoef, iFourier, maxExpCoefLevel, dimSV, dimSV_fc, nmutot, &
                               optPropRTMGridS%phasefCoefAbove(:,:,:,ilevel), geometryS, Zplus, Zmin, string)
            if (errorCheck(errS)) return

            ! Zplusst is z(-mu, -mu0)
            Zplusst = transform_top_bottom(dimSV_fc, nmutot, Zplus)

            ! calculate effective phase matrices, Pplus and Pmin used for the calculation
            ! of the (polarized) source function
          
            do imu0 = 1, nmutot
              do jSV = 1, dimSV_fc
                ind0 = jSV + (imu0 -1) * dimSV_fc
                do imu = 1, nmutot
                  do iSV = 1, dimSV_fc
                    ind = iSV + (imu -1) * dimSV_fc
                    Pplusst(ind,ind0) = 0.25d0 * Zplusst(ind,ind0) / geometryS%u(imu) / geometryS%u(imu0)
                    Pmin (ind,ind0)   = 0.25d0 * Zmin (ind,ind0)   / geometryS%u(imu) / geometryS%u(imu0)
                  end do ! iSV
                end do ! imu
              end do ! jSV
            end do ! imu0

            ! calculate the contributions to the source function:
            ! - PminED volume element is illuminated by downward direct and diffuse light
            ! - PplusU volume element is illuminated by upward diffuse light
            ind0 = 1 + (nGauss + 1) * dimSV_fc
            do iSV = 1, dimSV_fc
              ind = iSV + nGauss * dimSV_fc
              PminED(iSV) = dot_product( Pmin (ind, 1:dimSV_fc*nGauss), &
                                         UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                          + Pmin (ind, ind0) * UD_fc(ilevel)%E( ind0)
              PplusstU(iSV) = dot_product( Pplusst(ind, 1:dimSV_fc*nGauss), &
                                           UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))

              contribrefl_fc(iSV, ilevel) =  UD_fc(ilevel)%E(ind) * optPropRTMGridS%ksca(ilevel) &
                                          * (PminED(iSV) + PplusstU(iSV))
              ! integration
              sumRefl(iSV) = sumRefl(iSV) + optPropRTMGridS%RTMweight(ilevel) * contribrefl_fc(iSV, ilevel)
            end do ! iSV

          end if ! iFourier <= maxExpCoefLevel

        end do ! ilevel

        if ( iFourier == 0 ) then
        ! add surface contribution - for Lambertian surface => only intensity affected
          sumRefl(1) = sumRefl(1) + UD_fc(RTMnlevelCloud)%E(1 + nGauss * dimSV_fc) &
                                  * UD_fc(RTMnlevelCloud)%U(1 + nGauss * dimSV_fc, is)
        end if

        refl_fc = sumRefl

      else 

        ! The contribution to the reflectance is only calculated for controlS%integrateSourceFunction = .true.
        contribrefl_fc = 0.0d0

        ! reflectance for stacked homogeneous layers
        is = 2
        do iSV = 1, dimSV_fc
          ind = iSV + nGauss * dimSV_fc
          refl_fc(iSV) = UD_fc(RTMnlayer)%U(ind, is)
        end do

      end if ! controlS%integrateSourceFunction

      if (verbose) then
        write(intermediateFileUnit,*) 'refl_fc =           ', refl_fc
        write(intermediateFileUnit,*) 'total otical thickness = ', sum( optPropRTMGridS%opticalThicknLay(:))
        write(intermediateFileUnit,*) ' altitude   weight   contrib (I,Q,U,V)  for iFourier = ', iFourier
        do ilevel = 1, RTMnlayer
          write(intermediateFileUnit,'(20F14.10)') optPropRTMGridS%RTMaltitude(ilevel), &
                           optPropRTMGridS%RTMweight(ilevel), (contribrefl_fc(iSV, ilevel), iSV = 1, dimSV_fc) 
        end do
      end if

    end subroutine CalcReflectance


    subroutine CalcWeightingFunctionsInterface (errS, fcCoef, optPropRTMGridS, controlS, &
                                                iFourier, RTMnlayer, RTMnlevelCloud,     &
                                                dimSV, dimSV_fc, nmutot, nGauss,         &
                                                geometryS, UDsumLocal_fc, UD_fc,         &
                                                wfInterfkscaGas_fc,                      &
                                                wfInterfkscaAerAbove_fc,                 &
                                                wfInterfkscaAerBelow_fc,                 &
                                                wfInterfkscaCldAbove_fc,                 &
                                                wfInterfkscaCldBelow_fc,                 &
                                                wfInterfkabs_fc,                         &
                                                wfAlbedo_fc)

      ! Depending on the flags, calculate the Fourier coefficients of the 
      ! derivatives at the interfaces of the layers, the derivatives for the surface/cloud albedo,
      ! and the derivative w.r.t. the altitude. 

      implicit none

      type(errorType), intent(inout) :: errS
      type(optPropRTMGridType), intent(in)  :: optPropRTMGridS
      type(fc_coefficients),    intent(in)  :: fcCoef(0:optPropRTMGridS%maxExpCoef)
      type(controlType),        intent(in)  :: controlS 
      integer,                  intent(in)  :: iFourier, RTMnlayer, RTMnlevelCloud
      integer,                  intent(in)  :: dimSV, dimSV_fc, nmutot, nGauss
      type(geometryType),       intent(in)  :: geometryS
      type(UDLocalType),        intent(in)  :: UDsumLocal_fc(0: RTMnlayer)
      type(UDType),             intent(in)  :: UD_fc(0:RTMnlayer)

      real(8),                  intent(out) :: wfInterfkscaGas_fc(0:RTMnlayer)
      real(8),                  intent(out) :: wfInterfkscaAerAbove_fc(0:RTMnlayer)
      real(8),                  intent(out) :: wfInterfkscaAerBelow_fc(0:RTMnlayer)
      real(8),                  intent(out) :: wfInterfkscaCldAbove_fc(0:RTMnlayer)
      real(8),                  intent(out) :: wfInterfkscaCldBelow_fc(0:RTMnlayer)
      real(8),                  intent(out) :: wfInterfkabs_fc(0:RTMnlayer)
      real(8),                  intent(out) :: wfAlbedo_fc

      ! local

      real(8) :: OneOverFourMuMu0(nmutot,nmutot)

      integer :: ilevel, imu, imu0

      logical, parameter :: verbose = .false.

! JdH  counter helps to compare results of different runs
      integer :: counter
      save counter

! JdH
      counter = counter + 1

      ! pre-calculate 1/4/mu/mu0
      do imu0 = 1, nmutot
        do imu = 1 , nmutot        
          OneOverFourMuMu0(imu,imu0) = 0.25d0/(geometryS%u(imu)* geometryS%u(imu0))
        end do
      end do

      !-----------------------------------------------------------------------------------------------
      ! derivative of reflectance w.r.t. the volume absorption coefficient, dR/dkabs
      ! at the interfaces

      if (controlS%calculateDerivatives) then

        call CalcDerivdRdkabs(errS, controlS%useSphericalCorr, dimSV_fc, nGauss, RTMnlevelCloud,  &
                              RTMnlayer, geometryS, optPropRTMGridS%RTMaltitude,                  &
                              UDsumLocal_fc, UD_fc, wfInterfkabs_fc)
        if (errorCheck(errS)) return

        if (verbose) then

          write(intermediateFileUnit,*) ' output from  LabosModule: CalcWeightingFunctionsInterface'
          write(intermediateFileUnit,*) ' '
          write(intermediateFileUnit,*) 'counter = ', 1 + (counter-1)/3
          write(intermediateFileUnit,*) 'fouriercoefficients of d2R/dkabs/dz, iFourier = ', iFourier
          write(intermediateFileUnit,*) 'level  altitude  wfkabs'
          do ilevel = RTMnlayer, 0, -1
            write(intermediateFileUnit,'(i4, F10.4, E15.5)') ilevel, optPropRTMGridS%RTMaltitude(ilevel),  &
                     wfInterfkabs_fc(ilevel)
          end do

        end if ! verbose

      else

        wfInterfkabs_fc       = 0.0d0

      end if ! controlS%calculateDerivatives

      !-----------------------------------------------------------------------------------------------
      ! derivatives of reflectance w.r.t. the volume scattering coefficient, d2R/dksca/dz,
      ! at the interfaces for molecules, aerosol, and cloud particles

      if (controlS%calculateDerivatives) then

        call CalcDerivdRdksca(errS, fcCoef, dimSV, dimSV_fc, nGauss, iFourier, nmutot, &
                              OneOverFourMuMu0, RTMnlevelCloud, RTMnlayer, geometryS,  &
                              controlS, optPropRTMGridS, UD_fc,                        &
                              wfInterfkabs_fc, wfInterfkscaGas_fc,                     &
                              wfInterfkscaAerAbove_fc, wfInterfkscaAerBelow_fc,        &
                              wfInterfkscaCldAbove_fc, wfInterfkscaCldBelow_fc)
        if (errorCheck(errS)) return

        if (verbose) then
          write(intermediateFileUnit,*) ' '
          write(intermediateFileUnit,*) 'fouriercoefficients of d2R/dksca/dz, iFourier = ', iFourier
          write(intermediateFileUnit,'(A)') &
               'llevel  altitude    wfkscaGas    wfkscaAerA   wfkscaCloudA    wfkscaAerB   wfkscaCloudB'
          do ilevel = 0, RTMnlayer
            write(intermediateFileUnit,'(I4, F10.2, 20E16.5)') ilevel, optPropRTMGridS%RTMaltitude(ilevel),     &
                   wfInterfkscaGas_fc(ilevel), wfInterfkscaAerAbove_fc(ilevel), wfInterfkscaCldAbove_fc(ilevel),&
                   wfInterfkscaAerBelow_fc(ilevel), wfInterfkscaCldBelow_fc(ilevel)
          end do
        end if ! verbose

      else

        wfInterfkscaGas_fc = 0.0d0
        wfInterfkscaAerAbove_fc = 0.0d0
        wfInterfkscaAerBelow_fc = 0.0d0
        wfInterfkscaCldAbove_fc = 0.0d0
        wfInterfkscaCldBelow_fc = 0.0d0

      end if ! calculateDerivatives

      !-----------------------------------------------------------------------------------------------
      ! Drivative of reflectance w.r.t. surface or cloud albedo

      call CalcDerivdRdA(errS, dimSV_fc, nGauss, iFourier, RTMnlevelCloud, RTMnlayer, geometryS, UD_fc, &
                         wfAlbedo_fc)
      if (errorCheck(errS)) return


    end subroutine CalcWeightingFunctionsInterface


    subroutine CalcDerivdRdA(errS, dimSV_fc, nGauss, iFourier, startlevel, endlevel, geometryS, UD_fc, wfAlbedo_fc)

      ! calculate derivative with respect to surface or cloud albedo
      ! the Lambertian surface or cloud is located at the startlevel

      ! we have dR/dA = (E+td(mu)) * (E+td(mu0)) / (1 - As*)**2
      !               =  downward flux at surface for light incident at mu
      !               *  downward flux at surface for light incident at mu0

      implicit none

      type(errorType), intent(inout) :: errS
      integer,             intent(in)  :: dimSV_fc, nGauss
      integer,             intent(in)  :: iFourier, startlevel, endlevel
      type(geometryType),  intent(in)  :: geometryS
      type(UDType),        intent(in)  :: UD_fc(0:endlevel)

      real(8),             intent(out) :: wfAlbedo_fc

      ! local
      integer :: iGauss, ind, ind0
      real(8) :: dmu  ! diffuse downward flux if the incident direction is mu
      real(8) :: dmu0 ! diffuse downward flux if the incident direction is mu0

      if (iFourier == 0) then
        dmu0 = 0.0d0
        dmu  = 0.0d0
        do iGauss = 1, nGauss
          ind = 1 + (iGauss -1) * dimSV_fc ! index for the Stokes parameter I
          dmu0 = dmu0 + UD_fc(startlevel)%D(ind, 2) * geometryS%w(iGauss)
          dmu  = dmu  + UD_fc(startlevel)%D(ind, 1) * geometryS%w(iGauss)
        end do ! iGauss
        ind  = 1 + nGauss * dimSV_fc
        ind0 = 1 + (nGauss + 1) * dimSV_fc
        wfAlbedo_fc = ( UD_fc(startlevel)%E(ind)  + dmu  ) &
                    * ( UD_fc(startlevel)%E(ind0) + dmu0 )
      else
          wfAlbedo_fc = 0.0d0
      end if

    end subroutine CalcDerivdRdA


    subroutine CalcDerivdRdEmission(errS, dimSV_fc, nGauss, iFourier, endlevel, geometryS, UD_fc, wfEmission_fc)

      ! calculate derivative with respect to isotropic emission at the surface
      ! the radiance for a scattering atmosphere I = mu0 * R * PI * F / PI
      ! where mu0 * PI * F is the solar irradiance per unit area at the top of the atmosphere
      ! Similarly, dR/dEmission has to be multiplied with  E / PI to get the radiance at the top
      ! of the atmosphere due to isotropic emission E at the surface

      ! we have dR/dEmission = (E+td(mu)) / (1 - As*)
      !                      =  downward flux at surface for light incident at mu

      implicit none

      type(errorType), intent(inout) :: errS
      integer,             intent(in)  :: dimSV_fc, nGauss
      integer,             intent(in)  :: iFourier, endlevel
      type(geometryType),  intent(in)  :: geometryS
      type(UDType),        intent(in)  :: UD_fc(0:endlevel)

      real(8),             intent(out) :: wfEmission_fc

      ! local
      integer :: iGauss, ind
      real(8) :: dmu  ! diffuse downward flux if the incident direction is mu

      if (iFourier == 0) then
        dmu  = 0.0d0
        do iGauss = 1, nGauss
          ind = 1 + (iGauss -1) * dimSV_fc          ! index for the Stokes parameter I
          dmu  = dmu  + UD_fc(0)%D(ind, 1) * geometryS%w(iGauss)
        end do ! iGauss
        ind  = 1 + nGauss * dimSV_fc
        wfEmission_fc = ( UD_fc(0)%E(ind)  + dmu  )
      else
        wfEmission_fc = 0.0d0
      end if

    end subroutine CalcDerivdRdEmission


    subroutine CalcDerivdRdkabs(errS, usePseudoSpherical, dimSV_fc, nGauss, startlevel, endlevel, &
                                geometryS, RTMaltitude, UDsumLocal_fc, UD_fc, wfInterfkabs_fc)
                                

      ! Calculate derivative of the Fourier coefficient of the reflectance
      ! w.r.t. the volume absorption coefficient, d2R/dkabs/dz, at the interfaces
      ! for nlos values of the viewing direction and for different axes.
      ! geometry%u is declared in the beginning of this module and filled earlier

      ! correction for spherical geometry added on Jan 9, 2007

      ! modified to add direct term on Febr 18, 2008

      ! modified names on March 09, 2008

      implicit none

      type(errorType), intent(inout) :: errS
      logical,            intent(in) :: usePseudoSpherical
      integer,            intent(in) :: dimSV_fc, nGauss, startlevel, endlevel
      real(8),            intent(in) :: RTMaltitude(0:endLevel)
      type(geometryType), intent(in) :: geometryS
      type(UDLocalType),  intent(in) :: UDsumLocal_fc(0:endLevel)    ! used for pseudo spherical correction
      type(UDType),       intent(in) :: UD_fc(0:endlevel)

      real(8),           intent(out) :: wfInterfkabs_fc(0:endlevel)

      ! local
      real(8), parameter :: REarth = 6371.0d0  ! mean radius of the Earth in km
      integer            :: l, ilevel, iGauss, il, is, ind, imu, ind0, iSV
      real(8)            :: u0, u0_inverse, sum, y_k, y_l
      real(8)            :: factor(dimSV_fc * (nGauss + 2))      

      ! weighting functions vanish below the Lambertian cloud
      if (startlevel > 0) then
        wfInterfkabs_fc(0:startlevel - 1) = 0.0d0
      end if

      ! fill factor ! accounts for DT = q3 D_transposed q3   and UT = q3 U_transposed q3
      factor = 1.0d0
      if ( dimSV_fc > 2 ) then
        do imu = 1, nGauss + 2
          ind = 3 + (imu - 1) * dimSV_fc
          factor(ind) = - factor(ind)
        end do
      end if

      ! The derivative for a plane parallel atmosphere is given by
      ! 
      !   d2R/dkabs/dz = UT [-1/mu] D_tot + (D_tot)T [-1/mu] U
      ! 
      ! where T denotes the transpose, D_tot is the total (direct and diffuse) downward radiation field,
      ! and U is the upward field

      ! For pseudo spherical correction we need to deal with the direct term of UT [-1/mu] D_tot.

      if (usePseudoSpherical) then
        do ilevel = startlevel, endlevel

          ! numerical integration over directions for diffuse incident light
          sum = 0.0d0
          il = 1
          is = 2
          do iGauss = 1, nGauss
            do iSV = 1, dimSV_fc
              ind = iSV + (iGauss -1) * dimSV_fc
              sum = sum - (UD_fc(ilevel)%U(ind,il) * UD_fc(ilevel)%D(ind,is) +    &
                           UD_fc(ilevel)%D(ind,il) * UD_fc(ilevel)%U(ind,is)  )   &
                           * factor(ind) / geometryS%u(iGauss)
            end do ! iSV
          end do ! iGauss

          ! Deal with direct transmission to the top of the atmosphere (direct part of (D_tot)T [-1/mu] U)
          is = 2
          ind = 1 + nGauss * dimSV_fc
          wfInterfkabs_fc(ilevel) = sum   &
              - UD_fc(ilevel)%E(ind) * UD_fc(ilevel)%U(ind, is)  / geometryS%u(nGauss + il)

          ! Consider the direct part of UT [-1/mu] D_tot. Here pseudo spherical correction is needed.
          ! Using E(z,u0) for the attenuation of direct sunlight that reaches the altitude z
          ! and arrives there with the direction u0, we have for the direct part
          !
          ! sum_l ( Transpose{ U_local(z_l)*exp(-[(tau(z_l)-tau(z_k)]/u) } * [-1/u0] * E(z_k,u0) ) 
          !   
          ! where we sum over the levels below k including k itself. U_local is the source function
          ! for the layers for all orders of scattering, and tau(z) is the optical depth of the level at z.
          ! Because the scattering takes place after the incident sunlight light has crossed the thin
          ! layer at z_k, all this light is attenuated by the thin layer with additional absorption.
          !
          ! For a plane-parallel layer we have E(z_k,u0) = exp(-tau(z_k)]/u0), and performing the
          ! transpose yields
          !
          ! sum_l ( U_local(z_l,u0,u)*exp(-[(tau(z_l)-tau(z_k)]/u0) } * [-1/u0] *  exp(-tau(z_k)]/u0 )
          !
          ! and exp(-tau(z_k)]/u0cancels out providing
          !
          ! sum_l ( U_local(z_l,u0,u)*exp(-tau(z_l)/u0) } * [-1/u0]
          !
          ! writing the first order and the multiple scattering separately as
          ! U_local(z_l,u0,u) = U1_local(z_l,u0,u) + Um_local(z_l,u0,u) and using
          ! U1_local(z_l,u0,u) = R_l(u0,u) *  exp(-tau(z_l)]/u ), with R_l the symmetric
          ! reflection of layer l,provides
          !
          !     sum_l ( exp(-tau(z_l)]/u )*R_l(u,u0)*exp(-tau(z_l)/u0) } * [-1/u0] 
          !   + sum_l ( Um_local(z_l,u0,u)*exp(-tau(z_l)/u0) } * [-1/u0] 
          !
          ! The first term is he familiar single scattering contribution. The second term
          ! contains Um_local(z_l,u0,u) which is light reflected by layer l and transmitted
          ! by layer l+1 when these layers are difusely illuminated themselves. Hence,
          ! pseudo spherical correction is not needed for Um_local(z_l,u0,u) nor for
          ! exp(-tau(z_l)]/u, as long as u is not too small. Thus, we can combine the
          ! single and multiple scattered light and write
          !
          !     sum_l ( U_local(z_l,u0,u)*exp(-tau(z_l)/u0) } * [-1/u0] 
          !
          ! Pseudo spherical correction is obtained if
          !   - The attenuation exp(-tau(z_l)/u0) is replaced by the attenuation in a
          !     spherical shell atmosphere.
          !   - The slant path for the thin layer becomes
          !      y_k/sqrt( y_k**2 - y_l**2 * (1.0d0 - u0**2) ) where y_l = REarth + z_l.

          u0 = geometryS%u(nGauss + is)
          y_k = REarth + RTMaltitude(ilevel)
          sum = 0.0d0
          do l = ilevel, startlevel, -1    ! sum over levels equal to or below the level with additional absorption
            ! slant attenuation from level l to the TOA
            y_l = REarth + RTMaltitude(l)
            ! u0_inverse is the slant path in the thin layer at level k when light arrives at level l under an angle u0
            u0_inverse = y_k/sqrt( abs(y_k**2 - y_l**2 * (1.0d0 - u0**2)) )
            ind = (nGauss + 1) * dimSV_fc + 1
            il = 1
            sum = sum + UDsumLocal_fc(l)%U(ind, il) * UD_fc(l)%E(ind) * u0_inverse
          end do
          wfInterfkabs_fc(ilevel) = wfInterfkabs_fc(ilevel) - sum
        end do ! ilevel
      else
        ! no spherical correction used

        do ilevel = startlevel, endlevel
          sum = 0.0d0
          il = 1
          is = 2
          do iGauss = 1, nGauss
            do iSV = 1, dimSV_fc
              ind = iSV + (iGauss -1) * dimSV_fc
              sum = sum - (UD_fc(ilevel)%U(ind,il) * UD_fc(ilevel)%D(ind,is) +    &
                           UD_fc(ilevel)%D(ind,il) * UD_fc(ilevel)%U(ind,is)  )   &
                           * factor(ind) / geometryS%u(iGauss)
            end do ! iSV
          end do ! iGauss
          ! account for the direct transmission:
          ! first term: attenuation of the direct solar beam when it travels across the interface
          ! second term: attenuation of the upward traveling light that goes directly to the detector
          ind  = nGauss * dimSV_fc + 1
          ind0 = (nGauss + 1) * dimSV_fc + 1
          is = 2
          il = 1
          wfInterfkabs_fc(ilevel) = sum                                                      &
              - UD_fc(ilevel)%U(ind0, il) * UD_fc(ilevel)%E(ind0) / geometryS%u(nGauss + 2)  &
              - UD_fc(ilevel)%U(ind , is) * UD_fc(ilevel)%E(ind ) / geometryS%u(nGauss + 1)          
        end do ! ilevel
      end if ! usePseudoSpherical

    end subroutine CalcDerivdRdkabs


    subroutine CalcDerivdRdksca(errS, fcCoef, dimSV, dimSV_fc, nGauss, iFourier, nmutot,  &
                                OneOverFourMuMu0, startlevel, endlevel, geometryS,        &
                                controlS, optPropRTMGridS, UD_fc, wfInterfkabs_fc,        &
                                wfInterfkscaGas_fc, wfInterfkscaAerAbove_fc,              &
                                wfInterfkscaAerBelow_fc,                                  &
                                wfInterfkscaCldAbove_fc, wfInterfkscaCldBelow_fc)

      ! Calculate derivative of the Fourier coefficient of the reflectance
      ! w.r.t. the volume scattering coefficient, d2R/dksca/dz, at the interfaces

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                  intent(in)    :: dimSV, dimSV_fc, nGauss, iFourier
      integer,                  intent(in)    :: nmutot, startlevel, endlevel
      real(8),                  intent(in)    :: OneOverFourMuMu0(nmutot,nmutot)
      type(geometryType),       intent(in)    :: geometryS
      type(controlType),        intent(in)    :: controlS
      type(optPropRTMGridType), intent(in)    :: optPropRTMGridS
      type(UDType),             intent(in)    :: UD_fc(0:endlevel)
      type(fc_coefficients),    intent(in)    :: fcCoef(0:optPropRTMGridS%maxExpCoef)

      real(8),                  intent(inout) :: wfInterfkabs_fc(0:endlevel)
      real(8),                  intent(out)   :: wfInterfkscaGas_fc(0:endlevel)
      real(8),                  intent(out)   :: wfInterfkscaAerAbove_fc(0:endlevel)
      real(8),                  intent(out)   :: wfInterfkscaAerBelow_fc(0:endlevel)
      real(8),                  intent(out)   :: wfInterfkscaCldAbove_fc(0:endlevel)
      real(8),                  intent(out)   :: wfInterfkscaCldBelow_fc(0:endlevel)

      ! local
      integer :: ilevel, il, is, iSV, jSV, ind, ind0, imu, imu0
      real(8) :: sum
      real(8) :: Zplus (dimSV_fc*nmutot, dimSV_fc*nmutot), Zmin (dimSV_fc*nmutot, dimSV_fc*nmutot)
      real(8) :: Pplusplus (dimSV_fc*nmutot, dimSV_fc*nmutot), Pminplus (dimSV_fc*nmutot, dimSV_fc*nmutot)
      real(8) :: Pminmin   (dimSV_fc*nmutot, dimSV_fc*nmutot), Pplusmin (dimSV_fc*nmutot, dimSV_fc*nmutot)
      real(8) :: PplusplusED(dimSV_fc*nmutot), PminplusED(dimSV_fc*nmutot)
      real(8) :: PminminU   (dimSV_fc*nmutot), PplusminU (dimSV_fc*nmutot)
      real(8) :: UT(dimSV_fc*nmutot,2), DT(dimSV_fc*nmutot,2)

      ! parameters for aerosol and cloud
      real(8) :: distanceBaseFitInterval      
      real(8) :: distanceTopFitInterval

      ! calculate for fit interval or for all altitudes above the Lambertian cloud
      logical, parameter  :: calculate_only_fit_interval = .true.

      character(LEN=50) :: string      

      ! weighting functions vanish below the Lambertian cloud
      if (startlevel > 0) then
        wfInterfkscaGas_fc(0:startlevel - 1) = 0.0d0
        wfInterfkscaAerAbove_fc(0:startlevel - 1) = 0.0d0
        wfInterfkscaAerBelow_fc(0:startlevel    ) = 0.0d0
        wfInterfkscaCldAbove_fc(0:startlevel - 1) = 0.0d0
        wfInterfkscaCldBelow_fc(0:startlevel    ) = 0.0d0
      end if

      ! RAYLEIGH scattering

      ! initialize
      wfInterfkscaGas_fc = 0.0d0

      if ( iFourier < 3 ) then ! Only Fourier coefficients 0, 1, and 2 contribute for gas
        ! wfInterfkscaGas_fc is only used for fitting the surface pressure,
        ! hence, it is sufficient to calculate it only for the startlevel
        do ilevel = startlevel, startlevel

          string = trim('derivative scattering gas')
          call fillZplusZmin(errS, fcCoef, iFourier, optPropRTMGridS%maxExpCoefGas, dimSV, dimSV_fc, nmutot, &
                             optPropRTMGridS%phasefCoefGas(:,:,:,ilevel), geometryS, Zplus, Zmin, string)
          if (errorCheck(errS)) return
 
          ! calculate effective phase matrices, Pplusplus and Pminplus
          do imu0 = 1, nmutot
            do jSV = 1, dimSV_fc
              ind0 = jSV + (imu0 - 1) * dimSV_fc
              do imu = 1, nmutot
                do iSV = 1, dimSV_fc
                   ind = iSV + (imu - 1) * dimSV_fc
                   Pplusplus(ind, ind0) = OneOverFourMuMu0(imu, imu0) * Zplus(ind, ind0)
                   Pminplus (ind, ind0) = OneOverFourMuMu0(imu, imu0) * Zmin (ind, ind0)
                end do ! iSV
              end do ! imu
            end do ! jSV
          end do ! nmu0

          if ( dimSV_fc > 1 ) then
            Pminmin  = transform_top_bottom(dimSV_fc, nmutot, Pplusplus)
            Pplusmin = transform_top_bottom(dimSV_fc, nmutot, Pminplus)
          else
            Pminmin  = Pplusplus
            Pplusmin = Pminplus
          end if

          ! calculate PplusplusED, PminplusED, PminminU, PplusminU
          is = 2
          iSV = 1
          ind0 = iSV + (nGauss +1) * dimSV_fc
          PplusplusED(:) = matmul( Pplusplus(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                         + Pplusplus(:, ind0) * UD_fc(ilevel)%E(ind0)
          PminplusED (:) = matmul( Pminplus (:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                         + Pminplus (:, ind0) * UD_fc(ilevel)%E(ind0)
          PminminU   (:) = matmul( Pminmin(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))
          PplusminU  (:) = matmul( Pplusmin (:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))


          UT = apply_q3(dimSV_fc, nmutot, UD_fc(ilevel)%U)
          DT = apply_q4(dimSV_fc, nmutot, UD_fc(ilevel)%D)

          ! calculate derivative
          il = 1
          sum = dot_product( DT(1:dimSV_fc*nGauss,il),                                      &
                             PminplusED (1:dimSV_fc*nGauss) + PminminU(1:dimSV_fc*nGauss))  &
              + dot_product( UT(1:dimSV_fc*nGauss,il),                                      &
                             PplusplusED(1:dimSV_fc*nGauss) + PplusminU (1:dimSV_fc*nGauss))
          ! direct transmission
          iSV = 1
          ind = iSV + nGauss * dimSV_fc
          sum = sum + UD_fc(ilevel)%E(ind) * (PminplusED(ind) + PminminU(ind))
          wfInterfkscaGas_fc(ilevel) = sum
        end do ! ilevel

        ! above we ignored the attenuation of light when it travels across the boundary
        ! which is equal to the derivative d2R/dkabs/dz. If required, calculate that value
        ! Note that spherical correction is only required for light that travels across the interface

        wfInterfkscaGas_fc = wfInterfkscaGas_fc + wfInterfkabs_fc

      end if ! iFourier < 3

      ! AEROSOL
      
      do ilevel = startlevel, endlevel

        if ( calculate_only_fit_interval ) then

          ! perform calculations only for the fit interval
          distanceBaseFitInterval = optPropRTMGridS%RTMaltitude(ilevel) - optPropRTMGridS%lowerBoundFitInterval
          distanceTopFitInterval  = optPropRTMGridS%RTMaltitude(ilevel) - optPropRTMGridS%upperBoundFitInterval

        else

          ! perform calculations for all altitudes from startlevel to endlevel
          distanceBaseFitInterval =  0.0d0
          distanceTopFitInterval  = -1.0d0

        end if ! calculate_only_fit_interval

        if ( ( distanceBaseFitInterval > - 1.0d-5 ) .and. ( distanceTopFitInterval < - 1.0d-5 ) ) then

          string = trim('derivative scattering aerosol - above')
          call fillZplusZmin(errS, fcCoef, iFourier, optPropRTMGridS%maxExpCoefAer, dimSV, dimSV_fc, nmutot, &
                             optPropRTMGridS%phasefCoefAerAbove(:,:,:,ilevel), geometryS, Zplus, Zmin, &
                             string)
          if (errorCheck(errS)) return

          ! calculate effective phase matrices, Pplusplus and Pminplus
          do imu0 = 1, nmutot
            do jSV = 1, dimSV_fc
              ind0 = jSV + (imu0 - 1) * dimSV_fc
              do imu = 1, nmutot
                do iSV = 1, dimSV_fc
                  ind = iSV + (imu - 1) * dimSV_fc
                  Pplusplus(ind, ind0) = OneOverFourMuMu0(imu, imu0) * Zplus(ind, ind0)
                  Pminplus (ind, ind0) = OneOverFourMuMu0(imu, imu0) * Zmin (ind, ind0)
                end do ! iSV
              end do ! imu
            end do ! jSV
          end do ! nmu0

          if ( dimSV_fc > 1 ) then
            Pminmin  = transform_top_bottom(dimSV_fc, nmutot, Pplusplus)
            Pplusmin = transform_top_bottom(dimSV_fc, nmutot, Pminplus)
          else
            Pminmin  = Pplusplus
            Pplusmin = Pminplus
          end if

          ! calculate PplusplusED, PminplusED, PminminU, PplusminU
          is = 2
          iSV = 1
          ind0 = iSV + (nGauss +1) * dimSV_fc
          PplusplusED(:) = matmul( Pplusplus(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                     + Pplusplus(:, ind0) * UD_fc(ilevel)%E(ind0)
          PminplusED (:) = matmul( Pminplus (:,1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                     + Pminplus (:, ind0) * UD_fc(ilevel)%E(ind0)
          PminminU   (:) = matmul( Pminmin(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))
          PplusminU  (:) = matmul( Pplusmin (:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))

          ! calculate UT and DT
          UT = apply_q3(dimSV_fc, nmutot, UD_fc(ilevel)%U)
          DT = apply_q4(dimSV_fc, nmutot, UD_fc(ilevel)%D)

          ! calculate derivative
          il = 1
          sum = dot_product( DT(1:dimSV_fc*nGauss,il),                         &
                             PminplusED (1:dimSV_fc*nGauss) + PminminU(1:dimSV_fc*nGauss))  &
              + dot_product( UT(1:dimSV_fc*nGauss,il),                      &
                             PplusplusED(1:dimSV_fc*nGauss) + PplusminU (1:dimSV_fc*nGauss))
          ! direct transmission
          iSV = 1
          ind = iSV + nGauss * dimSV_fc
          sum = sum + UD_fc(ilevel)%E(ind) * (PminplusED(ind) + PminminU(ind))

          ! add attenuation term (equal to absorption term)
          sum = sum + wfInterfkabs_fc(ilevel)
          wfInterfkscaAerAbove_fc(ilevel) = sum

        else

          ! outside fit interval
          wfInterfkscaAerAbove_fc(ilevel) = 0.0d0

        end if

      end do ! ilevel

        
      do ilevel = startlevel, endlevel

        ! perform calculations only for the fit interval
        distanceBaseFitInterval = optPropRTMGridS%RTMaltitude(ilevel) - optPropRTMGridS%lowerBoundFitInterval
        distanceTopFitInterval  = optPropRTMGridS%RTMaltitude(ilevel) - optPropRTMGridS%upperBoundFitInterval

        if ( ( distanceBaseFitInterval >  1.0d-5 ) .and. ( distanceTopFitInterval < 1.0d-5 ) ) then

          string = trim('derivative scattering aerosol - below')
          call fillZplusZmin(errS, fcCoef, iFourier, optPropRTMGridS%maxExpCoefAer, dimSV, dimSV_fc, nmutot, &
                             optPropRTMGridS%phasefCoefAerBelow(:,:,:,ilevel), geometryS, Zplus, Zmin, &
                             string)
          if (errorCheck(errS)) return

          ! calculate effective phase matrices, Pplusplus and Pminplus
          do imu0 = 1, nmutot
            do jSV = 1, dimSV_fc
              ind0 = jSV + (imu0 - 1) * dimSV_fc
              do imu = 1, nmutot
                do iSV = 1, dimSV_fc
                  ind = iSV + (imu - 1) * dimSV_fc
                  Pplusplus(ind, ind0) = OneOverFourMuMu0(imu, imu0) * Zplus(ind, ind0)
                  Pminplus(ind, ind0)  = OneOverFourMuMu0(imu, imu0) * Zmin (ind, ind0)
                end do ! iSV
              end do ! imu
            end do ! jSV
          end do ! nmu0

          Pminmin  = transform_top_bottom(dimSV_fc, nmutot, Pplusplus)
          Pplusmin = transform_top_bottom(dimSV_fc, nmutot, Pminplus)

          ! calculate PplusplusED, PminplusED, PminminU, PplusminU
          is = 2
          iSV = 1
          ind0 = iSV + (nGauss +1) * dimSV_fc
          PplusplusED(:) = matmul( Pplusplus(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                         + Pplusplus(:, ind0) * UD_fc(ilevel)%E(ind0)
          PminplusED (:) = matmul( Pminplus (:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                         + Pminplus (:, ind0) * UD_fc(ilevel)%E(ind0)
          PminminU   (:) = matmul( Pminmin(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))
          PplusminU  (:) = matmul( Pplusmin (:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))

          ! calculate UT and DT
          UT = apply_q3(dimSV_fc, nmutot, UD_fc(ilevel)%U)
          DT = apply_q4(dimSV_fc, nmutot, UD_fc(ilevel)%D)

          ! calculate derivative

          il = 1
          sum = dot_product( DT(1:dimSV_fc*nGauss,il),                         &
                             PminplusED (1:dimSV_fc*nGauss) + PminminU(1:dimSV_fc*nGauss))  &
              + dot_product( UT(1:dimSV_fc*nGauss,il),                         &
                             PplusplusED(1:dimSV_fc*nGauss) + PplusminU (1:dimSV_fc*nGauss))
          ! direct transmission
          iSV = 1
          ind = iSV + nGauss * dimSV_fc
          sum = sum + UD_fc(ilevel)%E(ind) * (PminplusED(ind) + PminminU(ind))

          ! add attenuation term (equal to absorption term)
          sum = sum + wfInterfkabs_fc(ilevel)
          wfInterfkscaAerBelow_fc(ilevel) = sum

        else

          ! outside fit interval
          wfInterfkscaAerBelow_fc(ilevel) = 0.0d0

        end if  

      end do ! ilevel

      ! SCATTERING CLOUD

      ! skip cloud when aerosolLayerHeight is .true. as we assume there are no clouds
      if ( controlS%aerosolLayerHeight ) then
        wfInterfkscaCldAbove_fc(:) = 0.0d0
        wfInterfkscaCldBelow_fc(:) = 0.0d0
        return
      end if
      
      do ilevel = startlevel, endlevel

        ! perform calculations only for the fit interval
        distanceBaseFitInterval = optPropRTMGridS%RTMaltitude(ilevel) - optPropRTMGridS%lowerBoundFitInterval
        distanceTopFitInterval  = optPropRTMGridS%RTMaltitude(ilevel) - optPropRTMGridS%upperBoundFitInterval

        if ( ( distanceBaseFitInterval > - 1.0d-5 ) .and. ( distanceTopFitInterval <  -1.0d-5 ) ) then


          string = trim('derivative scattering cloud - above')
          call fillZplusZmin(errS, fcCoef, iFourier, optPropRTMGridS%maxExpCoefCld, dimSV, dimSV_fc, nmutot, &
                             optPropRTMGridS%phasefCoefCldAbove(:,:,:,ilevel), geometryS, Zplus, Zmin, &
                             string)
          if (errorCheck(errS)) return

          ! calculate effective phase matrices, Pplusplus and Pminplus
          do imu0 = 1, nmutot
            do jSV = 1, dimSV_fc
              ind0 = jSV + (imu0 - 1) * dimSV_fc
              do imu = 1, nmutot
                do iSV = 1, dimSV_fc
                  ind = iSV + (imu - 1) * dimSV_fc
                  Pplusplus(ind, ind0) = OneOverFourMuMu0(imu, imu0) * Zplus(ind, ind0)
                  Pminplus(ind, ind0)  = OneOverFourMuMu0(imu, imu0) * Zmin (ind, ind0)
                end do ! iSV
              end do ! imu
            end do ! jSV
          end do ! nmu0

          Pminmin  = transform_top_bottom(dimSV_fc, nmutot, Pplusplus)
          Pplusmin = transform_top_bottom(dimSV_fc, nmutot, Pminplus)

          ! calculate PplusplusED, PminplusED, PminminU, PplusminU

          is = 2
          iSV = 1
          ind0 = iSV + (nGauss +1) * dimSV_fc
          PplusplusED(:) = matmul( Pplusplus(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                     + Pplusplus(:, ind0) * UD_fc(ilevel)%E(ind0)
          PminplusED (:) = matmul( Pminplus (:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                     + Pminplus (:, ind0) * UD_fc(ilevel)%E(ind0)
          PminminU   (:) = matmul( Pminmin(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))
          PplusminU  (:) = matmul( Pplusmin (:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))

          ! calculate transposed internal fields UT and DT taking into account minus signs for polarization 
          UT = apply_q3(dimSV_fc, nmutot, UD_fc(ilevel)%U)
          DT = apply_q4(dimSV_fc, nmutot, UD_fc(ilevel)%D)

          ! calculate derivative
          il = 1
          sum = dot_product( DT(1:dimSV_fc*nGauss,il),                         &
                             PminplusED (1:dimSV_fc*nGauss) + PminminU(1:dimSV_fc*nGauss))  &
              + dot_product( UT(1:dimSV_fc*nGauss,il),                         &
                             PplusplusED(1:dimSV_fc*nGauss) + PplusminU (1:dimSV_fc*nGauss))
          ! direct transmission
          iSV = 1
          ind = iSV + nGauss * dimSV_fc
          sum = sum + UD_fc(ilevel)%E(ind) * (PminplusED(ind) + PminminU(ind))

          ! add attenuation term (equal to absorption term)
          sum = sum + wfInterfkabs_fc(ilevel)
          wfInterfkscaCldAbove_fc(ilevel) = sum

        else

          ! outside fit interval
          wfInterfkscaCldAbove_fc(ilevel) = 0.0d0

        end if  

      end do ! ilevel

    
      do ilevel = startlevel, endlevel

        ! perform calculations only for the fit interval
        distanceBaseFitInterval = optPropRTMGridS%RTMaltitude(ilevel) - optPropRTMGridS%lowerBoundFitInterval
        distanceTopFitInterval  = optPropRTMGridS%RTMaltitude(ilevel) - optPropRTMGridS%upperBoundFitInterval

        if ( ( distanceBaseFitInterval >  1.0d-5 ) .and. ( distanceTopFitInterval < 1.0d-5 ) ) then

          string = trim('derivative scattering cloud - below')
          call fillZplusZmin(errS, fcCoef, iFourier, optPropRTMGridS%maxExpCoefCld, dimSV, dimSV_fc, nmutot, &
                             optPropRTMGridS%phasefCoefCldBelow(:,:,:,ilevel), geometryS, Zplus, Zmin, &
                             string)
          if (errorCheck(errS)) return

          ! calculate effective phase matrices, Pplusplus and Pminplus
          do imu0 = 1, nmutot
            do jSV = 1, dimSV_fc
              ind0 = jSV + (imu0 - 1) * dimSV_fc
              do imu = 1, nmutot
                do iSV = 1, dimSV_fc
                  ind = iSV + (imu - 1) * dimSV_fc
                  Pplusplus(ind, ind0) = OneOverFourMuMu0(imu, imu0) * Zplus(ind, ind0)
                  Pminplus(ind, ind0)  = OneOverFourMuMu0(imu, imu0) * Zmin (ind, ind0)
                end do ! iSV
              end do ! imu
            end do ! jSV
          end do ! nmu0

          Pminmin  = transform_top_bottom(dimSV_fc, nmutot, Pplusplus)
          Pplusmin = transform_top_bottom(dimSV_fc, nmutot, Pminplus)

          ! calculate PplusplusED, PminplusED, PminminU, PplusminU
          is = 2
          iSV = 1
          ind0 = iSV + (nGauss +1) * dimSV_fc
          PplusplusED(:) = matmul( Pplusplus(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                     + Pplusplus(:, ind0) * UD_fc(ilevel)%E(ind0)
          PminplusED (:) = matmul( Pminplus (:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%D(1:dimSV_fc*nGauss,is)) &
                     + Pminplus (:, ind0) * UD_fc(ilevel)%E(ind0)
          PminminU   (:) = matmul( Pminmin(:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))
          PplusminU  (:) = matmul( Pplusmin (:, 1:dimSV_fc*nGauss), UD_fc(ilevel)%U(1:dimSV_fc*nGauss,is))

          ! calculate transposed internal fields UT and DT taking into account minus signs for polarization 
          UT = apply_q3(dimSV_fc, nmutot, UD_fc(ilevel)%U)
          DT = apply_q4(dimSV_fc, nmutot, UD_fc(ilevel)%D)

          ! calculate derivative
          il = 1
          sum = dot_product( DT(1:dimSV_fc*nGauss,il),                         &
                             PminplusED (1:dimSV_fc*nGauss) + PminminU(1:dimSV_fc*nGauss))  &
              + dot_product( UT(1:dimSV_fc*nGauss,il),                         &
                             PplusplusED(1:dimSV_fc*nGauss) + PplusminU (1:dimSV_fc*nGauss))
          ! direct transmission
          iSV = 1
          ind = iSV + nGauss * dimSV_fc
          sum = sum + UD_fc(ilevel)%E(ind) * (PminplusED(ind) + PminminU(ind))

          ! add attenuation term (equal to absorption term)
          sum = sum + wfInterfkabs_fc(ilevel)
          wfInterfkscaCldBelow_fc(ilevel) = sum

        else

          ! outside fit interval
          wfInterfkscaCldBelow_fc(ilevel) = 0.0d0

        end if  

      end do ! ilevel

    end subroutine CalcDerivdRdksca


    function smul(nmutot, nGauss, thresholdMul, a, b)

    ! supermatrix multiplication

      implicit none
      integer,                           intent(in)  :: nmutot, nGauss
      real(8),                           intent(in)  :: thresholdMul
      real(8), dimension(nmutot,nmutot), intent(in)  :: a, b
      real(8), dimension(nmutot,nmutot)              :: smul

      integer :: k, i, j
      real(8) :: Tra, Trb, Trab

      ! initial output to zero
      smul = 0.0d0

      ! calculate traces of the input matrices
      Tra = 0.0d0
      Trb = 0.0d0
      do k = 1, nGauss
        Tra = Tra + a(k,k)     
        Trb = Trb + b(k,k)                
      end do
      Trab = abs(Tra * Trb)

      ! fill all values
      if (Trab >  thresholdMul) then

        do j = 1, nmutot
          do i = 1, nmutot
            smul(i,j)=0.d0
          end do ! i
         do k = 1, nGauss
            do i= 1, nmutot
               smul(i,j) = smul(i,j) + a(i,k) * b(k,j)
            end do ! i
          end do ! k
        end do ! j

      end if ! Trab >  thresholdMul

    end function smul


    function esmul(nmutot, e, a)

    ! diagonal times supermatrix

      implicit none

      integer,                           intent(in) :: nmutot
      real(8), dimension(nmutot),        intent(in) :: e
      real(8), dimension(nmutot,nmutot), intent(in) :: a
      real(8), dimension(nmutot,nmutot)             :: esmul

      integer :: imu0, imu

      do imu0 = 1, nmutot
        do imu = 1, nmutot
          esmul(imu, imu0) = e(imu)*a(imu, imu0)
        end do
      end do

    end function esmul


    function semul(nmutot, a, e)

    ! supermatrix times diagonal

      implicit none

      integer,                           intent(in) :: nmutot
      real(8), dimension(nmutot),        intent(in) :: e
      real(8), dimension(nmutot,nmutot), intent(in) :: a
      real(8), dimension(nmutot,nmutot)             :: semul

      integer :: imu, imu0

      do imu0 = 1, nmutot
        do imu = 1, nmutot
          semul(imu, imu0) = a(imu, imu0) * e(imu0)
        end do
      end do

    end function semul


    function Qseries(errS, nmutot, nGauss, thresholdMul, a, b)

    ! evaluate repeated reflections: a*b + a*b*a*b + .......
    ! by using LU decoposition to find the inverse
    ! see Appendix F in "Transfer of polarized light in
    ! Planetary atmospheres", J.W. Hovenier, C.V.M. van
    ! der Mee, and H. Domke. 2004, Kluwer Academic Pobl.
    ! pp. 258

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                           intent(in)  :: nmutot, nGauss
      real(8), dimension(nmutot,nmutot), intent(in)  :: a, b
      real(8),                           intent(in)  :: thresholdMul
      real(8), dimension(nmutot,nmutot)              :: Qseries, ab

      ! local
      integer :: i, j, k
      integer :: indx(nGauss)     ! index that keeps count of changes due to pivots
      integer :: status_LUdecomp  ! status of LU decomposition
      real(8) :: d                ! odd(-1) or even(+1) number of row interchanges
      real(8) :: col(nGauss)
      real(8) :: Trab

      real(8), parameter :: ThresholdQ = 1.0d-3

      real(8), dimension(          nGauss,          nGauss) :: ab_gg, one_minus_ab_gg
      real(8), dimension(          nGauss,          nGauss) :: one, inverse
      real(8), dimension( nmutot - nGauss,          nGauss) :: ab_ag, tmp
      real(8), dimension(          nGauss, nmutot - nGauss) :: ab_ga
      real(8), dimension( nmutot - nGauss, nmutot - nGauss) :: ab_aa

      logical, parameter :: verbose = .false.


      ab =  smul(nmutot, nGauss, thresholdMul, a, b)

      ! calculate trace
      Trab = 0.0d0
      do k = 1, nGauss
        Trab = Trab + ab(k,k)     
      end do

      if (verbose) then
        write(intermediateFileUnit,*) 'ab'
        do i = 1, nmutot
          write(intermediateFileUnit,'(10E20.7)')  (ab(i,j), j = 1, nGauss)
        end do
      end if

      ! one term of the Q series suffices if Trace(ab) < ThresholdQ
      if ( abs(Trab) < ThresholdQ ) then
        Qseries = ab
        return
      end if

      ! more terms are needed: determine the inverse
      ! identify the varous sections      
      ab_gg = ab(        1:nGauss,        1:nGauss )
      ab_ag = ab(nGauss +1:nmutot,        1:nGauss )
      ab_ga = ab(        1:nGauss,nGauss +1:nmutot)   
      ab_aa = ab(nGauss +1:nmutot,nGauss +1:nmutot)   

      ! find the inverse of 1 - ab_gg

      one = 0.0d0
      do k = 1, nGauss
        one(k,k) = 1.0d0
      end do
      one_minus_ab_gg = one - ab_gg

      ! one_minus_ab_gg will be overwritten by its LU decomposed array
      call LU_decomposition(errS,  one_minus_ab_gg, indx, d, status_LUdecomp)
      if (errorCheck(errS)) return

      do k = 1, nGauss
        col(1:nGauss) = one(1:nGauss, k)
        call solve_lin_system_LU_based(errS, one_minus_ab_gg, indx, col)
        if (errorCheck(errS)) return
        inverse(1:nGauss, k) = col(1:nGauss)
      end do

      ! initialize
      Qseries = 0.0d0

      Qseries(1:nGauss,1:nGauss ) = inverse - one

      Qseries(1:nGauss, nGauss +1:nmutot) = matmul(inverse, ab_ga)

      tmp = matmul(ab_ag, inverse)
      Qseries(nGauss +1:nmutot, 1:nGauss ) = tmp

      Qseries(nGauss+1:nmutot, nGauss+1:nmutot) &
        = matmul(tmp(1:nmutot-nGauss, 1:nGauss), ab_ga(1:nGauss,1:nmutot-nGauss)) &
        + ab_aa(1:nmutot-nGauss, 1:nmutot-nGauss)

      if (verbose) then
        write(intermediateFileUnit,*) 'Qseries'
        do i = 1, nmutot
          write(intermediateFileUnit,'(10E20.7)')  (Qseries(i,j), j = 1, nmutot)
        end do
      end if

    end function Qseries


    subroutine singleScattering(errS, &
        optPropRTMGridS,        & ! optical properties at RTM grids
        controlS,               & ! control parameters
        geometryS,              & ! geometrical parameters
        albedo,                 & ! albedo of the Lambertian surface at the level RTMnlevelCloud
                                  ! which is the surface albedo if RTMnlevelCloud = 0
        RTMnlevelCloud,         & ! optPropRTMGridS%RTMaltitude(RTMnlevelCloud) is altitude of Lambertian cloud
        babs,                   & ! absorption optical thickness
        bsca,                   & ! scattering optical thickness
        refl,                   & ! reflectance
        contribRefl,            & ! path radiance profile
        wfInterfkscaGas,        & ! weighting function for scattering by molecules
        wfInterfkscaAerAbove,   & ! weighting function for scattering by aerosol; limit for approach from above
        wfInterfkscaAerBelow,   & ! weighting function for scattering by aerosol; limit for approach from below
        wfInterfkscaCldAbove,   & ! weighting function for scattering by cloud; limit for approach from above
        wfInterfkscaCldBelow,   & ! weighting function for scattering by cloud; limit for approach from below
        wfInterfkabs,           & ! weighting function for absorption (the same for different types of absorption)
        wfAlbedo,               & ! weighting function for the (surface/cloud) albedo
        wfEmission,             & ! weighting function for the surface emission
        status)

        implicit none

        !INPUT
      type(errorType), intent(inout) :: errS
        type(optPropRTMGridType),    intent(in) :: optPropRTMGridS
        type(controlType),           intent(in) :: controlS
        type(geometryType),          intent(in) :: geometryS 

        integer,                     intent(in) :: RTMnlevelCloud

        real(8),                     intent(in) :: albedo

        ! OUTPUT
        real(8),  intent(out) :: bsca, babs  ! scattering and absorption (vertical) optical thickness

        ! sun-normalized radiances

        real(8),  intent(out) :: refl(controlS%dimSV)

        ! contribution function or altitude resolved path radiance, such that the integral over
        ! the altitude gives the sun-normalized reflectance except for the direct radiance that
        ! has been reflected by the surface (or Lambertian cloud surface).

        real(8),  intent(out) :: contribRefl(controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)

        ! Weighting functions w.r.t. atmospheric parameters at the interfaces between the layers
        ! As the aerosol and cloud properties can be discontineous between layers, the weighting
        ! function can be discontineous. This happens if the phase function differs between layers.
        ! Therefore, we calculate the limits when the interface is approached from above and below.
        ! Note that for absorption and extinction the weighting functions for trace gas,
        ! aerosol and cloud are all the same. Absorption here can also be read as extinction.
        real(8),  intent(out) :: wfInterfkscaGas(0:optPropRTMGridS%RTMnlayer)
        real(8),  intent(out) :: wfInterfkscaAerAbove(0:optPropRTMGridS%RTMnlayer)
        real(8),  intent(out) :: wfInterfkscaAerBelow(0:optPropRTMGridS%RTMnlayer)
        real(8),  intent(out) :: wfInterfkscaCldAbove(0:optPropRTMGridS%RTMnlayer)
        real(8),  intent(out) :: wfInterfkscaCldBelow(0:optPropRTMGridS%RTMnlayer)
        real(8),  intent(out) :: wfInterfkabs(0:optPropRTMGridS%RTMnlayer)

        ! Weighting function for Lambertian ground surface albedo or Lamvertian cloud albedo
        real(8),  intent(out) :: wfAlbedo

        ! Weighting function for surface emission
        real(8),  intent(out) :: wfEmission ! Weighting function for surface emission

        integer,  intent(out) :: status


        real(8)      :: atten (geometryS%nmutot, 0:optPropRTMGridS%RTMnlayer, 0:optPropRTMGridS%RTMnlayer)

        ! reflection and transmission properties for individual atmospheric layers
        ! (surface is layer with index 0; Lambertian cloud has index RTMnlevelCloud)
        type(RTType) :: RT_fc(0:optPropRTMGridS%RTMnlayer)

        ! Fourier coefficients global internal fields at the interfaces for the axes
        type(UDType) :: UD_fc(0:optPropRTMGridS%RTMnlayer)

        ! storage for orders of scattering for the layers
        type(UDType) :: UDorde_fc(0:optPropRTMGridS%RTMnlayer)

        ! local internal fields (equivalent of source function, but for the layers)
        ! these are the result of scattering by a certain layer at the interfaces of that layer
        type(UDLocalType) :: UDLocal_fc(0:optPropRTMGridS%RTMnlayer)

        ! sum of the orders of scattering for the layers
        type(UDLocalType) :: UDsumLocal_fc(0:optPropRTMGridS%RTMnlayer)

        ! Fourier coefficients used for summation of the Fourier coefficients
        real(8) :: refl_fc(controlS%dimSV)
        real(8) :: wfAlbedo_fc, wfEmission_fc

        real(8) :: wfInterfkscaGas_fc(0:optPropRTMGridS%RTMnlayer)
        real(8) :: wfInterfkscaAerAbove_fc(0:optPropRTMGridS%RTMnlayer)
        real(8) :: wfInterfkscaAerBelow_fc(0:optPropRTMGridS%RTMnlayer)
        real(8) :: wfInterfkscaCldAbove_fc(0:optPropRTMGridS%RTMnlayer)
        real(8) :: wfInterfkscaCldBelow_fc(0:optPropRTMGridS%RTMnlayer)
        real(8) :: wfInterfkabs_fc(0:optPropRTMGridS%RTMnlayer)

        ! contribution to reflectance of a thin layer: dR/dz
        ! note that this differs from the change in reflectance if a thin layer is added
        ! to the atmosphere at an altitude z, because the scattering is non-linear
        ! in the amount of scattering and absorbing material
        ! The integral from 0 to TOA of the contribution function gives the path radiance
        ! The reflectance is the path radiance plus the direct surface contribution exp(-tau/mu)U(0,mu,mu0,dphi)
        real(8) :: contribRefl_fc(controlS%dimSV, 0:optPropRTMGridS%RTMnlayer)

        real(8) :: cos_m_dphi         ! cosine( m * azimuth diff)
        real(8) :: sin_m_dphi         ! sine  ( m * azimuth diff)
        real(8) :: cs_m_dphi          ! cosine or sine ( m * azimuth diff)
        real(8) :: factor 
        integer :: iFourier
        integer :: ilayer, dimSV, dimSV_fc, nmutot, nmuextra, nGauss, iSV

        integer :: RTMnlayer, maxExpCoef, FourierMax
        type(fc_coefficients), pointer  :: fcCoef(:)

        ! Assume proper processing
        status = 0

        ! initialize output to zero
        ! later the Fourier terms are calculated and
        ! added to these quantities

        refl                    = 0.0d0
        wfAlbedo                = 0.0d0
        wfEmission              = 0.0d0
        wfInterfkscaGas         = 0.0d0
        wfInterfkscaAerAbove    = 0.0d0
        wfInterfkscaAerBelow    = 0.0d0
        wfInterfkscaCldAbove    = 0.0d0
        wfInterfkscaCldBelow    = 0.0d0
        wfInterfkabs            = 0.0d0
        contribRefl             = 0.0d0

        nmuextra   = 2
        nGauss     = geometryS%nGauss
        nmutot     = geometryS%nmutot
        RTMnlayer  = optPropRTMGridS%RTMnlayer
        maxExpCoef = optPropRTMGridS%maxExpCoef
        dimSV      = controlS%dimSV

        if ( ( (1.0d0 - geometryS%uu) < 1.0d-5 ) .or. ( (1.0d0 - geometryS%u0) < 1.0d-5 ) ) then 
          if ( controlS%dimSV == 1 ) then
            FourierMax = 0
          else
            FourierMax = 2    ! azimuth dependence due to reference plane Stokes parameters 
          end if
        else
          FourierMax = maxExpCoef
        end if

        bsca = sum(optPropRTMGridS%opticalThicknLay(:) * optPropRTMGridS%ssaLay(:) )
        babs = sum(optPropRTMGridS%opticalThicknLay(:) * (1.0d0 - optPropRTMGridS%ssaLay(:)) )

        call fillAttenuation (errS, optPropRTMGridS, controlS, geometryS, nmutot, RTMnlayer, atten)
        if (errorCheck(errS)) return
    
        do iFourier = 0, FourierMax  ! start Fourier loop

          if ( iFourier > controlS%fourierFloorScalar ) then
            dimSV_fc = 1
          else
            dimSV_fc = dimSV
          end if

          ! allocate the elements of the structure 'fcCoef' declared in the header of this module
          call allocCoef(errS, fcCoef, nmutot, dimSV_fc, maxExpCoef)
          if (errorCheck(errS)) return

          ! Create space for values pertaining to the interfaces, layers and  atmosphere
          call allocateReflTransInternalField(errS, RTMnlayer, dimSV_fc, nmutot, nmuextra, RT_fc,  &
                                              UDorde_fc, UDLocal_fc, UDsumLocal_fc, UD_fc)
          if (errorCheck(errS)) return

          ! pre-calculate generalized spherical functions
          call fillPlmVector(errS, fcCoef, iFourier, dimSV_fc, nmutot, optPropRTMGridS%maxExpCoef, geometryS)
          if (errorCheck(errS)) return

          ! Initialize
          do ilayer = 0, RTMnlayer
            RT_fc(ilayer)%R   = 0.0d0
            RT_fc(ilayer)%T   = 0.0d0
            RT_fc(ilayer)%Rst = 0.0d0
            RT_fc(ilayer)%Tst = 0.0d0
          end do
                         
          ! fill reflection for Lambertian surface below the atmosphere (or cloud albedo)
          call fillsurface(errS, iFourier, dimSV_fc, nmutot, albedo, geometryS,       &
                           RT_fc(RTMnlevelCloud)%R,   RT_fc(RTMnlevelCloud)%T,  &
                           RT_fc(RTMnlevelCloud)%Rst, RT_fc(RTMnlevelCloud)%Tst)
          if (errorCheck(errS)) return

          ! calculate the internal radiation field at the interfaces between the layers
          call fieldsFromLambSurface(errS, RTMnlevelCloud, RTMnlayer, atten, dimSV_fc, nmutot, nGauss, &
                                     RT_fc, UDsumLocal_fc, UDLocal_fc, UDorde_fc, UD_fc)
          if (errorCheck(errS)) return

         ! calculate Fourier coefficient of the weighting functions
! JdH the calculation of all the derivatives (at all altitudes) takes a lot of time
! and is not really required. Make a list of the derivatives that are needed depending on
! the state vector elements and calculate only those derivatives that are required.

          call CalcWeightingFunctionsInterface (errS, fcCoef, optPropRTMGridS, controlS,      &
                                                iFourier, RTMnlayer, RTMnlevelCloud,    &
                                                dimSV, dimSV_fc, nmutot, nGauss,        &
                                                geometryS, UDsumLocal_fc, UD_fc,        &
                                                wfInterfkscaGas_fc,                     &
                                                wfInterfkscaAerAbove_fc,                &
                                                wfInterfkscaAerBelow_fc,                &
                                                wfInterfkscaCldAbove_fc,                &
                                                wfInterfkscaCldBelow_fc,                &
                                                wfInterfkabs_fc,                        &
                                                wfAlbedo_fc)
          if (errorCheck(errS)) return

          ! Calculate Fourier coefficient of the reflectance by numerical integration
          ! over the source function.

          call CalcReflectance (errS, fcCoef, optPropRTMGridS, controlS,  &
                                RTMnlayer, RTMnlevelCloud,          &
                                iFourier, dimSV, dimSV_fc, nmutot,  &
                                nGauss, geometryS,  UD_fc,          &
                                contribrefl_fc, refl_fc)
          if (errorCheck(errS)) return

          ! calculate the Fourier coefficient of the surface emission
          call CalcDerivdRdEmission(errS, dimSV_fc, nGauss, iFourier, RTMnlayer, geometryS, UD_fc, wfEmission_fc)
          if (errorCheck(errS)) return

          ! sum over Fourier terms

          cos_m_dphi = cos(iFourier * geometryS%dphiRad )
          sin_m_dphi = sin(iFourier * geometryS%dphiRad )

          factor = 2.0d0
          if (iFourier == 0) factor = 1.0d0

          wfAlbedo               = wfAlbedo                + factor * wfAlbedo_fc                * cos_m_dphi
          wfEmission             = wfEmission              + factor * wfEmission_fc              * cos_m_dphi
          wfInterfkscaAerAbove(:)= wfInterfkscaAerAbove(:) + factor * wfInterfkscaAerAbove_fc(:) * cos_m_dphi
          wfInterfkscaAerBelow(:)= wfInterfkscaAerBelow(:) + factor * wfInterfkscaAerBelow_fc(:) * cos_m_dphi
          wfInterfkscaCldAbove(:)= wfInterfkscaCldAbove(:) + factor * wfInterfkscaCldAbove_fc(:) * cos_m_dphi
          wfInterfkscaCldBelow(:)= wfInterfkscaCldBelow(:) + factor * wfInterfkscaCldBelow_fc(:) * cos_m_dphi
          wfInterfkscaGas(:)     = wfInterfkscaGas(:)      + factor * wfInterfkscaGas_fc(:)      * cos_m_dphi
          wfInterfkabs(:)        = wfInterfkabs(:)         + factor * wfInterfkabs_fc(:)         * cos_m_dphi

          do iSV = 1, dimSV_fc
            if ( iSV > 2 ) then
              cs_m_dphi = sin_m_dphi
            else
              cs_m_dphi = cos_m_dphi
            end if
            refl(iSV)          = refl(iSV)          + factor * refl_fc(iSV)           * cs_m_dphi
            contribrefl(iSV,:) = contribrefl(iSV,:) + factor * contribrefl_fc(iSV, :) * cs_m_dphi
          end do 

          ! clean up
          call deallocateReflTransIntField(errS, RTMnlayer, RT_fc, UDorde_fc, UDLocal_fc, UDsumLocal_fc, UD_fc)
          if (errorCheck(errS)) return
          call deallocCoef(errS, fcCoef, maxExpCoef)
          if (errorCheck(errS)) return

        end do  ! Fourier loop

    end subroutine singleScattering


    subroutine fieldsFromLambSurface(errS, startLevel, endLevel, atten, dimSV_fc, nmutot, nGauss,  &
                                     RT_fc, UDsumLocal_fc, UDLocal_fc, UDorde_fc, UD_fc)

      ! This routine calculates the Fourier coefficients of the internal radiation field
      ! at the interfaces, when the radiation is due to the Lambertian surface located at
      ! startLevel ( 0 for surface, > 0 for cloud). Scattering in the atmosphere is ignored.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,               intent(in) :: startLevel, endLevel
      integer,               intent(in) :: dimSV_fc, nmutot, nGauss
      real(8),               intent(in) :: atten (nmutot, 0:endLevel, 0:endLevel)
      type(RTType),          intent(in) :: RT_fc(0:endLevel)

      type(UDLocalType), intent(inout)  :: UDLocal_fc(0:endLevel)
      type(UDLocalType), intent(inout)  :: UDsumLocal_fc(0:endLevel)
      type(UDType),      intent(inout)  :: UDorde_fc(0:endLevel)
      type(UDType),      intent(inout)  :: UD_fc(0:endLevel)

      ! local

      integer :: ilevel, imu, imu0, iSV
      integer :: ind, ind0

      ! initialize
      do ilevel = 0, endLevel
        UDsumLocal_fc(ilevel)%U = 0.0d0
        UDsumLocal_fc(ilevel)%D = 0.0d0
        UDLocal_fc(ilevel)%U    = 0.0d0
        UDLocal_fc(ilevel)%D    = 0.0d0
        UDorde_fc(ilevel)%E     = 0.0d0
        UDorde_fc(ilevel)%U     = 0.0d0
        UDorde_fc(ilevel)%D     = 0.0d0
        UD_fc(ilevel)%E         = 0.0d0
        UD_fc(ilevel)%U         = 0.0d0
        UD_fc(ilevel)%D         = 0.0d0
      end do

      ! copy attenuation to UDorder and UD
      do ilevel = startLevel, endLevel
        do imu = 1, nmutot
          do iSV = 1, dimSV_fc
            ind = iSV + (imu - 1) * dimSV_fc
            UDorde_fc(ilevel)%E(ind) = atten(imu,endLevel,ilevel)
            UD_fc(ilevel)%E(ind)     = atten(imu,endLevel,ilevel)
          end do ! iSV
        end do ! imu
      end do

      ! initialize with reflection by the Lambertian surface
      do imu0 = 1, 2
        ind0 = 1 + (imu0 -1) * dimSV_fc
          do ind = 1, dimSV_fc * nmutot
            UDLocal_fc(startLevel)%U(ind,imu0) = RT_fc(startLevel)%R(ind, dimSV_fc * nGauss + ind0) &
                                               * atten(nGauss+imu0, endLevel, startLevel)
          end do ! ind
      end do ! imu0

      UDsumLocal_fc(startLevel)%U   = UDLocal_fc(startLevel)%U

      UDorde_fc(startlevel)%U = UDLocal_fc(startlevel)%U
      do ilevel = startLevel + 1, endLevel
        do imu0 = 1, 2
          do imu = 1, nmutot
            do iSV = 1, dimSV_fc
              ind = iSV + (imu - 1) * dimSV_fc
              UDorde_fc(ilevel)%U(ind,imu0) = atten(imu, ilevel-1, ilevel) * UDorde_fc(ilevel-1)%U(ind,imu0)
            end do ! iSV
          end do ! imu
        end do ! imu0
      end do ! ilevel

      ! copy first order to total

      do ilevel = startLevel, endLevel
        UD_fc(ilevel)%U  = UDorde_fc(ilevel)%U
      end do

    end subroutine fieldsFromLambSurface


end module LabosModule
