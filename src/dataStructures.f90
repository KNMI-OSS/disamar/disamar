!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module dataStructures

  ! This module is part of DISAMAR and contains:
  !   unit numbers for the different files making sure that no conflicts arise
  !   default file names for absorption cross sections
  !   boundary values for parameters
  !   data structures for DISAMAR
  !   subroutines to allocate and deallocate memory for the arrays in the data structures
  !
  ! Author : Johan de Haan
  ! Date:  2008 - 2015

  use errorHandlingModule
  use pqf_module
  use DISAMAR_file


  character(LEN= 50), parameter :: DISAMAR_version_number = '4.1.5'
  character(LEN= 50), parameter :: DISAMAR_version_date   = '2019-07-10'
  character(LEN= 50), parameter :: ALGORITHM_version_number = '1.0.0'

  integer, parameter :: outputFileUnit        = 2
  integer, parameter :: XsectionFileUnit      = 3
  integer, parameter :: configurationFileUnit = 4
  integer, parameter :: intermediateFileUnit  = 7
  integer, parameter :: solarIrrFileUnit      = 8
  integer, parameter :: errorCovOutUnit       = 9
  integer, parameter :: errorCovReadUnit      = 10
  integer, parameter :: radianceRefFileUnit   = 12
  integer, parameter :: ASCIIinputOMO3PRUnit  = 13
  integer, parameter :: readIrrRadUnit        = 14
  integer, parameter :: polarizationCorrUnit  = 15
  integer, parameter :: addtionalOutputUnit   = 16
  integer, parameter :: asciiHDFoutputUnit    = 17
  integer, parameter :: writeIrrRadUnit       = 18
  integer, parameter :: slitFunctionUnit      = 19
  integer, parameter :: expCoefFileUnit       = 20
  integer, parameter :: O2A_LISA_FileUnit     = 21
  integer, parameter :: HITRANFileUnit        = 22
  integer, parameter :: TOMS_V8_Temp_FileUnit = 23
  integer, parameter :: TOMS_V8_O3_FileUnit   = 24
  integer, parameter :: S5_SNR_FileUnit       = 25
  integer, parameter :: asciiHDFLUTUnit       = 26
  integer, parameter :: asciiHDFXsecLUTUnit   = 27

  ! default file names for absorption cross sections / line parameters
  character(LEN=250), parameter :: BrO_XsecFileName    = 'RefSpec/BrO.dat'
  character(LEN=250), parameter :: SO2_XsecFileName    = 'RefSpec/SO2.dat'
  character(LEN=250), parameter :: CH4_XsecFileName    = 'RefSpec/06_HIT08_TROPOMI.par'
  character(LEN=250), parameter :: CHOCHO_XsecFileName = 'RefSpec/CHOCHO.dat'
  character(LEN=250), parameter :: CO_XsecFileName     = 'RefSpec/05_HIT08_TROPOMI.par'
  character(LEN=250), parameter :: CO2_XsecFileName    = 'RefSpec/02_HIT08_TROPOMI.par'
  character(LEN=250), parameter :: H2O_XsecFileName    = 'RefSpec/01_HIT08_TROPOMI.par'
  character(LEN=250), parameter :: NH3_XsecFileName    = 'RefSpec/11_HIT19_TROPOMI.par'
  character(LEN=250), parameter :: HCHO_XsecFileName   = 'RefSpec/HCHO_Meller.dat'
  character(LEN=250), parameter :: NO2_XsecFileName    = 'RefSpec/NO2T_VD.dat'
  character(LEN=250), parameter :: O2_O2_XsecFileName  = 'RefSpec/O2O2T_BIRA.dat'
  character(LEN=250), parameter :: O3_XsecFileName     = 'RefSpec/O3_Brion_coeff_4Temp_ext_Chap.txt'
  ! special case for oxygen - line mixing and collision induced absorption => more files needed
  ! use for the line positions and absoption cross sections the JPL data base as default
#ifdef TROPNLL2DP
  character(LEN=250)            :: O2_LinePositionsFileName    = 'data/07_HIT08_O2AJPL_TROPOMI.par'
  character(LEN=250)            :: O2_XsecFileName             = 'data/07_HIT08_O2AJPL_TROPOMI.par'
  character(LEN=250)            :: O2_LISA_RMFXsecFileName     = 'data/O2A_LISA_RMF.dat'
  character(LEN=250)            :: O2_LISA_SDFXsecFileName     = 'data/O2A_LISA_SDF.dat'
#else
  character(LEN=250)            :: O2_LinePositionsFileName    = 'RefSpec/07_HIT08_O2AJPL_TROPOMI.par'
  character(LEN=250)            :: O2_XsecFileName             = 'RefSpec/07_HIT08_O2AJPL_TROPOMI.par'
  character(LEN=250)            :: O2_LISA_RMFXsecFileName     = 'RefSpec/O2A_LISA_RMF.dat'
  character(LEN=250)            :: O2_LISA_SDFXsecFileName     = 'RefSpec/O2A_LISA_SDF.dat'
#endif


  ! maximum values used when reading the configuration file
  integer, parameter :: nExcludeMax               = 5
  integer, parameter :: maxNumNodesWavelShift     = 20
  integer, parameter :: maxNumWavelAlbedo         = 20
  integer, parameter :: maxNumWavelEmission       = 20
  integer, parameter :: maxNumWavelCldAerFraction = 20
  integer, parameter :: maxNumWavelStrayLight     = 20
  integer, parameter :: maxNumWavelMulOffset      = 20
  integer, parameter :: maxNumWavelSNR            = 20
  integer, parameter :: maxNumWavelCalibErrRefl   = 20
  integer, parameter :: maxNumIntervals           = 20
  integer, parameter :: maxNumSpectralBands       = 20
  integer, parameter :: maxFourierIndexLUT        =  2
  integer, parameter :: maxNumPressure            = 20
  integer, parameter :: maxNumAlbedo              = 20
  integer, parameter :: maxNumScaleFactor         = 20 ! used for ignoring polarization scrambler
  integer, parameter :: maxNumExpansionCoeffXsec  = 30

  ! maximum values for trace gas columns (in Dobson units)
  real, parameter    :: maxColSO2       = 100.0d0
  real, parameter    :: maxColO3        = 1000.0d0
  real, parameter    :: maxColNO2       = 5.0d0
  real, parameter    :: maxColHCHO      = 5.0d0
  real, parameter    :: maxColBrO       = 5.0d0
  real, parameter    :: maxColCHOCHO    = 5.0d0
  real, parameter    :: maxColH2O       = 5.0d6
  real, parameter    :: maxColNH3       = 5.0d3
  real, parameter    :: maxColCO2       = 5.0d6
  real, parameter    :: maxColCH4       = 5.0d4
  real, parameter    :: maxColCO        = 5.0d3

  ! maximum number of aerosol / cloud scattering components
  integer, parameter :: maxNumMieModels           = 4

  integer, parameter :: xsmax                     = 20

! BOUNDARIES FOR PARAMETERS
  real(8), parameter :: maxOptThicknessCld = 250.0d0   ! maximal value optical thickness cloud
  real(8), parameter :: minOptThicknessCld =   0.0d0   ! minimal value optical thickness cloud
  real(8), parameter :: maxOptThicknessAer =  25.0d0   ! maximal value optical thickness aerosol / cloud
  real(8), parameter :: minOptThicknessAer =   1.0d-2  ! minimal value optical thickness aerosol / cloud
  real(8), parameter :: maxSSA             =   1.0d0   ! maximal value single scattering albedo aerosol / cloud
  real(8), parameter :: minSSA             =   0.0d0   ! minimal value single scattering albedo aerosol / cloud
  real(8), parameter :: maxCldFraction     =   1.5d0
  real(8), parameter :: minCldFraction     =   0.0d0
  real(8), parameter :: minCldFractionBnd  =   1.0d-2  ! minimal cloud fraction for boundary update
  real(8), parameter :: maxCldFractionBnd  =   0.95d0  ! maximal cloud fraction for boundary update

  ! For a Lambertian cloud the surface albedo is not updated if the cloud fraction > cldFractionBlockUpdateSurfAlbedo
  real(8), parameter :: cldFractionBlockUpdateSurfAlb  = 0.95d0

  ! For a scattering cloud the surface albedo is not updated if the cloud fraction > cldFractionBlockUpdateSurfAlbedo
  ! and the sum of the aerosol and cloud optical thicknesses is larger than OptThicknessBlockUpdateSurfAlbedo
  real(8), parameter :: OptThicknBlockUpdateSurfAlb    = 20.0d0

  ! If the cloud fraction is less than cldFractionBlockUpdateCldPram the cloud parameters, i.e. cloud
  ! pressure, cloud albedo, cloud optical thickness, and / or cloud angstrom coefficient are not updated
  real(8), parameter :: cldFractionBlockUpdateCldPram  = 0.0001d0

  ! If the surface albedo is large (ice/snow) it is not possible to fit the surface albedo and the
  ! cloud albedo. If the maximim of the surface albedo values > surfaceAlbBlockUpdateCldAlb
  ! then the cloud albedo is set to its a priori value and it is not updated.
  real(8), parameter :: surfaceAlbBlockUpdateCldAlb    = 0.40d0

  real(8), parameter :: maxSurfAlbedo     =    1.5d0
  real(8), parameter :: minSurfAlbedo     =    0.0d0   ! not used for AAI
  real(8), parameter :: maxSurfEmission   =    1.0d14
  real(8), parameter :: minSurfEmission   =    0.0d0
  real(8), parameter :: maxCldAlbedo      =    2.0d0
  real(8), parameter :: minCldAlbedo      =    0.0d0
  real(8), parameter :: maxAerAlbedo      =    0.3d0
  real(8), parameter :: minAerAlbedo      =    0.0d0
  real(8), parameter :: maxfracRamanLines =    1.000001d0
  real(8), parameter :: minfracRamanLines =    0.02d0
  ! the pressure ratio p_top / p_bottom of an interval
  ! in the atmosphere must be smaller than maxPressureRatio
  real(8), parameter :: maxPressureRatio    =    0.99999d0
  real(8), parameter :: maxAltResolvedAMF   =    100.0d0
  real(8), parameter :: minAltResolvedAMF   =    1.0d-8
  real(8), parameter :: minAltSurface       =   -2.0d0  ! minimum altitude for surface albedo in km
  real(8), parameter :: maxAltSurface       =    8.0d0  ! maximum altitude for surface albedo in km
  real(8), parameter :: maxAltTopFitInterval=   15.0d0  ! maximum altitude for cloud top      in km
  real(8), parameter :: minPresSurface      =   20.0d0  ! minimum surface pressure in hPa
  real(8), parameter :: maxPresSurface      = 1500.0d0  ! maximum surface pressure in hPa
  real(8), parameter :: maxTempOffset       =   50.0d0  ! Kelvin
  real(8), parameter :: maxTemperature      =  400.0d0  ! Kelvin
  real(8), parameter :: minTemperature      =  150.0d0  ! Kelvin
  real(8), parameter :: maxAngstromCoef     =    5.0d0
  real(8), parameter :: minAngstromCoef     =   -5.0d0
  real(8), parameter :: maxHGparam_g        =    0.9d0
  real(8), parameter :: minHGparam_g        =   -0.9d0
  real(8), parameter :: maxFWHM             =   20.0d0
  real(8), parameter :: minFWHM             =   0.10d0
  real(8), parameter :: threshWavelDiff     =   1.0d-3 ! SNR, cloud albedo, surface albedo, stray light can be
                                                       ! specified at a few wavelengths and interpolation is used
                                                       ! to calculate the values at other wavelengths.
                                                       ! The wavelengths where the values are specified must
                                                       ! differ at least threshWavelDiff nm.
  real(8), parameter :: threshWavelDiffRadIrr =   0.06 ! in nm; when read values of irradiance and radiance (.sim file)
                                                       ! differ more than threshWavelDiffRadIrr an error message is
                                                       ! generated
  real(8), parameter :: threshDiffGeometry    =   0.1  ! in degree; when read values of the geometry for simulation
                                                       ! and retrieval differ more than threshDiffGeometry an warning is
                                                       ! generated



! TYPE DECLERATIONS
! the type declarations are in alphabetical order, except for
! globalType, signalToNoiseType, and slitFunctionType because types have
! to be declared before they can be used.

  type absLinesType
    logical            :: useHITRAN              ! if true use HITRAN line database else read smooth cross sections
    character(LEN=250) :: XsectionFileName
    integer            :: gasIndex               ! HITRAN index for the gas (1 = H2O, ....)
    integer            :: nISO                   ! number of ISO values read from file
    integer            :: ISO(8)                 ! isotope index (1 = most abundant, 2 = second most abundant, etc.)
    real(8)            :: thresholdLine          ! lines with an intensity < thresholdLines * maximum intensity
                                                 ! are ignored for determining the wavelength grid
    integer            :: nlinePos               ! number of lines in the interval considered
    real(8), pointer   :: linePos(:)   => null() ! (nlinepos) line positions (nm)
    real(8), pointer   :: S(:)         => null() ! (nlinepos) line intensity (total line strength)
  end type absLinesType

  type calibrReflType ! note nwavel and nerror MUST be equal
    integer          :: nwavel                             ! number of wavelength where calibration error is specified
    integer          :: nerror                             ! number of of calibration errors
    real(8)          :: corrLengthMul                      ! correlation length for multiplicative error in nm
    real(8)          :: corrLengthAdd                      ! correlation length for additive error in nm
    real(8), pointer :: wavel(:)                 => null() ! (nwavel) wavelengths where calibration error is specified (in nm)
    real(8), pointer :: errorMul(:)              => null() ! (nerror) multiplicative calibration error reflectance (in percent)
    real(8), pointer :: errorAdd(:)              => null() ! (nerror) additive calibration error reflectance (in percent)
    real(8), pointer :: refladd(:)               => null() ! (nerror) added reflectance due to additive calibration error
  end type calibrReflType

  type cldAerFractionType ! specification of cloud fraction different from a single cloud fraction for all bands
    logical             :: fitCldAerFraction                     ! flag for fitting the cloud/aerosol fraction
    logical             :: fractionCldAerType                    ! if .true. cloud fraction if .false. aerosol fraction
    integer             :: nwavelCldAerFraction                  ! number of wavelength where the fraction is specified
    integer             :: nCldAerFraction                       ! number of cloud / aerosol fraction values
    integer             :: nVarianceCldAerFraction               ! number of variance values
    real(8), pointer    :: wavelCldAerFraction(:)      => null() ! (nwavelCldAerFraction) wavelemgths
    real(8), pointer    :: cldAerFraction(:)           => null() ! (nCldAerFraction) cloud fraction at wavelCloudFraction(:)
    real(8), pointer    :: varianceCldAerFraction(:)   => null() ! (nVarianceCldAerFraction) variance
    real(8), pointer    :: cldAerFractionAP(:)         => null() ! (nCldAerFraction) a-priori fraction
    real(8), pointer    :: varianceCldAerFractionAP(:) => null() ! (nVarianceCldAerFraction) a-priori variance
  end type cldAerFractionType

  type cloudAerosolRTMgridType

    logical             :: fitAlbedoLambSurfAllBands  ! if .true. fit the Lambertian surface albedo for all wavelengths
    logical             :: useAlbedoLambSurfAllBands  ! if .true. the surface albedo holds for all wavelengths
    real(8)             :: albedoLambSurfAllBands     ! albedo of the Lambertian cloud for all wavelengths
    real(8)             :: albedoLambSurfAllBandsAP   ! a priori albedo of the Lambertian cloud for all wavelengths
    real(8)             :: varAlbedoLambSurfAllAP     ! a priori variance for the surface albedo for all wavelengths
    real(8)             :: varAlbedoLambSurfAll       ! a posteriori variance for the surface albedo for all wavelengths

    logical             :: cloudPresent               ! if .true. there is a cloud
    logical             :: useLambertianCloud         ! if .true. a Lambertian surface at a certain altitude is used
    logical             :: useAlbedoLambCldAllBands   ! if .true. the cloud albedo holds for all spectral bands
    logical             :: fitAlbedoLambCldAllBands   ! if .true. fit the Lambertian cloud albedo (same for all bands)
    integer             :: RTMnlevelCloud             ! altitude Lamb cloud OptPropRTMGridS%RTMaltitude(RTMnlevelCloud)
    real(8)             :: albedoLambCldAllBands      ! albedo of the Lambertian cloud for all bands
    real(8)             :: albedoLambCldAllBandsAP    ! a priori albedo of the Lambertian cloud for all bands
    real(8)             :: varAlbedoLambCldAllAP      ! a priori variance for the cloud albedo for all bands
    real(8)             :: varAlbedoLambCldAll        ! a posteriori variance for the cloud albedo for all bands

    logical             :: aerosolPresent             ! if .true. there is aerosol
    logical             :: useLambertianAerosol       ! if .true. a Lambertian surface at a certain altitude is used
    logical             :: useAlbedoLambAerAllBands   ! if .true. the cloud albedo holds for all spectral bands
    logical             :: fitAlbedoLambAerAllBands   ! if .true. fit the Lambertian cloud albedo (same for all bands)
    real(8)             :: albedoLambAerAllBands      ! albedo of the Lambertian cloud for all bands
    real(8)             :: albedoLambAerAllBandsAP    ! a priori albedo of the Lambertian cloud for all bands
    real(8)             :: varAlbedoLambAerAllAP      ! a priori variance for the aerosol albedo for all bands
    real(8)             :: varAlbedoLambAerAll        ! a posteriori variance for the cloud albedo for all bands

    logical             :: fitCldAerFractionAllBands  ! if .true. the cloud/aerosol fraction is fitted
    logical             :: fractionCldAerType         ! if .true. cloud fraction if .false. aerosol fraction

    ! If we want to fit both the surface albedo and the cloud albedo but fix one of them as follows
    !    cloud fraction >  threshold then set AP variance of the surface albedo to 1.0E-8, i.e. fit cloud albedo
    !    cloud fraction <= threshold then set AP variance of the cloud   albedo to 1.0E-8, i.e. fit surf albedo
    ! this can only be used if both surface albedo and cloud albedo are fitted and the flag
    !    useThresholdSurfCloudAPvariance is .true.
    ! The modification of the AP variance is done in the verifyConfigFileModule, after reading the config file
    logical             :: useThresholdCldFraction
    real(8)             :: thresholdCldFraction

                        ! values if the cloud/aerosol fraction is the same for all spectral bands
    logical             :: useCldAerFractionAllBands   ! if .true. the cld/aer fraction is the same for all bands
    real(8)             :: cldAerfractionAllBandsAP    ! a-priori cld/aer fraction
    real(8)             :: varCldAerFractionAllBandsAP ! a-priori variance for the cld/aer fraction
    real(8)             :: varCldAerFractionAllBands   ! a-posteriori variance for the cld/aer fraction
    real(8)             :: cldAerfractionAllBands      ! cld/aer fraction (retrieved value)

    integer             :: ninterval                  ! number of atmospheric intervals to define cloud and aerosol
    integer             :: nvariance                  ! number of atmospheric intervals where altitude variance is given
    integer             :: nGaussAlt                  ! number of intervals where Gausspointe for altitude is given

! we assume the altitude of one interval can be fitted with the options:
    integer             :: numIntervalFit                     ! number of interval whose altitude can be fitted
    logical             :: fitIntervalDP                      ! if .true. the top pressure of the interval is fitted
                                                              ! keeping the pressure difference top and bot constant
    logical             :: fitIntervalTop                     ! if .true. fit the altitude of the top of the interval
    logical             :: fitIntervalBot                     ! if .true. fit the altitude of the bottom of the interval
    real(8), pointer    :: intervalBoundsAP(:)      => null() ! (0:ninterval) a-priori boundaries of the intervals [km]
    real(8), pointer    :: intervalBounds(:)        => null() ! (0:ninterval) boundaries of the intervals [km]
    real(8), pointer    :: varIntervalBoundsAP(:)   => null() ! (0:ninterval) a-priori variances for the boundaries [km2]
    real(8), pointer    :: varIntervalBounds(:)     => null() ! (0:ninterval) variances for the boundaries [km2]
    real(8), pointer    :: intervalBoundsAP_P(:)    => null() ! (0:ninterval) a-priori boundaries of the intervals [hPa]
    real(8), pointer    :: intervalBounds_P(:)      => null() ! (0:ninterval) boundaries of the intervals [hPa]
    real(8), pointer    :: varIntervalBoundsAP_P(:) => null() ! (0:ninterval) a-priori variances for the boundaries [hPa2]
    real(8), pointer    :: varIntervalBounds_P(:)   => null() ! (0:ninterval) variances for the boundaries [hPa2]

    integer, pointer    :: intervalnGauss(:)        => null() ! (ninterval) number of gaussian division points
                                                              !  on each interval

                                                         ! wavelength dependence aerosol optical thickness
                                                         ! is given by Angstrom law:
                                                         ! b_aer(w) = baer(w0) * (w/w0)**(-angstromCoef)
    real(8)             :: w0                            ! reference wavelength for the aerosol and cloud optical thickness
    logical             :: fitAerTau                     ! if .true. fit the aerosol optical thickness in one interval
    real(8), pointer    :: intervalAerTauAP(:) => null() ! (ninterval) a-priori optical thicknesses of aerosol at w0
    real(8)             :: varAerTauAP                   ! a priori variance variance aerosol optical thickness at w0
    real(8)             :: varAerTau                     ! a posteriori variance aerosol optical thickness at w0
    real(8), pointer    :: intervalAerTau(:)   => null() ! (ninterval) optical thickness of aerosol at wo

    logical             :: fitAerSSA                     ! if .true. fit the aerosol single scattering albedo
    real(8), pointer    :: intervalAerSSAAP(:) => null() ! (ninterval) a-priori single scattering albedo of aerosol
    real(8)             :: varAerSSAAP                   ! a priori variance SSA of aerosol (fit interval)
    real(8)             :: varAerSSA                     ! a posteriori variance aerosol SSA (fit interval)
    real(8), pointer    :: intervalAerSSA(:)   => null() ! (ninterval) single scattering albedo of aerosol

    logical             :: fitAerAC                      ! if .true. fit the aerosol Angstrom coefficient
    real(8), pointer    :: intervalAerACAP(:)  => null() ! (ninterval) a priori Angstrom coefficient aerosol
    real(8)             :: varAerACAP                    ! a priori variance aerosol Angstrom coefficient (fit interval)
    real(8)             :: varAerAC                      ! a posteriori variance aerosol Angstrom coefficient (fit interval)
    real(8), pointer    :: intervalAerAC(:)    => null() ! (ninterval) angstrom coefficient for aerosol

    real(8), pointer    :: intervalAer_g(:)    => null() ! (ninterval) Henyey Greenstein parameter for aerosol particles

    logical             :: fitCldTau                     ! if .true. fit the cloud optical thickness for one interval
    logical             :: fitLnCldTau                   ! if .true. the logarithm of the cloud optical thickness is
                                                         ! fitted instead of the cloud optical thickness itself
    real(8), pointer    :: intervalCldTauAP(:) => null() ! (ninterval) a-priori cloud optical thicknesses
    real(8)             :: varCldTauAP                   ! a priori variance for the cloud optical thickness  (fit interval)
    real(8)             :: varCldTau                     ! a posteriori variance for the cloud optical thickness (fit interval)
    real(8), pointer    :: intervalCldTau(:)   => null() ! (ninterval) optical thickness of cloud

    logical             :: fitCldAC                      ! if .true. fit the cloud Angstrom coefficient
    real(8), pointer    :: intervalCldACAP(:)  => null() ! (ninterval) a priori angstrom coefficient aerosol
    real(8)             :: varCldACAP                    ! a priori variance cloud Angstrom coefficient (fit interval)
    real(8)             :: varCldAC                      ! a posteriori variance cloud Angstrom coefficient (fit interval)
    real(8), pointer    :: intervalCldAC(:)    => null() ! (ninterval) angstrom coefficient for cloud

    real(8), pointer    :: intervalCldSSA(:)   => null() ! (ninterval) single scattering albedo of cloud
    real(8), pointer    :: intervalCld_g(:)    => null() ! (ninterval) Henyey Greenstein parameter for cloud

    logical             :: useMieScatAer                 ! if .true. Mie scattering is used for aerosol
    logical             :: useHGScatAer                  ! if .true. Henyey-Greenstein scattering is used for aerosol
    logical             :: useMieScatCld                 ! if .true. Mie scattering is used for cloud
    logical             :: useHGScatCld                  ! if .true. Henyey-Greenstein scattering is used for cloud

    real(8)             :: thresholdPhaseFunctionCoef    ! threshold for truncating the phase function coefficients
  end type cloudAerosolRTMgridType

  type columnType ! parameters for one column that can be divided into sub columns
    integer             :: nTrace                     ! number of trace gases
    integer             :: nwavel                     ! number of wavelengths on the instrument grid
    integer             :: nsubColumn                 ! number of subcolumns
    integer             :: nsubColumn_boundaries      ! number of subcolumns for tropopause
    integer             :: nsubColumn_altBoundaries   ! number of subcolumns for specified altitudes
    integer             :: nGaussCol                  ! # Gauss points for entire column
    integer             :: nstateCol                  ! # state vector elements for the column
    integer             :: nGaussInit(3)              ! number of Gassian points for PBL, free troposphere and stratosphere
                                                      ! as read from the configuration file
    character(LEN=50), pointer :: boundaries(:) => null() ! (0:nsubColumn_boundaries) text boundaries, e.g. tropopause or toa
    real(8),           pointer :: altBound(:)   => null() ! (0:nsubColumn) altitudes that define the (boundaries of) subcolumns
    real(8),           pointer :: Sa(:,:,:)     => null() ! (nsubColumn, nsubColumn, nTrace) a-priori error covariance
    real(8),           pointer :: S(:,:,:)      => null() ! (nsubColumn, nsubColumn, nTrace) error covariance
    real(8),           pointer :: Snoise(:,:,:) => null() ! (nsubColumn, nsubColumn, nTrace) noise part of error covariance
    real(8),           pointer :: Gain(:,:,:)   => null() ! (nsubColumn, nwavel, nTrace) gain for the subcolumns

    type(subcolumnType), pointer :: subColumn(:) => null() ! (nsubColumn) properties of a subcolumn
  end type columnType

  type controlType ! control parameters for radiative transfer

    ! file name for writing ASCII data for OMO3PR (ozone profile algorithm for OMI)
    character(LEN=250) :: ASCIIinputOMO3PR_FileName

    ! file name for reading ASCII data for the reflectance, temperature and pressure profiles from file
    character(LEN=250) :: ASCIIinputIrrRadFileName

    ! file name for additional output
    character(LEN=250) :: additionalOutputFileName

    ! if true then the reflectance is read from file
    ! replacing simulated measurements
    logical          :: useReflectanceFromFile

    logical          :: simulationOnly
    logical          :: aerosolLayerHeight      ! if true the flexibility of the code is reduced
                                                ! and the code is optimized for speed
    logical          :: allowNegativeSurfAlbedo ! used for ozone profile and AAI

    logical          :: singleScatteringOnly
    logical          :: integrateSourceFunction ! if true calculate reflectance by integrating the source function
    logical          :: renormPhaseFunction     ! if true renormalize the the first fc of the phase function
    logical          :: ignoreSlit              ! if true radiance and irradiance are not convoluted with slit
    logical          :: useEffXsec_OE           ! if true absorption cross section is convoluted with
                                                ! slit function including solar irradiance (I0 effect)

    ! method = 0  use full optimal estimation
    ! method = 1  use optimal estimation in the limit for weak absorption
    ! method = 2  use DOAS (retrieve columns of trace gases) no other parameters
    ! method = 3  use classical DOAS
    ! method = 4  use DOMINO for NO2
    integer          :: method

    logical          :: copyNO2profileRetrToSim      ! only used for NO2 - DOMINO
    logical          :: calculateAAI                 ! if .true. AAI is calculated in the retrieval

    ! switches to control the calculations
    logical          :: calculateDerivatives
    logical          :: useAdding                    ! use adding instead of LABOS
    logical          :: useSphericalCorr             ! use spherical correction
    logical          :: useDeltaM                    ! use truncation of the phase function (written to file)

    ! switches to control the use of a absorption cross section LUT
    ! LUT is based on an expansion in Legendre polynomials
    logical          :: usePolyExpXsec

    ! switches to control the additional output
    logical          :: writeHighResolutionRefl
    logical          :: writeInstrResolutionRefl
    logical          :: writeHighResReflAndDeriv
    logical          :: writeInstrResReflAndDeriv
    logical          :: writeSNR

    logical          :: writeContributionRadiance
    logical          :: writeInternalFieldUpHR
    logical          :: writeInternalFieldUp
    logical          :: writeInternalFieldDownHR
    logical          :: writeInternalFieldDown

    logical          :: writeAltResolvedAMF
    logical          :: writeAbsorptionXsec

    logical          :: writeRing_spectra
    logical          :: writeDiffRing_spectra
    logical          :: writeFilling_in_spectra

    logical          :: testUseAveragingKernel
    logical          :: writeSpecFeatIrr      ! if true write errors due to spectral features in solar irradiance
    logical          :: writeSpecFeatRad      ! if true write errors due to spectral features in earth radiance
    logical          :: writeSpecFeatRefl     ! if true write errors due to spectral features in sun-normalized radiance
    logical          :: polCorrectionFile     ! flag to indicate that ascii text correction files are to be written
    logical          :: testChandrasekhar     ! if true test Chandrasekhar expression for surface albedo, e.g. when RRS is present
    logical          :: testderivatives       ! if .true. derivatives are calculated numerically and compared
                                              ! to the semi-analytical derivatives

    ! accuracy of the solution is mostly controlled by these parameters
    integer          :: nStreams             ! number of streams (= 2 * nGauss)
    integer          :: numOrdersMax         ! maximum number of orders of scattering. If convergence is not
                                             ! reached after numOrdersMax the sum of the higher orders are estimated
                                             ! using a geometrical series approximation. Typically 8, but for
                                             ! optically thick scattering clouds the number should be about
                                             ! 1.5 * the optical thickness of the scattering cloud.
                                             ! note that during retrieval the optical thickness can change
                                             ! and the value of numOrdersMax may be adjusted in
                                             ! optimalEstimationModule: subroutine updateFitParameters
    real(8)          :: cloudFracThreshold   ! perform calculation for clouds only if the geometrical cloud fraction
                                             ! is larger than the threshold specified by cloudFracThreshold
    real(8)          :: thresholdDoubl       ! doubling is started wih single scattering. The optical thickness
                                             ! is halved a number of times until this threshold is reached
                                             ! then doubling is started fr this optical thickness (typically 1.0d-6)
    real(8)          :: thresholdMul         ! when calculating R and T of the layers using doubling a matrix product
                                             ! (C=A*B) is not executed if Trace(A) * Trace(B) < thresholdMul
                                             ! C is then set to zero. Typically 1.0d-8 - 1.0d-10
    real(8)          :: thresholdConv_first  ! threshold for the convergence of the first order of scattering
                                             ! higher orders of scattering are not calculated if this
                                             ! threshold is satisfied. Typically 1.0d-5
    real(8)          :: thresholdConv_mult   ! threshold for the convergence of multiple scattering scattering
                                             ! higher orders of scattering are not calculated if this
                                             ! threshold is satisfied. Typically 1.0d-8
    real(8)          :: relChange            ! relative change in parameters used for numerical differentiation
    ! parameters for polarization
    integer          :: dimSV                ! dimension of the Stokes vector
    integer          :: fourierFloorScalar   ! scalar calculations if Fourier index m > FourierFloorScalar

  end type controlType

  type createLUTType
    logical                       :: reflectanceLUT
    logical                       :: correctionLUT
    logical                       :: useChandraFormula
    integer                       :: npressure
    integer                       :: nlevel
    integer                       :: nsurfAlb
    integer                       :: nwavel
    integer                       :: nmu
    integer                       :: dimSV                        ! dimension Stokes vector
    integer                       :: nmup                         ! nmup = dimSV_fc * nmu
    real(8), pointer              :: pressure(:)        => null() ! (0:npressure) pressure levels for LUT
    real(8), pointer              :: surfAlb(:)         => null() ! (nsurfAlb) surface albedo values for LUT if Chandra is not used
    real(8), pointer              :: wavel(:)           => null() ! (nwavel) wavelength values for LUT
    real(8), pointer              :: mu(:)              => null() ! (nmu) polar angles
    ! reflectance LUT - monochromatic to support Chandrasekhar expression
    real(8), pointer              :: s_star(:,:)        => null() ! (0:npressure, nwavel) spherical albedo for illum. from below
    real(8), pointer              :: td(:,:,:)          => null() ! (nmu, 0:npressure, nwavel) diffuse tranmittance
    real(8), pointer              :: expt(:,:,:)        => null() ! (nmu, 0:npressure, nwavel) direct attenuation
    real(8), pointer              :: R0(:,:,:,:)        => null() ! (nmu, nmu, 0:npressure, nwavel)
    ! reflectance LUT - not using Chandrasekhar expression
    real(8), pointer              :: R(:,:,:,:,:)       => null() ! (nmu, nmu, nsurfAlb, npressure, nwavel)
    ! E, U (upward) and D (downward diffuse) are needed for RRS; nmup = dimSV_fc*nmu
    real(8), pointer              :: E(:,:,:,:)         => null() ! (nmup, 0:nlevel, 0:npressure, nwavel)
    real(8), pointer              :: U(:,:,:,:,:,:)     => null() ! (nmup, nmup, 0:nlevel, nsurfAlb, 0:npressure, nwavel)
    real(8), pointer              :: D(:,:,:,:,:,:)     => null() ! (nmup, nmup, 0:nlevel, nsurfAlb, 0:npressure, nwavel)
  end type createLUTType

  type diagnosticType ! diagnostic parameters for the retrieval

    integer          :: nlevel                      ! nlevel: levels for profile
    integer          :: nother                      ! nother number of fit parameters other than profile
    integer          :: nstate                      ! number of state vector elements
    integer          :: nTrace                      ! number of trace gases
    integer          :: nwavelRetr                  ! number of wavelengths on instrument grid (for all bands together)
    real(8), pointer :: x_smoothed(:)     => null() ! (nstate) x_smoothed = xa + A(x_true - xa)
    real(8), pointer :: S_lnvmr(:,:)      => null() ! (nstate, nstate) covariance state vector belonging to x; x in ln(vmr)
    real(8), pointer :: A_lnvmr(:,:)      => null() ! (nstate, nstate)  averaging kernel; x in ln(vmr)
    real(8), pointer :: G_lnvmr(:,:)      => null() ! (nstate, nwavelRetr) gain matrix; x in ln(vmr)
    real(8), pointer :: Snoise_lnvmr(:,:) => null() ! (nstate, nstate) noise part of the covariance ; x in ln(vmr)
    real(8), pointer :: S_vmr(:,:)        => null() ! (nstate, nstate) covariance state vector belonging to x; x in vmr (ppmv)
    real(8), pointer :: S_vmr_prof(:,:)   => null() ! (nlevel, nlevel) covariance state vector belonging to x; x in vmr (ppmv)
    real(8), pointer :: error_correlation_other(:,:)  => null() ! (nother, nother) error correlation r^2 not for profile
    real(8), pointer :: A_vmr(:,:)        => null() ! (nstate, nstate) full averaging kernel; x in vmr (ppmv)
    real(8), pointer :: A_vmr_prof(:,:)   => null() ! (nlevel, nlevel)  averaging kernel for profile
    real(8), pointer :: G_vmr(:,:)        => null() ! (nstate, nwavelRetr) gain matrix; x in vmr (ppmv)
    real(8), pointer :: Snoise_vmr(:,:)   => null() ! (nstate, nstate) noise part of the covariance ; x in vmr
    real(8), pointer :: S_ndens(:,:)      => null() ! (nstate, nstate) covariance state vector; x number density molecules/cm-3
    real(8), pointer :: A_ndens(:,:)      => null() ! (nstate, nstate)  averaging kernel; x number density molecules/cm-3
    real(8), pointer :: G_ndens(:,:)      => null() ! (nstate, nwavelRetr) gain matrix; x number density molecules/cm-3
    real(8), pointer :: Snoise_ndens(:,:) => null() ! (nstate, nstate) noise part of the covariance ; x number density
    real(8), pointer :: DFStrace(:)       => null() ! (nTrace) degrees of freedom for the signal for the trace gas
    real(8)          :: DFS                         ! degrees of freedom for the signal overall
    real(8)          :: DFSTemperature              ! degrees of freedom for the signal for the temperature
    real(8)          :: infoContent_lnvmr           ! see Rodgers 2000 Sec. 2.5.2.3:  0.5*sum_i(ln[1+eigenvalues_i**2])
    real(8)          :: infoContent_vmr
    real(8)          :: infoContent_ndens
  end type diagnosticType

  ! data for a node using cubic spline interpolation for the trace gas profile
  type dn_dnodeType
    integer          :: RTMnlayerSub            ! number of sublayers
    integer          :: naltNode                ! number of nodes for a trace gas
    real(8), pointer :: dn_dnode(:,:) => null() ! (0:RTMnlayerSub, 0:naltNode)
  end type dn_dnodeType

  type signalToNoiseType ! note numSNwavel MUST be equal to numSNvalues
    logical            :: use_S5_SNR                 ! if .true. SNR for Sentinel 5 is used (from ESA)
    logical            :: useLAB_SNR                 ! if .true. SNR in terms of L(radiance), A, and B is used
    real(8)            :: A                          ! only used when useLAB_SNR is .true.
    real(8)            :: B                          ! only used when useLAB_SNR is .true.
    logical            :: useRefSpectralBin          ! if .true. a reference spectral bin size (nm) is used for S/N
    real(8)            :: refSpectralBinSize         ! reference spectral bin size (in nm)
    integer            :: numSNwavel                 ! number of wavelengths where S/N for
                                                     ! reference spectrum is specified
    integer            :: numSNvalues                ! number of values where S/N for reference spectrum is specified
    real(8), pointer   :: SNwavel(:)       => null() ! (numSNwavel) wavelengths where  S/N is specified
    real(8), pointer   :: SN_ref_values(:) => null() ! (numSNvalues) S/N(w) values pertaining to the radiance
                                                     ! reference spectrum
    real(8), pointer   :: fvalues(:)       => null() ! (numSNvalues) f(w) values (used for shot noise)
    logical            :: useShotNoise               ! if .true. use S/N(w) = f(w) * radiance(w)**0.5
                                                     ! and interpolate on f(w); if .false. interpolate on S/N(w)
    logical            :: addNoise                   ! if .true. add gaussian noise to the (ir)radiance
  end type signalToNoiseType

  type slitFunctionType
    integer           :: slitIndex          ! index that specifies the type of slit function of the instrument
    real(8)           :: FWHM               ! Full Width Half Maximum of the slit function of the instrument
    ! the second part is for a tabulated slit function or ISRF
    character(LEN=250):: slitfunctionFileName             ! name of the slit function file
    integer           :: nwavelNominal                    ! number of nominal wavelengths
    integer           :: nwavelListed                     ! listed wavelengths for slit function
    integer, pointer  :: bandNumber(:)          => null() ! (nwavelNominal) array with pixel numbers for a spectral band
    integer, pointer  :: pixelNumber(:)         => null() ! (nwavelNominal) array with pixel numbers for a spectral band
    real(8), pointer  :: wavelNominal(:)        => null() ! (nwavelNominal) array with nominal wavelengths
    real(8), pointer  :: deltawavelListed(:,:)  => null() ! (nwavelListed,nwavelNominal) wavelengths slit function
    real(8), pointer  :: slitFunctionTable(:,:) => null() ! (nwavelListed,nwavelNominal) tabulated slit function
    ! parameters to manipulate the slit function
    ! multiply slit function by (1.0 + amplitude * sin^2[scale*(w-w0)/FWHM + phase]
    ! where w is the wavelength on the high resolution grid and w0 the wavelength on the instrument grid
    real(8)           :: amplitude
    real(8)           :: scale
    real(8)           :: phase
    ! statistical parameters to characterize the slit function (ISRF)
    ! only used for slitIndex = 0 or 1
    real(8)           :: mean
    real(8)           :: standardDeviation
    real(8)           :: skewness
    real(8)           :: kurtosis
    real(8)           :: slitIntegrated
end type slitFunctionType

  type earthRadianceType
    type(signalToNoiseType) :: SNS                       ! S/N specification
    type(slitFunctionType)  :: slitFunctionSpecsS        ! specification of the slit function
    integer            :: nwavelHR                       ! number of wavelengths on HR (integration) grid
    integer            :: nwavel                         ! number of wavelengths on instrument grid
    integer            :: nstate                         ! number of state vector elements
    integer            :: nTrace                         ! number of absorbing trace gases
    integer            :: nstateCol                      ! number of state vector elements for the (sub)columns
    integer            :: nGaussCol                      ! # Gauss points for entire column
    integer            :: RTMnlayer                      ! number of layers used for radiative transfer
    integer            :: dimSV                          ! dimension Stokes Vector
    ! arrays on high-resolution spectral grid (HR)
    real(8), pointer   :: rad_clr_HR(:,:)       => null() ! (dimSV,nwavelHR) radiance for cloud free pixel on HR grid
    real(8), pointer   :: rad_cld_HR(:,:)       => null() ! (dimSV,nwavelHR) radiance for cloud covered pixel on HR grid
    real(8), pointer   :: rad_HR(:,:)           => null() ! (dimSV,nwavelHR) radiance partial cloud covered pixel on HR grid
    real(8), pointer   :: rad_ns_HR(:,:)        => null() ! (dimSV, nwavel) radiance without wavelength shifts on HR grid
    real(8), pointer   :: rad_elas_HR(:,:)      => null() ! (dimSV,nwavelHR) radiance partial cloud covered pixel on HR grid
    real(8), pointer   :: rad_inelas_HR(:,:)    => null() ! (dimSV,nwavelHR) radiance partial cloud covered pixel on HR grid
    real(8), pointer   :: contribRadHR(:,:,:)   => null() ! (dimSV,nwavelHR,0:RTMnlayer) contribution to radiance on HR grid
    real(8), pointer   :: intFieldUpHR(:,:,:)   => null() ! (dimSV, nwavelHR, 0:RTMnlayer) internal fields for given SZA and VSA
    real(8), pointer   :: intFieldDownHR(:,:,:) => null() ! (dimSV, nwavelHR, 0:RTMnlayer) internal fields for given SZA and VSA
    real(8), pointer   :: KHR_clr_lnvmr(:,:)    => null() ! (nwavelHR, nstate) derivative dL/dln(vmr) on HR grid
    real(8), pointer   :: KHR_cld_lnvmr(:,:)    => null() ! (nwavelHR, nstate) derivative dL/dln(vmr) on HR grid
    real(8), pointer   :: KHR_lnvmr(:,:)        => null() ! (nwavelHR, nstate) derivative dL/dln(vmr) on HR grid
    real(8), pointer   :: KHR_vmr(:,:)          => null() ! (nwavelHR, nstate) derivative dL/dvmr on HR grid
    real(8), pointer   :: KHR_ndens(:,:)        => null() ! (nwavelHR, nstate) derivative dL/dnDens on HR grid

    ! arrays on instrument spectral grid
    real(8), pointer   :: K_clr_lnvmr(:,:)     => null() ! (nwavel, nstate) derivatives of radiance on instrument grid
    real(8), pointer   :: K_cld_lnvmr(:,:)     => null() ! (nwavel, nstate) derivatives of radiance on instrument grid
    real(8), pointer   :: K_lnvmr(:,:)         => null() ! (nwavel, nstate) derivatives of radiance on instrument grid
    real(8), pointer   :: K_vmr(:,:)           => null() ! (nwavel, nstate) derivatives of radiance on instrument grid
    real(8), pointer   :: K_ndens(:,:)         => null() ! (nwavel, nstate) derivatives of radiance on instrument grid

    real(8), pointer   :: rad_clr(:,:)         => null() ! (dimSV,nwavel) radiance on instr grid - clear part pixel
    real(8), pointer   :: rad_cld(:,:)         => null() ! (dimSV,nwavel) radiance on instr grid - cloudy part pixel
    real(8), pointer   :: rad(:,:)             => null() ! (dimSV,nwavel) radiance on instr grid after convolution slit
                                                         ! with stray light added
    real(8), pointer   :: rad_ns(:,:)          => null() ! (dimSV, nwavel) radiance without wavelength shifts
    real(8), pointer   :: rad_elas(:,:)        => null() ! (dimSV,nwavel) radiance on instr grid - elastic part
    real(8), pointer   :: rad_inelas(:,:)      => null() ! (dimSV,nwavel) radiance on instr grid - inelastic part
    real(8), pointer   :: rad_meas(:,:)        => null() ! (dimSV,nwavel) radiance with Gaussiam noise added;
    real(8), pointer   :: rad_error(:)         => null() ! (nwavel) standard deviation of radiance (random error)

    real(8), pointer   :: contribRad(:,:,:)      => null() ! (dimSV,nwavel,0:RTMnlayer) contribution to radiance after
                                                           !  convolution slit without stray light
    real(8), pointer   :: intFieldUp(:,:,:)      => null() ! (dimSV, nwavel, 0:RTMnlayer) internal field after convolution slit
    real(8), pointer   :: intFieldDown(:,:,:)    => null() ! (dimSV, nwavel, 0:RTMnlayer) internal field after convolution slit
    real(8), pointer   :: snIntFieldUp(:,:,:)    => null() ! (dimSV, nwavel, 0:RTMnlayer) sun-normalized int field on instr grid
    real(8), pointer   :: snIntFieldDown(:,:,:)  => null() ! (dimSV, nwavel, 0:RTMnlayer) sun-normalized int field on instr grid
    real(8), pointer   :: SNrefspec(:)           => null() ! (nwavel) signal to noise for radiance reference spectrum
    real(8), pointer   :: SNrefl(:)              => null() ! (nwavel) signal to noise ratio for reflectance
    real(8), pointer   :: SN(:)                  => null() ! (nwavel) actual signal to noise ratio for radiance
    real(8)            :: SNmax                            !  maximum SNR for earth radiance (noise floor)
    ! JdH arrays for column grid NOTE: these are allocated in subcolumnModule:fillAltitudeGridCol
    real(8), pointer   :: KHR_ndensCol(:,:)  => null() ! (nwavelHR, nstateCol) derivative on HR grid for (sub)columns
    real(8), pointer   :: K_ndensCol(:,:)    => null() ! (nwavel  , nstateCol) derivatives on instr grid for (sub)columns

    ! arrays / values for reference spectra used to define SNR
    integer            :: refSpecType         ! 0 = read from file, 1 = current simulated spectrum
    character(LEN=250) :: radianceRefFileName ! name of the file containing the radiance reference spectrum
    real(8), pointer   :: radianceRefHR(:)       => null() ! (nwavelHR) radiance reference spectrum for S/N specification
    real(8), pointer   :: radianceRef(:)         => null() ! (nwavel) radiance reference spectrum for S/N specification

    ! bias in measured radiance
    real(8)  :: featurePeriodAdd    ! distance between 2 maxima in nm for the sinusoidal feature
    real(8)  :: featureAmplAdd      ! amplitude of spectral feature (sine) in percent at largest wavelength
    real(8)  :: featurePhaseAdd     ! phase of feature at largest wavelength in degree (0-180)
    real(8)  :: featurePeriodMul    ! distance between 2 maxima in nm for the sinusoidal feature
    real(8)  :: featureAmplMul      ! amplitude of spectral feature (sine) in percent at largest wavelength
    real(8)  :: featurePhaseMul     ! phase of feature at largest wavelength in degree (0-180)
    real(8)  :: offsetAdd           ! additive offset in percent of the radiance at largest wavelength
    real(8)  :: offsetMul           ! multiplicative offset in percent of the radiance at largest wavelength
    real(8)  :: percentSmear        ! imperfect charge transfer when reading CCD

    logical  :: usePolScrambler     ! if .true. a polarization scrambler is present for simulated spectrum
    integer             :: nwavelScaleFactor             ! number of wavelength where the scaleFactor is specified
    integer             :: nScaleFactor                  ! number of scaleFactor values
    real(8), pointer    :: wavelScaleFactor(:) => null() ! (nwavelScaleFactor) wavelemgths where scaleFactor is specified
    real(8), pointer    :: scaleFactor(:)      => null() ! (nScaleFactor) scale factor at wavelScaleFactorIl(:)

  end type earthRadianceType

  type external_data  ! this data is not used by DISAMAR but is copied to the output file
    logical            :: missing
    real(8)            :: latitude
    real(8)            :: longitude
    logical            :: useSurfAlbedoDataBase
    integer            :: year
    integer            :: month
    integer            :: day
    integer            :: hour
    integer            :: minute
    real(8)            :: second
    integer            :: XtrackNumber                  ! cross track position
    integer            :: AtrackNumber                  ! along track position
    integer            :: orbitNumber
    character(LEN=400) :: originalIrradianceFileName    ! file name containing the irradiance
    character(LEN=400) :: originalRadianceUVFileName    ! file name containing the radiance for UV wavelengths
    character(LEN=400) :: originalRadianceVISFileName   ! file name containing the radiance for VIS wavelengths
  end type external_data

  ! phase function stuff: use generalized spherical functions to calculate the Fourier
  ! coefficients of the phase matrix
  ! for Fourier coefficient m use
  ! Zm_refl(u,u0) = (-1)**m * sum_k(fcCoef(k)%PlmMin (u,u0)*expCoef(k)*fcCoef(k)%PlmPlus(u,u0))
  ! Zm_tr(u,u0)   = (-1)**m * sum_k(fcCoef(k)%PlmPlus(u,u0)*expCoef(k)*fcCoef(k)%PlmPlus(u,u0))
  ! PlmPlus and PlmMin contain supermatrix-weights and have dimension dimSV * nmutot
  ! expCoef(k) is a (dimSV, dimSV) matrix that contains the coefficients for the expansion
  ! in generalized spherical functions in a form such that PlmPlus and PlmMin are diagonal.
  ! The factor (-1)**m occurs because the imaginary unit is omitted in PlmPlus and PlmMin.

  type fc_coefficients
    real(8), pointer :: PlmPlus(:) => null() ! (dimSV*nmutot) generalized spherical functions for +u
    real(8), pointer :: PlmMin (:) => null() ! (dimSV*nmutot) generalized spherical functions for -u
  end type fc_coefficients

  type gasPTType ! specification of the pressure and temperature profiles for the atmosphere
    logical             :: useTempClimatology       ! if .true. use TOMS-V8 temperature climatology
    logical             :: fitTemperatureProfile    ! if .true. fit temperature profile
    logical             :: fitTemperatureOffset     ! if .true. fit temperature offset
    logical             :: useLinInterp             ! if .true. use linear instead of spline interpolation for T(ln(P))
    logical             :: useAPCorrLength          ! if .true. use correlation length for non-diagonal elements of Sa
    real(8)             :: APCorrLength             ! correlation length for temperature in km used for Sa
    integer             :: npressureNodes           ! number of pressure levels - 1 (start at 0) where
                                                    ! temperature is specified
    integer             :: npressure                ! number of pressure levels - 1 (start at 0) - high resolution grid
    real(8)             :: tropopausePressure       ! pressusre of the tropopause (hPa)
    real(8)             :: tropopauseTemperature    ! temperature at tropopause (hPa)
    real(8)             :: tropopauseAltitude       ! altitude of the tropopause (km)
    real(8)             :: TOA                      ! altitude of the top of the atmosphere (km)
    real(8)             :: temperatureOffsetAP      ! a-priori offset temperature in Kelvin
    real(8)             :: temperatureOffset        ! offset temperature in Kelvin
    real(8)             :: varianceTempOffsetAP     ! a-priori variance of the offset in the temperature in Kelvin**2
    real(8)             :: varianceTempOffset       ! variance of the offset in the temperature in Kelvin**2
    real(8), pointer    :: altNodes(:)               => null() ! (0:npressureNodes) altitudes for nodes in km
    real(8), pointer    :: altNodesAP(:)             => null() ! (0:npressureNodes) a-priori altitude for nodes in km
    real(8), pointer    :: pressureNodes(:)          => null() ! (0:npressureNodes) pressure in hPa for the temperature nodes
    real(8), pointer    :: lnpressureNodes(:)        => null() ! (0:npressureNodes) ln of pressure at nodes
    real(8), pointer    :: temperatureNodesAP(:)     => null() ! (0:npressureNodes) a-priori temperature for nodes in Kelvin
    real(8), pointer    :: temperatureNodes(:)       => null() ! (0:npressureNodes) temperature in Kelvin for nodes in Kelvin
    real(8), pointer    :: temperature_trueNodes(:)  => null() ! (0:npressureNodes) temperature at nodes interpolated to retr grid
    real(8), pointer    :: covTempNodesAP(:,:)       => null() ! (0:npressureNodes) a-priori covariance temperature at nodes
    real(8), pointer    :: covTempNodes(:,:)         => null() ! (0:npressureNodes) covariance temperature in Kelvin at nodes
    real(8), pointer    :: alt(:)                    => null() ! (0:npressure) altitude in km on PT grid
    real(8), pointer    :: altAP(:)                  => null() ! (0:npressure) a-priori altitude in km on PT grid
    real(8), pointer    :: pressure(:)               => null() ! (0:npressure) pressure in hPa on PT grid
    real(8), pointer    :: lnpressure(:)             => null() ! (0:npressure) ln of pressure in hPa on PT grid
    real(8), pointer    :: temperatureAP(:)          => null() ! (0:npressure) a-priori temperature in Kelvin on PT grid
    real(8), pointer    :: temperature(:)            => null() ! (0:npressure) temperature in Kelvin on PT grid
    real(8), pointer    :: temperature_true(:)       => null() ! (0:npressure) temperature spline interpolated to retr grid
    real(8), pointer    :: numDensAir(:)             => null() ! (0:npressure) number density air molecules in molecules cm-3
    real(8), pointer    :: scaleHeightAP(:)          => null() ! (0:npressure) scale height in km
    real(8), pointer    :: scaleHeight(:)            => null() ! (0:npressure) scale height in km
  end type gasPTType

  type geometryType
    real(8)            :: sza                        ! solar zenith angle in degree
    real(8)            :: s_azimuth                  ! solar azimuth in degree NOTE: azimuth of the sun not of solar light
    real(8)            :: vza                        ! viewing zenith angle in degree
    real(8)            :: v_azimuth                  ! viewing azimuth angle in degree
    real(8)            :: dphi                       ! azimuth difference for light in degree
    real(8)            :: u0                         ! cos(sza)
    real(8)            :: uu                         ! cos(vza)
    real(8)            :: dphiRad                    ! azimuth difference in radians
    real(8)            :: refAltitude                ! altitude where these directions are specified

    integer            :: nGauss                     ! number of Gaussian division points integration polar angles
    integer            :: nmutot                     ! total number of polar angles
    real(8), pointer   :: ug(:)            => null() ! (nGauss) gaussian divison points on (0,1)
    real(8), pointer   :: wg(:)            => null() ! (nGauss) gaussian weights on (0,1)
    real(8), pointer   :: u(:)             => null() ! (nGauss + 2) mu values
    real(8), pointer   :: w(:)             => null() ! (nGauss + 2) special weights: sqrt(2*wg*ug) or 1
                                                     ! used for supermatrices
    real(8), pointer   :: u_sorted(:)      => null() ! used for LUT, contains sorted values
    integer, pointer   :: index_sort(:)    => null() ! u(index_sort(i))=u_sorted(i); index array for sorted values
  end type geometryType

  type LambertianType ! specification of a Lambertian surface, Lambertian cloud, or Lambertian aerosol layer
    logical             :: fitAlbedo                       ! flag for fitting the Lambertian albedo taken from the
                                                           ! configuration file
    logical             :: fitEmission                     ! flag for fitting the Lambertian emission taken from the
                                                           ! configuration file
    logical             :: fitSurfacePressure              ! flag for fitting surface pressure
    logical             :: updateAlbedo                    ! although fitAlbedo might be true, during the fitting
                                                           ! parameter values may change and updating the albedo
                                                           ! is not a good idea. e.g. when the cloud fraction
                                                           ! is larger than 1 the surface albedo should not be updated,
                                                           ! or if the cloud fraction is very small the cloud albedo
                                                           ! should not be updated.
    integer             :: nwavelAlbedo                    ! number of wavelength where the albedo is specified
    integer             :: nAlbedo                         ! number of albedo values
    integer             :: nwavelEmission                  ! number of wavelength where the emission is specified
    integer             :: nEmission                       ! number of emission values
    integer             :: nVarianceAlbedo                 ! number of variance values for albedo
    integer             :: nVarianceEmission               ! number of variance values for emission
    integer             :: ncorrelationRow                 ! number of correlation values in a row
    integer             :: ncorrelationCol                 ! number of correlation values in a column
    real(8), pointer    :: wavelAlbedo(:)        => null() ! (nwavelAlbedo) wavelemgths where albedo is specified
    real(8), pointer    :: albedo(:)             => null() ! (nAlbedo) albedo at wavelSpec(:)
    real(8), pointer    :: varianceAlbedo(:)     => null() ! (nVarianceAlbedo) variance of the albedo at wavelSpec(:)
    real(8), pointer    :: albedoAP(:)           => null() ! (nAlbedo) a-priori albedo at wavelSpec(:)
    real(8), pointer    :: varianceAlbedoAP(:)   => null() ! (nVarianceAlbedo) a-priori variance of the albedo
    real(8), pointer    :: correlationAP(:,:)    => null() ! (ncorrelationCol, ncorrelationRow) AP correlation albedo values
    real(8), pointer    :: wavelEmission(:)      => null() ! (nwavelEmission) wavelemgths where emission is specified (nm)
    real(8), pointer    :: emission(:)           => null() ! (nEmission) emission at wavelSpec(:) in 10^12 ph/s/cm2/nm/sr
    real(8), pointer    :: varianceEmission(:)   => null() ! (nVarianceEmission) variance emission in 10^24 [ph/s/cm2/nm/sr]**2
    real(8), pointer    :: emissionAP(:)         => null() ! (nEmission) a priori emission in 10^12 ph/s/cm2/nm/sr
    real(8), pointer    :: varianceEmissionAP(:) => null() ! (nVarianceEmission) a priori variance emission in
                                                           ! 10^24 [ph/s/cm2/nm/sr]**2
    real(8)             :: altitude                        ! altitude of the Lamberitan surface in km w.r.t. geoid
    real(8)             :: pressure                        ! pressure of the surface/cloud in hPa
    real(8)             :: temperature                     ! pressure of the surface/cloud in Kelvin
    real(8)             :: varPressure                     ! variance of the surface/cloud pressure in hPa**2
    real(8)             :: pressureAP                      ! a priori pressure of the surface/cloud in hPa
    real(8)             :: varPressureAP                   ! a priori variance of the surface/cloud pressure in hPa**2
  end type LambertianType

  type mieScatType
    integer            :: numExpCoef                 ! the number of expansion coefficients (0, 1, ..,numExpCoef)
    integer            :: nwavel                     ! (numComponents) number of wavelengths
    real(8)            :: tau(maxNumIntervals)       ! optical thickness for the intervals at 550 nm
    real(8)            :: tauAP(maxNumIntervals)     ! a priori optical thickness for the intervals at 550 nm
    real(8)            :: Cext550nm                  ! extinction cross section at 550 nm
    real(8)            :: volume                     ! average volume in micrometer**3 per particle
    real(8)            :: numberParticles            ! integrated number of particles
    real(8)            :: reff                       ! effective radius in micrometer
    real(8), pointer   :: wavel(:)         => null() ! (nwavel) wavelengths
    real(8), pointer   :: Cext(:)          => null() ! (nwavel) average extinction cross section
    real(8), pointer   :: a(:)             => null() ! (nwavel) single scattering albedo
    real(8), pointer   :: expCoef(:,:,:)   => null() ! (6,0:numExpCoef,nwavel) expansion coefficients
                                                     ! alpha1, alpha2, alpha3, alpha4, beta11, and beta2
    character(LEN=250)  :: fileNameExpCoef            ! file name containing expansion coefficients,
  end type mieScatType

  type mulOffsetType ! note nwavel, nvalues, and nvariance MUST all be equal
    logical          :: fitMulOffset                       ! if true multiplicative offset is fitted in the retrieval
    logical          :: useLinearInterpolation             ! if true linear interpolation else polynomial interpolation
    integer          :: nwavel                             ! number of wavelength where straylight is specified
    integer          :: nvalues                            ! number of values used for specification of straylight
    integer          :: nvariances                         ! number of values used for specification of the variance
    real(8), pointer :: wavel(:)                 => null() ! (nwavel) wavelengths where straylight is specified (in nm)
    real(8), pointer :: radiance(:)              => null() ! (nwavel) radiance at wavelengths in wavel(:)
    real(8), pointer :: offsetNodes(:)           => null() ! (nwavel) radiance times percentOffset
    real(8), pointer :: percentOffsetAP(:)       => null() ! (nvalues) a-priori percent offset
    real(8), pointer :: percentOffset(:)         => null() ! (nvalues) percent offset
    real(8), pointer :: percentOffsetPrev(:)     => null() ! (nvalues) previous offset in percent
    real(8), pointer :: varMulOffsetAP(:)        => null() ! (nvariances) a-priori variance (percent squared)
    real(8), pointer :: varMulOffset(:)          => null() ! (nvariances) variance (percent squared)
  end type mulOffsetType

  type O3ClimType
    character(LEN=250) :: temperatureClimFileName             ! string that identifies the temperature climatology
    character(LEN=250) :: ozoneClimFileName                   ! string that identifies the temperature climatology
    ! actual values for latitude, total ozone column, month, and day of month
    real(8)           :: latitude                            ! latitude (-80, 90)
    real(8)           :: totalOzoneColumn                    ! total ozone column [molecules / cm2]
    integer           :: month                               ! month (Jan = 1, Febr = 2, ..., Dec = 12)
    integer           :: day_of_month                        ! 1 - 30 if 31 truncated to 30
    ! a priori variance for temperature and
    real(8)           :: varianceTemp                        ! variance of the temperature [K**2] - the same for all altitudes
    real(8)           :: relError_vmr                        ! relative error for the vmr in percent the same for all altitudes
    ! dimensions for storing climatology
    integer           :: dim_months_clim
    integer           :: dim_lat_clim
    integer           :: dim_total_column_clim
    integer           :: dim_pressures_clim
    integer           :: dim_pressures_vmr                    ! number of midpressures for vmr
    ! nodes for latitudes, total ozone columns,
    integer, pointer  :: nodes_months(:)            => null() ! (dim_pressures_clim) pressure levels for climatology
    real(8), pointer  :: nodes_latitudes(:)         => null() ! (dim_lat_clim) latitudes in degrees
    real(8), pointer  :: nodes_total_column(:)      => null() ! (dim_total_column_clim) total ozone columns in DU
    real(8), pointer  :: nodes_pressures(:)         => null() ! (dim_pressures_clim) pressure levels for climatology
    real(8), pointer  :: nodes_pressures_vmr(:)     => null() ! (dim_pressures_vmr pressure levels for vmr
    ! temperature and ozone climatology
    real(8), pointer  :: clim_temperature(:,:,:)    => null()
                       ! (dim_pressures_clim, dim_lat_clim, dim_months_clim) temp climatology
    real(8), pointer  :: clim_O3(:,:,:,:)           => null()
                       ! (dim_pressures_clim, dim_total_column_clim, dim_lat_clim, dim_months_clim)
  end type O3ClimType

  type optPropRTMGridType
    ! optical properties at two grids : RTM grid and sublayer grid
    ! and the average optical properties for the layers on the RTM grid
    ! Note that the volume scattering and absorption coefficients are generally discontineous
    ! across the boundaries of intervals. The values for the interfaces between the layer pertain
    ! to the layer above the interface. When integration over sublayers is used to calculate layer
    ! properties, this is no problem because the integration weight is zero then.
    ! However, sometimes the value at an interval boundary is needed, e.g. to determine the
    ! derivative w.r.t. altitude of the special layer where the cloud optical thickness can be fitted.
    ! For that purpose we store the upper and lower limit at the boundaries of the intervals.
    integer          :: dimSV                             ! dimension Stokes vector
    integer          :: nTrace                            ! number of trace gases
    integer          :: ninterval                         ! number of intervals
    real(8), pointer :: intervalBounds(:)       => null() ! (0:ninterval) boundaries of the intervals
    real(8)          :: lowerBoundFitInterval             ! lower bound of fit interval, e.g. cloud base
    real(8)          :: upperBoundFitInterval             ! upper bound of fit interval, e.g. cloud top
    integer          :: indexRTMFitIntTop                 ! index for the top of the fit interval -RTM layer grid
    integer          :: indexRTMFitIntBot                 ! index for the bottom of the fit interval -RTM layer grid
    integer          :: indexRTMSubFitIntTop              ! index for the top of the fit interval -RTM sublayer grid
    integer          :: indexRTMSubFitIntBot              ! index for the bottom of the fit interval -RTM sublayer grid
    ! ksca and kabs may be discontinuous  at the interfaces between the intervals => store upper and lower limits
    real(8), pointer :: kscaIntAboveGas(:)      => null() ! (0:ninterval) ksca just above the boundary - gas
    real(8), pointer :: kabsIntAboveGas(:)      => null() ! (0:ninterval) kabs just above the boundary - gas
    real(8), pointer :: kscaIntBelowGas(:)      => null() ! (0:ninterval) ksca just below the boundary - gas
    real(8), pointer :: kabsIntBelowGas(:)      => null() ! (0:ninterval) kabs just below the boundary - gas
    real(8), pointer :: kscaIntAboveAer(:)      => null() ! (0:ninterval) ksca just above the boundary - aerosol
    real(8), pointer :: kabsIntAboveAer(:)      => null() ! (0:ninterval) kabs just above the boundary - aerosol
    real(8), pointer :: kscaIntBelowAer(:)      => null() ! (0:ninterval) ksca just below the boundary - aerosol
    real(8), pointer :: kabsIntBelowAer(:)      => null() ! (0:ninterval) kabs just below the boundary - aerosol
    real(8), pointer :: kextKextAerdiv550(:)    => null() ! (ninterval) kext / kext at 550 nm for aerosol
    real(8), pointer :: kscaIntAboveCld(:)      => null() ! (0:ninterval) ksca just above the boundary - cloud
    real(8), pointer :: kabsIntAboveCld(:)      => null() ! (0:ninterval) kabs just above the boundary - cloud
    real(8), pointer :: kscaIntBelowCld(:)      => null() ! (0:ninterval) ksca just below the boundary - cloud
    real(8), pointer :: kabsIntBelowCld(:)      => null() ! (0:ninterval) kabs just below the boundary - cloud
    real(8), pointer :: kextKextClddiv550(:)    => null() ! (ninterval) kext / kext at 550 nm  for cloud
    integer, pointer :: maxExpCoefLay(:)        => null() ! (RTMlayer) maximum number of coefficient for the RTM layers
    integer          :: maxExpCoef                        ! maximum number of phase function coefficients overall
    integer          :: maxExpCoefGas                     ! maximum number of phase function coefficients for molecules
    integer          :: maxExpCoefAer                     ! maximum number of phase function coefficients for aerosol
    integer          :: maxExpCoefCld                     ! maximum number of phase function coefficients for cloud
    integer          :: RTMnlayer                         ! number of layers on RTM grid
    real(8), pointer :: RTMaltitude(:)          => null() ! (0:RTMnlayer) altitudes of layer grid
    real(8), pointer :: RTMweight(:)            => null() ! (0:RTMnlayer) gaussian weights on layer grid
    real(8), pointer :: ndensGas(:,:)           => null() ! (0:RTMnlayer, nTrace) number density for trace gas on RTM grid
    real(8), pointer :: RTMndensAir(:)          => null() ! (0:RTMnlayer) number density of air in molecules / cm3
    real(8), pointer :: RTMtemperature(:)       => null() ! (0:RTMnlayer) temperature on RTM grid in Kelvin
    real(8), pointer :: RTMpressure(:)          => null() ! (0:RTMnlayer) pressure on RTM grid in hPa

! JdH arrays for column grid NOTE: these are allocated in subcolumnModule:fillAltitudeGridCol
    integer          :: nGaussCol                         ! total number of Gauss points for calculating column properties
    real(8), pointer :: Colaltitude(:)          => null() ! (nGaussCol) altitudes on column grid
    real(8), pointer :: Colweight(:)            => null() ! (nGaussCol) gaussian weights on column grid
    real(8), pointer :: ColXsec(:,:)            => null() ! (nGaussCol,nTrace) absorption cross section on column grid [cm2]
    real(8), pointer :: Colndens(:,:)           => null() ! (nGaussCol,nTrace) number density on column grid (molecules cm-3)
    real(8), pointer :: ColndensAP(:,:)         => null() ! (nGaussCol,nTrace) a-priori number density on column grid (mol cm-3)
    real(8), pointer :: ColndensAir(:)          => null() ! (nGaussCol) number density of air on column grid (molecules cm-3)
    real(8), pointer :: ksca(:)                 => null() ! (0:RTMnlayer) ksca on RTM layer grid [km-1]
    real(8), pointer :: kabs(:)                 => null() ! (0:RTMnlayer) kabs on RTM layer grid [km-1]
    real(8), pointer :: kext(:)                 => null() ! (0:RTMnlayer) kext on RTM layer grid [km-1]
    real(8), pointer :: ssaLay(:)               => null() ! (RTMnlayer) single scattering albedo for the layers
    real(8), pointer :: opticalThicknLay(:)     => null() ! (RTMnlayer) optical thickness for the layers (gas + particles)
    ! average coefficients for each homogeneous layer
    real(8), pointer :: phasefCoefLay(:,:,:,:)  => null() ! (dimSV, dimSV, 0:maxExpCoef, RTMnlayer)
    ! the expansion coefficients can be discontinuous  at the interfaces between the layers, namely there where
    ! the interface coincides with an interval boundary. Distinguish upper and lower limits.
    real(8), pointer :: phasefCoefAbove(:,:,:,:)    => null() ! (dimSV, dimSV, 0:maxExpCoef   , 0:RTMnlayer)
    real(8), pointer :: phasefCoefGas(:,:,:,:)      => null() ! (dimSV, dimSV, 0:maxExpCoefGas, 0:RTMnlayer)
    real(8), pointer :: phasefCoefRRS(:,:,:)        => null() ! (dimSV, dimSV, 0:maxExpCoefGas)
    real(8), pointer :: phasefCoefAerAbove(:,:,:,:) => null() ! (dimSV, dimSV, 0:maxExpCoefAer, 0:RTMnlayer)
    real(8), pointer :: phasefCoefAerBelow(:,:,:,:) => null() ! (dimSV, dimSV, 0:maxExpCoefAer, 0:RTMnlayer)
    real(8), pointer :: phasefCoefCldAbove(:,:,:,:) => null() ! (dimSV, dimSV, 0:maxExpCoefAer, 0:RTMnlayer)
    real(8), pointer :: phasefCoefCldBelow(:,:,:,:) => null() ! (dimSV, dimSV, 0:maxExpCoefAer, 0:RTMnlayer)
    integer          :: nGaussLay                           ! number of gaussian division points for subgrid
    integer          :: RTMnlayerSub                        ! total number of sublayers (nGaussLay+1)*RTMnlayer
    real(8), pointer :: RTMaltitudeSub(:)         => null() ! (0:RTMnlayerSub) altitudes of sublayer grid
    real(8), pointer :: RTMweightSub(:)           => null() ! (0:RTMnlayerSub) gaussian weights of sublayer grid
    real(8), pointer :: tempSub(:)                => null() ! (0:RTMnlayerSub) temperature on sublayer grid [K]
    real(8), pointer :: kscaSub(:)                => null() ! (0:RTMnlayerSub) ksca on sublayer grid [km-1]
    real(8), pointer :: kabsSub(:)                => null() ! (0:RTMnlayerSub) kabs on sublayer grid [km-1]
    real(8), pointer :: kextSub(:)                => null() ! (0:RTMnlayerSub) kext on sublayer grid [km-1]
    real(8), pointer :: ndensSubGas(:,:)          => null() ! (0:RTMnlayerSub,nTrace)
                                                            ! number density for trace gas on subgrid [molecules cm-3]
    real(8), pointer :: vmrSubGas(:,:)            => null() ! (0:RTMnlayerSub,nTrace)
                                                            ! volume mixing ratio for trace gas on subgrid [ppmv]
    real(8), pointer :: XsecSubGas(:,:)           => null() ! (0:RTMnlayerSub,nTrace) absorption
                                                            ! cross section for trsce gas on subgrid [cm2]
    real(8), pointer :: dXsecdTSubGas(:,:)        => null() ! (0:RTMnlayerSub,nTrace) temperature derivative of
                                                            ! absorption cross section for trsce gas [cm2 K-1]
    real(8), pointer :: kabsSubGas(:)             => null() ! (0:RTMnlayerSub) kabs for gas on subgrid [km-1]
    real(8), pointer :: kextSubGas(:)             => null() ! (0:RTMnlayerSub) kext for gas on subgrid  [km-1]
    real(8), pointer :: kextSubAer(:)             => null() ! (0:RTMnlayerSub) kext for aerosol on subgrid [km-1]
    real(8), pointer :: kextSubCld(:)             => null() ! (0:RTMnlayerSub) kext for cloud on subgrid [km-1]
  end type optPropRTMGridType

  type polCorrectionType ! correction for polarization and RRS
! to be applied to the earth radiance in the retrieval after convolution with the slit function
    character(LEN=250)  :: polarizationCorrectionFileName ! file name with polarization correction data
    logical             :: usePolarizationCorrection      ! flag for using polarization correction

    integer             :: nwavel                         ! number of wavelengths in LUT
    integer             :: nsurfaceAlbedo                 ! number of albedo values in LUT
    integer             :: nmu                            ! number of viewing angles (mu values) in LUT
    integer             :: nmu0                           ! number of solar zenith angles (mu0 values) in LUT
    integer             :: nozoneColumn                   ! number of column values for O3 (in DU)
    integer             :: nsurfacePressure               ! number of surface pressure values in LUT
    integer             :: nlatitude                      ! number of latitude values (different O3 profiles)

    real, pointer       :: wavel(:)             => null() ! (nwavel) wavelemgths for polarization correction data (nm)
    real, pointer       :: surfaceAlbedo(:)     => null() ! (nalbedo) albedo values in LUT
    real, pointer       :: mu(:)                => null() ! (nmu) cosine of viewing angles in LUT
    real, pointer       :: mu0(:)               => null() ! (nmu0) cosine of solar zenith angles in LUT
    real, pointer       :: ozoneColumn(:)       => null() ! (ncolumn) ozone column in LUT
    real, pointer       :: surfacePressure(:)   => null() ! (nsurfacePressure) surface pressure values in LUT
    real, pointer       :: latitude(:)          => null() ! (nlatitude) latitude in LUT

    real, pointer       :: correction(:)        => null()  ! (nwavel) used when read from text file
    real, pointer       :: SDCorrection(:)      => null()  ! (nwavel) derivatives for spline interpolation
    real, pointer       :: correctionLUT0(:,:,:,:,:,:,:) => null()  ! correction for Fourier coefficient 0 in %
    real, pointer       :: correctionLUT1(:,:,:,:,:,:,:) => null()  ! correction for Fourier coefficient 1 in %
    real, pointer       :: correctionLUT2(:,:,:,:,:,:,:) => null()  ! correction for Fourier coefficient 2 in %
! The dimensions of the three correctionLUTs are
! ( nsurfacealbedo, nmu, nmu0, nozoneColumn, nsurfacePressure, nlatitude, nwavel)
! here nwavel is last because we interpolate lineairly on the other parameters and use spline
! interpolation for the wavelength
  end type polCorrectionType

  type reflDerivType                                      ! all values are for the high resolution grid
    integer          :: nwavel                            ! number of wavelengths
    integer          :: RTMnlayer                         ! number of layers used in radiative transfer calculations
    integer          :: nstate                            ! total number of state vector elements
    integer          :: nstateCol                         ! total number of state vector elements for the (sub)columns
    integer          :: nTrace                            ! number of trace gases
    integer          :: nGaussCol                         ! # Gauss points for entire column
    integer          :: dimSV                             ! dimension Stokes vector
    real(8), pointer :: bsca(:)                 => null() ! (nwavel) scattering optical thickness used for interpolation
    real(8), pointer :: babs(:)                 => null() ! (nwavel) absorption optical thickness used for interpolation
    real(8), pointer :: depolarization(:)       => null() ! (nwavel) depolarization factor
    real(8), pointer :: refl(:,:)               => null() ! (dimSV, nwavel) reflecctance spectrum - tot
    real(8), pointer :: refl_clr(:,:)           => null() ! (dimSV, nwavel) monochromatic reflecctance spectrum - clear
    real(8), pointer :: refl_cld(:,:)           => null() ! (dimSV, nwavel) monochromatic reflecctance spectrum - cloud
    real(8), pointer :: refl_ns(:,:)            => null() ! (dimSV, nwavel) monochromatic reflecctance spectrum - clear
    real(8), pointer :: refl_ns_clr(:,:)        => null() ! (dimSV, nwavel) monochromatic reflecctance spectrum - clear
    real(8), pointer :: refl_ns_cld(:,:)        => null() ! (dimSV, nwavel) monochromatic reflecctance spectrum - cloud
    real(8), pointer :: refl_elas(:,:)          => null() ! (dimSV, nwavel) elastic part of reflecctance spectrum - tot
    real(8), pointer :: refl_inelas(:,:)        => null() ! (dimSV, nwavel) inelastic part of reflecctance spectrum - tot
    real(8), pointer :: refl_elas_clr(:,:)      => null() ! (dimSV, nwavel) elastic part of the reflectance - clear
    real(8), pointer :: refl_elas_cld(:,:)      => null() ! (dimSV, nwavel) elastic part of the reflectance - cloud
    real(8), pointer :: refl_inelas_clr(:,:)    => null() ! (dimSV, nwavel) inelastic part of the reflectance - clear
    real(8), pointer :: refl_inelas_cld(:,:)    => null() ! (dimSV, nwavel) inelastic part of the reflectance - cloud
    real(8), pointer :: contribRefl(:,:,:)      => null() ! (dimSV, nwavel, 0:RTMnlayer) altitude resolved path radiance
    real(8), pointer :: intFieldUp(:,:,:)       => null() ! (dimSV, nwavel, 0:RTMnlayer) internal fields for given SZA and VSA
    real(8), pointer :: intFieldDown(:,:,:)     => null() ! (dimSV, nwavel, 0:RTMnlayer) internal fields for given SZA and VSA
    real(8), pointer :: polCorrection(:)        => null() ! (nwavel) polarization correction for reflectance
    real(8), pointer :: altResAMFabs_clr(:,:)   => null() ! (nwavel, 0:RTMnlayer) altitude resolved air mass factor
    real(8), pointer :: altResAMFabs_cld(:,:)   => null() ! (nwavel, 0:RTMnlayer) altitude resolved air mass factor
    real(8), pointer :: altResAMFscaAer_clr(:,:)=> null() ! (nwavel, 0:RTMnlayer) altitude resolved air mass factor
    real(8), pointer :: altResAMFscaAer_cld(:,:)=> null() ! (nwavel, 0:RTMnlayer) altitude resolved air mass factor
    real(8), pointer :: K_lnvmr(:,:)            => null() ! (nwavel, nstate) derivative w.r.t. state vector elements
    real(8), pointer :: K_clr_lnvmr(:,:)        => null() ! (nwavel, nstate) derivative w.r.t. state vector elements
    real(8), pointer :: K_cld_lnvmr(:,:)        => null() ! (nwavel, nstate) derivative w.r.t. state vector elements
    real(8), pointer :: K_vmr(:,:)              => null() ! (nwavel, nstate) derivative w.r.t. state vector elements
    real(8), pointer :: K_clr_vmr(:,:)          => null() ! (nwavel, nstate) derivative w.r.t. state vector elements
    real(8), pointer :: K_cld_vmr(:,:)          => null() ! (nwavel, nstate) derivative w.r.t. state vector elements
    real(8), pointer :: K_ndens(:,:)            => null() ! (nwavel, nstate) derivative w.r.t. state vector elements
    real(8), pointer :: K_clr_ndens(:,:)        => null() ! (nwavel, nstate) derivative w.r.t. state vector elements
    real(8), pointer :: K_cld_ndens(:,:)        => null() ! (nwavel, nstate) derivative w.r.t. state vector elements
! JdH array for column grid NOTE: KHR_ndensCol is allocated in subcolumnModule:fillAltitudeGridCol
    real(8), pointer :: KHR_ndensCol(:,:)       => null() ! (nwavel, nstateCol) derivative w.r.t. number density column grid
    real(8), pointer :: KHR_clr_ndensCol(:,:)   => null() ! (nwavel, nstateCol) derivative w.r.t. number density column grid
    real(8), pointer :: KHR_cld_ndensCol(:,:)   => null() ! (nwavel, nstateCol) derivative w.r.t. number density column grid
  end type reflDerivType

  type retrType ! parameters used in the retrieval

    integer                      :: maxfitparameters                 ! number of all parameters that can be fitted
    character(LEN = 50), pointer :: codeFitParameters(:)      => null() ! (1:maxfitparameters) code for fit parameters
    integer, pointer             :: codeSpecBand(:)           => null() ! (1:maxfitparameters) spectral band number
    integer, pointer             :: codeTraceGas(:)           => null() ! (1:maxfitparameters) trace gas number
    integer, pointer             :: codeAltitude(:)           => null() ! (1:maxfitparameters) altitude index
    integer, pointer             :: codeIndexSurfAlb(:)       => null() ! (1:maxfitparameters) index surface albedo
    integer, pointer             :: codeIndexSurfEmission(:)  => null() ! (1:maxfitparameters) index surface emission
    integer, pointer             :: codeIndexLambCldAlb(:)    => null() ! (1:maxfitparameters) index cloud albedo
    integer, pointer             :: codeIndexCloudFraction(:) => null() ! (1:maxfitparameters) index cloud fraction
    integer, pointer             :: codeIndexMulOffset(:)     => null() ! (1:maxfitparameters) index multiplicative offset
    integer, pointer             :: codeIndexStraylight(:)    => null() ! (1:maxfitparameters) index straylight
    integer, pointer             :: codelnPolyCoef(:)         => null() ! (1:maxfitparameters) index for DOAS poly coeff

    integer          :: maxNumIterations               ! maximal number of iterations during optimal estimation
    integer          :: numIterations                  ! number of iterations during optimal estimation
    real(8)          :: maxChangeTransfState           ! maximal allowed change in transformed state vector
    integer          :: nwavelRetr                     ! number of instrument wavelengths used in the retrieval
    real(8), pointer :: wavelRetr(:)         => null() ! (nwavelRetr) wavelengths used in the retrieval
    integer          :: nstate                         ! number of state vector elements
    integer          :: nstate_other                   ! number of state vector elements minus profile nodes
    integer          :: nSa_not_diag                   ! number of state vector elements where Sa is not diagonal
    integer          :: nTrace                         ! number of absorbing gases
    integer          :: ncolumnfit                     ! number of trace gases whose column is fitted
    integer          :: nstateCol                      ! number of state vector elements for (sub)columns
    integer          :: nGaussCol                      ! # Gauss points for entire column
    integer          :: stateVectorElementAdjusted     ! index of first state vector element that was adjusted due to boundary
    real(8), pointer :: x(:)                 => null() ! (nstate) new state vector
    real(8), pointer :: x_not_adj(:)         => null() ! (nstate) adjusted state vector, e.g. due to boundaries
    real(8), pointer :: xPrev(:)             => null() ! (nstate) previous state vector
    real(8), pointer :: x_stored(:,:)        => null() ! (nstate, maxNumIterations) archive for state vector elements
    real(8), pointer :: x_upperBound(:)      => null() ! (nstate) upper bound for x
    real(8), pointer :: x_lowerBound(:)      => null() ! (nstate) upper bound for x
    real(8), pointer :: chisq_stored(:)      => null() ! (maxNumIterations) archive for chi squared
    real(8), pointer :: xConv_stored(:)      => null() ! (maxNumIterations) archive for convergence criterion
    real(8), pointer :: xa(:)                => null() ! (nstate) a-priori state vector
    real(8), pointer :: dx(:)                => null() ! (nstate) difference with a-priori : x - xa
    real(8), pointer :: x_true(:)            => null() ! (nstate) true state vector on retrieval grid
    real(8), pointer :: SNrefspec(:)         => null() ! (nwavelRetr) signal to noise for the reference spectrum
    real(8), pointer :: reflPrev(:)          => null() ! (nwavelRetr) previous reflectance
    real(8), pointer :: refl_clr(:)          => null() ! (nwavelRetr) reflectance (no noise nor offsets) - clear part
    real(8), pointer :: refl_cld(:)          => null() ! (nwavelRetr) reflectance (no noise nor offsets) - cloudy part
    real(8), pointer :: refl(:)              => null() ! (nwavelRetr) reflectance (no noise nor offsets)
    real(8), pointer :: reflMeas(:)          => null() ! (nwavelRetr) simulated measured reflectance with noise and offsets
    real(8), pointer :: reflNoiseError(:)    => null() ! (nwavelRetr) standard deviation of the reflectance noise error
    real(8), pointer :: reflCalibErrorAdd(:) => null() ! (nwavelRetr) additive calibration error for reflectance
    real(8), pointer :: reflCalibErrorMul(:) => null() ! (nwavelRetr) multiplicative calibration error for reflectance

    real(8), pointer :: dR_initial(:)        => null() ! (nwavelRetr) initial residue: reflMeas - refl

    real(8), pointer :: dR(:)                => null() ! (nwavelRetr) residue: reflMeas - refl
    real(8), pointer :: K_clr_lnvmr(:,:)     => null() ! (nwavelRetr, nState) derivative refl w.r.t. state vector elements
    real(8), pointer :: K_cld_lnvmr(:,:)     => null() ! (nwavelRetr, nState) derivative refl w.r.t. state vector elements
    real(8), pointer :: K_lnvmr(:,:)         => null() ! (nwavelRetr, nState) derivative refl w.r.t. state vector elements
    real(8), pointer :: K_vmr(:,:)           => null() ! (nwavelRetr, nState) derivative refl w.r.t. state vector elements
    real(8), pointer :: K_ndens(:,:)         => null() ! (nwavelRetr, nState) derivative refl w.r.t. state vector elements
    real(8), pointer :: Se(:,:)              => null() ! (nwavelRetr, nwavelRetr) error covariance of measured reflectance
    real(8), pointer :: sqrtSe(:,:)          => null() ! (nwavelRetr, nwavelRetr) square root of Se
    real(8), pointer :: sqrtInvSe(:,:)       => null() ! (nwavelRetr, nwavelRetr) inverse square root of Se
    real(8), pointer :: InvSe(:,:)           => null() ! (nwavelRetr, nwavelRetr) inverse of Se
    real(8), pointer :: Sa_lnvmr(:,:)        => null() ! (nstate, nstate) a-priori covariance matrix
    real(8), pointer :: Sa_vmr(:,:)          => null() ! (nstate, nstate) a-priori covariance matrix
    real(8), pointer :: Sa_ndens(:,:)        => null() ! (nstate, nstate) a-priori covariance matrix
    real(8), pointer :: SInv_lnvmr(:,:)      => null() ! (nstate, nstate) inverse of covariance state vector S
    real(8), pointer :: SInv_vmr(:,:)        => null() ! (nstate, nstate) inverse of covariance state vector S
    real(8), pointer :: SInv_ndens(:,:)      => null() ! (nstate, nstate) inverse of covariance state vector S

    ! absorbing aerosol index (AAI)
    real(8)          :: AAI                            ! value of absorbing aerosol index (AAI)
    real(8)          :: varAAI                         ! variance AAI

! JdH arrays for column grid NOTE: these are allocated in subcolumnModule:fillAltitudeGridCol
    real(8), pointer :: K_ndensCol(:,:)      => null() ! (nwavelRetr, nstateCol) derivative refl w.r.t. for column grid
    real(8), pointer :: Sa_ndensCol(:,:)     => null() ! (nstateCol, nstateCol) a-priori covariance matrix on column grid
    real(8), pointer :: S_ndensCol(:,:)      => null() ! (nstateCol, nstateCol) covariance matrix on column grid
    real(8), pointer :: Snoise_ndensCol(:,:) => null() ! (nstateCol, nstateCol) noise covariance matrix on column grid
    real(8), pointer :: A_ndensCol(:,:)      => null() ! (nstateCol, nstateCol) averaging kernel on column grid
    real(8), pointer :: G_ndensCol(:,:)      => null() ! (nstateCol, nwavelRetr) gain matrix on column grid
    ! end parameters for (sub)columns

    real(8)          :: rmse                ! root mean square error of fit
    real(8)          :: chi2                ! (R-Rprev)T Se-1 (R-Rprev)  + (x-xa)T Sa-1 (x-xa) (T = transposed)
    real(8)          :: chi2R               ! (R-Rprev)T Se-1 (R-Rprev) (T = transposed)
    real(8)          :: chi2x               ! (x-xa)T Sa-1 (x-xa) (T = transposed)
    real(8)          :: xConv               ! (x-xPrev)T S-1 (x-xPrev) / nstate
    real(8)          :: xConvBoundary       ! (x-xPrev)T S-1 (x-xPrev) / nstate after change due to boundary
    real(8)          :: xConvThreshold      ! if (x-xPrev)T S-1 (x-xPrev) < xConvThreshold isConverged is set to .true.
    logical          :: isConverged         ! flag to denote that we have a converged solution
    logical          :: SNRnormal           ! flag to denote that we work with normal (not reduced) signal to noise ratio
    logical          :: isConvergedBoundary ! flag to denote that we have convergence at a boundary
    logical          :: stateVectorAdjusted ! flag to denote that the state vector is adjusted
    logical          :: scale_delta_x_when_adj_x  ! flag to denote that delta x (= x - xprevious) is scaled
                                                  ! when x is adjusted due to boundaries. Ensure that we do not
                                                  ! cross boundaries and scale all elements of the state vector.
                                                  ! if flag is 0 adjust only individual elements to boundary values
                                                  ! and do not scale delta x.
  end type retrType

  type RRS_RingType
    logical          :: useCabannes                ! if .true. use Cabannes scattering else Rayleigh scattering
    logical          :: useRRS                     ! RRS is calculated, not fitted or added
    logical          :: approximateRRS             ! use approximation as in OMI O3 profile algorithm
    real(8)          :: fractionRamanLines         ! fraction of the Raman lines that are used in the convolution
    integer          :: nwavelHR                   ! number of wavelengths on the HR (integration) grid
    integer          :: nwavel                     ! number of wavelengths on the instrument grid
    integer          :: degreePoly                 ! degree of polynomial used to calculate the differential Ring spectrum
    logical          :: fitDiffRingSpec            ! fit differential Ring spectrum (retrieval only)
    logical          :: fitRingSpec                ! fit Ring spectrum (retrieval only)
    logical          :: addDiffRingSpec            ! add differential Ring spectrum (simulation only)
    logical          :: addRingSpec                ! add Ring spectrum (simulation only)
    logical          :: includeAbsorption          ! modify Ring spectrum by absorption in the atmosphere
    real(8)          :: temperature                ! mean temperature of the molecules
    real(8), pointer :: radRingHR(:)     => null() ! (nwavelHR) radiance Ring spectrum on HR grid        IR
    real(8), pointer :: RingHR(:)        => null() ! (nwavelHR) Ring spectrum on HR grid                 IR / F
    real(8), pointer :: RTMradElasHR(:)  => null() ! (nwavelHR) elastic radiance from radiative transfer on HR grid IR
    real(8), pointer :: RTMradRingHR(:)  => null() ! (nwavelHR) Ring spectrum from radiative transfer on HR grid IR
    real(8), pointer :: RTMRingHR(:)     => null() ! (nwavelHR) Ring spectrum from radiative transfer on HR grid IR / F
    real(8), pointer :: RTM_FIHR(:)      => null() ! (nwavelHR) filling in on HR grid [joiner definition: (Iinel - Iel) / Iel]
    real(8), pointer :: radRing(:)       => null() ! (nwavel)   radiance Ring spectrum after convolution with slit  IR
    real(8), pointer :: Ring(:)          => null() ! (nwavel)   Ring spectrum after convolution with slit  IR / F
    real(8), pointer :: diffRing(:)      => null() ! (nwavel)   differential Ring spectrum                 IR / F - Polynomial
    real(8), pointer :: RTMradElas(:)    => null() ! (nwavel)   elastic radiance from radiative transfer on IR
    real(8), pointer :: RTMradRing(:)    => null() ! (nwavel)   Ring spectrum from radiative transfer IR
    real(8), pointer :: RTMRing(:)       => null() ! (nwavel)   Ring spectrum from radiative transfer IR / F
    real(8), pointer :: RTMdiffRing(:)   => null() ! (nwavel)   differential Ring spectrum from radiative transfer IR / F - Poly
    real(8), pointer :: RTM_FI(:)        => null() ! (nwavel)   filling in (joiner definition: (Iinel - Iel) / Iel ]
    real(8)          :: ringCoeffAP                ! a priori ring coefficient
    real(8)          :: ringCoeff                  ! ring coefficient
    real(8)          :: varRingCoeffAP             ! a priori variance ring coefficient
    real(8)          :: varRingCoeff               ! variance ring coefficient
  end type RRS_RingType

  type RTType     ! reflection and transmission properties of an individual homogeneous layer
                  ! for a certain wavelength and a certain Fourier term
                  ! with polarization the dimension is dimSV * nmutot
    real(8), pointer   :: R(:,:)   => null() ! (dimSV*nmutot, dimSV*nmutot) reflectance illumination top
    real(8), pointer   :: T(:,:)   => null() ! (dimSV*nmutot, dimSV*nmutot) diffuse transmittance illumination top
    real(8), pointer   :: Rst(:,:) => null() ! (dimSV*nmutot, dimSV*nmutot) reflectance for illumination top
    real(8), pointer   :: Tst(:,:) => null() ! (dimSV*nmutot, dimSV*nmutot) diffuse transmittance illumination top
  end type RTType

  integer, parameter :: maxInputBands  = 2
  integer, parameter :: maxInputLevels = 100
  integer, parameter :: maxTime = 12
  integer, parameter :: maxLat  = 30
  integer, parameter :: maxLev  = 30
  integer, parameter :: maxTCO3 = 10

  type inputType
    real                            :: solarZenithAngle
    real                            :: solarAzimuthAngle
    real                            :: instrumentNadirAngle
    real                            :: instrumentAzimuthAngle
    real                            :: surfacePressure
    real                            :: surfaceAltitude
    real                            :: latitude
    integer                         :: numBands
    integer                         :: yday
    integer                         :: numLevels2
    integer                         :: pixel_index

    real(8), dimension(maxInputBands)             :: cloudFraction
    real(8), dimension(maxInputBands)             :: cloudAlbedo
    real(8), dimension(maxInputBands)             :: cloudPressure
    real(8), dimension(:), allocatable            :: temperature          ! PT table
    real(8), dimension(:), allocatable            :: pressure             ! PT table
    real(8), dimension(maxInputLevels)            :: pressure2            ! pressures for trace gas
    integer, dimension(maxInputBands)             :: isrfband
    integer, dimension(maxInputBands)             :: isrfpixel
    integer, dimension(maxInputBands)             :: numAlbedos
    real(8), dimension(:), allocatable            :: radiance
    real(8), dimension(:), allocatable            :: radianceError
    real(8), dimension(:), allocatable            :: radianceNoise
    real(8), dimension(:), allocatable            :: radianceWavelength
    real(8), dimension(:), allocatable            :: irradiance
    real(8), dimension(:), allocatable            :: irradianceError
    real(8), dimension(:), allocatable            :: irradianceNoise
    real(8), dimension(:), allocatable            :: irradianceWavelength
    real(8), dimension(:), allocatable            :: surfaceAlbedo
    real(8), dimension(:), allocatable            :: surfaceAlbedoWavelength
    type(polCorrectionType), pointer              :: polCorrectionRetrS(:)      !  polarization + RRS correction data  for retrieval
  end type inputType

  type solarIrrType
    character(LEN=250)      :: solarIrrFileName
    type(signalToNoiseType) :: SNS                      ! S/N specification
    type(slitFunctionType)  :: slitFunctionSpecsS       ! specification of the slit function
    integer                 :: nwavelHR                 ! number of wavelengths HR (integration) grid
    integer                 :: nwavelMR                 ! number of wavelengths HR - instr grid
    integer                 :: nwavel                   ! number of wavelengths on instrument grid
    real(8), pointer        :: solIrrHR(:)    => null() ! (nwavelHR) solar irradiance on HR (integration) grid
    real(8), pointer        :: solIrrMR(:)    => null() ! (nwavelMR) solar irradiance on HR - instr grid
    real(8), pointer        :: solIrr(:)      => null() ! (nwavel) solar irradiance after convolution with slit
    real(8), pointer        :: solIrrMeas(:)  => null() ! (nwavel) solIrr + Gaussian noise added if addNoise = true
    real(8), pointer        :: solIrrError(:) => null() ! (nwavel) standard deviation of irradiance
    real(8), pointer        :: SN(:)          => null() ! (nwavel) signal to noise ratio
    real(8)                 :: SNmax                    !  maximum SNR for solar irradiance (noise floor)
    ! bias in measured irradiance
    ! features relate to diffuser features and are only used for the high-resolution irradiance
    real(8)  :: wavelShift         ! shift added to the HR wavelengths when interpolating on the irradiance
    real(8)  :: featurePeriodAdd   ! distance between 2 maxima in nm for the sinusoidal feature
    real(8)  :: featureAmplAdd     ! amplitude of spectral feature (sine) in percent at largest wavelength
    real(8)  :: featurePhaseAdd    ! phase of feature at largest wavelength in degree (0-180)
    real(8)  :: featurePeriodMul   ! distance between 2 maxima in nm for the sinusoidal feature
    real(8)  :: featureAmplMul     ! amplitude of spectral feature (sine) in percent at largest wavelength
    real(8)  :: featurePhaseMul    ! phase of feature at largest wavelength in degree (0-180)
    real(8)  :: offsetAdd          ! additive offset in percent of the irradiance at largest wavelength
    real(8)  :: offsetMul          ! multiplicative offset in percent of the irradiance at largest wavelength
    real(8)  :: percentSmear       ! imperfect charge transfer when reading CCD
  end type solarIrrType

  type straylightType ! note nwavel, nvalues, and nvariance MUST all be equal
    logical          :: fitStrayLight                      ! if true straylight is fitted in the retrieval
    logical          :: useLinearInterpolation             ! if true linear interpolation else polynomial interpolation
    logical          :: useReferenceSpectrum               ! if true straylight is percentage of reference spectrum
                                                           ! if false straylight is percentage of current spectrum
    logical            :: useCharacteristicBias            ! if true useCharacteristicBias is used
    character(LEN=250) :: fileNameCharacteristicBias
    integer          :: nwavelCB                           ! number of wavelengths on instrument grid
                                                           ! where characteristic bias is specified
    integer          :: nwavel                             ! number of wavelengths where straylight is specified
    integer          :: nvalues                            ! number of values used for specification of straylight
    integer          :: nvariances                         ! number of values used for specification of the variance
    real(8), pointer :: wavel(:)                 => null() ! (nwavel) wavelengths where straylight is specified (in nm)
    real(8), pointer :: radiance(:)              => null() ! (nwavel) radiance at wavelengths in wavel(:)
    real(8), pointer :: strayLightNodes(:)       => null() ! (nwavel) radiance times percentStrayLight
    real(8), pointer :: percentStrayLightAP(:)   => null() ! (nvalues) a-priori percent straylight
    real(8), pointer :: percentStrayLight(:)     => null() ! (nvalues) percent straylight
    real(8), pointer :: percentStrayLightPrev(:) => null() ! (nvalues) previous percent straylight
    real(8), pointer :: varStrayLightAP(:)       => null() ! (nvariances) a-priori variance (percent squared)
    real(8), pointer :: varStrayLight(:)         => null() ! (nvariances) variance (percent squared)
    real(8), pointer :: radianceCB(:)            => null() ! (nwavelCB) characteristic radiance bias
                                                           ! on instr wavel grid
  end type straylightType

  type subcolumnType
    real(8), pointer    :: column(:)        => null() ! (nTrace) column in Dobson units for each trace gas
    real(8), pointer    :: columnAP(:)      => null() ! (nTrace) a-priori column in Dobson units for each trace gas
    real(8), pointer    :: columnAir(:)     => null() ! (nTrace) column of air
    real(8), pointer    :: meanVmr(:)       => null() ! (nTrace) average value of the volume mixing ratio for the column
    real(8), pointer    :: meanVmrAP(:)     => null() ! (nTrace) average value of the volume mixing ratio for the column
    real(8), pointer    :: errorColAP(:)    => null() ! (nTrace) a-priori error for the column in percent
    real(8), pointer    :: errorCol(:)      => null() ! (nTrace) a-posteriori total relative error for the column in percent
    real(8), pointer    :: noiseErrorCol(:) => null() ! (nTrace) a-posteriori noise error for the column in percent
    real(8), pointer    :: AK(:,:)          => null() ! (nGaussCol, nTrace) averaging kernel
    real(8), pointer    :: AKx(:,:)         => null() ! (nGaussCol, nTrace) averaging kernel multiplied with number density
                                                      !  and divided by the sybcolumn. It gives the sensitivity in terms
                                                      !  of a relative change in the profile
  end type subcolumnType

  type traceGasType ! specification for one trace gas
    ! if the trace gas is a collision complex the column is the integral over the number density squared
    ! and the number density is the number density is the square of the number density of the parent gas
    ! e.g. numDens is the is nO2**2 when the trace gas is O2-O2 and nO2 is the number density of oxygen
    ! similarly vmr is then in ppmv**2
    logical           :: fitProfile                     ! if true the trace gas profile is fitted
    logical           :: fitColumnTrace                 ! if true the total column of the trace gas is fitted
    logical           :: useLinInterp                   ! if true linear interpolation on ln(vmr) else cubic spline
    logical           :: unitFlag                       ! if true the values are in DU if false in mol/cm2
    logical           :: useAPCorrLength                ! if .true. introduce correlation
    logical           :: removeAPcorrTropStrat          ! if .true. a-priori correlation between levels below and above
                                                        ! the tropopause are set to zero
    logical           :: scaleProfileToColumn           ! if .true. scale profile to agree with the total column
    logical           :: collisioncmplx                 ! if true the absorption coefficient is to be multiplied by the
                                                        ! square of the number density of the absorbing gas
    logical           :: useO3Climatology               ! if .true. use ozone profile climatology (TOMS-V8)
    real(8)           :: APcorrLength                   ! correlation length h in km (see Rodgers, 2000, Sec 2.6)
    real(8)           :: scaleFactorXsec                ! multiply absorption cross section with this factor
    integer           :: correlationIndex               ! 1 = lnvmr (normal), 2 = ndens (special)
    logical           :: useSaFromFile                  ! if .true. read Sa from file gasAtmRetrS%APcovarianceFileName
    logical           :: useSaDiagFromFile              ! if .true. use only the diagonal elements of the Sa read from file
    integer           :: nalt                           ! number of altitude for trace gases (nodes) - 1
    integer           :: nprof                          ! number of altitude for trace gases (nodes) - 1
    integer           :: nalt_vmrAP                     ! number of altitude for trace gases (nodes) - 1
    integer           :: nalt_errorAP                   ! number of altitude for trace gases (nodes) - 1
    integer           :: toplevelTroposphere            ! index for uppermost level in the troposphere on retrieval grid
    real(8), pointer  :: alt(:)               => null() ! (0:nalt) altitudes for the nodes of the trace gas
    real(8), pointer  :: altAP(:)             => null() ! (0:nalt) a-priori altitudes for the nodes of the trace gas
    real(8), pointer  :: pressure(:)          => null() ! (0:nalt) pressure at the nodes of the trace gas
    real(8), pointer  :: temperature(:)       => null() ! (0:nalt) temperature at the nodes of the trace gas
    real(8), pointer  :: temperatureAP(:)     => null() ! (0:nalt) temperature at the nodes of the trace gas
    real(8), pointer  :: numDensAir(:)        => null() ! (0:nalt) number density air at the nodes (molecules cm-3)
    real(8), pointer  :: numDensAirAP(:)      => null() ! (0:nalt) number density air at the nodes (molecules cm-3)
    real(8), pointer  :: numDens(:)           => null() ! (0:nalt) number density trace gas (molecules cm-3)
    real(8), pointer  :: numDensAP(:)         => null() ! (0:nalt) a-priori number density trace gas (molecules cm-3)
    real(8), pointer  :: numDensTrue(:)       => null() ! (0:nalt) true number density trace gas interpolated to retr grid
    real(8), pointer  :: numDensEst(:)        => null() ! (0:nalt) = numDensAP + Andens * (numDensTrue - numDensAP)
    real(8), pointer  :: covNumDens(:,:)      => null() ! (0:nalt,0:nalt) covariance of the number density
    real(8), pointer  :: covNumDensAP(:,:)    => null() ! (0:nalt,0:nalt) a-priori covariance of the number density
    real(8), pointer  :: vmr(:)               => null() ! (0:nalt) volume mixing ratio trace gas in ppmv
    real(8), pointer  :: vmrAP(:)             => null() ! (0:nalt) a-priori volume mixing ratio trace gas in ppmv
    real(8), pointer  :: covVmr(:,:)          => null() ! (0:nalt,0:nalt) covariance of the volume mixing ratio
    real(8), pointer  :: covVmrAP(:,:)        => null() ! (0:nalt,0:nalt) a-priori covariance for the vmr
    real(8), pointer  :: relErrorAP(:)        => null() ! (0:nalt) a-priori relative error in percent for vmr and ndens
    ! trace gas at special altitudes - only filled when profile is retrieved
    real(8)           :: vmrAtSurface                   ! vmr in ppmv at ground surface
    real(8)           :: vmrAtCloud                     ! vmr in ppmv at Lambertian cloud
    real(8)           :: vmrAtTropopause                ! vmr in ppmv at Lambertian cloud
    real(8)           :: vmrAtSurface_precision         ! vmr in ppmv at ground surface
    real(8)           :: vmrAtCloud_precision           ! vmr in ppmv at Lambertian cloud
    real(8)           :: vmrAtTropopause_precision      ! vmr in ppmv at Lambertian cloud
    real(8)           :: column                         ! total column
    real(8)           :: columnAboveCloud               ! column above cloud
    real(8)           :: columnAP                       ! a-priori total column
    real(8)           :: covColumnAP                    ! a-priori variance of the total column
    real(8)           :: relErrorColumnAP               ! a-priori error in the total column in percent
    real(8)           :: covColumn                      ! current variance of the total column
    ! for collision complex we need the values of the parent trace gas
    real(8), pointer  :: numDensParent(:)        => null() ! (0:nalt) number density parent trace gas (molecules cm-3)
    real(8), pointer  :: numDensParentAP(:)      => null() ! (0:nalt) a-priori number density trace gas (molecules cm-3)
    real(8), pointer  :: covNumDensParent(:,:)   => null() ! (0:nalt,0:nalt) covariance of the number density
    real(8), pointer  :: covNumDensParentAP(:,:) => null() ! (0:nalt,0:nalt) a-priori covariance of the number density
    real(8), pointer  :: vmrParent(:)            => null() ! (0:nalt) volume mixing ratio parent trace gas in ppmv
    real(8), pointer  :: vmrParentAP(:)          => null() ! (0:nalt) a-priori volume mixing ratio parent trace gas in ppmv
    real(8), pointer  :: covVmrParent(:,:)       => null() ! (0:nalt,0:nalt) covariance of the volume mixing ratio
    real(8), pointer  :: covVmrParentAP(:,:)     => null() ! (0:nalt,0:nalt) a-priori covariance for the vmr
    real(8), pointer  :: relErrParentAP(:)       => null() ! (0:nalt) a-priori relative error in percent for vmr parent gas
    real(8)           :: columnParent                      ! total parent column
    real(8)           :: columnParentAP                    ! a-priori total parent column
    real(8)           :: covColumnParentAP                 ! a-priori variance of the total parent column
    real(8)           :: relErrorColumnParentAP            ! a-priori error in the total parent column in percent
    real(8)           :: covColumnParent                   ! current variance of the total parent column
    character(LEN=250) :: nameTraceGas                      ! string that identifies the trace gas e.g. NO2
    character(LEN=250) :: APcovarianceFileName              ! file name for a-priori covariance matrix to be read
  end type traceGasType

  type UDLocalType  ! Fourier coefficient of the radiation field at the interface of a layer due to
                    ! scattering in that layer.
                    ! dimension '2' is for the two solar positions needed to calculate the derivatives
    real(8), pointer   :: D(:,:) => null() ! (dimSV*nmutot, 2)) downward diffuse light at interface
    real(8), pointer   :: U(:,:) => null() ! (dimSV*nmutot, 2)) upward dffuse light at interface
  end type UDLocalType

  type UDType     ! Fourier coefficients of dirrect (E) downward, diffuse downward (D) and upward (U)
                  ! traveling light for a specific interfaces between layers
                  ! dimension '2' is for the two solar positions needed to calculate the derivatives
    real(8), pointer   :: E(:)      => null() ! (dimSV*nmutot) attenuation of direct solar beam at the interface
    real(8), pointer   :: D(:,:)    => null() ! (dimSV*nmutot, 2) downward diffuse light at interface
    real(8), pointer   :: U(:,:)    => null() ! (dimSV*nmutot, 2) upward dffuse light at interface
  end type UDType

  type UDlutType  ! Fourier coefficients of dirrect (E) downward, diffuse downward (D) and upward (U)
                  ! traveling light for a specific interfaces between layers
                  ! all solar positions are taken into account - needed for RRS
    real(8), pointer   :: E(:)   => null() ! (dimSV*nmutot) attenuation of direct solar beam at the interface
    real(8), pointer   :: D(:,:) => null() ! (dimSV*nmutot, dimSV*nmutot) downward diffuse light at interface
    real(8), pointer   :: U(:,:) => null() ! (dimSV*nmutot, dimSV*nmutot) upward dffuse light at interface
  end type UDlutType

  type wavelHRType
    ! for line aborption spectra the wavelength grid is based on line positions
    integer            :: nGaussMax                  ! maximum number of Gausspoints per wavelength interval
    integer            :: nGaussMin                  ! minimum number of Gausspoints per wavelength interval
    integer            :: nGaussFWHM                 ! number of Gausspoints per FWHM if there is no line absorption
    integer            :: nwavel                     ! number of wavelengths for trace gas grid
    real(8), pointer   :: wavel(:)         => null() ! (nwavel) wavelengths for trace gas grid (monochromatic)
    real(8), pointer   :: weight(:)        => null() ! (nwavel) weights for gaussian integration over wavelength
    integer            :: nExclude                   ! number of excluded intervals within the band
    real(8)            :: excludeStart(nExcludeMax)  ! begin of excluded interval
    real(8)            :: excludeEnd(nExcludeMax)    ! end of excluded interval
  end type wavelHRType

  type wavelInstrType
    logical            :: calibrateWavelengths       ! if .true. the simulated (or read) wavelengths are calibrated
    real(8)            :: velocity_instr             ! velocity in the direction of the sun in km/s used for Doppler shift
    real(8)            :: startWavel                 ! first wavelength - nominal wavelength grid for instrument
    real(8)            :: endWavel                   ! last wavelength  - nominal wavelength grid for instrument
    real(8)            :: stepWavel                  ! wavelength step  - nominal wavelength grid for instrument
    ! wavelengths on instruments grid
    integer            :: nwavel                     ! number of wavelengths for nominal grid
    real(8), pointer   :: wavelNominal(:)  => null() ! (nwavel) nominal (not shifted) wavelengths for instrument grid
    real(8), pointer   :: wavelShift(:)    => null() ! (nwavel) wavelength shift on nominal wavelength grid
    real(8), pointer   :: wavel(:)         => null() ! (nwavel) wavelengths for instrument grid = wavelNominal + wavelShift
    ! parameters for wavelength shifts
    integer            :: nNodes                     ! number of wavelengths where shifts are specified
    integer            :: nAPShift                   ! number nodes where a priori shifts are specifies
    integer            :: nAPvarShift                ! number nodes where a priori varices for shifts are specified
    real(8), pointer   :: wavelNode(:)     => null() ! (nNodes) wavelengths where shifts are specified
    real(8), pointer   :: APshift(:)       => null() ! (nAPShift) a priori wavelength shifta at wavelNodes
    real(8), pointer   :: shift(:)         => null() ! (nAPShift) wavelength shifta at wavelNodes
    real(8), pointer   :: APvarShift(:)    => null() ! (nAPvarShift) a priori variance of the wavelength shifta at wavelNodes
    real(8), pointer   :: varShift(:)      => null() ! (nAPvarShift) variance of the wavelength shifta at wavelNodes
    ! exclude part of the fit window
    integer            :: nExclude                   ! number of excluded intervals within the band
    real(8)            :: excludeStart(nExcludeMax)  ! begin of excluded interval
    real(8)            :: excludeEnd(nExcludeMax)    ! end of excluded interval
  end type wavelInstrType

  type weakAbsType ! parameters for one column that can be divided into subcolumns
    logical           :: XsecStrongAbs              ! correct effective absorption cross section for strong absorption
    logical           :: use_dAMFdXsec              ! correct air mass factor using dM/dXsec
    logical           :: useAbsOptThcknForAMF       ! regard alt resolved AMF as function of the abs optical thickness
    logical           :: calculateAMF               ! flag for calculating AMF when the slant column is fitted using DOAS
    real(8)           :: wavelengthAMF              ! wavelength where the AMF is calculated
    logical           :: useReferenceTempRetr       ! use an isothermal atmosphere in DOAS retrieval and a temperature correction
    real(8)           :: referenceTempRetr          ! reference temperature used in DOAS
    integer           :: nwavelRTM                  ! number of wavelengths for forward calculations
    integer           :: nwavelInstr                ! number of wavelengths on the instrument wavelength grid
    integer           :: nwavelHR                   ! number of wavelengths on the high resolution wavelength grid
    integer           :: nTrace                     ! number of absorbing gases
    integer           :: degreePoly                 ! degree of polynomial for DOAS
    integer           :: RTMnlayer                  ! number of layers in RTM calculations; the air mass
                                                    ! factor is calculated at the interfaces between these layers
    real(8)           :: radianceCldFraction        ! radiance cloud fraction (fraction coming from the cloudy part of the pixel)
    real(8), pointer  :: slantColumnAP(:)        => null() ! (nTrace) a-priori column of trace gas
    real(8), pointer  :: slantColumn(:)          => null() ! (nTrace) column of trace gas
    real(8), pointer  :: covSlantColumnAP(:)     => null() ! (nTrace) a-priori covariance for the columns
    real(8), pointer  :: covSlantColumn(:)       => null() ! (nTrace) covariance for the columns
    real(8), pointer  :: columnAP(:)             => null() ! (nTrace) a-priori column of trace gas
    real(8), pointer  :: column(:)               => null() ! (nTrace) column of trace gas
    real(8), pointer  :: covColumnAP(:)          => null() ! (nTrace) a-priori covariance for the columns
    real(8), pointer  :: covColumn(:)            => null() ! (nTrace) covariance for the columns
    real(8), pointer  :: lnpolyCoefAP(:)         => null() ! (0:degreePoly) a-priori polynomial coefficients for ln(P(l))
    real(8), pointer  :: lnpolyCoef(:)           => null() ! (0:degreePoly) polynomial coefficients for ln(P(l))
    real(8), pointer  :: covlnPolyCoefAP(:)      => null() ! (0:degreePoly) a-priori covariance
    real(8), pointer  :: covlnPolyCoef(:)        => null() ! (0:degreePoly,0:degreePoly) covariance
    real(8)           :: geometricalAMF                    ! geometrical airmass factor
    real(8), pointer  :: amfWindow(:)            => null() ! (nTrace) air mass factor for the fit window
    real(8), pointer  :: amfWindowCorrT(:)       => null() ! (nTrace) air mass factor for the fit window corrected for temperature
    real(8), pointer  :: Teff_minus_T0(:)        => null() ! (nTrace) air mass factor for the fit window
    real(8), pointer  :: amfInstrGrid(:,:)       => null() ! (nwavelInstr, nTrace) monochromatic AMF on instrument grid
    real(8), pointer  :: amfHRGrid(:,:)          => null() ! (nwavelHR, nTrace) monochromatic AMF on HR grid
    real(8), pointer  :: polynomial(:)           => null() ! (nwavelInstr) polynomial on instrument grid
    real(8), pointer  :: reflRTM(:)              => null() ! (nwavelRTM) reflectance on RTM grid
    real(8), pointer  :: amfAltRTM(:,:)          => null() ! (0:RTMnlayer, nwavelRTM) altitude resolved AMF on RTM grid
    real(8), pointer  :: amfAltInstr(:,:)        => null() ! (0:RTMnlayer, nwavelInstr) alt resolved AMF on instr grid
    real(8), pointer  :: amfAltHR(:,:)           => null() ! (0:RTMnlayer, nwavelHR) alt resolved AMF on HR grid
    real(8), pointer  :: XsecAvAltHR(:,:)        => null() ! (nwavelHR,nTrace) altitude averaged Xsec on HR grid
    ! effective absorption cross sections are convoluted with slit function and weighted by solar irradiance (I0 effect)
    real(8), pointer  :: XsecEffInstr(:,:)       => null() ! (nwavelInstr,nTrace) effective Xsec instr grid
    real(8), pointer  :: XsecEffSmoothInstr(:,:) => null() ! (nwavelInstr,nTrace) smooth part of XsecEffInstr
    real(8), pointer  :: XsecEffDiffInstr(:,:)   => null() ! (nwavelInstr,nTrace) differential part of XsecEffInstr

  end type weakAbsType

  type XsecType
    ! for some gases (O3, NO2) we use temperature expansion coefficients of the absorption cross section
    !    Xsec = Xsec0 + Xsec1 * T  + Xsec2 * T * T with T in degree Celsius
    !    Xsec is in cm2 molecule-1
    ! if we need to account for pressure broadening and temperature dependence the HITRAN database is used
    !    and the gasIndex, the isotope index and the molecular weight have to be specified
    ! the cross sections are stored in the array Xsec(iwave, ialt)
    logical            :: useHITRAN              ! if true use HITRAN database else use temperature dependent Xsec
    character(LEN=250) :: XsectionFileName       ! file name containing the cross-sections
    integer            :: gasIndex               ! HITRAN index for the gas (1 = H2O, ....)
    integer            :: nISO                   ! number of ISO values read from file
    integer            :: ISO(8)                 ! isotope index (1 = most abundant, 2 = second most abundant, etc.)
    real(8)            :: factorLM               ! line mixing is multiplied with this factor only for O2 A-band
    real(8)            :: cutoff                 ! in cm-1; lines at a distance larger than cutoff are ignored
    real(8)            :: thresholdLine          ! lines with an intensity < thresholdLines * maximum intensity
                                                 ! are ignored for determining the wavelength grid
    integer            :: nwavelHR               ! number of wavelengths on high resolution grid
    integer            :: nwavel                 ! number of wavelengths on instrument resolution grid
    integer            :: nalt                   ! nalt + 1 is the number of altitudes where trace gas is specified
    real(8)            :: corrFactorAMF          ! wavelength averaged ratio of differential cross sections
                                                 ! Xsec1diff / Xsec0diff - needed for NO2
    real(8), pointer :: Xsec(:,:)    => null()     ! (nwavelHR, 0:nalt) absorption cross sections at different
                                                   ! wavelengths and at retrieval altitudes for the trace gas
    real(8), pointer :: dXsecdT(:,:) => null()     ! (nwavelHR, 0:nalt) derivative of Xsec
                                                   ! with respect to temperature
    real(8), pointer :: XsecConv(:,:) => null()    ! (nwavel, 0:nalt) absorption cross sections convoluted
                                                   ! with the slit function
    real(8), pointer :: dXsecConvdT(:,:) => null() ! (nwavel, 0:nalt) derivative of XsecConv
                                                   ! with respect to temperature
  end type XsecType

  type XsecLUTType
    ! used for the O2 A-band
    !    Xsec(lambda_i,p',T') = sum_j sum_k ( c_j_k(lambda_i)*P_j(p')*P_k(T') )
    !    where p' and T' are scaled pressure and temperature
    !    P_i(x) are Legendre polynomials
    !    c_j_k(lambda_i) are expansion coefficients
    !
    logical            :: createXsecPolyLUT      ! 1: LUT is created; 0: LUT is not created
    integer            :: nwavelHR               ! # of wavelengths for the trace gas on high resolution grid
    integer            :: nPressure_LUT          ! # of pressures where Xsec is calculated for creating LUT
                                                 ! (Gaussian div points for lnp)
    integer            :: nTemp_LUT              ! # of temperatures where Xsec is calculated for creating LUT
                                                 ! (Gaussian div points for lnT)
    integer            :: ncoeff_lnp_LUT         ! # of expansion coefficients -1 for lnp (start at 0)
    integer            :: ncoeff_lnT_LUT         ! # of expansion coefficients -1 for lnT (start at 0)
    real(8)            :: pmax_LUT               ! maximum of p used for LUT
    real(8)            :: pmin_LUT               ! minimum of p used for LUT
    real(8)            :: Tmax_LUT               ! maximum of T used for LUT
    real(8)            :: Tmin_LUT               ! minimum of T used for LUT
    real(8), pointer   :: coeff_lnTlnp_LUT(:,:,:)   => null() ! (ncoeff_lnT_LUT, ncoeff_lnp_LUT, nwavelHR)

  end type XsecLUTType


  type globalType

    type(external_data)              :: external_dataS             ! external data not used by DISAMAR
    type(wavelHRType),       pointer :: wavelHRSimS(:)             ! (globalS%numSpectrBands) high resolution wavelength grid
    type(wavelHRType),       pointer :: wavelHRRetrS(:)            ! (globalS%numSpectrBands) high resolution wavelength grid
    type(wavelHRType),       pointer :: wavelMRIrrSimS(:)          ! (globalS%numSpectrBands) medium resolution irr wavel grid
    type(wavelHRType),       pointer :: wavelMRIrrRetrS(:)         ! (globalS%numSpectrBands) medium resolution irr wavel grid
    type(wavelHRType),       pointer :: wavelMRRadSimS(:)          ! (globalS%numSpectrBands) medium resolution rad wavel grid
    type(wavelHRType),       pointer :: wavelMRRadRetrS(:)         ! (globalS%numSpectrBands) medium resolution rad wavel grid
    type(wavelInstrType),    pointer :: wavelInstrIrrSimS(:)       ! (globalS%numSpectrBands) wavel grid for irradiance sim
    type(wavelInstrType),    pointer :: wavelInstrIrrRetrS(:)      ! (globalS%numSpectrBands) wavel grid for irradiance retr
    type(wavelInstrType),    pointer :: wavelInstrRadSimS(:)       ! (globalS%numSpectrBands) wavel grid for radiance - simulation
    type(wavelInstrType),    pointer :: wavelInstrRadRetrS(:)      ! (globalS%numSpectrBands) wavel grid for radiance - retrieval
    type(absLinesType),      pointer :: absLinesSimS(:,:)          ! (globalS%numSpectrBands, globalS%nTrace) line positions
    type(absLinesType),      pointer :: absLinesRetrS(:,:)         ! (globalS%numSpectrBands, globalS%nTrace) line positions
    type(XsecType),          pointer :: XsecHRSimS(:,:)            ! (globalS%numSpectrBands, globalS%nTrace) abs cross sections
    type(XsecType),          pointer :: XsecHRRetrS(:,:)           ! (globalS%numSpectrBands, globalS%nTrace) abs cross sections
    type(XsecLUTType),       pointer :: XsecHRLUTSimS(:,:)         ! (globalS%numSpectrBands, globalS%nTrace) abs cross sec LUT
    type(XsecLUTType),       pointer :: XsecHRLUTRetrS(:,:)        ! (globalS%numSpectrBands, globalS%nTrace) abs cross sec LUT
    type(geometryType)               :: geometrySimS               ! geometrical information for simulation
    type(geometryType)               :: geometryRetrS              ! geometrical information for retrieval
    type(cloudAerosolRTMgridType)    :: cloudAerosolRTMgridSimS    ! cloud-aerosol properties (simulation)
    type(cloudAerosolRTMgridType)    :: cloudAerosolRTMgridRetrS   ! cloud-aerosol properties (retrieval)
    type(gasPTType)                  :: gasPTSimS                  ! temperature and pressure profiles for simulation
    type(gasPTType)                  :: gasPTRetrS                 ! temperature and pressure profiles for retrieval
    type(traceGasType),      pointer :: traceGasSimS(:)            ! (globalS%nTrace) trace gas properties for simulation
    type(traceGasType),      pointer :: traceGasRetrS(:)           ! (globalS%nTrace) trace gas properties for retrieval
    type(LambertianType),    pointer :: surfaceSimS(:)             ! (globalS%numSpectrBands) surface properties for simulation
    type(LambertianType),    pointer :: surfaceRetrS(:)            ! (globalS%numSpectrBands) surface properties for for retrieval
    type(LambertianType),    pointer :: LambCloudSimS(:)           ! (globalS%numSpectrBands) Lambertian cloud properties (sim)
    type(LambertianType),    pointer :: LambCloudRetrS(:)          ! (globalS%numSpectrBands) Lambertian cloud properties (retr)
    type(LambertianType),    pointer :: LambAerSimS(:)             ! (globalS%numSpectrBands) Lambertian aerosol properties (sim)
    type(LambertianType),    pointer :: LambAerRetrS(:)            ! (globalS%numSpectrBands) Lambertian aerosol properties (retr)
    type(cldAerFractionType),pointer :: cldAerFractionSimS(:)      ! (globalS%numSpectrBands) cloud fraction properties (sim)
    type(cldAerFractionType),pointer :: cldAerFractionRetrS(:)     ! (globalS%numSpectrBands) cloud fraction properties (retr)
    type(polCorrectionType), pointer :: polCorrectionRetrS(:)      ! polarization + RRS correction data  for retrieval
    type(createLUTType),     pointer :: createLUTSimS(:,:)         ! (0:globalS%maxFourierTermLUT, globalS%numSpectrBands)
    type(createLUTType),     pointer :: createLUTRetrS(:,:)        ! (0:globalS%maxFourierTermLUT, globalS%numSpectrBands)
    type(optPropRTMGridType)         :: optPropRTMGridSimS         ! optical properties on RTM grid (simulation)
    type(optPropRTMGridType)         :: optPropRTMGridRetrS        ! optical properties on RTM grid (retrieval)
    type(controlType)                :: controlSimS                ! control parameters for radiative transfer (simulation)
    type(controlType)                :: controlRetrS               ! control parameters for radiative transfer (retrieval)
    type(O3ClimType)                 :: O3ClimSimS                 ! control parameters for radiative transfer (simulation)
    type(O3ClimType)                 :: O3ClimRetrS                ! control parameters for radiative transfer (retrieval)
    type(reflDerivType),     pointer :: reflDerivHRSimS(:)         ! (globalS%numSpectrBands) refl(R) and deriv(K) on HR grid
    type(reflDerivType),     pointer :: reflDerivHRRetrS(:)        ! (globalS%numSpectrBands) refl(R) and deriv(K) on HR grid
    type(solarIrrType),      pointer :: solarIrradianceSimS(:)     ! (globalS%numSpectrBands) solar irr on HR and instr grid
    type(solarIrrType),      pointer :: solarIrradianceRetrS(:)    ! (globalS%numSpectrBands) solar irr on HR and instr grid
    type(earthRadianceType), pointer :: earthRadianceSimS(:)       ! (globalS%numSpectrBands) radiance and deriv (simulation)
    type(earthRadianceType), pointer :: earthRadianceRetrS(:)      ! (globalS%numSpectrBands) radiance and deriv (retrieval)
    type(mulOffsetType),     pointer :: earthRadMulOffsetSimS(:)   ! (globalS%numSpectrBands) straylight for simulation
    type(mulOffsetType),     pointer :: earthRadMulOffsetRetrS(:)  ! (globalS%numSpectrBands) straylight for retrieval
    type(straylightType),    pointer :: earthRadStrayLightSimS(:)  ! (globalS%numSpectrBands) straylight for simulation
    type(straylightType),    pointer :: earthRadStrayLightRetrS(:) ! (globalS%numSpectrBands) straylight for retrieval
    type(RRS_RingType),      pointer :: RRS_RingSimS(:)            ! (globalS%numSpectrBands) Ring spectrum for simulation
    type(RRS_RingType),      pointer :: RRS_RingRetrS(:)           ! (globalS%numSpectrBands) Ring spectrum for retrieval
    type(calibrReflType),    pointer :: calibErrorReflS(:)         ! (globalS%numSpectrBands) calibration error reflectance
    type(retrType)                   :: retrS                      ! parameters for iteratively solving the retrieval
    type(diagnosticType)             :: diagnosticS                ! diagnostic information
    type(columnType),        pointer :: columnSimS(:)              ! (globalS%ncolumn) results for (sub)columns for simulation
    type(columnType),        pointer :: columnRetrS(:)             ! (globalS%ncolumn) results for (sub)columns for retrieval
    type(weakAbsType),       pointer :: weakAbsSimS(:)             ! (globalS%numSpectrBands) parameters for DISMAS
    type(weakAbsType),       pointer :: weakAbsRetrS(:)            ! (globalS%numSpectrBands) parameters for DISMAS or DOAS
    type(dn_dnodeType),      pointer :: dn_dnodeSimS(:)            ! gives change at other altitudes when the amount
                                                                   ! of air/trace gas changes at one node
    type(dn_dnodeType),      pointer :: dn_dnodeRetrS(:)
    type(mieScatType)                :: mieAerSimS(maxNumMieModels)  ! aerosol advanced scattering models
    type(mieScatType)                :: mieAerRetrS(maxNumMieModels) ! aerosol advanced scattering models
    type(mieScatType)                :: mieCldSimS(maxNumMieModels)  ! cloud advanced scattering models
    type(mieScatType)                :: mieCldRetrS(maxNumMieModels) ! cloud advanced scattering models
    integer                          :: numSpectrBands, nTrace, ncolumn, maxFourierTermLUT
    integer,                 pointer :: nwavelRTM(:) => null()
    logical,                 pointer :: use_abs_opt_thickn_for_AMF(:) => null()
    integer                          :: totalNodes, totalNodesTemp
    character(LEN = 10)              :: nameTraceGas
    integer                          :: iteration
    type(errorType)                  :: errorS                     ! Error handling
    type(inputType)                  :: inputS                     ! Input from L2DP framework
    integer                          :: processingQF               ! processing quality flag, content conform pqf_module.f90
    type(file_type)                  :: configFileS                !  Configuration file

  end type globalType

!  type staticXSType                                                   ! Data to be loaded only once
!    character(LEN=10)                :: name
!    integer                          :: numwav
!    real(8), pointer                 :: wavelength(:)
!    real(8), pointer                 :: a1(:)
!    real(8), pointer                 :: a2(:)
!    real(8), pointer                 :: a3(:)
!  end type staticXSType
!
!  type staticType                                                   ! Data to be loaded only once
!    type(globalType), pointer        :: globalS
!    integer                          :: irrNum
!    real(8), pointer                 :: irrWavHR(:)
!    real(8), pointer                 :: irrPower(:)
!    real(8), pointer                 :: irrFlux(:)
!    integer                          :: xsnum
!    type(staticXSType)               :: xs(xsmax)
!  end type staticType

  type staticXSType                                                ! Data to be loaded only once
    character(LEN=10)                :: name
    integer                          :: numwav
    real(8), pointer                 :: wavelength(:)
    real(8), pointer                 :: a1(:)
    real(8), pointer                 :: a2(:)
    real(8), pointer                 :: a3(:)
  end type staticXSType

  type staticIsrfType
    integer                          :: numGroundPixel
    integer                          :: numCentralWavelength
    integer                          :: numDeltaWavelength
    integer, pointer                 :: groundPixel(:)
    integer, pointer                 :: centralWavelength(:)
    real, pointer                    :: deltaWavelength(:)
    real, pointer                    :: isrf(:,:,:)
  end type staticIsrfType

  type staticHRWavelType
    integer                          :: nwavel
    real(8), pointer                 :: wavel(:) => null()
    real(8), pointer                 :: weights(:) => null()
  end type staticHRWavelType

  type staticType                                                  ! Data to be set only once
    integer                          :: operational                ! 0 = no; 1 = yes
    integer                          :: abortFlag                  ! 0 = no; 1 = yes
    integer                          :: replaceCldAerIntervalBounds ! 0 = no; 1 = yes
    integer                          :: writeResults               ! 0 = no; 1 = yes
    character(LEN=250)               :: configFileName
    character(LEN=250)               :: dataPath
    integer                          :: irrNum
    real(8), pointer                 :: irrWavHR(:)
    real(8), pointer                 :: irrPower(:)
    real(8), pointer                 :: irrFlux(:)
    integer                          :: xsnum
    type(staticXSType)               :: xs(xsmax)
    integer                          :: climNumTim
    integer                          :: climNumLat
    integer                          :: climNumLev
    integer                          :: climNumTCO3
    real, dimension(maxTime)         :: climTime
    real, dimension(maxLat)          :: climLat
    real, dimension(maxLev)          :: climLev
    real, dimension(maxTCO3)         :: climTCO3
    real, pointer                    :: climO3(:,:,:,:)
    type(XsecLUTType), pointer       :: o2xsection => null()
    type(XsecLUTType), pointer       :: o2o2xsection => null()
    type(staticHRWavelType), pointer :: hr_wavel => null()
    type(staticIsrfType), pointer    :: isrfS(:)
    logical                          :: isrfLoaded
    logical                          :: o2XsectionLoaded
    type(polCorrectionType), pointer :: polCorrectionRetrS(:)      ! polarization + RRS correction data  for retrieval
    logical                          :: polCorrectionLoaded

    type(file_type)                        :: configFileS
    real(8), dimension(:), allocatable     :: hires_wavelength
    real(8), dimension(:), allocatable     :: hires_solar
    real(8), dimension(:), allocatable     :: isrf_wavelength
    real(8), dimension(:), allocatable     :: isrf_offset
    integer                                :: nrows
    real(8), dimension(:), allocatable     :: isrf
    real(8), dimension(:), allocatable     :: refspec_wavelength
    integer                                :: refspec_pressure
    integer                                :: refspec_temperature
    real(8)                                :: refspec_pressure_min
    real(8)                                :: refspec_pressure_max
    real(8)                                :: refspec_temperature_min
    real(8)                                :: refspec_temperature_max
    real(8), dimension(:), allocatable     :: gaussian_weights
    real(8), dimension(:), allocatable     :: refspec_o2
    real(8), dimension(:), allocatable     :: refspec_o2o2
    logical                                :: fort

  end type staticType


! END OF TYPE DECLERATIONS


  contains

  pure function isError(globalS)

    implicit none
    type(globalType), pointer :: globalS
    logical isError

    isError = (iand(globalS%processingQF, PQF_ERROR_MASK) /= 0)

  end function isError

  subroutine setError(globalS, pqfError)

    implicit none
    type(globalType), pointer :: globalS
    integer, intent(in)       :: pqfError

    globalS%processingQF = ior(globalS%processingQF, pqfError)

  end subroutine setError


  subroutine claimMemWavelInstrS(errS, wavelInstrS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(wavelInstrType), intent(inout) :: wavelInstrS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    allocate( wavelInstrS%wavelNominal(wavelInstrS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( wavelInstrS%wavelShift(wavelInstrS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( wavelInstrS%wavel(wavelInstrS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    ! the following lines are commented out because allocation takes place when
    ! reading the configuration file
    !allocate( wavelInstrS%wavelNode(wavelInstrS%nNodes), STAT = allocStatus )
    !sumAllocStatus = sumAllocStatus + allocStatus
    !
    !allocate( wavelInstrS%APshift(wavelInstrS%nNodes), STAT = allocStatus )
    !sumAllocStatus = sumAllocStatus + allocStatus
    !
    !allocate( wavelInstrS%shift(wavelInstrS%nNodes), STAT = allocStatus )
    !sumAllocStatus = sumAllocStatus + allocStatus
    !
    !allocate( wavelInstrS%APvarShift(wavelInstrS%nNodes), STAT = allocStatus )
    !sumAllocStatus = sumAllocStatus + allocStatus
    !
    !allocate( wavelInstrS%varShift(wavelInstrS%nNodes), STAT = allocStatus )
    !sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemWavelInstrS has nonzero allocStatus. alloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemWavelInstrS


  subroutine freeMemWavelInstrS(errS, wavelInstrS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(wavelInstrType), intent(inout) :: wavelInstrS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( wavelInstrS%wavelNominal) ) then
      deallocate( wavelInstrS%wavelNominal, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( wavelInstrS%wavelShift) ) then
      deallocate( wavelInstrS%wavelShift, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( wavelInstrS%wavel) ) then
      deallocate( wavelInstrS%wavel, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( wavelInstrS%wavelNode) ) then
      deallocate( wavelInstrS%wavelNode, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( wavelInstrS%APshift) ) then
      deallocate( wavelInstrS%APshift, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( wavelInstrS%shift) ) then
      deallocate( wavelInstrS%shift, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( wavelInstrS%APvarShift) ) then
      deallocate( wavelInstrS%APvarShift, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( wavelInstrS%varShift) ) then
      deallocate( wavelInstrS%varShift, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
        !call mystop(errS, 'freeMemWavelInstrS has nonzero deallocStatus. dealloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemWavelInstrS


  subroutine claimMemWavelHRS(errS, wavelHRS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(wavelHRType), intent(inout) :: wavelHRS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    allocate( wavelHRS%wavel(wavelHRS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( wavelHRS%weight(wavelHRS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
      call mystop(errS, 'claimMemWavelHRS has nonzero sumAllocStatus. Alloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine claimMemWavelHRS


  subroutine freeMemWavelHRS(errS, wavelHRS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(wavelHRType), intent(inout) :: wavelHRS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated(wavelHRS%wavel) ) then
      deallocate( wavelHRS%wavel, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(wavelHRS%weight) ) then
      deallocate( wavelHRS%weight, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemWavelHRS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemWavelHRS


  subroutine claimMemO3ClimS(errS, O3ClimS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(O3ClimType), intent(inout) :: O3ClimS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus = 0
    sumAllocStatus = 0

    allocate( O3ClimS%nodes_months(O3ClimS%dim_months_clim), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( O3ClimS%nodes_latitudes(O3ClimS%dim_lat_clim), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( O3ClimS%nodes_total_column(O3ClimS%dim_total_column_clim), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( O3ClimS%nodes_pressures(O3ClimS%dim_pressures_clim), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( O3ClimS%nodes_pressures_vmr(O3ClimS%dim_pressures_vmr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( O3ClimS%clim_temperature(O3ClimS%dim_pressures_clim, O3ClimS%dim_lat_clim,  &
                                       O3ClimS%dim_months_clim), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( O3ClimS%clim_O3(O3ClimS%dim_pressures_clim, O3ClimS%dim_total_column_clim,        &
                              O3ClimS%dim_lat_clim, O3ClimS%dim_months_clim), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemO3ClimS has nonzero sumAllocStatus. alloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemO3ClimS


  subroutine freeMemO3ClimS(errS, O3ClimS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(O3ClimType), intent(inout) :: O3ClimS

    integer :: deallocStatus
    integer :: sumdeAllocStatus

    deallocStatus = 0
    sumdeAllocStatus = 0

    if ( associated( O3ClimS%nodes_months ) ) then
      deallocate( O3ClimS%nodes_months, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated( O3ClimS%nodes_latitudes ) ) then
      deallocate( O3ClimS%nodes_latitudes, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated( O3ClimS%nodes_total_column ) ) then
      deallocate( O3ClimS%nodes_total_column, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated( O3ClimS%nodes_pressures ) ) then
      deallocate( O3ClimS%nodes_pressures, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated( O3ClimS%nodes_pressures_vmr ) ) then
      deallocate( O3ClimS%nodes_pressures_vmr, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated( O3ClimS%clim_temperature ) ) then
      deallocate( O3ClimS%clim_temperature, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated( O3ClimS%clim_O3 ) ) then
      deallocate( O3ClimS%clim_O3, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if (sumdeAllocStatus /= 0) then
        !call mystop(errS, 'freeMemO3ClimS has nonzero sumdeAllocStatus. dealloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemO3ClimS


  subroutine claimMemGeometryS (errS, geometryS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(geometryType), intent(inout) :: geometryS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus = 0
    sumAllocStatus = 0

    allocate( geometryS%ug(geometryS%nGauss), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( geometryS%wg(geometryS%nGauss), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( geometryS%u(geometryS%nmutot), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( geometryS%w(geometryS%nmutot), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( geometryS%u_sorted(geometryS%nmutot), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( geometryS%index_sort(geometryS%nmutot), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemGeometryS has nonzero sumAllocStatus. alloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemGeometryS


  subroutine freeMemGeometryS (errS, geometryS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(geometryType), intent(inout) :: geometryS

    integer :: deallocStatus
    integer :: sumdeAllocStatus

    deallocStatus = 0
    sumdeAllocStatus = 0

    deallocate( geometryS%ug, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    deallocate( geometryS%wg, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    deallocate( geometryS%u, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    deallocate( geometryS%w, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    if (sumdeAllocStatus /= 0) then
        !call mystop(errS, 'freeMemGeometryS has nonzero sumdeAllocStatus. dealloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemGeometryS


  subroutine claimMemXsecS(errS, XsecS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(XsecType), intent(inout) :: XsecS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    allocate( XsecS%Xsec(XsecS%nwavelHR,0:XsecS%nalt), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( XsecS%dXsecdT(XsecS%nwavelHR,0:XsecS%nalt), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( XsecS%XsecConv(XsecS%nwavel,0:XsecS%nalt), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( XsecS%dXsecConvdT(XsecS%nwavel,0:XsecS%nalt), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call logDebugI('nwavelHR: ', XsecS%nwavelHR)
        call logDebugI('nwavel: ', XsecS%nwavel)
        call logDebugI('nalt: ', XsecS%nalt)
        call mystop(errS, 'claimMemXsecS has nonzero sumAllocStatus. alloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemXsecS


  subroutine freeMemXsecS(errS, XsecS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(XsecType), intent(inout) :: XsecS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( XsecS%Xsec, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( XsecS%dXsecdT, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( XsecS%XsecConv, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( XsecS%dXsecConvdT, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemXsecS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemXsecS

  subroutine claimMemXsecLUTS(errS, XsecLUTS)

    implicit none

    type(errorType),   intent(inout) :: errS
    type(XsecLUTType), intent(inout) :: XsecLUTS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus = 0
    sumAllocStatus = 0

    ! array with coefficients for the expansion of the absorption cross section in Legendre functions

    allocate( XsecLUTS%coeff_lnTlnp_LUT(XsecLUTS%ncoeff_lnT_LUT, XsecLUTS%ncoeff_lnp_LUT, XsecLUTS%nwavelHR), &
              STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemXsecLUTS has nonzero sumAllocStatus. alloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemXsecLUTS


  subroutine freeMemXsecLUTS(errS, XsecLUTS)

    implicit none

    type(errorType),   intent(inout) :: errS
    type(XsecLUTType), intent(inout) :: XsecLUTS

    integer :: deallocStatus
    integer :: sumdeAllocStatus

    deallocStatus = 0
    sumdeAllocStatus = 0

    deallocate( XsecLUTS%coeff_lnTlnp_LUT, STAT = deallocStatus  )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    if (sumdeAllocStatus /= 0) then
        call mystop(errS, 'freeMemXsecLUTS has nonzero sumdeAllocStatus. dealloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemXsecLUTS


  subroutine claimMemAbsLinesS(errS, absLinesS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(absLinesType), intent(inout) :: absLinesS

    integer :: allocStatus
    integer :: sumAllocStatus


    allocStatus = 0
    sumAllocStatus = 0

    allocate( absLinesS%linePos(absLinesS%nlinePos), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( absLinesS%S(absLinesS%nlinePos), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemAbsLinesS has nonzero SumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemAbsLinesS


  subroutine freeMemAbsLinesS(errS, absLinesS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(absLinesType), intent(inout) :: absLinesS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( absLinesS%linePos, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( absLinesS%S, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemAbsLinesS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemAbsLinesS


  subroutine claimMemPolCorrectionS(errS, polCorrectionS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(polCorrectionType), intent(inout) :: polCorrectionS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus = 0
    sumAllocStatus = 0

    if ( .not. associated( polCorrectionS%wavel ) ) then
      allocate( polCorrectionS%wavel(polCorrectionS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%surfacealbedo ) ) then
      allocate( polCorrectionS%surfacealbedo(polCorrectionS%nsurfaceAlbedo), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%mu ) ) then
      allocate( polCorrectionS%mu(polCorrectionS%nmu), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%mu0 ) ) then
      allocate( polCorrectionS%mu0(polCorrectionS%nmu0), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%ozoneColumn ) ) then
      allocate( polCorrectionS%ozoneColumn(polCorrectionS%nozoneColumn), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%surfacePressure ) ) then
      allocate( polCorrectionS%surfacePressure(polCorrectionS%nsurfacePressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%latitude ) ) then
      allocate( polCorrectionS%latitude(polCorrectionS%nlatitude), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%correction ) ) then
      allocate( polCorrectionS%correction(polCorrectionS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%SDCorrection ) ) then
      allocate( polCorrectionS%SDCorrection(polCorrectionS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%correctionLUT0 ) ) then
      allocate( polCorrectionS%correctionLUT0                                                        &
     (polCorrectionS%nwavel, polCorrectionS%nsurfacealbedo, polCorrectionS%nmu, polCorrectionS%nmu0, &
      polCorrectionS%nozoneColumn, polCorrectionS%nsurfacePressure, polCorrectionS%nlatitude),       &
      STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%correctionLUT1 ) ) then
      allocate( polCorrectionS%correctionLUT1                                                        &
     (polCorrectionS%nwavel, polCorrectionS%nsurfacealbedo, polCorrectionS%nmu, polCorrectionS%nmu0, &
      polCorrectionS%nozoneColumn, polCorrectionS%nsurfacePressure, polCorrectionS%nlatitude),       &
      STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated( polCorrectionS%correctionLUT2 ) ) then
      allocate( polCorrectionS%correctionLUT2                                                        &
     (polCorrectionS%nwavel, polCorrectionS%nsurfacealbedo, polCorrectionS%nmu, polCorrectionS%nmu0, &
      polCorrectionS%nozoneColumn, polCorrectionS%nsurfacePressure, polCorrectionS%nlatitude),       &
      STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemPolCorrectionS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemPolCorrectionS


  subroutine freeMemPolCorrectionS(errS, polCorrectionS)

    implicit none

    type(errorType),         intent(inout) :: errS
    type(polCorrectionType), intent(inout) :: polCorrectionS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( polCorrectionS%wavel ) ) then
      deallocate( polCorrectionS%wavel, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%surfacealbedo ) ) then
      deallocate( polCorrectionS%surfacealbedo, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%mu ) ) then
      deallocate( polCorrectionS%mu, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%mu0 ) ) then
      deallocate( polCorrectionS%mu0, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%ozoneColumn ) ) then
      deallocate( polCorrectionS%ozoneColumn, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%surfacePressure ) ) then
      deallocate( polCorrectionS%surfacePressure, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%latitude ) ) then
      deallocate( polCorrectionS%latitude, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%correction ) ) then
      deallocate( polCorrectionS%correction, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%SDCorrection ) ) then
      deallocate( polCorrectionS%SDCorrection, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%correctionLUT0 ) ) then
      deallocate( polCorrectionS%correctionLUT0, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%correctionLUT1 ) ) then
      deallocate( polCorrectionS%correctionLUT1, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( polCorrectionS%correctionLUT2 ) ) then
      deallocate( polCorrectionS%correctionLUT2, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemPolCorrectionS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemPolCorrectionS


  subroutine claimMemCreateLUTS(errS, createLUTS)

    implicit none

    type(errorType),     intent(inout) :: errS
    type(createLUTType), intent(inout) :: createLUTS

    integer :: allocStatus
    integer :: sumAllocStatus

    createLUTS%nmup = createLUTS%nmu * createLUTS%dimSV

    allocStatus = 0
    sumAllocStatus = 0

    if ( .not. associated( createLUTS%pressure ) ) then
      allocate( createLUTS%pressure(0:createLUTS%npressure), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%surfAlb ) ) then
      allocate( createLUTS%surfAlb(createLUTS%nsurfAlb), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%wavel ) ) then
      allocate( createLUTS%wavel(createLUTS%nwavel), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%mu ) ) then
      allocate( createLUTS%mu(createLUTS%nmu), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%s_star ) ) then
      allocate( createLUTS%s_star(0:createLUTS%npressure, createLUTS%nwavel), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%td ) ) then
      allocate( createLUTS%td(createLUTS%nmu, 0:createLUTS%npressure, createLUTS%nwavel), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%expt ) ) then
      allocate( createLUTS%expt(createLUTS%nmu, 0:createLUTS%npressure, createLUTS%nwavel), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%R0 ) ) then
      allocate( createLUTS%R0(createLUTS%nmu, createLUTS%nmu, 0:createLUTS%npressure, createLUTS%nwavel), &
                            STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%R ) ) then
      allocate( createLUTS%R(createLUTS%nmu, createLUTS%nmu, createLUTS%nsurfAlb, 0:createLUTS%npressure, &
                createLUTS%nwavel), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%E ) ) then
      allocate( createLUTS%E(createLUTS%nmup, 0:createLUTS%nlevel, 0:createLUTS%npressure, createLUTS%nwavel), &
                STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%D ) ) then
      allocate( createLUTS%D(createLUTS%nmup, createLUTS%nmup, 0:createLUTS%nlevel, createLUTS%nsurfAlb, &
                0:createLUTS%npressure, createLUTS%nwavel), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if ( .not. associated( createLUTS%U ) ) then
      allocate( createLUTS%U(createLUTS%nmup, createLUTS%nmup, 0:createLUTS%nlevel, createLUTS%nsurfAlb, &
                0:createLUTS%npressure, createLUTS%nwavel), STAT = allocStatus )
    end if
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemCreateLUTS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemCreateLUTS


  subroutine freeMemCreateLUTS(errS, createLUTS)

    implicit none

    type(errorType),     intent(inout) :: errS
    type(createLUTType), intent(inout) :: createLUTS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus = 0
    sumdeallocStatus = 0

    if ( associated( createLUTS%pressure ) ) then
      deallocate( createLUTS%pressure, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%surfAlb ) ) then
      deallocate( createLUTS%surfAlb, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%wavel ) ) then
      deallocate( createLUTS%wavel, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%mu ) ) then
      deallocate( createLUTS%mu, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%s_star ) ) then
      deallocate( createLUTS%s_star, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%td ) ) then
      deallocate( createLUTS%td, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%expt ) ) then
      deallocate( createLUTS%expt, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%R0 ) ) then
      deallocate( createLUTS%R0, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%R ) ) then
      deallocate( createLUTS%R, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%D ) ) then
      deallocate( createLUTS%D, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( createLUTS%U ) ) then
      deallocate( createLUTS%U, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
        call mystop(errS, 'freeMemCreateLUTS has nonzero sumdeallocStatus. dealloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemCreateLUTS

  subroutine freeMemGasPTS(errS, gasPTS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(gasPTType), intent (inout) :: gasPTS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( gasPTS%altNodes, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%altNodesAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%alt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%altAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%pressureNodes, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%lnpressureNodes, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%pressure, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%lnpressure, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%temperatureNodesAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%temperatureNodes, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%temperatureAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%temperature, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%temperature_trueNodes, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%temperature_true, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%covTempNodesAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%covTempNodes, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%numDensAir, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%scaleHeightAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( gasPTS%scaleHeight, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemGasPTS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemGasPTS


  subroutine claimMemTraceGasS(errS, traceGasS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(traceGasType), intent (inout) :: traceGasS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus = 0
    sumAllocStatus = 0

    allocate( traceGasS%alt(0:traceGasS%nalt), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%altAP(0:traceGasS%nalt), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%pressure(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%temperature(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%temperatureAP(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%numDensAir(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%numDensAirAP(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%numDens(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%numDensAP(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%numDensTrue(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%numDensEst(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%covNumDens(0:traceGasS%nalt,0:traceGasS%nalt ), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%covNumDensAP(0:traceGasS%nalt,0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%vmr(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%vmrAP(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%covVmr(0:traceGasS%nalt,0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%covVmrAP(0:traceGasS%nalt,0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%relErrorAP(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%numDensParent(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%numDensParentAP(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%covNumDensParent(0:traceGasS%nalt,0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%covNumDensParentAP(0:traceGasS%nalt,0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%vmrParent(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%vmrParentAP(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%covVmrParent(0:traceGasS%nalt,0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%covVmrParentAP(0:traceGasS%nalt,0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( traceGasS%relErrParentAP(0:traceGasS%nalt), STAT = allocStatus  )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemTraceGasS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemTraceGasS


  subroutine freeMemTraceGasS(errS, traceGasS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(traceGasType), intent (inout) :: traceGasS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    deallocate( traceGasS%alt, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%altAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%pressure, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%temperature, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%temperatureAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%numDensAir, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%numDensAirAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%numDens, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%numDensAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%numDensTrue, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%numDensEst, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%covNumDens, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%covNumDensAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%vmr, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%vmrAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%covVmr, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%covVmrAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%relErrorAP, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%numDensParent, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%numDensParentAP, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%covNumDensParent, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%covNumDensParentAP, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%vmrParent, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%vmrParentAP, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%covVmrParent, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%covVmrParentAP, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( traceGasS%relErrParentAP, STAT = deallocStatus  )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemTraceGasS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemTraceGasS


  subroutine claimMemMieScatS(errS, mieScatS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(mieScatType), intent(inout) :: mieScatS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    allocate( mieScatS%wavel(mieScatS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( mieScatS%Cext(mieScatS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( mieScatS%a(mieScatS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( mieScatS%expCoef(6,0:mieScatS%numExpCoef,mieScatS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemMieScatS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemMieScatS


  subroutine freeMemMieScatS(errS, mieScatS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(mieScatType), intent(inout) :: mieScatS

    integer :: deallocStatus
    integer :: sumdeAllocStatus

    deallocStatus    = 0
    sumdeAllocStatus = 0

    deallocate( mieScatS%wavel, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    deallocate( mieScatS%Cext, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    deallocate( mieScatS%a, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    deallocate( mieScatS%expCoef, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    if (sumdeAllocStatus /= 0) then
        !call mystop(errS, 'freeMemMieScatS has nonzero sumdeAllocStatus. deAlloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemMieScatS


  subroutine claimMemReflDerivS(errS, reflDerivS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(reflDerivType), intent(inout) :: reflDerivS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    if ( .not. associated ( reflDerivS%bsca ) ) then
      allocate( reflDerivS%bsca(reflDerivS%nwavel), STAT = allocStatus )
      reflDerivS%bsca                = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%babs ) ) then
      allocate( reflDerivS%babs(reflDerivS%nwavel), STAT = allocStatus )
      reflDerivS%babs                = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%depolarization ) ) then
      allocate( reflDerivS%depolarization(reflDerivS%nwavel), STAT = allocStatus )
      reflDerivS%depolarization      = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl ) ) then
      allocate( reflDerivS%refl(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl                = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_clr ) ) then
      allocate( reflDerivS%refl_clr(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_clr            = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_cld ) ) then
      allocate( reflDerivS%refl_cld(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_cld            = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_ns ) ) then
      allocate( reflDerivS%refl_ns(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_ns             = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_ns_clr ) ) then
      allocate( reflDerivS%refl_ns_clr(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_ns_clr         = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_ns_cld ) ) then
      allocate( reflDerivS%refl_ns_cld(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_ns_cld         = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_elas ) ) then
      allocate( reflDerivS%refl_elas(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_elas           = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_inelas ) ) then
      allocate( reflDerivS%refl_inelas(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_inelas         = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_elas_clr ) ) then
      allocate( reflDerivS%refl_elas_clr(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_elas_clr       = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_elas_cld ) ) then
      allocate( reflDerivS%refl_elas_cld(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_elas_cld       = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_inelas_clr ) ) then
      allocate( reflDerivS%refl_inelas_clr(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_inelas_clr     = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%refl_inelas_cld ) ) then
      allocate( reflDerivS%refl_inelas_cld(reflDerivS%dimSV,reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%refl_inelas_cld     = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%polCorrection ) ) then
      allocate( reflDerivS%polCorrection(reflDerivS%nwavel), STAT = allocStatus)
      reflDerivS%polCorrection       = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%contribRefl ) ) then
      allocate( reflDerivS%contribRefl(reflDerivS%dimSV,reflDerivS%nwavel,0:reflDerivS%RTMnlayer), STAT = allocStatus )
      reflDerivS%contribRefl         = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%intFieldUp ) ) then
      allocate( reflDerivS%intFieldUp(reflDerivS%dimSV,reflDerivS%nwavel,0:reflDerivS%RTMnlayer), STAT = allocStatus )
      reflDerivS%intFieldUp          = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%intFieldDown ) ) then
      allocate( reflDerivS%intFieldDown(reflDerivS%dimSV,reflDerivS%nwavel,0:reflDerivS%RTMnlayer), STAT = allocStatus )
      reflDerivS%intFieldDown        = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%altResAMFabs_clr ) ) then
      allocate( reflDerivS%altResAMFabs_clr(reflDerivS%nwavel,0:reflDerivS%RTMnlayer), STAT = allocStatus )
      reflDerivS%altResAMFabs_clr    = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%altResAMFabs_cld ) ) then
      allocate( reflDerivS%altResAMFabs_cld(reflDerivS%nwavel,0:reflDerivS%RTMnlayer), STAT = allocStatus )
      reflDerivS%altResAMFabs_cld    = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%altResAMFscaAer_clr ) ) then
      allocate( reflDerivS%altResAMFscaAer_clr(reflDerivS%nwavel,0:reflDerivS%RTMnlayer), STAT = allocStatus )
      reflDerivS%altResAMFscaAer_clr = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%altResAMFscaAer_cld ) ) then
      allocate( reflDerivS%altResAMFscaAer_cld(reflDerivS%nwavel,0:reflDerivS%RTMnlayer), STAT = allocStatus )
      reflDerivS%altResAMFscaAer_cld = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%K_lnvmr ) ) then
      allocate( reflDerivS%K_lnvmr(reflDerivS%nwavel,reflDerivS%nstate), STAT = allocStatus )
      reflDerivS%K_lnvmr             = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%K_clr_lnvmr ) ) then
      allocate( reflDerivS%K_clr_lnvmr(reflDerivS%nwavel,reflDerivS%nstate), STAT = allocStatus )
      reflDerivS%K_clr_lnvmr         = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%K_cld_lnvmr ) ) then
      allocate( reflDerivS%K_cld_lnvmr(reflDerivS%nwavel,reflDerivS%nstate), STAT = allocStatus )
      reflDerivS%K_cld_lnvmr         = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%K_vmr ) ) then
      allocate( reflDerivS%K_vmr(reflDerivS%nwavel,reflDerivS%nstate), STAT = allocStatus )
      reflDerivS%K_vmr               = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%K_clr_vmr ) ) then
      allocate( reflDerivS%K_clr_vmr(reflDerivS%nwavel,reflDerivS%nstate), STAT = allocStatus )
      reflDerivS%K_clr_vmr           = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%K_cld_vmr ) ) then
      allocate( reflDerivS%K_cld_vmr(reflDerivS%nwavel,reflDerivS%nstate), STAT = allocStatus )
      reflDerivS%K_cld_vmr           = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%K_ndens ) ) then
      allocate( reflDerivS%K_ndens(reflDerivS%nwavel,reflDerivS%nstate), STAT = allocStatus )
      reflDerivS%K_ndens             = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%K_clr_ndens ) ) then
      allocate( reflDerivS%K_clr_ndens(reflDerivS%nwavel,reflDerivS%nstate), STAT = allocStatus )
      reflDerivS%K_clr_ndens         = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( reflDerivS%K_cld_ndens ) ) then
      allocate( reflDerivS%K_cld_ndens(reflDerivS%nwavel,reflDerivS%nstate), STAT = allocStatus )
      reflDerivS%K_cld_ndens         = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemReflDerivS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemReflDerivS


  subroutine freeMemReflDerivS(errS, reflDerivS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(reflDerivType), intent (inout) :: reflDerivS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( reflDerivS%bsca ) ) then
      deallocate( reflDerivS%bsca, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%babs ) ) then
      deallocate( reflDerivS%babs, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%depolarization ) ) then
      deallocate( reflDerivS%depolarization, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl ) ) then
      deallocate( reflDerivS%refl, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_clr ) ) then
      deallocate( reflDerivS%refl_clr, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_cld ) ) then
      deallocate( reflDerivS%refl_cld, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_ns ) ) then
      deallocate( reflDerivS%refl_ns, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_ns_clr ) ) then
      deallocate( reflDerivS%refl_ns_clr, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_ns_cld ) ) then
      deallocate( reflDerivS%refl_ns_cld, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_elas ) ) then
      deallocate( reflDerivS%refl_elas, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_inelas ) ) then
      deallocate( reflDerivS%refl_inelas, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_elas_clr ) ) then
      deallocate( reflDerivS%refl_elas_clr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_elas_cld ) ) then
      deallocate( reflDerivS%refl_elas_cld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_inelas_clr ) ) then
      deallocate( reflDerivS%refl_inelas_clr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%refl_inelas_cld ) ) then
      deallocate( reflDerivS%refl_inelas_cld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%polCorrection ) ) then
      deallocate( reflDerivS%polCorrection, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%contribRefl ) ) then
      deallocate( reflDerivS%contribRefl, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%intFieldUp ) ) then
      deallocate( reflDerivS%intFieldUp, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%intFieldDown ) ) then
      deallocate( reflDerivS%intFieldDown, STAT = deallocStatus)
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%altResAMFabs_clr ) ) then
      deallocate( reflDerivS%altResAMFabs_clr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%altResAMFabs_cld ) ) then
      deallocate( reflDerivS%altResAMFabs_cld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%altResAMFscaAer_clr ) ) then
      deallocate( reflDerivS%altResAMFscaAer_clr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%altResAMFscaAer_cld ) ) then
      deallocate( reflDerivS%altResAMFscaAer_cld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%K_lnvmr ) ) then
      deallocate( reflDerivS%K_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%K_clr_lnvmr ) ) then
      deallocate( reflDerivS%K_clr_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%K_cld_lnvmr ) ) then
      deallocate( reflDerivS%K_cld_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%K_vmr ) ) then
      deallocate( reflDerivS%K_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%K_clr_vmr ) ) then
      deallocate( reflDerivS%K_clr_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%K_cld_vmr ) ) then
      deallocate( reflDerivS%K_cld_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%K_ndens ) ) then
      deallocate( reflDerivS%K_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%K_clr_ndens ) ) then
      deallocate( reflDerivS%K_clr_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%K_cld_ndens ) ) then
      deallocate( reflDerivS%K_cld_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%KHR_ndensCol ) ) then
      deallocate( reflDerivS%KHR_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%KHR_clr_ndensCol ) ) then
      deallocate( reflDerivS%KHR_clr_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( reflDerivS%KHR_cld_ndensCol ) ) then
      deallocate( reflDerivS%KHR_cld_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemReflDerivS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemReflDerivS


  subroutine claimMemSolarIrradianceS(errS, solarIrradianceS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(SolarIrrType), intent(inout) :: solarIrradianceS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    if ( .not. associated ( solarIrradianceS%solIrrHR ) ) then
      allocate( solarIrradianceS%solIrrHR(solarIrradianceS%nwavelHR), STAT = allocStatus  )
      solarIrradianceS%solIrrHR     = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( solarIrradianceS%solIrrMR ) ) then
      allocate( solarIrradianceS%solIrrMR(solarIrradianceS%nwavelMR), STAT = allocStatus  )
      solarIrradianceS%solIrrMR     = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( solarIrradianceS%solIrr ) ) then
      allocate( solarIrradianceS%solIrr(solarIrradianceS%nwavel), STAT = allocStatus  )
      solarIrradianceS%solIrr       = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( solarIrradianceS%solIrrMeas ) ) then
      allocate( solarIrradianceS%solIrrMeas(solarIrradianceS%nwavel), STAT = allocStatus  )
      solarIrradianceS%solIrrMeas   = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( solarIrradianceS%solIrrError ) ) then
      allocate( solarIrradianceS%solIrrError(solarIrradianceS%nwavel), STAT = allocStatus  )
      solarIrradianceS%solIrrError  = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( solarIrradianceS%SN ) ) then
      allocate( solarIrradianceS%SN(solarIrradianceS%nwavel), STAT = allocStatus  )
      solarIrradianceS%SN           = 0.0d0
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if (sumAllocStatus /= 0) then
      call mystop(errS, 'claimMemSolarIrradianceS has nonzero sumAllocStatus. Alloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine claimMemSolarIrradianceS


  subroutine freeMemSolarIrradianceS(errS, solarIrradianceS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(SolarIrrType), intent (inout) :: solarIrradianceS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    !if ( associated(solarIrradianceS%slitFunctionSpecsS%bandNumber ) )  then
    !     deallocate(solarIrradianceS%slitFunctionSpecsS%bandNumber, STAT = deallocStatus )
    !     sumdeallocStatus = sumdeallocStatus + deallocStatus
    !end if
    !
    !if ( associated( solarIrradianceS%slitFunctionSpecsS%pixelNumber ) )  then
    !     deallocate( solarIrradianceS%slitFunctionSpecsS%pixelNumber, STAT = deallocStatus )
    !     sumdeallocStatus = sumdeallocStatus + deallocStatus
    !end if
    !
    !if ( associated( solarIrradianceS%slitFunctionSpecsS%wavelNominal ) )  then
    !     deallocate( solarIrradianceS%slitFunctionSpecsS%wavelNominal, STAT = deallocStatus )
    !     sumdeallocStatus = sumdeallocStatus + deallocStatus
    !end if
    !
    !if ( associated( solarIrradianceS%slitFunctionSpecsS%deltawavelListed ) )  then
    !     deallocate( solarIrradianceS%slitFunctionSpecsS%deltawavelListed, STAT = deallocStatus )
    !     sumdeallocStatus = sumdeallocStatus + deallocStatus
    !end if
    !
    !if ( associated( solarIrradianceS%slitFunctionSpecsS%SlitFunctionTable ) )  then
    !     deallocate( solarIrradianceS%slitFunctionSpecsS%SlitFunctionTable, STAT = deallocStatus )
    !     sumdeallocStatus = sumdeallocStatus + deallocStatus
    !end if

    if ( associated( solarIrradianceS%solIrrHR ) ) then
         deallocate( solarIrradianceS%solIrrHR, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( solarIrradianceS%solIrrMR ) ) then
         deallocate( solarIrradianceS%solIrrMR, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( solarIrradianceS%solIrr ) ) then
         deallocate( solarIrradianceS%solIrr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( solarIrradianceS%solIrrMeas ) ) then
         deallocate( solarIrradianceS%solIrrMeas, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( solarIrradianceS%solIrrError ) ) then
         deallocate( solarIrradianceS%solIrrError, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( solarIrradianceS%SN ) ) then
         deallocate( solarIrradianceS%SN, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemSolarIrradianceS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemSolarIrradianceS


  subroutine freeMemSolarIrradianceS_Slit(errS, solarIrradianceS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(SolarIrrType), intent (inout) :: solarIrradianceS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated(solarIrradianceS%slitFunctionSpecsS%bandNumber ) )  then
         deallocate(solarIrradianceS%slitFunctionSpecsS%bandNumber, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( solarIrradianceS%slitFunctionSpecsS%pixelNumber ) )  then
         deallocate( solarIrradianceS%slitFunctionSpecsS%pixelNumber, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( solarIrradianceS%slitFunctionSpecsS%wavelNominal ) )  then
         deallocate( solarIrradianceS%slitFunctionSpecsS%wavelNominal, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( solarIrradianceS%slitFunctionSpecsS%deltawavelListed ) )  then
         deallocate( solarIrradianceS%slitFunctionSpecsS%deltawavelListed, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( solarIrradianceS%slitFunctionSpecsS%SlitFunctionTable ) )  then
         deallocate( solarIrradianceS%slitFunctionSpecsS%SlitFunctionTable, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if


    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemSolarIrradianceS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemSolarIrradianceS_Slit


  subroutine claimMemRRS_RingS(errS, RRS_RingS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(RRS_RingType), intent(inout) :: RRS_RingS

    integer    :: allocStatus
    integer    :: sumAllocStatus

    ! initialize
    allocStatus    = 0
    sumAllocStatus = 0

    allocate( RRS_RingS%RTMradElasHR(RRS_RingS%nwavelHR), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%RTMradElas(RRS_RingS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%radRingHR(RRS_RingS%nwavelHR), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%radRing(RRS_RingS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%RingHR(RRS_RingS%nwavelHR), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%Ring(RRS_RingS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%diffRing(RRS_RingS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%RTMradRingHR(RRS_RingS%nwavelHR), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%RTMradRing(RRS_RingS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%RTMRingHR(RRS_RingS%nwavelHR), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%RTMRing(RRS_RingS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%RTMdiffRing(RRS_RingS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%RTM_FIHR(RRS_RingS%nwavelHR), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( RRS_RingS%RTM_FI(RRS_RingS%nwavel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemRRS_RingS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

    ! initialize
    RRS_RingS%RTMradElasHR  = 0.0d0
    RRS_RingS%RTMradElas    = 0.0d0
    RRS_RingS%radRingHR     = 0.0d0
    RRS_RingS%radRing       = 0.0d0
    RRS_RingS%RingHR        = 0.0d0
    RRS_RingS%Ring          = 0.0d0
    RRS_RingS%diffRing      = 0.0d0
    RRS_RingS%RTMradRingHR  = 0.0d0
    RRS_RingS%RTMradRing    = 0.0d0
    RRS_RingS%RTMRingHR     = 0.0d0
    RRS_RingS%RTMRing       = 0.0d0
    RRS_RingS%RTMdiffRing   = 0.0d0
    RRS_RingS%RTM_FIHR      = 0.0d0
    RRS_RingS%RTM_FI        = 0.0d0

  end subroutine claimMemRRS_RingS


  subroutine freeMemRRS_RingS(errS, RRS_RingS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(RRS_RingType), intent(inout) :: RRS_RingS

    integer    :: deallocStatus
    integer    :: sumdeAllocStatus

    ! initialize
    deallocStatus    = 0
    sumdeAllocStatus = 0

    if ( associated(RRS_RingS%RTMradElasHR) )  then
      deallocate( RRS_RingS%RTMradElasHR, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%RTMradElas) )  then
      deallocate( RRS_RingS%RTMradElas, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%radRingHR) )  then
      deallocate( RRS_RingS%radRingHR, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%radRing) )  then
      deallocate( RRS_RingS%radRing, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%RingHR) )  then
      deallocate( RRS_RingS%RingHR, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%Ring) )  then
      deallocate( RRS_RingS%Ring, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%diffRing) )  then
      deallocate( RRS_RingS%diffRing, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%RTMradRingHR) )  then
      deallocate( RRS_RingS%RTMradRingHR, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%RTMradRing ) )  then
      deallocate( RRS_RingS%RTMradRing, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%RTMRingHR) )  then
      deallocate( RRS_RingS%RTMRingHR, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%RTMRing ) ) then
      deallocate( RRS_RingS%RTMRing, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%RTMdiffRing ) ) then
      deallocate( RRS_RingS%RTMdiffRing, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%RTM_FIHR) )  then
      deallocate( RRS_RingS%RTM_FIHR, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if ( associated(RRS_RingS%RTM_FI )  ) then
      deallocate( RRS_RingS%RTM_FI, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
    end if

    if (sumdeAllocStatus /= 0) then
        call mystop(errS, 'claimMemRRS_RingS has nonzero sumdeAllocStatus. deallocation Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemRRS_RingS


  subroutine claimMemEarthRadianceS(errS, earthRadianceS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(EarthRadianceType), intent(inout) :: earthRadianceS

    integer    :: allocStatus
    integer    :: sumAllocStatus

    ! initialize
    allocStatus    = 0
    sumAllocStatus = 0

    if ( .not. associated ( earthRadianceS%rad_clr_HR ) ) then
      allocate( earthRadianceS%rad_clr_HR(earthRadianceS%dimSV, earthRadianceS%nwavelHR), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_cld_HR ) ) then
      allocate( earthRadianceS%rad_cld_HR(earthRadianceS%dimSV, earthRadianceS%nwavelHR), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_HR ) ) then
      allocate( earthRadianceS%rad_HR(earthRadianceS%dimSV, earthRadianceS%nwavelHR), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_ns_HR ) ) then
      allocate( earthRadianceS%rad_ns_HR(earthRadianceS%dimSV, earthRadianceS%nwavelHR), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_elas_HR ) ) then
      allocate( earthRadianceS%rad_elas_HR(earthRadianceS%dimSV, earthRadianceS%nwavelHR), STAT = allocStatus )
    end if

    if ( .not. associated ( earthRadianceS%rad_inelas_HR ) ) then
      allocate( earthRadianceS%rad_inelas_HR(earthRadianceS%dimSV, earthRadianceS%nwavelHR), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%contribRadHR ) ) then
      allocate( earthRadianceS%contribRadHR &
            (earthRadianceS%dimSV, earthRadianceS%nwavelHR, 0:earthRadianceS%RTMnlayer), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%intFieldUpHR ) ) then
      allocate( earthRadianceS%intFieldUpHR &
            (earthRadianceS%dimSV, earthRadianceS%nwavelHR, 0:earthRadianceS%RTMnlayer), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%intFieldDownHR ) ) then
      allocate( earthRadianceS%intFieldDownHR &
            (earthRadianceS%dimSV, earthRadianceS%nwavelHR, 0:earthRadianceS%RTMnlayer), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_clr ) ) then
      allocate( earthRadianceS%rad_clr(earthRadianceS%dimSV, earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_cld ) ) then
      allocate( earthRadianceS%rad_cld(earthRadianceS%dimSV, earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad ) ) then
      allocate( earthRadianceS%rad(earthRadianceS%dimSV, earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_ns ) ) then
      allocate( earthRadianceS%rad_ns(earthRadianceS%dimSV, earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_elas ) ) then
      allocate( earthRadianceS%rad_elas(earthRadianceS%dimSV, earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_inelas ) ) then
      allocate( earthRadianceS%rad_inelas(earthRadianceS%dimSV, earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%contribRad ) ) then
      allocate( earthRadianceS%contribRad(earthRadianceS%dimSV, earthRadianceS%nwavel,0:earthRadianceS%RTMnlayer), &
                STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%intFieldUp ) ) then
      allocate( earthRadianceS%intFieldUp(earthRadianceS%dimSV, earthRadianceS%nwavel,0:earthRadianceS%RTMnlayer), &
                STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%intFieldDown ) ) then
      allocate( earthRadianceS%intFieldDown(earthRadianceS%dimSV, earthRadianceS%nwavel,0:earthRadianceS%RTMnlayer), &
                STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%radianceRefHR ) ) then
      allocate( earthRadianceS%radianceRefHR(earthRadianceS%nwavelHR), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%radianceRef ) ) then
      allocate( earthRadianceS%radianceRef(earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_Meas ) ) then
      allocate( earthRadianceS%rad_Meas(earthRadianceS%dimSV, earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%rad_error ) ) then
      allocate( earthRadianceS%rad_error(earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%SNrefspec ) ) then
      allocate( earthRadianceS%SNrefspec(earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%SNrefl ) ) then
      allocate( earthRadianceS%SNrefl(earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%SN ) ) then
      allocate( earthRadianceS%SN(earthRadianceS%nwavel), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%KHR_clr_lnvmr ) ) then
      allocate( earthRadianceS%KHR_clr_lnvmr(earthRadianceS%nwavelHR, earthRadianceS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%KHR_cld_lnvmr ) ) then
    allocate( earthRadianceS%KHR_cld_lnvmr(earthRadianceS%nwavelHR, earthRadianceS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%KHR_lnvmr ) ) then
      allocate( earthRadianceS%KHR_lnvmr(earthRadianceS%nwavelHR, earthRadianceS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%KHR_vmr ) ) then
      allocate( earthRadianceS%KHR_vmr(earthRadianceS%nwavelHR, earthRadianceS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%KHR_ndens ) ) then
      allocate( earthRadianceS%KHR_ndens(earthRadianceS%nwavelHR, earthRadianceS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%K_clr_lnvmr ) ) then
      allocate( earthRadianceS%K_clr_lnvmr(earthRadianceS%nwavel, earthRadianceS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%K_cld_lnvmr ) ) then
      allocate( earthRadianceS%K_cld_lnvmr(earthRadianceS%nwavel, earthRadianceS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%K_lnvmr ) ) then
      allocate( earthRadianceS%K_lnvmr(earthRadianceS%nwavel, earthRadianceS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%K_vmr ) ) then
      allocate( earthRadianceS%K_vmr(earthRadianceS%nwavel, earthRadianceS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if ( .not. associated ( earthRadianceS%K_ndens ) ) then
      allocate( earthRadianceS%K_ndens(earthRadianceS%nwavel, earthRadianceS%nstate), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
    end if

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemEarthRadianceS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

    ! initialize
    earthRadianceS%rad_clr_HR         = 0.0d0
    earthRadianceS%rad_cld_HR         = 0.0d0
    earthRadianceS%rad_HR             = 0.0d0
    earthRadianceS%rad_elas_HR        = 0.0d0
    earthRadianceS%rad_ns_HR          = 0.0d0
    earthRadianceS%rad_inelas_HR      = 0.0d0
    earthRadianceS%contribRadHR       = 0.0d0
    earthRadianceS%intFieldUpHR       = 0.0d0
    earthRadianceS%intFieldDownHR     = 0.0d0
    earthRadianceS%rad_clr            = 0.0d0
    earthRadianceS%rad_cld            = 0.0d0
    earthRadianceS%rad                = 0.0d0
    earthRadianceS%rad_ns             = 0.0d0
    earthRadianceS%rad_elas           = 0.0d0
    earthRadianceS%rad_inelas         = 0.0d0
    earthRadianceS%contribRad         = 0.0d0
    earthRadianceS%intFieldUp         = 0.0d0
    earthRadianceS%intFieldDown       = 0.0d0
    earthRadianceS%radianceRefHR      = 0.0d0
    earthRadianceS%radianceRef        = 0.0d0
    earthRadianceS%rad_meas           = 0.0d0
    earthRadianceS%rad_error          = 0.0d0
    earthRadianceS%SNrefspec          = 0.0d0
    earthRadianceS%SNrefl             = 0.0d0
    earthRadianceS%SN                 = 0.0d0
    earthRadianceS%KHR_clr_lnvmr      = 0.0d0
    earthRadianceS%KHR_cld_lnvmr      = 0.0d0
    earthRadianceS%KHR_lnvmr          = 0.0d0
    earthRadianceS%KHR_vmr            = 0.0d0
    earthRadianceS%KHR_ndens          = 0.0d0
    earthRadianceS%K_clr_lnvmr        = 0.0d0
    earthRadianceS%K_cld_lnvmr        = 0.0d0
    earthRadianceS%K_lnvmr            = 0.0d0
    earthRadianceS%K_vmr              = 0.0d0
    earthRadianceS%K_ndens            = 0.0d0

  end subroutine claimMemEarthRadianceS


  subroutine freeMemEarthRadianceS(errS, earthRadianceS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(EarthRadianceType), intent (inout) :: earthRadianceS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( earthRadianceS%rad_clr_HR ) ) then
         deallocate( earthRadianceS%rad_clr_HR, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_cld_HR ) ) then
         deallocate( earthRadianceS%rad_cld_HR, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_HR ) ) then
         deallocate( earthRadianceS%rad_HR, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated ( earthRadianceS%rad_ns_HR ) ) then
         deallocate( earthRadianceS%rad_ns_HR, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_elas_HR ) ) then
         deallocate( earthRadianceS%rad_elas_HR, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_inelas_HR ) ) then
         deallocate( earthRadianceS%rad_inelas_HR, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%contribRadHR ) ) then
      deallocate( earthRadianceS%contribRadHR, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated ( earthRadianceS%intFieldUpHR ) ) then
      deallocate( earthRadianceS%intFieldUpHR, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated ( earthRadianceS%intFieldDownHR ) ) then
      deallocate( earthRadianceS%intFieldDownHR, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_clr ) ) then
         deallocate( earthRadianceS%rad_clr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_cld ) ) then
         deallocate( earthRadianceS%rad_cld, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad ) ) then
         deallocate( earthRadianceS%rad, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated ( earthRadianceS%rad_ns ) ) then
         deallocate( earthRadianceS%rad_ns, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_elas ) ) then
         deallocate( earthRadianceS%rad_elas, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_inelas ) ) then
         deallocate( earthRadianceS%rad_inelas, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%contribRad ) ) then
         deallocate( earthRadianceS%contribRad, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated ( earthRadianceS%intFieldUp ) ) then
      deallocate( earthRadianceS%intFieldUp, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated ( earthRadianceS%intFieldDown ) ) then
         deallocate( earthRadianceS%intFieldDown, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%radianceRefHR ) ) then
         deallocate( earthRadianceS%radianceRefHR, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%radianceRef ) ) then
         deallocate( earthRadianceS%radianceRef, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_meas ) ) then
         deallocate( earthRadianceS%rad_meas, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%rad_error ) ) then
         deallocate( earthRadianceS%rad_error, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%SNrefspec ) ) then
         deallocate( earthRadianceS%SNrefspec, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%SNrefl ) ) then
         deallocate( earthRadianceS%SNrefl, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%SN ) ) then
         deallocate( earthRadianceS%SN, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%KHR_clr_lnvmr ) ) then
         deallocate( earthRadianceS%KHR_clr_lnvmr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%KHR_cld_lnvmr ) ) then
         deallocate( earthRadianceS%KHR_cld_lnvmr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%KHR_lnvmr ) ) then
         deallocate( earthRadianceS%KHR_lnvmr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%KHR_vmr ) ) then
         deallocate( earthRadianceS%KHR_vmr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%KHR_ndens ) ) then
         deallocate( earthRadianceS%KHR_ndens, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%K_clr_lnvmr ) ) then
         deallocate( earthRadianceS%K_clr_lnvmr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%K_cld_lnvmr ) ) then
         deallocate( earthRadianceS%K_cld_lnvmr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%K_lnvmr ) ) then
         deallocate( earthRadianceS%K_lnvmr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%K_vmr ) ) then
         deallocate( earthRadianceS%K_vmr, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( earthRadianceS%K_ndens ) ) then
         deallocate( earthRadianceS%K_ndens, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemEarthRadianceS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemEarthRadianceS


  subroutine freeMemEarthRadianceS_Slit(errS, earthRadianceS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(EarthRadianceType), intent (inout) :: earthRadianceS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated(earthRadianceS%slitFunctionSpecsS%bandNumber ) )  then
         deallocate(earthRadianceS%slitFunctionSpecsS%bandNumber, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(earthRadianceS%slitFunctionSpecsS%pixelNumber ) )  then
         deallocate(earthRadianceS%slitFunctionSpecsS%pixelNumber, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(earthRadianceS%slitFunctionSpecsS%wavelNominal ) )  then
         deallocate(earthRadianceS%slitFunctionSpecsS%wavelNominal, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(earthRadianceS%slitFunctionSpecsS%deltawavelListed ) ) then
         deallocate(earthRadianceS%slitFunctionSpecsS%deltawavelListed, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(earthRadianceS%slitFunctionSpecsS%SlitFunctionTable ) )  then
         deallocate(earthRadianceS%slitFunctionSpecsS%SlitFunctionTable, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemEarthRadianceS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemEarthRadianceS_Slit


  subroutine claimMemOptPropRTMGridS(errS, optPropRTMGridS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(optPropRTMGridType), intent (inout) :: optPropRTMGridS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    allocate( optPropRTMGridS%intervalBounds(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kscaIntAboveGas(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kabsIntAboveGas(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kscaIntBelowGas(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kabsIntBelowGas(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kscaIntAboveAer(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kabsIntAboveAer(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kscaIntBelowAer(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kabsIntBelowAer(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kextKextAerdiv550(optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kscaIntAboveCld(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kabsIntAboveCld(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kscaIntBelowCld(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kabsIntBelowCld(0:optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kextKextClddiv550(optPropRTMGridS%ninterval), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%RTMaltitude(0:optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%RTMweight(0:optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%ndensGas(0:optPropRTMGridS%RTMnlayer,optPropRTMGridS%nTrace), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%RTMndensAir(0:optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%RTMtemperature(0:optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%RTMpressure(0:optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%ksca(0:optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kabs(0:optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kext(0:optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%ssaLay(optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%opticalThicknLay(optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%maxExpCoefLay(optPropRTMGridS%RTMnlayer), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%phasefCoefLay  &
       (optPropRTMGridS%dimSV,optPropRTMGridS%dimSV,0:optPropRTMGridS%maxExpCoef,optPropRTMGridS%RTMnlayer), &
       STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%phasefCoefAbove  &
       (optPropRTMGridS%dimSV,optPropRTMGridS%dimSV,0:optPropRTMGridS%maxExpCoef,0:optPropRTMGridS%RTMnlayer), &
       STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%phasefCoefGas  &
       (optPropRTMGridS%dimSV,optPropRTMGridS%dimSV,0:optPropRTMGridS%maxExpCoef,0:optPropRTMGridS%RTMnlayer), &
       STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%phasefCoefRRS (optPropRTMGridS%dimSV,optPropRTMGridS%dimSV,0:2), &
       STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%phasefCoefAerAbove  &
       (optPropRTMGridS%dimSV,optPropRTMGridS%dimSV,0:optPropRTMGridS%maxExpCoef,0:optPropRTMGridS%RTMnlayer), &
       STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%phasefCoefAerBelow  &
       (optPropRTMGridS%dimSV,optPropRTMGridS%dimSV,0:optPropRTMGridS%maxExpCoef,0:optPropRTMGridS%RTMnlayer), &
       STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%phasefCoefCldAbove  &
       (optPropRTMGridS%dimSV,optPropRTMGridS%dimSV,0:optPropRTMGridS%maxExpCoef,0:optPropRTMGridS%RTMnlayer), &
       STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%phasefCoefCldBelow  &
       (optPropRTMGridS%dimSV,optPropRTMGridS%dimSV,0:optPropRTMGridS%maxExpCoef,0:optPropRTMGridS%RTMnlayer), &
       STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%RTMaltitudeSub(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%RTMweightSub(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%tempSub(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kscaSub(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kabsSub(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kextSub(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%ndensSubGas  (0:optPropRTMGridS%RTMnlayerSub,optPropRTMGridS%nTrace), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%vmrSubGas    (0:optPropRTMGridS%RTMnlayerSub,optPropRTMGridS%nTrace), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%XsecSubGas   (0:optPropRTMGridS%RTMnlayerSub,optPropRTMGridS%nTrace), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%dXsecdTSubGas(0:optPropRTMGridS%RTMnlayerSub,optPropRTMGridS%nTrace), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kabsSubGas(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kextSubGas(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kextSubAer(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( optPropRTMGridS%kextSubCld(0:optPropRTMGridS%RTMnlayerSub), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemOptPropRTMGridS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemOptPropRTMGridS


  subroutine freeMemOptPropRTMGridS(errS, optPropRTMGridS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(optPropRTMGridType), intent (inout) :: optPropRTMGridS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated(optPropRTMGridS%intervalBounds ) ) then
      deallocate( optPropRTMGridS%intervalBounds, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kscaIntAboveGas) ) then
      deallocate( optPropRTMGridS%kscaIntAboveGas, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kabsIntAboveGas) ) then
      deallocate( optPropRTMGridS%kabsIntAboveGas, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kscaIntBelowGas) ) then
      deallocate( optPropRTMGridS%kscaIntBelowGas, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kabsIntBelowGas) ) then
      deallocate( optPropRTMGridS%kabsIntBelowGas, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kscaIntAboveAer) ) then
      deallocate( optPropRTMGridS%kscaIntAboveAer, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kabsIntAboveAer) ) then
      deallocate( optPropRTMGridS%kabsIntAboveAer, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kscaIntBelowAer) ) then
      deallocate( optPropRTMGridS%kscaIntBelowAer, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kabsIntBelowAer) ) then
      deallocate( optPropRTMGridS%kabsIntBelowAer, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kextKextAerdiv550) ) then
      deallocate( optPropRTMGridS%kextKextAerdiv550, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kscaIntAboveCld) ) then
      deallocate( optPropRTMGridS%kscaIntAboveCld, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kabsIntAboveCld) ) then
      deallocate( optPropRTMGridS%kabsIntAboveCld, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kscaIntBelowCld) ) then
      deallocate( optPropRTMGridS%kscaIntBelowCld, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kabsIntBelowCld) ) then
      deallocate( optPropRTMGridS%kabsIntBelowCld, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%kextKextClddiv550) ) then
      deallocate( optPropRTMGridS%kextKextClddiv550, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%RTMaltitude) ) then
      deallocate( optPropRTMGridS%RTMaltitude, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%RTMweight) ) then
      deallocate( optPropRTMGridS%RTMweight, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%ndensGas) ) then
      deallocate( optPropRTMGridS%ndensGas, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%RTMndensAir) ) then
      deallocate( optPropRTMGridS%RTMndensAir, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%RTMtemperature) ) then
      deallocate( optPropRTMGridS%RTMtemperature, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%RTMpressure) ) then
      deallocate( optPropRTMGridS%RTMpressure, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%Colaltitude) ) then
      deallocate( optPropRTMGridS%Colaltitude, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%Colweight) ) then
      deallocate( optPropRTMGridS%Colweight, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%ColXsec) )  then
      deallocate( optPropRTMGridS%ColXsec, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%Colndens) ) then
      deallocate( optPropRTMGridS%Colndens, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%ColndensAP) ) then
      deallocate( optPropRTMGridS%ColndensAP, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated(optPropRTMGridS%ColndensAir) ) then
      deallocate( optPropRTMGridS%ColndensAir, STAT=deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    deallocate( optPropRTMGridS%ksca, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%kabs, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%kext, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%ssaLay, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%opticalThicknLay, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%maxExpCoefLay, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%phasefCoefLay, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%phasefCoefAbove, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%phasefCoefGas, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%phasefCoefRRS, STAT = deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%phasefCoefAerAbove, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%phasefCoefAerBelow, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%phasefCoefCldAbove, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%phasefCoefCldBelow, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%RTMaltitudeSub, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%RTMweightSub, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%tempSub, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%kscaSub, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%kabsSub, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%kextSub, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%ndensSubGas, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%vmrSubGas, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%XsecSubGas, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%dXsecdTSubGas, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%kabsSubGas, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%kextSubGas, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%kextSubAer, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    deallocate( optPropRTMGridS%kextSubCld, STAT=deallocStatus )
    sumdeallocStatus = sumdeallocStatus + deallocStatus

    if (sumdeallocStatus /= 0) then
        !call mystop(errS, 'freeMemOptPropRTMGridS has nonzero sumdeallocStatus. Dealloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemOptPropRTMGridS


  subroutine freeMemCloudAerosolRTMgridS(errS, CloudAerosolRTMgridS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(cloudAerosolRTMgridType), intent(inout) :: CloudAerosolRTMgridS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( CloudAerosolRTMgridS%intervalBoundsAP ) ) then
      deallocate( CloudAerosolRTMgridS%intervalBoundsAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalBounds ) ) then
      deallocate( CloudAerosolRTMgridS%intervalBounds, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%varIntervalBoundsAP ) ) then
      deallocate( CloudAerosolRTMgridS%varIntervalBoundsAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%varIntervalBounds ) ) then
      deallocate( CloudAerosolRTMgridS%varIntervalBounds, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalBoundsAP_P ) ) then
      deallocate( CloudAerosolRTMgridS%intervalBoundsAP_P, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalBounds_P ) ) then
      deallocate( CloudAerosolRTMgridS%intervalBounds_P, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%varIntervalBoundsAP_P ) ) then
      deallocate( CloudAerosolRTMgridS%varIntervalBoundsAP_P, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%varIntervalBounds_P ) ) then
      deallocate( CloudAerosolRTMgridS%varIntervalBounds_P, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalnGauss ) ) then
      deallocate( CloudAerosolRTMgridS%intervalnGauss, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalAerTauAP ) ) then
      deallocate( CloudAerosolRTMgridS%intervalAerTauAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalAerTau ) ) then
      deallocate( CloudAerosolRTMgridS%intervalAerTau, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalAerACAP ) ) then
      deallocate( CloudAerosolRTMgridS%intervalAerACAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalAerAC ) ) then
      deallocate( CloudAerosolRTMgridS%intervalAerAC, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalAerSSAAP ) ) then
      deallocate( CloudAerosolRTMgridS%intervalAerSSAAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalAerSSA ) ) then
      deallocate( CloudAerosolRTMgridS%intervalAerSSA, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalAer_g ) ) then
      deallocate( CloudAerosolRTMgridS%intervalAer_g, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalCldTauAP ) ) then
      deallocate( CloudAerosolRTMgridS%intervalCldTauAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalCldTau ) ) then
      deallocate( CloudAerosolRTMgridS%intervalCldTau, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalCldACAP ) ) then
      deallocate( CloudAerosolRTMgridS%intervalCldACAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalCldAC ) ) then
      deallocate( CloudAerosolRTMgridS%intervalCldAC, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalCldSSA ) ) then
      deallocate( CloudAerosolRTMgridS%intervalCldSSA, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( CloudAerosolRTMgridS%intervalCld_g ) ) then
      deallocate( CloudAerosolRTMgridS%intervalCld_g, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemCloudAerosolRTMgridS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemCloudAerosolRTMgridS


  subroutine freeMemLambertianS(errS, LambertianS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(LambertianType), intent(inout) :: LambertianS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( LambertianS%wavelAlbedo ) ) then
      deallocate( LambertianS%wavelAlbedo, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%albedo ) ) then
      deallocate( LambertianS%albedo, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%varianceAlbedo ) ) then
      deallocate( LambertianS%varianceAlbedo, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%albedoAP ) ) then
      deallocate( LambertianS%albedoAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%varianceAlbedoAP ) ) then
      deallocate( LambertianS%varianceAlbedoAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%wavelEmission ) ) then
      deallocate( LambertianS%wavelEmission, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%emission ) ) then
      deallocate( LambertianS%emission, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%varianceEmission ) ) then
      deallocate( LambertianS%varianceEmission, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%emissionAP ) ) then
      deallocate( LambertianS%emissionAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%varianceEmissionAP ) ) then
      deallocate( LambertianS%varianceEmissionAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( LambertianS%correlationAP ) ) then
      deallocate( LambertianS%correlationAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemLambertianS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemLambertianS


  subroutine freeMemCldAerFractionS(errS, cldAerFractionS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(cldAerFractionType), intent(inout) :: cldAerFractionS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( cldAerFractionS%wavelCldAerFraction )  ) then
      deallocate( cldAerFractionS%wavelCldAerFraction, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( cldAerFractionS%cldAerFraction )  ) then
      deallocate( cldAerFractionS%cldAerFraction, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( cldAerFractionS%varianceCldAerFraction )  ) then
      deallocate( cldAerFractionS%varianceCldAerFraction, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( cldAerFractionS%cldAerFractionAP )  ) then
      deallocate( cldAerFractionS%cldAerFractionAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( cldAerFractionS%varianceCldAerFractionAP )  ) then
      deallocate( cldAerFractionS%varianceCldAerFractionAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemCloudFractionS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemCldAerFractionS


  subroutine claimMemRetrS_not_Se(errS, retrS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(retrType) :: retrS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    allocate( retrS%wavelRetr(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%x(retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%x_not_adj(retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%xPrev(retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%x_upperBound(retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%x_lowerBound(retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%x_stored(retrS%nstate, retrS%maxNumIterations), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%chisq_stored(retrS%maxNumIterations), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%xConv_stored(retrS%maxNumIterations), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%xa(retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%dx(retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%x_true(retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%SNrefspec(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflMeas(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflNoiseError(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflCalibErrorAdd(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflCalibErrorMul(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflPrev(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%refl_clr(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%refl_cld(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%refl(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%dR_initial(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%dR(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_clr_lnvmr(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_cld_lnvmr(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_lnvmr(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_vmr(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_ndens(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%Sa_lnvmr(retrS%nstate, retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%Sa_vmr(retrS%nstate, retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%Sa_ndens(retrS%nstate, retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%Sinv_lnvmr(retrS%nstate, retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%Sinv_vmr(retrS%nstate, retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%Sinv_ndens(retrS%nstate, retrS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemRetrS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemRetrS_not_Se


  subroutine claimMemRetrS_Se(errS, retrS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(retrType) :: retrS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    allocate( retrS%Se(retrS%nwavelRetr, retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%sqrtSe(retrS%nwavelRetr, retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%sqrtInvSe(retrS%nwavelRetr, retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%InvSe(retrS%nwavelRetr, retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemRetrS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemRetrS_Se


  subroutine freeMemRetrS_not_Se(errS, retrS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(retrType), intent(inout) :: retrS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( retrS%codeFitParameters ) ) then
      deallocate( retrS%codeFitParameters, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codeSpecBand ) ) then
      deallocate( retrS%codeSpecBand, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codeTraceGas ) ) then
      deallocate( retrS%codeTraceGas, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codeAltitude ) ) then
      deallocate( retrS%codeAltitude, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codeIndexSurfAlb ) ) then
      deallocate( retrS%codeIndexSurfAlb, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codeIndexSurfEmission ) ) then
      deallocate( retrS%codeIndexSurfEmission, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codeIndexLambCldAlb ) ) then
      deallocate( retrS%codeIndexLambCldAlb, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codeIndexCloudFraction ) ) then
      deallocate( retrS%codeIndexCloudFraction, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codeIndexMulOffset ) ) then
      deallocate( retrS%codeIndexMulOffset, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codeIndexStraylight ) ) then
      deallocate( retrS%codeIndexStraylight, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%codelnPolyCoef ) ) then
      deallocate( retrS%codelnPolyCoef, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%wavelRetr ) ) then
      deallocate( retrS%wavelRetr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%x ) ) then
      deallocate( retrS%x, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%x_not_adj ) ) then
      deallocate( retrS%x_not_adj, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%xPrev ) ) then
      deallocate( retrS%xPrev, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%x_upperBound ) ) then
      deallocate( retrS%x_upperBound, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%x_lowerBound ) ) then
      deallocate( retrS%x_lowerBound, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%x_stored ) ) then
      deallocate( retrS%x_stored, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%chisq_stored ) ) then
      deallocate( retrS%chisq_stored, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%xConv_stored ) ) then
      deallocate( retrS%chisq_stored, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%xa ) ) then
      deallocate( retrS%xa, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%dx ) ) then
      deallocate( retrS%dx, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%x_true ) ) then
      deallocate( retrS%x_true, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%SNrefspec ) ) then
      deallocate( retrS%SNrefspec, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflMeas ) ) then
      deallocate( retrS%reflMeas, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflNoiseError ) ) then
      deallocate( retrS%reflNoiseError, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflCalibErrorAdd ) ) then
      deallocate( retrS%reflCalibErrorAdd, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflCalibErrorMul ) ) then
      deallocate( retrS%reflCalibErrorMul, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflPrev ) ) then
      deallocate( retrS%reflPrev, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%refl_clr ) ) then
      deallocate( retrS%refl_clr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%refl_cld ) ) then
      deallocate( retrS%refl_cld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%refl ) ) then
      deallocate( retrS%refl, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%dR_initial ) ) then
      deallocate( retrS%dR_initial, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%dR ) ) then
      deallocate( retrS%dR, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_clr_lnvmr ) ) then
      deallocate( retrS%K_clr_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_cld_lnvmr ) ) then
      deallocate( retrS%K_cld_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_lnvmr ) ) then
      deallocate( retrS%K_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_vmr ) ) then
      deallocate( retrS%K_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_ndens ) ) then
      deallocate( retrS%K_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%Sa_lnvmr ) ) then
      deallocate( retrS%Sa_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%Sa_vmr ) ) then
      deallocate( retrS%Sa_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%Sa_ndens ) ) then
      deallocate( retrS%Sa_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%Sinv_lnvmr ) ) then
      deallocate( retrS%Sinv_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%Sinv_vmr ) ) then
      deallocate( retrS%Sinv_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%Sinv_ndens ) ) then
      deallocate( retrS%Sinv_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_ndensCol ) ) then
      deallocate( retrS%K_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%Sa_ndensCol ) ) then
      deallocate( retrS%Sa_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%S_ndensCol ) ) then
      deallocate( retrS%S_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%Snoise_ndensCol ) ) then
      deallocate( retrS%Snoise_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%A_ndensCol ) ) then
      deallocate( retrS%A_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%G_ndensCol ) ) then
      deallocate( retrS%G_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemRetrS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemRetrS_not_Se


  subroutine freeMemRetrS_Se(errS, retrS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(retrType), intent(inout) :: retrS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( retrS%Se ) ) then
      deallocate( retrS%Se, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%sqrtSe ) ) then
      deallocate( retrS%sqrtSe, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%sqrtInvSe ) ) then
      deallocate( retrS%sqrtInvSe, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%InvSe ) ) then
      deallocate( retrS%InvSe, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemRetrS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemRetrS_Se


  subroutine claimMemRetrS_wavel_only_not_Se(errS, retrS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(retrType) :: retrS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus    = 0
    sumAllocStatus = 0

    allocate( retrS%wavelRetr(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%SNrefspec(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflMeas(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflNoiseError(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflCalibErrorAdd(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflCalibErrorMul(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%reflPrev(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%refl_clr(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%refl_cld(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%refl(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%dR_initial(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%dR(retrS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_clr_lnvmr(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_cld_lnvmr(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_lnvmr(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_vmr(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( retrS%K_ndens(retrS%nwavelRetr, retrS%nstate) )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemRetrS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemRetrS_wavel_only_not_Se


  subroutine freeMemRetrS_wavel_only_not_Se(errS, retrS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(retrType), intent(inout) :: retrS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( retrS%wavelRetr ) ) then
      deallocate( retrS%wavelRetr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%SNrefspec ) ) then
      deallocate( retrS%SNrefspec, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflMeas ) ) then
      deallocate( retrS%reflMeas, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflNoiseError ) ) then
      deallocate( retrS%reflNoiseError, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflCalibErrorAdd ) ) then
      deallocate( retrS%reflCalibErrorAdd, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflCalibErrorMul ) ) then
      deallocate( retrS%reflCalibErrorMul, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%reflPrev ) ) then
      deallocate( retrS%reflPrev, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%refl_clr ) ) then
      deallocate( retrS%refl_clr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%refl_cld ) ) then
      deallocate( retrS%refl_cld, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%refl ) ) then
      deallocate( retrS%refl, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%dR_initial ) ) then
      deallocate( retrS%dR_initial, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%dR ) ) then
      deallocate( retrS%dR, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_clr_lnvmr ) ) then
      deallocate( retrS%K_clr_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_cld_lnvmr ) ) then
      deallocate( retrS%K_cld_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_lnvmr ) ) then
      deallocate( retrS%K_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_vmr ) ) then
      deallocate( retrS%K_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_ndens ) ) then
      deallocate( retrS%K_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%K_ndensCol ) ) then
      deallocate( retrS%K_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( retrS%G_ndensCol ) ) then
      deallocate( retrS%G_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemRetrS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemRetrS_wavel_only_not_Se


  subroutine claimMemDiagnosticS(errS, diagnosticS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(diagnosticType), intent(inout) :: diagnosticS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus = 0
    sumAllocStatus = 0

    allocate( diagnosticS%x_smoothed   (diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%S_lnvmr      (diagnosticS%nstate, diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%A_lnvmr      (diagnosticS%nstate, diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%G_lnvmr      (diagnosticS%nstate, diagnosticS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%Snoise_lnvmr (diagnosticS%nstate, diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%S_vmr        (diagnosticS%nstate, diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%S_vmr_prof   (diagnosticS%nlevel, diagnosticS%nlevel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%error_correlation_other(diagnosticS%nother, diagnosticS%nother), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%A_vmr        (diagnosticS%nstate, diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%A_vmr_prof   (diagnosticS%nlevel, diagnosticS%nlevel), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%G_vmr        (diagnosticS%nstate, diagnosticS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%Snoise_vmr   (diagnosticS%nstate, diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%S_ndens      (diagnosticS%nstate, diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%A_ndens      (diagnosticS%nstate, diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%G_ndens      (diagnosticS%nstate, diagnosticS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%Snoise_ndens (diagnosticS%nstate, diagnosticS%nstate), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%DFStrace     (diagnosticS%nTrace) )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemDiagnosticS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemDiagnosticS


  subroutine freeMemDiagnosticS(errS, diagnosticS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(diagnosticType), intent(inout) :: diagnosticS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( diagnosticS%x_smoothed ) ) then
      deallocate( diagnosticS%x_smoothed, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%S_lnvmr ) ) then
      deallocate( diagnosticS%S_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%A_lnvmr ) ) then
      deallocate( diagnosticS%A_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%G_lnvmr ) ) then
      deallocate( diagnosticS%G_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%Snoise_lnvmr ) ) then
      deallocate( diagnosticS%Snoise_lnvmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%S_vmr ) ) then
      deallocate( diagnosticS%S_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%error_correlation_other ) ) then
      deallocate( diagnosticS%error_correlation_other, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%A_vmr ) ) then
      deallocate( diagnosticS%A_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%A_vmr_prof ) ) then
      deallocate( diagnosticS%A_vmr_prof, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%G_vmr ) ) then
      deallocate( diagnosticS%G_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%Snoise_vmr ) ) then
      deallocate( diagnosticS%Snoise_vmr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%S_ndens ) ) then
      deallocate( diagnosticS%S_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%A_ndens ) ) then
      deallocate( diagnosticS%A_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%G_ndens ) ) then
      deallocate( diagnosticS%G_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%Snoise_ndens ) ) then
      deallocate( diagnosticS%Snoise_ndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( diagnosticS%DFStrace ) ) then
      deallocate( diagnosticS%DFStrace, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemDiagnosticS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemDiagnosticS


  subroutine claimMemDiagnosticS_wavel_only(errS, diagnosticS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(diagnosticType), intent(inout) :: diagnosticS

    integer :: allocStatus
    integer :: sumAllocStatus

    allocStatus = 0
    sumAllocStatus = 0

    allocate( diagnosticS%G_lnvmr      (diagnosticS%nstate, diagnosticS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%G_vmr        (diagnosticS%nstate, diagnosticS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    allocate( diagnosticS%G_ndens      (diagnosticS%nstate, diagnosticS%nwavelRetr), STAT = allocStatus )
    sumAllocStatus = sumAllocStatus + allocStatus

    if (sumAllocStatus /= 0) then
        call mystop(errS, 'claimMemDiagnosticS has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemDiagnosticS_wavel_only


  subroutine freeMemDiagnosticS_wavel_only(errS, diagnosticS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(diagnosticType), intent(inout) :: diagnosticS

    integer :: deallocStatus
    integer :: sumdeAllocStatus

    deallocStatus = 0
    sumdeAllocStatus = 0

    deallocate( diagnosticS%G_lnvmr, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    deallocate( diagnosticS%G_vmr, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    deallocate( diagnosticS%G_ndens, STAT = deallocStatus )
    sumdeAllocStatus = sumdeAllocStatus + deallocStatus

    if (sumdeAllocStatus /= 0) then
        !call mystop(errS, 'freeMemDiagnosticS_wavel_only has nonzero sumAllocStatus. Alloc Failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemDiagnosticS_wavel_only


  subroutine claimMemColumnS(errS, columnS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(columnType), intent(inout) :: columnS

    ! local
    integer    :: isubCol

    integer :: allocStatus
    integer :: sumallocStatus

    allocStatus    = 0
    sumallocStatus = 0

    columnS%nwavel = 2000

    allocate( columnS%boundaries(columnS%nsubColumn_boundaries), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( columnS%altBound(0:columnS%nsubColumn), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( columnS%Sa(columnS%nsubColumn, columnS%nsubColumn, columnS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( columnS%S(columnS%nsubColumn, columnS%nsubColumn, columnS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( columnS%Snoise(columnS%nsubColumn, columnS%nsubColumn, columnS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    !allocate( columnS%Gain(columnS%nsubColumn, columnS%nwavel, columnS%nTrace), STAT = allocStatus )
    !sumallocStatus = sumallocStatus + allocStatus

    allocate( columnS%subColumn(columnS%nsubColumn), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    do isubCol = 1, columnS%nsubColumn
      allocate( columnS%subColumn(isubCol)%column(columnS%nTrace), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( columnS%subColumn(isubCol)%columnAP(columnS%nTrace), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( columnS%subColumn(isubCol)%columnAir(columnS%nTrace), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( columnS%subColumn(isubCol)%meanVmr(columnS%nTrace), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( columnS%subColumn(isubCol)%meanVmrAP(columnS%nTrace), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( columnS%subColumn(isubCol)%errorColAP(columnS%nTrace), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( columnS%subColumn(isubCol)%errorCol(columnS%nTrace), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( columnS%subColumn(isubCol)%noiseErrorCol(columnS%nTrace), STAT = allocStatus)
      sumallocStatus = sumallocStatus + allocStatus

      ! AK and AKx can only be allocated when columnS%nGaussCol is known
      ! columnS%nGaussCol is calculated in module subcolumnModule: subroutine fillAltitudeGridCol
      ! which is called much later. Therefore AK is allocated in fillAltitudeGridCol
      ! allocate( columnS%subColumn(isubCol)%AK (columnS%nGaussCol, columnS%nTrace) )
      ! allocate( columnS%subColumn(isubCol)%AKx(columnS%nGaussCol, columnS%nTrace) )

    end do

    if (sumallocStatus /= 0) then
      call mystop(errS, 'claimMemColumnS has nonzero sumallocStatus. alloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine claimMemColumnS


  subroutine freeMemColumnS(errS, columnS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(columnType), intent(inout) :: columnS

    ! local
    integer :: isubCol

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    do isubCol = 1, columnS%nsubColumn

      if ( associated( columnS%subColumn(isubCol)%column ) ) then
           deallocate( columnS%subColumn(isubCol)%column, STAT = deallocStatus )
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated( columnS%subColumn(isubCol)%columnAP ) ) then
           deallocate( columnS%subColumn(isubCol)%columnAP, STAT = deallocStatus )
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated( columnS%subColumn(isubCol)%columnAir ) ) then
           deallocate( columnS%subColumn(isubCol)%columnAir, STAT = deallocStatus )
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated( columnS%subColumn(isubCol)%meanVmr ) ) then
           deallocate( columnS%subColumn(isubCol)%meanVmr, STAT = deallocStatus )
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated( columnS%subColumn(isubCol)%meanVmrAP ) ) then
           deallocate( columnS%subColumn(isubCol)%meanVmrAP, STAT = deallocStatus )
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated( columnS%subColumn(isubCol)%errorColAP ) ) then
           deallocate( columnS%subColumn(isubCol)%errorColAP, STAT = deallocStatus )
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated( columnS%subColumn(isubCol)%errorCol ) ) then
           deallocate( columnS%subColumn(isubCol)%errorCol, STAT = deallocStatus )
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated( columnS%subColumn(isubCol)%noiseErrorCol ) ) then
           deallocate( columnS%subColumn(isubCol)%noiseErrorCol, STAT = deallocStatus)
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated( columnS%subColumn(isubCol)%AK ) ) then
           deallocate( columnS%subColumn(isubCol)%AK, STAT = deallocStatus)
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

      if ( associated( columnS%subColumn(isubCol)%AKx ) ) then
           deallocate( columnS%subColumn(isubCol)%AKx, STAT = deallocStatus)
           sumdeallocStatus = sumdeallocStatus + deallocStatus
      end if

    end do

    if ( associated( columnS%subColumn ) ) then
         deallocate( columnS%subColumn, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( columnS%boundaries ) ) then
         deallocate( columnS%boundaries, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( columnS%altBound ) ) then
         deallocate( columnS%altBound, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( columnS%Sa ) ) then
         deallocate( columnS%Sa, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( columnS%S ) ) then
         deallocate( columnS%S, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( columnS%Snoise ) ) then
         deallocate( columnS%Snoise, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( columnS%Gain ) ) then
         deallocate( columnS%Gain, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemColumnS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemColumnS


  subroutine claimMemColumnSGainOnly(errS, columnS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(columnType), intent(inout) :: columnS

    ! local
    integer :: allocStatus
    integer :: sumallocStatus

    allocStatus    = 0
    sumallocStatus = 0

    allocate( columnS%Gain(columnS%nsubColumn, columnS%nwavel, columnS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    if (sumallocStatus /= 0) then
      call mystop(errS, 'claimMemColumnSGainOnly has nonzero sumallocStatus. alloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine claimMemColumnSGainOnly


  subroutine freeMemColumnSGainOnly(errS, columnS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(columnType), intent(inout) :: columnS

    ! local
    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( columnS%Gain ) ) then
         deallocate( columnS%Gain, STAT = deallocStatus )
         sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemColumnSGainOnly has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemColumnSGainOnly


  subroutine claimMemWeakAbsS(errS, weakAbsS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(weakAbsType), intent(inout) :: weakAbsS

    integer :: allocStatus
    integer :: sumallocStatus

    allocStatus    = 0
    sumallocStatus = 0

    allocate( weakAbsS%slantColumnAP(weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%slantColumn  (weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%covSlantColumnAP(weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%covSlantColumn  (weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%columnAP(weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%column  (weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%covColumnAP(weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%covColumn  (weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%lnpolyCoefAP(0:weakAbsS%degreePoly), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%lnpolyCoef  (0:weakAbsS%degreePoly), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%covlnPolyCoefAP(0:weakAbsS%degreePoly), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%covlnPolyCoef  (0:weakAbsS%degreePoly), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%amfWindowCorrT (weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%amfWindow (weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%Teff_minus_T0 (weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%amfInstrGrid (weakAbsS%nwavelInstr, weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%amfHRGrid (weakAbsS%nwavelHR, weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%polynomial (weakAbsS%nwavelInstr), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%reflRTM     (weakAbsS%nwavelRTM), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%amfAltRTM   (0:weakAbsS%RTMnlayer, weakAbsS%nwavelRTM), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%amfAltInstr (0:weakAbsS%RTMnlayer, weakAbsS%nwavelInstr), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%amfAltHR    (0:weakAbsS%RTMnlayer, weakAbsS%nwavelHR), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%XsecAvAltHR (weakAbsS%nwavelHR, weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%XsecEffInstr (weakAbsS%nwavelInstr, weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%XsecEffSmoothInstr (weakAbsS%nwavelInstr, weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    allocate( weakAbsS%XsecEffDiffInstr   (weakAbsS%nwavelInstr, weakAbsS%nTrace), STAT = allocStatus )
    sumallocStatus = sumallocStatus + allocStatus

    if (sumallocStatus /= 0) then
      call mystop(errS, 'claimMemWeakAbsS has nonzero sumallocStatus. alloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine claimMemWeakAbsS


  subroutine freeMemWeakAbsS(errS, weakAbsS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(weakAbsType), intent(inout) :: weakAbsS

    integer :: deallocStatus
    integer :: sumdeallocStatus

    deallocStatus    = 0
    sumdeallocStatus = 0

    if ( associated( weakAbsS%slantcolumnAP ) ) then
      deallocate( weakAbsS%slantcolumnAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%slantcolumn ) ) then
      deallocate( weakAbsS%slantcolumn, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%covSlantColumnAP ) ) then
      deallocate( weakAbsS%covSlantColumnAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%covSlantColumn ) ) then
      deallocate( weakAbsS%covSlantColumn, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%columnAP ) ) then
      deallocate( weakAbsS%columnAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%column ) ) then
      deallocate( weakAbsS%column, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%covColumnAP ) ) then
      deallocate( weakAbsS%covColumnAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%covColumn ) ) then
      deallocate( weakAbsS%covColumn, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%lnpolyCoefAP ) ) then
      deallocate( weakAbsS%lnpolyCoefAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%lnpolyCoef ) ) then
      deallocate( weakAbsS%lnpolyCoef, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%covlnPolyCoefAP ) ) then
      deallocate( weakAbsS%covlnPolyCoefAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%covlnPolyCoef ) ) then
      deallocate( weakAbsS%covlnPolyCoef, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%amfWindowCorrT ) ) then
      deallocate( weakAbsS%amfWindowCorrT, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%amfWindow ) ) then
      deallocate( weakAbsS%amfWindow, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%Teff_minus_T0 ) ) then
      deallocate( weakAbsS%Teff_minus_T0 , STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%amfInstrGrid ) ) then
      deallocate( weakAbsS%amfInstrGrid, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%amfHRGrid ) ) then
      deallocate( weakAbsS%amfHRGrid, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%polynomial ) ) then
      deallocate( weakAbsS%polynomial, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%reflRTM ) ) then
      deallocate( weakAbsS%reflRTM, STAT = deallocStatus  )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%amfAltRTM ) ) then
      deallocate( weakAbsS%amfAltRTM, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%amfAltInstr ) ) then
      deallocate( weakAbsS%amfAltInstr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%amfAltHR ) ) then
      deallocate( weakAbsS%amfAltHR, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%XsecAvAltHR ) ) then
      deallocate( weakAbsS%XsecAvAltHR, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%XsecEffInstr ) ) then
      deallocate( weakAbsS%XsecEffInstr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%XsecEffSmoothInstr ) ) then
      deallocate( weakAbsS%XsecEffSmoothInstr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if ( associated( weakAbsS%XsecEffDiffInstr ) ) then
      deallocate( weakAbsS%XsecEffDiffInstr, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
    end if

    if (sumdeallocStatus /= 0) then
      !call mystop(errS, 'freeMemWeakAbsS has nonzero sumdeallocStatus. dealloc Failed')
      if (errorCheck(errS)) return
    end if

  end subroutine freeMemWeakAbsS


  subroutine claimMemdn_dnodeS(errS, dn_dnodeS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(dn_dnodeType), intent(inout) :: dn_dnodeS

    integer :: allocStatus

    allocStatus = 0

    allocate( dn_dnodeS%dn_dnode(0:dn_dnodeS%RTMnlayerSub,0:dn_dnodeS%naltNode), STAT = allocStatus )

    if (allocStatus /= 0) then
        call mystop(errS, 'claimMemdn_dnodeS has nonzero allocStatus. alloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimMemdn_dnodeS


  subroutine freeMemdn_dnodeS(errS, dn_dnodeS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(dn_dnodeType), intent(inout) :: dn_dnodeS

    integer :: deallocStatus

    deallocate( dn_dnodeS%dn_dnode, STAT = deallocStatus )

    if (deallocStatus /= 0) then
        !call mystop(errS, 'freeMemdn_dnodeS has nonzero deallocStatus. deallocate failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeMemdn_dnodeS


  subroutine claimGlobalS(errS, globalS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(globalType), pointer :: globalS

    integer :: allocStatus

    allocStatus = 0

    allocate(globalS, STAT = allocStatus )

    if (allocStatus /= 0) then
        call mystop(errS, 'claimGlobalS has nonzero allocStatus. alloc failed')
        if (errorCheck(errS)) return
    end if

  end subroutine claimGlobalS


  subroutine freeGlobalS(errS, globalS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(globalType), pointer :: globalS

    integer :: deallocStatus

    deallocate(globalS, STAT = deallocStatus)

    if (deallocStatus /= 0) then
        !call mystop(errS, 'freeglobalS has nonzero deallocStatus. deallocate failed')
        if (errorCheck(errS)) return
    end if

  end subroutine freeGlobalS

  subroutine claimStaticS(errS, staticS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(staticType), pointer :: staticS

    integer :: allocStatus

    allocStatus = 0

    allocate(staticS, STAT = allocStatus )

    if (allocStatus /= 0) then
        call mystop(errS, 'Failed to allocate staticS')
        if (errorCheck(errS)) return
    end if

  end subroutine claimStaticS


  subroutine freeStaticS(errS, staticS)

    implicit none

    type(errorType), intent(inout) :: errS
    type(staticType), pointer :: staticS

    integer :: deallocStatus

    deallocate(staticS, STAT = deallocStatus)

    if (deallocStatus /= 0) then
        call mystop(errS, 'Failed to deallocate staticS')
        if (errorCheck(errS)) return
    end if

  end subroutine freeStaticS

end module dataStructures
