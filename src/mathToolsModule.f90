!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!
! Routines in this module
! gaussian division points:                             GaussDivPoints  #
! linear interpolation:                                 splintLin
! polynomial interpolation:                             polyInt
! derivatives for polynomial interpolation:             polyDeriv
! spline interpolation:                                 spline, splint  #
! derivatives for spline interpolation:                 splintDeriv     #
! derivatives for linear interpolation:                 splintLinDeriv
! singular value decomposition (svd):                   svdcmp, svbksb  #
! sorting array:                                        indexx          #
! LU decomposistion:                                    LU_decomposition, solve_lin_system_LU_based  #
! verify that elements differ:                           verfy_elem_differ
! svd fitting:                                           svdfit   #
! smooth and differential Xsections:                     getSmoothAndDiffXsec
! fit and evaluate Legendre polynomials:                 fitAndEvaluateLegendrePolynomial
! log version of fit and evaluate Legendre polynomials:  lnfitAndEvaluateLegendrePolynomial
!
! # These subroutines may have the same names as subroutines in Numerical Recipe but they are different subroutines.
! 

module mathTools

  use dataStructures

  ! default is private
  private 
  public  :: splintLin, polyInt, polyDeriv, spline, splint, splintDeriv, splintLinDeriv, locate
  public  :: svdcmp, svbksb, LU_decomposition, solve_lin_system_LU_based
  public  :: gaussDivPoints, indexx, verfy_elem_differ, svdfit, fpoly, fleg, getSmoothAndDiffXsec
  public  :: fitAndEvalLegPolynomial, lnfitAndEvalLegPolynomial, mgeo, slitfunction
  public  :: interpol6D

  contains

  function mgeo(theta, z)

  ! Function mgeo returns the path lenth through a layer with unit
  ! (vertical) thickness located at a (vertical) altitude z if the
  ! light arrives at z under an angle thata with the normal.
  ! Typically, z is the altitude of the absorbing gas in the
  ! stratosphere. For absorbing gas located in the boundary layer,
  ! one should take z = 0, and mgeo becomes 1/cos (theta), the path
  ! for a plane-parallel layer.

  ! theta in degree
  ! z in km

    real(8), intent(in)     :: theta, z
    real(8)                 :: mgeo
    real(8), parameter      :: R0 = 6371.000d0     ! mean radius of the Earth in km

    real(8)                 :: sth, R0pz

    sth  = sin(Theta *acos(0d0)/90d0)
    R0pz = R0 + z

    if ( z < 0.0d0) then
       mgeo = 1.0d0/ cos(Theta *acos(0d0)/90d0)
    else
       mgeo = R0pz / SQRT(R0pz**2 - (R0*sth)**2)
    end if

  end function mgeo


  function splintLin(errS, xa, ya, x, statusSplint)

    ! splintLin returns the linear interpolated value, i.e. y(x)
    ! when y(x) is specified by x values in xa and y values in ya.
    ! It is assumed that the values in xa are increasing, and bisection
    ! is used to locate the nodes close to x.

    implicit none

    type(errorType), intent(inout) :: errS
    REAL(8), DIMENSION(:), INTENT(IN) :: xa,ya
    REAL(8), INTENT(IN) :: x
    integer, INTENT(OUT) :: statusSplint
    REAL(8) :: splintLin
    integer :: khi,klo,n
    REAL(8) :: a,b,h

    ! assume correct processing
    statusSplint = 0

    ! if status = -1 the input is incorrect

    n = size(xa)
    
    if (n < 1) then
      call mystop(errS, 'index n less than 1 in splintLin()')
      return
    end if

    klo=max(min(locate(xa,x,n),n-1),1)
    khi=klo+1
    h=xa(khi)-xa(klo)
    if (h == 0.0d0) then
      statusSplint = -1
    end if
    a=(xa(khi)-x)/h
    b=(x-xa(klo))/h
    splintLin=a*ya(klo)+b*ya(khi)
  end function splintLin

  
  function polyInt(errS, xa, ya, x)

   ! function polyInt returns the Lagrange interpolated function y(x)
   ! when the value (x1, x2, ..., xn) are given in the array xa
   ! and the corresponding function values (y1, y2, ..., yn)
   ! are given in the array ya.
   ! The values (x1, x2, ..., xn) should be all different

    implicit none
    type(errorType), intent(inout) :: errS
    real(8)             :: polyInt
    real(8), intent(in) :: xa(:), ya(:), x

    ! local
    integer     :: i, j, n

    ! product = (x - x1) (x-x2) ... (x - xn)
    real(8)     :: productDivxmxi  ! productDivxmxi = (x - x1) (x-x2) .. (x-xi-1) (x-xi+1).. (x - xn)
                                   !                = product / (x - xi)
    real(8)     :: derivProd       ! derivProd = derivetive of the product dProduct/dx for x = xi
                                   ! derivProd = (xi - x1) (xi-x2) .. (xi-xi-1) (xi-xi+1)... (xi - xn)
    real(8)     :: partialSum

    ! polyInt = y(x) is given by sum_i[ Li * yi ]

    ! check size of arrays
    n = size(xa)
    if ( n /= size(ya) ) then
      call logDebug('ERROR: in mathTools: polyInt the x and y arrays have different lengths)')
      call mystop(errS, 'stopped in mathToolsModule: polyInt : different sizes for arrays')
      if (errorCheck(errS)) return
    end if

    if ( n == 1 ) then
      polyInt = ya(1)
      return
    end if

    ! check if x equals one of the points in xa
    do i = 1, n
      if ( abs( (x - xa(i)) / (xa(n) - xa(1)) ) < 1.0d-8 ) then
        polyInt = ya(i)
        return
      end if
    end do

    ! calculate polyInt
    partialSum = 0.0d0
    do i = 1, n
      ! calculate product / (x - xi)
      productDivxmxi = 1.0d0
      derivProd      = 1.0d0
      do j = 1, n
        if ( i /= j) then
          productDivxmxi = productDivxmxi * (x - xa(j))
          derivProd      = derivProd      * (xa(i) - xa(j))
        end if
      end do ! j
      partialSum = partialSum + productDivxmxi * ya(i) / derivProd
    end do
    polyInt = partialSum

  end function polyInt


  function polyDeriv(errS, i, xa, ya, x)

   ! function polyDeriv returns the derivative of a Lagrange interpolated function y(x)
   ! with respect to the tabulated function value ya(i) that is used in the interpolation.
   ! Here the value (x1, x2, ..., xn) are given in the array xa
   ! and the corresponding function values (y1, y2, ..., yn)
   ! are given in the array ya. PolyDeriv = dy(x)/dya(i)
   ! The values (x1, x2, ..., xn) should be all different

   ! Works also for size(xa) = 1, and i = 1, i.e. a constant value,
   ! in which case polyDeriv = 1.0d0

    implicit none

    type(errorType), intent(inout) :: errS
    integer, intent(in) :: i
    real(8), intent(in) :: xa(:), ya(:), x
    real(8)             :: polyDeriv

    ! local
    integer :: j, n

    ! product = (x - x1) (x-x2) ... (x - xn)
    real(8) :: productDivxmxi  ! productDivxmxi = (x - x1) (x-x2) .. (x-xi-1) (x-xi+1).. (x - xn)
                               !                = product / (x - xi)
    real(8) :: derivProd       ! derivProd = derivetive of the product dProduct/dx for x = xi
                               ! derivProd = (xi - x1) (xi-x2) .. (xi-xi-1) (xi-xi+1)... (xi - xn)

    ! y(x) is given by sum_i[ Li * yi ]
    ! therefore polyDeriv =  dy(x)/dyi = Li

    ! check size of arrays
    n = size(xa)
    if ( n /= size(ya) ) then
      call logDebug('ERROR: in mathTools: polyInt the x and y arrays have different lengths)')
      call mystop(errS, 'stopped in mathToolsModule: polyDeriv : different sizes for arrays')
      if (errorCheck(errS)) return
    end if 

    ! check if x equals xa(i)
      if ( abs( (x - xa(i)) / (xa(n) - xa(1)) ) < 1.0d-8 ) then
      polyDeriv = 1.0d0    ! because then y(x) = ya(i)
      return
    end if

    ! calculate polyDeriv
    productDivxmxi = 1.0d0
    derivProd      = 1.0d0
    do j = 1, n
      if ( i /= j) then
        productDivxmxi = productDivxmxi * (x - xa(j))
        derivProd      = derivProd      * (xa(i) - xa(j))
      end if
    end do ! j
    polyDeriv = productDivxmxi / derivProd

  end function polyDeriv


  function splintLinDeriv(errS, xa, ya, x, statusSplint)

    ! splintLinDeriv returns the first derivative i.e. dy(x)/dx
    ! assuming linear interpolation
    ! y(x) is specified by x values in xa and y values in ya.
    ! It is assumed that the values in xa are increasing, and bisection
    ! is used to locate the nodes close to x.

    implicit none

    type(errorType), intent(inout) :: errS
    REAL(8), DIMENSION(:), INTENT(IN)  :: xa, ya
    REAL(8),               INTENT(IN)  :: x
    integer,               INTENT(OUT) :: statusSplint
    REAL(8) :: splintLinDeriv
    integer :: khi,klo,n
    REAL(8) :: h

    ! assume correct processing
    statusSplint = 0
    ! if the input 

    n = size(xa)
    
    if (n < 1) then
      call mystop(errS, 'index n less than 1 in splintLinDeriv()')
      return
    end if

    klo=max(min(locate(xa,x,n),n-1),1)
    khi=klo+1
    h=xa(khi)-xa(klo)
    if (h == 0.0d0) then
      statusSplint = -1
      call logDebug('x_i and x_i+1 are the same in splintLinDeriv')
      call logDebug('can not divide by zere')
      call mystop(errS, 'stopped in mathToolsModule: splintLinDeriv : array elements are equal')
      if (errorCheck(errS)) return
    end if
    splintLinDeriv = (ya(khi)-ya(klo))/h

  end function splintLinDeriv



    pure function verfy_elem_differ(x, threshold)

      ! verfy_elem_differ is true if all the elements of the array x differ
      ! from each other and that the absolute difference is larger than the threshold 

      implicit none

      real(8), intent(in) :: x(:)     
      real(8), intent(in) :: threshold
      logical             :: verfy_elem_differ
      
      ! local
      integer  :: i, k, n
      real(8)  :: tmp
      
      n = size (x)
      
      verfy_elem_differ = .true.
      if ( n < 2 ) then
        return
      else
        do i = 1, n
          do k = 2, n
            if ( k /= i  ) then
              tmp = abs( x(i) - x(k) )
              if ( tmp < threshold ) then
                verfy_elem_differ = .false.
                return
              end if ! tmp < threshold 
            end if ! k /= i
          end do ! k
        end do ! i
      end if   

    end function verfy_elem_differ

!

    pure FUNCTION vabs(v)
    REAL(8), DIMENSION(:), INTENT(IN) :: v
    REAL(8) :: vabs
    vabs=sqrt(dot_product(v,v))
    END FUNCTION vabs


  subroutine interpol6D(errS,                 &
    dim1, dim2, dim3 , dim4, dim5, dim6,      &
    axis1, axis2, axis3, axis4, axis5, axis6, &
    data6D,                                   &
    x1, x2, x3, x4, x5, x6,                   &
    value,                                    &
    status)

    !input
    type(errorType),       intent(inout) :: errS
    integer,               intent(in) :: dim1, dim2, dim3, dim4, dim5, dim6
    real, dimension(dim1), intent(in) :: axis1
    real, dimension(dim2), intent(in) :: axis2
    real, dimension(dim3), intent(in) :: axis3
    real, dimension(dim4), intent(in) :: axis4
    real, dimension(dim5), intent(in) :: axis5
    real, dimension(dim6), intent(in) :: axis6
    real, dimension(dim1,dim2,dim3,dim4,dim5,dim6), intent(in) :: data6D
    real, intent(in) :: x1,x2,x3,x4,x5,x6

    !OUTPUT
    real,    intent(out) :: value
    integer, intent(out) :: status

    !LOCAL
    integer, parameter :: ndim=6
    integer :: node1, node2,node3,node4,node5,node6
    real, dimension(2,ndim) :: distance
    real, dimension(2**ndim) :: weights, values
    integer :: i, j, k, l, m, n
    integer :: npoint
    real    :: d1,d2,d3,d4,d5,d6
    real, parameter :: small = 1.0E-30

    Status = 0 ! set to success

    ! Init output:
    value = 0.0

    !find nodes
    node1 = findNode(axis1, dim1, x1)
    node2 = findNode(axis2, dim2, x2)
    node3 = findNode(axis3, dim3, x3)
    node4 = findNode(axis4, dim4, x4)
    node5 = findNode(axis5, dim5, x5)
    node6 = findNode(axis6, dim6, x6)

    !consider case node = 0 (extrapolation)
    if (node1 == 0) node1 = 1 
    if (node2 == 0) node2 = 1 
    if (node3 == 0) node3 = 1 
    if (node4 == 0) node4 = 1 
    if (node5 == 0) node5 = 1 
    if (node6 == 0) node6 = 1 

    !consider case node = dim (extrapolation)
    if (node1 == dim1) node1 = dim1-1 
    if (node2 == dim2) node2 = dim2-1 
    if (node3 == dim3) node3 = dim3-1 
    if (node4 == dim4) node4 = dim4-1 
    if (node5 == dim5) node5 = dim5-1 
    if (node6 == dim6) node6 = dim6-1 

    !precompute binsize
    d1 = axis1(node1+1) - axis1(node1)
    d2 = axis2(node2+1) - axis2(node2)
    d3 = axis3(node3+1) - axis3(node3)
    d4 = axis4(node4+1) - axis4(node4)
    d5 = axis5(node5+1) - axis5(node5)
    d6 = axis6(node6+1) - axis6(node6)

    ! Catch div by zero
    if ((abs(d1) < small) .OR. (abs(d2) < small) .OR. (abs(d3) < small) .OR. & 
        (abs(d4) < small) .OR. (abs(d5) < small) .OR. (abs(d6) < small)) Then
        Status = 1
        return
    end if

    !precompute distances
    distance(1,1) = ( axis1(node1+1) - x1 ) / d1
    distance(2,1) = ( x1 - axis1(node1)   ) / d1
    distance(1,2) = ( axis2(node2+1) - x2 ) / d2
    distance(2,2) = ( x2 - axis2(node2)   ) / d2
    distance(1,3) = ( axis3(node3+1) - x3 ) / d3
    distance(2,3) = ( x3 - axis3(node3)   ) / d3
    distance(1,4) = ( axis4(node4+1) - x4 ) / d4
    distance(2,4) = ( x4 - axis4(node4)   ) / d4
    distance(1,5) = ( axis5(node5+1) - x5 ) / d5
    distance(2,5) = ( x5 - axis5(node5)   ) / d5
    distance(1,6) = ( axis6(node6+1) - x6 ) / d6
    distance(2,6) = ( x6 - axis6(node6)   ) / d6

    !compute function values and weights
    npoint = 0
    do i = node1, node1 + 1
      do j = node2, node2 + 1
        do k = node3, node3 + 1
          do l = node4, node4 + 1
            do m = node5, node5 + 1
              do n = node6, node6 + 1

                npoint = npoint + 1

                weights(npoint) = distance(i+1-node1,1)*&
                                  distance(j+1-node2,2)*&
                                  distance(k+1-node3,3)*&
                                  distance(l+1-node4,4)*&
                                  distance(m+1-node5,5)*&
                                  distance(n+1-node6,6)

                values(npoint) = data6D(i,j,k,l,m,n)

              END DO
            END DO
          END DO
        END DO
      END DO    
    END DO  

    !compute interpolated value
    value = DOT_PRODUCT( weights, values )

  end subroutine interpol6D


  INTEGER FUNCTION findNode(axis, dim, x)
      !
      ! Purpose: Find the node (location) of the value X in an array 
      ! 
      ! Author: Roeland van Oss, 2003
      ! Modifications: Olaf Tuinder, 20050117
      ! Modifications: Johan de Haan, 2015 08 24
      !
      ! Input: axis, dim, x
      ! InOut: -
      ! Output (as a function value): findNode
      ! Dependencies: -
      !  
      ! Basic Flow Diagram
      !
      ! Find the nearest point near X
      ! Check upper boundary condition
      !

      integer, intent(in) :: dim
      real, dimension(dim), intent(in) :: axis
      real, intent(in) :: x
      integer, dimension(1) :: nearestLocation

      ! deal with two exceptions:
      ! (1) x = axis(1)
      ! (2) x = axis(dim)
      if ( x == axis(1) ) then
          findNode = 1
          return
      end if

      if ( x == axis(dim) ) then
          findNode = dim -1
          return
      end if

      ! find the nearest grid point near x
      nearestLocation = minloc (abs (axis-x)) 

      ! Check upper boundary condition
      if (axis(nearestLocation(1)) > x ) then
          findNode = nearestLocation(1) - 1
      else
          findNode = nearestLocation(1)
      end if

  END FUNCTION findNode


    subroutine getSmoothAndDiffXsec(errS, degreePoly, nwavel, wavel, Xsec, XsecSmooth,  XsecDiff)

      ! Fit a low degree (Legendre) polynomial to the absorption cross section.
      ! Use Legendre polynomials as they are orthogonal.

      implicit none
  
      type(errorType), intent(inout) :: errS
      integer, intent(in)  :: degreePoly          ! degree of the polynomial
      integer, intent(in)  :: nwavel              ! number of wavelengths ( 0, 1, .., nwavel)
      real(8), intent(in)  :: wavel(nwavel)       ! wavelengths                   
      real(8), intent(in)  :: Xsec(nwavel)        ! absorption Xsec
      real(8), intent(out) :: XsecSmooth(nwavel)  ! smooth part absorption Xsec
      real(8), intent(out) :: XsecDiff(nwavel)    ! differential part absorption Xsec

      ! local
      integer :: iwave
      real(8) :: wStart, wEnd

      real(8) :: sig(nwavel)
      real(8) :: wavelScaled(nwavel)

      real(8) :: a(0:degreePoly )
      real(8) :: w(0:degreePoly )
      real(8) :: v(0:degreePoly, 0:degreePoly)

      real(8) :: chisq  ! chi**2 of the fit

      integer, parameter :: addtionalOutputUnit   = 16

      logical, parameter :: verbose = .false.

      wStart = wavel(1)
      wEnd   = wavel(nwavel)
      wavelScaled(:) = 2.0d0 * ( wavel(:) - wStart ) / ( wEnd - wStart ) - 1.0d0

      ! assume constant errors for the least squares polynomial fit
      sig(:) = 1.0d0

      ! perform Legendre polynomial fit
      call svdfit(errS, wavelScaled, Xsec, sig, a, v, w, chisq, fleg)
      if (errorCheck(errS)) return

      ! calculate the smooth part of the absorption cross section.
      do iwave = 1, nwavel
        XsecSmooth(iwave) = dot_product( a, fleg(wavelScaled(iwave), degreePoly+1) )
      end do ! iwave

      ! Calculate the differential part of the  absorption cross section
      XsecDiff(:)   = Xsec(:) - XsecSmooth(:)

      if ( verbose ) then
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,*) 'output from getSmoothAndDiffXsec in module mathTools'
        write(addtionalOutputUnit,*)
        write(addtionalOutputUnit,'(A, 20ES20.5)') 'coefficients a: ', a
        write(addtionalOutputUnit,*) '  wavel(nm)       Xsec       XsecSmooth        XseDiff'
        do iwave = 1, nwavel
          write(addtionalOutputUnit,'(F12.6, 3ES15.5)') &
            wavel(iwave), Xsec(iwave), XsecSmooth(iwave), XsecDiff(iwave)
        end do

      end if

    end subroutine getSmoothAndDiffXsec


  function fitAndEvalLegPolynomial(errS, xFit, yFit, x)

    ! Input values: yFit on the grid xFit, and output grid x.
    ! subroutine svdfit is used to evaluate the coefficients
    ! for the Legendre polynomial. Next the Legandre polynomial
    ! is evaluated for the points given in x.

    implicit none

    type(errorType), intent(inout) :: errS
    real(8), intent(in) :: xFit(:), yFit(:), x(:)
    real(8)             :: fitAndEvalLegPolynomial(size(x))


 
    ! local
    real(8) :: xScaled(size(x))
    real(8) :: xFitScaled(size(xFit)) 
    real(8) :: sig(size(xFit)) 
    real(8) :: a(size(xFit))
    real(8) :: w(size(xFit))
    real(8) :: v(size(xFit),size(xFit))
    real(8) :: chisq

    integer :: i

    ! first scale x to (-1,1) and scale xFit in the same manner
    xScaled(:) = 2.0d0 *    ( x(:) - x(1) ) / (x(size(x)) - x(1) ) - 1.0d0
    xFitScaled = 2.0d0 * ( xFit(:) - x(1) ) / (x(size(x)) - x(1) ) - 1.0d0

    sig = 1.0  ! all points have equal weight
    call svdfit(errS, xFitScaled, yFit, sig, a, v, w, chisq, fleg)
    if (errorCheck(errS)) return
    ! evaluate polynomial on grid x
    do i = 1, size(x)
      fitAndEvalLegPolynomial(i) = dot_product( a, fleg(xScaled(i),size(xFit)) )
    end do ! i

  end function fitAndEvalLegPolynomial


  function lnfitAndEvalLegPolynomial(errS, xFit, yFit, x)

    ! Input values: yFit on the grid xFit, and output grid x.
    ! the natural logarithm of yFit is taken before the Legendre polynomial is fitted
    ! subroutine svdfit is used to evaluate the coefficients
    ! for the Legendre polynomial. Next the Legandre polynomial
    ! is evaluated for the points given in x. The value returned
    ! is exp (polynomial)

    implicit none

    type(errorType), intent(inout) :: errS
    real(8), intent(in) :: xFit(:), yFit(:), x(:)
    real(8)             :: lnfitAndEvalLegPolynomial(size(x))
 
    ! local
    real(8) :: polynomial(size(x))
    real(8) :: xScaled(size(x))
    real(8) :: lnyFit(size(xFit))
    real(8) :: xFitScaled(size(xFit)) 
    real(8) :: sig(size(xFit)) 
    real(8) :: a(size(xFit))
    real(8) :: w(size(xFit))
    real(8) :: v(size(xFit),size(xFit))
    real(8) :: chisq
    integer :: i

    ! check that yFit contains positive values
    if (any( yFit <= 0.0d0 ) ) then
      call logDebug('Error in function lnfitAndEvaluateLegendrePolynomial')
      call logDebug('in module mathTools' )
      call logDebug('attempt to fit ln(yFit) but yFit is not positive')
      call mystop(errS, 'stopped because we do not take the log of negative values')
      if (errorCheck(errS)) return
    end if

    lnyFit(:) = log(yFit)

    ! scale x to (-1,1) and scale xFit in the same manner
    xScaled(:) = 2.0d0 *    ( x(:) - x(1) ) / (x(size(x)) - x(1) ) - 1.0d0
    xFitScaled = 2.0d0 * ( xFit(:) - x(1) ) / (x(size(x)) - x(1) ) - 1.0d0

    sig = 1.0  ! all points have equal weight
    call svdfit(errS, xFitScaled, lnyFit, sig, a, v, w, chisq, fleg)
    if (errorCheck(errS)) return
    ! evaluate polynomial on grid x
    do i = 1, size(x)
      polynomial(i) = dot_product( a, fleg(xScaled(i), size(xFit)) )
    end do ! i
    lnfitAndEvalLegPolynomial(:) = exp(polynomial(:))

  end function lnfitAndEvalLegPolynomial

    function slitfunction(errS, wavelHRS, iwaveInstr, nwaveInstr, wavelInstr, slitFunctionSpecsS, &
                          startIndex, endIndex)

      ! Function slitfunction returns an array with values of the slitfunction on the
      ! high resolution wavelength grid for one nominal detector wavelength given by
      ! wavelInstrS%wavel(iwavelInstr) 
      
      ! The index for the slit function and its FWHM are taken from the structure wavelInstrS.

      implicit none

      type(errorType),        intent(inout) :: errS
      type(wavelHRType),      intent(in)    :: wavelHRS                 ! high resolution wavelength grid
      integer,                intent(in)    :: iwaveInstr, nwaveInstr
      real(8),                intent(in)    :: wavelInstr(nwaveInstr)   ! wavelength grid for (simulated) measurements
      type(slitFunctionType), intent(inout) :: slitFunctionSpecsS       ! specification slit function
      integer,                intent(out)   :: startIndex, endIndex     ! indices used for numerical integration slit function
      real(8)                               :: slitfunction(wavelHRS%nwavel) ! array with slit function values on HR grid

      ! local
      integer    :: iwave, index, n, klo, khi, status
      integer    :: closestIndex(1)       ! index such that wavelHRS%wavel(closestIndex) is close to
                                          ! wavelInstr(iwaveInstr)
      real(8)    :: w, stdev, h
      real(8)    :: deltaWave, slitIntegrated, factor
      real(8)    :: slitValue(wavelHRS%nwavel)
      real(8)    :: deltaWaveInterpolated(slitFunctionSpecsS%nwavelListed)
      real(8)    :: slitFunctionInterpolated(slitFunctionSpecsS%nwavelListed)
      real(8)    :: FWHM, amplitude, scale, phase, mean, variance, skewness, kurtosis

      real(8), parameter     :: PI = 3.141592653589793d0


      FWHM      = slitFunctionSpecsS%FWHM
      amplitude = slitFunctionSpecsS%amplitude
      scale     = slitFunctionSpecsS%scale
      phase     = slitFunctionSpecsS%phase

      ! find indices startIndex and endIndex corresponding to 3*FWHM distance from the
      ! nominal wavelength of a spectral bin


      closestIndex = minloc(abs(wavelHRS%wavel(:) - wavelInstr(iwaveInstr) ) )
      index = closestIndex(1)
      do
        index = index - 1
        if ( index < 1 ) then
          startIndex = 1
          exit  ! exit loop
        end if
        if ( abs( wavelInstr(iwaveInstr) - wavelHRS%wavel(index) ) > 3.0 * FWHM ) then
          startIndex = index
          exit  ! exit loop
        end if
      end do

      index = closestIndex(1)
      do
        index = index + 1
        if ( index > wavelHRS%nwavel ) then
          endIndex = wavelHRS%nwavel
          exit  ! exit loop
        end if
        if ( abs( wavelInstr(iwaveInstr) - wavelHRS%wavel(index) ) > 3.0 * FWHM ) then
          endIndex = index
          exit  ! exit loop
        end if
      end do

      ! initialize slit function values to zero
      slitValue = 0.0d0

      ! use select case for the various slit function types
      select case ( slitFunctionSpecsS%slitIndex )

        case(0) ! gaussian slit function
                ! FWHM/stdev= 2*SQRT(2*ln(2)) =  2.354820045d0

          stdev          = FWHM / 2.354820045d0
          w              = sqrt(2.0d0) * stdev
          factor         = 1.0d0 / stdev / sqrt( 2.0d0 * PI )
          slitIntegrated = 0.0d0                              
          mean           = 0.0d0
          do iwave = startIndex, endIndex
            deltaWave        = wavelHRS%wavel(iwave) - wavelInstr(iwaveInstr)
            slitValue(iwave) = factor * exp( - ( deltaWave / w )**2 )
            ! scale amplitude with peakvalue
            amplitude = factor * amplitude
            slitValue(iwave) = slitValue(iwave)*(1.0 + amplitude * (sin( scale * deltaWave / FWHM + phase) ) **2)
            slitIntegrated   = slitIntegrated +  wavelHRS%weight(iwave) * slitValue(iwave)
            mean = mean + wavelHRS%weight(iwave) * slitValue(iwave) * deltaWave
          end do

          ! return values are normalized by the integrated value over the slit function
          ! to make sure that the numerical integral over the slit function is one
          slitfunction = slitValue / slitIntegrated
          mean = mean / slitIntegrated

        case(1) ! flat topped slit function with n = 4
                ! S = const* 2**(-2* (x - x0)**4/w**4)                                                
                ! FWHM = 1.681793 * w

          w              = slitFunctionSpecsS%FWHM / 1.681793d0    
          slitIntegrated = 0.0d0                              
          mean           = 0.0d0
          do iwave = startIndex, endIndex
            deltaWave        = wavelHRS%wavel(iwave) - wavelInstr(iwaveInstr)
            slitValue(iwave) = 2**( - 2*(deltaWave / w)**4 )
            slitValue(iwave) = slitValue(iwave)*(1.0 + amplitude * (sin( scale * deltaWave / FWHM + phase) ) **2)
            slitIntegrated   = slitIntegrated + wavelHRS%weight(iwave) * slitValue(iwave)
            mean = mean + wavelHRS%weight(iwave) * slitValue(iwave) * deltaWave
          end do
          ! return values are normalized by the integrated value over the slit function
          ! to make sure that the numerical integral over the slit function is one
          slitfunction = slitValue / slitIntegrated
          mean = mean / slitIntegrated

        case(2) ! 3 times flat topped slit function with n = 4
                ! S = const* 2**(-2* (x - x0)**4/w**4)                                                
                ! FWHM = 1.681793 * w

          w              = slitFunctionSpecsS%FWHM / 1.681793d0    
          slitIntegrated = 0.0d0                              
          mean           = 0.0d0
          do iwave = startIndex, endIndex
            deltaWave        = wavelHRS%wavel(iwave) - wavelInstr(iwaveInstr)
            slitValue(iwave) = 2**( - 2*(deltaWave / w)**4 )
            slitValue(iwave) = slitValue(iwave) + 2**( - 2*((deltaWave - 0.1) / w)**4 )
            slitValue(iwave) = slitValue(iwave) + 2**( - 2*((deltaWave + 0.1) / w)**4 )
            slitIntegrated   = slitIntegrated + wavelHRS%weight(iwave) * slitValue(iwave)
            mean = mean + wavelHRS%weight(iwave) * slitValue(iwave) * deltaWave
          end do
          ! return values are normalized by the integrated value over the slit function
          ! to make sure that the numerical integral over the slit function is one
          slitfunction = slitValue / slitIntegrated
          mean = mean / slitIntegrated

        case(5) ! tabulated slitunction with parameters read from file, e.g. GOME 2

          ! determine indices klo and khi so that wavelInstr(iwaveInstr) lies between
          ! slitFunctionSpecsS%wavelNominal(index - 1) and slitFunctionSpecsS%wavelNominal(index)
          n   = size(slitFunctionSpecsS%wavelNominal)
          if (n < 1) then
            call mystop(errS, 'index n less than 1 in splintLinDeriv()')
            return
          end if
          klo = max(min(locate(slitFunctionSpecsS%wavelNominal,wavelInstr(iwaveInstr),n),n-1),1)
          khi = klo+1
          ! change indices when khi and klo pertain to different spectral bands
          ! and use extrapolation within the spectral band
          if ( slitFunctionSpecsS%bandNumber(klo) /= slitFunctionSpecsS%bandNumber(khi) ) then
            ! 500 is the pixel number for about the middle of the spectral band
            if ( slitFunctionSpecsS%pixelNumber(klo) > 500 ) then
              ! near the end of a spectral band
              klo = klo - 1
              khi = khi - 1
            else
              ! near the beginning of a spectral band
              klo = klo + 1
              khi = khi + 1
            end if
          end if

          ! linear interpolation (or extrapolation near the end of a spectral band)
          h   = slitFunctionSpecsS%wavelNominal(khi)- slitFunctionSpecsS%wavelNominal(klo)
          w   = ( wavelInstr(iwaveInstr) - slitFunctionSpecsS%wavelNominal(klo) ) / h
          deltaWaveInterpolated(:)    = slitFunctionSpecsS%deltawavelListed(:,khi)
          slitFunctionInterpolated(:) = w * slitFunctionSpecsS%slitFunctionTable(:,khi)  &
                                      + (1.0d0 - w) * slitFunctionSpecsS%slitFunctionTable(:,klo)

          mean           = 0.0d0
          slitIntegrated = 0.0d0                              
          do iwave = startIndex, endIndex
            deltaWave        = wavelHRS%wavel(iwave) - wavelInstr(iwaveInstr)
            slitValue(iwave) = splintLin(errS, deltaWaveInterpolated, slitFunctionInterpolated, &
                                          deltaWave, status)
            slitIntegrated   = slitIntegrated + wavelHRS%weight(iwave) * slitValue(iwave)
            mean = mean + wavelHRS%weight(iwave) * slitValue(iwave) * deltaWave
          end do
          ! return values are normalized by the integrated value over the slit function
          ! to make sure that the numerical integral over the slit function is one
          slitfunction = slitValue / slitIntegrated
          mean = mean / slitIntegrated
      
        case default

          call logDebug(' ERROR: incorrect value for the slitIndex in slitfunction')
          call logDebugI('        slitIndex = ', slitFunctionSpecsS%slitIndex)
          call mystop(errS, ' stopped because of an incorrect slitIndex')
          if (errorCheck(errS)) return

      end select

      variance       = 0.0d0
      skewness       = 0.0d0
      kurtosis       = 0.0d0
      do iwave = startIndex, endIndex
       deltaWave        = wavelHRS%wavel(iwave) - wavelInstr(iwaveInstr)
       variance = variance + slitfunction(iwave) * (deltaWave - mean)**2 * wavelHRS%weight(iwave)
      end do
      variance = variance - mean**2
      stdev    = sqrt(variance)
      do iwave = startIndex, endIndex
        deltaWave = wavelHRS%wavel(iwave) - wavelInstr(iwaveInstr)
        skewness  = skewness + slitfunction(iwave) * ((deltaWave - mean) /stdev )**3  * wavelHRS%weight(iwave)
      end do
      do iwave = startIndex, endIndex
        deltaWave = wavelHRS%wavel(iwave) - wavelInstr(iwaveInstr)
        kurtosis = kurtosis + slitfunction(iwave) * ((deltaWave - mean) /stdev )**4  * wavelHRS%weight(iwave)
      end do
      kurtosis = kurtosis - 3 

      ! store in datastructure
      slitFunctionSpecsS%mean              = mean
      slitFunctionSpecsS%standardDeviation = stdev
      slitFunctionSpecsS%skewness          = skewness
      slitFunctionSpecsS%kurtosis          = kurtosis
      slitFunctionSpecsS%slitIntegrated    = slitIntegrated
    end function slitfunction


!   New subroutines to replace numerical recipe. 20220113
!   The new subroutines are downloaded from netlib.org and https://jblevins.org/mirror/amiller/.

    SUBROUTINE GaussDivPoints(errS, a0, b0, t, w, npoints)
    !gaussq(errS, a0, b0, t, w, n)    
    !gaussDivPoints(errS, -1.d0, 1.d0, xg1, wg1, 8) 
    !   SUBROUTINE gaussq(kind, n, alpha, beta, kpts, endpts, b, t, w)
    !
    !        This set of routines computes the nodes t(j) and weights
    !        w(j) for gaussian-type quadrature rules with pre-assigned
    !        nodes.  these are used when one wishes to approximate

    !                 integral (from a to b)  f(x) w(x) dx

    !                              n
    !        by                   sum w  f(t )
    !                             j=1  j    j

    !        (note w(x) and w(j) have no connection with each other.)
    !        here w(x) is one of six possible non-negative weight
    !        functions (listed below), and f(x) is the
    !        function to be integrated.  gaussian quadrature is particularly
    !        useful on infinite intervals (with appropriate weight
    !        functions), since then other techniques often fail.

    !           associated with each weight function w(x) is a set of
    !        orthogonal polynomials.  the nodes t(j) are just the zeroes
    !        of the proper n-th degree polynomial.

    !     input parameters (all real numbers are in double precision)

    !        kind     an integer between 1 and 6 giving the type of
    !                 quadrature rule:  
    !        kind = 1:  legendre quadrature, w(x) = 1 on (-1, 1)
    !        kind = 2:  chebyshev quadrature of the first kind
    !                   w(x) = 1/sqrt(1 - x*x) on (-1, +1)
    !        kind = 3:  chebyshev quadrature of the second kind
    !                   w(x) = sqrt(1 - x*x) on (-1, 1)
    !        kind = 4:  hermite quadrature, w(x) = exp(-x*x) on
    !                   (-infinity, +infinity)
    !        kind = 5:  jacobi quadrature, w(x) = (1-x)**alpha * (1+x)**
    !                   beta on (-1, 1), alpha, beta .gt. -1.
    !                   note: kind=2 and 3 are a special case of this.
    !        kind = 6:  generalized laguerre quadrature, w(x) = exp(-x)*
    !                   x**alpha on (0, +infinity), alpha .gt. -1
    !
    !        n        the number of points used for the quadrature rule
    !  
    !     output parameters (both double precision arrays of length n)

    !        t        will contain the desired nodes.
    !        w        will contain the desired weights w(j).
    !
    !     other parameters removed from input because of 0 for kind 1
    !        alpha    real parameter used only for gauss-jacobi and gauss-
    !                 laguerre quadrature (otherwise use 0.d0).
    !        beta     real parameter used only for gauss-jacobi quadrature--
    !                 (otherwise use 0.d0)
    !        kpts     (integer) normally 0, unless the left or right end-
    !                 point (or both) of the interval is required to be a
    !                 node (this is called gauss-radau or gauss-lobatto
    !                 quadrature).  then kpts is the number of fixed
    !                 endpoints (1 or 2).
    !        endpts   real array of length 2.  contains the values of
    !                 any fixed endpoints, if kpts = 1 or 2.
    !        b        real scratch array of length n
    !     underflow may sometimes occur, but is harmless.

    !     references
    !        1.  golub, g. h., and welsch, j. h., "calculation of gaussian
    !            quadrature rules," mathematics of computation 23 (april,
    !            1969), pp. 221-230.
    !        2.  golub, g. h., "some modified matrix eigenvalue problems,"
    !            siam review 15 (april, 1973), pp. 318-334 (section 7).
    !        3.  stroud and secrest, gaussian quadrature formulas, prentice-
    !            hall, englewood cliffs, n.j., 1966.

    !        original version 20 jan 1975 from stanford
    !        modified 21 dec 1983 by eric grosse
    !          imtql2 => gausq2
    !          hex constant => d1mach (from core library)
    !          compute pi using datan
    !          removed accuracy claims, description of method
    !          added single precision version
    !    
    !      Original code was downloaded from http://www.netlib.org/go/gaussq.f 
    !      Inputs variables and subroutine name are adapted to disamar. 
    !      dgamma is not needed used in disamar 
    !      d1mach() is not used in gausq2, set to 2.0d-16 
    !      a0, b0 are added to scale w, t from inteval (-1, 1) to (a0, b0).
    !      changed the variable kind to gaussq_kind, only kind = 1 is used,
    !      other kind options and correponding codes are removed.
    !      
    !      P. Wang KNMI 06 Jan 2022

    IMPLICIT NONE
    INTEGER                         :: gaussq_kind
    INTEGER, optional, INTENT(IN)   :: npoints
    REAL(8), INTENT(IN)             :: a0, b0  
    REAL(8), INTENT(OUT)            :: t(:)
    REAL(8), INTENT(OUT)            :: w(:)
    type(errorType), intent(inout)  :: errS
    REAL(8)                         :: alpha
    REAL(8)                         :: beta
    INTEGER                         :: kpts
    REAL(8)                         :: endpts(2)
    REAL(8),     dimension(size(t)) :: b
    REAL(8)                         :: muzero, t1, gam, solve, DSQRT
    INTEGER                         :: i, ierr, n

!   added to match disamar calls, in some calls npoints is missing. 
    if ( present(npoints) ) then
       n = npoints
    else
       n = size(t)
    end if
    
    
    alpha = 0.0D0
    beta = 0.0D0
    kpts = 0.0D0 
    endpts = 0.0D0

    gaussq_kind = 1

    CALL class (gaussq_kind, n, alpha, beta, b, t, muzero)

    !           the matrix of coefficients is assumed to be symmetric.
    !           the array t contains the diagonal elements, the array
    !           b the off-diagonal elements.
    !           make appropriate changes in the lower right 2 by 2
    !           submatrix.

!    IF (kpts == 0)  GO TO 100

    !           note that the indices of the elements of b run from 1 to n-1
    !           and thus the value of b(n) is arbitrary.
    !           now compute the eigenvalues of the symmetric tridiagonal
    !           matrix, which has been modified as necessary.
    !           the method used is a ql-type method with origin shifting

    w(1) = 1.0D0           ! line number 100
    DO  i = 2, n
      w(i) = 0.0D0
    END DO

    CALL gausq2 (n, t, b, w, ierr)
    
    if (ierr > 0) then  
        call mystop(errS, 'in gausq2 no convergence to an eigenvalue after 30 iterations')
    end if    
    
    DO  i = 1, n
      w(i) = muzero * w(i) * w(i)
    END DO
    
    ! scale w, t from interval (-1, 1) to interval (a0, b0)  
    w = w / 2.0d0 * (b0 - a0)
    t = (t + 1.0d0) / 2.0d0 * (b0-a0) + a0
    
   
    RETURN
    END SUBROUTINE GaussDivPoints



    FUNCTION solve(shift, n, a, b)

    !       this procedure performs elimination to solve for the
    !       n-th component of the solution delta to the equation

    !             (jn - shift*identity) * delta  = en,

    !       where en is the vector of all zeroes except for 1 in
    !       the n-th position.

    !       the matrix jn is symmetric tridiagonal, with diagonal
    !       elements a(i), off-diagonal elements b(i).  this equation
    !       must be solved to obtain the appropriate changes in the lower
    !       2 by 2 submatrix of coefficients for orthogonal polynomials.

    IMPLICIT NONE
    REAL(8), INTENT(IN)             :: shift
    INTEGER, INTENT(IN)             :: n
    REAL(8), INTENT(IN)             :: a(n)
    REAL(8), INTENT(IN)             :: b(n)
    REAL(8) :: alpha, solve
    INTEGER :: i, nm1                    

    alpha = a(1) - shift
    nm1 = n - 1
    DO  i = 2, nm1
      alpha = a(i) - shift - b(i-1)**2/alpha
    END DO
    solve = 1.0D0/alpha
    RETURN
    END FUNCTION solve



    SUBROUTINE class(gaussq_kind, n, alpha, beta, b, a, muzero)

    !           this procedure supplies the coefficients a(j), b(j) of the
    !        recurrence relation

    !             b p (x) = (x - a ) p   (x) - b   p   (x)
    !              j j            j   j-1       j-1 j-2

    !        for the various classical (normalized) orthogonal polynomials,
    !        and the zero-th moment

    !             muzero = integral w(x) dx

    !        of the given polynomial's weight function w(x).  since the
    !        polynomials are orthonormalized, the tridiagonal matrix is
    !        guaranteed to be symmetric.

    !           the input parameter alpha is used only for laguerre and
    !        jacobi polynomials, and the parameter beta is used only for
    !        jacobi polynomials.  the laguerre and jacobi polynomials
    !        require the gamma function.
    !        
    !        only keep the kind = 1, other options are removed.
    !        P. Wang. 06 Jan. 2022

    IMPLICIT NONE
    INTEGER, INTENT(IN OUT)                  :: gaussq_kind
    INTEGER, INTENT(IN)                      :: n
    REAL(8), INTENT(IN)             :: alpha
    REAL(8), INTENT(IN)             :: beta
    REAL(8), INTENT(OUT)            :: b(n)
    REAL(8), INTENT(OUT)            :: a(n)
    REAL(8), INTENT(OUT)            :: muzero
    
    INTEGER :: i, nm1
    REAL(8) :: abi, a2b2, dgamma, pi, DSQRT, ab

    pi = 4.0D0 * DATAN(1.0D0)
    nm1 = n - 1

    !              gaussq_kind = 1:  legendre polynomials p(x)
    !              on (-1, +1), w(x) = 1.

    muzero = 2.0D0            ! line numer 10
    DO  i = 1, nm1
      a(i) = 0.0D0
      abi = i
      b(i) = abi/DSQRT(4*abi*abi - 1.0D0)
    END DO
    a(n) = 0.0D0
    RETURN

    END SUBROUTINE class


    SUBROUTINE gausq2(n, d, e, z, ierr)

    !     this subroutine is a translation of an algol procedure,
    !     num. math. 12, 377-383(1968) by martin and wilkinson,
    !     as modified in num. math. 15, 450(1970) by dubrulle.
    !     handbook for auto. comp., vol.ii-linear algebra, 241-248(1971).
    !     this is a modified version of the 'eispack' routine imtql2.

    !     this subroutine finds the eigenvalues and first components of the
    !     eigenvectors of a symmetric tridiagonal matrix by the implicit ql
    !     method.

    !     on input:

    !        n is the order of the matrix;

    !        d contains the diagonal elements of the input matrix;

    !        e contains the subdiagonal elements of the input matrix
    !          in its first n-1 positions.  e(n) is arbitrary;

    !        z contains the first row of the identity matrix.

    !      on output:

    !        d contains the eigenvalues in ascending order.  if an
    !          error exit is made, the eigenvalues are correct but
    !          unordered for indices 1, 2, ..., ierr-1;

    !        e has been destroyed;

    !        z contains the first components of the orthonormal eigenvectors
    !          of the symmetric tridiagonal matrix.  if an error exit is
    !          made, z contains the eigenvectors associated with the stored
    !          eigenvalues;

    !        ierr is set to
    !          zero       for normal return,
    !          j          if the j-th eigenvalue has not been
    !                     determined after 30 iterations.

    !     ------------------------------------------------------------------

    IMPLICIT NONE
    INTEGER, INTENT(IN)                      :: n
    REAL*8, INTENT(IN OUT)                   :: d(n)
    REAL*8, INTENT(OUT)                      :: e(n)
    REAL*8, INTENT(IN OUT)                   :: z(n)
    INTEGER, INTENT(OUT)                     :: ierr
    INTEGER :: i, j, k, l, m, ii, mml
    REAL*8  b, c, f, g, p, r, s, machep
    REAL*8 DSQRT, DABS, DSIGN, d1mach

    !
    !machep=d1mach(4)     ! commented by P. Wang 2022-01-06
    machep = 2.0d-16


    ierr = 0
    IF (n == 1) GO TO 1001

    e(n) = 0.0D0
    DO  l = 1, n
      j = 0
    !     :::::::::: look for small sub-diagonal element ::::::::::
      105    DO  m = l, n
        IF (m == n) GO TO 120
        IF (DABS(e(m)) <= machep * (DABS(d(m)) + DABS(d(m+1)))) GO TO 120
      END DO
      
      120    p = d(l)
      IF (m == l) CYCLE
      IF (j == 30) GO TO 1000
      j = j + 1
    !     :::::::::: form shift ::::::::::
      g = (d(l+1) - p) / (2.0D0 * e(l))
      r = DSQRT(g*g+1.0D0)
      g = d(m) - p + e(l) / (g + DSIGN(r, g))
      s = 1.0D0
      c = 1.0D0
      p = 0.0D0
      mml = m - l
      
    !     :::::::::: for i=m-1 step -1 until l do -- ::::::::::
      DO  ii = 1, mml
        i = m - ii
        f = s * e(i)
        b = c * e(i)
        IF (DABS(f) < DABS(g)) GO TO 150
        c = g / f
        r = DSQRT(c*c+1.0D0)
        e(i+1) = f * r
        s = 1.0D0 / r
        c = c * s
        GO TO 160
        150       s = f / g
        r = DSQRT(s*s+1.0D0)
        e(i+1) = g * r
        c = 1.0D0 / r
        s = s * c
        160       g = d(i+1) - p
        r = (d(i) - g) * s + 2.0D0 * c * b
        p = s * r
        d(i+1) = g + p
        g = c * r - b
    !     :::::::::: form first component of vector ::::::::::
        f = z(i+1)
        z(i+1) = s * z(i) + c * f
        z(i) = c * z(i) - s * f
      END DO
      
      d(l) = d(l) - p
      e(l) = g
      e(m) = 0.0D0
      GO TO 105
    END DO

    !     :::::::::: order eigenvalues and eigenvectors ::::::::::
    DO  ii = 2, n
      i = ii - 1
      k = i
      p = d(i)
      
      DO  j = ii, n
        IF (d(j) >= p) CYCLE
        k = j
        p = d(j)
      END DO
      
      IF (k == i) CYCLE
      d(k) = d(i)
      d(i) = p
      p = z(i)
      z(i) = z(k)
      z(k) = p
    END DO

    GO TO 1001
    !     :::::::::: set error -- no convergence to an
    !                eigenvalue after 30 iterations ::::::::::
    1000 ierr = l
    1001 RETURN
    !     :::::::::: last card of gausq2 ::::::::::
    END SUBROUTINE gausq2
    

    ! The FMM collection
    ! PURPOSE - A collection of Fortran procedures for mathematical computation
    !   based on the procedures from the book "Computer Methods for Mathematical
    !   Computations", by George E. Forsythe, Michael A. Malcolm, and 
    !   Cleve B. Moler. Prentice-Hall, 1977.
    
    !  COLLECTION AUTHORS - George E. Forsythe, Michael A. Malcolm, & 
    !    Cleve B. Moler, Stanford University (1977)
    !    Ralph L. Carmichael, Public Domain Aeronautical Software
    
    ! ORIGINAL AUTHORS - 
    !  Decomp#,Solve# - Forsythe,Malcolm,Moler  
    !  Spline,Seval# - Forsythe,Malcolm,Moler  
    !  Quanc8 - Forsythe,Malcolm,Moler
    !  RKF45 - H.A. Watts AND L.F. Shampine  (Sandia)
    !  ZEROIN - Richard Brent
    !  FMIN - Van Winjngaarden,Decker,Brent, Et Al
    !  SVD - Golub and Reinsch
    
    !  REVISION HISTORY
    !   DATE  VERS PERSON STATEMENT OF CHANGES
    !   1977   1.0   FMM  Original publication
    ! 07Jul83  1.1   RLC  Mods for Fortran77
    ! 09Jul89  1.2   RLC  Added SEVAL3, IMPLICIT NONE
    ! 17Apr91  1.3   RLC  Fortran 90 style loops
    ! 10May92  1.4   RLC  Fortran 90 coding
    ! 23Aug00  1.5   RLC  Made compatible with Elf90
    ! 26Apr02  1.6   RLC  Added NaturalSpline #
    ! 03May02  1.7   RLC  Charged calling sequence of SVD
    ! 20May02  1.8   RLC  Added BrentZero - mod of ZeroIn
    ! 28May02  1.9   RLC  Added errCode to Decomp
    ! 28Jun02  1.91  RLC  Made cond OPTIONAL in Decomp
    ! 21Aug02  1.95  RLC  Made single and double precision versions 
    ! 11Oct02  1.96  RLC  Compute eps each time in RKFS
    !
    ! https://www.pdas.com/fmm2.html 
    ! (#) subroutines used in Disamar
    !
!        
    SUBROUTINE Naturalspline(errS, x, y, dy2, status)
    !SUBROUTINE NaturalSplineDouble(errS, x,y,b,c,d)
    ! ---------------------------------------------------------------------------
    ! PURPOSE - Construct the natural spline thru a set of points
    ! NOTES - A natural spline has zero second derivative at both endpoints.
!
!     Original code from the FMM collection
!     Seval - original authors Forsythe,Malcolm,Moler
!     NaturalSpline - RLC Added  26Apr02   
!     https://www.pdas.com/fmm2.html 
!     
!     Changed the name to spline
!     Adapted to Disamar, P. Wang  06 Jan. 2022

      implicit none
      REAL(8),INTENT(IN),DIMENSION(:)  :: x, y   ! coordinates of knots
      REAL(8),INTENT(OUT),DIMENSION(:) :: dy2   ! cubic coeff.  b, c, d
      type(errorType), intent(inout)   :: errS
      INTEGER,     INTENT(OUT)         :: status
      !REAL(8),INTENT(OUT),DIMENSION(:):: b, c, d  ! cubic coeff.
      REAL(8),     DIMENSION(size(dy2)):: b, c, d  ! cubic coeff.
      !REAL(8) ,      DIMENSION(size(x)):: dy2
      INTEGER                          :: k,n
      REAL(8),                PARAMETER:: ZERO=0.0, TWO=2.0, THREE=3.0
    !-----------------------------------------------------------------------
      status = 0
       
      n=SIZE(x)
      
       
      IF (n < 3) THEN   ! Straight line - special case for n < 3
        b(1)=ZERO
        IF (n == 2) b(1)=(y(2)-y(1))/(x(2)-x(1))
        c(1)=ZERO
        d(1)=ZERO
        b(2)=b(1)
        c(2)=ZERO
        d(2)=ZERO
        RETURN
      END IF

      d(1:n-1) = x(2:n)-x(1:n-1)  ! Put the h-array of the text into array d
      

    !.....Set up the upper triangular system in locations 2 thru n-1 of
    !        arrays b and c. B holds the diagonal and c the right hand side.
      b(2)=TWO*(d(1)+d(2))
      c(2)=(y(3)-y(2))/d(2)-(y(2)-y(1))/d(1)
      DO  k=3,n-1
        b(k)=TWO*(d(k-1)+d(k))-d(k-1)*d(k-1)/b(k-1)
        c(k)=(y(k+1)-y(k))/d(k)-(y(k)-y(k-1))/d(k-1)-d(k-1)*c(k-1)/b(k-1)
      END DO

      c(n-1)=c(n-1)/b(n-1)   ! Back substitute to get c-array
      DO  k=n-2,2,-1
        c(k)=(c(k)-d(k)*c(k+1))/b(k)
      END DO
      c(1)=ZERO
      c(n)=ZERO   ! c now holds the sigma array of the text 


    !.....Compute polynomial coefficients ..................................
      b(n)=(y(n)-y(n-1))/d(n-1)+d(n-1)*(c(n-1)+c(n)+c(n))
      DO  k=1,n-1
        b(k)=(y(k+1)-y(k))/d(k)-d(k)*(c(k+1)+c(k)+c(k))
        d(k)=(c(k+1)-c(k))/d(k)
        c(k)=THREE*c(k)
      END DO
      c(n)=THREE*c(n)
      d(n)=d(n-1)
      
      dy2 = c * 2.0d0   ! convert to second derivatives
      RETURN
      
    END Subroutine Naturalspline   ! -------------


 
    function splint(errS, x, y, m, u, statusSplint)
      
      implicit none
      type(errorType),        intent(inout) :: errS
      integer,          intent(out) :: statusSplint
      real(8),          intent(in) :: u, x(:), y(:), m(:) !, d(n), b(n)
      real(8)                      :: splint  
      real(8)                      :: b, d  
      integer                      :: n
!      real(8),  dimension(size(x1)):: x, y, m          
!c
!c  this subroutine evaluates the cubic spline function
!c    c = m(i)/2
!c    b = (y(i+1)-y(i))/(x(i+1)-x(i)) - (2.0*m(i)+m(i+1))*(x(i+1)-x(i))/6.0
!c    d = (m(i+1)-m(i))/(6.0*x(i+1)-x(i))
!c    seval = y(i) + b*(u-x(i)) + c*(u-x(i))**2 + d*(u-x(i))**3
!c
!c    where  x(i) .lt. u .lt. x(i+1), using horner's rule
!c
!c  if  u .lt. x(1) then  i = 1  is used.
!c  if  u .ge. x(n) then  i = n  is used.
!c
!c  input..
!c
!c    n = the number of data points
!c    u = the abscissa at which the spline is to be evaluated
!c    x,y = the arrays of data abscissas and ordinates
!c    b,c,d = arrays of spline coefficients computed by spline
!c
!c  if  u  is not in the same interval as the previous call, then a
!c  binary search is performed to determine the proper interval.
!c
!     Original code from the FMM collection
!     Seval - original authors Forsythe,Malcolm,Moler
!     https://www.pdas.com/fmm2.html
!     
!     Adapted to Disamar, only c as input, calculate b, d, 
!     changed name seval to splint
!     replaced the search to call locate()
!     P. Wang 15 Jan. 2022
      
      integer                   :: i  
      real(8)                   :: dx
          
      ! assume correct processing
      statusSplint = 0
      i = 1
    
      n = size(x)
    
      if (n < 1) then
        call logDebug('ERROR: in mathTools: index n less than 1 in splint()')
        call mystop(errS, 'index n less than 1 in splint()')
        return
      end if
     
      i = max(min(locate(x,u,n),n-1),1)
      dx = u - x(i)
      
      if (x(i+1) == x(i)) then
        call logDebug('ERROR: in mathTools: divide 0 in splint()')
        call mystop(errS, 'Divide 0 in splint()')
        return
      end if
      
      b = (y(i+1)-y(i))/(x(i+1)-x(i))-(2.0d0*m(i)+m(i+1))*(x(i+1)-x(i))/6.0d0
      d = (m(i+1)-m(i))/(6.0d0*(x(i+1)-x(i)) ) 
   
      splint = y(i) + dx*(b + dx*(m(i)/2.0d0 + dx*d))
      
      return

    end function splint    


      ! 
    SUBROUTINE LU_Decomposition(errS, a, ipvt, d, errCode) 
    !SUBROUTINE DecompLU(errS, a, ipvt, d, errCode)  !, cond) 
    !pure subroutine LU_decomposition(errS,  a, indx, d, status)
    
    ! ---------------------------------------------------------------------------
    ! PURPOSE - Matrix triangularization by Gaussian elimination and estimation 
    !  of the condition of the matrix. The matrix a is replaced by the LU 
    !  decomposition of a rowwise permutation of itself. The array ipvt records
    !  the row permutations of the partial pivoting. ipvt(k) is the index of the
    !  kth pivot row and ipvt(n) is (-1)**(no. of interchanges). The optional
    !  variable cond is an estimate of the conditioning of a. For the linear
    !  system Ax=B, changes in A and B may cause changes cond times as large
    !  in x. If cond+1.0 == cond, then a is singular to the working precision.
    !  The determinant of a can be obtained on output as
    !    det = ipvt(n)*a(1,1)*a(2,2)*a(3,3)*...*a(n,n)
    ! NOTES - If errCode is not zero, the remaining arguments are not to be
    !   believed.
    !
    !   
    !   Original code is from the FMM collection 
    !   Decomp,Solve - original authors Forsythe,Malcolm,Moler   
    !   https://www.pdas.com/fmm2.html 
    !
    !   Adapted to Disamar by P. Wang     6 Jan. 2022
    !   added errS and d (=ipvt(n)) 
    !
      IMPLICIT NONE
      REAL(8),INTENT(IN OUT),DIMENSION(:,:):: a   ! matrix to be decomposed
      INTEGER,     INTENT(OUT),DIMENSION(:):: ipvt   !    index of pivot rows
      INTEGER,                  INTENT(OUT):: errCode   ! =0 OK; =1 singular
      REAL(8),                  INTENT(OUT):: d         ! = ipvt(n). 
      TYPE(errorType),       INTENT(inout) :: errS
    !  REAL(8),INTENT(OUT),OPTIONAL:: cond  ! condition number
      REAL(8)                              :: cond   
        
      REAL(8)                              :: anorm
      REAL(8)                              :: ek
      INTEGER                              :: j,k,m
      INTEGER,                 DIMENSION(1):: mloc  ! receives the result of MAXLOC
      INTEGER                              :: n
      REAL(8)                              :: t
      REAL(8),         DIMENSION(SIZE(a,1)):: work  ! scratch array
      REAL(8)                              :: ynorm,znorm
      REAL(8),                    PARAMETER:: ZERO=0.0d0, ONE=1.0d0
    !-----------------------------------------------------------------------
      errCode=0
      n=SIZE(a,1)

      IF (n <= 1) THEN
        IF (a(1,1) == ZERO) THEN
          errCode=1
          call logDebug('ERROR: in mathTools:  n<=1, a(1,1) = 0.0 in  LU_Decomposition, singular matrix')
          call mystop(errS, 'stopped in mathToolsModule: LU_Decomposition : n<=1 and a is singular')
        ELSE
          cond=ONE
        END IF
        RETURN        ! note abnormal RETURN
       END IF

!      IF (Present(cond)) THEN
!        anorm=ZERO   ! compute 1-norm of a
!        DO j=1,n
!          anorm=MAX(anorm, SUM(ABS(a(1:n,j))))
!        END DO   
!      END IF  

      DO k=1,n-1   ! Gaussian elimination with partial pivoting
        mloc= MAXLOC(ABS(a(k:n,k)))   ! pivot row
        m=k-1+mloc(1)
        ipvt(k)=m
        IF (m /= k) ipvt(n)=-ipvt(n)
        t=a(m,k)
        a(m,k)=a(k,k)
        a(k,k)=t
        IF (t /= ZERO) THEN
          t=ONE/t
          a(k+1:n,k)=-t*a(k+1:n,k)
          DO j=k+1,n   ! interchange and eliminate by columns.......
            t=a(m,j)
            a(m,j)=a(k,j)
            a(k,j)=t
            IF (t /= ZERO) a(k+1:n,j)=a(k+1:n,j) + t*a(k+1:n,k)
          END DO
        END IF
      END DO

      DO k=1,n   ! solve (a-transpose)*y=e
        t=ZERO
        IF (k > 1) t=DOT_PRODUCT(a(1:k-1,k), work(1:k-1))
        ek=ONE
        IF (t < ZERO) ek=-ONE
        IF (a(k,k) == ZERO) THEN    ! singular matrix
          errCode=1
          call logDebug('ERROR: in mathTools: a(k,k) = 0.0 in  LU_Decomposition, singular matrix')
          call mystop(errS, 'stopped in mathToolsModule: LU_Decomposition : a is singular')
          RETURN                            ! note abnormal RETURN
        END IF
        work(k)=-(ek+t)/a(k,k)
      END DO

      DO k=n-1,1,-1 
        t=ZERO
        t=work(k)*SUM(a(k+1:n,k))
        work(k)=t
        m=ipvt(k)
        IF (m /= k) THEN
          t=work(m)
          work(m)=work(k)
          work(k)=t
        END IF
      END DO
      d = ipvt(n)
!      IF (Present(cond)) THEN
!        ynorm=SUM(ABS(work(1:n)))
!        CALL SolveLU(a,work,ipvt)   ! solve a*z=y
!        znorm=SUM(ABS(work(1:n)))
!        cond=anorm*znorm/ynorm   ! estimate condition
!        IF (cond < ONE) cond=ONE
!      END IF
      
      
      RETURN
    END Subroutine LU_Decomposition   ! ---------------------------------------------------

    !+
    SUBROUTINE solve_lin_system_LU_based(errS, a, ipvt, b)
    !SUBROUTINE SolveLU(errS, a, ipvt, b)
    ! ---------------------------------------------------------------------------
    ! PURPOSE - Solve the linear system a*x=b
    !   Original code is from the FMM collection 
    !   Decomp,Solve - original authors Forsythe,Malcolm,Moler 
    !   https://www.pdas.com/fmm2.html   
    !
    !   Adapted to Disamar by P. Wang     6 Jan. 2022
      IMPLICIT NONE
      REAL(8),INTENT(IN),DIMENSION(:,:)    :: a  ! the decomposed matrix from Decomp
      INTEGER,INTENT(IN),DIMENSION(:)      :: ipvt  ! index of pivot rows (from Decomp)
      REAL(8),INTENT(IN OUT),DIMENSION(:)  :: b ! in:right-hand side; out:solution      
      type(errorType), intent(inout)       :: errS
      
      INTEGER                              :: i,k,m
      INTEGER                              :: n
      REAL(8)                              :: t
    !----------------------------------------------------------------------------
      n=SIZE(a,1)
      DO k=1,n-1   ! forward elimination
        m=ipvt(k)
        t=b(m)
        b(m)=b(k)
        b(k)=t
  
        b(k+1:n)=b(k+1:n) + t*a(k+1:n,k)
      END DO       

      DO k=n,1,-1      ! back substitution
        b(k)=b(k)/a(k,k)
        t=-b(k)
        DO i=1,k-1
          b(i)=b(i)+a(i,k)*t
        END DO       
      END DO
      RETURN
    END Subroutine solve_lin_system_LU_based   ! ----------------------------------------------------
    
    
      
    
    SUBROUTINE indexx(errS,x,ind)
 
    !SUBROUTINE qsortd(errS,x,ind)
 
   ! Code converted using TO_F90 by Alan Miller
   ! Date: 2002-12-18  Time: 11:55:47
   ! https://jblevins.org/mirror/amiller/qsortd.f90 
   !
   ! Adapted to Disamar by P. Wang 06. Jan. 2022
   ! changed name from qsortd to indexx.

    IMPLICIT NONE
    !INTEGER, PARAMETER  :: dp = SELECTED_REAL_KIND(12, 60)

    REAL (8), INTENT(IN)           :: x(:)
    INTEGER, INTENT(OUT)           :: ind(:)
    type(errorType), intent(inout) :: errS
    INTEGER                        :: n

    !***************************************************************************

    !                                                         ROBERT RENKA
    !                                                 OAK RIDGE NATL. LAB.

    ! THIS SUBROUTINE USES AN ORDER N*LOG(N) QUICK SORT TO SORT A REAL (dp)
    ! ARRAY X INTO INCREASING ORDER.  THE ALGORITHM IS AS FOLLOWS.  IND IS
    ! INITIALIZED TO THE ORDERED SEQUENCE OF INDICES 1,...,N, AND ALL INTERCHANGES
    ! ARE APPLIED TO IND.  X IS DIVIDED INTO TWO PORTIONS BY PICKING A CENTRAL
    ! ELEMENT T.  THE FIRST AND LAST ELEMENTS ARE COMPARED WITH T, AND
    ! INTERCHANGES ARE APPLIED AS NECESSARY SO THAT THE THREE VALUES ARE IN
    ! ASCENDING ORDER.  INTERCHANGES ARE THEN APPLIED SO THAT ALL ELEMENTS
    ! GREATER THAN T ARE IN THE UPPER PORTION OF THE ARRAY AND ALL ELEMENTS
    ! LESS THAN T ARE IN THE LOWER PORTION.  THE UPPER AND LOWER INDICES OF ONE
    ! OF THE PORTIONS ARE SAVED IN LOCAL ARRAYS, AND THE PROCESS IS REPEATED
    ! ITERATIVELY ON THE OTHER PORTION.  WHEN A PORTION IS COMPLETELY SORTED,
    ! THE PROCESS BEGINS AGAIN BY RETRIEVING THE INDICES BOUNDING ANOTHER
    ! UNSORTED PORTION.

    ! INPUT PARAMETERS -   N - LENGTH OF THE ARRAY X.

    !                      X - VECTOR OF LENGTH N TO BE SORTED.

    !                    IND - VECTOR OF LENGTH >= N.

    ! N AND X ARE NOT ALTERED BY THIS ROUTINE.

    ! OUTPUT PARAMETER - IND - SEQUENCE OF INDICES 1,...,N PERMUTED IN THE SAME
    !                          FASHION AS X WOULD BE.  THUS, THE ORDERING ON
    !                          X IS DEFINED BY Y(I) = X(IND(I)).

    !*********************************************************************

    ! NOTE -- IU AND IL MUST BE DIMENSIONED >= LOG(N) WHERE LOG HAS BASE 2.

    !*********************************************************************

    INTEGER   :: iu(21), il(21)
    INTEGER   :: m, i, j, k, l, ij, it, itt, indx
    REAL      :: r
    REAL (8)  :: t

    ! LOCAL PARAMETERS -

    ! IU,IL =  TEMPORARY STORAGE FOR THE UPPER AND LOWER
    !            INDICES OF PORTIONS OF THE ARRAY X
    ! M =      INDEX FOR IU AND IL
    ! I,J =    LOWER AND UPPER INDICES OF A PORTION OF X
    ! K,L =    INDICES IN THE RANGE I,...,J
    ! IJ =     RANDOMLY CHOSEN INDEX BETWEEN I AND J
    ! IT,ITT = TEMPORARY STORAGE FOR INTERCHANGES IN IND
    ! INDX =   TEMPORARY INDEX FOR X
    ! R =      PSEUDO RANDOM NUMBER FOR GENERATING IJ
    ! T =      CENTRAL ELEMENT OF X

    n = size(x)

    if (n > 2**21) then 
        call logDebug('ERROR: in mathTools: indexx: n is larger than 2**21, increase dimension of iu, il, default is 21')
        call mystop(errS, 'indexx: n is larger than 2**21, increase dimension of iu, il, default is 21')
    end if    
    
    IF (n <= 0) RETURN

    ! INITIALIZE IND, M, I, J, AND R

    DO  i = 1, n
      ind(i) = i
    END DO
    m = 1
    i = 1
    j = n
    r = .375
    

    ! TOP OF LOOP

    20 IF (i >= j) GO TO 70
    IF (r <= .5898437) THEN
      r = r + .0390625
    ELSE
      r = r - .21875
    END IF

    ! INITIALIZE K

    30 k = i

    ! SELECT A CENTRAL ELEMENT OF X AND SAVE IT IN T

    ij = i + r*(j-i)
    it = ind(ij)
    t = x(it)

    ! IF THE FIRST ELEMENT OF THE ARRAY IS GREATER THAN T,
    !   INTERCHANGE IT WITH T

    indx = ind(i)
    IF (x(indx) > t) THEN
      ind(ij) = indx
      ind(i) = it
      it = indx
      t = x(it)
    END IF

    ! INITIALIZE L

    l = j

    ! IF THE LAST ELEMENT OF THE ARRAY IS LESS THAN T,
    !   INTERCHANGE IT WITH T

    indx = ind(j)
    IF (x(indx) >= t) GO TO 50
    ind(ij) = indx
    ind(j) = it
    it = indx
    t = x(it)

    ! IF THE FIRST ELEMENT OF THE ARRAY IS GREATER THAN T,
    !   INTERCHANGE IT WITH T

    indx = ind(i)
    IF (x(indx) <= t) GO TO 50
    ind(ij) = indx
    ind(i) = it
    it = indx
    t = x(it)
    GO TO 50

    ! INTERCHANGE ELEMENTS K AND L

    40 itt = ind(l)
    ind(l) = ind(k)
    ind(k) = itt

    ! FIND AN ELEMENT IN THE UPPER PART OF THE ARRAY WHICH IS
    !   NOT LARGER THAN T

    50 l = l - 1
    indx = ind(l)
    IF (x(indx) > t) GO TO 50

    ! FIND AN ELEMENT IN THE LOWER PART OF THE ARRAY WHCIH IS NOT SMALLER THAN T

    60 k = k + 1
    indx = ind(k)
    IF (x(indx) < t) GO TO 60

    ! IF K <= L, INTERCHANGE ELEMENTS K AND L

    IF (k <= l) GO TO 40

    ! SAVE THE UPPER AND LOWER SUBSCRIPTS OF THE PORTION OF THE
    !   ARRAY YET TO BE SORTED

    IF (l-i > j-k) THEN
      il(m) = i
      iu(m) = l
      i = k
      m = m + 1
      GO TO 80
    END IF

    il(m) = k
    iu(m) = j
    j = l
    m = m + 1
    GO TO 80

    ! BEGIN AGAIN ON ANOTHER UNSORTED PORTION OF THE ARRAY

    70 m = m - 1
    IF (m == 0) RETURN
    i = il(m)
    j = iu(m)

    80 IF (j-i >= 11) GO TO 30
    IF (i == 1) GO TO 20
    i = i - 1

    ! SORT ELEMENTS I+1,...,J.  NOTE THAT 1 <= I < J AND J-I < 11.

    90 i = i + 1
    IF (i == j) GO TO 70
    indx = ind(i+1)
    t = x(indx)
    it = indx
    indx = ind(i)
    IF (x(indx) <= t) GO TO 90
    k = i

    100 ind(k+1) = ind(k)
    k = k - 1
    indx = ind(k)
    IF (t < x(indx)) GO TO 100

    ind(k+1) = it
    GO TO 90
    
    END SUBROUTINE indexx
    
    
    INTEGER FUNCTION locate(axis, x, dim)
      !
      ! Purpose: Find the node (location) of the value X in an array 
      ! 
      ! Author: Roeland van Oss, 2003
      ! Modifications: Olaf Tuinder, 20050117
      ! Modifications: Johan de Haan, 2015 08 24
      !
      ! Input: axis, dim, x
      ! InOut: -
      ! Output (as a function value): findNode
      ! Dependencies: -
      !
      ! Find the nearest point near X
      ! Check upper boundary condition
      ! modified from findNode to replace old locate
      ! added descending order of x   
      ! P. Wang 13 Jan. 2022
     
      implicit none
      integer, intent(in) :: dim
      real(8), dimension(dim), intent(in) :: axis
      real(8), intent(in) :: x
      integer, dimension(1) :: nearestLocation

      ! deal with two exceptions:
      ! (1) x = axis(1)
      ! (2) x = axis(dim)
      
      if ( x == axis(1) ) then
          locate = 1
          return
      end if
          
      if ( x == axis(dim) ) then
          locate = dim 
          return
      end if
      
      ! find the nearest grid point near x
      nearestLocation = minloc (abs (axis-x)) 

      ! Check upper boundary condition 
      if (axis(1) < axis(dim)) then   !for ascending 
          if (axis(nearestLocation(1)) > x ) then
              locate = nearestLocation(1) - 1
          else
              locate = nearestLocation(1)
          end if
      else
      !  for descending   P. Wang
          if (axis(nearestLocation(1)) < x ) then
              locate = nearestLocation(1) - 1
          else
              locate = nearestLocation(1)
          end if
      end if    
  
      return

    END FUNCTION locate   
      
    pure function fleg(x,n)
    !
    ! fleg(1,2,....,nl) contains P0(x), P1(x), etc
    ! where Pk(x) is the Legendre function
    ! 
    ! Recursive used in fortran 
    ! P(n,x) = (2*n-3)/(n-1) * x * P(n-1,x) - (n-2)/(n-1) * P(n-2,x)
    ! P. Wang 14 Jan. 2022
    
    implicit none
    real(8),    intent(in) :: x
    integer,    intent(in) :: n
    real(8), dimension(n)  :: fleg
    integer    :: i
    real(8) :: d,a,b,twox
    fleg(1) = 1.0d0
    if ( n == 1) return
    fleg(2) = x
    
    if (n > 2) then
        fleg(1) = 1.0d0
        do i=3,n
            a = (2.0d0*i - 3.0d0) * x  
            b = (i-2.0d0) 
            fleg(i) = (a * fleg(i-1) - b * fleg(i-2))/(i-1.0d0)
        end do
    end if
    
    end function fleg
    
    
    pure function fpoly(x, m)
    !
    ! returns 1, x, x**2, ..., x**(m-1) in the array fpoly(1:m)
    ! P. Wang 14 Jan. 2022
    implicit none
    real(8),   intent(in) :: x
    integer,   intent(in) :: m
    real(8), dimension(m) :: fpoly
    integer               :: i 
    fpoly(1) = 1.0d0
    do i = 2, m
        fpoly(i) = fpoly(i-1)*x
    enddo
    
    end function fpoly
    
    
    function splintDeriv(errS, x, y, m, u, statusSplint)
    
    ! modified from splint
    ! P. Wang 15 Jan. 2022 
    
      implicit none
      type(errorType),        intent(inout) :: errS
      integer,          intent(out) :: statusSplint
      real(8),          intent(in) :: u, x(:), y(:), m(:) 
      real(8)                      :: splintDeriv  
      real(8)                      :: h  
      integer                      :: n 
  
      integer i  
      real(8) dx
  
      statusSplint = 0
      i = 1
      
      n = size(x)
      if (n < 1) then
      call logDebug('index n less than 1 in splintDeriv')
      call mystop(errS, 'index n less than 1 in splintDeriv()')
      return
      end if
      
      i = max(min(locate(x,u,n),n-1),1)
      
      h = x(i+1)-x(i)
      
      if (h == 0.0d0) then
          statusSplint = -1
          call logDebug('x_i and x_i+1 are the same in splintDeriv')
          call logDebug('can not divide by zere')
          call mystop(errS, 'stopped in mathToolsModule: splintDeriv : array elements are equal')
          if (errorCheck(errS)) return
      end if
      
      splintDeriv = (h/6.0d0 - (x(i+1)-u)**2/(2.0d0*h))*m(i) + &
                ((u-x(i))**2/(2.0d0*h)-h/6.0d0)*m(i+1) + (y(i+1)-y(i))/h

  
    end function splintDeriv    
    
      ! 

    
    subroutine svdfit(errS, x,y,sigma,a,v,w,chisq,fit_funcs)
    !
    !  P. Wang 06. Jan. 2022 
    implicit none
    type(errorType), intent(inout) :: errS
    real(8), dimension(:), intent(in)    :: x,y,sigma
    real(8), dimension(:), intent(out)   :: a,w
    real(8), dimension(:,:), intent(out) :: v
    real(8), intent(out)                 :: chisq
    ! interface for function
    interface
        function fit_funcs(x, m)
        implicit none
        real(8),  intent(in)   :: x
        integer,  intent(in)   :: m
        real(8),  dimension(m) :: fit_funcs
        end function fit_funcs
    end interface
    real(8), parameter :: eps=1.0d-15
    integer :: k, ma, nx
    real(8), dimension(size(x))         :: b  
    real(8), dimension(size(x),size(a)) :: u, u_sav
    
    nx = size(x)
    ma = size(a)
   
    b = y / sigma
    do k = 1, nx
        u(k,:) = fit_funcs(x(k), ma) / sigma(k)
    end do
    u_sav = u
    
    ! calculate SVD of u, output in u, w, v 
    !  

    call svdcmp(errS, u,w,v,size(w)) 
    if (errorCheck(errS)) return
    
    !set small weight w to 0.  w should be positive by definition
    where (w < eps*maxval(w)) w = 0.0d0
    
    ! solve a x = b using u, w, v. a is output. 
     
    call svbksb(errS, u,w,v,b,a)      
    if (errorCheck(errS)) return

    ! calculate chisq
    chisq=sum((matmul(u_sav,a)-b)*(matmul(u_sav,a)-b))
      
    end subroutine svdfit
    
    
    
    pure subroutine svbksb(errS, u,w,v,b,x)
    !pure subroutine eval_svdx(errS, u,w,v,b,x)
    !
    ! solve A x = b for x with given b
    ! A has been decomposed as A = u w vT
    ! using singular value decomposition
    ! x = v w uT b
    ! 
    ! P. Wang 06. Jan. 2022 
    
    implicit none
    type(errorType), intent(inout) :: errS
    real(8), dimension(:,:), intent(in)  :: u, v
    real(8), dimension(:),   intent(in)  :: w, b
    real(8), dimension(:),   intent(out) :: x
    real(8), dimension(size(x)) :: work
      
    work(:) = 0.d0
    work = matmul(b,u)
    
    where (w /= 0.0d0)
        work = work / w    
    elsewhere
        work = 0.0d0
    end where

    x = matmul(v, work)
    
    end subroutine svbksb 
    
    
    SUBROUTINE drotg(da, db, dc, ds)
    
    !     DESIGNED BY C.L.LAWSON, JPL, 1977 SEPT 08

    !     CONSTRUCT THE GIVENS TRANSFORMATION

    !         ( DC  DS )
    !     G = (        ) ,    DC**2 + DS**2 = 1 ,
    !         (-DS  DC )

    !     WHICH ZEROS THE SECOND ENTRY OF THE 2-VECTOR  (DA,DB)**T .

    !     THE QUANTITY R = (+/-)SQRT(DA**2 + DB**2) OVERWRITES DA IN
    !     STORAGE.  THE VALUE OF DB IS OVERWRITTEN BY A VALUE Z WHICH
    !     ALLOWS DC AND DS TO BE RECOVERED BY THE FOLLOWING ALGORITHM:
    !           IF Z=1  SET  DC=0.D0  AND  DS=1.D0
    !           IF DABS(Z) < 1  SET  DC=SQRT(1-Z**2)  AND  DS=Z
    !           IF DABS(Z) > 1  SET  DC=1/Z  AND  DS=SQRT(1-DC**2)

    !     NORMALLY, THE SUBPROGRAM DROT(N,DX,INCX,DY,INCY,DC,DS) WILL
    !     NEXT BE CALLED TO APPLY THE TRANSFORMATION TO A 2 BY N MATRIX.

    ! ------------------------------------------------------------------
    IMPLICIT NONE
    REAL (8), INTENT(INOUT)  :: da
    REAL (8), INTENT(INOUT)  :: db
    REAL (8), INTENT(OUT)     :: dc
    REAL (8), INTENT(OUT)     :: ds

    REAL (8)  :: u, v, r
    IF (ABS(da) <= ABS(db)) GO TO 10

    ! *** HERE ABS(DA) > ABS(DB) ***

    u = da + da
    v = db / u

    !     NOTE THAT U AND R HAVE THE SIGN OF DA

    r = SQRT(.25D0 + v**2) * u

    !     NOTE THAT DC IS POSITIVE

    dc = da / r
    ds = v * (dc + dc)
    db = ds
    da = r
    RETURN

    ! *** HERE ABS(DA) <= ABS(DB) ***

    10 IF (db == 0.0d0) GO TO 20
    u = db + db
    v = da / u

    !     NOTE THAT U AND R HAVE THE SIGN OF DB
    !     (R IS IMMEDIATELY STORED IN DA)

    da = SQRT(.25D0 + v**2) * u

    !     NOTE THAT DS IS POSITIVE

    ds = db / da
    dc = v * (ds + ds)
    IF (dc == 0.0d0) GO TO 15
    db = 1.0d0 / dc
    RETURN
    15 db = 1.0d0
    RETURN

    ! *** HERE DA = DB = 0.D0 ***

    20 dc = 1.0d0
    ds = 0.0d0
    RETURN

    END SUBROUTINE drotg


    SUBROUTINE dswap1 (n, dx, dy)

    !     INTERCHANGES TWO VECTORS.
    !     USES UNROLLED LOOPS FOR INCREMENTS EQUAL ONE.
    !     JACK DONGARRA, LINPACK, 3/11/78.
    !     This version is for increments = 1.
    IMPLICIT NONE
    INTEGER, INTENT(IN)        :: n
    REAL (8), INTENT(INOUT)  :: dx(*)
    REAL (8), INTENT(INOUT)  :: dy(*)

    REAL (8)  :: dtemp
    INTEGER    :: i, m, mp1

    IF(n <= 0) RETURN

    !       CODE FOR BOTH INCREMENTS EQUAL TO 1

    !       CLEAN-UP LOOP

    m = MOD(n,3)
    IF( m == 0 ) GO TO 40
    DO  i = 1,m
      dtemp = dx(i)
      dx(i) = dy(i)
      dy(i) = dtemp
    END DO
    IF( n < 3 ) RETURN
    40 mp1 = m + 1
    DO  i = mp1,n,3
      dtemp = dx(i)
      dx(i) = dy(i)
      dy(i) = dtemp
      dtemp = dx(i + 1)
      dx(i + 1) = dy(i + 1)
      dy(i + 1) = dtemp
      dtemp = dx(i + 2)
      dx(i + 2) = dy(i + 2)
      dy(i + 2) = dtemp
    END DO
    RETURN
    END SUBROUTINE  dswap1


    SUBROUTINE  drot1 (n, dx, dy, c, s)

    !     APPLIES A PLANE ROTATION.
    !     JACK DONGARRA, LINPACK, 3/11/78.
    !     This version is for increments = 1.
    IMPLICIT NONE
    INTEGER, INTENT(IN)        :: n
    REAL (8), INTENT(INOUT)  :: dx(*)
    REAL (8), INTENT(INOUT)  :: dy(*)
    REAL (8), INTENT(IN)      :: c
    REAL (8), INTENT(IN)      :: s

    REAL (8)  :: dtemp
    INTEGER    :: i

    IF(n <= 0) RETURN
    !       CODE FOR BOTH INCREMENTS EQUAL TO 1

    DO  i = 1,n
      dtemp = c*dx(i) + s*dy(i)
      dy(i) = c*dy(i) - s*dx(i)
      dx(i) = dtemp
    END DO
    RETURN
    END SUBROUTINE  drot1


    !SUBROUTINE dsvdc(x, n, p, s, e, u, v, job, info)
    !INTEGER, INTENT(IN)        :: n
    !INTEGER, INTENT(IN)        :: p
    !REAL (dp), INTENT(IN OUT)  :: x(:,:)
    !REAL (dp), INTENT(OUT)     :: s(:)
    !REAL (dp), INTENT(OUT)     :: e(:)
    !REAL (dp), INTENT(OUT)     :: u(:,:)
    !REAL (dp), INTENT(OUT)     :: v(:,:)
    !INTEGER, INTENT(IN)        :: job
    !INTEGER, INTENT(OUT)       :: info

    !     DSVDC IS A SUBROUTINE TO REDUCE A DOUBLE PRECISION NXP MATRIX X
    !     BY ORTHOGONAL TRANSFORMATIONS U AND V TO DIAGONAL FORM.  THE
    !     DIAGONAL ELEMENTS S(I) ARE THE SINGULAR VALUES OF X.  THE
    !     COLUMNS OF U ARE THE CORRESPONDING LEFT SINGULAR VECTORS,
    !     AND THE COLUMNS OF V THE RIGHT SINGULAR VECTORS.

    !     ON ENTRY

    !         X         DOUBLE PRECISION(LDX,P), WHERE LDX.GE.N.
    !                   X CONTAINS THE MATRIX WHOSE SINGULAR VALUE
    !                   DECOMPOSITION IS TO BE COMPUTED.  X IS
    !                   DESTROYED BY DSVDC.

    !         LDX       INTEGER.
    !                   LDX IS THE LEADING DIMENSION OF THE ARRAY X.

    !         N         INTEGER.
    !                   N IS THE NUMBER OF ROWS OF THE MATRIX X.

    !         P         INTEGER.
    !                   P IS THE NUMBER OF COLUMNS OF THE MATRIX X.

    !         LDU       INTEGER.
    !                   LDU IS THE LEADING DIMENSION OF THE ARRAY U.
    !                   (SEE BELOW).

    !         LDV       INTEGER.
    !                   LDV IS THE LEADING DIMENSION OF THE ARRAY V.
    !                   (SEE BELOW).

    !         JOB       INTEGER.
    !                   JOB CONTROLS THE COMPUTATION OF THE SINGULAR
    !                   VECTORS.  IT HAS THE DECIMAL EXPANSION AB
    !                   WITH THE FOLLOWING MEANING

    !                        A.EQ.0    DO NOT COMPUTE THE LEFT SINGULAR VECTORS.
    !                        A.EQ.1    RETURN THE N LEFT SINGULAR VECTORS IN U.
    !                        A.GE.2    RETURN THE FIRST MIN(N,P) SINGULAR
    !                                  VECTORS IN U.
    !                        B.EQ.0    DO NOT COMPUTE THE RIGHT SINGULAR VECTORS.
    !                        B.EQ.1    RETURN THE RIGHT SINGULAR VECTORS IN V.

    !     ON RETURN

    !         S         DOUBLE PRECISION(MM), WHERE MM=MIN(N+1,P).
    !                   THE FIRST MIN(N,P) ENTRIES OF S CONTAIN THE SINGULAR
    !                   VALUES OF X ARRANGED IN DESCENDING ORDER OF MAGNITUDE.

    !         E         DOUBLE PRECISION(P).
    !                   E ORDINARILY CONTAINS ZEROS.  HOWEVER SEE THE
    !                   DISCUSSION OF INFO FOR EXCEPTIONS.

    !         U         DOUBLE PRECISION(LDU,K), WHERE LDU.GE.N.  IF
    !                                   JOBA.EQ.1 THEN K.EQ.N, IF JOBA.GE.2
    !                                   THEN K.EQ.MIN(N,P).
    !                   U CONTAINS THE MATRIX OF LEFT SINGULAR VECTORS.
    !                   U IS NOT REFERENCED IF JOBA.EQ.0.  IF N.LE.P
    !                   OR IF JOBA.EQ.2, THEN U MAY BE IDENTIFIED WITH X
    !                   IN THE SUBROUTINE CALL.

    !         V         DOUBLE PRECISION(LDV,P), WHERE LDV.GE.P.
    !                   V CONTAINS THE MATRIX OF RIGHT SINGULAR VECTORS.
    !                   V IS NOT REFERENCED IF JOB.EQ.0.  IF P.LE.N,
    !                   THEN V MAY BE IDENTIFIED WITH X IN THE
    !                   SUBROUTINE CALL.

    !         INFO      INTEGER.
    !                   THE SINGULAR VALUES (AND THEIR CORRESPONDING SINGULAR
    !                   VECTORS) S(INFO+1),S(INFO+2),...,S(M) ARE CORRECT
    !                   (HERE M=MIN(N,P)).  THUS IF INFO.EQ.0, ALL THE
    !                   SINGULAR VALUES AND THEIR VECTORS ARE CORRECT.
    !                   IN ANY EVENT, THE MATRIX B = TRANS(U)*X*V IS THE
    !                   BIDIAGONAL MATRIX WITH THE ELEMENTS OF S ON ITS DIAGONAL
    !                   AND THE ELEMENTS OF E ON ITS SUPER-DIAGONAL (TRANS(U)
    !                   IS THE TRANSPOSE OF U).  THUS THE SINGULAR VALUES
    !                   OF X AND B ARE THE SAME.

    !     LINPACK. THIS VERSION DATED 03/19/79 .
    !     G.W. STEWART, UNIVERSITY OF MARYLAND, ARGONNE NATIONAL LAB.

    !     DSVDC USES THE FOLLOWING FUNCTIONS AND SUBPROGRAMS.

    !     EXTERNAL DROT
    !     BLAS DAXPY,DDOT,DSCAL,DSWAP,DNRM2,DROTG
    !     FORTRAN DABS,DMAX1,MAX0,MIN0,MOD,DSQRT

    !     INTERNAL VARIABLES
    !     
    !     Original code was downloaded from https://jblevins.org/mirror/amiller/dsvdc.f90
    !     Adapted to Disamar, added error message, changed subroutine name
    !     u has size of (max(n,p), max(n,p)), s has demension of (p+1)
    !     x dimension (n,p), v dimension(p,p)
    !     P. Wang 16 Jan. 2022
    !     Changed maxit from 30 to 100 because of error message
    !     P. Wang 23 Jan. 2022
    

    SUBROUTINE dsvdc(errS, x, s, v, nn)

    IMPLICIT NONE
    INTEGER, INTENT(IN)        :: nn
    REAL (8), INTENT(INOUT)  :: x(:,:)
    REAL (8), INTENT(OUT)     :: s(:)
    REAL (8), INTENT(OUT)     :: v(:,:)
    type(errorType), intent(inout) :: errS


    INTEGER                    :: n, p
    INTEGER                    :: job
    INTEGER                    :: info
    !REAL (8), DIMENSION(max(size(x,1), size(x,2)),max(size(x,1),size(x,2)) ) :: u
    REAL (8), ALLOCATABLE, DIMENSION(:,:) :: u
    REAL (8), DIMENSION(size(x,2))   :: e

    INTEGER :: iter, j, jobu, k, kase, kk, l, ll, lls, lm1, lp1, ls, lu, m, maxit,  &
        mm, mm1, mp1, nct, nctp1, ncu, nrt, nrtp1
    REAL (8) :: t
    REAL (8), DIMENSION(size(x,1)) :: work
    REAL (8) :: b, c, cs, el, emm1, f, g, scale, shift, sl, sm, sn,  &
        smm1, t1, test, ztest
    LOGICAL :: wantu, wantv
    ! SET input parameters 
    n = size(x,1)              ! number of row
    p = size(x,2)              ! number of column  
    job = 11                   ! calculate  u, v
    
    allocate( u( max(n, p),max(n,p) ) )
    u(:,:) = 0.0d0
      
    !     SET THE MAXIMUM NUMBER OF ITERATIONS.

    maxit = 100 !  original value 30 

    !     DETERMINE WHAT IS TO BE COMPUTED.


    wantu = .false.
    wantv = .false.
    jobu = MOD(job,100)/10
    ncu = n
    IF (jobu > 1) ncu = MIN(n,p)
    IF (jobu /= 0) wantu = .true.
    IF (MOD(job,10) /= 0) wantv = .true.

    !     REDUCE X TO BIDIAGONAL FORM, STORING THE DIAGONAL ELEMENTS
    !     IN S AND THE SUPER-DIAGONAL ELEMENTS IN E.

    info = 0
    nct = MIN(n-1, p)
    s(1:nct+1) = 0.0d0
    nrt = MAX(0, MIN(p-2,n))
    lu = MAX(nct,nrt)
    IF (lu < 1) GO TO 170
    DO  l = 1, lu
      lp1 = l + 1
      IF (l > nct) GO TO 20
      
    !           COMPUTE THE TRANSFORMATION FOR THE L-TH COLUMN AND
    !           PLACE THE L-TH DIAGONAL IN S(L).
      
      s(l) = SQRT( SUM( x(l:n,l)**2 ) )
      IF (s(l) == 0.0D0) GO TO 10
      IF (x(l,l) /= 0.0D0) s(l) = SIGN(s(l), x(l,l))
      x(l:n,l) = x(l:n,l) / s(l)
      x(l,l) = 1.0D0 + x(l,l)

      10 s(l) = -s(l)

      20 IF (p < lp1) GO TO 50
      DO  j = lp1, p
        IF (l > nct) GO TO 30
        IF (s(l) == 0.0D0) GO TO 30
        
    !              APPLY THE TRANSFORMATION.
        
        t = -DOT_PRODUCT(x(l:n,l), x(l:n,j)) / x(l,l)
        x(l:n,j) = x(l:n,j) + t * x(l:n,l)
        
    !           PLACE THE L-TH ROW OF X INTO  E FOR THE
    !           SUBSEQUENT CALCULATION OF THE ROW TRANSFORMATION.
        
        30 e(j) = x(l,j)
      END DO

      50 IF (.NOT.wantu .OR. l > nct) GO TO 70
      
    !           PLACE THE TRANSFORMATION IN U FOR SUBSEQUENT BACK MULTIPLICATION.
      
      u(l:n,l) = x(l:n,l)

      70 IF (l > nrt) CYCLE
      
    !           COMPUTE THE L-TH ROW TRANSFORMATION AND PLACE THE
    !           L-TH SUPER-DIAGONAL IN E(L).
      
      e(l) = SQRT( SUM( e(lp1:p)**2 ) )
      IF (e(l) == 0.0D0) GO TO 80
      IF (e(lp1) /= 0.0D0) e(l) = SIGN(e(l), e(lp1))
      e(lp1:lp1+p-l-1) = e(lp1:p) / e(l)
      e(lp1) = 1.0D0 + e(lp1)

      80 e(l) = -e(l)
      IF (lp1 > n .OR. e(l) == 0.0D0) GO TO 120
      
    !              APPLY THE TRANSFORMATION.
      
      work(lp1:n) = 0.0D0
      DO  j = lp1, p
        work(lp1:lp1+n-l-1) = work(lp1:lp1+n-l-1) + e(j) * x(lp1:lp1+n-l-1,j)
      END DO
      DO  j = lp1, p
        x(lp1:lp1+n-l-1,j) = x(lp1:lp1+n-l-1,j) - (e(j)/e(lp1)) * work(lp1:lp1+n-l-1)
      END DO

      120 IF (.NOT.wantv) CYCLE
      
    !              PLACE THE TRANSFORMATION IN V FOR SUBSEQUENT
    !              BACK MULTIPLICATION.
      
      v(lp1:p,l) = e(lp1:p)
    END DO

    !     SET UP THE FINAL BIDIAGONAL MATRIX OF ORDER M.

    170 m = MIN(p,n+1)
    nctp1 = nct + 1
    nrtp1 = nrt + 1
    IF (nct < p) s(nctp1) = x(nctp1,nctp1)
    IF (n < m) s(m) = 0.0D0
    IF (nrtp1 < m) e(nrtp1) = x(nrtp1,m)
    e(m) = 0.0D0

    !     IF REQUIRED, GENERATE U.

    IF (.NOT.wantu) GO TO 300
    IF (ncu < nctp1) GO TO 200
    DO  j = nctp1, ncu
      u(1:n,j) = 0.0d0
      u(j,j) = 1.0d0
    END DO

    200 DO  ll = 1, nct
      l = nct - ll + 1
      IF (s(l) == 0.0D0) GO TO 250
      lp1 = l + 1
      IF (ncu < lp1) GO TO 220
      DO  j = lp1, ncu
        t = -DOT_PRODUCT(u(l:n,l), u(l:n,j)) / u(l,l)
        u(l:n,j) = u(l:n,j) + t * u(l:n,l)
      END DO

      220 u(l:n,l) = -u(l:n,l)
      u(l,l) = 1.0D0 + u(l,l)
      lm1 = l - 1
      IF (lm1 < 1) CYCLE
      u(1:lm1,l) = 0.0d0
      CYCLE

      250 u(1:n,l) = 0.0d0
      u(l,l) = 1.0d0
    END DO

    !     IF IT IS REQUIRED, GENERATE V.

    300 IF (.NOT.wantv) GO TO 350
    DO  ll = 1, p
      l = p - ll + 1
      lp1 = l + 1
      IF (l > nrt) GO TO 320
      IF (e(l) == 0.0D0) GO TO 320
      DO  j = lp1, p
        t = -DOT_PRODUCT(v(lp1:lp1+p-l-1,l), v(lp1:lp1+p-l-1,j)) / v(lp1,l)
        v(lp1:lp1+p-l-1,j) = v(lp1:lp1+p-l-1,j) + t * v(lp1:lp1+p-l-1,l)
      END DO

      320 v(1:p,l) = 0.0D0
      v(l,l) = 1.0D0
    END DO

    !     MAIN ITERATION LOOP FOR THE SINGULAR VALUES.

    350 mm = m
    iter = 0

    !        QUIT IF ALL THE SINGULAR VALUES HAVE BEEN FOUND.

    !     ...EXIT  program, m decrease after iteration
    360 IF (m == 0) GO TO 620
    !        IF TOO MANY ITERATIONS HAVE BEEN PERFORMED, SET FLAG AND RETURN.


    IF (iter < maxit) GO TO 370
    info = m
   
    !     ......EXIT
    GO TO 620

    !        THIS SECTION OF THE PROGRAM INSPECTS FOR NEGLIGIBLE ELEMENTS
    !        IN THE S AND E ARRAYS.  ON COMPLETION
    !        THE VARIABLES KASE AND L ARE SET AS FOLLOWS.

    !           KASE = 1     IF S(M) AND E(L-1) ARE NEGLIGIBLE AND L < M
    !           KASE = 2     IF S(L) IS NEGLIGIBLE AND L < M
    !           KASE = 3     IF E(L-1) IS NEGLIGIBLE, L < M, AND
    !                        S(L), ..., S(M) ARE NOT NEGLIGIBLE (QR STEP).
    !           KASE = 4     IF E(M-1) IS NEGLIGIBLE (CONVERGENCE).

    370 DO  ll = 1, m
      l = m - ll
    !        ...EXIT
      IF (l == 0) EXIT

      test = ABS(s(l)) + ABS(s(l+1))
      ztest = test + ABS(e(l))
      IF (ztest /= test) CYCLE
      e(l) = 0.0D0
    !        ......EXIT

      EXIT
    END DO

    IF (l /= m - 1) GO TO 410
    kase = 4
    GO TO 480

    410 lp1 = l + 1
    mp1 = m + 1
    DO  lls = lp1, mp1
      ls = m - lls + lp1
    !           ...EXIT
      IF (ls == l) EXIT

      test = 0.0D0
      IF (ls /= m) test = test + ABS(e(ls))
      IF (ls /= l + 1) test = test + ABS(e(ls-1))
      ztest = test + ABS(s(ls))
      IF (ztest /= test) CYCLE
      s(ls) = 0.0D0
    !           ......EXIT

      EXIT
    END DO

    IF (ls /= l) GO TO 450
    kase = 3
    GO TO 480

    450 IF (ls /= m) GO TO 460
    kase = 1
    GO TO 480

    460 kase = 2
    l = ls
    480 l = l + 1

    !        PERFORM THE TASK INDICATED BY KASE.

    SELECT CASE ( kase )
      CASE (    1)
        GO TO 490
      CASE (    2)
        GO TO 520
      CASE (    3)
        GO TO 540
      CASE (    4)
        GO TO 570
    END SELECT

    !        DEFLATE NEGLIGIBLE S(M).

    490 mm1 = m - 1
    f = e(m-1)
    e(m-1) = 0.0D0
    DO  kk = l, mm1
      k = mm1 - kk + l
      t1 = s(k)
      CALL drotg(t1, f, cs, sn)
      s(k) = t1
      IF (k == l) GO TO 500
      f = -sn*e(k-1)
      e(k-1) = cs*e(k-1)

      500 IF (wantv) CALL drot1(p, v(1:,k), v(1:,m), cs, sn)
    END DO
    GO TO 610

    !        SPLIT AT NEGLIGIBLE S(L).

    520 f = e(l-1)
    e(l-1) = 0.0D0
    DO  k = l, m
      t1 = s(k)
      CALL drotg(t1, f, cs, sn)
      s(k) = t1
      f = -sn*e(k)
      e(k) = cs*e(k)
      IF (wantu) CALL drot1(n, u(1:,k), u(1:,l-1), cs, sn)
    END DO
    GO TO 610

    !        PERFORM ONE QR STEP.

    !           CALCULATE THE SHIFT.

    540 scale = MAX(ABS(s(m)), ABS(s(m-1)), ABS(e(m-1)), ABS(s(l)), ABS(e(l)))
    sm = s(m)/scale
    smm1 = s(m-1)/scale
    emm1 = e(m-1)/scale
    sl = s(l)/scale
    el = e(l)/scale
    b = ((smm1 + sm)*(smm1 - sm) + emm1**2)/2.0D0
    c = (sm*emm1)**2
    shift = 0.0D0
    IF (b == 0.0D0 .AND. c == 0.0D0) GO TO 550
    shift = SQRT(b**2+c)
    IF (b < 0.0D0) shift = -shift
    shift = c/(b + shift)

    550 f = (sl + sm)*(sl - sm) - shift
    g = sl*el

    !           CHASE ZEROS.

    mm1 = m - 1
    DO  k = l, mm1
      CALL drotg(f, g, cs, sn)
      IF (k /= l) e(k-1) = f
      f = cs*s(k) + sn*e(k)
      e(k) = cs*e(k) - sn*s(k)
      g = sn*s(k+1)
      s(k+1) = cs*s(k+1)
      IF (wantv) CALL drot1(p, v(1:,k), v(1:,k+1), cs, sn)
      CALL drotg(f, g, cs, sn)
      s(k) = f
      f = cs*e(k) + sn*s(k+1)
      s(k+1) = -sn*e(k) + cs*s(k+1)
      g = sn*e(k+1)
      e(k+1) = cs*e(k+1)
      IF (wantu .AND. k < n) CALL drot1(n, u(1:,k), u(1:,k+1), cs, sn)
    END DO
    e(m-1) = f
    iter = iter + 1
    GO TO 610

    !        CONVERGENCE.

    !           MAKE THE SINGULAR VALUE  POSITIVE.

    570 IF (s(l) >= 0.0D0) GO TO 590
    s(l) = -s(l)
    IF (wantv) v(1:p,l) = -v(1:p,l)

    !           ORDER THE SINGULAR VALUE.

    590 IF (l == mm) GO TO 600
    !           ...EXIT
    IF (s(l) >= s(l+1)) GO TO 600
    t = s(l)
    s(l) = s(l+1)
    s(l+1) = t
    IF (wantv .AND. l < p) CALL dswap1(p, v(1:,l), v(1:,l+1))
    IF (wantu .AND. l < n) CALL dswap1(n, u(1:,l), u(1:,l+1))
    l = l + 1
    GO TO 590

    600 iter = 0
    m = m - 1
    610 GO TO 360

    620 IF (m == 0) then
            x(1:n,1:p) = u(1:n,1:p)
            deallocate(u) 
            !write(*,*) 'info, m  =', info, m
            if (info /=0) then
                call logDebug('info /=0 in dsvdc() replacement of svdcmp()')
                call mystop(errS, 'info /=0 in dsvdc() replacement of svdcmp()')
            end if   
            RETURN
        else
            write(*,*) 'stop ! ERROR in dsvdc info, m  =', info, m
            
            if (info /=0) then
                call logDebug('info /=0 in dsvdc() replacement of svdcmp(), try to increase maxit')
                call mystop(errS, 'info /=0 in dsvdc() replacement of svdcmp(), try to increase maxit')
            end if   
        
        endif    

    END SUBROUTINE dsvdc


    subroutine svdcmp(errS, x, s, v, nn)
    ! Call dsvdc because dimension s has to add 1 in dsvdc.
    ! Otherwise all call of dsvdc have to be changed in Disamar.
    ! P. Wang  16 Jan. 2022

    implicit none
    integer,  intent(in)      :: nn              ! nn is not used in dsvdc, to it is in svdcmp from NR
    real (8), intent(inout)   :: x(:,:)
    real (8), INTENT(OUT)     :: s(:)
    real (8), INTENT(OUT)     :: v(:,:)
    type(errorType), intent(inout) :: errS
    
    real (8), dimension(size(s)+1) :: s1
    
    s1(:) = 0.0d0
    call dsvdc(errS, x, s1, v, nn)
    if (errorCheck(errS)) then
        call logDebug('errS /=0 in svdcmp')
        call mystop(errS, 'errS /=0 in svdcmp')
    end if 
    
    
    s(:) = s1(1:size(s))
    
    return
    end subroutine  svdcmp    
    
    subroutine spline(errS, x, y, y2, status)
    
    ! this subroutine is a wrapper of cubspl.
    ! It sets derivatives at the first and last points of x
    ! The output is the second derivatives.
    ! P. Wang
    ! 24 Feb. 2022
    
    implicit none

    type(errorType),       intent(inout) :: errS
    real (8), dimension(:), intent(in)   :: x, y
    integer,               intent(out)   :: status
    real (8), dimension(:), intent(inout)  :: y2

   ! variables for cubspl
    real (8), dimension(4, size(x))      :: c  
    integer :: n
    integer   :: ibcbeg
    integer   :: ibcend
   
    status = 0
    n = size(x)
    ibcbeg = 1
    ibcend = 1
    c(1,1:n) = y(1:n)
    c(2,1) = (y(2)-y(1))/(x(2)-x(1))
    c(2,n) = (y(n)-y(n-1))/(x(n)-x(n-1))
      
    call cubspl (x, c, n, ibcbeg, ibcend )
    
    ! The second drivatives are in c(3, 2:n-1). 
    ! The first and last second derivatives are calculated as follow 
    y2(2:n-1) = c(3,2:n-1)
    y2(1) = -0.5*c(3,2)
    y2(n) = -0.5*c(3,n-1)
    
    return
    
    end subroutine spline
    
    subroutine cubspl ( tau, c, n, ibcbeg, ibcend )

    !*****************************************************************************80
    !
    !! CUBSPL defines an interpolatory cubic spline.
    !
    !  Discussion:
    !
    !    A tridiagonal linear system for the unknown slopes S(I) of
    !    F at TAU(I), I=1,..., N, is generated and then solved by Gauss
    !    elimination, with S(I) ending up in C(2,I), for all I.
    !
    !  Modified:
    !
    !    14 February 2007
    !
    !  Author:
    !
    !    Original FORTRAN77 version by Carl de Boor.
    !    FORTRAN90 version by John Burkardt.
    !
    !  Reference:
    !
    !    Carl de Boor,
    !    A Practical Guide to Splines,
    !    Springer, 2001,
    !    ISBN: 0387953663,
    !    LC: QA1.A647.v27.
    !
    !  Parameters:
    !
    !    Input, real ( kind = 8 ) TAU(N), the abscissas or X values of
    !    the data points.  The entries of TAU are assumed to be
    !    strictly increasing.
    !
    !    Input, integer ( kind = 4 ) N, the number of data points.  N is
    !    assumed to be at least 2.
    !
    !    Input/output, real ( kind = 8 ) C(4,N).
    !    On input, if IBCBEG or IBCBEG is 1 or 2, then C(2,1)
    !    or C(2,N) should have been set to the desired derivative
    !    values, as described further under IBCBEG and IBCEND.
    !    On output, C contains the polynomial coefficients of
    !    the cubic interpolating spline with interior knots
    !    TAU(2) through TAU(N-1).
    !    In the interval interval (TAU(I), TAU(I+1)), the spline
    !    F is given by
    !      F(X) = 
    !        C(1,I) + 
    !        C(2,I) * H +
    !        C(3,I) * H^2 / 2 + 
    !        C(4,I) * H^3 / 6.
    !    where H=X-TAU(I).  The routine PPVALU may be used to
    !    evaluate F or its derivatives from TAU, C, L=N-1,
    !    and K=4.
    !
    !    Input, integer ( kind = 4 ) IBCBEG, IBCEND, boundary condition indicators.
    !    IBCBEG = 0 means no boundary condition at TAU(1) is given.
    !    In this case, the "not-a-knot condition" is used.  That
    !    is, the jump in the third derivative across TAU(2) is
    !    forced to zero.  Thus the first and the second cubic
    !    polynomial pieces are made to coincide.
    !    IBCBEG = 1 means the slope at TAU(1) is to equal the
    !    input value C(2,1).
    !    IBCBEG = 2 means the second derivative at TAU(1) is
    !    to equal C(2,1).
    !    IBCEND = 0, 1, or 2 has analogous meaning concerning the
    !    boundary condition at TAU(N), with the additional
    !    information taken from C(2,N).
    !
      implicit none

      integer ( kind = 4 ) n

      real ( kind = 8 ) c(4,n)
      real ( kind = 8 ) divdf1
      real ( kind = 8 ) divdf3
      real ( kind = 8 ) dtau
      real ( kind = 8 ) g
      integer ( kind = 4 ) i
      integer ( kind = 4 ) ibcbeg
      integer ( kind = 4 ) ibcend
      real ( kind = 8 ) tau(n)
    !
    !  C(3,*) and C(4,*) are used initially for temporary storage.
    !
    !  Store first differences of the TAU sequence in C(3,*).
    !
    !  Store first divided difference of data in C(4,*).
    !
      do i = 2, n
        c(3,i) = tau(i) - tau(i-1)
      end do

      do i = 2, n 
        c(4,i) = ( c(1,i) - c(1,i-1) ) / ( tau(i) - tau(i-1) )
      end do
    !
    !  Construct the first equation from the boundary condition
    !  at the left endpoint, of the form:
    !
    !    C(4,1) * S(1) + C(3,1) * S(2) = C(2,1)
    !
    !  IBCBEG = 0: Not-a-knot
    !
      if ( ibcbeg == 0 ) then

        if ( n <= 2 ) then
          c(4,1) = 1.0D+00
          c(3,1) = 1.0D+00
          c(2,1) = 2.0D+00 * c(4,2)
          go to 120
        end if

        c(4,1) = c(3,3)
        c(3,1) = c(3,2) + c(3,3)
        c(2,1) = ( ( c(3,2) + 2.0D+00 * c(3,1) ) * c(4,2) * c(3,3) &
          + c(3,2)**2 * c(4,3) ) / c(3,1)
    !
    !  IBCBEG = 1: derivative specified.
    !
      else if ( ibcbeg == 1 ) then

        c(4,1) = 1.0D+00
        c(3,1) = 0.0D+00

        if ( n == 2 ) then
          go to 120
        end if
    !
    !  Second derivative prescribed at left end.
    !
      else

        c(4,1) = 2.0D+00
        c(3,1) = 1.0D+00
        c(2,1) = 3.0D+00 * c(4,2) - c(3,2) / 2.0D+00 * c(2,1)

        if ( n == 2 ) then
          go to 120
        end if

      end if
    !
    !  If there are interior knots, generate the corresponding
    !  equations and carry out the forward pass of Gauss elimination,
    !  after which the I-th equation reads:
    !
    !    C(4,I) * S(I) + C(3,I) * S(I+1) = C(2,I).
    !
      do i = 2, n-1
        g = -c(3,i+1) / c(4,i-1)
        c(2,i) = g * c(2,i-1) + 3.0D+00 * ( c(3,i) * c(4,i+1) + c(3,i+1) * c(4,i) )
        c(4,i) = g * c(3,i-1) + 2.0D+00 * ( c(3,i) + c(3,i+1))
      end do
    !
    !  Construct the last equation from the second boundary condition, of
    !  the form
    !
    !    -G * C(4,N-1) * S(N-1) + C(4,N) * S(N) = C(2,N)
    !
    !  If slope is prescribed at right end, one can go directly to
    !  back-substitution, since the C array happens to be set up just
    !  right for it at this point.
    !
      if ( ibcend == 1 ) then
        go to 160
      end if

      if ( 1 < ibcend ) then
        go to 110
      end if
     
    90    continue
    !
    !  Not-a-knot and 3 <= N, and either 3 < N or also not-a-knot
    !  at left end point.
    !
      if ( n /= 3 .or. ibcbeg /= 0 ) then
        g = c(3,n-1) + c(3,n)
        c(2,n) = ( ( c(3,n) + 2.0D+00 * g ) * c(4,n) * c(3,n-1) + c(3,n)**2 &
          * ( c(1,n-1) - c(1,n-2) ) / c(3,n-1) ) / g
        g = - g / c(4,n-1)
        c(4,n) = c(3,n-1)
        c(4,n) = c(4,n) + g * c(3,n-1)
        c(2,n) = ( g * c(2,n-1) + c(2,n) ) / c(4,n)
        go to 160
      end if
    !
    !  N = 3 and not-a-knot also at left.
    !
    100   continue
     
      c(2,n) = 2.0D+00 * c(4,n)
      c(4,n) = 1.0D+00
      g = -1.0D+00 / c(4,n-1)
      c(4,n) = c(4,n) - c(3,n-1) / c(4,n-1)
      c(2,n) = ( g * c(2,n-1) + c(2,n) ) / c(4,n)
      go to 160
    !
    !  IBCEND = 2: Second derivative prescribed at right endpoint.
    !
    110   continue
     
      c(2,n) = 3.0D+00 * c(4,n) + c(3,n) / 2.0D+00 * c(2,n)
      c(4,n) = 2.0D+00
      g = -1.0D+00 / c(4,n-1)
      c(4,n) = c(4,n) - c(3,n-1) / c(4,n-1)
      c(2,n) = ( g * c(2,n-1) + c(2,n) ) / c(4,n)
      go to 160
    !
    !  N = 2.
    !
    120   continue
      
      if ( ibcend == 2  ) then

        c(2,n) = 3.0D+00 * c(4,n) + c(3,n) / 2.0D+00 * c(2,n)
        c(4,n) = 2.0D+00
        g = -1.0D+00 / c(4,n-1)
        c(4,n) = c(4,n) - c(3,n-1) / c(4,n-1)
        c(2,n) = ( g * c(2,n-1) + c(2,n) ) / c(4,n)
     
      else if ( ibcend == 0 .and. ibcbeg /= 0 ) then

        c(2,n) = 2.0D+00 * c(4,n)
        c(4,n) = 1.0D+00
        g = -1.0D+00 / c(4,n-1)
        c(4,n) = c(4,n) - c(3,n-1) / c(4,n-1)
        c(2,n) = ( g * c(2,n-1) + c(2,n) ) / c(4,n)

      else if ( ibcend == 0 .and. ibcbeg == 0 ) then

        c(2,n) = c(4,n)

      end if
    !
    !  Back solve the upper triangular system 
    !
    !    C(4,I) * S(I) + C(3,I) * S(I+1) = B(I)
    !
    !  for the slopes C(2,I), given that S(N) is already known.
    !
    160   continue
     
      do i = n-1, 1, -1
        c(2,i) = ( c(2,i) - c(3,i) * c(2,i+1) ) / c(4,i)
      end do
    !
    !  Generate cubic coefficients in each interval, that is, the
    !  derivatives at its left endpoint, from value and slope at its
    !  endpoints.
    !
      do i = 2, n
        dtau = c(3,i)
        divdf1 = ( c(1,i) - c(1,i-1) ) / dtau
        divdf3 = c(2,i-1) + c(2,i) - 2.0D+00 * divdf1
        c(3,i-1) = 2.0D+00 * ( divdf1 - c(2,i-1) - divdf3 ) / dtau
        c(4,i-1) = 6.0D+00 * divdf3 / dtau**2
      end do
     
      return
    end subroutine cubspl

end module mathTools


