!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

  module createLUTModule

    ! This module contains subroutines to make LUTs for
    !  - reflectance
    !  - polarization correction
    !  - correction for polarization and RRS

  use dataStructures
  use inputModule
  use mathTools,    only: spline, splint, splintLin, gaussDivPoints, getSmoothAndDiffXsec, polyInt, &
                          GaussDivPoints
  use ramansspecs,  only: ConvoluteSpecRaman

  ! default is private
  private 
  public  :: createReflectanceLUT, createPolcorrectionLUT, createPolRRScorrectionLUT

  contains

!    subroutine createReflectanceLUT(errS, ......)
    subroutine createReflectanceLUT(errS)

      implicit none

      type(errorType), intent(inout) :: errS
    ! create reflectance LUT
    ! containing values when the Chandrasekhar flag is false
    !     Fourier coefficients for the reflectance, R(mu,mu0, A)
    !     where A is the surface albedo (Lambertian surface)
    !     Note that the Chandrasekhar expression holds for
    !     strictly monochromatic light and is an approximation
    !     when RRS is involved or when the slit function is wide
    ! containing values when the Chandrasekhar flag is true
    !     Fourier coefficients for the reflectance, R(mu,mu0)
    !     transmittance T = [exp(-tau/mu) + td(mu)] x [exp(-tau/mu0) + td(mu0)]
    !     spherical albedo for illumination from below, s*
    !     all these values are for a black surface below the atmosphere
    !     
    !     nodes are given for the geometry (mu,mu0), the Fourier index,
    !     the latitude (based on TOMS climatology), the total ozone column,
    !     the surface albedo, and the wavelengths.



    end subroutine createReflectanceLUT


!    subroutine createPolcorrectionLUT(errS, ......)
    subroutine createPolcorrectionLUT(errS)

      implicit none

      type(errorType), intent(inout) :: errS


    end subroutine createPolcorrectionLUT


!    subroutine createPolRRScorrectionLUT(errS, ......)
    subroutine createPolRRScorrectionLUT(errS)

      implicit none

      type(errorType), intent(inout) :: errS


    end subroutine createPolRRScorrectionLUT



  end module createLUTModule


