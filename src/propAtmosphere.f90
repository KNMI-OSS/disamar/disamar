!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

! **********************************************************************
! This module is part of DISAMAR
! The module propAtmosphereModule contains subroutines to
!     - determine the number of phase function coefficients from the threshold
!       given in the cnfiguration file
!     - read absorption cross sections and determine them on
!       a wavelength and the retrieval altitude grid
!     - calculate local optical properties of an atmosphere
!       on the RTM grid and a subgrid for a specific wavelength
!
! Author: Johan de Haan, Sept 2007 - febr 2008
!
!         Modfied to account for polarization effects on the radiance - dec 2010
!
! **********************************************************************

module propAtmosphereModule

  use dataStructures
  use staticDataModule
  use mathTools,          only: splintLin, spline, splint, polyInt, gaussDivPoints, &
                                getSmoothAndDiffXsec, slitfunction, fleg
  use readModule,         only: getAbsorptionCoef, getAbsorptionCoefStatic
  use ramansspecs,        only: RayXsec, CabannesXsec, CabannesAlbedo, depolarization_factor_air
  use HITRANModule,       only: getAbsorptionXsecLineAbs

  ! default is private
  private
  public  :: getNumberPhasefcoef, getAbsorptionXsec, getAbsorptionXsecUsingLUT, createXsecLUT, getOptPropAtm
  public  :: update_altitudes_PTz_grid, update_T_z_other_grids, update_ndens_vmr, &
             calculate_T_true, calculateTotalColumn, fillHighResolutionPressureGrid
  public  :: insertCorrelationSa_vmr, insertCorrelationSa_T, getOptPropAtmSim

  contains


    subroutine fillHighResolutionPressureGrid(errS, cloudAerosolRTMgridS, gasPTS)

      ! The temperature as function of pressure is given in the configuration file at, perhaps a few,
      ! pressures, called pressureNodes. It is possible to use spline or linear interpolation
      ! to evaluate the temperature at other pressures. However, we also need a pressure grid
      ! with many pressure levels, for instance to calculate the altitudes of the nodes for
      ! trace gases using interpolation on the pressure - altitude grid for air. Here the high
      ! resolution pressure grid is defined.

      ! A few nodes for the temperature are used if the temperature is to be fitted on a grid
      ! with a crude resolution. This can be used to see how many temperature nodes are needed
      ! for an accurate retrieval.

      ! The temperature on the high-resolution grid is not filled here, but filled in
      ! subroutine update_T_z_other_grids defined in propAtmosphereModule. If the temperature
      ! changes during fitting, the high-resolution pressure grid remains the same, but the
      ! temperature and altitudes in the gasPTS structure changes.

      ! In addition, memory is claimed for those arrays in the gasPTS structure
      ! that are defined on the high-resolution pressure grid.


      implicit none

      type(errorType),               intent(inout) :: errS
      type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS   ! contains upper boundary atmosphere
      type(gasPTType),               intent(inout) :: gasPTS                 ! pressure and temperature profiles

      ! local
      integer     :: ipressure, iadditional, index
      integer     :: numAdditionalPressures(gasPTS%npressureNodes)
      integer     :: lastPressure
      real(8), parameter :: scaleHeight = 8.0d0  ! in km
      real(8)     :: delta_z, delta_lnp, deltaNodes_lnp
      real(8)     :: upperBoundaryAtm

      integer     :: allocStatus
      integer     :: sumAllocStatus

      logical, parameter :: verbose = .false.

      call enter('fillHighResolutionPressureGrid')

      upperBoundaryAtm = cloudAerosolRTMgridS%intervalBounds_P(cloudAerosolRTMgridS%ninterval)

      if ( gasPTS%pressureNodes(gasPTS%npressureNodes) > upperBoundaryAtm ) then
        call logDebug('ERROR in configuration file: temperatures do not cover the atmosphere')
        call logDebug('detected by subroutine fillHighResolutionPressureGrid')
        call logDebug('in module readConfigFileModule')
        call logDebug('extend the PT table to cover the atmosphere')
        call logDebug('or reduce the upper bound of the atmosphere')
        call mystop(errS, 'stopped because PT table does not cover the atmosphere')
        if (errorCheck(errS)) return
      end if

      ! determine the number of pressures between the nodes
      lastPressure = 0
      do ipressure = 1, gasPTS%npressureNodes
        delta_z = scaleHeight * log( gasPTS%pressureNodes(ipressure - 1) / gasPTS%pressureNodes(ipressure) )
        numAdditionalPressures(ipressure) = int(delta_z)
        lastPressure = lastPressure + numAdditionalPressures(ipressure) + 1
      end do

      gasPTS%npressure = lastPressure

      ! allocate memory

      allocStatus    = 0
      sumAllocStatus = 0

      allocate( gasPTS%alt(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( gasPTS%altAP(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( gasPTS%pressure(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( gasPTS%lnpressure(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( gasPTS%temperatureAP(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( gasPTS%temperature(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( gasPTS%temperature_true(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( gasPTS%numDensAir(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( gasPTS%scaleHeight(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      allocate( gasPTS%scaleHeightAP(0:gasPTS%npressure), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      ! fill pressures
      index = 0
      do ipressure = 1, gasPTS%npressureNodes
        gasPTS%pressure(index) =  gasPTS%pressureNodes(ipressure-1)
        index = index + 1
        if ( numAdditionalPressures(ipressure) > 0 ) then
          deltaNodes_lnp = log( gasPTS%pressureNodes(ipressure - 1) / gasPTS%pressureNodes(ipressure) )
          delta_lnp = deltaNodes_lnp / ( numAdditionalPressures(ipressure) + 1 )
          do iadditional = 1, numAdditionalPressures(ipressure)
            gasPTS%pressure(index) = gasPTS%pressure(index - 1) * exp ( - delta_lnp )
            index = index + 1
          end do ! iadditional
        end if
        gasPTS%pressure(index) = gasPTS%pressureNodes(ipressure)
      end do

      gasPTS%lnpressure(:) = log( gasPTS%pressure(:) )

      if ( verbose ) then
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*) 'pressure at nodes'
        do ipressure = 0, gasPTS%npressureNodes
          if ( ipressure == 0 ) then
            write(intermediateFileUnit,'(F14.8)') gasPTS%pressureNodes(ipressure)
          else
            write(intermediateFileUnit,'(F14.8,I4)') gasPTS%pressureNodes(ipressure), &
                                                     numAdditionalPressures(ipressure)
          end if
        end do
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*) 'all pressures'
        do ipressure = 0, gasPTS%npressure
          write(intermediateFileUnit, '(F14.8)' ) gasPTS%pressure(ipressure)
        end do
      end if

      call exit('fillHighResolutionPressureGrid')

    end subroutine fillHighResolutionPressureGrid


    subroutine update_altitudes_PTz_grid(errS, surfaceS, gasPTS)

     ! Given a pressure grid and associated temperatures and the surface altitude
     ! the altitude grid associated with the temperature and pressure grid is calculated
     ! assuming hydrostatic equilibrium. Note that the gravitational acceleration g(z)
     ! is given in terms of the altitude above the geoid, which means that the altitude
     ! grid can only be calculated iteratively.

     ! The altitude follows from the expression
     !
     ! z_i = z_s + int_ln(p_i)_ln(p_s)[ H(T) dln(p) ]
     !
     ! where z_i is the altitude corresponding to pressure level p_i
     !       z_s is the altitude at the surface
     !       p_s is the surface pressure
     !       the integration inteval runs from ln(p_i) to ln(p_s)
     !       the integration variable is ln(p), the natural logarithm of the pressure

     !       the scale height H(p) = R * T(p) / g(p) / M   ! the unit of H is [m]
     !       R    = universalGasConstant = 8.3144621d0     ! R in J K-1 mole-1
     !       M    = meanMolWeightAir     = 28.964d-3       ! M in kg mole-1  (dry air only)
     !       T(p) = temperature at pressure level p in Kelvin
     !       g(p) = gravitational acceleration

     ! The initial altitude grid used for the gravitational acceleartion assumes a
     ! fixed scale height H = 8000 m
     ! We use repeated gauss integration using 2 gaussian division points on each interval [ln(p_i+1), ln(p_i)]
     ! and the values at the division points are obtained using cubic spline interpolation on ln(p) and T.

      implicit none

      type(errorType),      intent(inout) :: errS
      type(LambertianType), intent(in)    :: surfaceS                    ! contains surface altitude
      type(gasPTType),      intent(inout) :: gasPTS                      ! pressure and temperature profiles

      ! local
      real(8), parameter :: meanMolWeightAir     = 28.964d-3   ! M in kg mole-1
      real(8), parameter :: universalGasConstant = 8.3144621d0 ! R in J K-1 mole-1
      integer, parameter :: numDivisionPoints    = 2

      ! length of arrays with Gaussian division points = numDivisionPoints * gasPTS%nalt
      real(8)    :: lnpressure_gp   (numDivisionPoints*gasPTS%npressure)
      real(8)    :: scaleHeight_gp  (numDivisionPoints*gasPTS%npressure)
      real(8)    :: scaleHeightAP_gp(numDivisionPoints*gasPTS%npressure)
      real(8)    :: temperature_gp  (numDivisionPoints*gasPTS%npressure)
      real(8)    :: temperatureAP_gp(numDivisionPoints*gasPTS%npressure)
      real(8)    :: altitude_gp     (numDivisionPoints*gasPTS%npressure)
      real(8)    :: altitudeAP_gp   (numDivisionPoints*gasPTS%npressure)
      real(8)    :: weight_gp       (numDivisionPoints*gasPTS%npressure)

      ! arrays for the gaussian division points and weights on (0,1)
      real(8)    :: x0(numDivisionPoints), w0(numDivisionPoints)

      real(8)    :: dlnp, chi2
      real(8)    :: surfalt_update         ! surface altitude after update

      ! counters
      integer    :: ipressure, ialt, igauss, status
      integer    :: startIndex, iteration

      ! second order derivatives for spline interpolation
      real(8)    :: SDtemperatureNodes(0:gasPTS%npressureNodes)
      real(8)    :: SDtemperatureNodesAP(0:gasPTS%npressureNodes)
      real(8)    :: SDalt  (0:gasPTS%npressure)
      real(8)    :: SDaltAP(0:gasPTS%npressure)

      ! arrays on pressure grid
      real(8)    :: prev_altitude(0:gasPTS%npressure), altitude(0:gasPTS%npressure)
      real(8)    :: prev_altitudeAP(0:gasPTS%npressure), altitudeAP(0:gasPTS%npressure)

      ! set maximum number of iterations
      integer, parameter :: maxNumIterations = 6

      logical, parameter :: verbose = .false.

      call enter('update_altitudes_PTz_grid')

      ! calculate the temperature on the high resolution grid using linear or spline interpolation
      if ( gasPTS%useLinInterp ) then
        do ipressure = 0, gasPTS%npressure
          gasPTS%temperature(ipressure)   = splintLin(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodes, &
                                                      gasPTS%lnpressure(ipressure), status)
          gasPTS%temperatureAP(ipressure) = splintLin(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodesAP, &
                                                      gasPTS%lnpressure(ipressure), status)
        end do
      else
        call spline(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodes  , SDtemperatureNodes  , status)
        if (errorCheck(errS)) return
        call spline(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodesAP, SDtemperatureNodesAP, status)
        if (errorCheck(errS)) return
        do ipressure = 0, gasPTS%npressure
          gasPTS%temperature(ipressure)   = splint(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodes, &
                                                   SDtemperatureNodes, gasPTS%lnpressure(ipressure), status)
          gasPTS%temperatureAP(ipressure) = splint(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodesAP, &
                                                   SDtemperatureNodesAP, gasPTS%lnpressure(ipressure), status)
        end do
      end if

      ! determine Gaussian division points and fill lnpressure_gp and weight_gp
      call GaussDivPoints(errS, 0.0d0, 1.0d0, x0, w0)
      
      if (errorCheck(errS)) return
      do ipressure = 1, gasPTS%npressure
        dlnp = gasPTS%lnpressure(ipressure - 1) - gasPTS%lnpressure(ipressure)
        startIndex = (ipressure - 1) * numDivisionPoints
        do igauss = 1, numDivisionPoints
          lnpressure_gp(startIndex + igauss) = gasPTS%lnpressure(ipressure) + dlnp * x0(igauss)
          weight_gp    (startIndex + igauss) = dlnp * w0(igauss)
        end do ! iGauss
      end do ! ipressure

      if ( gasPTS%useLinInterp ) then
        !  linear interpolation on the temperature: get results on Gauss grid
        do ipressure = 1, numDivisionPoints*gasPTS%npressure
          temperature_gp(ipressure)   = splintLin(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodes, &
                                                  lnpressure_gp(ipressure), status)
          temperatureAP_gp(ipressure) = splintLin(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodesAP, &
                                                  lnpressure_gp(ipressure), status)
        end do ! ipressure
      else
        !  spline interpolate on the temperature: get results on Gauss grid
        do ipressure = 1, numDivisionPoints*gasPTS%npressure
          temperature_gp(ipressure)   = splint(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodes, &
                                        SDtemperatureNodes, lnpressure_gp(ipressure), status)
          temperatureAP_gp(ipressure) = splint(errS, gasPTS%lnpressureNodes, gasPTS%temperatureNodesAP, &
                                        SDtemperatureNodesAP, lnpressure_gp(ipressure), status)
        end do ! ipressure
      end if

      ! intinialize chi2
      chi2 = 1000.0

      ! initialize altitude (in km) using scale height of 8 km
      altitude(:)    = 8.0d0 * ( log(surfaceS%pressure) - gasPTS%lnpressure(:) )
      altitude_gp(:) = 8.0d0 * ( log(surfaceS%pressure) - lnpressure_gp(:) )
      prev_altitude(:)   = altitude(:)
      altitudeAP(:)      = altitude(:)
      altitudeAP_gp(:)   = altitude_gp(:)
      prev_altitudeAP(:) = altitude(:)

      ! start iteration
      do iteration = 1, maxNumIterations

        if ( verbose ) then
          write(intermediateFileUnit, *)
          write(intermediateFileUnit, *) 'updating the altitudes for PT grid, iteration = ', iteration
          write(intermediateFileUnit, '(A,ES15.6)' ) 'chi2 = ', chi2
          write(intermediateFileUnit, *) 'pressure       altitude    altitudeAP   temperature  temperatureAP'
          do ialt = 0, gasPTS%npressure
            write(intermediateFileUnit,'(F10.4,4F12.6)') gasPTS%pressure(ialt), altitude(ialt), altitudeAP(ialt), &
                                                         gasPTS%temperature(ialt),  gasPTS%temperatureAP(ialt)
          end do ! ialt
        end if

        ! calculate scale height (in km) at gauss grid
        do ipressure = 1, numDivisionPoints*gasPTS%npressure
          scaleHeight_gp(ipressure) = 1.0d-3 * universalGasConstant * temperature_gp(ipressure) / meanMolWeightAir &
                                    / gravitationalAcceleration(45.0d0, altitude_gp(ipressure))
          scaleHeightAP_gp(ipressure) = scaleHeight_gp(ipressure) &
                                      * temperatureAP_gp(ipressure) / temperature_gp(ipressure)
        end do ! ipressure

        ! calculate altitude at pressure grid using integration
        do ipressure = 1, gasPTS%npressure
          startIndex = (ipressure - 1) * numDivisionPoints
          altitude(ipressure)   = altitude(ipressure-1)
          altitudeAP(ipressure) = altitudeAP(ipressure-1)
          do igauss = 1, numDivisionPoints
            altitude(ipressure)   = altitude(ipressure) &
                                  + weight_gp(startIndex + igauss) * scaleHeight_gp(startIndex + igauss)
            altitudeAP(ipressure) = altitudeAP(ipressure) &
                                  + weight_gp(startIndex + igauss) * scaleHeightAP_gp(startIndex + igauss)
          end do ! igauss
        end do ! ipressure

        ! calculate chi2
        chi2 = sum( (altitude(:) - prev_altitude(:) )**2 )

        if ( chi2 < 1.0d-6 ) exit  ! exit iteration loop

        prev_altitude(:)   = altitude(:)
        prev_altitudeAP(:) = altitudeAP(:)

      end do  ! iteration

      ! shift altitude grid such that the surface altitude is zero
      surfalt_update = splintLin(errS, gasPTS%pressure, altitude, surfaceS%pressure, status)
      altitude(:)   = altitude(:)   - surfalt_update
      altitudeAP(:) = altitudeAP(:) - surfalt_update

      if ( verbose ) then
        write(intermediateFileUnit, *)
        write(intermediateFileUnit, *) 'altitude shift'
        write(intermediateFileUnit, *) 'pressure       altitude    altitudeAP'
        do ialt = 0, gasPTS%npressure
          write(intermediateFileUnit,'(F10.4,2F12.6)') gasPTS%pressure(ialt), altitude(ialt), altitudeAP(ialt)
        end do ! ialt
      end if


      ! copy altitude to gasPT structure

      gasPTS%alt(:)   = altitude(:)
      gasPTS%altAP(:) = altitudeAP(:)

      ! determine altitude of the nodes using interpolation
      call spline(errS, gasPTS%lnpressure,   gasPTS%alt,   SDalt, status)
      if (errorCheck(errS)) return
      call spline(errS, gasPTS%lnpressure, gasPTS%altAP, SDaltAP, status)
      if (errorCheck(errS)) return
      do ialt = 0, gasPTS%npressureNodes
        gasPTS%altNodes(ialt)   = splint(errS, gasPTS%lnpressure,   gasPTS%alt,   SDalt, gasPTS%lnpressureNodes(ialt), status)
        gasPTS%altNodesAP(ialt) = splint(errS, gasPTS%lnpressure, gasPTS%altAP, SDaltAP, gasPTS%lnpressureNodes(ialt), status)
      end do

      ! fill the scale height on the pressure grid
      do ipressure = 0, gasPTS%npressure
        gasPTS%scaleHeight(ipressure)   = 1.0d-3 * universalGasConstant * gasPTS%temperature(ipressure) &
              / meanMolWeightAir / gravitationalAcceleration( 45.0d0, gasPTS%alt(ipressure) )
        gasPTS%scaleHeightAP(ipressure) = 1.0d-3 * universalGasConstant * gasPTS%temperatureAP(ipressure) &
              / meanMolWeightAir / gravitationalAcceleration( 45.0d0, gasPTS%altAP(ipressure) )
      end do

      call exit('update_altitudes_PTz_grid')

    end subroutine update_altitudes_PTz_grid


    subroutine update_T_z_other_grids(errS, numSpectrBands, nTrace, surfaceS, gasPTS, traceGasS, &
                                      cloudAerosolRTMgridS, LambCloudS)

       ! Use spline or linear interpolation on lnpressure to obtain the temperature
       ! on the retrieval grid for each trace gas.
       ! Once P and T are known on the retrieval grid one can
       ! easily calculate the number density of air on this grid.
       ! As the volume mixing ratio is specified, the number density of the
       ! trace gas is just a multiplication away.

       ! In addition, the altitudes and the variance for the altitudes for the interval grid
       ! are calculated.

       implicit none

       type(errorType),               intent(inout) :: errS
       integer,                       intent(in)    :: numSpectrBands
       integer,                       intent(in)    :: nTrace
       type(LambertianType),          intent(inout) :: surfaceS(numSpectrBands)        ! contains surface altitude
       type(gasPTType),               intent(inout) :: gasPTS
       type(traceGasType),            intent(inout) :: traceGasS(nTrace)
       type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS
       type(LambertianType),          intent(inout) :: LambCloudS(numSpectrBands)

       ! local
       integer    :: status
       integer    :: iTrace, ialt, ipressure

       real(8), parameter :: meanMolWeightAir     = 28.964d-3   ! M in kg mole-1
       real(8), parameter :: universalGasConstant = 8.3144621d0 ! R in J K-1 mole-1

       real(8)    :: scaleHeight, temperature, factor

       real(8)    :: SDtemperatureNodesAP(0:gasPTS%npressureNodes)
       real(8)    :: SDtemperatureNodes  (0:gasPTS%npressureNodes)
       real(8)    :: lnpressureNodes     (0:gasPTS%npressureNodes)

       reaL(8)    :: lnpressure    (0:gasPTS%npressure)
       reaL(8)    :: SDaltitude    (0:gasPTS%npressure)  ! spline derivatives for the altitude
       reaL(8)    :: SDaltitudeAP  (0:gasPTS%npressure)  ! spline derivatives for the a-priori altitude

       logical, parameter :: verbose = .false.


       ! calculate temperature on the high resolution pressure grid in gasPTS

       lnpressure      = log(gasPTS%pressure)
       lnpressureNodes = log(gasPTS%pressureNodes)
       if ( gasPTS%useLinInterp ) then
         do ipressure = 0, gasPTS%npressure
           gasPTS%temperature(ipressure) = splintLin(errS, lnpressureNodes, gasPTS%temperatureNodes, &
                                                      lnpressure(ipressure), status)
         end do
       else
         call spline(errS, lnpressureNodes,   gasPTS%temperatureNodes,   SDtemperatureNodes, status)
         if (errorCheck(errS)) return
         call spline(errS, lnpressureNodes, gasPTS%temperatureNodesAP, SDtemperatureNodesAP, status)
         if (errorCheck(errS)) return
         do ipressure = 0, gasPTS%npressure
           gasPTS%temperature(ipressure) = splint(errS, lnpressureNodes, gasPTS%temperatureNodes, SDtemperatureNodes, &
                                                      lnpressure(ipressure), status)
         end do
       end if


       call spline(errS, lnpressure, gasPTS%altAP, SDaltitudeAP, status)
       if (errorCheck(errS)) return
       call spline(errS, lnpressure, gasPTS%alt  ,   SDaltitude, status)
       if (errorCheck(errS)) return

       do iTrace = 1, nTrace

         ! calculate temperatures at nodes where trace gases are specified

         if ( gasPTS%useLinInterp ) then
           do ialt = 0, traceGasS(iTrace)%nalt
             traceGasS(iTrace)%temperature(ialt) = &
               splintLin(errS, lnpressureNodes, gasPTS%temperatureNodes,   log(traceGasS(iTrace)%pressure(ialt)), status)
             traceGasS(iTrace)%temperatureAP(ialt) = &
               splintLin(errS, lnpressureNodes, gasPTS%temperatureNodesAP, log(traceGasS(iTrace)%pressure(ialt)), status)
           end do
         else
           do ialt = 0, traceGasS(iTrace)%nalt
             traceGasS(iTrace)%temperature(ialt) = splint(errS, lnpressureNodes, gasPTS%temperatureNodes,  &
                                           SDtemperatureNodes,   log(traceGasS(iTrace)%pressure(ialt)), status)
             traceGasS(iTrace)%temperatureAP(ialt) = splint(errS, lnpressureNodes, gasPTS%temperatureNodesAP, &
                                           SDtemperatureNodesAP, log(traceGasS(iTrace)%pressure(ialt)), status)
           end do
         end if

         ! calculate altitudes for nodes where trace gases are specified
         ! interpolation is used: presure levels are known and altitude-pressure relation is known for
         ! the high-resolution gasPTS grid

         do ialt = 0, traceGasS(iTrace)%nalt
           traceGasS(iTrace)%alt(ialt) = &
             splint(errS, lnpressure, gasPTS%alt,  SDaltitude,   log(traceGasS(iTrace)%pressure(ialt)), status)
           traceGasS(iTrace)%altAP(ialt) = &
             splint(errS, lnpressure,gasPTS%altAP, SDaltitudeAP, log(traceGasS(iTrace)%pressure(ialt)), status)
         end do

       end do ! iTrace

       ! calculate the altitudes for the atmospheric intervals using interpolation

       do ialt = 0, cloudAerosolRTMgridS%ninterval
         cloudAerosolRTMgridS%intervalBoundsAP(ialt) = &
           splint(errS, lnpressure, gasPTS%altAP, SDaltitudeAP, log(cloudAerosolRTMgridS%intervalBoundsAP_P(ialt)), status)
         cloudAerosolRTMgridS%intervalBounds(ialt) = &
           splint(errS, lnpressure, gasPTS%alt, SDaltitude, log(cloudAerosolRTMgridS%intervalBounds_P(ialt)), status)
       end do
       surfaceS(:)%altitude = cloudAerosolRTMgridS%intervalBounds(0)
       surfaceS(:)%pressure = cloudAerosolRTMgridS%intervalBounds_P(0)

       ! calculate also the variances in the altitudes of the interval boundaries
       ! from the variance for the pressure
       ! using var_z = [ H(z) / p ]**2 var_p because dz = H(z) dln(p)

       do ialt = 0, cloudAerosolRTMgridS%ninterval

         ! a-priori variance

         if ( gasPTS%useLinInterp ) then
           temperature = splintLin(errS, lnpressureNodes, gasPTS%temperatureNodesAP,  &
                                   log(cloudAerosolRTMgridS%intervalBoundsAP_P(ialt)), status)
         else
           temperature = splint(errS, lnpressureNodes, gasPTS%temperatureNodesAP, SDtemperatureNodesAP, &
                                   log(cloudAerosolRTMgridS%intervalBoundsAP_P(ialt)), status)
         end if
         if (ialt == 0 ) surfaceS(:)%temperature = temperature
         if (ialt == cloudAerosolRTMgridS%numIntervalFit) LambCloudS(:)%temperature = temperature

         ! scale height in km
         scaleHeight = 1.0d-3 * universalGasConstant * temperature / meanMolWeightAir &
                     / gravitationalAcceleration(45.0d0, cloudAerosolRTMgridS%intervalBoundsAP(ialt))
         factor = ( scaleHeight / cloudAerosolRTMgridS%intervalBoundsAP_P(ialt) )**2
         cloudAerosolRTMgridS%varIntervalBoundsAP(ialt) = factor * cloudAerosolRTMgridS%varIntervalBoundsAP_P(ialt)

         ! actual variance

         if ( gasPTS%useLinInterp ) then
           temperature = splintLin(errS, lnpressureNodes, gasPTS%temperatureNodes,  &
                                   log(cloudAerosolRTMgridS%intervalBounds_P(ialt)), status)
         else
           temperature = splint(errS, lnpressureNodes, gasPTS%temperatureNodes, SDtemperatureNodes, &
                                   log(cloudAerosolRTMgridS%intervalBounds_P(ialt)), status)
         end if
         ! scale height in km
         scaleHeight = 1.0d-3 * universalGasConstant * temperature / meanMolWeightAir &
                     / gravitationalAcceleration(45.0d0, cloudAerosolRTMgridS%intervalBounds(ialt))
         factor = ( scaleHeight / cloudAerosolRTMgridS%intervalBounds_P(ialt) )**2
         cloudAerosolRTMgridS%varIntervalBounds(ialt) = factor * cloudAerosolRTMgridS%varIntervalBounds_P(ialt)
       end do

       if ( verbose) then
         write(intermediateFileUnit, *)
         do iTrace = 1, nTrace
           write(intermediateFileUnit, *)
           write(intermediateFileUnit, *) 'output from update_T_z_other_grids for iTrace = ', iTrace
           write(intermediateFileUnit, *) 'trace gas grid, nalt = ', traceGasS(iTrace)%nalt
           write(intermediateFileUnit, *) 'altitude(km)  pressure(hPa)   temperature(K)  temperatureAP(K)'
           do ialt = 0, traceGasS(iTrace)%nalt
             write(intermediateFileUnit, '(5ES15.5)') &
                  traceGasS(iTrace)%alt(ialt), traceGasS(iTrace)%pressure(ialt), &
                  traceGasS(iTrace)%temperature(ialt), traceGasS(iTrace)%temperatureAP(ialt)
           end do
         end do ! iTrace
         write(intermediateFileUnit, *)
         write(intermediateFileUnit, *) 'output from update_T_z_other_grids : interval boundaries '
         write(intermediateFileUnit, '(A,10F10.4)') 'pressure bounds AP:', cloudAerosolRTMgridS%intervalBoundsAP_P
         write(intermediateFileUnit, '(A,10F10.4)') 'pressure bounds   :', cloudAerosolRTMgridS%intervalBounds_P
         write(intermediateFileUnit, '(A,10F10.4)') 'altitude bounds AP:', cloudAerosolRTMgridS%intervalBoundsAP
         write(intermediateFileUnit, '(A,10F10.4)') 'altitude bounds   :', cloudAerosolRTMgridS%intervalBounds
       end if

    end subroutine update_T_z_other_grids


    subroutine update_ndens_vmr(errS, nTrace, gasPTS, traceGasS)

       implicit none

       type(errorType),    intent(inout) :: errS
       integer,            intent(in)    :: nTrace
       type(gasPTType),    intent(inout) :: gasPTS
       type(traceGasType), intent(inout) :: traceGasS(nTrace)

       ! local
       integer            :: iTrace, ialt
       logical, parameter :: verbose  = .false.


       ! fill number density of air in the gasPT structures
       ! k = Boltzmann constant = 1.380658d-25 J K-1
       ! if the number density is expressed in molocules cm-3 we have
       gasPTS%numDensAir = gasPTS%pressure / gasPTS%temperature / 1.380658d-19

       do iTrace = 1, nTrace

         ! k = Boltzmann's constant = 1.380658d-25 J K-1
         ! if the number density is expressed in molocules cm-3 we have

         traceGasS(iTrace)%numDensAir   = traceGasS(iTrace)%pressure/traceGasS(iTrace)%temperature  /1.380658d-19
         traceGasS(iTrace)%numDensAirAP = traceGasS(iTrace)%pressure/traceGasS(iTrace)%temperatureAP/1.380658d-19

         if ( traceGasS(iTrace)%collisioncmplx ) then

           ! calculate the number density of the parent gas
           traceGasS(iTrace)%numDensParent   = traceGasS(iTrace)%numDensAir   * traceGasS(iTrace)%vmrParent   * 1.0d-6
           traceGasS(iTrace)%numDensParentAP = traceGasS(iTrace)%numDensAirAP * traceGasS(iTrace)%vmrParentAP * 1.0d-6

           ! fill number density using the number density of the collision complex
           traceGasS(iTrace)%numDens   = traceGasS(iTrace)%numDensParent ** 2
           traceGasS(iTrace)%numDensAP = traceGasS(iTrace)%numDensParentAP **2

           ! calculate the vmr for the collision complex in ppmv
           traceGasS(iTrace)%vmr   = 1.0d6 * traceGasS(iTrace)%numDens   / traceGasS(iTrace)%numDensAir
           traceGasS(iTrace)%vmrAP = 1.0d6 * traceGasS(iTrace)%numDensAP / traceGasS(iTrace)%numDensAirAP

           ! initialize covariance to zero
           traceGasS(iTrace)%covNumDensParentAP = 0.0d0
           traceGasS(iTrace)%covNumDensParent   = 0.0d0
           traceGasS(iTrace)%covVmrAP           = 0.0d0

           ! calculate the diagonal elements covariance of the collision complex

           !  - first calculate diagonal elements of the covariance of the number density of the parent gas
           do ialt = 0, traceGasS(iTrace)%nalt
               traceGasS(iTrace)%covNumDensParentAP(ialt,ialt) = traceGasS(iTrace)%numDensAirAP(ialt) **2 &
                                                   * traceGasS(iTrace)%covVmrParentAP(ialt,ialt) * 1.0d-12

           end do ! ialt

           !  - then calculate the diagonal elements of the covariance of the number density of the collision complex
           ! use n_O2_O2 = n_O2 **2 => dn_O2_O2/dn_O2 = 2*n_O2 => (dn_O2_O2)**2 = 4 * n_O2_O2 * (dn_O2)**2
           ! therefore cov_O2_O2 = 4 * n_O2_O2 * cov_O2
           do ialt = 0, traceGasS(iTrace)%nalt
             traceGasS(iTrace)%covNumDensAP(ialt,ialt) = traceGasS(iTrace)%covNumDensParentAP(ialt,ialt) &
                                                       * traceGasS(iTrace)%numDensAP(ialt)
           end do ! ialt

           !  - then calculate the diagonal elements of the covariance for vmr of the collision complex
           !  vmr in ppmv => 1.0d12
           do ialt = 0, traceGasS(iTrace)%nalt
             traceGasS(iTrace)%covVmrAP(ialt,ialt) = 1.0d12 * traceGasS(iTrace)%covNumDensAP(ialt,ialt) &
                                                   / traceGasS(iTrace)%numDensAirAP(ialt)**2
           end do ! ialt

         else

           ! fill values for the number density of the trace gas
           ! note vmr is in ppmv while number density is in molecules cm-3 => multiply with 1.0d-6

           traceGasS(iTrace)%numDens   = traceGasS(iTrace)%numDensAir   * traceGasS(iTrace)%vmr   * 1.0d-6
           traceGasS(iTrace)%numDensAP = traceGasS(iTrace)%numDensAirAP * traceGasS(iTrace)%vmrAP * 1.0d-6

           ! initialize covariance
           traceGasS(iTrace)%covNumDensAP = 0.0d0

           do ialt = 0, traceGasS(iTrace)%nalt
             traceGasS(iTrace)%covNumDensAP(ialt,ialt) = traceGasS(iTrace)%covVmrAP(ialt,ialt) * 1.0d-12 &
                                                       * traceGasS(iTrace)%numDensAirAP(ialt)**2
           end do ! ialt


         end if  ! traceGasS(iTrace)%collisioncmplx

         if ( traceGasS(iTrace)%useAPCorrLength ) call insertCorrelationSa_vmr(errS, traceGasS(iTrace))
                                                  if (errorCheck(errS)) return

       end do ! iTrace

       if ( verbose) then
           write(intermediateFileUnit, *)
           do iTrace = 1, nTrace
             write(intermediateFileUnit, *)
             write(intermediateFileUnit, *) 'output from update_ndens_vmr for iTrace = ', iTrace
             write(intermediateFileUnit, *) 'trace gas grid, nalt = ', traceGasS(iTrace)%nalt
             write(intermediateFileUnit, *) 'altitude(km)  pressure(hPa)   numberDensity   numberDensityAP    T    TAP'
             do ialt = 0, traceGasS(iTrace)%nalt
               write(intermediateFileUnit, '(8ES15.5)') &
                    traceGasS(iTrace)%alt(ialt), traceGasS(iTrace)%pressure(ialt),      &
                    traceGasS(iTrace)%numDens(ialt), traceGasS(iTrace)%numDensAP(ialt), &
                    traceGasS(iTrace)%temperature(ialt), traceGasS(iTrace)%temperatureAP(ialt)
             end do
           end do ! iTrace
       end if ! verbose

    end subroutine update_ndens_vmr


    subroutine insertCorrelationSa_vmr(errS, traceGasS)

      implicit none

      type(errorType),    intent(inout) :: errS
      type(traceGasType), intent(inout) :: traceGasS

      ! local
      integer :: ialt, jalt
      real(8) :: absDist, sign

      logical, parameter :: verbose  = .false.

      ! NOTE: if the covariance has been read from file this routine overwrites the non-diagonal elements
      !       useAPCorrLength should be set to .false. in the configuration file if you do not want this.

      ! we use the Markov model (Rodgers) for correlation index = 1
      ! the other values of the index have been used for numerical experiments

      ! The correlation between the errors can be applied to ln(vmr), i.e. the relative errors for the
      ! volume mixing ratio or the errors in the number density with a special approach that reduces
      ! errors near the ozone maximum.

      select case ( traceGasS%correlationIndex )

        case(1) ! correlation for the relative errors in the volume mixing ratio - normal case

          do jalt = 0, traceGasS%nalt
            ! calculate the diagonal elements for Sa_lnvmr and store them in covVmrAP
            traceGasS%covVmrAP(jalt,jalt) = traceGasS%covVmrAP(jalt,jalt) &
                                          / ( traceGasS%vmrAP(jalt) ) **2
          end do
          ! calculate the non-diagonal elements for Sa_lnvmr and store them in covVmrAP
          do jalt = 0, traceGasS%nalt
            do ialt = 0, traceGasS%nalt
              absDist = abs( traceGasS%alt(ialt) - traceGasS%alt(jalt) )
              traceGasS%covVmrAP(ialt,jalt) = exp(- absDist / traceGasS%APcorrLength ) &
                * sqrt( traceGasS%covVmrAP(ialt,ialt) ) * sqrt( traceGasS%covVmrAP(jalt,jalt) )
            end do
          end do

          if ( traceGasS%removeAPcorrTropStrat ) then
            do jalt = traceGasS%toplevelTroposphere + 1, traceGasS%nalt
              do ialt = 0, traceGasS%toplevelTroposphere
                traceGasS%covVmrAP(ialt,jalt) = 0.0d0
              end do
            end do
            do jalt = 0, traceGasS%toplevelTroposphere
              do ialt =  traceGasS%toplevelTroposphere + 1, traceGasS%nalt
                traceGasS%covVmrAP(ialt,jalt) = 0.0d0
              end do
            end do
          end if

          ! next calculate Sa_vmr and Sa_ndens
          do jalt = 0, traceGasS%nalt
            do ialt = 0, traceGasS%nalt
              traceGasS%covVmrAP(ialt,jalt) = traceGasS%covVmrAP(ialt,jalt)   &
                            * traceGasS%vmrAP(ialt) * traceGasS%vmrAP(jalt)
              traceGasS%covNumDensAP(ialt,jalt) = traceGasS%covVmrAP(ialt,jalt) * 1.0d-12 &
                            * traceGasS%numDensAir(ialt) * traceGasS%numDensAir(jalt)
            end do
          end do

        case(2)  ! correlation for the errors in the number density
                 ! use cov(i,j) = cov(i.i) * exp( -abs(z(i) - z(j))/corrLength ) for i > j
                 ! and make it symmetrical cov(j,i) = cov(i,j)  for j > i
                 ! reduces oscilations near the ozone maximum

          do jalt = 0, traceGasS%nalt
            do ialt = jalt, traceGasS%nalt
              absDist = abs( traceGasS%alt(ialt) - traceGasS%alt(jalt) )
              traceGasS%covNumDensAP(ialt,jalt) = exp(- absDist / traceGasS%APcorrLength ) &
                                                * traceGasS%covNumDensAP(jalt,jalt)
              traceGasS%covNumDensAP(jalt,ialt) = traceGasS%covNumDensAP(ialt,jalt)
            end do
          end do

          ! next calculate Sa_vmr
          do jalt = 0, traceGasS%nalt
            do ialt = 0, traceGasS%nalt
              traceGasS%covVmrAP(ialt,jalt) = traceGasS%covNumDensAP(ialt,jalt) * 1.0d12 &
                                            / traceGasS%numDensAir(ialt) / traceGasS%numDensAir(jalt)
            end do
          end do

        case(3) ! anti-correlation for the relative errors in the volume mixing ratio

          do jalt = 0, traceGasS%nalt
            ! calculate the values for Sa_lnvmr and store them in vmrCovTraceAP
            traceGasS%covVmrAP(jalt,jalt) = traceGasS%covVmrAP(jalt,jalt) &
                                          / ( traceGasS%vmrAP(jalt) ) **2
          end do
          ! calculate correlation for Sa_lnvmr and store them in vmrCovTraceAP
          do jalt = 0, traceGasS%nalt
            do ialt = 0, traceGasS%nalt
              if ( abs(ialt - jalt) == 1) then
                sign = -1.0d0
              else
                sign =  1.0d0
              end if
              absDist = abs( traceGasS%alt(ialt) - traceGasS%alt(jalt) )
              traceGasS%covVmrAP(ialt,jalt) = sign * exp(- absDist / traceGasS%APcorrLength ) &
                * sqrt( traceGasS%covVmrAP(ialt,ialt) ) * sqrt( traceGasS%covVmrAP(jalt,jalt) )
            end do
          end do

          ! next calculate Sa_vmr and Sa_ndens
          do jalt = 0, traceGasS%nalt
            do ialt = 0, traceGasS%nalt
              traceGasS%covVmrAP(ialt,jalt)  = traceGasS%covVmrAP(ialt,jalt)     &
                             * traceGasS%vmrAP(ialt) * traceGasS%vmrAP(jalt)
              traceGasS%covNumDensAP(ialt,jalt) = traceGasS%covVmrAP(ialt,jalt)  &
                             * 1.0d-12 * traceGasS%numDensAir(ialt) * traceGasS%numDensAir(jalt)
            end do
          end do

        case default

          call logDebug('incorrect value for traceGasS%correlationIndex  ')
          call logDebugI('in insertCorrelationSa: ', traceGasS%correlationIndex)
          call logDebug('for trace gas: '// traceGasS%nameTraceGas )
          call mystop(errS, 'stopped due to incorrect value for traceGasS(iTrace)%correlationIndex')
          if (errorCheck(errS)) return

      end select

      if (verbose) then
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*) 'trace gas = ' , traceGasS%nameTraceGas
        write(intermediateFileUnit,'(A, F6.2)') &
           'output from insertCorrelationSa with APcorrLength (km) =', traceGasS%APcorrLength
        write(intermediateFileUnit,'(A, I4)') &
           'index top level troposphere =', traceGasS%toplevelTroposphere
        write(intermediateFileUnit,*)
        do ialt = 0,traceGasS%nalt
         write(intermediateFileUnit, '(F10.3,100E15.7)' ) traceGasS%alt(ialt), &
                               (traceGasS%covVmrAP(ialt, jalt), jalt = 0,traceGasS%nalt)
        end do
      end if

    end subroutine insertCorrelationSa_vmr


    subroutine insertCorrelationSa_T(gasPTS)

      implicit none

      type(gasPTType), intent(inout) :: gasPTS

      ! local
      integer :: ialt, jalt
      real(8) :: absDist

      logical, parameter :: verbose  = .false.

      ! we use the Markov model (Rodgers)

      ! calculate the non-diagonal elements for Sa_lnvmr and store them in covTemperatureAP
      do jalt = 0, gasPTS%npressureNodes
        do ialt = 0, gasPTS%npressureNodes
          absDist = abs( gasPTS%alt(ialt) - gasPTS%alt(jalt) )
          gasPTS%covTempNodesAP(ialt,jalt) = exp(- absDist / gasPTS%APcorrLength ) &
            * sqrt( gasPTS%covTempNodesAP(ialt,ialt) ) * sqrt( gasPTS%covTempNodesAP(jalt,jalt) )
        end do
      end do

      if (verbose) then
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,'(A, F6.2)') &
           'output from insertCorrelationSa_T with APcorrLength (km) =', gasPTS%APcorrLength
        write(intermediateFileUnit,*)
        do ialt = 0,gasPTS%npressureNodes
         write(intermediateFileUnit, '(F10.3,100E15.7)' ) gasPTS%alt(ialt), &
                               (gasPTS%covTempNodesAP(ialt, jalt), jalt = 0,gasPTS%npressureNodes)
        end do
      end if

    end subroutine insertCorrelationSa_T


    subroutine calculate_T_true(errS, gasPTSimS, gasPTRetrS, TmeanSim, TmeanRetr)

      ! The temperature grid can differe between simulation and retrieval.
      ! In order to be able to compare the temperature profile, in particular
      ! if the temperature profile is fitted, we need to calculate the temperature
      ! used in the simulation ( the true temperature) on the retrieval grid.
      ! Interpolation is used for the calculations.

      type(errorType), intent(inout) :: errS
       type(gasPTType), intent(in)    :: gasPTSimS
       type(gasPTType), intent(inout) :: gasPTRetrS
       real(8)        , intent(out)   :: TmeanSim
       real(8)        , intent(out)   :: TmeanRetr

      ! local
      integer :: ipressure, status
      real(8) :: SDtemperatureNodesSim(0:gasPTSimS%npressureNodes)


      ! fill temperature_true for retrieval interpolating on the temperature used in the simulation

      if ( gasPTSimS%useLinInterp ) then
        do ipressure = 0, gasPTRetrS%npressure
          gasPTRetrS%temperature_true(ipressure) = splintLin(errS, gasPTSimS%lnpressureNodes, gasPTSimS%temperatureNodes, &
                                                             gasPTRetrS%lnpressure(ipressure), status)
        end do
        do ipressure = 0, gasPTRetrS%npressureNodes
          gasPTRetrS%temperature_trueNodes(ipressure) = splintLin(errS, gasPTSimS%lnpressureNodes,  &
                                   gasPTSimS%temperatureNodes, gasPTRetrS%lnpressureNodes(ipressure), status)
        end do
      else
        call spline(errS, gasPTSimS%lnpressureNodes, gasPTSimS%temperatureNodes, SDtemperatureNodesSim, status)
        if (errorCheck(errS)) return
        do ipressure = 0, gasPTRetrS%npressure
          gasPTRetrS%temperature_true(ipressure) = splint(errS, gasPTSimS%lnpressureNodes, gasPTSimS%temperatureNodes, &
                                                   SDtemperatureNodesSim, gasPTRetrS%lnpressure(ipressure), status)
        end do
        do ipressure = 0, gasPTRetrS%npressureNodes
          gasPTRetrS%temperature_trueNodes(ipressure) = splint(errS, gasPTSimS%lnpressureNodes, gasPTSimS%temperatureNodes, &
                                               SDtemperatureNodesSim, gasPTRetrS%lnpressureNodes(ipressure), status)
        end do
      end if

      ! calculate the mean temperature for the temperature profile, weighted with the number density of air molecules
      TmeanSim  = sum( gasPTSimS %temperature(:) * gasPTSimS %numDensAir(:) ) / sum( gasPTSimS %numDensAir(:) )
      TmeanRetr = sum( gasPTRetrS%temperature(:) * gasPTRetrS%numDensAir(:) ) / sum( gasPTRetrS%numDensAir(:) )

    end subroutine calculate_T_true


    function gravitationalAcceleration(latitude, altitude)

      ! returns the gravitational acceleration, g, on Earth
      ! latitude is the geocentric latitude in degree (0 = equator; 90 is pole)
      ! altitude h in km is the altitude above the reference geoid (mean sea level of oceans)

      implicit none

      real(8), intent(in) :: latitude       ! geocentric latitude in degree
      real(8), intent(in) :: altitude       ! altitude above mean sea level in km
      real(8) :: gravitationalAcceleration  ! in m s-1
      real(8) :: g_at_mean_sea_level

      real(8) :: geodeticLatitude   ! in radians
      real(8) :: sinLat             ! sine of geodetic latitude

      real(8), parameter  :: PI = 3.141592653589793d0

      ! from "Allen's Astrophysical Quantities", fourth edition, A.N Cox (ed), 2000
      geodeticLatitude = atan( tan(latitude*PI/180.0d0) / (0.993306 + 1.049583d-6 * altitude ) )
      sinLat = sin(geodeticLatitude)

      ! surface gravity at mean sea level in ms-2
      g_at_mean_sea_level = 9.78031d0 + 0.05186d0 * sinLat**2

      gravitationalAcceleration = g_at_mean_sea_level - 3.086d-3 * altitude

    end function gravitationalAcceleration


    subroutine calculateTotalColumn(errS, gasPTS, traceGasS, cloudAerosolRTMgridS, column, &
                                    columnAboveCloud, columnAP)

      ! calculate total column of trace gas and its a-priori total column
      ! using numerical integration and spline interpolation on ln(vmr)
      ! the values are returned in column and columnAP in Dobson units
      ! columnAP is an optional parameter
      ! integration is performed from the surface to the top of the atmosphere

      implicit none

      type(errorType),               intent(inout) :: errS
      type(gasPTType),               intent(in)    :: gasPTS
      type(traceGasType),            intent(in)    :: traceGasS
      type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS
      real(8),                       intent(out)   :: column
      real(8),                       intent(out)   :: columnAboveCloud
      real(8), optional,             intent(out)   :: columnAP

      ! parameters for calculating the total column
      integer, parameter :: ngaussLay = 6
      integer :: ilevel, iGauss, statusSpline, statusSplint
      real(8) :: x0(ngaussLay), w0(ngaussLay)
      real(8) :: z_top, z_bot, z, dz, w, dzLayer
      real(8) :: pressure_at_z, temperature_at_z, numDensAir_at_z
      real(8) :: numDensTrace_at_z, numDensTraceAP_at_z, vmr_at_z, vmrAP_at_z
      real(8) :: ln_vmr  (0:traceGasS%nalt), SDln_vmr(0:traceGasS%nalt)
      real(8) :: ln_vmrAP(0:traceGasS%nalt), SDln_vmrAP(0:traceGasS%nalt)
      real(8) :: SDlnpressure(0:gasPTS%npressure)
      real(8) :: SDtemperature(0:gasPTS%npressure)

      logical, parameter :: verbose = .false.

      ! fill gaussian division points and weights on (0.1)
      call GaussDivPoints(errS, 0.0d0, 1.0d0, x0, w0)
      
      if (errorCheck(errS)) return

      ! interpolate on pressure, temperature, and volume mixing ratio to get the
      ! properties of the gas at the integration grid
      ! spline interpolation on the natural logarithm is used for the pressure and the vmr

      ln_vmr     = log(traceGasS%vmr)
      ln_vmrAP   = log(traceGasS%vmrAP)
      call spline(errS, gasPTS%alt,    gasPTS%lnpressure  , SDlnpressure , statusSpline)
      if (errorCheck(errS)) return
      call spline(errS, gasPTS%alt,    gasPTS%temperature , SDtemperature, statusSpline)
      if (errorCheck(errS)) return
      if ( .not. traceGasS%useLinInterp ) then
        call spline(errS, traceGasS%alt, ln_vmr           , SDln_vmr     , statusSpline)
        if (errorCheck(errS)) return
        call spline(errS, traceGasS%alt, ln_vmrAP         , SDln_vmrAP   , statusSpline)
        if (errorCheck(errS)) return
      end if

      if ( verbose ) then
        write(intermediateFileUnit, *)
        write(intermediateFileUnit, *) 'output from calculateTotalColumn'
        write(intermediateFileUnit, *) 'trace gas grid, nalt = ', traceGasS%nalt
        write(intermediateFileUnit, *) 'altitude(km)   pressure(hPa)   temperature(K)    vmr(ppmv)'
        do ilevel = 0, traceGasS%nalt
          write(intermediateFileUnit, '(4ES15.5)') traceGasS%alt(ilevel), traceGasS%pressure(ilevel), &
                                                   traceGasS%temperature(ilevel), traceGasS%vmr(ilevel)
        end do
      end if

      column   = 0.0d0
      if ( present(columnAP) ) columnAP = 0.0d0
      z_top = cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%ninterval)
      z_bot = cloudAerosolRTMgridS%intervalBounds(0)
      if ( verbose ) then
        write(intermediateFileUnit,'(A, 2F15.5)') 'z_bot and z_top (km)', z_bot, z_top
      end if

      do ilevel = 1, traceGasS%nalt
        dzLayer = traceGasS%alt(ilevel) - traceGasS%alt(ilevel-1)
        ! integrate from the surface to the top of the atmosphere: (z_bot,z_top)
        if ( traceGasS%alt(ilevel) > z_top ) dzLayer = z_top - traceGasS%alt(ilevel-1)
        if ( traceGasS%alt(ilevel) < z_bot ) cycle
        if ( traceGasS%alt(ilevel - 1) < z_bot ) dzLayer = traceGasS%alt(ilevel) - z_bot
        do iGauss = 1, ngaussLay
          w  = w0(iGauss) * dzLayer
          dz = x0(iGauss) * dzLayer
          z  = traceGasS%alt(ilevel-1) + dz
          pressure_at_z    = exp( splint(errS, gasPTS%alt, gasPTS%lnpressure, SDlnpressure, z, statusSplint) )
          if ( gasPTS%useLinInterp ) then
            temperature_at_z = splintLin(errS, gasPTS%alt, gasPTS%temperature, z, statusSplint)
          else
            temperature_at_z = splint(errS, gasPTS%alt, gasPTS%temperature, SDtemperature, z, statusSplint)
          end if
          numDensAir_at_z  = pressure_at_z / temperature_at_z / 1.380658d-19
          if ( traceGasS%useLinInterp ) then
            vmr_at_z         = exp( splintLin(errS, traceGasS%alt, ln_vmr  , z, statusSplint) )
            vmrAP_at_z       = exp( splintLin(errS, traceGasS%alt, ln_vmrAP, z, statusSplint) )
          else
            vmr_at_z         = exp( splint(errS, traceGasS%alt, ln_vmr  , SDln_vmr  , z, statusSplint) )
            vmrAP_at_z       = exp( splint(errS, traceGasS%alt, ln_vmrAP, SDln_vmrAP, z, statusSplint) )
          end if
          ! vmr is in ppmv => factor 1.0d-6
          numDensTrace_at_z   = numDensAir_at_z * vmr_at_z   * 1.0d-6
          numDensTraceAP_at_z = numDensAir_at_z * vmrAP_at_z * 1.0d-6
          ! altitude z is in km and numDensTrace is in molecules cm-3
          ! the total column is in mol cm-2 if we express z in cm => factor 1.0d5
          column   = column   + w * numDensTrace_at_z   * 1.0d5
          if ( present(columnAP) ) columnAP = columnAP + w * numDensTraceAP_at_z * 1.0d5
          if ( verbose ) write(intermediateFileUnit,'(F15.5,2E14.5)') z, numDensTrace_at_z, numDensTraceAP_at_z
        end do ! iGauss
        if ( traceGasS%alt(ilevel) > z_top ) exit  ! exit ilevel loop
      end do ! ilevel

      ! separate calculation of the column above the cloud, needed for polarization correction

      columnAboveCloud   = 0.0d0
      z_top = cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%ninterval)
      ! assumes that we have the cloud is located in the fit interval
      ! for a Lambertian cloud the cloud location is the top of the fit interval
      z_bot = cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit)
      if ( verbose ) then
        write(intermediateFileUnit,'(A, 2F15.5)') 'z_bot and z_top (km)', z_bot, z_top
      end if

      do ilevel = 1, traceGasS%nalt
        dzLayer = traceGasS%alt(ilevel) - traceGasS%alt(ilevel-1)
        ! integrate from the cloud to the top of the atmosphere: (z_bot,z_top)
        if ( traceGasS%alt(ilevel) > z_top ) dzLayer = z_top - traceGasS%alt(ilevel-1)
        if ( traceGasS%alt(ilevel) < z_bot ) cycle
        if ( traceGasS%alt(ilevel - 1) < z_bot ) dzLayer = traceGasS%alt(ilevel) - z_bot
        do iGauss = 1, ngaussLay
          w  = w0(iGauss) * dzLayer
          dz = x0(iGauss) * dzLayer
          z  = traceGasS%alt(ilevel-1) + dz
          pressure_at_z    = exp( splint(errS, gasPTS%alt, gasPTS%lnpressure, SDlnpressure, z, statusSplint) )
          if ( gasPTS%useLinInterp ) then
            temperature_at_z = splintLin(errS, gasPTS%alt, gasPTS%temperature, z, statusSplint)
          else
            temperature_at_z = splint(errS, gasPTS%alt, gasPTS%temperature, SDtemperature, z, statusSplint)
          end if
          numDensAir_at_z  = pressure_at_z / temperature_at_z / 1.380658d-19
          if ( traceGasS%useLinInterp ) then
            vmr_at_z         = exp( splintLin(errS, traceGasS%alt, ln_vmr  , z, statusSplint) )
            vmrAP_at_z       = exp( splintLin(errS, traceGasS%alt, ln_vmrAP, z, statusSplint) )
          else
            vmr_at_z         = exp( splint(errS, traceGasS%alt, ln_vmr  , SDln_vmr  , z, statusSplint) )
            vmrAP_at_z       = exp( splint(errS, traceGasS%alt, ln_vmrAP, SDln_vmrAP, z, statusSplint) )
          end if
          ! vmr is in ppmv => factor 1.0d-6
          numDensTrace_at_z   = numDensAir_at_z * vmr_at_z   * 1.0d-6
          numDensTraceAP_at_z = numDensAir_at_z * vmrAP_at_z * 1.0d-6
          ! altitude z is in km and numDensTrace is in molecules cm-3
          ! the total column is in mol cm-2 if we express z in cm => factor 1.0d5
          columnAboveCloud   = columnAboveCloud   + w * numDensTrace_at_z   * 1.0d5
          if ( verbose ) write(intermediateFileUnit,'(F15.5,2E14.5)') z, numDensTrace_at_z, numDensTraceAP_at_z
        end do ! iGauss
        if ( traceGasS%alt(ilevel) > z_top ) exit  ! exit ilevel loop
      end do ! ilevel

    end subroutine calculateTotalColumn



    subroutine getNumberPhasefcoef(cloudAerosolRTMgridS, mieAerS, mieCldS, optPropRTMGridS)

    ! determine the number of phase function coefficients required
    ! so that memory can be allocated

      implicit none

      type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS
      type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)
      type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS

      ! local
      real(8)            :: max_g_aer, max_g_cld, coef
      integer            :: icoef
      integer, parameter :: maxCoef = 150

      ! Rayleigh scattering
      optPropRTMGridS%maxExpCoefGas = 2  ! value for Rayleigh scattering

      ! initialize with no Mie or HG scattering for aerosol
      optPropRTMGridS%maxExpCoefAer = 0

      ! Henysy Greenstein scattering for aerosol
      if ( cloudAerosolRTMgridS%useHGScatAer ) then
        max_g_aer = maxval( cloudAerosolRTMgridS%intervalAer_g(:) )
          coef = 1.0d0
          do icoef = 1, maxCoef
            coef = coef * max_g_aer * dble(2*icoef - 1) / dble(2*icoef + 1)
            if ( coef < cloudAerosolRTMgridS%thresholdPhaseFunctionCoef ) exit
          end do
          optPropRTMGridS%maxExpCoefAer = icoef - 1
      end if ! cloudAerosolRTMgridS%useHGScatAer

      ! Mie scattering for aerosol
      if ( cloudAerosolRTMgridS%useMieScatAer ) then
        optPropRTMGridS%maxExpCoefAer = maxval( mieAerS(:)%numExpCoef )
      end if

      ! initialize with no Mie or HG scattering for cloud
      optPropRTMGridS%maxExpCoefCld = 0

      ! Henysy Greenstein scattering for cloud
      if ( cloudAerosolRTMgridS%useHGScatCld ) then
        max_g_cld = maxval( cloudAerosolRTMgridS%intervalCld_g(:) )
          coef = 1.0d0
          do icoef = 1, maxCoef
            coef = coef * max_g_cld * dble(2*icoef - 1) / dble(2*icoef + 1)
            if ( coef < cloudAerosolRTMgridS%thresholdPhaseFunctionCoef ) exit
          end do
          optPropRTMGridS%maxExpCoefCld = icoef - 1
      end if ! associated

      ! Mie scattering for cloud
      if ( cloudAerosolRTMgridS%useMieScatCld ) then
        optPropRTMGridS%maxExpCoefCld = maxval( mieCldS(:)%numExpCoef )
      end if

      optPropRTMGridS%maxExpCoef = max(optPropRTMGridS%maxExpCoefGas, optPropRTMGridS%maxExpCoefAer, &
                                       optPropRTMGridS%maxExpCoefCld)

    end subroutine getNumberPhasefcoef


    subroutine getAbsorptionXsec (errS, staticS, controlS, weakAbsRetrS, wavelMRS, gasPTS, traceGasS, XsecS, &
                                  wavelInstrS, solarIrrS, earthRadS, wavelHRS)

     ! determine the absorption coefficients of the trace gas
     ! at all wavelengths and altitudes using the temperatures and
     ! pressures in traceGasS and fill the array XsecS%Xsec(nwavel, 0:nalt)

     ! if useHITRAN in XsecS is true, aborption coefficients are determined
     ! using spectral line information in the HITRAN database
     ! if useHITRAN in XsecS is false, it is assumed that the cross section
     ! depends only on the temperature and that the coefficients are available on file

     implicit none

     type(errorType),         intent(inout) :: errS
     type(staticType),        intent(in)    :: staticS
     type(controlType),       intent(in)    :: controlS              ! controls writing of absorption cross sections
     type(weakAbsType),       intent(in)    :: weakAbsRetrS          ! flags and values for an isothermal atmosphere
     type(wavelHRType),       intent(in)    :: wavelMRS              ! high resolution wavelength grid
     type(wavelInstrType),    intent(in)    :: wavelInstrS           ! instrument wavelength grid
     type(gasPTType),         intent(in)    :: gasPTS                ! pressure and temperature
     type(traceGasType),      intent(inout) :: traceGasS             ! atmospheric trace gas properties
     type(XsecType),          intent(inout) :: XsecS                 ! absorption cross sections
     type(SolarIrrType),      intent(inout) :: solarIrrS             ! solar irradiance
     type(EarthRadianceType), intent(inout) :: earthRadS             ! earth radiance
     type(wavelHRType),       intent(inout) :: wavelHRS              ! high resolution wavelength grid
     optional :: wavelInstrS, solarIrrS, earthRadS, wavelHRS         ! needed for convolution with slit

     ! local
     real(8) :: maxAbsXsecRefDiff
     real(8) :: Xsec0(wavelMRS%nwavel)
     real(8) :: Xsec1(wavelMRS%nwavel)
     real(8) :: Xsec2(wavelMRS%nwavel)
     real(8) :: Xsec0Conv(XsecS%nwavel)
     real(8) :: Xsec1Conv(XsecS%nwavel)
     real(8) :: Xsec2Conv(XsecS%nwavel)
     real(8) :: XsecRefSmooth(wavelMRS%nwavel)
     real(8) :: dXsecdTRefSmooth(wavelMRS%nwavel)
     real(8) :: Xsec(wavelMRS%nwavel, 0:XsecS%nalt)
     real(8) :: XsecRefDiff(wavelMRS%nwavel)
     real(8) :: dXsecdTRefDiff(wavelMRS%nwavel)
     real(8), allocatable :: XsecHR0(:)
     real(8), allocatable :: XsecHR1(:)
     real(8), allocatable :: XsecHR2(:)

     real(8) :: sumRatio

     real(8) :: T   ! temperature
     real(8), parameter :: DT = 0.5d0 ! temperature difference used in numerical differentiation

     integer    :: iwave, ialt, iISO, numwavel, nw
     integer    :: allocStatus, sumAllocStatus
     integer    :: deallocStatus, sumdeAllocStatus

     character(len=50) :: unit_abs_Xsec

     ! initialize
     XsecS%Xsec(:,:)        = 0.0d0
     XsecS%dXsecdT(:,:)     = 0.0d0
     XsecS%XsecConv(:,:)    = 0.0d0
     XsecS%dXsecConvdT(:,:) = 0.0d0

     if ( XsecS%useHITRAN ) then

       ! use the normal HITRAN code

       call getAbsorptionXsecLineAbs(errS,  wavelMRS, traceGasS, XsecS )
       if (errorCheck(errS)) return

       ! calculate temperature derivative if it is required

       if ( gasPTS%fitTemperatureProfile .or. gasPTS%fitTemperatureOffset ) then

         ! copy values from XsecS%Xsec to local array Xsec (for storage)
         Xsec(:,:) = XsecS%Xsec(:,:)

         ! perturb temperature
         traceGasS%temperature = traceGasS%temperature + DT

         ! calculate absorption cross section for perturbed temperature
         call getAbsorptionXsecLineAbs(errS,  wavelMRS, traceGasS, XsecS )
         if (errorCheck(errS)) return

         ! calculate numerical derivative
         XsecS%dXsecdT(:,:) = ( XsecS%Xsec(:,:) - Xsec(:,:) ) / DT

         ! restore temperature and cross section to original value
         traceGasS%temperature = traceGasS%temperature - DT
         XsecS%Xsec(:,:) = Xsec(:,:)

       else

         XsecS%dXsecdT = 0.0d0

       end if ! gasPTS%fitTemperatureProfile .or. gasPTS%fitTemperatureOffset

       XsecS%corrFactorAMF = 0.0d0

     else

       ! no line absorbing species
       ! read absorption coefficients from file

       if ( controlS%useEffXsec_OE ) then

         if ( present(wavelInstrS) .and. present(solarIrrS) .and. present(earthRadS) .and. present(wavelHRS) ) then

           sumAllocStatus = 0
           allocate ( XsecHR0(wavelHRS%nwavel),  STAT = allocStatus )
           sumAllocStatus = sumAllocStatus + allocStatus
           allocate ( XsecHR1(wavelHRS%nwavel),  STAT = allocStatus )
           sumAllocStatus = sumAllocStatus + allocStatus
           allocate ( XsecHR2(wavelHRS%nwavel),  STAT = allocStatus )
           sumAllocStatus = sumAllocStatus + allocStatus
           if ( sumAllocStatus /= 0 ) then
             call logDebug('ERROR: allocation failed in subroutine getAbsorptionXsec')
             call logDebug('in module propAtmosphereModule')
             call logDebug('stopped because allocation failed')
             call mystop(errS, 'allocation failed in getAbsorptionXsec')
             if (errorCheck(errS)) return
           end if

           ! MTL if (staticS%operational == 1) then
           if (staticS%operational == 2) then
             if (traceGasS%nameTraceGas == "O2-O2") then
               call getAbsorptionCoef(errS, wavelHRS, XsecS, XsecHR0, XsecHR1, XsecHR2)
             else
               call getAbsorptionCoefStatic(errS, staticS, traceGasS%nameTraceGas, wavelHRS, XsecS, XsecHR0, XsecHR1, XsecHR2)
             end if
           else
             call getAbsorptionCoef(errS, wavelHRS, XsecS, XsecHR0, XsecHR1, XsecHR2)
           end if
           if (errorCheck(errS)) return

           call convoluteXsec(errS, wavelHRS, wavelInstrS, solarIrrS, earthRadS, &
                              XsecHR0, XsecHR1, XsecHR2, Xsec0Conv, Xsec1Conv, Xsec2Conv)
           if (errorCheck(errS)) return

           ! temperature profile specified in configuration file
           do ialt = 0, traceGasS%nalt
             ! temperature for expansion is given in in degres Celsius
             T = traceGasS%temperature(ialt) - 273.15d0
             XsecS%XsecConv(:,ialt)    = Xsec0Conv(:) + T * Xsec1Conv(:) + T * T * Xsec2Conv(:)
             XsecS%dXsecConvdT(:,ialt) = Xsec1Conv(:) + 2.0d0 * T * Xsec2Conv(:)
             XsecS%XsecConv(:,ialt)    = traceGasS%scaleFactorXsec * XsecS%XsecConv(:,ialt)
             XsecS%dXsecConvdT(:,ialt) = traceGasS%scaleFactorXsec * XsecS%dXsecConvdT(:,ialt)
           end do

           ! clean up
           sumdeAllocStatus = 0
           deallocate ( XsecHR0,  STAT = deallocStatus )
           sumdeAllocStatus = sumdeAllocStatus + deallocStatus
           deallocate ( XsecHR1,  STAT = deallocStatus )
           sumdeAllocStatus = sumdeAllocStatus + deallocStatus
           deallocate ( XsecHR2,  STAT = deallocStatus )
           sumdeAllocStatus = sumdeAllocStatus + deallocStatus
           if ( sumdeAllocStatus /= 0 ) then
             call logDebug('ERROR: deallocation failed in subroutine getAbsorptionXsec')
             call logDebug('in module propAtmosphereModule')
             call logDebug('stopped because deallocation failed')
             call mystop(errS, 'deallocation failed in getAbsorptionXsec')
             if (errorCheck(errS)) return
           end if

         else
           call logDebug('ERROR in subroutine getAbsorptionXsec')
           call logDebug('in module propAtmosphere')
           call logDebug('getAbsorptionXsec is not called with optional parameters')
         end if ! present ......

       else

         ! MTL if (staticS%operational == 1) then
         if (staticS%operational == 2) then
           if (traceGasS%nameTraceGas == "O2-O2") then
             call getAbsorptionCoef(errS, wavelMRS, XsecS, Xsec0, Xsec1, Xsec2)
           else
             call getAbsorptionCoefStatic(errS, staticS, traceGasS%nameTraceGas, wavelMRS, XsecS, Xsec0, Xsec1, Xsec2)
           end if
         else
           call getAbsorptionCoef(errS, wavelMRS, XsecS, Xsec0, Xsec1, Xsec2)
         end if
         if (errorCheck(errS)) return

         XsecS%XsecConv(:,:)    = 0.0d0
         XsecS%dXsecConvdT(:,:) = 0.0d0

       end if ! controlS%useEffXsec_OE

       if ( controlS%ignoreSlit ) then
         numwavel = wavelInstrS%nwavel
       else
         numwavel = wavelMRS%nwavel
       end if

       ! MTL if (staticS%operational == 1) then
       if (staticS%operational == 2) then
         if (traceGasS%nameTraceGas == "O2-O2") then
           call getAbsorptionCoef(errS, wavelMRS, XsecS, Xsec0, Xsec1, Xsec2)
         else
           call getAbsorptionCoefStatic(errS, staticS, traceGasS%nameTraceGas, wavelMRS, XsecS, Xsec0, Xsec1, Xsec2)
         end if
       else
         call getAbsorptionCoef(errS, wavelMRS, XsecS, Xsec0, Xsec1, Xsec2)
       end if
       if (errorCheck(errS)) return

       if ( weakAbsRetrS%useReferenceTempRetr .and. (controlS%method == 3 .or. controlS%method == 4 ) ) then
         ! use reference temperature as specified in configuration file
         ! but only for classical DOAS or DOMINO
         do ialt = 0, traceGasS%nalt
           ! temperature for expansion is given in in degres Celsius
           T = weakAbsRetrS%referenceTempRetr - 273.15d0
           do iwave = 1, numwavel
             XsecS%Xsec(iwave,ialt)    = Xsec0(iwave) + T * Xsec1(iwave) + T * T * Xsec2(iwave)
             XsecS%dXsecdT(iwave,ialt) = Xsec1(iwave) + 2.0d0 * T * Xsec2(iwave)
             XsecS%Xsec(iwave,ialt)    = traceGasS%scaleFactorXsec * XsecS%Xsec(iwave,ialt)
             XsecS%dXsecdT(iwave,ialt) = traceGasS%scaleFactorXsec * XsecS%dXsecdT(iwave,ialt)
           end do
         end do
       else
         ! temperature profile specified in configuration file
         do ialt = 0, traceGasS%nalt
           ! temperature for expansion is given in in degres Celsius
           T = traceGasS%temperature(ialt) - 273.15d0
           do iwave = 1, numwavel
             XsecS%Xsec(iwave,ialt)    = Xsec0(iwave) + T * Xsec1(iwave) + T * T * Xsec2(iwave)
             XsecS%dXsecdT(iwave,ialt) = Xsec1(iwave) + 2.0d0 * T * Xsec2(iwave)
             XsecS%Xsec(iwave,ialt)    = traceGasS%scaleFactorXsec * XsecS%Xsec(iwave,ialt)
             XsecS%dXsecdT(iwave,ialt) = traceGasS%scaleFactorXsec * XsecS%dXsecdT(iwave,ialt)
           end do
         end do ! ialt
       end if ! weakAbsRetrS%useReferenceTempRetr

       ! calculate the temperature correction factor for the AMF - needed only for NO2 and only if a
       ! reference temperature is used
       if ( weakAbsRetrS%useReferenceTempRetr .and. (controlS%method == 3 .or. controlS%method == 4 ) ) then
         if ( trim(traceGasS%nameTraceGas) =='NO2'      .or. &
              trim(traceGasS%nameTraceGas) =='trop_NO2' .or. &
              trim(traceGasS%nameTraceGas) =='strat_NO2') then
           ! used fixed value for the degree of the polynomial, namely 3
           if ( controlS%ignoreSlit ) then
             call getSmoothAndDiffXsec(errS, 3, numwavel,  wavelInstrS%wavel, XsecS%Xsec(:,0), XsecRefSmooth, XsecRefDiff)
             if (errorCheck(errS)) return
             call getSmoothAndDiffXsec(errS, 3, numwavel,  wavelInstrS%wavel, XsecS%dXsecdT(:,0), dXsecdTRefSmooth, dXsecdTRefDiff)
             if (errorCheck(errS)) return
           else
             call getSmoothAndDiffXsec(errS, 3, numwavel,  wavelMRS%wavel, XsecS%Xsec(:,0), XsecRefSmooth, XsecRefDiff)
             if (errorCheck(errS)) return
             call getSmoothAndDiffXsec(errS, 3, numwavel,  wavelMRS%wavel, XsecS%dXsecdT(:,0), dXsecdTRefSmooth, dXsecdTRefDiff)
             if (errorCheck(errS)) return
           end if
           ! Calculate mean ratio; note that the differential cross sections become negative and they
           ! are sometimes nearly zero. Therefore, consider only wavelengths where the absolute value of
           !  Xsec0Diff is larger than 20% of the maximum value of abs(Xsec0Diff).
           maxAbsXsecRefDiff = maxval(abs(XsecRefDiff(:)))
           nw = 0
           sumRatio = 0.0d0
           do iwave = 1, numwavel
             if ( abs(XsecRefDiff(iwave) ) > 0.20 * maxAbsXsecRefDiff ) then
               nw = nw + 1
               sumRatio = sumRatio + dXsecdTRefDiff(iwave) / XsecRefDiff(iwave)
             end if ! abs(XsecRefDiff(iwave) ) > 0.20 * maxAbsXsecRefDiff
           end do ! iwave
           XsecS%corrFactorAMF = sumRatio / nw
        else
           XsecS%corrFactorAMF = 0.0d0
         end if ! species is NO2
       else
         XsecS%corrFactorAMF = 0.0d0
       end if ! weakAbsRetrS%useReferenceTempRetr

     end if ! XsecS%useHITRAN

     ! negative values can occur due to spline interpolation, e.g. for ozone around 380 nm
     ! because the tabulated values of Bass and Paur end at 365 nm and values begin again 407 nm
     ! set negative values to zero

     if ( controlS%ignoreSlit ) then
       numwavel = wavelInstrS%nwavel
     else
       numwavel = wavelMRS%nwavel
     end if
     do ialt = 0, traceGasS%nalt
       do iwave = 1, numwavel
         if ( XsecS%Xsec(iwave, ialt) < 0.0d0 )  XsecS%Xsec(iwave, ialt) = 0.0d0
       end do
     end do

     where ( XsecS%XsecConv < 0.0d0 ) XsecS%XsecConv = 0.0d0

     if ( controlS%writeAbsorptionXsec ) then
       if ( trim(traceGasS%nameTraceGas) == 'O2-O2' ) then
         unit_abs_Xsec = 'unit is cm^5 / molecule^2'
       else
         unit_abs_Xsec = 'unit is cm^2 / molecule'
       end if
       if ( controlS%useEffXsec_OE ) then
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit, *) 'CONVOLUTED ABSORPTION CROSS SECTIONS as function of wavelength on instr grid'
         write(addtionalOutputUnit, *) 'for different pressure levels in the atmosphere'
         write(addtionalOutputUnit, *) trim(unit_abs_Xsec)
         write(addtionalOutputUnit, *) 'convolution with slit function weigted with solar irradiance (I0-effect) is used'
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit, *) 'filename                   gasIndex  ISO values'
         write(addtionalOutputUnit, '(A,10I4)') trim(XsecS%XsectionFileName), XsecS%gasIndex,  &
                                                   ( XsecS%ISO(iISO), iISO = 1, XsecS%nISO )
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') 'altitude(km)', &
                                              ('    z =', traceGasS%alt(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                              ('    p =',traceGasS%pressure(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                              ('    T =',traceGasS%temperature(ialt), ialt = 0, traceGasS%nalt)
         do iwave = 1, wavelInstrS%nwavel
           write(addtionalOutputUnit,'(F12.5, 100ES15.7)') wavelInstrS%wavel(iwave), &
                                                    (XsecS%XsecConv(iwave,ialt), ialt = 0, traceGasS%nalt)
         end do

       else

         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit, *) 'ABSORPTION CROSS SECTIONS as function of wavelength on HR grid'
         write(addtionalOutputUnit, *) 'for different pressure levels in the atmosphere'
         write(addtionalOutputUnit, *) trim(unit_abs_Xsec)
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit, *) 'filename                   gasIndex  ISO values'
         write(addtionalOutputUnit, '(A,10I4)') trim(XsecS%XsectionFileName), XsecS%gasIndex,  &
                                                   ( XsecS%ISO(iISO), iISO = 1, XsecS%nISO )
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') 'altitude(km)', &
                                              ('    z =', traceGasS%alt(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                              ('    p =',traceGasS%pressure(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                              ('    T =',traceGasS%temperature(ialt), ialt = 0, traceGasS%nalt)
         do iwave = 1, wavelMRS%nwavel
           write(addtionalOutputUnit,'(F12.5, 100ES15.7)') wavelMRS%wavel(iwave), &
                                                    (XsecS%Xsec(iwave,ialt), ialt = 0, traceGasS%nalt)
         end do

       end if ! controlS%useEffXsec_OE

       if ( gasPTS%fitTemperatureProfile .or. gasPTS%fitTemperatureOffset ) then
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit,'(A)') 'temperature derivative absorption cross secctions'
         write(addtionalOutputUnit,'(A,F8.3)') 'calculated with delta T = ', DT
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') 'altitude(km)', &
                                            ('    z =', traceGasS%alt(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                            ('    p =',traceGasS%pressure(ialt), ialt = 0, traceGasS%nalt)
         do iwave = 1, wavelMRS%nwavel
           write(addtionalOutputUnit,'(F12.5, 100ES15.7)') wavelMRS%wavel(iwave), &
                                                    (XsecS%dXsecdT(iwave,ialt), ialt = 0, traceGasS%nalt)
         end do
       end if

     end if  ! controlS%writeAbsorptionXsec

    end subroutine getAbsorptionXsec


    subroutine getAbsorptionXsecUsingLUT(errS, controlS, wavelMRS, gasPTS, traceGasS, XsecS, XsecLUTS, &
                                         wavelInstrS, solarIrrS, earthRadS, wavelHRS)

     ! Call getAbsorptionXsecFromLUT to get the absorption coefficients when the Xsec LUT is used
     ! at all wavelengths and altitudes/pressures. The temperatures and pressures in traceGasS are used here.
     ! The array XsecS%Xsec(1:nwavel, 0:nalt) is filled.

     implicit none

     type(errorType),         intent(inout) :: errS
     type(controlType),       intent(in)    :: controlS              ! controls writing of absorption cross sections
     type(wavelHRType),       intent(in)    :: wavelMRS              ! high resolution wavelength grid
     type(gasPTType),         intent(in)    :: gasPTS                ! pressure and temperature
     type(traceGasType),      intent(inout) :: traceGasS             ! atmospheric trace gas properties
     type(XsecType),          intent(inout) :: XsecS                 ! absorption cross sections
     type(XsecLUTType),       intent(inout) :: XsecLUTS              ! LUT for expansion coefficients
     type(wavelInstrType),    intent(in)    :: wavelInstrS           ! instrument wavelength grid
     type(SolarIrrType),      intent(inout) :: solarIrrS             ! solar irradiance
     type(EarthRadianceType), intent(inout) :: earthRadS             ! earth radiance
     type(wavelHRType),       intent(inout) :: wavelHRS              ! high resolution wavelength grid
     optional :: wavelInstrS, solarIrrS, earthRadS, wavelHRS         ! needed for convolution with slit

     ! local
     integer    :: iwave, ialt, iISO, numwavel

     character(len=50) :: unit_abs_Xsec

     call enter('getAbsorptionXsecUsingLUT')

     ! This subroutine only be used for optimal estimation
     if (controlS%method > 0) then
       call logDebug('ERROR in configuration file: getAbsorptionXsecUsingLUT is called when method > 0')
       call logDebug('in module propAtmosphereModule')
       call logDebug('change method in configuration file or set usePolyExpXsec to zero')
       call mystop(errS, 'stopped because getAbsorptionXsecUsingLUT is called')
       if (errorCheck(errS)) return
     end if ! controlS%method > 0

     ! initialize
     XsecS%Xsec(:,:)        = 0.0d0
     XsecS%dXsecdT(:,:)     = 0.0d0
     XsecS%XsecConv(:,:)    = 0.0d0
     XsecS%dXsecConvdT(:,:) = 0.0d0

     call getAbsorptionXsecFromLUT(wavelHRS, gasPTS, traceGasS, XsecS, XsecLUTS)

     ! convolute absorption cross section with slit function
     if ( controlS%useEffXsec_OE ) then
       ! convolute absorption cross section with slit function

       if ( present(wavelInstrS) .and. present(solarIrrS) .and. present(earthRadS) .and. present(wavelHRS) ) then

         call convoluteXsecLUT(errS, wavelHRS, wavelInstrS, solarIrrS, earthRadS, XsecS)
         if (errorCheck(errS)) return

         ! multiply with scale factor
         XsecS%XsecConv(:,:)    = traceGasS%scaleFactorXsec * XsecS%XsecConv(:,:)
         XsecS%dXsecConvdT(:,:) = traceGasS%scaleFactorXsec * XsecS%dXsecConvdT(:,:)

       else
         call logDebug('ERROR in subroutine getAbsorptionXsec')
         call logDebug('in module propAtmosphere')
         call logDebug('getAbsorptionXsec is not called with optional parameters')
       end if ! present ......

     else

       XsecS%XsecConv(:,:)    = 0.0d0
       XsecS%dXsecConvdT(:,:) = 0.0d0

     end if ! controlS%useEffXsec_OE

     if ( controlS%ignoreSlit ) then
       numwavel = wavelInstrS%nwavel
     else
       numwavel = wavelMRS%nwavel
     end if

     ! negative values can occur due to spline interpolation, e.g. for ozone around 380 nm
     ! because the tabulated values of Bass and Paur end at 365 nm and values begin again 407 nm
     ! set negative values to zero

     if ( controlS%ignoreSlit ) then
       numwavel = wavelInstrS%nwavel
     else
       numwavel = wavelMRS%nwavel
     end if
     do ialt = 0, traceGasS%nalt
       do iwave = 1, numwavel
         if ( XsecS%Xsec(iwave, ialt) < 0.0d0 )  XsecS%Xsec(iwave, ialt) = 0.0d0
       end do
     end do

     where ( XsecS%XsecConv < 0.0d0 ) XsecS%XsecConv = 0.0d0

     if ( controlS%writeAbsorptionXsec ) then
       if ( trim(traceGasS%nameTraceGas) == 'O2-O2' ) then
         unit_abs_Xsec = 'unit is cm^5 / molecule^2'
       else
         unit_abs_Xsec = 'unit is cm^2 / molecule'
       end if
       if ( controlS%useEffXsec_OE ) then
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit, *) 'CONVOLUTED ABSORPTION CROSS SECTIONS as function of wavelength on instr grid'
         write(addtionalOutputUnit, *) 'for different pressure levels in the atmosphere calculated using'
         write(addtionalOutputUnit, *) 'the expansion in Legendre polynomials'
         write(addtionalOutputUnit, *) trim(unit_abs_Xsec)
         write(addtionalOutputUnit, *) 'convolution with slit function weigted with solar irradiance (I0-effect) is used'
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit, *) 'filename                   gasIndex  ISO values'
         write(addtionalOutputUnit, '(A,10I4)') trim(XsecS%XsectionFileName), XsecS%gasIndex,  &
                                                   ( XsecS%ISO(iISO), iISO = 1, XsecS%nISO )
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') 'altitude(km)', &
                                              ('    z =', traceGasS%alt(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                              ('    p =',traceGasS%pressure(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                              ('    T =',traceGasS%temperature(ialt), ialt = 0, traceGasS%nalt)
         do iwave = 1, wavelInstrS%nwavel
           write(addtionalOutputUnit,'(F12.5, 100ES15.7)') wavelInstrS%wavel(iwave), &
                                                    (XsecS%XsecConv(iwave,ialt), ialt = 0, traceGasS%nalt)
         end do

       else

         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit, *) 'ABSORPTION CROSS SECTIONS as function of wavelength on HR grid'
         write(addtionalOutputUnit, *) 'for different pressure levels in the atmosphere calculated using'
         write(addtionalOutputUnit, *) 'the expansion in Legendre polynomials'
         write(addtionalOutputUnit, *) trim(unit_abs_Xsec)
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit, *) 'filename                   gasIndex  ISO values'
         write(addtionalOutputUnit, '(A,10I4)') trim(XsecS%XsectionFileName), XsecS%gasIndex,  &
                                                   ( XsecS%ISO(iISO), iISO = 1, XsecS%nISO )
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') 'altitude(km)', &
                                              ('    z =', traceGasS%alt(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                              ('    p =',traceGasS%pressure(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                              ('    T =',traceGasS%temperature(ialt), ialt = 0, traceGasS%nalt)
         do iwave = 1, wavelMRS%nwavel
           write(addtionalOutputUnit,'(F12.5, 100ES15.7)') wavelMRS%wavel(iwave), &
                                                    (XsecS%Xsec(iwave,ialt), ialt = 0, traceGasS%nalt)
         end do

       end if ! controlS%useEffXsec_OE

       if ( gasPTS%fitTemperatureProfile .or. gasPTS%fitTemperatureOffset ) then
         write(addtionalOutputUnit, *)
         write(addtionalOutputUnit,'(A)') 'temperature derivative of the absorption cross secctions'
         write(addtionalOutputUnit,'(A)') 'calculated using the expansion in Legendre polynomials'
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') 'altitude(km)', &
                                            ('    z =', traceGasS%alt(ialt), ialt = 0, traceGasS%nalt)
         write(addtionalOutputUnit,'(A,100(A7,F8.3))') '            ', &
                                            ('    p =',traceGasS%pressure(ialt), ialt = 0, traceGasS%nalt)
         do iwave = 1, wavelMRS%nwavel
           write(addtionalOutputUnit,'(F12.5, 100ES15.7)') wavelMRS%wavel(iwave), &
                                                    (XsecS%dXsecdT(iwave,ialt), ialt = 0, traceGasS%nalt)
         end do
       end if

     end if  ! controlS%writeAbsorptionXsec
     call exit('getAbsorptionXsecUsingLUT')

    end subroutine getAbsorptionXsecUsingLUT


    subroutine getAbsorptionXsecFromLUT(wavelHRS, gasPTS, traceGasS, XsecS, XsecLUTS)

      ! calculate the absorption cross section using the expansion coefficients for temperature and pressure
      ! the expansion coefficients are stored in XsecLUTS
      ! the absorption coefficients are stored in XsecS
      ! if the derivative w.r.t. the temperature is needed XsecS%dXsecdT is also calculated.

      implicit none

      type(wavelHRType),       intent(in)    :: wavelHRS              ! high resolution wavelength grid
      type(gasPTType),         intent(in)    :: gasPTS                ! pressure and temperature
      type(traceGasType),      intent(inout) :: traceGasS             ! atmospheric trace gas properties
      type(XsecType),          intent(inout) :: XsecS                 ! absorption cross sections
      type(XsecLUTType),       intent(inout) :: XsecLUTS              ! LUT for expansion coefficients

      ! local
      integer  :: ipressure, iTemp, iwave, icoeff_lnp, icoeff_lnT
      integer  :: ncoeff_lnT, ncoeff_lnp
      integer  :: nPressure, nTemp
      real(8)  :: a, b
      real(8)  :: legendre_lnT(XsecLUTS%ncoeff_lnT_LUT, 0:traceGasS%nalt)
      real(8)  :: legendre_lnp(XsecLUTS%ncoeff_lnp_LUT, 0:traceGasS%nalt)
      real(8)  :: derivative_legendre_lnT(XsecLUTS%ncoeff_lnT_LUT, 0:traceGasS%nalt)
      real(8)  :: scaled_lnT(0:traceGasS%nalt), scaled_lnp(0:traceGasS%nalt)
      real(8)  :: lnT(0:traceGasS%nalt), lnp(0:traceGasS%nalt)
      real(8)  :: lnpmax, lnpmin, lnTmax, lnTmin

      logical, parameter :: verbose = .true.

      call enter('getAbsorptionXsecFromLUT')

      ! calculate the absorption cross section at the temperature, pressure,
      ! and wavelength test grid using the expansion in Legendre polynomials

      lnpmax = log(XsecLUTS%pmax_LUT)
      lnpmin = log(XsecLUTS%pmin_LUT)
      lnTmax = log(XsecLUTS%Tmax_LUT)
      lnTmin = log(XsecLUTS%Tmin_LUT)

      nTemp     = XsecLUTS%nTemp_LUT
      nPressure = XsecLUTS%nPressure_LUT

      ncoeff_lnT = XsecLUTS%ncoeff_lnT_LUT
      ncoeff_lnp = XsecLUTS%ncoeff_lnp_LUT

      ! first calculate scaled values for temperature and pressure
      lnp(:) = log(traceGasS%pressure(:))
      a = -(lnpmax + lnpmin) / (lnpmax - lnpmin)
      b = 2.0d0 / (lnpmax - lnpmin)
      scaled_lnp(:) = a + b * lnp(:)

      lnT(:) = log(traceGasS%temperature(:))
      a = -(lnTmax + lnTmin) / (lnTmax - lnTmin)
      b = 2.0d0 / (lnTmax - lnTmin)
      scaled_lnT(:) = a + b * lnT(:)

      ! calculate values for the Legendre functions
      do iTemp = 0, traceGasS%nalt
        legendre_lnT(:, iTemp) = fleg(scaled_lnT(iTemp), ncoeff_lnT)
      end do ! iTemp

      do ipressure = 0, traceGasS%nalt
        legendre_lnp(:, ipressure) = fleg(scaled_lnp(ipressure), ncoeff_lnp)
      end do ! ipressure

      ! calculate the absorption cross section for the different pressure levels
      do iwave = 1, wavelHRS%nwavel
        do ipressure = 0, traceGasS%nalt
          XsecS%Xsec(iwave,ipressure) = 0.0d0
          do icoeff_lnp = 1, ncoeff_lnp
            do icoeff_lnT = 1, ncoeff_lnT
              XsecS%Xsec(iwave,ipressure)  =  XsecS%Xsec(iwave,ipressure)  &
                  + XsecLUTS%coeff_lnTlnp_LUT(icoeff_lnT, icoeff_lnp, iwave) &
                  * legendre_lnT(icoeff_lnT, ipressure) * legendre_lnp(icoeff_lnp, ipressure)
            end do ! icoeff_lnT
          end do ! icoeff_lnp
        end do ! ipressure
      end do ! iwave

      if ( gasPTS%fitTemperatureProfile .or. gasPTS%fitTemperatureOffset ) then

        ! calculate temperature derivative if it is required
        !   Let Pk(T'') be the Legendre polynomial k with argument T'' where
        !   T'' is logarithm of T scaled to (-1,+1)
        !   dXsec/dT follows from
        !    dXsec/dT = sum_k[sum_j{ckj*Pj(p'') * dPk(T'')/dT}]  or
        !    dXsec/dT = sum_k[sum_j{ckj*Pj(p'') * dPk(T'')/dT'' * dT''/dT}]
        !
        !    for dPk(T'')/dT'' we have the recurrence relation
        !
        !    dPk+1(T'')/dT''= T'' * dPk(T'')/dT'' + (k+1)*Pk(T'')
        !    with starting values P0(T'') = 1; P1(T'') = T''
        !                         dP0(T'')/dT'' = 0; dP1(T'')/dT''= 1
        !
        !   and
        !
        !   dT''/dT = 2/[ {ln(Tmax) - ln(Tmin} * T ]

        ! first calculate the derivative of the Legendre funtion
        do iTemp = 0, traceGasS%nalt
          ! factor a is due to scaling the logarithm of the temperature
          a =  2.0d0 / (lnTmax - lnTmin) / traceGasS%temperature(iTemp)
          derivative_legendre_lnT(:, iTemp) = 0.0d0
          if ( ncoeff_lnT > 1 ) then
            do icoeff_lnT = 2, ncoeff_lnT
              derivative_legendre_lnT(icoeff_lnT, iTemp) =                              &
                  scaled_lnT(iTemp) * derivative_legendre_lnT(icoeff_lnT - 1, iTemp)    &
                + icoeff_lnT * legendre_lnT(icoeff_lnT - 1, iTemp)
              ! scale with a
              derivative_legendre_lnT(icoeff_lnT, iTemp) = a * derivative_legendre_lnT(icoeff_lnT, iTemp)
            end do ! icoeff_lnT
          end if ! ncoeff_lnT_LUT > 1
        end do ! iTemp

        ! then calculate the derivative of the absorption cross section with respect to the temperature

        do iwave = 1, wavelHRS%nwavel
          do ipressure = 0, traceGasS%nalt
              XsecS%dXsecdT(iwave,ipressure) = 0.0d0
              do icoeff_lnp = 1, ncoeff_lnp
                do icoeff_lnT = 1, ncoeff_lnT
                 XsecS%dXsecdT(iwave, ipressure)  = XsecS%dXsecdT(iwave,ipressure)  &
                     + XsecLUTS%coeff_lnTlnp_LUT(icoeff_lnT, icoeff_lnp, iwave)     &
                     * derivative_legendre_lnT(icoeff_lnT, ipressure)  * legendre_lnp(icoeff_lnp, ipressure)
                end do ! icoeff_lnT
              end do ! icoeff_lnp
            end do ! ipressure
        end do ! iwave

      else

        XsecS%dXsecdT = 0.0d0

      end if ! gasPTS%fitTemperatureProfile .or. gasPTS%fitTemperatureOffset

      XsecS%corrFactorAMF = 0.0d0

      call exit('getAbsorptionXsecFromLUT')
    end subroutine getAbsorptionXsecFromLUT


    subroutine createXsecLUT (errS, staticS, controlS, wavelHRS, weakAbsRetrS, gasPTS, traceGasS, XsecS, XsecLUTS)

      ! The absorption coefficient depends in general on the wavelength, pressure and temperature.
      ! The temperature and pressure dependence can be expanded in a sum over expansion coefficients
      ! and orthogonal Legendre coefficients, so that
      !
        !XsecLUTS%Xsec_expansion(iTemp,ipressure,iwave) = 0.0d0
        !do icoeff_lnp = 0, XsecLUTS%ncoeff_lnp
        !  do icoeff_lnT = 0, XsecLUTS%ncoeff_lnT
        !    XsecLUTS%Xsec(iTemp,ipressure,iwave)  =  XsecLUTS%Xsec(iTemp,ipressure,iwave)  &
        !       + XsecLUTS%coeff_lnTlnp_LUT(icoeff_lnT, icoeff_lnp, iwave) &
        !       * XsecLUTS%Legendre_lnT(icoeff_lnT, iTemp)             &
        !       * XsecLUTS%Legendre_lnp(icoeff_lnp, ipressure)
        !  end do ! icoeff_lnT
        ! end do ! icoeff_lnp
      !
      ! gives the absorption cross section for any temperature and pressure within a certain
      ! temperature and pressure interval: (Tmin, Tmax) and (pmin,pmax). As these intervals are
      ! rather large, the logarithms of T and p are used for the calculation of the coefficients.
      ! Further, the intervals are scaled to (-1,+1) as the Legendre functions are defined on this
      ! interval.

      implicit none

      type(errorType),         intent(inout) :: errS
      type(staticType),        intent(in)    :: staticS
      type(controlType),       intent(in)    :: controlS              ! controls writing of absorption cross sections
      type(wavelHRType),       intent(in)    :: wavelHRS              ! high resolution wavelength grid
      type(weakAbsType),       intent(in)    :: weakAbsRetrS          ! flags and values for an isothermal atmosphere
      type(gasPTType),         intent(in)    :: gasPTS                ! pressure and temperature
      type(traceGasType),      intent(inout) :: traceGasS             ! atmospheric trace gas properties
      type(XsecType),          intent(inout) :: XsecS                 ! absorption cross sections
      type(XsecLUTType),       intent(inout) :: XsecLUTS              ! LUT for expansion coefficients


      ! local
      integer  :: ipressure, iTemp, iwave, icoeff_lnp, icoeff_lnT
      integer  :: ncoeff_lnT, ncoeff_lnp
      integer  :: nPressure, nTemp
      real(8)  :: a, b
      real(8)  :: legendre_lnT(XsecLUTS%ncoeff_lnT_LUT, XsecLUTS%nTemp_LUT)
      real(8)  :: legendre_lnp(XsecLUTS%ncoeff_lnp_LUT, XsecLUTS%nPressure_LUT)
      real(8)  :: scaled_lnT(XsecLUTS%nTemp_LUT), scaled_lnp(XsecLUTS%nPressure_LUT)
      real(8)  :: weight_scaled_lnT(XsecLUTS%nTemp_LUT), weight_scaled_lnp(XsecLUTS%nPressure_LUT)
      real(8)  :: lnT(XsecLUTS%nTemp_LUT), lnp(XsecLUTS%nPressure_LUT)
      real(8)  :: T(XsecLUTS%nTemp_LUT), p(XsecLUTS%nPressure_LUT)
      real(8)  :: Xsec(XsecLUTS%nTemp_LUT, XsecLUTS%nPressure_LUT, wavelHRS%nwavel)
      !real(8)  :: coeff_lnT(XsecLUTS%nTemp_LUT, XsecLUTS%nPressure_LUT, wavelHRS%nwavel)
      !real(8)  :: coeff_lnTlnp(XsecLUTS%nTemp_LUT, XsecLUTS%nPressure_LUT, wavelHRS%nwavel)   ! dimension is wrong  P.W. 27 Jan. 2022
      
      real(8)  :: coeff_lnT(XsecLUTS%ncoeff_lnT_LUT, XsecLUTS%nPressure_LUT, wavelHRS%nwavel)
      real(8)  :: coeff_lnTlnp(XsecLUTS%ncoeff_lnT_LUT, XsecLUTS%ncoeff_lnp_LUT, wavelHRS%nwavel)   ! correct dimension  P.W. 27 Jan. 2022
      
      real(8)  :: lnpmax, lnpmin, lnTmax, lnTmin

      type(XsecType)    :: XsecLocalS
      type(traceGasType):: traceGasLocalS             ! atmospheric trace gas properties


      logical, parameter :: verbose = .true.

      call enter('createXsecLUT')

      ! use the logarithm of pressure and temperature for the approximation with Legendre functions
      lnpmax = log(XsecLUTS%pmax_LUT)
      lnpmin = log(XsecLUTS%pmin_LUT)
      lnTmax = log(XsecLUTS%Tmax_LUT)
      lnTmin = log(XsecLUTS%Tmin_LUT)

      nTemp     = XsecLUTS%nTemp_LUT
      nPressure = XsecLUTS%nPressure_LUT

      ncoeff_lnT = XsecLUTS%ncoeff_lnT_LUT
      ncoeff_lnp = XsecLUTS%ncoeff_lnp_LUT


      ! copy values to the local data structure, except for nalt
      ! these values are input for call getAbsorptionXsec
      XsecLocalS%useHITRAN        = XsecS%useHITRAN
      XsecLocalS%XsectionFileName = XsecS%XsectionFileName
      XsecLocalS%gasIndex         = XsecS%gasIndex
      XsecLocalS%nISO             = XsecS%nISO
      XsecLocalS%ISO(:)           = XsecS%ISO(:)
      XsecLocalS%factorLM         = XsecS%factorLM
      XsecLocalS%cutoff           = XsecS%cutoff
      XsecLocalS%thresholdLine    = XsecS%thresholdLine
      XsecLocalS%nwavelHR         = XsecS%nwavelHR
      XsecLocalS%nwavel           = XsecS%nwavel
      XsecLocalS%corrFactorAMF    = XsecS%corrFactorAMF
      XsecLocalS%nalt             = nPressure - 1

      ! only a few values in the data structure  traceGasLocalS are needed
      ! to calculate the absoption cross section
      traceGasLocalS%scaleFactorXsec = traceGasS%scaleFactorXsec
      traceGasLocalS%nameTraceGas    = traceGasS%nameTraceGas
      traceGasLocalS%nalt            = nPressure - 1      ! ialt = 0: nalt  while ipressure = 1: nPressure

      ! deallocate and allocate arrays in local data structures
      call claimMemXsecS(errS, XsecLocalS)
      call claimMemTraceGasS(errS, traceGasLocalS)

      ! fill pressure grids for creating the expansion LUT
      call  gaussDivPoints(errS, -1.0d0, 1.0d0, scaled_lnp, weight_scaled_lnp, nPressure)
      a = ( lnpmax + lnpmin ) * 0.5d0
      b = ( lnpmax - lnpmin ) * 0.5d0
      lnp(:) = a + b * scaled_lnp(:)
      p(:) = exp(lnp(:))

      ! fill temperature grid for creating the expansion LUT
      call  gaussDivPoints(errS, -1.0d0, 1.0d0, scaled_lnT, weight_scaled_lnT, nTemp)
      a = ( lnTmax + lnTmin ) * 0.5d0
      b = ( lnTmax - lnTmin ) * 0.5d0
      lnT(:) = a + b * scaled_lnT(:)
      T(:) = exp(lnT(:))

      ! fill Legendre values for temperature and pressure
      !FUNCTION fleg(x,nl)
      ! fleg(1,2,....,nl) contains P0(x), P1(x), etc
      ! where Pk(x) is the Legendre function
      do iTemp = 1, nTemp
        legendre_lnT(:, iTemp) = fleg(scaled_lnT(iTemp), ncoeff_lnT)
      end do ! iTemp
      do ipressure = 1, nPressure
        legendre_lnp(:, ipressure) = fleg(scaled_lnp(ipressure), ncoeff_lnp)
      end do ! ipressure

      ! When calculating the absorption cross section the values for temperature and pressure
      ! are taken from the values stored in traceGasS%temperature(ipressure) and traceGasS%pressure(ipressure).
      ! the absorption cross sections are then stored in XsecS%Xsec(nwavel, nPressure)
      ! Because the p-T relation is fixed only results for various pressure levels are clculated.

      ! initialize
      XsecLocalS%Xsec = 0.0d0
      traceGasLocalS%pressure(:)    = p(:)

      ! we skip the calculation of the altitudes corresponding to the pressure levels used for the LUT
      traceGasLocalS%alt(:)         = 0.0d0

      ! calculate the absorption cross section at the temperature, pressure, and wavelength grid
      ! needed for the calculation of the expansion coefficients
      do iTemp = 1, nTemp
        traceGasLocalS%temperature(:) =T(iTemp)
        call getAbsorptionXsec(errS, staticS, controlS, weakAbsRetrS, wavelHRS, gasPTS, traceGasLocalS, XsecLocalS)
        do iwave = 1, wavelHRS%nwavel
          do ipressure = 1, nPressure
            Xsec(iTemp, ipressure, iwave) = XsecLocalS%Xsec(iwave, ipressure - 1)
          end do ! ipressure
        end do ! iwave
      end do ! iTemp

      ! calculate the expansion coefficients for the temperature,  coeff_T_LUT(0:ncoeff_T, nPressure_LUT, nwavel)
      do iwave = 1, wavelHRS%nwavel
        do ipressure = 1, nPressure
          do icoeff_lnT = 1, ncoeff_lnT
            coeff_lnT(icoeff_lnT, ipressure, iwave) = 0.0d0
            do iTemp = 1, nTemp
              coeff_lnT(icoeff_lnT, ipressure, iwave) = coeff_lnT(icoeff_lnT, ipressure, iwave) +                            &
                    weight_scaled_lnT(iTemp) * Xsec(iTemp, ipressure, iwave) * legendre_lnT(icoeff_lnT, iTemp)
            end do ! iTemp
            coeff_lnT(icoeff_lnT, ipressure, iwave) = &
                  coeff_lnT(icoeff_lnT, ipressure, iwave) * (2.0d0 * icoeff_lnT - 1.0d0) / 2.0d0
          end do ! icoeff_T
        end do ! ipressure
      end do ! iwave

      ! expand the coefficients for the temperature in Legendre polynomials and calculate the coefficients
      ! coeff_Tlnp_LUT(0:ncoeff_T, 0:ncoeff_lnp, nwavel)
      do iwave = 1, wavelHRS%nwavel
        do icoeff_lnT = 1, ncoeff_lnT
          do icoeff_lnp = 1, ncoeff_lnp
            coeff_lnTlnp(icoeff_lnT, icoeff_lnp, iwave) = 0.0d0
            do ipressure = 1, nPressure
              coeff_lnTlnp(icoeff_lnT, icoeff_lnp, iwave) = coeff_lnTlnp(icoeff_lnT, icoeff_lnp, iwave) +   &
                    weight_scaled_lnp(ipressure) * coeff_lnT(icoeff_lnT, ipressure, iwave) * &
                    Legendre_lnp(icoeff_lnp, ipressure)
            end do ! ipressure
            coeff_lnTlnp(icoeff_lnT, icoeff_lnp, iwave) = &
                 coeff_lnTlnp(icoeff_lnT, icoeff_lnp, iwave) *  (2.0d0 * icoeff_lnp - 1.0d0) / 2.0d0
          end do ! icoeff_lnp
        end do ! icoeff_lnT
      end do ! iwave

      ! store coefficients in data structure
      
      write(*,*) 'ncoeff_lnT, ncoeff_lnp =', ncoeff_lnT, ncoeff_lnp
      write(*,*) 'size XsecLUTS%coeff_lnTlnp_LUT', size(XsecLUTS%coeff_lnTlnp_LUT,1), &
                                                                 size(XsecLUTS%coeff_lnTlnp_LUT,2), &
                                                                 size(XsecLUTS%coeff_lnTlnp_LUT,3) 
      write(*,*) 'coeff_lnTlnp', size(coeff_lnTlnp,1), size(coeff_lnTlnp,2), size(coeff_lnTlnp,3)
   
      XsecLUTS%coeff_lnTlnp_LUT = coeff_lnTlnp


      if (verbose) then
        write(intermediateFileUnit,'(A)')
        write(intermediateFileUnit,'(A)') 'expansion coefficients for the absorption cross section'
        write(intermediateFileUnit,'(A)') 'based on date in the file'
        write(intermediateFileUnit, '(A)') trim(XsecS%XsectionFileName)
        write(intermediateFileUnit,'(A,F10.4)') 'Tmin = ', XsecLUTS%Tmin_LUT
        write(intermediateFileUnit,'(A,F10.4)') 'Tmax = ', XsecLUTS%Tmax_LUT
        write(intermediateFileUnit,'(A,F10.4)') 'pmin = ', XsecLUTS%pmin_LUT
        write(intermediateFileUnit,'(A,F10.4)') 'pmax = ', XsecLUTS%pmax_LUT
        write(intermediateFileUnit,'(A)')
        do iwave = 1, wavelHRS%nwavel
          write(intermediateFileUnit,*)
          write(intermediateFileUnit,'(A, F10.5)') 'expansion coefficients for wavelength = ', wavelHRS%wavel(iwave)
          do icoeff_lnT = 1, ncoeff_lnT
            write(intermediateFileUnit,'(30ES15.5)') &
               (coeff_lnTlnp(icoeff_lnT, icoeff_lnp, iwave), icoeff_lnp = 1, ncoeff_lnp)
          end do ! icoeff_lnT
        end do ! iwave
      end if ! verbose

      ! free allocated (local) memory
      call freeMemXsecS(errS, XsecLocalS)
      call freeMemTraceGasS(errS, traceGasLocalS)

      !call logDebug('finished calculating expansion coefficients')
      !call mystop(errS, 'stopped because the expansion coefficients have been calculated')
      !if (errorCheck(errS)) return

9999  continue
      call exit('createXsecLUT')
    end subroutine createXsecLUT


    subroutine convoluteXsec(errS, wavelHRS, wavelInstrS, solarIrrS, earthRadS,        &
                             Xsec0, Xsec1, Xsec2, Xsec0Conv, Xsec1Conv, Xsec2Conv)

      ! convolute Xsec0, Xsec1, Xsec2 with the slit function weighted by the solar irradiance
      !
      implicit none

      type(errorType),         intent(inout) :: errS
      type(wavelHRType),       intent(in)    :: wavelHRS           ! high resolution wavelength grid
      type(wavelInstrType),    intent(in)    :: wavelInstrS        ! wavelength grid for retrieval
      type(SolarIrrType),      intent(in)    :: solarIrrS          ! solar irradiance
      type(earthRadianceType), intent(inout) :: earthRadS          ! earth radiance; info slit function
      real(8),                 intent(in)    :: Xsec0(wavelHRS%nwavel)
      real(8),                 intent(in)    :: Xsec1(wavelHRS%nwavel)
      real(8),                 intent(in)    :: Xsec2(wavelHRS%nwavel)
      real(8),                 intent(out)   :: Xsec0Conv(wavelInstrS%nwavel)
      real(8),                 intent(out)   :: Xsec1Conv(wavelInstrS%nwavel)
      real(8),                 intent(out)   :: Xsec2Conv(wavelInstrS%nwavel)

      ! local
      real(8) :: slitfunctionValues(wavelHRS%nwavel)
      real(8) :: solarIrr(wavelInstrS%nwavel)
      integer :: iwave
      integer :: startIndex, endIndex, index

      ! normal expressions for weak absorption

        do iwave = 1, wavelInstrS%nwavel
          slitfunctionValues = slitfunction(errS, wavelHRS, iwave, wavelInstrS%nwavel, wavelInstrS%wavel,  &
                                            earthRadS%slitFunctionSpecsS, startIndex, endIndex)
          ! multiply with gaussian weights for wavelength integration
          do index = startIndex, endIndex
            slitfunctionValues(index) = wavelHRS%weight(index) * slitfunctionValues(index)
            ! multiply with solar irradiance for I0 effect
            slitfunctionValues(index) = slitfunctionValues(index) * solarIrrS%solIrrHR(index)
          end do ! index
          Xsec0Conv(iwave) = 0.0d0
          Xsec1Conv(iwave) = 0.0d0
          Xsec2Conv(iwave) = 0.0d0
          solarIrr (iwave) = 0.0d0
          do index = startIndex, endIndex
            Xsec0Conv(iwave) = Xsec0Conv(iwave) + Xsec0(index) * slitfunctionValues(index)
            Xsec1Conv(iwave) = Xsec1Conv(iwave) + Xsec1(index) * slitfunctionValues(index)
            Xsec2Conv(iwave) = Xsec2Conv(iwave) + Xsec2(index) * slitfunctionValues(index)
            solarIrr (iwave) = solarIrr (iwave) + slitfunctionValues(index)
          end do ! index
          Xsec0Conv(iwave) = Xsec0Conv(iwave) / solarIrr(iwave)
          Xsec1Conv(iwave) = Xsec1Conv(iwave) / solarIrr(iwave)
          Xsec2Conv(iwave) = Xsec2Conv(iwave) / solarIrr(iwave)
        end do ! iwave

    end subroutine convoluteXsec


    subroutine convoluteXsecLUT(errS, wavelHRS, wavelInstrS, solarIrrS, earthRadS, XsecS)

      ! Convolute Xsec with the slit function weighted by the solar irradiance (I0 effect)
      ! In convoluteXsec we convolute the three temperature coefficients for non-line absorbing
      ! species which is efficient as we do not have to repeat the convolution for all pressure levels.
      ! Here we convolute not the expansion coefficients but the absorption coeffcients at all
      ! pressure levels, as the number of expansion coefficients may be larger than the number
      ! of pressure levels.

      implicit none

      type(errorType),         intent(inout) :: errS
      type(wavelHRType),       intent(in)    :: wavelHRS           ! high resolution wavelength grid
      type(wavelInstrType),    intent(in)    :: wavelInstrS        ! wavelength grid for retrieval
      type(SolarIrrType),      intent(in)    :: solarIrrS          ! solar irradiance
      type(earthRadianceType), intent(inout) :: earthRadS          ! earth radiance; info slit function
      type(XsecType),          intent(inout) :: XsecS              ! absorption cross sections

      ! local
      real(8) :: slitfunctionValues(wavelHRS%nwavel)
      real(8) :: solarIrr(wavelInstrS%nwavel)
      integer :: iwave, iPressure
      integer :: startIndex, endIndex, index

        do iwave = 1, wavelInstrS%nwavel
          slitfunctionValues = slitfunction(errS, wavelHRS, iwave, wavelInstrS%nwavel, wavelInstrS%wavel,  &
                                            earthRadS%slitFunctionSpecsS, startIndex, endIndex)
          ! multiply with gaussian weights for wavelength integration
          do index = startIndex, endIndex
            slitfunctionValues(index) = wavelHRS%weight(index) * slitfunctionValues(index)
            ! multiply with solar irradiance for I0 effect
            slitfunctionValues(index) = slitfunctionValues(index) * solarIrrS%solIrrHR(index)
          end do ! index

          solarIrr (:) = 0.0d0
          XsecS%XsecConv(:, :) = 0.0d0
          do index = startIndex, endIndex
            do iPressure = 0, XsecS%nalt
              XsecS%XsecConv(iwave, iPressure) = XsecS%XsecConv(iwave, iPressure)  &
                                               + XsecS%Xsec(index, iPressure) * slitfunctionValues(index)
            end do ! iPressure
            ! note that the slitfunctionValues are multiplied by the solar irradiance
            solarIrr (iwave) = solarIrr (iwave) + slitfunctionValues(index)
          end do ! index
          do iPressure = 0, XsecS%nalt
            XsecS%XsecConv(iwave, iPressure) = XsecS%XsecConv(iwave, iPressure) / solarIrr(iwave)
          end do ! iPressure
        end do ! iwave

    end subroutine convoluteXsecLUT


    subroutine getOptPropAtm (errS, cloudyPart, iwave, nTrace, wavelHRS, XsecS, RRS_RingS,       &
                              controlS, gasPTS, traceGasS, surfaceS, LambCloudS, mieAerS,  &
                              mieCldS, cloudAerosolRTMgridS, OptPropRTMGridS,              &
                              surfAlb_iwave, surfEmission_iwave, cloudAlb_iwave, depol)

    ! input:  iwave, wavelengthS, XsecS, parts of the structures gasPTS, traceGasS, surfaceS,
    !         cloudAerosolRTMgridS, and OptPropRTMGridS
    ! output: parts of the structure OptPropRTMGridS:
    !              on the RTM grid:         ksca, kabs, phasefCoef
    !              on the sublayer grid:    kabsSub
    !              for the RTM grid layers: opticalthicknLay, ssaLay, phasefCoefLay
    !         values for surface and cloud albedo: surfAlb_iwave, cloudAlb_iwave

    implicit none

    type(errorType),               intent(inout) :: errS
    logical,                       intent(in)    :: cloudyPart ! flag to denote that we deal with the cloudy part
    integer,                       intent(in)    :: iwave      ! wavelength index
    integer,                       intent(in)    :: nTrace     ! number of trace gases

    type(wavelHRType),             intent(in)    :: wavelHRS                  ! high resolution wavelength grid
    type(XsecType),                intent(in)    :: XsecS(nTrace)             ! absorption cross section (lbl)
    type(RRS_RingType),            intent(in)    :: RRS_RingS                 ! Raman / Ring information

    type(controlType),             intent(inout) :: controlS                  ! control parameters
    type(gasPTType),               intent(in)    :: gasPTS                    ! specification of the gaseous atmosphere
    type(traceGasType),            intent(in)    :: traceGasS(nTrace)         ! specification of trace gases
    type(LambertianType),          intent(in)    :: surfaceS                  ! specification of the surface properties
    type(LambertianType),          intent(in)    :: LambCloudS                ! specification of the Lambertian cloud
    type(mieScatType),             intent(in)    :: mieAerS(maxNumMieModels)  ! specification expansion coefficients
    type(mieScatType),             intent(in)    :: mieCldS(maxNumMieModels)  ! specification expansion coefficients
    type(cloudAerosolRTMgridType), intent(inout) :: cloudAerosolRTMgridS      ! cloud-aerosol properties
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS           ! optical properties on RTM grid (simulation)
    real(8),                       intent(out)   :: surfAlb_iwave             ! surface albedo for iwave
    real(8),                       intent(out)   :: surfEmission_iwave        ! surface emission for iwave
    real(8),                       intent(out)   :: cloudAlb_iwave            ! cloud albedo for iwave
    real(8),                       intent(out)   :: depol                     ! depolarization factor

    ! local
    integer  :: icoef, ilevel, ibound, igauss, igaussSub, index, indexSub, iTrace, imodel, iExpCoef
    integer  :: status

    ! tempory arrays for interpolation
    real(8)  :: lnpressureGrid(0:gasPTS%npressure)
    real(8)  :: SDlnpressure  (0:gasPTS%npressure)
    real(8)  :: SDtemperature (0:gasPTS%npressure)

    ! tempory arrays for interpolation; here the length depends on the trace gas
    real(8), allocatable  :: lnvmrTraceGrid(:)
    real(8), allocatable  :: lnvmrTraceGridAP(:)
    real(8), allocatable  :: SDlnvmrTrace(:)
    real(8), allocatable  :: SDlnvmrTraceAP(:)
    real(8), allocatable  :: XsecNodes(:)
    real(8), allocatable  :: SDXsecNodes(:)
    real(8), allocatable  :: dXsecdTNodes(:)
    real(8), allocatable  :: SDdXsecdTNodes(:)

    ! arrays on sublayer grid (p in hPa, T in K, vmr in ppmv, ndens in mol cm-3)
    real(8)  :: pressure(0:optPropRTMGridS%RTMnlayerSub)
    real(8)  :: lnpressure(0:optPropRTMGridS%RTMnlayerSub)           ! natural logarithm of pressure
    real(8)  :: temperature(0:optPropRTMGridS%RTMnlayerSub)
    real(8)  :: vmrTrace(0:optPropRTMGridS%RTMnlayerSub,nTrace)
    real(8)  :: lnvmrTrace(0:optPropRTMGridS%RTMnlayerSub,nTrace)    ! natural logarithm of volume mixing ratio
    real(8)  :: XsecTrace(0:optPropRTMGridS%RTMnlayerSub,nTrace)     ! cross section on RTM sub grid
    real(8)  :: dXsecdTTrace(0:optPropRTMGridS%RTMnlayerSub,nTrace)     ! cross section on RTM sub grid
    real(8)  :: numberDensityAir(0:optPropRTMGridS%RTMnlayerSub)

    ! arrays on the column altitude grid (p in hPa, T in K, vmr in ppmv, ndens in mol cm-3)
    real(8)  :: pressureCol(optPropRTMGridS%nGaussCol)
    real(8)  :: lnpressureCol(optPropRTMGridS%nGaussCol)                  ! natural logarithm of pressure
    real(8)  :: temperatureCol(optPropRTMGridS%nGaussCol)
    real(8)  :: vmrTraceCol(optPropRTMGridS%nGaussCol,nTrace)
    real(8)  :: lnvmrTraceCol(optPropRTMGridS%nGaussCol,nTrace) ! natural logarithm of volume mixing ratio column grid
    real(8)  :: vmrTraceAPCol(optPropRTMGridS%nGaussCol,nTrace)
    real(8)  :: lnvmrTraceAPCol(optPropRTMGridS%nGaussCol,nTrace) ! natural logarithm of volume mixing ratio column grid
    real(8)  :: XsecCol(optPropRTMGridS%nGaussCol,nTrace)       ! cross section on column grid (cm2 per molecule)
    real(8)  :: numberDensityAirCol(optPropRTMGridS%nGaussCol)           ! number density of air on column grid
    real(8)  :: numberDensityTraceCol(optPropRTMGridS%nGaussCol,nTrace)  ! number density of trace gas on column grid
    real(8)  :: numberDensityTraceAPCol(optPropRTMGridS%nGaussCol,nTrace)  ! number density of trace gas on column grid

    ! properties of the gaseous atmosphere on the sublayer grid
    real(8)  :: kscaGas(0:optPropRTMGridS%RTMnlayerSub)
    real(8)  :: kabsGas(0:optPropRTMGridS%RTMnlayerSub)
    real(8)  :: expCoefGas(optPropRTMGridS%dimSV, optPropRTMGridS%dimSV, &
                           0:optPropRTMGridS%maxExpCoef, 0:optPropRTMGridS%RTMnlayerSub)
    real(8)  :: expCoefRRS(optPropRTMGridS%dimSV, optPropRTMGridS%dimSV, 0:2)

    ! scattering and absorption aerosol
    real(8)  :: kscaAer(-1:optPropRTMGridS%RTMnlayerSub+1)
    real(8)  :: kabsAer(-1:optPropRTMGridS%RTMnlayerSub+1)
    real(8)  :: kextAer(-1:optPropRTMGridS%RTMnlayerSub+1)
    real(8)  :: expCoefAer(optPropRTMGridS%dimSV, optPropRTMGridS%dimSV, &
                           0:optPropRTMGridS%maxExpCoef, 0:optPropRTMGridS%RTMnlayerSub)

    ! properties of the cloud
    real(8)  :: kscaCld(-1:optPropRTMGridS%RTMnlayerSub+1)
    real(8)  :: kabsCld(-1:optPropRTMGridS%RTMnlayerSub+1)
    real(8)  :: kextCld(-1:optPropRTMGridS%RTMnlayerSub+1)
    real(8)  :: expCoefCld(optPropRTMGridS%dimSV, optPropRTMGridS%dimSV, &
                           0:optPropRTMGridS%maxExpCoef, 0:optPropRTMGridS%RTMnlayerSub)

    ! total properties for gas aerosol and cloud
    real(8)  :: ksca(0:optPropRTMGridS%RTMnlayerSub)
    real(8)  :: kabs(0:optPropRTMGridS%RTMnlayerSub)
    real(8)  :: expCoef(optPropRTMGridS%dimSV, optPropRTMGridS%dimSV, &
                        0:optPropRTMGridS%maxExpCoef, 0:optPropRTMGridS%RTMnlayerSub)

    ! layer properties
    real(8)  :: babs   (optPropRTMGridS%RTMnlayer)
    real(8)  :: babsGas(optPropRTMGridS%RTMnlayer)
    real(8)  :: babsPar(optPropRTMGridS%RTMnlayer)
    real(8)  :: bsca   (optPropRTMGridS%RTMnlayer)
    real(8)  :: bscaGas(optPropRTMGridS%RTMnlayer)
    real(8)  :: bscaPar(optPropRTMGridS%RTMnlayer)
    real(8)  :: expCoefLay(optPropRTMGridS%dimSV, optPropRTMGridS%dimSV, &
                           0:optPropRTMGridS%maxExpCoef,optPropRTMGridS%RTMnlayer)

    ! depolarization parameters
    real(8)  :: eps, ssa
    real(8)  :: wavelength

    ! single scattering albedo used for combinations of Mie aerosol models
    real(8)  :: kext_comb, ksca_comb, kabs_comb, kext_550, ssa_w, kext_w, ksca_w, kabs_w
    real(8)  :: expCoef_w(6)
    real(8)  :: expCoef_comb(6, 0:optPropRTMGridS%maxExpCoef)

    ! parameters for cloud and aerosol
    real(8)  :: dzInterval
    real(8)  :: factAngstromAer, acAer
    real(8)  :: factAngstromCld, acCld

    integer  :: statusSpline, statusSplint

    integer  :: allocStatus, sumAllocStatus
    integer  :: deallocStatus, sumdeAllocStatus

    integer  :: dimSV

    logical  :: verbose, details

    if ( iwave == 1 ) then
! JdH Debug
      verbose = .false.
      details = .false.
      !verbose = .true.
      !details = .true.
    else
      verbose = .false.
      details = .false.
!      verbose = .true.
!      details = .true.
    end if

    dimSV = optPropRTMGridS%dimSV

    ! Lambertian surface albedo
    if ( cloudAerosolRTMgridS%useAlbedoLambSurfAllBands ) then

      surfAlb_iwave = cloudAerosolRTMgridS%albedoLambSurfAllBands

    else

      select case ( surfaceS%nwavelAlbedo )
        case(1) ! no wavelength dependence surface albedo
          surfAlb_iwave = surfaceS%albedo(1)
        case(2:100) ! linear interpolation or extrapolation
          surfAlb_iwave =  polyInt(errS, surfaceS%wavelAlbedo, surfaceS%albedo, wavelHRS%wavel(iwave))
        case default
          call logDebug('ERROR in subroutine getOptPropAtm:')
          call logDebug('number of surface albedo values is incorrect')
          call mystop(errS, 'incorrect number of surface albedo values in getOptPropAtm')
          if (errorCheck(errS)) return
      end select

    end if ! cloudAerosolRTMgridS%useAlbedoLambSurfAllBands

    ! test on limits for surface albedo (except for AAI and ozone profile)
    if ( .not. controlS%allowNegativeSurfAlbedo ) then
      if ( surfAlb_iwave < minSurfAlbedo ) surfAlb_iwave = minSurfAlbedo
      if ( surfAlb_iwave > maxSurfAlbedo ) surfAlb_iwave = maxSurfAlbedo
    end if

    ! surface emission, only specified for wavelength dependent surface albedo

    if ( cloudAerosolRTMgridS%useAlbedoLambSurfAllBands ) then
      surfEmission_iwave = 0.0d0
    else
      select case ( surfaceS%nwavelEmission )
        case(1) ! no wavelength dependence surface emission
          surfEmission_iwave = surfaceS%emission(1)
        case(2:100) ! linear interpolation or extrapolation
          surfEmission_iwave =  polyInt(errS, surfaceS%wavelEmission, surfaceS%emission, wavelHRS%wavel(iwave))
        case default
          call logDebug('ERROR in subroutine getOptPropAtm:')
          call logDebug('number of surface emission values is incorrect')
          call mystop(errS, 'incorrect number of surface emission values in getOptPropAtm')
          if (errorCheck(errS)) return
      end select

      if ( surfEmission_iwave < minSurfEmission ) surfEmission_iwave = minSurfEmission
      if ( surfEmission_iwave > maxSurfEmission ) surfEmission_iwave = maxSurfEmission

    end if ! cloudAerosolRTMgridS%useAlbedoLambSurfAllBands

    ! Lambertian cloud albedo
    if ( cloudAerosolRTMgridS%useLambertianCloud ) then
      if ( cloudAerosolRTMgridS%useAlbedoLambCldAllBands ) then

        cloudAlb_iwave = cloudAerosolRTMgridS%albedoLambCldAllBands

      else

        select case ( LambCloudS%nwavelAlbedo )
          case(1) ! no wavelength dependence cloud albedo
            cloudAlb_iwave = LambCloudS%albedo(1)
          case(2:100) ! polynomial interpolation or extrapolation
            cloudAlb_iwave =  polyInt(errS, LambCloudS%wavelAlbedo, LambCloudS%albedo, wavelHRS%wavel(iwave))
          case default
            call logDebug('ERROR in subroutine getOptPropAtm:')
            call logDebug('number of cloud albedo values is incorrect')
            call mystop(errS, 'incorrect number of cloud albedo values in getOptPropAtm')
            if (errorCheck(errS)) return
        end select

      end if ! cloudAerosolRTMgridS%useAlbedoLambCldAllBands
    else

      cloudAlb_iwave = 0.0d0      ! set value to zero if a scattering cloud is used

    end if ! cloudAerosolRTMgridS%useLambertianCloud

    if ( cloudAlb_iwave < minCldAlbedo ) cloudAlb_iwave = minCldAlbedo
    if ( cloudAlb_iwave > maxCldAlbedo ) cloudAlb_iwave = maxCldAlbedo

    wavelength = wavelHRS%wavel(iwave)

    ! interpolate on pressure, temperature, and volume mixing ratio to get the
    ! optical properties of the gas at the RTM sublayer grid
    ! spline interpolation on the natural logarithm is used for the pressure and vmr
    ! linear interpolation is used for the temperature if the flag useLinInterp is set

    ! calculate values for RTM sublayer grid

    lnpressureGrid = log(gasPTS%pressure)
    call spline(errS, gasPTS%alt, lnpressureGrid, SDlnpressure , statusSpline)
    if (errorCheck(errS)) return
    do ilevel = 0, optPropRTMGridS%RTMnlayerSub
      lnpressure (ilevel) = splint(errS, gasPTS%alt, lnpressureGrid, SDlnpressure , &
                                   optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
    end do
    pressure(:) = exp(lnpressure(:))

    if ( gasPTS%useLinInterp ) then
      do ilevel = 0, optPropRTMGridS%RTMnlayerSub
        temperature(ilevel) = splintLin(errS, gasPTS%alt, gasPTS%temperature, &
                                        optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
      end do
    else
      call spline(errS, gasPTS%alt, gasPTS%temperature, SDtemperature, statusSpline)
      if (errorCheck(errS)) return
      do ilevel = 0, optPropRTMGridS%RTMnlayerSub
        temperature(ilevel) = splint(errS, gasPTS%alt, gasPTS%temperature, SDtemperature, &
                                     optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
      end do
    end if

    optPropRTMGridS%tempSub(:) = temperature(:)

    ! 1.380658d-19 is Boltzmann's constant (but for cm-3, not for m-3)
    numberDensityAir(:) = pressure(:) / temperature(:) / 1.380658d-19 ! in molecules cm-3

    ! calculate values for column grid

    do ilevel = 1, optPropRTMGridS%nGaussCol
      lnpressureCol (ilevel) = splint(errS, gasPTS%alt, lnpressureGrid, SDlnpressure , &
                                optPropRTMGridS%Colaltitude(ilevel), statusSplint)
      if ( gasPTS%useLinInterp ) then
        temperatureCol(ilevel) = splintLin(errS, gasPTS%alt, gasPTS%temperature, &
                                     optPropRTMGridS%Colaltitude(ilevel), statusSplint)
      else
        temperatureCol(ilevel) = splint(errS, gasPTS%alt, gasPTS%temperature, SDtemperature, &
                                     optPropRTMGridS%Colaltitude(ilevel), statusSplint)
      end if
    end do

    pressureCol(:) = exp(lnpressureCol(:))

    numberDensityAirCol(:) = pressureCol(:) / temperatureCol(:) / 1.380658d-19 ! in molecules cm-3

    do iTrace = 1, nTrace

      ! allocate arrays

      allocStatus    = 0
      sumAllocStatus = 0

      allocate( lnvmrTraceGrid  (0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( lnvmrTraceGridAP(0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDlnvmrTrace    (0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDlnvmrTraceAP  (0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( XsecNodes       (0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDXsecNodes     (0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( dXsecdTNodes    (0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDdXsecdTNodes  (0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      if (sumAllocStatus /= 0) then
        call logDebug('ERROR: allocation failed in subroutine getOptPropAtm')
        call logDebugI('for iTrace = ', iTrace)
        call logDebug('in subroutine getOptPropAtm')
        call logDebug('in module propAtmosphereModule')
        call mystop(errS, 'stopped because allocation failed')
        if (errorCheck(errS)) return
      end if

      lnvmrTraceGrid   = log(traceGasS(iTrace)%vmr)
      lnvmrTraceGridAP = log(traceGasS(iTrace)%vmrAP)
      if ( .not. traceGasS(iTrace)%useLinInterp ) then
        call spline(errS, traceGasS(iTrace)%alt, lnvmrTraceGrid,   SDlnvmrTrace  , statusSpline)
        if (errorCheck(errS)) return
        call spline(errS, traceGasS(iTrace)%alt, lnvmrTraceGridAP, SDlnvmrTraceAP, statusSpline)
        if (errorCheck(errS)) return
      end if

      if ( controlS%useEffXsec_OE ) then
        XsecNodes      = XsecS(iTrace)%XsecConv(iwave,:)
        dXsecdTNodes   = XsecS(iTrace)%dXsecConvdT(iwave,:)
      else
        XsecNodes      = XsecS(iTrace)%Xsec(iwave,:)
        dXsecdTNodes   = XsecS(iTrace)%dXsecdT(iwave,:)
      end if

      call spline(errS, traceGasS(iTrace)%alt, XsecNodes   , SDXsecNodes   , statusSpline)
      if (errorCheck(errS)) return
      call spline(errS, traceGasS(iTrace)%alt, dXsecdTNodes, SDdXsecdTNodes, statusSpline)
      if (errorCheck(errS)) return

      ! calculate the absorption cross section its temperature derivative and vmr on the RTM sub grid

      if ( traceGasS(iTrace)%useLinInterp ) then
        do ilevel = 0, optPropRTMGridS%RTMnlayerSub
          lnvmrTrace(ilevel,iTrace)   = splintLin(errS, traceGasS(iTrace)%alt, lnvmrTraceGrid,   &
                                        optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
        end do
      else
        do ilevel = 0, optPropRTMGridS%RTMnlayerSub
          lnvmrTrace(ilevel,iTrace)   = splint(errS, traceGasS(iTrace)%alt, lnvmrTraceGrid, SDlnvmrTrace,   &
                                        optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
        end do
      end if
      vmrTrace(:,iTrace) = exp(lnvmrTrace(:,iTrace))

      do ilevel = 0, optPropRTMGridS%RTMnlayerSub
        XsecTrace (ilevel,iTrace)   = splint(errS, traceGasS(iTrace)%alt, XsecNodes,      SDXsecNodes,    &
                                      optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
        dXsecdTTrace(ilevel,iTrace) = splint(errS, traceGasS(iTrace)%alt, dXsecdTNodes,   SDdXsecdTNodes, &
                                      optPropRTMGridS%RTMaltitudeSub(ilevel), statusSplint)
      end do

      ! calculate the absorption cross section and number density for the column altitude grid

      do ilevel = 1, optPropRTMGridS%nGaussCol
        XsecCol (ilevel,iTrace)        = splint(errS, traceGasS(iTrace)%alt, XsecNodes, SDXsecNodes,       &
                                         optPropRTMGridS%Colaltitude(ilevel), statusSplint)
      end do

      if ( traceGasS(iTrace)%useLinInterp ) then
        do ilevel = 1, optPropRTMGridS%nGaussCol
          lnvmrTraceCol(ilevel,iTrace)   = splintLin(errS, traceGasS(iTrace)%alt, lnvmrTraceGrid,   &
                                           optPropRTMGridS%Colaltitude(ilevel), statusSplint)
          lnvmrTraceAPCol(ilevel,iTrace) = splintLin(errS, traceGasS(iTrace)%alt, lnvmrTraceGridAP, &
                                           optPropRTMGridS%Colaltitude(ilevel), statusSplint)
        end do
      else
        do ilevel = 1, optPropRTMGridS%nGaussCol
          lnvmrTraceCol(ilevel,iTrace)   = splint(errS, traceGasS(iTrace)%alt, lnvmrTraceGrid, SDlnvmrTrace,     &
                                           optPropRTMGridS%Colaltitude(ilevel), statusSplint)
          lnvmrTraceAPCol(ilevel,iTrace) = splint(errS, traceGasS(iTrace)%alt, lnvmrTraceGridAP, SDlnvmrTraceAP, &
                                           optPropRTMGridS%Colaltitude(ilevel), statusSplint)
        end do
      end if
      vmrTraceCol  (:, iTrace) = exp( lnvmrTraceCol  (:,iTrace) )
      vmrTraceAPCol(:, iTrace) = exp( lnvmrTraceAPCol(:,iTrace) )

      ! vmr in ppmv => factor 1.0d-6
      numberDensityTraceCol  (:, iTrace) = vmrTraceCol  (:, iTrace) * numberDensityAirCol(:) * 1.0d-6
      numberDensityTraceAPCol(:, iTrace) = vmrTraceAPCol(:, iTrace) * numberDensityAirCol(:) * 1.0d-6

      ! deallocate arrays
      deallocStatus    = 0
      sumdeAllocStatus = 0

      deallocate( lnvmrTraceGrid, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( lnvmrTraceGridAP, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDlnvmrTrace, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDlnvmrTraceAP, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( XsecNodes, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDXsecNodes, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( dXsecdTNodes, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDdXsecdTNodes, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus

      if (sumdeAllocStatus /= 0) then
        call logDebug('ERROR: deallocation failed in subroutine getOptPropAtm')
        call logDebugI('for iTrace = ', iTrace)
        call logDebug('in module propAtmosphereModule')
        call mystop(errS, 'stopped because deallocation failed')
        if (errorCheck(errS)) return
      end if

    end do ! iTrace

    ! assign values
    if ( associated(optPropRTMGridS%ColXsec) )     optPropRTMGridS%ColXsec(:,:)    = XsecCol(:,:)
    if ( associated(optPropRTMGridS%Colndens) )    optPropRTMGridS%Colndens(:,:)   = numberDensityTraceCol(:,:)
    if ( associated(optPropRTMGridS%ColndensAP) )  optPropRTMGridS%ColndensAP(:,:) = numberDensityTraceAPCol(:,:)
    if ( associated(optPropRTMGridS%ColndensAir) ) optPropRTMGridS%ColndensAir(:)  = numberDensityAirCol(:)

    ! RayXsec returns the Rayleigh scattering cross section in molecules per cm2
    ! altitude is given in km, not in cm, therefore we need a factor 1.0d5 to express
    ! kscaGas as the volume scattering coefficient per km

    if ( RRS_RingS%useCabannes ) then
      if ( ( wavelength > 265.0) .and. ( wavelength < 505.0) ) then
        do ilevel = 0, optPropRTMGridS%RTMnlayerSub
          ssa = CabannesAlbedo(errS, wavelength, temperature(ilevel) )
          kscaGas(ilevel) = ssa * RayXsec(wavelength) * numberDensityAir(ilevel) * 1.0d5          ! in km-1
          kabsGas(ilevel) = (1.0d0 - ssa) * kscaGas(ilevel) / ssa
        end do
      else
        do ilevel = 0, optPropRTMGridS%RTMnlayerSub
          kscaGas(ilevel) = CabannesXsec(wavelength, temperature(ilevel) ) * numberDensityAir(ilevel) * 1.0d5  ! in km-1
          kabsGas(ilevel) = numberDensityAir(ilevel) * 1.0d5 &
                   * ( RayXsec(wavelength) - CabannesXsec(wavelength, temperature(ilevel) ) )
        end do
      end if
    else
      do ilevel = 0, optPropRTMGridS%RTMnlayerSub
        kscaGas(ilevel) = RayXsec(wavelength) * numberDensityAir(ilevel) * 1.0d5          ! in km-1
        kabsGas(ilevel) = 0.0d0
      end do
    end if ! RRS_RingS%useCabannes

    do iTrace = 1, nTrace
        kabsGas(:) = kabsGas(:) &
          + XsecTrace(:,iTrace) *  1.0d-6 * vmrTrace(:,iTrace) * numberDensityAir * 1.0d5  ! km-1; vmr in ppmv
    end do

    ! see Stam et al. for equations
    depol = depolarization_factor_air(wavelength)
    eps   = 45.0d0 * depol / (6.0d0 - 7.0d0 * depol )

    ! initialization
    expCoefGas           = 0.0d0

    if ( RRS_RingS%useCabannes ) then

      expCoefGas(1,1,0,:)  = 1.0d0
      expCoefGas(1,1,2,:)  = (180.0d0 + eps) / (18.0d0 + eps) / 20.0d0

      if ( dimSV > 2 ) then
        expCoefGas(2,2,2,:)  = 3.0d0 * (180.0d0 + eps) / (18.0d0 + eps) / 10.0d0
        expCoefGas(1,2,2,:)  = sqrt(1.5d0) * (180.0d0 + eps) / (18.0d0 + eps) / 10.0d0
        expCoefGas(2,1,2,:)  = expCoefGas(1,2,2,:)
      end if
      if ( dimSV > 3 ) then
        expCoefGas(4,4,1,:)  = 3.0d0 * (36.0d0 - eps) / (18.0d0 + eps) / 4.0d0
      end if

    else

      expCoefGas(1,1,0,:)  = 1.0d0
      expCoefGas(1,1,2,:)  = (45.0d0 + eps) / (90.0d0 + 20.0d0 * eps)

      if ( dimSV > 2 ) then
        expCoefGas(2,2,2,:)  = 6.0d0 * (45.0d0 + eps) / (90.0d0 + 20.0d0 * eps)
        expCoefGas(1,2,2,:)  = sqrt(1.5d0) * (90.0d0 + 2.0d0 * eps) / (90.0d0 + 20.0d0 * eps)
        expCoefGas(2,1,2,:)  = expCoefGas(1,2,2,:)
      end if
      if ( dimSV > 3 ) then
        expCoefGas(4,4,1,:)  = 15.0d0 * (9.0d0 - eps) / (90.0d0 + 20.0d0 * eps)
      end if

    end if !  ! RRS_RingS%useCabannes

    ! expansion coefficients for Rotational Raman Scattering (RRS)
    ! initialize
    expCoefRRS         = 0.0d0

    expCoefRRS(1,1,0)  = 1.00d0
    expCoefRRS(1,1,2)  = 0.05d0

    if ( dimSV > 2 ) then
      expCoefRRS(2,2,2)  = 0.30d0
      expCoefRRS(1,2,2)  = sqrt(1.5d0) / 10.0d0
      expCoefRRS(2,1,2)  = expCoefRRS(1,2,2)
    end if
    if ( dimSV > 3 ) then
      expCoefRRS(4,4,1)  = -0.75d0
    end if

    ! calculate the optical properties of aerosol on the sublayer grid
    ! Values on the sublayer grid 0, 1, ..., nGaussLay have the same value
    ! Therefore, values on the interface between two layers pertain to the
    ! layer above the interface

    ! intialize for aerosol and cloud
    kextAer    = 0.0d0
    kscaAer    = 0.0d0
    kabsAer    = 0.0d0
    expCoefAer = 0.0d0
    kextCld    = 0.0d0
    kscaCld    = 0.0d0
    kabsCld    = 0.0d0
    expCoefCld = 0.0d0

    ! HG scattering aerosol
    if ( cloudAerosolRTMgridS%useHGScatAer ) then
      ! intialize
      index = 0
      do ibound = 1, cloudAerosolRTMgridS%ninterval
        dzInterval = cloudAerosolRTMgridS%intervalBounds(ibound) &
                 -   cloudAerosolRTMgridS%intervalBounds(ibound-1)
        acAer = cloudAerosolRTMgridS%intervalAerAC(ibound)
        factAngstromAer = ( wavelength / cloudAerosolRTMgridS%w0 ) **(-acAer)
        optPropRTMGridS%kextKextAerdiv550(ibound) = factAngstromAer
        do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
          do igaussSub = 0, optPropRTMGridS%nGaussLay
            kscaAer(index) = cloudAerosolRTMgridS%intervalAerTau(ibound) * factAngstromAer  &
                           * cloudAerosolRTMgridS%intervalAerSSA (ibound) / dzInterval
            kabsAer(index) = cloudAerosolRTMgridS%intervalAerTau(ibound) * factAngstromAer  &
                           * (1.0d0 - cloudAerosolRTMgridS%intervalAerSSA(ibound) ) / dzInterval
            expCoefAer(1,1, 0, index) = 1.0d0
            do icoef = 1, optPropRTMGridS%maxExpCoef
              expCoefAer(1, 1, icoef, index) = cloudAerosolRTMgridS%intervalAer_g(ibound) ** icoef &
                                       * (2.0d0 * icoef + 1.0d0)
            end do ! icoef
            index = index + 1
          end do ! igaussSub
        end do ! igauss
      end do  ! ibound
      ! fill last entry
      index = optPropRTMGridS%RTMnlayerSub
      kscaAer(index)       = kscaAer(index-1)
      kabsAer(index)       = kabsAer(index-1)
      expCoefAer(1,1, :, index) = expCoefAer(1, 1, :, index-1)
    end if ! cloudAerosolRTMgridS%useHGScatAer

    ! Mie scattering aerosol
    if ( cloudAerosolRTMgridS%useMieScatAer ) then
      ! intialize
      index = 0
      do ibound = 1, cloudAerosolRTMgridS%ninterval

        dzInterval = cloudAerosolRTMgridS%intervalBounds(ibound) &
                 -   cloudAerosolRTMgridS%intervalBounds(ibound-1)
        kext_550   = cloudAerosolRTMgridS%intervalAerTau(ibound) / dzInterval
        ! optical properties of the aerosol do not vary inside an interval
        ! calculate only the optical properties when the aerosol optical thickness > threshold
        if ( cloudAerosolRTMgridS%intervalAerTau(ibound) > 1.0d-10 ) then
          ! extinction and single scattering albedo
          kext_comb      = 0.0d0
          ksca_comb      = 0.0d0
          kabs_comb      = 0.0d0
          expCoef_comb   = 0.0d0
          do imodel = 1, maxNumMieModels
            if ( .not. associated( mieAerS(imodel)%Cext ) ) exit ! exit imodel loop
            kext_550  = mieAerS(imodel)%tau(ibound) / dzInterval
            kext_w    = splintLin(errS, mieAerS(imodel)%wavel, mieAerS(imodel)%Cext, wavelength, status) &
                      * kext_550  / mieAerS(imodel)%Cext550nm
            kext_comb = kext_comb + kext_w
            ssa_w     = splintLin(errS, mieAerS(imodel)%wavel, mieAerS(imodel)%a, wavelength, status)
            ksca_w    = ssa_w * kext_w
            kabs_w    = (1.0d0 - ssa_w) * kext_w
            ksca_comb = ksca_comb + ksca_w
            kabs_comb = kabs_comb + kabs_w
            do icoef = 0, mieAerS(imodel)%numExpCoef
              do iExpCoef = 1, 6
                expCoef_w(iExpCoef) = splintLin(errS, mieAerS(imodel)%wavel(:), mieAerS(imodel)%expCoef(iExpCoef,icoef,:), &
                                                wavelength, status)
                expCoef_comb(iExpCoef,iCoef) = expCoef_comb(iExpCoef,iCoef) +  ksca_w * expCoef_w(iExpCoef)
              end do ! iExpCoef
            end do ! icoef
          end do ! imodel
          optPropRTMGridS%kextKextAerdiv550(ibound) =  kext_comb / kext_550
          cloudAerosolRTMgridS%intervalAerSSA (ibound) = ksca_comb / kext_comb
          expCoef_comb(:,:) = expCoef_comb(:,:) / ksca_comb
          do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
            do igaussSub = 0, optPropRTMGridS%nGaussLay
              kextAer(index) = kext_comb
              kscaAer(index) = ksca_comb
              kabsAer(index) = kabs_comb
              expCoefAer(:,:, :, index) = 0.0d0
              select case (dimSV)
                case(1)
                  expCoefAer(1,1, :, index) = expCoef_comb(1,:)
                case(3)
                  expCoefAer(1,1, :, index) = expCoef_comb(1,:)
                  expCoefAer(2,2, :, index) = expCoef_comb(2,:)
                  expCoefAer(3,3, :, index) = expCoef_comb(3,:)
                  expCoefAer(1,2, :, index) = expCoef_comb(5,:)
                  expCoefAer(2,1, :, index) = expCoef_comb(5,:)
                case(4)
                  expCoefAer(1,1, :, index) =  expCoef_comb(1,:)
                  expCoefAer(2,2, :, index) =  expCoef_comb(2,:)
                  expCoefAer(3,3, :, index) =  expCoef_comb(3,:)
                  expCoefAer(4,4, :, index) =  expCoef_comb(4,:)
                  expCoefAer(1,2, :, index) =  expCoef_comb(5,:)
                  expCoefAer(2,1, :, index) =  expCoef_comb(5,:)
                  expCoefAer(3,4, :, index) =  expCoef_comb(6,:)
                  expCoefAer(4,3, :, index) = -expCoef_comb(6,:)
              end select
              index = index + 1
            end do ! igaussSub
          end do ! igauss
        else
          do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
            do igaussSub = 0, optPropRTMGridS%nGaussLay
              kextAer(index) = 0.0d0
              kscaAer(index) = 0.0d0
              kabsAer(index) = 0.0d0
              expCoefAer(:,:,:, index) = 0.0d0
              index = index + 1
            end do ! igaussSub
          end do ! igauss
        end if ! cloudAerosolRTMgridS%intervalAerTau(ibound) > 1.0d-10

      end do  ! ibound
      ! fill last entry
      index = optPropRTMGridS%RTMnlayerSub
      kextAer(index)          = kextAer(index-1)
      kscaAer(index)          = kscaAer(index-1)
      kabsAer(index)          = kabsAer(index-1)
      expCoefAer(:,:,:,index) = expCoefAer(:,:,:, index-1)

    end if ! cloudAerosolRTMgridS%useMieScatAer

    if ( cloudyPart ) then

      ! Calculate the optical properties of cloud on the sublayer grid.
      ! Values on the sublayer grid 0, 1, ..., nGaussLay have the same value
      ! Therefore, values on the interface between two layers pertain to the
      ! layer above the interface

      ! HG scattering cloud
      if ( cloudAerosolRTMgridS%useHGScatCld ) then
        ! intialize
        index = 0
        do ibound = 1, cloudAerosolRTMgridS%ninterval
          dzInterval = cloudAerosolRTMgridS%intervalBounds(ibound) &
                     - cloudAerosolRTMgridS%intervalBounds(ibound-1)
          acCld = cloudAerosolRTMgridS%intervalCldAC(ibound)
          factAngstromCld = ( wavelength / cloudAerosolRTMgridS%w0 ) **(-acCld)
          optPropRTMGridS%kextKextClddiv550(ibound) = factAngstromCld
          do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
            do igaussSub = 0, optPropRTMGridS%nGaussLay
              kscaCld(index) = cloudAerosolRTMgridS%intervalCldTau(ibound) * factAngstromCld &
                             * cloudAerosolRTMgridS%intervalCldSSA (ibound) / dzInterval
              kabsCld(index) = cloudAerosolRTMgridS%intervalCldTau(ibound) * factAngstromCld  &
                             * (1.0d0 - cloudAerosolRTMgridS%intervalCldSSA(ibound) ) / dzInterval
              if ( kscaCld(index) > 1.0d-8 ) then
                expCoefCld(1, 1, 0, index) = 1.0d0
                do icoef = 1, optPropRTMGridS%maxExpCoef
                  expCoefCld(1, 1, icoef, index) = cloudAerosolRTMgridS%intervalCld_g(ibound) ** icoef &
                                                  * (2.0d0 * icoef + 1.0d0)
                end do ! icoef
              else
                expCoefCld(1, 1, :, index) = 0.0d0
              end if
              index = index + 1
            end do ! igaussSub
          end do ! igauss
        end do  ! ibound
        ! fill last entry
        index = optPropRTMGridS%RTMnlayerSub
        kscaCld(index)       = kscaCld(index-1)
        kabsCld(index)       = kabsCld(index-1)
        expCoefCld(1, 1, :, index) = expCoefCld(1, 1, :, index-1)

      end if ! cloudAerosolRTMgridS%useHGScatCld

      ! Mie scattering cloud
      if ( cloudAerosolRTMgridS%useMieScatCld ) then
        ! intialize
        index = 0
        do ibound = 1, cloudAerosolRTMgridS%ninterval

          dzInterval = cloudAerosolRTMgridS%intervalBounds(ibound) &
                   -   cloudAerosolRTMgridS%intervalBounds(ibound-1)
          kext_550   = cloudAerosolRTMgridS%intervalCldTau(ibound) / dzInterval
          ! optical properties of the cloud do not vary inside an interval
          ! calculate only the optical properties when the cloud optical thickness > threshold
          if ( cloudAerosolRTMgridS%intervalCldTau(ibound) > 1.0d-10 ) then
            ! extinction and single scattering albedo
            kext_comb      = 0.0d0
            ksca_comb      = 0.0d0
            kabs_comb      = 0.0d0
            expCoef_comb   = 0.0d0
            do imodel = 1, maxNumMieModels
              if ( .not. associated( mieCldS(imodel)%Cext ) ) exit ! exit imodel loop
              kext_550  = mieCldS(imodel)%tau(ibound) / dzInterval
              kext_w    = splintLin(errS, mieCldS(imodel)%wavel, mieCldS(imodel)%Cext, wavelength, status) &
                        * kext_550  / mieCldS(imodel)%Cext550nm
              kext_comb = kext_comb + kext_w
              ssa_w     = splintLin(errS, mieCldS(imodel)%wavel, mieCldS(imodel)%a, wavelength, status)
              ksca_w    = ssa_w * kext_w
              kabs_w    = (1.0d0 - ssa_w) * kext_w
              ksca_comb = ksca_comb + ksca_w
              kabs_comb = kabs_comb + kabs_w
              do icoef = 0, mieCldS(imodel)%numExpCoef
                do iExpCoef = 1, 6
                 expCoef_w(iExpCoef) = splintLin(errS, mieCldS(imodel)%wavel(:), mieCldS(imodel)%expCoef(iExpCoef,icoef,:), &
                                                 wavelength, status)
                 expCoef_comb(iExpCoef,iCoef) = expCoef_comb(iExpCoef,iCoef) +  ksca_w * expCoef_w(iExpCoef)
                end do ! iExpCoef
              end do ! icoef
            end do ! imodel
            optPropRTMGridS%kextKextClddiv550(ibound) = kext_comb / kext_550
            cloudAerosolRTMgridS%intervalCldSSA (ibound) = ksca_comb / kext_comb
            expCoef_comb(:,:) = expCoef_comb(:,:) / ksca_comb
            do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
              do igaussSub = 0, optPropRTMGridS%nGaussLay
                kextCld(index) = kext_comb
                kscaCld(index) = ksca_comb
                kabsCld(index) = kabs_comb
                expCoefCld(:,:,:, index) = 0.0d0
                select case (dimSV)
                  case(1)
                    expCoefCld(1,1, :, index) = expCoef_comb(1,:)
                  case(3)
                    expCoefCld(1,1, :, index) = expCoef_comb(1,:)
                    expCoefCld(2,2, :, index) = expCoef_comb(2,:)
                    expCoefCld(3,3, :, index) = expCoef_comb(3,:)
                    expCoefCld(1,2, :, index) = expCoef_comb(5,:)
                    expCoefCld(2,1, :, index) = expCoef_comb(5,:)
                  case(4)
                    expCoefCld(1,1, :, index) =  expCoef_comb(1,:)
                    expCoefCld(2,2, :, index) =  expCoef_comb(2,:)
                    expCoefCld(3,3, :, index) =  expCoef_comb(3,:)
                    expCoefCld(4,4, :, index) =  expCoef_comb(4,:)
                    expCoefCld(1,2, :, index) =  expCoef_comb(5,:)
                    expCoefCld(2,1, :, index) =  expCoef_comb(5,:)
                    expCoefCld(3,4, :, index) =  expCoef_comb(6,:)
                    expCoefCld(4,3, :, index) = -expCoef_comb(6,:)
                end select
                index = index + 1
              end do ! igaussSub
            end do ! igauss
          else
            do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
              do igaussSub = 0, optPropRTMGridS%nGaussLay
                kextCld(index) = 0.0d0
                kscaCld(index) = 0.0d0
                kabsCld(index) = 0.0d0
                expCoefCld(:,:,:, index) = 0.0d0
                index = index + 1
              end do ! igaussSub
            end do ! igauss
          end if ! cloudAerosolRTMgridS%intervalCldTau(ibound) > 1.0d-10

        end do  ! ibound
        ! fill last entry
        index = optPropRTMGridS%RTMnlayerSub
        kextCld(index)       = kextCld(index-1)
        kscaCld(index)       = kscaCld(index-1)
        kabsCld(index)       = kabsCld(index-1)
        expCoefCld(:,:,:,index) = expCoefCld(:,:,:, index-1)

      end if ! cloudAerosolRTMgridS%useMieScatCld

    end if ! cloudyPart

    ! calculate the overall properties
    expCoef = 0.0d0
    do indexSub = 0, optPropRTMGridS%RTMnlayerSub
      ksca(indexSub)  = kscaGas(indexSub) + kscaAer(indexSub) + kscaCld(indexSub)
      kabs(indexSub)  = kabsGas(indexSub) + kabsAer(indexSub) + kabsCld(indexSub)
      expCoef(:,:,:,indexSub) = ( kscaAer(indexSub) * expCoefAer(:,:,:,indexSub)   &
                                + kscaCld(indexSub) * expCoefCld(:,:,:,indexSub)   &
                                + kscaGas(indexSub) * expCoefGas(:,:,:,indexSub) )  / ksca(indexSub)
    end do ! indexSub

    ! calculate the values at the interval boundaries
    indexSub = 0
    do ibound = 0, cloudAerosolRTMgridS%ninterval
      optPropRTMGridS%kscaIntAboveGas(ibound) = kscaGas(indexSub)
      optPropRTMGridS%kscaIntBelowGas(ibound) = kscaGas(indexSub)
      optPropRTMGridS%kabsIntAboveGas(ibound) = kabsGas(indexSub)
      optPropRTMGridS%kabsIntBelowGas(ibound) = kabsGas(indexSub)
      optPropRTMGridS%kscaIntAboveAer(ibound) = kscaAer(indexSub + 1)
      optPropRTMGridS%kscaIntBelowAer(ibound) = kscaAer(indexSub - 1)
      optPropRTMGridS%kabsIntAboveAer(ibound) = kabsAer(indexSub + 1)
      optPropRTMGridS%kabsIntBelowAer(ibound) = kabsAer(indexSub - 1)
      optPropRTMGridS%kscaIntAboveCld(ibound) = kscaCld(indexSub + 1)
      optPropRTMGridS%kscaIntBelowCld(ibound) = kscaCld(indexSub - 1)
      optPropRTMGridS%kabsIntAboveCld(ibound) = kabsCld(indexSub + 1)
      optPropRTMGridS%kabsIntBelowCld(ibound) = kabsCld(indexSub - 1)
      if ( ibound < cloudAerosolRTMgridS%ninterval ) then
        indexSub = indexSub + ( optPropRTMGridS%nGaussLay + 1 ) &
                            * ( cloudAerosolRTMgridS%intervalnGauss(ibound + 1) + 1 )
      end if
    end do ! ibound

    ! set the boundaries for the fit interval in the structure OptPropRTMGridS
    ! because this information is needed in the radiative transfer module
    optPropRTMGridS%lowerBoundFitInterval = &
         cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit - 1)
    optPropRTMGridS%upperBoundFitInterval = &
         cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit)

    index    = 0
    indexSub = 0
    do ibound = 1, cloudAerosolRTMgridS%numIntervalFit - 1
      do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
        index    = index    + 1
        indexSub = indexSub + 1 + optPropRTMGridS%nGaussLay
      end do ! igauss
    end do ! ibound

    optPropRTMGridS%indexRTMFitIntBot    = index
    optPropRTMGridS%indexRTMSubFitIntBot = indexSub
    index = index + 1 + cloudAerosolRTMgridS%intervalnGauss( cloudAerosolRTMgridS%numIntervalFit )
    indexSub = indexSub + cloudAerosolRTMgridS%intervalnGauss( cloudAerosolRTMgridS%numIntervalFit ) &
               * (1 + optPropRTMGridS%nGaussLay )
    optPropRTMGridS%indexRTMFitIntTop    = index
    optPropRTMGridS%indexRTMSubFitIntTop = indexSub

    ! calculate the layer properties
    index    = 0
    indexSub = 0
    do ibound = 1, cloudAerosolRTMgridS%ninterval
      do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
        index    = index    + 1
        indexSub = indexSub + 1
        babs   (index) = 0.0d0
        bsca   (index) = 0.0d0
        babsGas(index) = 0.0d0
        bscaGas(index) = 0.0d0
        babsPar(index) = 0.0d0
        bscaPar(index) = 0.0d0
        expCoefLay(:,:,:,index) = 0.0d0
        do igaussSub = 1, optPropRTMGridS%nGaussLay
          babs   (index) = babs   (index) + optPropRTMGridS%RTMweightSub(indexSub) * kabs(indexSub)
          bsca   (index) = bsca   (index) + optPropRTMGridS%RTMweightSub(indexSub) * ksca(indexSub)
          babsGas(index) = babsGas(index) + optPropRTMGridS%RTMweightSub(indexSub) * kabsGas(indexSub)
          bscaGas(index) = bscaGas(index) + optPropRTMGridS%RTMweightSub(indexSub) * kscaGas(indexSub)
          babsPar(index) = babsPar(index) + optPropRTMGridS%RTMweightSub(indexSub)     &
                                          * ( kabsAer(indexSub)+ kabsCld(indexSub) )
          bscaPar(index) = bscaPar(index) + optPropRTMGridS%RTMweightSub(indexSub)     &
                                          * ( kscaAer(indexSub)+ kscaCld(indexSub) )
          expCoefLay(:,:,:, index) =  expCoefLay(:,:,:, index) &
                  + optPropRTMGridS%RTMweightSub(indexSub) * ksca(indexSub) * expCoef(:,:,:, indexSub)
          indexSub = indexSub + 1
        end do ! igaussSub

        if ( bsca(index) > 1.0d-8 ) then
          expCoefLay(:,:,:, index) = expCoefLay(:,:,:, index) / bsca(index)
        else
          expCoefLay(:,:,:, index) = 0.0d0
          expCoefLay(1,1,0, index) = 1.0d0
        end if

        ! determine maxExpCoef for the layers
        do iExpCoef = optPropRTMGridS%maxExpCoef, 2, -1
           optPropRTMGridS%maxExpCoefLay(index) = iExpCoef
          if ( abs( expCoefLay(1,1, iExpCoef, index) ) > cloudAerosolRTMgridS%thresholdPhaseFunctionCoef ) exit
        end do ! iExpCoef

      end do ! igauss
    end do  ! ibound

    ! assign extinction, scattering, and absorption values to the levels of the RTM grid
    index    = 0
    indexSub = 0
    do ibound = 1, cloudAerosolRTMgridS%ninterval
      do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
        optPropRTMGridS%ksca(index) = ksca(indexSub)
        optPropRTMGridS%kabs(index) = kabs(indexSub)
        optPropRTMGridS%kext(index) = kabs(indexSub) + ksca(indexSub)
        optPropRTMGridS%RTMtemperature(index) = temperature(indexSub)
        optPropRTMGridS%RTMpressure(index)    = pressure(indexSub)
        optPropRTMGridS%RTMndensAir(index)    = numberDensityAir(indexSub)
        index    = index    + 1
        indexSub = indexSub + optPropRTMGridS%nGaussLay + 1
      end do ! igauss
    end do  ! ibound
    ! fill last entry
    optPropRTMGridS%ksca(optPropRTMGridS%RTMnlayer) = ksca(optPropRTMGridS%RTMnlayerSub)
    optPropRTMGridS%kabs(optPropRTMGridS%RTMnlayer) = kabs(optPropRTMGridS%RTMnlayerSub)
    optPropRTMGridS%kext(optPropRTMGridS%RTMnlayer) = ksca(optPropRTMGridS%RTMnlayerSub) &
                                                    + kabs(optPropRTMGridS%RTMnlayerSub)
    optPropRTMGridS%RTMtemperature(optPropRTMGridS%RTMnlayer) = temperature(optPropRTMGridS%RTMnlayerSub)
    optPropRTMGridS%RTMpressure(optPropRTMGridS%RTMnlayer)    = pressure(optPropRTMGridS%RTMnlayerSub)

    ! assign phase function coefficients to interfaces between the layers
    do index = 0, optPropRTMGridS%RTMnlayer - 1
      indexSub = index * ( optPropRTMGridS%nGaussLay + 1 )
      optPropRTMGridS%phasefCoefGas(:,:,:,index)      = expCoefGas(:,:,:,indexSub)
      optPropRTMGridS%phasefCoefAerAbove(:,:,:,index) = 0.0d0
      optPropRTMGridS%phasefCoefAerAbove(:,:,:,index) = expCoefAer(:,:,:,indexSub)
      optPropRTMGridS%phasefCoefCldAbove(:,:,:,index) = 0.0d0
      optPropRTMGridS%phasefCoefCldAbove(:,:,:,index) = expCoefCld(:,:,:,indexSub)
      optPropRTMGridS%phasefCoefAbove(:,:,:,index)    = expCoef(:,:,:,indexSub)
    end do  ! index
    ! add last entry
    index    = optPropRTMGridS%RTMnlayer
    indexSub = optPropRTMGridS%RTMnlayerSub
    optPropRTMGridS%phasefCoefGas(:,:,:,index)      = expCoefGas(:,:,:,indexSub)
    optPropRTMGridS%phasefCoefAerAbove(:,:,:,index) = 0.0d0
    optPropRTMGridS%phasefCoefAerAbove(:,:,:,index) = expCoefAer(:,:,:,indexSub)
    optPropRTMGridS%phasefCoefCldAbove(:,:,:,index) = 0.0d0
    optPropRTMGridS%phasefCoefCldAbove(:,:,:,index) = expCoefCld(:,:,:,indexSub)
    optPropRTMGridS%phasefCoefAbove   (:,:,:,index) = expCoef(:,:,:,indexSub)

    ! fill the values when approaching from below
    optPropRTMGridS%phasefCoefAerBelow(:,:,:,0) = 0.0d0
    optPropRTMGridS%phasefCoefCldBelow(:,:,:,0) = 0.0d0
    do index = 1, optPropRTMGridS%RTMnlayer
        optPropRTMGridS%phasefCoefAerBelow(:,:,:,index) = optPropRTMGridS%phasefCoefAerAbove(:,:,:,index - 1)
        optPropRTMGridS%phasefCoefCldBelow(:,:,:,index) = optPropRTMGridS%phasefCoefCldAbove(:,:,:,index - 1)
    end do ! index

    ! expansion coefficients for RRS
    optPropRTMGridS%phasefCoefRRS = expCoefRRS

    ! assign values to the optical properties of the layers
    index    = 0
    do ibound = 1, cloudAerosolRTMgridS%ninterval
      do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
        index    = index    + 1
        optPropRTMGridS%opticalThicknLay   (index) = babs   (index) + bsca   (index)
        optPropRTMGridS%ssaLay(index)              = bsca(index) / ( babs(index) + bsca(index) )
        optPropRTMGridS%phasefCoefLay(:,:,:,index) = expCoefLay(:,:,:,index)
      end do ! igauss
    end do  ! ibound

    ! assign values to the optical properties for the sublayer  grid
    index = 0
    indexSub = 0
    do ibound = 1, cloudAerosolRTMgridS%ninterval
      do igauss = 0, cloudAerosolRTMgridS%intervalnGauss(ibound)
        do iTrace = 1, nTrace
          optPropRTMGridS%ndensGas(index,iTrace) = vmrTrace(indexSub,iTrace) * 1.0d-6 &
                                                 * numberDensityAir(indexSub)
        end do
        index = index + 1
        do igaussSub = 0, optPropRTMGridS%nGaussLay
          optPropRTMGridS%kscaSub(indexSub) = ksca(indexSub)
          optPropRTMGridS%kabsSub(indexSub) = kabs(indexSub)
          optPropRTMGridS%kextSub(indexSub) = kabs(indexSub) + ksca(indexSub)
          do iTrace = 1, nTrace
            optPropRTMGridS%ndensSubGas(indexSub,iTrace)   = vmrTrace(indexSub,iTrace) * 1.0d-6 &
                                                           * numberDensityAir(indexSub)
            optPropRTMGridS%vmrSubGas(indexSub,iTrace)     = vmrTrace(indexSub,iTrace)
            optPropRTMGridS%XsecSubGas(indexSub,iTrace)    = XsecTrace(indexSub,iTrace)
            optPropRTMGridS%dXsecdTSubGas(indexSub,iTrace) = dXsecdTTrace(indexSub,iTrace)
          end do
          optPropRTMGridS%kabsSubGas(indexSub) = kabsGas(indexSub)
          optPropRTMGridS%kextSubGas(indexSub) = kabsGas(indexSub) + kscaGas(indexSub)
          optPropRTMGridS%kextSubAer(indexSub) = kscaAer(indexSub) + kabsAer(indexSub)
          optPropRTMGridS%kextSubCld(indexSub) = kscaCld(indexSub) + kabsCld(indexSub)

          indexSub = indexSub + 1
        end do ! igaussSub
      end do ! igauss
    end do  ! ibound
    ! fill values for last entry
    index = optPropRTMGridS%RTMnlayer
    indexSub = optPropRTMGridS%RTMnlayerSub
    optPropRTMGridS%kscaSub(indexSub) = ksca(indexSub)
    optPropRTMGridS%kabsSub(indexSub) = kabs(indexSub)
    optPropRTMGridS%kextSub(indexSub) = kabs(indexSub) + ksca(indexSub)
    do iTrace = 1, nTrace
      optPropRTMGridS%ndensGas(index,iTrace)         = vmrTrace(indexSub,iTrace) * 1.0d-6 &
                                                     * numberDensityAir(indexSub)
      optPropRTMGridS%ndensSubGas(indexSub,iTrace)   = vmrTrace(indexSub,iTrace) * 1.0d-6 &
                                                     * numberDensityAir(indexSub)
      optPropRTMGridS%vmrSubGas (indexSub,iTrace)    = vmrTrace(indexSub,iTrace)
      optPropRTMGridS%XsecSubGas(indexSub,iTrace)    = XsecTrace(indexSub,iTrace)
      optPropRTMGridS%dXsecdTSubGas(indexSub,iTrace) = dXsecdTTrace(indexSub,iTrace)
    end do
    optPropRTMGridS%kabsSubGas(indexSub) = kabsGas(indexSub)
    optPropRTMGridS%kextSubGas(indexSub) = kabsGas(indexSub) + kscaGas(indexSub)
    optPropRTMGridS%kextSubAer(indexSub) = kscaAer(indexSub) + kabsAer(indexSub)
    optPropRTMGridS%kextSubCld(indexSub) = kscaCld(indexSub) + kabsCld(indexSub)

    if ( verbose ) then

      write(intermediateFileUnit,*) 'output from getOptPropAtm'
      write(intermediateFileUnit,*)
      write(intermediateFileUnit,*) 'values on retrieval grids'
      write(intermediateFileUnit,'(A,100E12.3)') ' surfaceAlbedo     :', surfAlb_iwave
      write(intermediateFileUnit,'(A,100E12.3)') ' cloudAlbedo       :', cloudAlb_iwave
      do iTrace = 1, ntrace
        write(intermediateFileUnit,'(A,I4)')       ' retr grid iTrace = ', iTrace
        write(intermediateFileUnit,'(A,100E15.6)') ' altitude[km]      :', traceGasS(iTrace)%alt
        write(intermediateFileUnit,'(A,100E15.6)') ' XsecTrace         :', XsecS(iTrace)%Xsec(iwave,:)
        write(intermediateFileUnit,'(A,100E15.6)') ' numDensTrace      :', traceGasS(iTrace)%numDens
        write(intermediateFileUnit,'(A,100E15.6)') ' numDensAir        :', traceGasS(iTrace)%numDensAir
      end do

      do iTrace = 1, ntrace
        write(intermediateFileUnit,'(A,I4)')       ' RTM grid  iTrace = ', iTrace
        write(intermediateFileUnit,'(A,100E15.6)') ' altitude[km]      :', optPropRTMGridS%RTMaltitude(:)
        write(intermediateFileUnit,'(A,100E15.6)') ' numDensTrace      :', optPropRTMGridS%ndensGas(:,iTrace)
      end do

      if (details) then

        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*)     'limiting values for interval boundaries'
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,'(A)') '    alt     kscaAboveGas     kscaBelowGas    kabsAboveGas   kabsBelowGas'
        do ibound = 0, optPropRTMGridS%ninterval
          write(intermediateFileUnit,'(F10.4, 4E15.5)') optPropRTMGridS%intervalBounds(ibound), &
                        OptPropRTMGridS%kscaIntAboveGas(ibound), OptPropRTMGridS%kscaIntBelowGas(ibound), &
                        OptPropRTMGridS%kabsIntAboveGas(ibound), OptPropRTMGridS%kabsIntBelowGas(ibound)
        end do ! ibounds
        write(intermediateFileUnit,'(A)') '    alt     kscaAboveAer     kscaBelowAer    kabsAboveAer   kabsBelowAer'
        do ibound = 0, optPropRTMGridS%ninterval
          write(intermediateFileUnit,'(F10.4, 4E15.5)') optPropRTMGridS%intervalBounds(ibound), &
                        OptPropRTMGridS%kscaIntAboveAer(ibound), OptPropRTMGridS%kscaIntBelowAer(ibound), &
                        OptPropRTMGridS%kabsIntAboveAer(ibound), OptPropRTMGridS%kabsIntBelowAer(ibound)
        end do ! ibounds
        write(intermediateFileUnit,'(A)') '    alt     kscaAboveCld     kscaBelowCld    kabsAboveCld   kabsBelowCld'
        do ibound = 0, optPropRTMGridS%ninterval
          write(intermediateFileUnit,'(F10.4, 4E15.5)') optPropRTMGridS%intervalBounds(ibound), &
                        OptPropRTMGridS%kscaIntAboveCld(ibound), OptPropRTMGridS%kscaIntBelowCld(ibound), &
                        OptPropRTMGridS%kabsIntAboveCld(ibound), OptPropRTMGridS%kabsIntBelowCld(ibound)
        end do ! ibounds


        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*)     'values on RTM grid with phasefCoefAbove'
        write(intermediateFileUnit,'(A)') '    alt        ksca        kabs      phasefCoef-0    phasefCoef-1 ....'
        do ilevel = 0, optPropRTMGridS%RTMnlayer
          write(intermediateFileUnit,'(F10.4, 2E15.5, 200F12.7)') optPropRTMGridS%RTMaltitude(ilevel), &
                                                                  optPropRTMGridS%ksca(ilevel),        &
                                                                  optPropRTMGridS%kabs(ilevel),        &
             ( optPropRTMGridS%phasefCoefAbove (1,1,icoef,ilevel), icoef = 0, optPropRTMGridS%maxExpCoef)
        end do ! ilevel

        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*)     'values on sublayer grid gas + particles'
        write(intermediateFileUnit,'(A)') '    alt     kscaSub     kabsSub'
        do ilevel = 0, optPropRTMGridS%RTMnlayerSub
          write(intermediateFileUnit,'(F10.4, 2E15.5)') optPropRTMGridS%RTMaltitudeSub(ilevel), &
                                                        optPropRTMGridS%kscaSub(ilevel),        &
                                                        optPropRTMGridS%kabsSub(ilevel)
        end do ! ilevel

        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*)     'values on sublayer grid gas'
        write(intermediateFileUnit,'(A)') '    alt       kabsSub '
        do ilevel = 0, optPropRTMGridS%RTMnlayerSub
          write(intermediateFileUnit,'(F10.4, E15.5)') optPropRTMGridS%RTMaltitudeSub(ilevel), &
                                                       optPropRTMGridS%kabsSubGas(ilevel)
        end do ! ilevel

        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*)     'values on sublayer grid particles'
        write(intermediateFileUnit,'(A)') '    alt     kextAer     kextCld'
        do ilevel = 0, optPropRTMGridS%RTMnlayerSub
          write(intermediateFileUnit,'(F10.4, 2F15.8)') optPropRTMGridS%RTMaltitudeSub(ilevel), &
                                                        optPropRTMGridS%kextSubAer(ilevel),     &
                                                        optPropRTMGridS%kextSubCld(ilevel)
        end do ! ilevel
        write(intermediateFileUnit,'(A)') &
           '       alt(km)   pressure(hPa)   temperature(K) numDensAir(molecules/cm-3)    Xsec   weight'
        do ilevel = 0, optPropRTMGridS%RTMnlayerSub
          write(intermediateFileUnit,'(F15.8,2F15.8,2E15.5,F26.8)') optPropRTMGridS%RTMaltitudeSub(ilevel), &
                                                                   pressure(ilevel),         &
                                                                   temperature(ilevel),      &
                                                                   numberDensityAir(ilevel), &
                                                                   XsecTrace (ilevel,1),     &
                                                                   optPropRTMGridS%RTMweightSub(ilevel)
        end do ! ilevel

        write(intermediateFileUnit,*)
        write(intermediateFileUnit,*)     'values for the layers gas + particles'
        write(intermediateFileUnit,'(A)') &
      ' layer  optical thickness     ssa      phasefCoefLay-0 phasefCoefLay-1 ....'
        do ilevel = 1, optPropRTMGridS%RTMnlayer
          write(intermediateFileUnit,'(I6, 2F15.8, 100F17.7)') ilevel,                                       &
                                                                optPropRTMGridS%opticalThicknLay(ilevel),    &
                                                                optPropRTMGridS%ssaLay(ilevel),              &
                     ( optPropRTMGridS%phasefCoefLay(1,1,icoef,ilevel), icoef = 0, optPropRTMGridS%maxExpCoef)
        end do ! ilevel

      end if ! details

    end if ! verbose

! JdH test interpolation, e.g. oscillations due to spline interpolation
!   if ( iwave == 0 ) then
!     write(24, '(15X, 100ES15.6)') ( optPropRTMGridS%RTMaltitude(ilevel), ilevel = 0, optPropRTMGridS%RTMnlayer)
!   end if
!   write(24, '(101ES15.6)') wavelHRS%wavel(iwave), &
!      (optPropRTMGridS%kabs(ilevel)/optPropRTMGridS%kabs(0), ilevel = 0, optPropRTMGridS%RTMnlayer)

    end subroutine getOptPropAtm


    subroutine getOptPropAtmSim (errS, nTrace, gasPTS, traceGasS, OptPropRTMGridS )

    ! When the simulation is replaced with the disamar.sim file so that different retrievals
    ! can be tested whithout the need to repeat the simulation some arrays are not filled.
    ! The information in those arrays is, however, required when, for ozone profile retrieval,
    ! properties for subcolumns pertaining to the simulation is to be calculated and written to output.

    ! This subroutine getOptPropAtmSim is a vastly simplified version of subroutine getOptPropAtm
    ! which fills arrays related to the number density so that proper simulated subcolumns can
    ! be calculated when the disamar.sim file is used.

    implicit none

    type(errorType),               intent(inout) :: errS
    integer,                       intent(in)    :: nTrace     ! number of trace gases

    type(gasPTType),               intent(in)    :: gasPTS                    ! specification of the gaseous atmosphere
    type(traceGasType),            intent(in)    :: traceGasS(nTrace)         ! specification of trace gases
    type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS           ! optical properties on RTM grid (simulation)

    ! local
    integer  :: ilevel, iTrace

    ! tempory arrays for interpolation
    real(8)  :: lnpressureGrid(0:gasPTS%npressure)
    real(8)  :: SDlnpressure  (0:gasPTS%npressure)
    real(8)  :: SDtemperature (0:gasPTS%npressure)

    ! tempory arrays for interpolation; here the length depends on the trace gas
    real(8), allocatable  :: lnvmrTraceGrid(:)
    real(8), allocatable  :: SDlnvmrTrace(:)

    ! arrays on the column altitude grid (p in hPa, T in K, vmr in ppmv, ndens in mol cm-3)
    real(8)  :: pressureCol(optPropRTMGridS%nGaussCol)
    real(8)  :: lnpressureCol(optPropRTMGridS%nGaussCol)                  ! natural logarithm of pressure
    real(8)  :: temperatureCol(optPropRTMGridS%nGaussCol)
    real(8)  :: vmrTraceCol(optPropRTMGridS%nGaussCol,nTrace)
    real(8)  :: lnvmrTraceCol(optPropRTMGridS%nGaussCol,nTrace) ! natural logarithm of volume mixing ratio column grid
    real(8)  :: numberDensityAirCol(optPropRTMGridS%nGaussCol)           ! number density of air on column grid
    real(8)  :: numberDensityTraceCol(optPropRTMGridS%nGaussCol,nTrace)  ! number density of trace gas on column grid


    integer  :: statusSpline, statusSplint

    integer  :: allocStatus, sumAllocStatus
    integer  :: deallocStatus, sumdeAllocStatus


    ! calculate values for column grid
    lnpressureGrid = log(gasPTS%pressure)
    call spline(errS, gasPTS%alt, lnpressureGrid, SDlnpressure , statusSpline)
    call spline(errS, gasPTS%alt, gasPTS%temperature, SDtemperature , statusSpline)
    do ilevel = 1, optPropRTMGridS%nGaussCol
      lnpressureCol (ilevel) = splint(errS, gasPTS%alt, lnpressureGrid, SDlnpressure , &
                                optPropRTMGridS%Colaltitude(ilevel), statusSplint)
      if ( gasPTS%useLinInterp ) then
        temperatureCol(ilevel) = splintLin(errS, gasPTS%alt, gasPTS%temperature, &
                                     optPropRTMGridS%Colaltitude(ilevel), statusSplint)
      else
        temperatureCol(ilevel) = splint(errS, gasPTS%alt, gasPTS%temperature, SDtemperature, &
                                     optPropRTMGridS%Colaltitude(ilevel), statusSplint)
      end if
    end do

    pressureCol(:) = exp(lnpressureCol(:))

    numberDensityAirCol(:) = pressureCol(:) / temperatureCol(:) / 1.380658d-19 ! in molecules cm-3

    do iTrace = 1, nTrace

      ! allocate arrays

      allocStatus    = 0
      sumAllocStatus = 0

      allocate( lnvmrTraceGrid  (0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus
      allocate( SDlnvmrTrace    (0:traceGasS(iTrace)%nalt), STAT = allocStatus )
      sumAllocStatus = sumAllocStatus + allocStatus

      if (sumAllocStatus /= 0) then
        call logDebug('ERROR: allocation failed in subroutine getOptPropAtmSim')
        call logDebugI('for iTrace = ', iTrace)
        call logDebug('in module propAtmosphereModule')
        call mystop(errS, 'stopped because allocation failed')
        if (errorCheck(errS)) return
      end if

      lnvmrTraceGrid   = log(traceGasS(iTrace)%vmr)
      if ( .not. traceGasS(iTrace)%useLinInterp ) then
        call spline(errS, traceGasS(iTrace)%alt, lnvmrTraceGrid,   SDlnvmrTrace  , statusSpline)
        if (errorCheck(errS)) return
      end if

      ! calculate the number density for the column altitude grid

      if ( traceGasS(iTrace)%useLinInterp ) then
        do ilevel = 1, optPropRTMGridS%nGaussCol
          lnvmrTraceCol(ilevel,iTrace)   = splintLin(errS, traceGasS(iTrace)%alt, lnvmrTraceGrid,   &
                                           optPropRTMGridS%Colaltitude(ilevel), statusSplint)
        end do
      else
        do ilevel = 1, optPropRTMGridS%nGaussCol
          lnvmrTraceCol(ilevel,iTrace)   = splint(errS, traceGasS(iTrace)%alt, lnvmrTraceGrid, SDlnvmrTrace,     &
                                           optPropRTMGridS%Colaltitude(ilevel), statusSplint)
        end do
      end if
      vmrTraceCol  (:, iTrace) = exp( lnvmrTraceCol  (:,iTrace) )

      ! vmr in ppmv => factor 1.0d-6
      numberDensityTraceCol  (:, iTrace) = vmrTraceCol  (:, iTrace) * numberDensityAirCol(:) * 1.0d-6

      ! deallocate arrays
      deallocStatus    = 0
      sumdeAllocStatus = 0

      deallocate( lnvmrTraceGrid, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus
      deallocate( SDlnvmrTrace, STAT = deallocStatus )
      sumdeAllocStatus = sumdeAllocStatus + deallocStatus

      if (sumdeAllocStatus /= 0) then
        call logDebug('ERROR: deallocation failed in subroutine getOptPropAtmSim')
        call logDebugI('for iTrace = ', iTrace)
        call logDebug('in module propAtmosphereModule')
        call mystop(errS, 'stopped because deallocation failed')
        if (errorCheck(errS)) return
      end if

    end do ! iTrace

    ! assign values
    if ( associated(optPropRTMGridS%Colndens) )    optPropRTMGridS%Colndens(:,:)   = numberDensityTraceCol(:,:)
    if ( associated(optPropRTMGridS%ColndensAir) ) optPropRTMGridS%ColndensAir(:)  = numberDensityAirCol(:)

    end subroutine getOptPropAtmSim

end module propAtmosphereModule
