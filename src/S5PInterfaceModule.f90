!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

module S5PInterfaceModule

    ! This module contains several subroutines that are used by the operational
    ! version of DISAMAR as used in the Sentinel 5P TROPOMI L2 data processors.
    !
    ! The subroutines in this module are called by the C++ part of the processors.
    ! The C++ part of the interface is coded in L2P_WrapperDisamar.cpp.

    use, intrinsic         :: iso_c_binding              ! for making the interface to C a bit more friendly.

    use DISAMARModule, only : init, retrieve2 => retrieve, cleanupretrieval
    use verifyConfigFileModule, only : verifyConfigFile
    use dataStructures
    use staticDataModule
    use netcdfModule
    use pqf_module
    use mathTools,    only: spline, splint

    implicit none

    contains

    ! -------------------------------------------------------------------------------

    subroutine loadO2xsection(o2xsectionFile, status)

    ! load the oxygen A-band parametrization of the cross sections into the XsecLUTType
    ! and the wavelHRType.

        implicit none

        character*(*)             :: o2xsectionFile
        integer                   :: status, allocStatus, sumAllocStatus

        character*(256)           :: str

        status = 0
        if (o2xsectionFile == '') return

        write(str,'(A)') "Loading file '" // trim(o2xsectionFile) // "'"
        call fortranlog(trim(str), len(trim(str)), 3)


        ! allocate data structures within static
        sumAllocStatus = 0
        allocate(staticS%o2xsection, stat=allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate(staticS%o2o2xsection, stat=allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate(staticS%hr_wavel, stat=allocStatus)
        sumAllocStatus = sumAllocStatus + allocStatus
        if (sumAllocStatus /= 0) goto 997

        staticS%o2xsection%createXsecPolyLUT = .false.
        staticS%o2o2xsection%createXsecPolyLUT = .false.

        status = NetCDF_open(trim(o2xsectionFile), nf90_nowrite)
        if (status /= 0) goto 997

        ! get dimensions
        status = NetCDF_get_dimension("wavelength", staticS%o2xsection%nwavelHR)
        if (status /= 0) goto 996
        staticS%hr_wavel%nwavel = staticS%o2xsection%nwavelHR
        staticS%o2o2xsection%nwavelHR = staticS%o2xsection%nwavelHR

        status = NetCDF_get_dimension("pressure", staticS%o2xsection%ncoeff_lnp_LUT)
        if (status /= 0) goto 996
        staticS%o2o2xsection%ncoeff_lnp_LUT = staticS%o2xsection%ncoeff_lnp_LUT

        status = NetCDF_get_dimension("temperature", staticS%o2xsection%ncoeff_lnT_LUT)
        if (status /= 0) goto 996
        staticS%o2o2xsection%ncoeff_lnT_LUT = staticS%o2xsection%ncoeff_lnT_LUT


        ! get data
        status = NetCDF_get_var_d1("wavelength", staticS%hr_wavel%wavel)
        if (status /= 0) goto 996

        status = NetCDF_get_var_d1("gaussianWeights", staticS%hr_wavel%weights)
        if (status /= 0) goto 996

        status = NetCDF_get_var_d3("oxygenReferenceO2", staticS%o2xsection%coeff_lnTlnp_LUT)
        if (status /= 0) goto 996

        ! get attributes
        status = NetCDF_get_var_att_d("pressure", "pressureMin", staticS%o2xsection%pmin_LUT)
        if (status /= 0) goto 996
        staticS%o2o2xsection%pmin_LUT = staticS%o2xsection%pmin_LUT

        status = NetCDF_get_var_att_d("pressure", "pressureMax", staticS%o2xsection%pmax_LUT)
        if (status /= 0) goto 996
        staticS%o2o2xsection%pmax_LUT = staticS%o2xsection%pmax_LUT

        status = NetCDF_get_var_att_d("temperature", "temperatureMin", staticS%o2xsection%Tmin_LUT)
        if (status /= 0) goto 996
        staticS%o2o2xsection%Tmin_LUT = staticS%o2xsection%Tmin_LUT

        status = NetCDF_get_var_att_d("temperature", "temperatureMax", staticS%o2xsection%Tmax_LUT)
        if (status /= 0) goto 996
        staticS%o2o2xsection%Tmax_LUT = staticS%o2xsection%Tmax_LUT

        status = NetCDF_get_var_d3("oxygenReferenceO2O2", staticS%o2o2xsection%coeff_lnTlnp_LUT)
        if (status /= 0) goto 996

996     continue
        status = NetCDF_close()
        if (status /= 0) goto 996

997     continue

        staticS%o2XsectionLoaded = .true.

    end subroutine loadO2xsection


    ! -------------------------------------------------------------------------------

    subroutine loadSolar(solarFile, status)

    ! Load the high resolution solar reference file. Note that this subroutine loads
    ! the reference spectrum with units 'mol/s/nm/m2/sr'. These units must be consistent
    ! with the measured radiance and irradiance spectra.
    ! (we may want to keep this optional, like the polarization correction LUT, for easier GOME-2 processing)

        implicit none

        character*(*)             :: solarFile
        integer                   :: status

        character*(256)           :: str

        status = 0

        write(str,'(A)') "Loading file '" // trim(solarFile) // "'"
        call fortranlog(trim(str), len(trim(str)), 3)

        status = NetCDF_open(trim(solarFile), nf90_nowrite)
        if (status /= 0) goto 999

        status = NetCDF_get_dimension("wavelength_hr", staticS%irrNum)
        if (status /= 0) goto 998

        status = NetCDF_get_var_d1("wavelength_hr", staticS%irrWavHR)
        if (status /= 0) goto 998

        status = NetCDF_get_var_d1("irradiance_flux", staticS%irrFlux)
        if (status /= 0) goto 998

998     continue
        status = NetCDF_close()
        if (status /= 0) goto 998

999     continue

    end subroutine loadSolar

    ! -------------------------------------------------------------------------------

    subroutine loadIsrf(isrfFile, status)

    ! Load the ISRF from netCDF file, overruling the code within DISAMAR to load from
    ! ASCII file (or use a Gaussian/flat topped Gaussian ISRF).
    ! (we may want to keep this optional, like the polarization correction LUT, for easier GOME-2 processing)

        implicit none

        character*(*)       :: isrfFile
        integer             :: status

        character*(256)     :: str
        integer             :: i, j
        integer             :: iband
        integer             :: status2

        if (isrfFile == '') return

        status = 0

        write(str,'(A)') "Loading file '" // trim(isrfFile) // "'"
        call fortranlog(trim(str), len(trim(str)), 3)

        allocate(staticS%isrfS(8), stat=status)
        if (status /= 0) goto 999

        status = NetCDF_open(trim(isrfFile), nf90_nowrite)
        if (status /= 0) goto 999

        do iband = 1, 6

            write(str,'(A,I1)') "/band_", iband

            status = NetCDF_set_group(trim(str))
            if (status /= 0) goto 998

            status = NetCDF_get_dimension("ground_pixel", staticS%isrfS(iband)%numGroundPixel)
            if (status /= 0) goto 998

            status = NetCDF_get_dimension("central_wavelength", staticS%isrfS(iband)%numCentralWavelength)
            if (status /= 0) goto 998

            status = NetCDF_get_dimension("delta_wavelength", staticS%isrfS(iband)%numDeltaWavelength)
            if (status /= 0) goto 998

            status = NetCDF_get_var_i1("ground_pixel", staticS%isrfS(iband)%groundPixel)
            if (status /= 0) goto 998

            status = NetCDF_get_var_i1("central_wavelength", staticS%isrfS(iband)%centralWavelength)
            if (status /= 0) goto 998

            status = NetCDF_get_var_f1("delta_wavelength", staticS%isrfS(iband)%deltaWavelength)
            if (status /= 0) goto 998

            status = NetCDF_get_var_f3("isrf", staticS%isrfS(iband)%isrf)
            if (status /= 0) goto 998

        end do

998     continue
        status2 = NetCDF_close()
        if (status2 /= 0) status = status2

        staticS%isrfLoaded = .true.

999     continue

    end subroutine loadIsrf

    ! -------------------------------------------------------------------------------

    subroutine loadPolCor(polCorFile, status)

    ! Load the polarization and Rman scattering correction lookup table
    ! (ozone profile only, pass empty string to skip)

        implicit none

        character*(*)             :: polCorFile
        integer                   :: status

        character*(256)           :: str
        integer                   :: iBand
        integer                   :: status2

        status = 0

        if (polCorFile == '') return

        write(str,'(A)') "Loading file '" // trim(polCorFile) // "'"
        call fortranlog(trim(str), len(trim(str)), 3)

        allocate(staticS%polCorrectionRetrS(2), stat=status)
        if (status /= 0) goto 999

        status = NetCDF_open(trim(polCorFile), nf90_nowrite)
        if (status /= 0) goto 999

        do iBand = 1, 2

            status = NetCDF_get_dimension("surface_albedo", staticS%polCorrectionRetrS(iband)%nsurfaceAlbedo)
            if (status /= 0) goto 998

            status = NetCDF_get_var_p1("surface_albedo", staticS%polCorrectionRetrS(iband)%surfaceAlbedo)
            if (status /= 0) goto 998

            status = NetCDF_get_dimension("mu_0", staticS%polCorrectionRetrS(iband)%nmu0)
            if (status /= 0) goto 998

            status = NetCDF_get_var_p1("mu_0", staticS%polCorrectionRetrS(iband)%mu0)
            if (status /= 0) goto 998

            status = NetCDF_get_dimension("mu", staticS%polCorrectionRetrS(iband)%nmu)
            if (status /= 0) goto 998

            status = NetCDF_get_var_p1("mu", staticS%polCorrectionRetrS(iband)%mu)
            if (status /= 0) goto 998

            status = NetCDF_get_dimension("surface_pressure", staticS%polCorrectionRetrS(iband)%nsurfacePressure)
            if (status /= 0) goto 998

            status = NetCDF_get_var_p1("surface_pressure", staticS%polCorrectionRetrS(iband)%surfacePressure)
            if (status /= 0) goto 998

            status = NetCDF_get_dimension("latitude", staticS%polCorrectionRetrS(iband)%nlatitude)
            if (status /= 0) goto 998

            status = NetCDF_get_var_p1("latitude", staticS%polCorrectionRetrS(iband)%latitude)
            if (status /= 0) goto 998

            status = NetCDF_get_dimension("ozone_column", staticS%polCorrectionRetrS(iband)%nozoneColumn)
            if (status /= 0) goto 998

            status = NetCDF_get_var_p1("ozone_column", staticS%polCorrectionRetrS(iBand)%ozoneColumn)
            if (status /= 0) goto 998

        end do

        status = NetCDF_set_group("/band_1")
        if (status /= 0) goto 998

        status = NetCDF_get_dimension("wavelength", staticS%polCorrectionRetrS(1)%nwavel)
        if (status /= 0) goto 998

        status = NetCDF_get_var_p1("wavelength", staticS%polCorrectionRetrS(1)%wavel)
        if (status /= 0) goto 998

        status = NetCDF_get_var_p7("polcor0", staticS%polCorrectionRetrS(1)%correctionLUT0)
        if (status /= 0) goto 998

        status = NetCDF_get_var_p7("polcor1", staticS%polCorrectionRetrS(1)%correctionLUT1)
        if (status /= 0) goto 998

        status = NetCDF_get_var_p7("polcor2", staticS%polCorrectionRetrS(1)%correctionLUT2)
        if (status /= 0) goto 998

        status = NetCDF_set_group("/band_2")
        if (status /= 0) goto 998

        status = NetCDF_get_dimension("wavelength", staticS%polCorrectionRetrS(2)%nwavel)
        if (status /= 0) goto 998

        status = NetCDF_get_var_p1("wavelength", staticS%polCorrectionRetrS(2)%wavel)
        if (status /= 0) goto 998

        status = NetCDF_get_var_p7("polcor0", staticS%polCorrectionRetrS(2)%correctionLUT0)
        if (status /= 0) goto 998

        status = NetCDF_get_var_p7("polcor1", staticS%polCorrectionRetrS(2)%correctionLUT1)
        if (status /= 0) goto 998

        status = NetCDF_get_var_p7("polcor2", staticS%polCorrectionRetrS(2)%correctionLUT2)
        if (status /= 0) goto 998

998     continue
        status2 = NetCDF_close()
        if (status2 /= 0) status = status2

        staticS%polCorrectionLoaded = .true.

999     continue

    end subroutine loadPolCor

    ! -------------------------------------------------------------------------------

    subroutine initialize(configFile, solarFile, isrfFile, polCorFile, o3File, o2xsectionFile, dataPath, operational, replaceBounds, writeResults, status)

    ! Global initialization routine (call once)
    !

        implicit none

        character*(*)             :: configFile   ! Master DISAMAR configuration file
        character*(*)             :: solarFile    ! netCDF file with high-resolution solar reference spectrum
        character*(*)             :: isrfFile     ! netCDF file with ISRF / if empty we may want to set slitfunctionFileName in the slitFunctionType.
        character*(*)             :: polCorFile   ! netCDF file with polarization & Raman scattering correction LUT (can be '')
        character*(*)             :: o3File       ! netCDF file with ozone climatology (TODO)
        character*(*)             :: o2xsectionFile ! netCDF file with LUT for O2 and O2-O2 for the A-band (B-band to be generated) (can be '' for O3 profile)
        character*(*)             :: dataPath     ! path to static input data (data/O2A_LISA_RMF.dat, data/O2A_LISA_SDF.dat & HITRAN input file)
        integer                   :: operational  ! Set execution mode (influences chattiness)
        integer                   :: replaceBounds! Replace Cloud/Aerosol Interval Bounds (yes for O3 Profile, no for Aerosol Layer Height)
        integer                   :: writeResults ! Write DISAMAR output file; should only be used single threaded
        integer                   :: status       ! return status

        type(errorType)           :: errorS
        character*(256)           :: str

        status = 0
        call errorInit(errorS)
        call errorSetInteractive(errorS, .true.)

        call claimStaticS(errorS, staticS)
        if (errorCheck(errorS)) then
            status = 1
            return
        end if

        staticS%operational = operational
        staticS%configFileName = configFile
        staticS%dataPath = dataPath
        staticS%replaceCldAerIntervalBounds = replaceBounds
        staticS%writeResults = writeResults
        staticS%isrfLoaded = .false.
        staticS%polCorrectionLoaded = .false.
        staticS%o2XsectionLoaded = .false.

        O2_LISA_RMFXsecFileName = trim(staticS%dataPath) // 'data/O2A_LISA_RMF.dat'
        O2_LISA_SDFXsecFileName = trim(staticS%dataPath) // 'data/O2A_LISA_SDF.dat'
        O2_XsecFileName         = trim(staticS%dataPath) // O2_XsecFileName

        staticS%xsnum = 0

        call loadSolar(solarFile, status)
        if (status /= 0) return

        call loadIsrf(isrfFile, status)
        if (status /= 0) return

        call loadPolCor(polCorFile, status)
        if (status /= 0) return

        !call loadO2xsection(o2xsectionFile, status)
        !if (status /= 0) return

        write(str,'(A)') "Loading file '" // trim(configFile) // "'"
        call fortranlog(trim(str), len(trim(str)), 3)

    end subroutine initialize

    ! -------------------------------------------------------------------------------

    subroutine finalize()

    ! cleanup, call after all retrievals.

        implicit none

        integer i

        deallocate(staticS%irrWavHR)
        deallocate(staticS%irrFlux)

        do i = 1, staticS%xsnum
            if (associated(staticS%xs(i)%wavelength)) then
                deallocate(staticS%xs(i)%wavelength)
            endif
            if (associated(staticS%xs(i)%a1)) then
                deallocate(staticS%xs(i)%a1)
            endif
            if (associated(staticS%xs(i)%a2)) then
                deallocate(staticS%xs(i)%a2)
            endif
            if (associated(staticS%xs(i)%a3)) then
                deallocate(staticS%xs(i)%a3)
            endif
        end do

        if (staticS%polCorrectionLoaded) then
            do i = 1, 2
                if (associated(staticS%polCorrectionRetrS(i)%wavel)) then
                    deallocate(staticS%polCorrectionRetrS(i)%wavel)
                endif
                if (associated(staticS%polCorrectionRetrS(i)%correctionLUT0)) then
                    deallocate(staticS%polCorrectionRetrS(i)%correctionLUT0)
                endif
                if (associated(staticS%polCorrectionRetrS(i)%correctionLUT1)) then
                    deallocate(staticS%polCorrectionRetrS(i)%correctionLUT1)
                endif
                if (associated(staticS%polCorrectionRetrS(i)%correctionLUT2)) then
                    deallocate(staticS%polCorrectionRetrS(i)%correctionLUT2)
                endif
            end do
            deallocate(staticS%polCorrectionRetrS)
            staticS%polCorrectionLoaded = .false.
        endif

        if (staticS%isrfLoaded) then
            do i = 1, 6
                if (associated(staticS%isrfS(i)%groundPixel)) then
                    deallocate(staticS%isrfS(i)%groundPixel)
                endif
                if (associated(staticS%isrfS(i)%centralWavelength)) then
                    deallocate(staticS%isrfS(i)%centralWavelength)
                endif
                if (associated(staticS%isrfS(i)%deltaWavelength)) then
                    deallocate(staticS%isrfS(i)%deltaWavelength)
                endif
                if (associated(staticS%isrfS(i)%isrf)) then
                    deallocate(staticS%isrfS(i)%isrf)
                endif
            enddo
            deallocate(staticS%isrfS)
            staticS%isrfLoaded = .false.
        endif

        if (associated(staticS%irrWavHR)) then
            deallocate(staticS%irrWavHR)
        endif
        if (associated(staticS%irrFlux)) then
            deallocate(staticS%irrFlux)
        endif

        if (associated(staticS%hr_wavel)) then
            if (associated(staticS%hr_wavel%wavel)) deallocate(staticS%hr_wavel%wavel)
            if (associated(staticS%hr_wavel%weights)) deallocate(staticS%hr_wavel%weights)
            deallocate(staticS%hr_wavel)
        end if

        !deallocate(staticS%climO3)

        if (associated(staticS%o2xsection)) then
            if (associated(staticS%o2xsection%coeff_lnTlnp_LUT)) deallocate(staticS%o2xsection%coeff_lnTlnp_LUT)
            deallocate(staticS%o2xsection)
        end if
        if (associated(staticS%o2o2xsection)) then
            if (associated(staticS%o2o2xsection%coeff_lnTlnp_LUT)) deallocate(staticS%o2o2xsection%coeff_lnTlnp_LUT)
            deallocate(staticS%o2o2xsection)
        end if

        deallocate(staticS)

    end subroutine finalize

    !  -------------------------------------------------------------------------------

    subroutine setInputO3Profile(numtim, numlat, numlev, numtco3, time, lat, lev, tco3, o3, status)

    ! set ozone climatology for profile retrieval

        implicit none

        integer                   :: numtim
        integer                   :: numlat
        integer                   :: numlev
        integer                   :: numtco3
        real(4)                   :: time(1:numtim)
        real(4)                   :: lat(1:numlat)
        real(4)                   :: lev(1:numlev)
        real(4)                   :: tco3(1:numtco3)
        real(4)                   :: o3(1:numlat,1:numlev,1:numtim,1:numtco3)
        integer                   :: status

        integer i, j, k, l

        allocate(staticS%climO3(1:numlev,1:numtco3,1:numlat,1:numtim), STAT=status)
        if (status .ne. 0) return

        staticS%climNumTim = numtim
        staticS%climNumLat = numlat
        staticS%climNumLev = numlev
        staticS%climNumTCO3 = numtco3
        staticS%climTime(1:numtim) = time(1:numtim)
        staticS%climLat(1:numlat) = lat(1:numlat)
        staticS%climLev(1:numlev) = lev(1:numlev)
        staticS%climTCO3(1:numtco3) = tco3(1:numtco3)

        do i = 1, numlev
            do j = 1, numtco3
                do k = 1, numtim
                    do l = 1, numlat
                        staticS%climO3(i, j, l, k) = o3(l, i, k, j)
                    end do
                end do
            end do
        end do

        status = 0

    end subroutine setInputO3Profile

    ! -------------------------------------------------------------------------------

    subroutine setCrossSection(name, num, wavelength, a1, a2, a3, status)

    ! set ozone cross sections

        implicit none

        character*(*)             :: name
        integer                   :: num
        real(8)                   :: wavelength(*)
        real(8)                   :: a1(*)
        real(8)                   :: a2(*)
        real(8)                   :: a3(*)
        integer                   :: status

        type(errorType)           :: errorS

        status = 0
        call errorInit(errorS)
        call errorSetInteractive(errorS, .true.)

        if (staticS%xsnum .ge. xsmax) then
          status = 1
          return
        end if

        staticS%xsnum = staticS%xsnum + 1
        staticS%xs(staticS%xsnum)%name = name
        staticS%xs(staticS%xsnum)%numwav = num
        allocate(staticS%xs(staticS%xsnum)%wavelength(num))
        staticS%xs(staticS%xsnum)%wavelength(1:num) = wavelength(1:num)
        allocate(staticS%xs(staticS%xsnum)%a1(num))
        staticS%xs(staticS%xsnum)%a1(1:num) = a1(1:num)
        allocate(staticS%xs(staticS%xsnum)%a2(num))
        staticS%xs(staticS%xsnum)%a2(1:num) = a2(1:num)
        allocate(staticS%xs(staticS%xsnum)%a3(num))
        staticS%xs(staticS%xsnum)%a3(1:num) = a3(1:num)

    end subroutine setCrossSection

    ! ------------------------------------------------------------------------------

    subroutine prepare(globalS)

        !

        implicit none

        type(globalType), pointer :: globalS

        type(errorType)           :: errorS

        integer numtim, numlat, numlev, numtco3
        integer status

        call errorInit(errorS)
        call errorSetInteractive(errorS, .true.)

        call claimGlobalS(errorS, globalS)
        if (errorCheck(errorS)) then
            call errorWrite(errorS)
            return
        end if

        globalS%processingQF = PQF_S_SUCCESS

    end subroutine prepare

    ! -------------------------------------------------------------------------------

    subroutine retrieve(globalS)

        ! perform retrieval

        implicit none

        type(globalType), pointer :: globalS

        call errorInit(globalS%errorS)
        call errorSetInteractive(globalS%errorS, .true.)

        call init(globalS%errorS, globalS)
        if (errorCheck(globalS%errorS)) then
            return
        end if

        call retrieve2(globalS%errorS, globalS)
        if (errorCheck(globalS%errorS)) then
            return
        end if

        ! Some O3 values or O3 precision values could be Nan. In that case the pixel is rejected.
        ! The processing quality flag is set to assertion error.
        call checkPixel(globalS)
        if (isError(globalS)) return

    end subroutine retrieve

    !  -------------------------------------------------------------------------------

    subroutine setInputGeneric(globalS, sza, saa, ina, iaa, surfpres, surfalt, numlev, &
                               pres, temp, numlev2, pres2, numband, yday, latitude, status)

        ! Set generic input parameters for an input spectrum

        implicit none

        type(globalType), pointer :: globalS
        real(4)                   :: sza         ! solar zenith angle
        real(4)                   :: saa         ! solar azimuth angle (OMI/GOME2 definition, degrees east of north)
        real(4)                   :: ina         ! instrument nadir angle (viewing zenith angle)
        real(4)                   :: iaa         ! instrument azimuth angle (OMI/GOME2 definition, degrees east of north)
        real(4)                   :: surfpres    ! surface pressure (hPa)
        real(4)                   :: surfalt     ! surface elevation (m)
        integer                   :: numlev      ! number of levels in PT profile
        real(4)                   :: pres(*)     ! pressure levels (1:numlev)
        real(4)                   :: temp(*)     ! temperature (K) (1:numlev)
        integer                   :: numlev2     ! number of levels in input (a priori) trace gas profile
        real(4)                   :: pres2(*)    ! pressure levels of trace gas profile
        integer                   :: numband     ! number of input bands
        integer                   :: yday        ! day of year
        real(4)                   :: latitude    ! latitude of pixel
        integer                   :: status

        if (numband .gt. maxInputBands) then
            status = 1
            return
        end if

        if (numlev .gt. maxInputLevels) then
            status = 1
            return
        end if

        if (numlev2 .gt. maxInputLevels) then
            status = 1
            return
        end if

        globalS%inputS%numBands = numband
        globalS%inputS%solarZenithAngle = sza
        globalS%inputS%solarAzimuthAngle = saa
        globalS%inputS%instrumentNadirAngle = ina
        globalS%inputS%instrumentAzimuthAngle = iaa
        globalS%inputS%surfacePressure = surfpres
        globalS%inputS%surfaceAltitude = surfalt
        globalS%inputS%latitude = latitude
        globalS%inputS%yday = yday

        ! temperature profile
        globalS%inputS%numLevels = numlev
        globalS%inputS%temperature(1:numlev) = temp(1:numlev)
        globalS%inputS%pressure(1:numlev) = pres(1:numlev)

        ! trace gass profile
        globalS%inputS%numLevels2 = numlev2
        globalS%inputS%pressure2(1:numlev2) = pres2(1:numlev2)

        status = 0

    end subroutine setInputGeneric

    !  -------------------------------------------------------------------------------

    subroutine setInputBand(globalS, band, &
                            numrad, radwav, rad, raderr, radnoi, &
                            numirr, irrwav, irr, irrerr, irrnoi, &
                            cloudFraction, cloudAlbedo, cloudPressure, &
                            nSurfaceAlbedo, surfaceAlbedo, surfaceAlbedoWav, &
                            isrfband, isrfpixel, status)
        ! Set input spectrum

        implicit none

        type(globalType), pointer :: globalS
        integer                   :: band                     ! Which band (1, ...)
        integer                   :: numrad                   ! number of points in radiance spectrum
        real(4)                   :: radwav(*)                ! wavelengths for the radiance spectrum
        real(4)                   :: rad(*)                   ! radiances
        real(4)                   :: raderr(*)                ! radiance error (propagation of calibration errors)
        real(4)                   :: radnoi(*)                ! photon shot noise of radiance
        integer                   :: numirr                   ! wavelengths for the irradiance spectrum
        real(4)                   :: irrwav(*)                ! wavelengths for the irradiance spectrum
        real(4)                   :: irr(*)                   ! irradiances
        real(4)                   :: irrerr(*)                ! irradiance error (propagation of calibration errors)
        real(4)                   :: irrnoi(*)                ! photon shot noise of irradiance
        real(4)                   :: cloudFraction            ! (FRESCO) cloud fraction
        real(4)                   :: cloudAlbedo              ! (FRESCO) cloud albedo (0.8)
        real(4)                   :: cloudPressure            ! (FRESCO) cloud pressure (hPa)
        integer                   :: nSurfaceAlbedo           ! number of surface albedo values
        real(4)                   :: surfaceAlbedo(*)         ! surface albedo values
        real(4)                   :: surfaceAlbedoWav(*)      ! wavelength of the surface albedo
        integer                   :: isrfband                 ! band number for ISRF
        integer                   :: isrfpixel                ! pixel number for ISRF
        integer                   :: status

        integer i

        if (numrad .gt. maxInputWavel) then
            status = 1
            return
        end if

        if (numirr .gt. maxInputWavel) then
            status = 1
            return
        end if

        if (nSurfaceAlbedo .gt. maxInputAlbedo) then
            status = 1
            return
        end if

        if (band .gt. globalS%inputS%numBands) then
            status = 1
            return
        end if

        globalS%inputS%cloudFraction(band) = cloudFraction
        globalS%inputS%cloudAlbedo(band) = cloudAlbedo
        globalS%inputS%cloudPressure(band) = cloudPressure

        globalS%inputS%numRadiances(band) = numrad
        globalS%inputS%radiance(band,1:numrad) = rad(1:numrad)
        globalS%inputS%radianceError(band,1:numrad) = raderr(1:numrad)
        globalS%inputS%radianceNoise(band,1:numrad) = radnoi(1:numrad)
        globalS%inputS%radianceWavelength(band,1:numrad) = radwav(1:numrad)

        globalS%inputS%numIrradiances(band) = numirr
        globalS%inputS%irradiance(band,1:numirr) = irr(1:numirr)
        globalS%inputS%irradianceError(band,1:numirr) = irrerr(1:numirr)
        globalS%inputS%irradianceNoise(band,1:numirr) = irrnoi(1:numirr)
        globalS%inputS%irradianceWavelength(band,1:numirr) = irrwav(1:numirr)

        globalS%inputS%numAlbedos(band) = 2
        globalS%inputS%surfaceAlbedo(band,1) = surfaceAlbedo(1)
        globalS%inputS%surfaceAlbedo(band,2) = surfaceAlbedo(1)
        globalS%inputS%surfaceAlbedoWav(band,1) = surfaceAlbedoWav(1)
        globalS%inputS%surfaceAlbedoWav(band,2) = surfaceAlbedoWav(1)

        globalS%inputS%isrfband(band) = isrfband
        globalS%inputS%isrfpixel(band) = isrfpixel + 1

        status = 0

    end subroutine setInputBand

    ! -------------------------------------------------------------------------------

    subroutine setAbortFlag(abortFlag)

        integer :: abortFlag

        character*(256) :: str

        if (associated(staticS)) then
            staticS%abortFlag = abortFlag
            write(str,'(A)') "Aborting DISAMAR"
            call fortranlog(trim(str), len(trim(str)), 3)
        end if

    end subroutine setAbortFlag

    ! -------------------------------------------------------------------------------

#if defined(MCS)
    function disamar_set_static_input_d(key, keylen, ndims, dims, val, staticS)
        implicit none
        integer(C_INT), value                :: keylen
        CHARACTER(len=keylen, KIND=C_CHAR)   :: key
        integer(C_INT), value                :: ndims
        integer(C_INT), intent(in)           :: dims(ndims)
        type(c_ptr), intent(in), value       :: val     ! on C-side: double*
        type(staticType)                     :: staticS ! on C-side: void*

        integer(kind=C_INT)                  :: disamar_set_static_input_d

        real(8), pointer                     :: scalar => null()
        integer                              :: i, allocStatus
        character*(*)                        :: str


        write(str,'(A,A,A)') "disamar_set_static_input_d(", key, ")"
        call fortranlog(trim(str), len(trim(str)), 1)

        disamar_set_static_input_d = PQF_S_SUCCESS

        if (.not. c_associated(val)) then
            write(str,'(A)') "input value must not be NULL"
            call fortranlog(trim(str), len(trim(str)), 4)
            disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
            return
        end if

        select case (trim(key))
            case ("highres_wavelength")
                if (ndims /= 1) then
                    write(str,'(A)') "highres_wavelength must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%irrNum = dims(1)
                if (associated(staticS%irrWavHR)) then
                    deallocate(staticS%irrWavHR)
                    staticS%irrWavHR => null()
                end if
                call C_F_POINTER(val, staticS%irrWavHR, [staticS%irrNum])

            case ("highres_solar")
                if (ndims /= 1) then
                    write(str,'(A)') "highres_solar must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (dims(1) /= staticS%irrNum) then
                    write(str,'(A)') "highres_solar must have the same length as highres_wavelength"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%irrFlux)) then
                    deallocate(staticS%irrFlux)
                    staticS%irrFlux => null()
                end if
                call C_F_POINTER(val, staticS%irrFlux, [staticS%irrNum])

            case ("isrf_wavelength_bd1")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_wavelength_bd1 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(1)%numCentralWavelength = dims(1)
                if (associated(staticS%isrfS(1)%centralWavelength)) then
                    deallocate(staticS%isrfS(1)%centralWavelength)
                    staticS%isrfS(1)%centralWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(1)%centralWavelength, [staticS%isrfS(1)%numCentralWavelength])

            case ("isrf_offset_bd1")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_offset_bd1 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(1)%numDeltaWavelength = dims(1)
                if (associated(staticS%isrfS(1)%deltaWavelength)) then
                    deallocate(staticS%isrfS(1)%deltaWavelength)
                    staticS%isrfS(1)%deltaWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(1)%deltaWavelength, [staticS%isrfS(1)%numDeltaWavelength])

            case ("isrf_bd1")
                if (ndims /= 3) then
                    write(str,'(A)') "isrf_bd1 must be a three dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if

                if (staticS%isrfS(1)%numDeltaWavelength /= dims(3) .OR. &
                    staticS%isrfS(1)%numCentralWavelength /= dims(2) .OR. &
                    staticS%isrfS(1)%groundPixel /= dims(1)) then
                    write(str,'(A)') "isrf_bd1 has inconsistent dimensions"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%isrfS(1)%isrf)) then
                    deallocate(staticS%isrfS(1)%isrf)
                    staticS%isrfS(1)%isrf => null()
                end if
                ! TODO: check order of dimensions !
                call C_F_POINTER(val, staticS%isrfS(1)%isrf, &
                    [staticS%isrfS(1)%numDeltaWavelength, staticS%isrfS(1)%numCentralWavelength, staticS%isrfS(1)%groundPixel])

            case ("isrf_wavelength_bd2")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_wavelength_bd2 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(2)%numCentralWavelength = dims(1)
                if (associated(staticS%isrfS(2)%centralWavelength)) then
                    deallocate(staticS%isrfS(2)%centralWavelength)
                    staticS%isrfS(2)%centralWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(2)%centralWavelength, [staticS%isrfS(2)%numCentralWavelength])

            case ("isrf_offset_bd2")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_offset_bd2 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(2)%numDeltaWavelength = dims(1)
                if (associated(staticS%isrfS(2)%deltaWavelength)) then
                    deallocate(staticS%isrfS(2)%deltaWavelength)
                    staticS%isrfS(2)%deltaWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(2)%deltaWavelength, [staticS%isrfS(2)%numDeltaWavelength])

            case ("isrf_bd2")
                if (ndims /= 3) then
                    write(str,'(A)') "isrf_bd2 must be a three dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if

                if (staticS%isrfS(2)%numDeltaWavelength /= dims(3) .OR. &
                    staticS%isrfS(2)%numCentralWavelength /= dims(2) .OR. &
                    staticS%isrfS(2)%groundPixel /= dims(1)) then
                    write(str,'(A)') "isrf_bd2 has inconsistent dimensions"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%isrfS(2)%isrf)) then
                    deallocate(staticS%isrfS(2)%isrf)
                    staticS%isrfS(2)%isrf => null()
                end if
                ! TODO: check order of dimensions !
                call C_F_POINTER(val, staticS%isrfS(2)%isrf, &
                    [staticS%isrfS(2)%numDeltaWavelength, staticS%isrfS(2)%numCentralWavelength, staticS%isrfS(2)%groundPixel])

            case ("isrf_wavelength_bd3")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_wavelength_bd3 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(3)%numCentralWavelength = dims(1)
                if (associated(staticS%isrfS(3)%centralWavelength)) then
                    deallocate(staticS%isrfS(3)%centralWavelength)
                    staticS%isrfS(3)%centralWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(3)%centralWavelength, [staticS%isrfS(3)%numCentralWavelength])

            case ("isrf_offset_bd3")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_offset_bd3 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(3)%numDeltaWavelength = dims(1)
                if (associated(staticS%isrfS(3)%deltaWavelength)) then
                    deallocate(staticS%isrfS(3)%deltaWavelength)
                    staticS%isrfS(3)%deltaWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(3)%deltaWavelength, [staticS%isrfS(3)%numDeltaWavelength])

            case ("isrf_bd3")
                if (ndims /= 3) then
                    write(str,'(A)') "isrf_bd3 must be a three dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if

                if (staticS%isrfS(3)%numDeltaWavelength /= dims(3) .OR. &
                    staticS%isrfS(3)%numCentralWavelength /= dims(2) .OR. &
                    staticS%isrfS(3)%groundPixel /= dims(1)) then
                    write(str,'(A)') "isrf_bd3 has inconsistent dimensions"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%isrfS(3)%isrf)) then
                    deallocate(staticS%isrfS(3)%isrf)
                    staticS%isrfS(3)%isrf => null()
                end if
                ! TODO: check order of dimensions !
                call C_F_POINTER(val, staticS%isrfS(3)%isrf, &
                    [staticS%isrfS(3)%numDeltaWavelength, staticS%isrfS(3)%numCentralWavelength, staticS%isrfS(3)%groundPixel])

            case ("isrf_wavelength_bd4")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_wavelength_bd4 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(4)%numCentralWavelength = dims(1)
                if (associated(staticS%isrfS(4)%centralWavelength)) then
                    deallocate(staticS%isrfS(4)%centralWavelength)
                    staticS%isrfS(4)%centralWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(4)%centralWavelength, [staticS%isrfS(4)%numCentralWavelength])

            case ("isrf_offset_bd4")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_offset_bd4 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(4)%numDeltaWavelength = dims(1)
                if (associated(staticS%isrfS(4)%deltaWavelength)) then
                    deallocate(staticS%isrfS(4)%deltaWavelength)
                    staticS%isrfS(4)%deltaWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(4)%deltaWavelength, [staticS%isrfS(4)%numDeltaWavelength])

            case ("isrf_bd4")
                if (ndims /= 3) then
                    write(str,'(A)') "isrf_bd4 must be a three dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if

                if (staticS%isrfS(4)%numDeltaWavelength /= dims(3) .OR. &
                    staticS%isrfS(4)%numCentralWavelength /= dims(2) .OR. &
                    staticS%isrfS(4)%groundPixel /= dims(1)) then
                    write(str,'(A)') "isrf_bd4 has inconsistent dimensions"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%isrfS(4)%isrf)) then
                    deallocate(staticS%isrfS(4)%isrf)
                    staticS%isrfS(4)%isrf => null()
                end if
                ! TODO: check order of dimensions !
                call C_F_POINTER(val, staticS%isrfS(4)%isrf, &
                    [staticS%isrfS(4)%numDeltaWavelength, staticS%isrfS(4)%numCentralWavelength, staticS%isrfS(4)%groundPixel])

            case ("isrf_wavelength_bd5")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_wavelength_bd5 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(5)%numCentralWavelength = dims(1)
                if (associated(staticS%isrfS(5)%centralWavelength)) then
                    deallocate(staticS%isrfS(5)%centralWavelength)
                    staticS%isrfS(5)%centralWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(5)%centralWavelength, [staticS%isrfS(5)%numCentralWavelength])

            case ("isrf_offset_bd5")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_offset_bd5 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(5)%numDeltaWavelength = dims(1)
                if (associated(staticS%isrfS(5)%deltaWavelength)) then
                    deallocate(staticS%isrfS(5)%deltaWavelength)
                    staticS%isrfS(5)%deltaWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(5)%deltaWavelength, [staticS%isrfS(5)%numDeltaWavelength])

            case ("isrf_bd5")
                if (ndims /= 3) then
                    write(str,'(A)') "isrf_bd5 must be a three dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if

                if (staticS%isrfS(5)%numDeltaWavelength /= dims(3) .OR. &
                    staticS%isrfS(5)%numCentralWavelength /= dims(2) .OR. &
                    staticS%isrfS(5)%groundPixel /= dims(1)) then
                    write(str,'(A)') "isrf_bd5 has inconsistent dimensions"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%isrfS(5)%isrf)) then
                    deallocate(staticS%isrfS(5)%isrf)
                    staticS%isrfS(5)%isrf => null()
                end if
                ! TODO: check order of dimensions !
                call C_F_POINTER(val, staticS%isrfS(5)%isrf, &
                    [staticS%isrfS(5)%numDeltaWavelength, staticS%isrfS(5)%numCentralWavelength, staticS%isrfS(5)%groundPixel])

            case ("isrf_wavelength_bd6")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_wavelength_bd6 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(6)%numCentralWavelength = dims(1)
                if (associated(staticS%isrfS(6)%centralWavelength)) then
                    deallocate(staticS%isrfS(6)%centralWavelength)
                    staticS%isrfS(6)%centralWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(6)%centralWavelength, [staticS%isrfS(6)%numCentralWavelength])

            case ("isrf_offset_bd6")
                if (ndims /= 1) then
                    write(str,'(A)') "isrf_offset_bd6 must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%isrfS(6)%numDeltaWavelength = dims(1)
                if (associated(staticS%isrfS(6)%deltaWavelength)) then
                    deallocate(staticS%isrfS(6)%deltaWavelength)
                    staticS%isrfS(6)%deltaWavelength => null()
                end if
                call C_F_POINTER(val, staticS%isrfS(6)%deltaWavelength, [staticS%isrfS(6)%numDeltaWavelength])

            case ("isrf_bd6")
                if (ndims /= 3) then
                    write(str,'(A)') "isrf_bd6 must be a three dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if

                if (staticS%isrfS(6)%numDeltaWavelength /= dims(3) .OR. &
                    staticS%isrfS(6)%numCentralWavelength /= dims(2) .OR. &
                    staticS%isrfS(6)%groundPixel /= dims(1)) then
                    write(str,'(A)') "isrf_bd6 has inconsistent dimensions"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%isrfS(6)%isrf)) then
                    deallocate(staticS%isrfS(6)%isrf)
                    staticS%isrfS(6)%isrf => null()
                end if
                ! TODO: check order of dimensions !
                call C_F_POINTER(val, staticS%isrfS(6)%isrf, &
                    [staticS%isrfS(6)%numDeltaWavelength, staticS%isrfS(6)%numCentralWavelength, staticS%isrfS(6)%groundPixel])

            case ("refspec_wavelength")
                if (ndims /= 1) then
                    write(str,'(A)') "refspec_wavelength must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                staticS%hr_wavel%nwavel = dims(1)
                staticS%o2xsection%nwavelHR = dims(1)
                staticS%o2o2xsection%nwavelHR = dims(1)
                if (associated(staticS%hr_wavel%wavel)) then
                    deallocate(staticS%hr_wavel%wavel)
                    staticS%hr_wavel%wavel => null()
                end if
                call C_F_POINTER(val, staticS%hr_wavel%wavel, [staticS%hr_wavel%nwavel])

            case ("refspec_gauss_weights")
                if (ndims /= 1) then
                    write(str,'(A)') "refspec_gauss_weights must be a single dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (staticS%hr_wavel%nwavel /= dims(1)) then
                    write(str,'(A)') "refspec_gauss_weights must have the same length as refspec_wavelength"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%hr_wavel%weight)) then
                    deallocate(staticS%hr_wavel%weight)
                    staticS%hr_wavel%weight => null()
                end if
                call C_F_POINTER(val, staticS%hr_wavel%weight, [staticS%hr_wavel%nwavel])

            case ("refspec_pressure_max")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "refspec_pressure_max must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%o2xsection%pmax_LUT = scalar

            case ("refspec_pressure_min")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "refspec_pressure_min must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%o2xsection%pmin_LUT = scalar

                if (staticS%o2xsection%pmin_LUT > staticS%o2xsection%pmax_LUT) then
                    write(str,'(A)') "refspec_pressure_min must be smaller than refspec_pressure_max"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_RANGE_ERROR
                    return
                end if

            case ("refspec_temperature_max")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "refspec_temperature_max must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%o2xsection%Tmax_LUT = scalar

            case ("refspec_temperature_min")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "refspec_temperature_min must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%o2xsection%Tmin_LUT = scalar

                if (staticS%o2xsection%Tmin_LUT > staticS%o2xsection%Tmax_LUT) then
                    write(str,'(A)') "refspec_temperature_min must be smaller than refspec_temperature_max"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_RANGE_ERROR
                    return
                end if

            case ("refspec_o2")
                if (ndims /= 3) then
                    write(str,'(A)') "refspec_o2 must be a three dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if

                if (staticS%o2xsection%nwavelHR /= dims(3) .OR. &
                    staticS%o2xsection%ncoeff_lnT_LUT /= dims(2) .OR. &
                    staticS%o2xsection%ncoeff_lnp_LUT /= dims(1)) then
                    write(str,'(A)') "refspec_o2 has inconsistent dimensions"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%o2xsection%coeff_lnTlnp_LUT)) then
                    deallocate(staticS%o2xsection%coeff_lnTlnp_LUT)
                    staticS%o2xsection%coeff_lnTlnp_LUT => null()
                end if
                ! TODO: check order of dimensions !
                call C_F_POINTER(val, staticS%o2xsection%coeff_lnTlnp_LUT, &
                    [staticS%o2xsection%nwavelHR, &
                     staticS%o2xsection%ncoeff_lnT_LUT, &
                     staticS%o2xsection%ncoeff_lnp_LUT])

            case ("refspec_o2o2")
                if (ndims /= 3) then
                    write(str,'(A)') "refspec_o2o2 must be a three dimensional array"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if

                if (staticS%o2o2xsection%nwavelHR /= dims(3) .OR. &
                    staticS%o2o2xsection%ncoeff_lnT_LUT /= dims(2) .OR. &
                    staticS%o2o2xsection%ncoeff_lnp_LUT /= dims(1)) then
                    write(str,'(A)') "refspec_o2o2 has inconsistent dimensions"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                if (associated(staticS%o2o2xsection%coeff_lnTlnp_LUT)) then
                    deallocate(staticS%o2o2xsection%coeff_lnTlnp_LUT)
                    staticS%o2o2xsection%coeff_lnTlnp_LUT => null()
                end if

                ! TODO: check order of dimensions !
                call C_F_POINTER(val, staticS%o2o2xsection%coeff_lnTlnp_LUT, &
                    [staticS%o2o2xsection%nwavelHR, &
                     staticS%o2o2xsection%ncoeff_lnT_LUT, &
                     staticS%o2o2xsection%ncoeff_lnp_LUT])

            case default
                write(str,'(A,A)') "disamar_set_static_input_d received an unknown key: ", key
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_set_static_input_d = PQF_E_KEY_ERROR
                return
        end select
        disamar_set_static_input_d = PQF_S_SUCCESS
    end function disamar_set_static_input_d

    function disamar_set_static_input_i(key, keylen, ndims, dims, val, staticS)
        implicit none
        integer(C_INT), value                :: keylen
        CHARACTER(len=keylen, KIND=C_CHAR)   :: key
        integer(C_INT), value                :: ndims
        integer(C_INT), intent(in)           :: dims(ndims)
        type(c_ptr), intent(in), value       :: val     ! on C-side: int*
        type(staticType)                     :: staticS ! on C-side: void*

        integer(kind=C_INT)                  :: disamar_set_static_input_d

        integer, pointer                     :: scalar => null()
        integer                              :: i, allocStatus
        character*(*)                        :: str


        write(str,'(A,A,A)') "disamar_set_static_input_i(", key, ")"
        call fortranlog(trim(str), len(trim(str)), 1)

        disamar_set_static_input_i = PQF_S_SUCCESS

        if (.not. c_associated(val)) then
            write(str,'(A)') "input value must not be NULL"
            call fortranlog(trim(str), len(trim(str)), 4)
            disamar_set_static_input_i = PQF_E_GENERIC_EXCEPTION
            return
        end if

        select case (trim(key))
            case ("nrows_bd1")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "nrows_bd1 must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%isrfS(1)%groundPixel = scalar

            case ("nrows_bd2")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "nrows_bd2 must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%isrfS(2)%groundPixel = scalar

            case ("nrows_bd3")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "nrows_bd3 must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%isrfS(3)%groundPixel = scalar

            case ("nrows_bd4")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "nrows_bd4 must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%isrfS(4)%groundPixel = scalar

            case ("nrows_bd5")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "nrows_bd5 must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%isrfS(5)%groundPixel = scalar

            case ("nrows_bd6")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "nrows_bd6 must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%isrfS(6)%groundPixel = scalar

            case ("refspec_npressure")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "refspec_npressure must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%o2xsection%ncoeff_lnp_LUT = scalar
                staticS%o2o2xsection%ncoeff_lnp_LUT = scalar

            case ("refspec_ntemperature")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    write(str,'(A)') "refspec_ntemperature must be a scalar"
                    call fortranlog(trim(str), len(trim(str)), 4)
                    disamar_set_static_input_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(val, scalar)
                staticS%o2o2xsection%ncoeff_lnT_LUT = scalar
                staticS%o2xsection%ncoeff_lnT_LUT = scalar

            case default
                write(str,'(A,A)') "disamar_set_static_input_i received an unknown key: ", key
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_set_static_input_i = PQF_E_KEY_ERROR
                return
        end select
        disamar_set_static_input_i = PQF_S_SUCCESS
    end function disamar_set_static_input_i

    ! -------------------------------------------------------------------------------

    function disamar_set_static_input_c(key, keylen, val, vallen, staticS)
        implicit none
        integer(C_INT), value                :: keylen
        CHARACTER(len=keylen, KIND=C_CHAR)   :: key
        integer(C_INT), value                :: vallen
        CHARACTER(len=keylen, KIND=C_CHAR)   :: val     ! on C-side: char*
        type(staticType)                     :: staticS ! on C-side: void*

        integer(kind=C_INT)                  :: disamar_set_static_input_c

        integer                              :: i, allocStatus
        character*(*)                        :: str


        write(str,'(A,A,A)') "disamar_set_static_input_c(", key, ")"
        call fortranlog(trim(str), len(trim(str)), 1)

        disamar_set_static_input_c = PQF_S_SUCCESS

        select case (trim(key))
            case ("config")
                write(str,'(A)') "Passing the configuration via disamar_set_static_input_c is not supported yet"
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_set_static_input_c = PQF_E_GENERIC_EXCEPTION
                return

            case default
                write(str,'(A,A)') "disamar_set_static_input_c received an unknown key: ", key
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_set_static_input_c = PQF_E_KEY_ERROR
                return

        end select
        disamar_set_static_input_c = PQF_S_SUCCESS
    end function disamar_set_static_input_i

    ! -------------------------------------------------------------------------------

    function disamar_get_static_output_c(key, keylen, buf_size, buffer, staticS)
        implicit none
        integer(C_INT), value            :: keylen
        CHARACTER(keylen, KIND=C_CHAR)   :: key
        integer(C_INT), intent(inout)    :: buf_size    ! when incorrectly sized buffer is received, return correct value here.
        CHARACTER(buf_size, KIND=C_CHAR) :: buffer      ! put result in this string (if buf_size is sufficient)
        type(staticType)                 :: staticS ! on C-side: void*

        integer(kind=C_INT)              :: disamar_get_static_output_c

        write(str,'(A,A)') "disamar_get_static_output_c: ", trim(key)
        call fortranlog(trim(str), len(trim(str)), 1)

        select case (trim(key))
            case ("version")
                if (buf_size .le. len(trim(DISAMAR_version_number))) then ! .le. to ensure that '\0' can be added.
                    buf_size = len(trim(DISAMAR_version_number)) + 1
                    disamar_get_static_output_c = PQF_E_BUFFER_SIZE_ERROR !! error ro be added to PQF
                    return
                end if
                buffer = trim(DISAMAR_version_number)//C_NULL_CHAR    ! trim string and append '\0' character.

            case ("state_vector_elements")
                !! TODO
                write(str,'(A)') "disamar_get_static_output_c needs to be connected with getStatevectorElementNamesAndIndices()"
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_get_static_output_c = PQF_E_GENERIC_EXCEPTION
                return

            case default
                write(str,'(A,A)') "disamar_get_static_output_c received an unknown key: ", key
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_get_static_output_c = STATUS_E_KEY_ERROR
                return
        end select
        disamar_get_static_output_c = STATUS_S_SUCCESS
    end function disamar_get_static_output_c

    ! -------------------------------------------------------------------------------
    function disamar_get_static_output_i(key, keylen, ndims, dims, buffer, staticS)
        implicit none
        integer(C_INT), value           :: keylen
        CHARACTER(keylen, KIND=C_CHAR)  :: key
        integer(C_INT), value           :: ndims
        integer(C_INT), intent(inout)   :: dims(ndims)
        type(C_PTR), value              :: buffer
        type(staticType)                :: staticS ! on C-side: void*

        integer(kind=C_INT)             :: disamar_get_static_output_i

        integer(C_INT), pointer         :: scalar, a1(:)


        call disamar_logger("disamar_get_static_output_i "//trim(key), LOG_DEBUG)

        select case (trim(key))
            case ("state_vector_length")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    dims(1) = 1
                    disamar_get_static_output_i = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(buffer, scalar)
                scalar = 0

                !! TODO needs globalS
                write(str,'(A)') "disamar_get_static_output_i needs to be connected with getStatevectorElementNamesAndIndices()"
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_get_static_output_i = PQF_E_GENERIC_EXCEPTION
                return

            case default
                write(str,'(A,A)') "disamar_get_static_output_i received an unknown key: ", key
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_get_static_output_i = STATUS_E_KEY_ERROR
                return
        end select
        disamar_get_static_output_i = STATUS_S_SUCCESS
    end function disamar_get_static_output_i

    ! -------------------------------------------------------------------------------

    function disamar_get_static_output_d(key, keylen, ndims, dims, buffer, staticS)
        implicit none
        integer(C_INT), value           :: keylen
        CHARACTER(keylen, KIND=C_CHAR)  :: key
        integer(C_INT), value           :: ndims
        integer(C_INT), intent(inout)   :: dims(ndims)
        type(C_PTR), value              :: buffer
        type(staticType)                :: staticS ! on C-side: void*

        integer(kind=C_INT)             :: disamar_get_static_output_d

        real(C_double), pointer         :: scalar, a1(:)


        call disamar_logger("disamar_get_static_output_d "//trim(key), LOG_DEBUG)

        select case (trim(key))
            case ("assumed_layer_pressure_thickness_hPa")
                if (ndims /= 1 .OR. dims(1) /= 1) then
                    dims(1) = 1
                    disamar_get_static_output_d = PQF_E_GENERIC_EXCEPTION
                    return
                end if
                call C_F_POINTER(buffer, scalar)
                scalar = 0

                !! TODO
                write(str,'(A)') "disamar_get_static_output_i needs to be connected with assumed_layer_pressure_thickness_hPa in getOutput (data in globalS)"
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_get_static_output_i = PQF_E_GENERIC_EXCEPTION
                return

            case default
                write(str,'(A,A)') "disamar_get_static_output_i received an unknown key: ", key
                call fortranlog(trim(str), len(trim(str)), 4)
                disamar_get_static_output_i = STATUS_E_KEY_ERROR
                return
        end select
        disamar_get_static_output_i = STATUS_S_SUCCESS
    end function disamar_get_static_output_i

#endif

    ! -------------------------------------------------------------------------------

    subroutine getStatevectorElementNamesAndIndices(globalS, sep, which_elements, names, indices, counter, length)

    ! Getting the names of the state vector elements is not trivial, as it depends on the configuration
    ! Best way to get the names (uniquely) of the state vector elements is to as DISAMAR.

        implicit none

        type(globalType), pointer, intent(in) :: globalS        ! the DISAMAR global datastructure
        character*(*), intent(in)             :: sep            ! the separator, set by caller
        integer, intent(in)                   :: which_elements ! if eq 0: all parameters, if gt 0: only trace gas, if lt 0: only other
        character*(*), intent(inout)          :: names          ! the element names
        integer, pointer, intent(inout)       :: indices(:)     ! selected indices from state vector (zero based!)
        integer, intent(out)                  :: counter        ! the number of element names
        integer, intent(out)                  :: length         ! the total length of the output string

        integer                               :: istate, iband, itrace, idx, ialt
        character*(255)                       :: str, element_name

        counter = 0

        do istate = 1, globalS%retrS%nstate
            ! get the band for the current parameter
            iband = globalS%retrS%codeSpecBand(istate)

            ! default name is internal DISAMAR name
            element_name = globalS%retrS%codeFitParameters(istate)

            ! a few parameters need to have a wavelength appended to the name to distinguish them
            ! these are the cases where that is needed.
            !
            select case (globalS%retrS%codeFitParameters(istate))
                case ('nodeTrace')
                    ! if we are looking for the 'other' parameters only, the we continue with next element in state vector.
                    if (which_elements.lt.0) cycle

                    itrace = globalS%retrS%codeTraceGas(istate)
                    ialt   = globalS%retrS%codeAltitude(istate)

                    ! replace 'nodeTrace' string with name of gas
                    element_name = globalS%traceGasRetrS(iTrace)%nameTraceGas

                    ! use index in altitude as suffix
                    write(str, '(i2.2)') ialt

                case ('columnTrace')
                    ! if we are looking for the 'trace gas profile' parameters only, the we continue with next element in state vector.
                    if (which_elements.gt.0) cycle

                    itrace = globalS%retrS%codeTraceGas(istate)
                    ! replace 'columnTrace' string with name of gas
                    element_name = globalS%traceGasRetrS(iTrace)%nameTraceGas

                    ! no suffix
                    str = ''

                case ('surfAlbedo')
                    ! if we are looking for the 'trace gas' parameters only, the we continue with next element in state vector.
                    if (which_elements.gt.0) cycle

                    if ( .not. globalS%cloudAerosolRTMgridRetrS%useAlbedoLambSurfAllBands ) then
                        idx = globalS%retrS%codeIndexSurfAlb(istate)
                        write(str, '(i3.3)') nint(globalS%surfaceRetrS(iband)%wavelAlbedo(idx))
                    end if

                case ('surfEmission')
                    ! if we are looking for the 'trace gas profile' parameters only, the we continue with next element in state vector.
                    if (which_elements.gt.0) cycle

                    idx = globalS%retrS%codeIndexSurfEmission(istate)
                    write(str, '(i3.3)') nint(globalS%surfaceRetrS(iband)%wavelEmission(idx))

                case ('LambCldAlbedo')
                    ! if we are looking for the 'trace gas profile' parameters only, the we continue with next element in state vector.
                    if (which_elements.gt.0) cycle

                    if ( .not. globalS%cloudAerosolRTMgridRetrS%useAlbedoLambCldAllBands ) then
                        idx = globalS%retrS%codeIndexLambCldAlb(istate)
                        write(str, '(i3.3)') nint(globalS%LambCloudRetrS(iband)%wavelAlbedo(idx))
                    end if

                case ('mulOffset')
                    ! if we are looking for the 'trace gas profile' parameters only, the we continue with next element in state vector.
                    if (which_elements.gt.0) cycle

                    idx = globalS%retrS%codeIndexMulOffset(istate)
                    write(str, '(i3.3)') nint(globalS%earthRadMulOffsetRetrS(iband)%wavel(idx))

                case ('straylight')
                    ! if we are looking for the 'trace gas profile' parameters only, the we continue with next element in state vector.
                    if (which_elements.gt.0) cycle

                    idx = globalS%retrS%codeIndexStraylight(istate)
                    write(str, '(i3.3)') nint(globalS%earthRadStrayLightRetrS(iband)%wavel(idx))

                case ('cloudFraction')
                    ! if we are looking for the 'trace gas profile' parameters only, the we continue with next element in state vector.
                    if (which_elements.gt.0) cycle

                    if ( .not. globalS%cloudAerosolRTMgridRetrS%useCldAerFractionAllBands ) then
                        idx = globalS%retrS%codeIndexCloudFraction(istate)
                        write(str, '(i3.3)') nint(globalS%cldAerFractionRetrS(iband)%wavelCldAerFraction(idx))
                    end if

                case default
                    ! if we are looking for the 'trace gas profile' parameters only, the we continue with next element in state vector.
                    if (which_elements.gt.0) cycle

                    ! no suffix by default
                    str = ""

            end select

            ! start with base name of parameter, concatenate with separator string.
            ! separator string is set by the caller.

            if (counter.eq.0) then
                names = trim(element_name) // "[" // trim(str) // "]"
            else
                names = trim(names) // sep // trim(element_name) // "[" // trim(str) // "]"
            endif

            counter = counter + 1
            !indices(counter) = istate-1 ! output indices for use with C.
        end do

        length = len(trim(names))

    end subroutine getStatevectorElementNamesAndIndices

    ! -------------------------------------------------------------------------------

    subroutine getAerosol_mid_altitude(aerosol_mid_altitude, lnaerosol_mid_pressure, lnp, z, n)

        ! interpolate the ln(mid pressure (hPa)) to an mid-altitude (in km), as these are the units of the input arrays.

        real(4), intent(inout)    :: aerosol_mid_altitude
        real(4), intent(in)       :: lnaerosol_mid_pressure
        real(8), intent(in)       :: lnp(0:n)
        real(8), intent(in)       :: z(0:n)
        integer                   :: n

        type(errorType)           :: errS
        integer                   :: status_spline, status_alloc, i
        real(8), allocatable      :: SDz_p(:)

        real(4), parameter        :: fill_value=9969209968386869046778552952102584320.0

        aerosol_mid_altitude = fill_value

        call errorInit(errS)
        call errorSetInteractive(errS, .false.)

        allocate(SDz_p(0:n), STAT = status_alloc)

        if ( status_alloc /= 0 .and. errorCheck(errS)) return

        call spline(errS, lnp, z, SDz_p, status_spline)
        if ( status_spline /= 0 .and. errorCheck(errS)) then
            deallocate( SDz_p, STAT = status_alloc )
            return
        end if

        aerosol_mid_altitude = real(splint(errS, lnp, z, SDz_p, dble(lnaerosol_mid_pressure), status_spline))

        if ( status_spline /= 0 .and. errorCheck(errS)) then
            deallocate( SDz_p, STAT = status_alloc )
            return
        end if

        deallocate( SDz_p, STAT = status_alloc )

    end subroutine getAerosol_mid_altitude

    ! -------------------------------------------------------------------------------

    subroutine getOutput(globalS, key, data, lev, n)

        ! get output values from 'somewhere' in the DISAMAR data structure.
        ! See getOutputInt1 below to get integer values.

        implicit none

        type(globalType), pointer :: globalS   ! DISAMAR data
        character*(*)             :: key       ! key (look in the case to see what is available)
        real(4)                   :: data(lev) ! output data
        integer                   :: lev       ! number of expected values (input)
        integer                   :: n         ! number of delivered values (output)

        integer                   :: i, j, k, m, n1, nrun, nplus, nminus
        integer                   :: iTrace
        logical                   :: positive

        ! unit convertion constants
        real(4), parameter        :: DU2mol_per_m2=0.0004461995  ! DU -> mol/m^2
        real(4), parameter        :: km2m=1000.0                 ! km -> m
        real(4), parameter        :: fluorescence2mol=1.66054e-8 ! 10^12 ph/s/cm2/nm/sr -> mol/s/m2/nm/sr
        real(4), parameter        :: molec_per_cm2_2_mol_per_m2=1.66054e-20 ! molecules/cm2 -> mol/m2
        real(4), parameter        :: fill_value=9969209968386869046778552952102584320.0

        iTrace = 1
        n = 1

        select case (trim(key))
            ! dimensions
            case ("level")
                n = globalS%traceGasRetrS(iTrace)%nalt
                do i = 1,n
                    data(i) = i-1
                end do


            case ("sub_column")
                j = 2 ! j = 1 contains troposphere and total column
                n = globalS%columnRetrS(j)%nsubColumn
                do i = 1,n
                    data(i) = i-1 ! only an index here.
                end do

            case ("subcolumns_bounds")
                j = 2 ! j = 1 contains troposphere and total column
                n = 2 * globalS%columnRetrS(j)%nsubColumn
                i = 1
                do k = 1, globalS%columnRetrS(j)%nsubColumn
                    data(i:i+1) = km2m * globalS%columnRetrS(j)%altBound(k-1:k) ! result may need to be transposed!
                    i = i + 2
                end do

            case ("dimension_surface_albedo")
                n = 0
                if (associated(globalS%surfaceRetrS)) then
                    n = sum(globalS%surfaceRetrS(:)%nAlbedo)
                    j = 1
                    do i = 1, size(globalS%surfaceRetrS)
                        n1 = globalS%surfaceRetrS(i)%nAlbedo
                        if (n1 .gt. 0) then
                            data(j:j+n1-1) = globalS%surfaceRetrS(i)%wavelAlbedo(:)
                            j = j + n1
                        end if
                    end do
                end if

             case ("dimension_straylight")
                n = 0
                if (associated(globalS%earthRadStrayLightRetrS)) then
                    n = sum(globalS%earthRadStrayLightRetrS(:)%nwavel)
                    j = 1
                    do i = 1, size(globalS%earthRadStrayLightRetrS)
                        n1 = globalS%earthRadStrayLightRetrS(i)%nwavel
                        if (n1 .gt. 0) then
                            data(j:j+n1-1) = globalS%earthRadStrayLightRetrS(i)%wavel(:)
                            j = j + n1
                        end if
                    end do
                end if

            case ("dimension_apriori_other")
                n = 0 ! use getStatevectorElementNamesAndIndices instead

            case ("dimension_fluorescence")
                n = 0
                if (associated(globalS%surfaceRetrS)) then
                    n = sum(globalS%surfaceRetrS(:)%nwavelEmission)
                    j = 1
                    do i = 1, size(globalS%surfaceRetrS)
                        n1 = globalS%surfaceRetrS(i)%nwavelEmission
                        if (n1 .gt. 0) then
                            data(j:j+n1-1) = globalS%surfaceRetrS(i)%wavelEmission(:)
                            j = j + n1
                        end if
                    end do
                end if

            case ("altitude")
                n = globalS%traceGasRetrS(iTrace)%nalt + 1
                do i = 1, lev
                    if (i <= n) then
                        data(i) = km2m * globalS%traceGasRetrS(iTrace)%alt(i-1)
                    else
                        data(i) = fill_value
                    end if
                end do
                n = lev

            case ("temperature")
                n = globalS%traceGasRetrS(iTrace)%nalt + 1
                do i = 1, lev
                    if (i <= n) then
                        data(i) = globalS%traceGasRetrS(iTrace)%temperature(i-1)
                    else
                        data(i) = fill_value
                    end if
                end do
                n = lev

            case ("pressure")
                n = globalS%traceGasRetrS(iTrace)%nalt + 1
                do i = 1, lev
                    if (i <= n) then
                        data(i) = globalS%traceGasRetrS(iTrace)%pressure(i-1)
                    else
                        data(i) = fill_value
                    end if
                end do
                n = lev

            case ("ozone_profile")
                n = globalS%traceGasRetrS(iTrace)%nalt + 1
                do i = 1, lev
                    if (i <= n) then
                        data(i) = globalS%traceGasRetrS(iTrace)%vmr(i-1)
                    else
                        data(i) = fill_value
                    end if
                end do
                n = lev

            case ("ozone_profile_precision")
                n = globalS%traceGasRetrS(iTrace)%nalt + 1
                do i = 1, lev
                    if (i <= n) then
                        data(i) = sqrt(globalS%traceGasRetrS(iTrace)%covVmr(i-1,i-1))
                    else
                        data(i) = fill_value
                    end if
                end do
                n = lev

            case ("ozone_profile_apriori")
                n = globalS%traceGasRetrS(iTrace)%nalt + 1
                do i = 1, lev
                    if (i <= n) then
                        data(i) = globalS%traceGasRetrS(iTrace)%vmrAP(i-1)
                    else
                        data(i) = fill_value
                    end if
                end do
                n = lev

            case ("surface_pressure")
                data(1) = globalS%inputS%surfacePressure

            case ("cloud_pressure_crb")
                data(1) = globalS%LambCloudRetrS(1)%pressure

            case ("surface_albedo")
                n = 0
                if (associated(globalS%surfaceRetrS)) then
                    n = sum(globalS%surfaceRetrS(:)%nAlbedo)
                    j = 1
                    do i = 1, size(globalS%surfaceRetrS)
                        n1 = globalS%surfaceRetrS(i)%nAlbedo
                        if (n1 .gt. 0) then
                            data(j:j+n1-1) = globalS%surfaceRetrS(i)%albedo(:)
                            j = j + n1
                        end if
                    end do
                end if

            case ("surface_albedo_precision")
                n = 0
                if (associated(globalS%surfaceRetrS)) then
                    n = sum(globalS%surfaceRetrS(:)%nAlbedo)
                    j = 1
                    do i = 1, size(globalS%surfaceRetrS)
                        n1 = globalS%surfaceRetrS(i)%nAlbedo
                        if (n1 .gt. 0) then
                            do k = 1, n1
                                data(j+k-1) = sqrt(globalS%surfaceRetrS(i)%varianceAlbedo(k))
                            end do
                            j = j + n1
                        end if
                    end do
                end if

            case ("surface_albedo_apriori")
                n = 0
                if (associated(globalS%surfaceRetrS)) then
                    n = sum(globalS%surfaceRetrS(:)%nAlbedo)
                    j = 1
                    do i = 1, size(globalS%surfaceRetrS)
                        n1 = globalS%surfaceRetrS(i)%nAlbedo
                        if (n1 .gt. 0) then
                            data(j:j+n1-1) = globalS%surfaceRetrS(i)%albedoAP(:)
                            j = j + n1
                        end if
                    end do
                end if

            case ("cloud_fraction_crb")
                if (globalS%cloudAerosolRTMgridRetrS%useCldAerFractionAllBands) then
                    n = 1
                    data(1) = globalS%cloudAerosolRTMgridRetrS%CldAerFractionAllBands
                else
                    n = sum(globalS%cldAerFractionRetrS(:)%nCldAerFraction)
                    j = 1
                    do i = 1, size(globalS%cldAerFractionRetrS(:))
                        n1 = globalS%cldAerFractionRetrS(i)%nCldAerFraction
                        if (n1 .gt. 0) then
                            data(j:j+n1-1) = globalS%cldAerFractionRetrS(i)%cldAerFraction(:)
                            j = j + n1
                        end if
                    end do
                end if

            case ("cloud_fraction_crb_precision")
                if (globalS%cloudAerosolRTMgridRetrS%useCldAerFractionAllBands) then
                    n = 1
                    data(1) = sqrt(globalS%cloudAerosolRTMgridRetrS%varCldAerFractionAllBands)
                else
                    n = sum(globalS%cldAerFractionRetrS(:)%nCldAerFraction)
                    j = 1
                    do i = 1, size(globalS%cldAerFractionRetrS(:))
                        n1 = globalS%cldAerFractionRetrS(i)%nCldAerFraction
                        if (n1 .gt. 0) then
                            do k = 1, n1
                                data(j+k-1) = sqrt(globalS%cldAerFractionRetrS(i)%varianceCldAerFraction(k))
                            end do
                            j = j + n1
                        end if
                    end do
                end if

            case ("cloud_fraction_apriori")
                if (globalS%cloudAerosolRTMgridRetrS%useCldAerFractionAllBands) then
                    n = 1
                    data(1) = globalS%cloudAerosolRTMgridRetrS%cldAerfractionAllBandsAP
                else
                    n = 0
                    if (associated(globalS%surfaceRetrS)) then
                        n = sum(globalS%cldAerFractionRetrS(:)%nCldAerFraction)
                        j = 1
                        do i = 1, size(globalS%surfaceRetrS)
                            n1 = globalS%surfaceRetrS(i)%nAlbedo
                            if (n1 .gt. 0) then
                                data(j:j+n1-1) = globalS%cldAerFractionRetrS(i)%cldAerFractionAP(:)
                                j = j + n1
                            end if
                        end do
                    end if
                end if

            case ("straylight_coefficients")
                n = 0
                if (associated(globalS%earthRadStrayLightRetrS)) then
                    n = sum(globalS%earthRadStrayLightRetrS(:)%nwavel)
                    j = 1
                    do i = 1, size(globalS%earthRadStrayLightRetrS)
                        n1 = globalS%earthRadStrayLightRetrS(i)%nwavel
                        if (n1 .gt. 0) then
                            data(j:j+n1-1) = globalS%earthRadStrayLightRetrS(i)%strayLightNodes(:)
                            j = j + n1
                        end if
                    end do
                end if

            case ("straylight_coefficients_precision")
                n = 0
                if (associated(globalS%earthRadStrayLightRetrS)) then
                    n = sum(globalS%earthRadStrayLightRetrS(:)%nwavel)
                    j = 1
                    do i = 1, size(globalS%earthRadStrayLightRetrS)
                        n1 = globalS%earthRadStrayLightRetrS(i)%nwavel
                        if (n1 .gt. 0) then
                            do k = 1, n1
                                data(j+k-1) = sqrt(globalS%earthRadStrayLightRetrS(i)%varStrayLight(k))
                            end do
                            j = j + n1
                        end if
                    end do
                end if

            case ("ozone_profile_subcolumns")
                j = 2
                n = size(globalS%columnRetrS(j)%subColumn)
                do i = 1, n
                    data(i) = molec_per_cm2_2_mol_per_m2 * globalS%columnRetrS(j)%subColumn(i)%column(1)
                end do

            case ("ozone_profile_subcolumns_precision")
                j = 2
                n = size(globalS%columnRetrS(j)%subColumn)
                do i = 1, n
                    ! errorCol values are percentage of column values
                    data(i) = molec_per_cm2_2_mol_per_m2 * globalS%columnRetrS(j)%subColumn(i)%column(1) * globalS%columnRetrS(j)%subColumn(i)%errorCol(1) / 100
                end do

            case ("ozone_concentration_at_surface")
                data(1) = globalS%traceGasRetrS(iTrace)%vmrAtSurface

            case ("ozone_concentration_at_surface_precision")
                data(1) = globalS%traceGasRetrS(iTrace)%vmrAtSurface_precision

            case ("ozone_concentration_at_cloud_height")
                data(1) = globalS%traceGasRetrS(iTrace)%vmrAtCloud

            case ("ozone_concentration_at_cloud_height_precision")
                data(1) = globalS%traceGasRetrS(iTrace)%vmrAtCloud_precision

            case ("ozone_concentration_at_tropopause")
                data(1) = globalS%traceGasRetrS(iTrace)%vmrAtTropopause

            case ("ozone_concentration_at_tropopause_precision")
                data(1) = globalS%traceGasRetrS(iTrace)%vmrAtTropopause_precision

            case ("ozone_tropospheric_column")
                ! column values in molecules / cm2 (see propAtmosphere.f90, subroutine calculateTotalColumn)
                j = 1 ! j = 1 contains troposphere and total column, j = 2 contains the sub_column values
                data(1) = molec_per_cm2_2_mol_per_m2 * globalS%columnRetrS(1)%subColumn(j)%column(1)

            case ("ozone_tropospheric_column_precision")
                ! errorCol values are percentage of column values
                j = 1 ! j = 1 contains troposphere and total column, j = 2 contains the sub_column values
                data(1) = molec_per_cm2_2_mol_per_m2 * globalS%columnRetrS(1)%subColumn(j)%column(1) * globalS%columnRetrS(1)%subColumn(j)%errorCol(1) / 100

            case ("ozone_total_column")
                data(1) = molec_per_cm2_2_mol_per_m2 * globalS%traceGasRetrS(iTrace)%column

            case ("ozone_total_column_precision")
                if (GlobalS%traceGasRetrS(1)%fitColumnTrace) then
                    data(1) = molec_per_cm2_2_mol_per_m2 * sqrt(globalS%traceGasRetrS(1)%covColumn)
                else
                    data(1) = fill_value
                endif

            case ("root_mean_square_error_of_fit")
                data(1) = globalS%retrS%rmse

            !!?? TODO: seems awfully similar to chi^2: (R-Rprev)T Se-1 (R-Rprev)  + (x-xa)T Sa-1 (x-xa) (T = transposed)
            case ("cost_function")
                data(1) = globalS%retrS%chi2

            case ("fluorescence_emission")
                n = 0
                if (associated(globalS%surfaceRetrS)) then
                    n = sum(globalS%surfaceRetrS(:)%nwavelEmission)
                    j = 1
                    do i = 1, size(globalS%surfaceRetrS(:))
                        n1 = globalS%surfaceRetrS(i)%nwavelEmission
                        if (n1 .gt. 0) then
                            do k = 1, n1
                                data(j+k-1) = fluorescence2mol * globalS%surfaceRetrS(i)%emission(k)
                            end do
                            j = j + n1
                        end if
                    end do
                end if

            case ("fluorescence_emission_precision")
                n = 0
                if (associated(globalS%surfaceRetrS)) then
                    n = sum(globalS%surfaceRetrS(:)%nwavelEmission)
                    j = 1
                    do i = 1, size(globalS%surfaceRetrS(:))
                        n1 = globalS%surfaceRetrS(i)%nwavelEmission
                        if (n1 .gt. 0) then
                            do k = 1, n1
                                data(j+k-1) = fluorescence2mol *  sqrt(globalS%surfaceRetrS(i)%varianceEmission(k))
                            end do
                            j = j + n1
                        end if
                    end do
                end if

            case ("sulfurdioxide_column")
                if (size(globalS%traceGasRetrS) .ge. 2) then
                    data(1) = DU2mol_per_m2 * globalS%traceGasRetrS(2)%column
                else
                    n = 0
                end if

            case ("sulfurdioxide_column_precision")
                if (size(globalS%traceGasRetrS) .ge. 2) then
                    data(1) = DU2mol_per_m2 * sqrt(globalS%traceGasRetrS(2)%covColumn)
                else
                    n = 0
                end if

            case ("pressure_at_tropopause")
                data(1) = globalS%gasPTRetrS%tropopausePressure

            case ("surface_temperature")
                data(1) = globalS%surfaceRetrS(1)%temperature

            case ("temperature_at_cloud_height")
                data(1) = globalS%LambCloudRetrS(1)%temperature

            case ("temperature_at_tropopause")
                data(1) = globalS%gasPTRetrS%tropopauseTemperature

            case ("wavelength_index")           ! dimension, optional, just an index, maximum size (maybe better through framework, split by band?)
                n = sum(globalS%wavelInstrRadRetrS(:)%nwavel)
                do i = 1, n
                    data(i) = i
                end do

            case ("chi_square_probability")     ! optional, framework (if ever)
                n = 0

            !!?? TODO: special care when calling, as only the first parameter needs to be stored.
            case ("number_of_runs")             ! optional
                nrun = 1
                nplus = 0
                nminus = 0
                n1 = size(globalS%retrS%dR)
                positive = (globalS%retrS%dR(1) .gt. 0)
                do i = 1, n1
                    if ((globalS%retrS%dR(i).gt.0) .neqv. positive) then
                        positive = globalS%retrS%dR(i).gt.0
                        nrun = nrun + 1
                    end if
                    if (globalS%retrS%dR(i).gt.0) then
                        nplus = nplus + 1
                    else
                        nminus = nminus + 1
                    endif
                end do
                n = 3
                data(1) = nrun
                data(2) = nplus
                data(3) = nminus

            case ("number_of_runs_probability") ! optional, calculate in framework from three values returned by "number_of_runs"
                n = 0

            case ("state_vector")               ! optional
                !! This call is meant to collect the evolution of the state vector in the iteration steps.
                !! This means that disamar will have to store the statevector for each iteration.
                ! iterations,state_vector_length (2D)
                n = size(globalS%retrS%x_stored)
                j = 1
                ! (nstate, maxNumIterations)
                do i = 1, globalS%retrS%maxNumIterations
                    data(j:j+globalS%retrS%nstate) = globalS%retrS%x_stored(:, i)
                    j = j + globalS%retrS%nstate
                end do

            case ("convergence_criterium")      ! optional
                !! This call is meant to collect the evolution of the convergence criterium in the iteration steps.
                !! This means that disamar will have to store the convergence criterium for each iteration.
                ! iterations (1D)
                n = globalS%retrS%maxNumIterations
                data(1:n) = globalS%retrS%xConv_stored

            case ("chi_square_iterations")      ! optional
                !! This call is meant to collect the evolution of chi^2 in the iteration steps.
                !! This means that disamar will have to store chi^2 for each iteration.
                ! iterations (1D)
                n = globalS%retrS%maxNumIterations
                data(1:n) = globalS%retrS%chisq_stored

            case ("wavelength")                 ! optional
                n = globalS%retrS%nwavelRetr + 1
                data(1:n) = globalS%retrS%wavelRetr(:)

            case ("residual")                   ! optional
                n = globalS%retrS%nwavelRetr + 1
                data(1:n) = globalS%retrS%dR(:)

            case ("reflectance")                ! optional
                n = globalS%retrS%nwavelRetr + 1
                data(1:n) = globalS%retrS%reflMeas(:)

            case ("reflectance_precision")      ! optional
                n = globalS%retrS%nwavelRetr + 1
                data(1:n) = globalS%retrS%reflNoiseError(:)

            case ("model")                      ! optional
                n = globalS%retrS%nwavelRetr + 1
                data(1:n) = globalS%retrS%refl(:)

            case ("state_vector_length")
                n = 0 ! use getStatevectorElementNamesAndIndices instead (variable contains strings).

            case ("iterations") ! optional output (dimension)
                n = globalS%retrS%maxNumIterations
                do i = 1, n
                    data(i) = i
                end do

            ! optional (debug) output.
            ! not included: wavelength_index, as this should be overdimensioned.

            case ("ozone_profile_apriori_precision")
                n = globalS%traceGasRetrS(iTrace)%nalt + 1
                do i = 1, lev
                    if (i <= n) then
                        data(i) = sqrt(globalS%traceGasRetrS(iTrace)%covVmrAP(i-1, i-1))
                    else
                        data(i) = fill_value
                    end if
                end do
                n = lev

            case ("correlation_length")
                ! correlation length of the a priori O3 error covariance matrix (eventually in m).
                ! to be attached as attribute to O3_apriori_error_covariance_matrix
                data(1) = km2m * globalS%traceGasRetrS(iTrace)%APcorrLength

            case ("apriori_error_covariance_matrix_other")
                if (globalS%traceGasRetrS(iTrace)%fitProfile) then
                    k = globalS%traceGasRetrS(iTrace)%nalt + 1
                else
                    k = 0
                end if
                n = size(globalS%retrS%Sa_vmr, 1) - k
                do i = 1, n
                    data(i) = sqrt(globalS%retrS%Sa_vmr(k+i, k+i))
                end do

            case ("ozone_profile_error_covariance_matrix")
                n = globalS%traceGasRetrS(iTrace)%nalt + 1
                m = 0
                do i = 1, lev
                    do j = 1, lev
                        m = m + 1
                        if ((i <= n) .and. (j <= n)) then
                            data(m) = globalS%traceGasRetrS(iTrace)%covVmr(j-1, i-1)
                        else
                            data(m) = fill_value
                        end if
                    end do
                end do
                n = m

            case ("error_covariance_matrix_other")
                n = 0
                n1 = size(globalS%diagnosticS%error_correlation_other, 1)
                do i = 1, n1
                    do j = 1, n1
                        n = n + 1
                        data(n) = globalS%diagnosticS%error_correlation_other(j, i)
                    end do
                end do

            case ("covariance_matrix")    ! full matrix for ALH
                n = 0
                n1 = size(globalS%diagnosticS%error_correlation_other, 1)
                do i = 1, n1
                    do j = 1, n1
                        n = n + 1
                        data(n) = globalS%diagnosticS%error_correlation_other(i, j)
                    end do
                end do

            case ("averaging_kernel")
                n = globalS%traceGasRetrS(iTrace)%nalt + 1
                m = 0
                do i = 1, lev
                    do j = 1, lev
                        m = m + 1
                        if ((i <= n) .and. (j <= n)) then
                            data(m) = globalS%diagnosticS%A_vmr(j, i)
                        else
                            data(m) = fill_value
                        end if
                    end do
                end do
                n = m

            case ("chi_square")
                data(1) = globalS%retrS%chi2

            case ("chi_square_reflectance")
                data(1) = globalS%retrS%chi2R

            case ("chi_square_state_vector")
                data(1) = globalS%retrS%chi2x

            case ("degrees_of_freedom")
                data(1) = globalS%diagnosticS%DFS

            case ("root_mean_square_error")
                data(1) = globalS%retrS%rmse

            case ("dfs_total")
                data(1) = globalS%diagnosticS%DFS

            case ("degrees_of_freedom_ozone")
                data(1) = globalS%diagnosticS%DFStrace(iTrace)

            case ("assumed_layer_pressure_thickness_hPa")
                data(1) = abs(globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) - &
                           globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit - 1))

            case ("aerosol_mid_pressure")
                data(1) = (globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) + &
                           globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit - 1)) / 2

            case ("aerosol_mid_height")
                ! calculate altitude from mid-pressure
                call getAerosol_mid_altitude(data(1), real(log((globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit) + &
                           globalS%cloudAerosolRTMgridRetrS%intervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit - 1)) / 2)), &
                           globalS%gasPTRetrS%lnpressure, &
                           globalS%gasPTRetrS%altAP, &
                           globalS%gasPTRetrS%npressure)
                data(1) = km2m * data(1)

            case ("aerosol_optical_thickness")
                data(1) = globalS%cloudAerosolRTMgridRetrS%intervalAerTau(globalS%cloudAerosolRTMgridRetrS%numIntervalFit)

            case ("aerosol_mid_pressure_precision")
                data(1) = sqrt(globalS%cloudAerosolRTMgridRetrS%varIntervalBounds_P(globalS%cloudAerosolRTMgridRetrS%numIntervalFit))

            case ("aerosol_mid_height_precision")
                data(1) = km2m * sqrt(globalS%cloudAerosolRTMgridRetrS%varIntervalBounds(globalS%cloudAerosolRTMgridRetrS%numIntervalFit))

            case ("aerosol_optical_thickness_precision")
                data(1) = sqrt(globalS%cloudAerosolRTMgridRetrS%varAerTau)

            case ("number_of_iterations")
                data(1) = globalS%retrS%numIterations

            case ("number_of_spectral_points_in_retrieval")
                data(1) = globalS%retrS%nwavelRetr

            case default
                write(globalS%errorS%temp,*) 'invalid key in getOutput: ', key
                call errorSet(globalS%errorS, globalS%errorS%temp)

        end select

    end subroutine getOutput

    ! -------------------------------------------------------------------------------

    subroutine checkPixel(globalS)

    implicit none

    type(globalType), pointer :: globalS

    integer              :: i, j, n

    ! For O3 tropospheric profile the sub-columns are not written. These should not be tested.
    j = 2
    n = size(globalS%columnRetrS(j)%subColumn)
    do i = 1, n
        if (isNan(globalS%columnRetrS(j)%subColumn(i)%column(1))) then
            call logDebugA('Pixel rejected: Nan value in O3_subcolumns')
            call setError(globalS, PQF_E_ASSERTION_ERROR)
            return
        end if
        if (isNan(globalS%columnRetrS(j)%subColumn(i)%errorCol(1))) then
            call logDebugA('Pixel rejected: Nan value in O3_subcolumns_precision')
            call setError(globalS, PQF_E_ASSERTION_ERROR)
            return
        end if
    end do

    j = 1 ! j = 1 contains troposphere and total column, j = 2 contains the sub_column values
    if (isNan(globalS%columnRetrS(1)%subColumn(j)%column(1))) then
        call logDebugA('Pixel rejected: Nan value in O3_total_column')
        call setError(globalS, PQF_E_ASSERTION_ERROR)
        return
    end if
    if (isNan(globalS%columnRetrS(1)%subColumn(j)%errorCol(1))) then
        call logDebugA('Pixel rejected: Nan value in O3_total_column_precision')
        call setError(globalS, PQF_E_ASSERTION_ERROR)
        return
    end if

    if (isNan(globalS%traceGasRetrS(1)%column)) then
        call logDebugA('Pixel rejected: Nan value in O3_tropospheric_column')
        call setError(globalS, PQF_E_ASSERTION_ERROR)
        return
    end if
    if (GlobalS%traceGasRetrS(1)%fitColumnTrace .and. isNan(globalS%traceGasRetrS(1)%covColumn)) then
        globalS%traceGasRetrS(1)%covColumn = 0.0
        ! call logDebugA('Pixel rejected: Nan value in O3_tropospheric_column_precision')
        ! call setError(globalS, PQF_E_ASSERTION_ERROR)
        ! return
    end if

    end subroutine checkPixel

    ! -------------------------------------------------------------------------------

    subroutine getOutputInt1(globalS, key, data)

        ! Obtain a few integer values.

        implicit none

        type(globalType), pointer :: globalS
        character*(*)             :: key
        integer                   :: data

        integer                   :: iTrace

        iTrace = 1

        select case (key)

            case ("n_iterations")
                data = globalS%iteration

            case ("nLayers")
                data = globalS%traceGasRetrS(iTrace)%nalt

            case ("ProcessingStatus")
                data = globalS%errorS%code

            case ("processing_quality_flags")
                data = globalS%processingQF

            case ("ConvergenceStatus")
                data = 1
                if (globalS%retrS%isConverged) data = 0
                if (globalS%iteration == globalS%retrS%maxNumIterations) data = 2
                if (globalS%retrS%isConvergedBoundary) data = 4
                if (globalS%retrS%stateVectorElementAdjusted > 0) then
                    if (globalS%retrS%codeFitParameters(globalS%retrS%stateVectorElementAdjusted) == 'aerosolTau') then
                        data = 3
                    end if
                end if

            case default
                write(globalS%errorS%temp,*) 'invalid key in getOutputInt1: ', key
                call errorSet(globalS%errorS, globalS%errorS%temp)

        end select

    end subroutine getOutputInt1

    !--------------------------------------------------------------------------------
    !> Returns meta-data string for key / subkey combination
    !!
    !! Meta-data is written to output file by the framework. DISAMAR processor specific
    !! meta-data is enquired and returned here.
    !! Nothing is returned for unknown key / subkey combination
    subroutine getOutputString(key, subkey, data, keylen, subkeylen, datalen)
        implicit none
        character, intent(in)         :: key(keylen)
        character, intent(in)         :: subkey(subkeylen)
        character, intent(inout)      :: data(datalen)
        integer, intent(in)           :: keylen
        integer, intent(in)           :: subkeylen
        integer, intent(in)           :: datalen

        character(keylen) :: lkey
        character(subkeylen) :: lsubkey
        character(datalen) :: ldata
        integer :: i

        do i = 1, keylen
            lkey(i:i) = key(i)
        end do
        do i = 1, subkeylen
            lsubkey(i:i) = subkey(i)
        end do

        select case (lkey)

            case ("version")
                write(ldata,'(a)') ALGORITHM_version_number

            case ("disamar")
                select case (lsubkey)
                    case ("Name")
                        write(ldata,'(a)') 'Disamar'
                    case ("Version")
                        write(ldata,'(a)') DISAMAR_version_number
                    case ("VersionDate")
                        write(ldata,'(a)') DISAMAR_version_date
                    case ("Authors")
                        write(ldata,'(a)') 'J.F. de Haan'
                    case ("Email")
                        write(ldata,'(a)') 'haandej@knmi.nl'
                    case ("Institution")
                        write(ldata,'(a)') 'KNMI (Royal Netherlands Meteorological Institute)'
                    case ("Reference")
                        write(ldata,'(a)') ''
                end select
        end select

        do i = 1, datalen
            data(i) = ldata(i:i)
        end do

    end subroutine getOutputString

    ! -------------------------------------------------------------------------------

    subroutine cleanup(globalS)

        ! Cleanup, call after each retrieval.

        implicit none

        type(globalType), pointer :: globalS
        type(errorType)           :: errorS

        call cleanupretrieval(errorS, globalS)
        call freeGlobalS(errorS, globalS)

    end subroutine cleanup

    ! -------------------------------------------------------------------------------

    subroutine checkConfigFile(globalS)

        implicit none

        type(globalType), pointer :: globalS

	    ! Reads an verifies contents of config file. Called to test for once whether config file
	    ! is correct.
        call errorInit(globalS%errorS)
        call errorSetInteractive(globalS%errorS, .true.)
		call init(globalS%errorS, globalS)
        if (errorCheck(globalS%errorS)) then
            return
        end if

		! Verifies contents
!		call verifyConfigFile(globalS%errorS, staticS%operational,         &
!			 globalS%numSpectrBands, globalS%nTrace, globalS%ncolumn,   &
!			 globalS%wavelInstrRadSimS, globalS%wavelInstrRadRetrS,     &
!			 globalS%cloudAerosolRTMgridSimS,                           &
!			 globalS%cloudAerosolRTMgridRetrS,                          &
!			 globalS%gasPTSimS, globalS%gasPTRetrS,                     &
!			 globalS%geometrySimS, globalS%geometryRetrS,               &
!			 globalS%traceGasSimS, globalS%traceGasRetrS,               &
!			 globalS%mieAerSimS, globalS%mieAerRetrS,                   &
!			 globalS%mieCldSimS, globalS%mieCldRetrS,                   &
!			 globalS%surfaceSimS, globalS%surfaceRetrS,                 &
!			 globalS%LambCloudRetrS, globalS%LambAerRetrS,              &
!			 globalS%controlSimS, globalS%controlRetrS,                 &
!			 globalS%RRS_RingSimS, globalS%RRS_RingRetrS,               &
!			 globalS%calibErrorReflS, globalS%polCorrectionRetrS,       &
!			 globalS%columnSimS, globalS%columnRetrS,                   &
!			 globalS%weakAbsRetrS, globalS%retrS)
!		if (errorCheck(globalS%errorS)) return

        call verifyConfigFile(globalS%errorS, staticS%operational,     &
            globalS%maxFourierTermLUT,                                 &
            globalS%numSpectrBands, globalS%nTrace, globalS%ncolumn,   &
            globalS%wavelInstrRadSimS, globalS%wavelInstrRadRetrS,     &
            globalS%cloudAerosolRTMgridSimS,                           &
            globalS%cloudAerosolRTMgridRetrS,                          &
            globalS%createLUTSimS, globalS%createLUTRetrS,             &
            globalS%XsecHRLUTSimS, globalS%XsecHRLUTRetrS,             &
            globalS%gasPTSimS, globalS%gasPTRetrS,                     &
            globalS%geometrySimS, globalS%geometryRetrS,               &
            globalS%traceGasSimS, globalS%traceGasRetrS,               &
            globalS%mieAerSimS, globalS%mieAerRetrS,                   &
            globalS%mieCldSimS, globalS%mieCldRetrS,                   &
            globalS%surfaceSimS, globalS%surfaceRetrS,                 &
            globalS%LambCloudRetrS, globalS%LambAerRetrS,              &
            globalS%controlSimS, globalS%controlRetrS,                 &
            globalS%RRS_RingSimS, globalS%RRS_RingRetrS,               &
            globalS%calibErrorReflS, globalS%polCorrectionRetrS,       &
            globalS%columnSimS, globalS%columnRetrS,                   &
            globalS%weakAbsRetrS, globalS%retrS,                       &
            globalS%earthRadianceSimS)
		if (errorCheck(globalS%errorS)) return


    end subroutine checkConfigFile

    ! -------------------------------------------------------------------------------

end module S5PInterfaceModule
