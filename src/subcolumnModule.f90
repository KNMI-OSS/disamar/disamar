!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

! **********************************************************************
! This module is part of DISAMAR
! The module subcolumnModule contains subroutines to
! calculate properties of subcolumns of trace gases
!
! Author: Johan de Haan, June 2009
!
! **********************************************************************

module subcolumnModule

  use dataStructures
  use mathTools,            only: splintLin, spline, splint, gaussDivPoints, indexx, svdcmp, svbksb
  use propAtmosphereModule, only: calculateTotalColumn
    
  ! default is private
  private 
  public  :: fillAltitudeGridCol, calculateColumnPropertiesSim, calculateColumnPropertiesRetr

  contains

    subroutine fillAltitudeGridCol(errS, ncolumn, numSpectrBands, nTrace, cloudAerosolRTMgridS, columnS, &
                                   gasPTRetrS, optPropRTMGridS, reflDerivHRS, earthRadianceS, retrS)

      ! Subroutine fillAltitudeGridCol fills altitude grids and gaussian weights for subcolumns.
      ! Specifically fill nGaussCol, Colaltitude(errS, nGaussCol), and Colweight(nGaussCol)
      ! which are part of the structure optPropRTMGridS.
      ! In addition nGaussCol is filled in the structure columnS
      !
      ! The number of column cases that are divided into subcolumns is ncolumn
      ! The number of subcolumns for column case icolumn is given by columnS(icolumn)%nsubColumn
      ! The altitudes for the boundaries of the subcolumns are given by columnS(icolumn)%altBound(0:nsubColumn)
      ! The number of Gaussian division points for the PBL (1), free troposphere (2), and stratosphere (3)
      !   are given by (columnS(icolumn)%nGaussInit(i) , i = 1,3) respectively. These values are the same
      !   for each value of icolumn.

      ! Method:
      ! The subcolumns are divided into sub-subcolumn and to each sub-subcolumn a set of Gaussian division
      ! points for the altitude is assigned. Integration over altitude for s subcolumn is then the sum
      ! over all Gaussian points whose altitude lies within the subcolumn. Hence one Gaussian grid suffices
      ! for all subcolumn cases.
      ! The boundaries (altitudes) of the sub-subcolumns are found by a) setting all altitudes for
      ! the subcolumns in one array, b) adding the boundaries for the PBL, free troposphere, and stratosphere,
      ! and c) eliminating altitudes that are already present (within 1.0e-4 km).
      ! The number of Gaussian division points per sub-subcolumn is calculated from the available
      ! numbers for PBL, free troposphere, and stratosphere, and is proportional to the altitude
      ! difference for the top and the bottom of the sub-subcolumn, with a minimum of naltsubsubMin per sub-subcolumn. 

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: ncolumn, numSpectrBands, nTrace
      type(cloudAerosolRTMgridType), intent(in)    :: cloudAerosolRTMgridS
      type(columnType),              intent(inout) :: columnS(ncolumn)
      type(gasPTType),               intent(inout) :: gasPTRetrS
      type(optPropRTMGridType),      intent(inout) :: optPropRTMGridS
      type(reflDerivType),           intent(inout) :: reflDerivHRS(numSpectrBands)   
      type(earthRadianceType),       intent(inout) :: earthRadianceS(numSpectrBands)
      type(retrType),                intent(inout) :: retrS

      ! local
      integer              :: index, igauss, nGauss, ibound, indexFirst, istate
      integer              :: icolumn, isubcolumn, nsubsubcol, iband
      integer              :: nGaussCol, nstateCol
      real(8)              :: z_surf, z_boundary_layer, z_tropopause, z_TOA  ! altitudes in km
      real(8)              :: z_cloud    ! altitude of a Lambertian cloud
      real(8)              :: dz_pbl, dz_freeTrop, dz_strat

      integer, parameter   :: naltsubsubMax = 250   ! at most 250 altitudes for the sub-subcolumns
      integer, parameter   :: naltsubsubMin = 2     ! at least 2 altitudes for the sub-subcolumns
      integer, parameter   :: naltGaussMax  = 500   ! at most 500 altitudes for the Gaussian division points
      integer              :: maxGausspoints        ! maximal number of Gaussian division points on sub-subintervals
      integer              :: sortIndex(naltsubsubMax)
      real(8)              :: altsubsubcol(naltsubsubMax)
      real(8)              :: altsubsubcolSorted(naltsubsubMax)
      real(8)              :: altGauss(naltGaussMax)
      real(8)              :: weightGauss(naltGaussMax)
      real(8)              :: z_prev, z, dz

      real(8), allocatable :: x0(:), w0(:)     ! gaussian points on (a,b)

      integer              :: allocStatus, sumallocStatus
      integer              :: deallocStatus, sumdeallocStatus

      logical, parameter   :: verbose = .false.

      ! the boundaries of the subcolumns are stored in columnS(icolumn)%altBound(0:columnS(icolumn)%nsubcolumn)
      ! the number of Gausspoints for the boundary layer   are stored in columnS(1)%nGaussInit(1)
      ! the number of Gausspoints for the free troposphere are stored in columnS(1)%nGaussInit(2)
      ! the number of Gausspoints for the stratosphere     are stored in columnS(1)%nGaussInit(3)
      z_TOA = cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%ninterval)
     
      if ( z_TOA < 50.0d0 ) then
         call logDebugD('WARNING: TOA smaller than 50 km', z_TOA)
      end if
      if ( z_TOA < 20.0d0 ) then
         call logDebugD('ERROR: TOA smaller than 20 km', z_TOA)
         call logDebug('in subroutine fillAltitudeGridCol')
         call mystop(errS, ' stopped because the top of the atmosphere is too small ')
         if (errorCheck(errS)) return
      end if

      z_surf           = cloudAerosolRTMgridS%intervalBounds(0)
      z_boundary_layer = z_surf + 2.0d0
      z_tropopause     = 15.0d0
      z_cloud          = cloudAerosolRTMgridS%intervalBounds(cloudAerosolRTMgridS%numIntervalFit)

      ! fill values for tropopause and TOA
      ! it is assumed that the second line in the configutation file in the
      ! section specifying the subcolumns is : boundaries   tropopause   toa
      columnS (1)%altBound(1) = gasPTretrS%tropopauseAltitude
      do icolumn = 1, ncolumn
        columnS(icolumn)%altBound(columnS(icolumn)%nsubColumn) = z_TOA
      end do


      ! initialize altsubsubcol starting with -1000.0d0
      ! and ending with -1000.0d0 + naltsubsubMax
      ! because the values should be different for sorting
      do index = 1, naltsubsubMax
        altsubsubcol(index) = -1000.d0 + index
      end do

      index = 1
      do icolumn = 1, ncolumn
        do isubcolumn = 0, columnS(icolumn)%nsubcolumn
          altsubsubcol(index) = columnS(icolumn)%altBound(isubcolumn)
          index = index + 1
          if ( index > naltsubsubMax ) then
            call logDebug('ERROR: too many sub-subcolumns - increase naltsubsubMax')
            call logDebug('in subroutine fillAltitudeGridCol in module subcolumnModule')
            call mystop(errS, 'stopped because the number of sub-subcolumns is too large')
            if (errorCheck(errS)) return
          end if
        end do
      end do

      altsubsubcol(index  ) = z_surf
      altsubsubcol(index+1) = z_boundary_layer
      altsubsubcol(index+2) = z_cloud
      altsubsubcol(index+3) = z_tropopause
      altsubsubcol(index+4) = z_TOA

      ! sort on increasing altitudes
      call indexx(errS, altsubsubcol, sortIndex)
      if (errorCheck(errS)) return

      ! set sortIndex to a large value if the altitude is < -10 km
      do ibound = 1, naltsubsubMax
         z = altsubsubcol(sortIndex(ibound))
         if ( z < -10.0d0 ) sortIndex(ibound) = naltsubsubMax + 100
      end do

      ! initialize z_prev
      z_prev = -10.0d0

      ! find first value with normal altitude
      indexFirst = 1   ! initialization
      do ibound = 1, naltsubsubMax
         index = sortIndex(ibound)
         if ( index < naltsubsubMax + 1 ) then
           z_prev = altsubsubcol(index)
           indexFirst = index
           exit ! leave do-loop
         end if
      end do

      do ibound = 1, naltsubsubMax
         index = sortIndex(ibound)
         if ( (index < naltsubsubMax + 1) .and. (index /= indexFirst) ) then
           z = altsubsubcol(sortIndex(ibound))
           if ( abs( z - z_prev ) < 1.0d-4 ) sortIndex(ibound) = naltsubsubMax + 100
           z_prev = z
         end if
      end do

      index = 1
      do ibound = 1, naltsubsubMax
         if ( sortIndex(ibound) > naltsubsubMax ) cycle ! skip unwanted altitudes
         altsubsubcolSorted(index) = altsubsubcol(sortIndex(ibound))
         index = index + 1
      end do

      nsubsubcol = index - 1
      
      !  altsubsubcolSorted(1:nsubsubcol) now contains the altitudes of the sub-subcolumns

      ! fill Gausspoints and weights
      maxGausspoints = maxval( columnS(1)%nGaussInit(:) ) 

      allocStatus = 0
      allocate( x0(maxGausspoints), w0(maxGausspoints), STAT = allocStatus )
      if ( allocStatus /= 0) then
        call logDebug('FATAL ERROR: allocation failed')
        call logDebug('for x0(maxGausspoints) or w0(maxGausspoints)')
        call logDebug('in subroutine fillAltitudeGridCol')
        call logDebug('in module subcolumnModule')
        call mystop(errS, ' stopped because allocation failed ')
        if (errorCheck(errS)) return
      end if

      dz_pbl      = z_boundary_layer - z_surf
      dz_freeTrop = z_tropopause - z_boundary_layer
      dz_strat    = z_TOA - z_tropopause

      index = 0
      do ibound = 2, nsubsubcol
        dz = altsubsubcolSorted(ibound) - altsubsubcolSorted(ibound-1)
        if ( altsubsubcolSorted(ibound)  <= z_boundary_layer ) nGauss = nint( columnS(1)%nGaussInit(1) * dz / dz_pbl  )
        if ( altsubsubcolSorted(ibound)  >  z_tropopause     ) nGauss = nint( columnS(1)%nGaussInit(3) * dz / dz_strat)
        if ( (altsubsubcolSorted(ibound) <= z_tropopause ) .and. (altsubsubcolSorted(ibound) > z_boundary_layer ) ) &
           nGauss = nint( columnS(1)%nGaussInit(2) * dz / dz_freeTrop)
        if ( nGauss < naltsubsubMin ) nGauss = naltsubsubMin  
        call GaussDivPoints(errS, altsubsubcolSorted(ibound-1), &
                            altsubsubcolSorted(ibound)  , &
                            x0, w0, nGauss)
        if (errorCheck(errS)) return
        do igauss = 1, nGauss
          index = index + 1 
          altGauss   (index) = x0(igauss)
          weightGauss(index) = w0(igauss)
        end do ! gauss
      end do ! ibound

      deallocStatus = 0
      deallocate( x0, w0, STAT = deallocStatus )
      if ( deallocStatus /= 0) then
        call logDebug('FATAL ERROR: deallocation failed')
        call logDebug('for x0 or w0')
        call logDebug('in subroutine fillAltitudeGridCol')
        call logDebug('in module subcolumnModule')
        call mystop(errS, ' stopped because deallocation failed ')
        if (errorCheck(errS)) return
      end if

      nGaussCol = index

      nstateCol = nTrace * nGaussCol

      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case( 'nodeTemp'  , 'offsetTemp' , 'surfPressure', 'surfAlbedo', 'surfEmission' , 'LambCldAlbedo',         &
                'mulOffset', 'straylight',  'aerosolTau',  'aerosolSSA' , 'aerosolAC' , 'cloudFraction', 'cloudTau', &
                'cloudAC'   ,  'intervalDP',  'intervalTop', 'intervalBot', 'diffRingCoef', 'RingCoef' )

            nstateCol = nstateCol + 1

        end select

      end do ! istate

      optPropRTMGridS%nGaussCol = nGaussCol

      do iband = 1, numSpectrBands
        reflDerivHRS(iband)%nGaussCol   = nGaussCol
        earthRadianceS(iband)%nGaussCol = nGaussCol
        reflDerivHRS(iband)%nstateCol   = nstateCol
        earthRadianceS(iband)%nstateCol = nstateCol
      end do
      retrS%nGaussCol  = nGaussCol
      retrS%nstateCol  = nstateCol
      
      ! deallocate arrays if they are allocated because the dimension may have to change
      ! for instance if the altitude of a cloud layer has changed

      deallocStatus    = 0
      sumdeallocStatus = 0

      if ( associated(OptPropRTMGridS%Colaltitude) ) deallocate( OptPropRTMGridS%Colaltitude, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated(OptPropRTMGridS%Colweight)   ) deallocate( OptPropRTMGridS%Colweight, STAT = deallocStatus   )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated(OptPropRTMGridS%ColXsec)     ) deallocate( OptPropRTMGridS%ColXsec, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated(OptPropRTMGridS%Colndens)    ) deallocate( OptPropRTMGridS%Colndens, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated(OptPropRTMGridS%ColndensAP)  ) deallocate( OptPropRTMGridS%ColndensAP, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated(OptPropRTMGridS%ColndensAir) ) deallocate( OptPropRTMGridS%ColndensAir, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus

      do icolumn = 1, ncolumn
        do isubcolumn = 1, columnS(icolumn)%nsubColumn
          if ( associated( columnS(icolumn)%subColumn(isubcolumn)%AK ) ) &
               deallocate( columnS(icolumn)%subColumn(isubcolumn)%AK, STAT = deallocStatus )
          sumdeallocStatus = sumdeallocStatus + deallocStatus
          if ( associated( columnS(icolumn)%subColumn(isubcolumn)%AKx ) ) &
               deallocate( columnS(icolumn)%subColumn(isubcolumn)%AKx, STAT = deallocStatus )
          sumdeallocStatus = sumdeallocStatus + deallocStatus
        end do
      end do

      ! deallocate arrays for the derivatives and optimal estimation matrices
      do iband = 1, numSpectrBands
        if ( associated( reflDerivHRS(iband)%KHR_ndensCol ) )   &
             deallocate( reflDerivHRS(iband)%KHR_ndensCol, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        if ( associated( reflDerivHRS(iband)%KHR_clr_ndensCol ) )   &
             deallocate( reflDerivHRS(iband)%KHR_clr_ndensCol, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        if ( associated( reflDerivHRS(iband)%KHR_cld_ndensCol ) )   &
             deallocate( reflDerivHRS(iband)%KHR_cld_ndensCol, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        if ( associated( earthRadianceS(iband)%KHR_ndensCol ) ) &
             deallocate( earthRadianceS(iband)%KHR_ndensCol, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
        if ( associated( earthRadianceS(iband)%K_ndensCol ) ) &
             deallocate( earthRadianceS(iband)%K_ndensCol, STAT = deallocStatus )
        sumdeallocStatus = sumdeallocStatus + deallocStatus
      end do
      if ( associated( retrS%K_ndensCol )      )  deallocate ( retrS%K_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated( retrS%Sa_ndensCol )     )  deallocate ( retrS%Sa_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated( retrS%S_ndensCol )      )  deallocate ( retrS%S_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated( retrS%Snoise_ndensCol ) )  deallocate ( retrS%Snoise_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated( retrS%A_ndensCol )      )  deallocate ( retrS%A_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus
      if ( associated( retrS%G_ndensCol )      )  deallocate ( retrS%G_ndensCol, STAT = deallocStatus )
      sumdeallocStatus = sumdeallocStatus + deallocStatus

      if (sumdeallocStatus /= 0) then 
        call logDebug('FATAL ERROR: deallocation failed')
        call logDebug('in subroutine fillAltitudeGridCol')
        call logDebug('in module subcolumnModule')
        call mystop(errS, ' stopped because deallocation failed ')
        if (errorCheck(errS)) return
      end if

      ! allocate arrays for the columns
      sumallocStatus = 0
      allocate( OptPropRTMGridS%Colaltitude(OptPropRTMGridS%nGaussCol),       STAT=allocStatus )
      sumallocStatus = allocStatus + sumallocStatus
      allocate( OptPropRTMGridS%Colweight(OptPropRTMGridS%nGaussCol),         STAT=allocStatus )
      sumallocStatus = allocStatus + sumallocStatus
      allocate( OptPropRTMGridS%ColXsec(OptPropRTMGridS%nGaussCol,nTrace),    STAT=allocStatus )
      sumallocStatus = allocStatus + sumallocStatus
      allocate( OptPropRTMGridS%Colndens(OptPropRTMGridS%nGaussCol,nTrace),   STAT=allocStatus )
      sumallocStatus = allocStatus + sumallocStatus
      allocate( OptPropRTMGridS%ColndensAP(OptPropRTMGridS%nGaussCol,nTrace), STAT=allocStatus )
      sumallocStatus = allocStatus + sumallocStatus
      allocate( OptPropRTMGridS%ColndensAir(OptPropRTMGridS%nGaussCol),       STAT=allocStatus )
      sumallocStatus = allocStatus + sumallocStatus

      ! allocate arrays for the derivatives and optimal estimation matrices
      
      do iband = 1, numSpectrBands
        allocate( reflDerivHRS(iband)%KHR_ndensCol(reflDerivHRS(iband)%nwavel,reflDerivHRS(iband)%nstateCol), &
                  STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus

        allocate( reflDerivHRS(iband)%KHR_clr_ndensCol(reflDerivHRS(iband)%nwavel,reflDerivHRS(iband)%nstateCol), &
                  STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus

        allocate( reflDerivHRS(iband)%KHR_cld_ndensCol(reflDerivHRS(iband)%nwavel,reflDerivHRS(iband)%nstateCol), &
                  STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus

        allocate( earthRadianceS(iband)%KHR_ndensCol(earthRadianceS(iband)%nwavelHR, &
                  earthRadianceS(iband)%nstateCol), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus

        allocate( earthRadianceS(iband)%K_ndensCol(earthRadianceS(iband)%nwavel, &
                  earthRadianceS(iband)%nstateCol), STAT = allocStatus )
        sumallocStatus = sumallocStatus + allocStatus
      end do ! iband

      allocate( retrS%K_ndensCol(retrS%nwavelRetr, retrS%nstateCol),   STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( retrS%Sa_ndensCol(retrS%nstateCol, retrS%nstateCol),     STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( retrS%S_ndensCol(retrS%nstateCol, retrS%nstateCol),      STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( retrS%Snoise_ndensCol(retrS%nstateCol, retrS%nstateCol), STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( retrS%A_ndensCol(retrS%nstateCol, retrS%nstateCol),      STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      allocate( retrS%G_ndensCol(retrS%nstateCol, retrS%nwavelRetr),   STAT = allocStatus )
      sumallocStatus = sumallocStatus + allocStatus

      ! allocate AK and AKx
      do icolumn = 1, ncolumn
        do isubcolumn = 1, columnS(icolumn)%nsubColumn
          allocate( columnS(icolumn)%subColumn(isubcolumn)%AK (nGaussCol, columnS(icolumn)%nTrace), &
                    STAT = allocStatus )
          sumallocStatus = sumallocStatus + allocStatus
          allocate( columnS(icolumn)%subColumn(isubcolumn)%AKx(nGaussCol, columnS(icolumn)%nTrace), &
                    STAT = allocStatus )
          sumallocStatus = sumallocStatus + allocStatus
        end do
      end do

      if ( sumallocStatus /= 0 ) then
        call logDebug('FATAL ERROR: allocation failed')
        call logDebug('in subroutine fillAltitudeGridCol')
        call logDebug('in module subcolumnModule')
        call mystop(errS, ' stopped because allocation failed ')
        if (errorCheck(errS)) return
      end if

      ! initialize allocated arrays
      OptPropRTMGridS%Colaltitude(:)  = 0.0d0
      OptPropRTMGridS%Colweight(:)    = 0.0d0
      OptPropRTMGridS%ColXsec(:,:)    = 0.0d0
      OptPropRTMGridS%Colndens(:,:)   = 0.0d0
      OptPropRTMGridS%ColndensAP(:,:) = 0.0d0 
      OptPropRTMGridS%ColndensAir(:)  = 0.0d0

      do iband = 1, numSpectrBands
        reflDerivHRS(iband)%KHR_ndensCol(:,:)       = 0.0d0
        reflDerivHRS(iband)%KHR_clr_ndensCol(:,:)   = 0.0d0
        reflDerivHRS(iband)%KHR_cld_ndensCol(:,:)   = 0.0d0
        earthRadianceS(iband)%KHR_ndensCol(:,:)     = 0.0d0
        earthRadianceS(iband)%K_ndensCol(:,:)       = 0.0d0
      end do ! iband

      retrS%K_ndensCol(:,:)       = 0.0d0
      retrS%Sa_ndensCol(:,:)      = 0.0d0
      retrS%S_ndensCol(:,:)       = 0.0d0
      retrS%Snoise_ndensCol(:,:)  = 0.0d0
      retrS%A_ndensCol(:,:)       = 0.0d0
      retrS%G_ndensCol(:,:)       = 0.0d0

      ! assign values
       do index = 1, optPropRTMGridS%nGaussCol
        OptPropRTMGridS%Colaltitude(index) = altGauss   (index)
        OptPropRTMGridS%Colweight(index)   = weightGauss(index)
      end do

      ! assign values for all of the columns
      do icolumn = 1, ncolumn
        columnS(icolumn)%nGaussCol = optPropRTMGridS%nGaussCol
      end do

      if (verbose) then
        write(intermediateFileUnit, *) 
        write(intermediateFileUnit,'(A)') 'column altitude grid calculated in subcolumnModule:fillAltitudeGridCol'
        write(intermediateFileUnit,'(A)') '   altitude        weight   for sub-subColumn grid'
        do index = 1, optPropRTMGridS%nGaussCol
          write(intermediateFileUnit,'(2F15.8)') OptPropRTMGridS%Colaltitude(index), &
                                                 OptPropRTMGridS%Colweight(index)
        end do
        write(intermediateFileUnit,'(A)') 'sub-subcolumn altitudes calculated in subcolumnModule:fillAltitudeGridCol'
        do ibound = 1, nsubsubcol
          write(intermediateFileUnit,'(F15.8)') altsubsubcolSorted(ibound)
        end do
      end if ! verbose

    end subroutine fillAltitudeGridCol


    subroutine fillAprioriCovarianceColumnGrid(errS, nTrace, gasPTS, traceGasS, optPropRTMGridS, retrS)

      ! fill the a-priori covariance for the column grid and store it in
      ! retrS%Sa_ndensCol(:,:,:) with dimensions (nGaussCol, nGaussCol, nTrace)
      ! The folowing steps are taken
      ! a) For each trace gas the a-priori relative error (in percent) at the retrieval nodes
      !    has been read from the configuration file and is stored in 
      !      traceGasRetrS(iTrace)%relErrorAP(0:nalt) for trace gas iTrace = 1, nTrace
      !    We use spline interpolation to get the a-priori relative error (in percent)
      !    at the column grid. After multiplying the relative error by 0.01d0 and multiplying
      !    it with the number density at the column grid and taking the square we have the diagonal
      !    elements of the a-priori covariance for the column grid.
      ! b) use the correlation length for each trace gas to calculate the non-diagonal elements, if needed.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: nTrace
      type(gasPTType),               intent(in)    :: gasPTS                 ! tropopause altitude
      type(traceGasType),            intent(in)    :: traceGasS(nTrace)
      type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS
      type(retrType),                intent(inout) :: retrS

      ! local
      integer       :: ilevel, jlevel, iTrace, index, indexCol, istate
      integer       :: startIndex
      integer       :: nalt, nGaussCol
      real(8)       :: absDist

      ! allocation parameters
      integer       :: sumAllocStatus, allocStatus, deallocStatus

      ! index uppermost level in troposphere
      integer               :: indexUppermostTropLevel

      ! parameters for spline interpolation
      integer               :: statusSpline, statusSplint
      real(8), allocatable  :: alt(:)
      real(8), allocatable  :: relErrorAP(:)
      real(8), allocatable  :: SDrelErrorAP(:)
      real(8)               :: relErrorAPCol(optPropRTMGridS%nGaussCol)
      real(8)               :: altitudeCol  (optPropRTMGridS%nGaussCol)

      nGaussCol      = optPropRTMGridS%nGaussCol
      altitudeCol(:) = optPropRTMGridS%Colaltitude(:)

      ! initialization
      retrS%Sa_ndensCol(:,:) = 0.0d0

      do iTrace = 1, nTrace

        nalt = traceGasS(iTrace)%nalt

        allocStatus    = 0
        sumAllocStatus = 0
        allocate( alt(0:nalt),          STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( relErrorAP(0:nalt),   STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus
        allocate( SDrelErrorAP(0:nalt), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus

        if ( sumAllocStatus /= 0 ) then
          call logDebug('FATAL ERROR: allocation failed')
          call logDebug('for alt, relErrorAP, or SDrelErrorAP')
          call logDebug('in subroutine fillAprioriCovarianceColumnGrid')
          call logDebug('in module subcolumnModule')
          call mystop(errS, 'stopped because allocation failed')
          if (errorCheck(errS)) return
        end if

        ! fill values on retrieval grid
        alt(0:nalt)        = traceGasS(iTrace)%alt(0:nalt)
        relErrorAP(0:nalt) = traceGasS(iTrace)%relErrorAP(0:nalt)

        ! prepare for spline interpolation
        call spline(errS, alt, relErrorAP, SDrelErrorAP, statusSpline)
        if (errorCheck(errS)) return

        ! interpolation of the relative error
        do ilevel = 1, nGaussCol
          relErrorAPCol(ilevel) = splint(errS, alt, relErrorAP, SDrelErrorAP, altitudeCol(ilevel), statusSplint)
        end do

        startIndex = (iTrace -1) * nGaussCol
        ! calculate values for the diagonal
        do ilevel = 1, nGaussCol
          index = startIndex + ilevel
          retrS%Sa_ndensCol(index,index) = &
              ( 1.0d-2 * relErrorAPCol(ilevel) * optPropRTMGridS%Colndens(ilevel,iTrace) )**2
        end do

        ! calculate the non-diagonal elements
        if ( traceGasS(iTrace)%useAPCorrLength ) then
          
          do jlevel = 1, nGaussCol
            do ilevel = 1, nGaussCol
              absDist = abs( altitudeCol(ilevel) - altitudeCol(jlevel) )
              retrS%Sa_ndensCol(startIndex+ilevel,startIndex+jlevel) = &
                      exp(- absDist / traceGasS(iTrace)%APcorrLength ) &
                      * sqrt( retrS%Sa_ndensCol(startIndex+ilevel,startIndex+ilevel) )  &
                      * sqrt( retrS%Sa_ndensCol(startIndex+jlevel,startIndex+jlevel) )
            end do ! ilevel
          end do ! jlevel
        end if

        ! When traceGasS(iTrace)%removeAPcorrTropStrat is .true. set correlation between
        ! stratospheric and tropospheric levels to zero. Note that we 
        ! cannot use toplevelTroposphere because that is the top level
        ! for the retrieval grid, not for the subcolumn grid

        if ( traceGasS(iTrace)%removeAPcorrTropStrat ) then

          ! determine top level troposphere
          do ilevel = 1, nGaussCol
            if ( altitudeCol(ilevel) <= gasPTS%tropopauseAltitude ) then
              indexUppermostTropLevel = ilevel
            else
              exit   ! exit ilevel loop
            end if
          end do
          
          do jlevel = indexUppermostTropLevel + 1, nGaussCol
            do ilevel = 1, indexUppermostTropLevel
              retrS%Sa_ndensCol(startIndex+ilevel,startIndex+jlevel) = 0.0d0
            end do
          end do
          do jlevel = 1, indexUppermostTropLevel
            do ilevel =  indexUppermostTropLevel + 1, nGaussCol
              retrS%Sa_ndensCol(startIndex+ilevel,startIndex+jlevel) = 0.0d0
            end do
          end do
        end if

        deallocStatus = 0
        deallocate( alt, relErrorAP, SDrelErrorAP, STAT = deallocStatus )
        if ( deallocStatus /= 0 ) then
          call logDebug('FATAL ERROR: deallocation failed')
          call logDebug('for alt, relErrorAP, or SDrelErrorAP')
          call logDebug('in subroutine fillAprioriCovarianceColumnGrid')
          call logDebug('in module subcolumnModule')
          call mystop(errS, 'stopped because deallocation failed')
          if (errorCheck(errS)) return
        end if

      end do ! iTrace

      ! Fill the remaining elements of the a-priori covariance matrix
      ! Here we have to exclude the a-priori covariance for the total column, because
      ! we simulate profile retrieval here in order to calculate the errors for the (sub)columns.
      ! for the other elements we assume that there is no correlation

      ! we go through the fit parameters and if a parameter (not profile or column) is fitted
      ! we copy the a-priori value from the value in retrS%Sa_ndens

      indexCol = nTrace * nGaussCol

      do istate = 1, retrS%nstate

        select case (retrS%codeFitParameters(istate))

          case( 'surfAlbedo', 'LambCldAlbedo', 'mulOffset', 'straylight', 'aerosolTau', 'aerosolSSA', 'aerosolAC', &
                'nodeTemp', 'offsetTemp', 'cloudFraction', 'cloudTau', 'intervalDP', 'cloudAC', 'surfPressure',    &
                'intervalTop', 'intervalBot' )

            indexCol = indexCol + 1
            retrS%Sa_ndensCol(indexCol,indexCol) = retrS%Sa_ndens(istate, istate)

        end select

      end do ! istate

    end subroutine fillAprioriCovarianceColumnGrid


    subroutine calculateErrorCovColumnGrid(errS, retrS)

      ! calculate the error covariance matrix and the averaging kernel 
      ! for the number density on the column grid using
      ! 
      ! S = ( KT SeInv K + SaInv)-1   and A = S KT SeInv K
      ! 
      ! To improve stability whitening and SVD is used
      !
      ! it is assumed that the noise matrix Se is diagonal

      implicit none

      type(errorType), intent(inout) :: errS
      type(retrType),                intent(inout) :: retrS

      ! local
      real(8) :: Kwhite(retrS%nwavelRetr,retrS%nstateCol)
      real(8) :: sqrtSa(retrS%nstateCol,retrS%nstateCol), sqrtInvSa(retrS%nstateCol,retrS%nstateCol) 
      real(8) :: SaInv(retrS%nstateCol,retrS%nstateCol)
      real(8) :: Awhite(retrS%nstateCol,retrS%nstateCol), Atrans(retrS%nstateCol,retrS%nstateCol)
      real(8) :: Swhite(retrS%nstateCol,retrS%nstateCol), Strans(retrS%nstateCol,retrS%nstateCol)

      ! matrices for svd
      real(8) :: w(retrS%nstateCol)
      real(8) :: ua(retrS%nstateCol,retrS%nstateCol), uaT(retrS%nstateCol,retrS%nstateCol)
      real(8) :: u(retrS%nwavelRetr,retrS%nstateCol), uT(retrS%nstateCol,retrS%nwavelRetr)
      real(8) :: v(  retrS%nstateCol,retrS%nstateCol), vT(retrS%nstateCol,  retrS%nstateCol)

      real(8) :: diagSaInv(retrS%nstateCol, retrS%nstateCol)
      real(8) :: diagsqrtSa(retrS%nstateCol, retrS%nstateCol)
      real(8) :: diagsqrtInvSa(retrS%nstateCol, retrS%nstateCol)

      real(8) :: wMax 

      ! various
      integer    :: istate, jstate, iwave

      logical, parameter :: verbose = .false.
      
      ! calculate the inverse of Sa 

      diagSaInv     = 0.0d0
      diagsqrtSa    = 0.0d0
      diagsqrtInvSa = 0.0d0

      !write(*,*) 'szie ua, w, v, retrS%nstateCol', size(ua),  size(w), size(v), retrS%nstateCol
      ua = retrS%Sa_ndensCol
      ! svdcmp overwrites first array (input) with u
      call svdcmp(errS, ua, w, v, retrS%nstateCol)
      if (errorCheck(errS)) return
      uaT = transpose(ua)

      wMax = maxval( abs( w(:) ) )

      ! write(intermediateFileUnit,*)
      ! write(intermediateFileUnit,'(A)') 'w for Sa'
      ! write(intermediateFileUnit,'(100ES15.6)') w

      do istate = 1, retrS%nstateCol
          diagSaInv(istate,istate)     = 1.0d0 / w(istate)
          diagsqrtSa(istate,istate)    = sqrt(w(istate))
          diagsqrtInvSa(istate,istate) = 1.0d0 / diagsqrtSa(istate,istate)
      end do

      sqrtInvSa = matmul(v, matmul(diagsqrtInvSa, uaT) )
      sqrtSa    = matmul(v, matmul(diagsqrtSa   , uaT) )
      SaInv     = matmul(v, matmul(diagSaInv    , uaT) )

      ! calculate Kwhite

      Kwhite   = matmul(retrS%sqrtInvSe, matmul(retrS%K_ndensCol, sqrtSa))

      ! find the singular value decomposition for Kwhite = U W VT

      u = Kwhite
    
      ! svdcmp overwrites first array (input) with u
      call svdcmp(errS, u, w, v, retrS%nstateCol)
      if (errorCheck(errS)) return
      vT = transpose(v)
      uT = transpose(u)

      wMax = maxval( abs( w(:) ) )

      ! write(intermediateFileUnit,*)
      ! write(intermediateFileUnit,'(A)') 'w for Kwhite'
      ! write(intermediateFileUnit,'(100ES15.6)') w
       
      Strans    = 0.0d0
      Atrans    = 0.0d0
      do istate = 1, retrS%nstateCol
          Strans(istate,istate)    = 1.0d0 / (1.0d0 + w(istate)**2)
          Atrans(istate,istate)    = w(istate)**2 * Strans(istate,istate)
      end do

      Swhite    = matmul(v,matmul(Strans,vT))
      Awhite    = matmul(v,matmul(Atrans,vT))

      retrS%S_ndensCol      = matmul(sqrtSa,matmul(Swhite,sqrtSa))
      retrS%A_ndensCol      = matmul(sqrtSa,matmul(Awhite,sqrtInvSa))
      retrS%G_ndensCol      = matmul(matmul(retrS%S_ndensCol,transpose(retrS%K_ndensCol)), retrS%InvSe)
      retrS%Snoise_ndensCol = matmul(matmul(retrS%G_ndensCol,retrS%Se), transpose(retrS%G_ndensCol))


      if (verbose) then
        write(intermediateFileUnit,*)
        write(intermediateFileUnit,'(A)') 'a-priori error covariance'
        do istate = 1, retrS%nstateCol
          write(intermediateFileUnit,'(250E15.5)') &
                (retrS%Sa_ndensCol(istate,jstate), jstate = 1, retrS%nstateCol)
        end do
        write(intermediateFileUnit,'(A)') 'aposteriori error covariance'
        do istate = 1, retrS%nstateCol
          write(intermediateFileUnit,'(250E15.5)') &
                (retrS%S_ndensCol(istate,jstate), jstate = 1, retrS%nstateCol)
        end do
        write(intermediateFileUnit,'(A)') 'Transposed G_ndensCol'
        do iwave = 1, retrS%nwavelRetr
          write(intermediateFileUnit,'(250E15.5)') &
                (retrS%G_ndensCol(istate,jstate), jstate = 1, retrS%nstateCol)
        end do
      end if


    end subroutine calculateErrorCovColumnGrid


    subroutine calculateColumnPropertiesRetr(errS, ncolumn, nTrace, gasPTS, traceGasS, &
                                             optPropRTMGridS, retrS, columnS)

      ! calculate the properties of the (sub)columns stored in columnS
      !
      ! As the values are given at Gaussian division points the (sub)column can be
      ! calculated using Gaussian integration, i.e. multiply wih the Gaussian weights.
      ! For a subcolumn we use only the altitudes that lie within the altitude range of the subcolum
      ! The average vmr is ontained by dividing the column of the trace gas by the column of air
      ! and multiplying it with 1.0d6 to obtain the value in ppmv.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: ncolumn
      integer,                       intent(in)    :: nTrace
      type(gasPTType),               intent(in)    :: gasPTS                 ! tropopause pressure
      type(traceGasType),            intent(in)    :: traceGasS(nTrace)
      type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS
      type(retrType),                intent(inout) :: retrS
      type(columnType),              intent(inout) :: columnS(ncolumn)


      ! local

      integer              :: iGauss, iTrace, icolumn, isubcolumn
      integer              :: startIndex, endIndex, ngaussCol
      real(8)              :: column, columnAP, columnAir
      ! submatrices for the individual trace gases on gauss grid for the columns
      real(8)              :: STr(optPropRTMGridS%nGaussCol, optPropRTMGridS%nGaussCol)
      real(8)              :: SaTr(optPropRTMGridS%nGaussCol, optPropRTMGridS%nGaussCol)
      real(8)              :: SnoiseTr(optPropRTMGridS%nGaussCol, optPropRTMGridS%nGaussCol)
      real(8)              :: GainTr (optPropRTMGridS%nGaussCol, columnS(ncolumn)%nwavel)
      real(8)              :: ATr (optPropRTMGridS%nGaussCol, optPropRTMGridS%nGaussCol)
      ! W matrices and matrices for the columns
      real(8), allocatable :: W(:,:), WT(:,:), WTAK(:,:)
      real(8), allocatable :: removeWeight(:,:)

      ! allocation parameters
      integer    :: sumAllocStatus, allocStatus
      integer    :: sumdeallocStatus, deallocStatus

      logical, parameter :: verbose = .false.

      do icolumn = 1, ncolumn
        do isubcolumn = 1, columnS(icolumn)%nsubColumn
          do iTrace = 1, nTrace
            column    = 0.0d0
            columnAP  = 0.0d0
            columnAir = 0.0d0
            do iGauss = 1, optPropRTMGridS%nGaussCol
              ! the weights are for km and the number density is in cm-3 => need a factor 1.0d5
              if (   (optPropRTMGridS%Colaltitude(iGauss) >  columnS(icolumn)%altBound(isubcolumn - 1)) &
               .and. (optPropRTMGridS%Colaltitude(iGauss) <  columnS(icolumn)%altBound(isubcolumn    )) ) then
                column    = column &
                          + optPropRTMGridS%Colweight(iGauss) * optPropRTMGridS%Colndens(iGauss, iTrace) *1.0d5
                columnAP  = columnAP &
                          + optPropRTMGridS%Colweight(iGauss) * optPropRTMGridS%ColndensAP(iGauss, iTrace) *1.0d5
                columnAir = columnAir &
                          + optPropRTMGridS%Colweight(iGauss) * optPropRTMGridS%ColndensAir(iGauss) *1.0d5
              end if
            end do
            columnS(icolumn)%subColumn(isubcolumn)%column   (iTrace) = column
            columnS(icolumn)%subColumn(isubcolumn)%columnAP (iTrace) = columnAP
            columnS(icolumn)%subColumn(isubcolumn)%columnAir(iTrace) = columnAir
            columnS(icolumn)%subColumn(isubcolumn)%meanVmr  (iTrace) = 1.0d6 * column / columnAir
            columnS(icolumn)%subColumn(isubcolumn)%meanVmrAP(iTrace) = 1.0d6 * columnAP / columnAir
          end do
        end do
      end do

      ! calculate a-priori covariance matrix on column grid
      call fillAprioriCovarianceColumnGrid(errS, nTrace, gasPTS, traceGasS, optPropRTMGridS, retrS)
      if (errorCheck(errS)) return

      ! calculate error covariance and noise error covariance on column grid
      call calculateErrorCovColumnGrid(errS, retrS)
      if (errorCheck(errS)) return

      ! evaluate the errors for the subcolumns
      
      do icolumn = 1, ncolumn

        allocStatus    = 0
        sumAllocStatus = 0

        allocate( W(optPropRTMGridS%nGaussCol, columnS(icolumn)%nsubColumn),            STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus

        allocate( WT(columnS(icolumn)%nsubColumn, optPropRTMGridS%nGaussCol),           STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus

        allocate( WTAK(columnS(icolumn)%nsubColumn, optPropRTMGridS%nGaussCol),         STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus

        allocate( removeWeight(optPropRTMGridS%nGaussCol, columnS(icolumn)%nsubColumn), STAT = allocStatus )
        sumAllocStatus = sumAllocStatus + allocStatus

        if ( sumAllocStatus /= 0 ) then
          call logDebug('FATAL ERROR: allocation failed')
          call logDebug('for W, WT, WTAK, or removeWeight')
          call logDebug('in subroutine calculateColumnPropertiesRetr')
          call logDebug('in module subcolumnModule')
          call mystop(errS, 'stopped because allocation failed')
          if (errorCheck(errS)) return
        end if

        ! set up W matrix for the subcolumns
        W = 0.0d0
        do isubcolumn = 1, columnS(icolumn)%nsubColumn
          do iGauss = 1, optPropRTMGridS%nGaussCol
            if (   (optPropRTMGridS%Colaltitude(iGauss) >  columnS(icolumn)%altBound(isubcolumn - 1)) &
             .and. (optPropRTMGridS%Colaltitude(iGauss) <  columnS(icolumn)%altBound(isubcolumn    )) ) then

              ! the weights in Colweight are for km and the number density is in cm-3 => need a factor 1.0d5
              W(iGauss, isubcolumn) = 1.0d5 * optPropRTMGridS%Colweight(iGauss)
            end if
            removeWeight(iGauss, isubcolumn) = 1.0d-5 / optPropRTMGridS%Colweight(iGauss)
          end do ! iGauss
        end do ! isubcolumn

        WT = transpose(W)

        do iTrace = 1, nTrace

! JdH debug
! The code fails if for some trace gases the profile is retrieved, and for
! others the column or nothing at all (e.g. O2 for cloud properties).
! As temporary solution assume that that the profile is fitted only for the first trace gas.

          if ( iTrace > 1 ) cycle

          ! fill submatrices
          ngaussCol  = optPropRTMGridS%nGaussCol
          startIndex = (iTrace - 1) * ngaussCol + 1
          endIndex   =  iTrace      * ngaussCol
          STr     (1:ngaussCol,1:ngaussCol) = retrS%S_ndensCol     (startIndex:endIndex, startIndex:endIndex)
          SaTr    (1:ngaussCol,1:ngaussCol) = retrS%Sa_ndensCol    (startIndex:endIndex, startIndex:endIndex)
          SnoiseTr(1:ngaussCol,1:ngaussCol) = retrS%Snoise_ndensCol(startIndex:endIndex, startIndex:endIndex)
          ATr     (1:ngaussCol,1:ngaussCol) = retrS%A_ndensCol     (startIndex:endIndex, startIndex:endIndex)
          GainTr  (1:ngaussCol,:)           = retrS%G_ndensCol     (startIndex:endIndex, :)

          if ( verbose .and. (icolumn == 1)) then
            write(intermediateFileUnit,*)
            write(intermediateFileUnit,'(15X,100E15.5)') &
                   (optPropRTMGridS%Colaltitude(iGauss), iGauss = 1, optPropRTMGridS%nGaussCol)
            write(intermediateFileUnit,'(A, I4)') '     alt       Sa on column grid, iTrace = ', iTrace
            do iGauss = 1, optPropRTMGridS%nGaussCol
              write(intermediateFileUnit,'(100E15.5)') optPropRTMGridS%Colaltitude(iGauss), SaTr(iGauss,:)
            end do ! iGauss
            write(intermediateFileUnit,'(A, I4)') '     alt       S on column grid, iTrace = ', iTrace
            do iGauss = 1, optPropRTMGridS%nGaussCol
              write(intermediateFileUnit,'(100E15.5)') optPropRTMGridS%Colaltitude(iGauss), STr(iGauss,:)
            end do ! iGauss
            write(intermediateFileUnit,'(A, I4)') '     alt       A on column grid, iTrace = ', iTrace
            do iGauss = 1, optPropRTMGridS%nGaussCol
              write(intermediateFileUnit,'(100E15.5)') optPropRTMGridS%Colaltitude(iGauss), ATr(iGauss,:)
            end do ! iGauss
          end if ! verbose

          
          columnS(icolumn)%S(:,:,iTrace)      = matmul( matmul(WT,STr), W)
          columnS(icolumn)%Sa(:,:,iTrace)     = matmul( matmul(WT,SaTr), W)
          columnS(icolumn)%Snoise(:,:,iTrace) = matmul( matmul(WT,SnoiseTr), W)
          columnS(icolumn)%Gain(:,:,iTrace)   = matmul(WT,GainTr)
          do isubcolumn = 1, columnS(icolumn)%nsubColumn
            columnS(icolumn)%subColumn(isubcolumn)%errorCol(iTrace)      = &
                  sqrt(columnS(icolumn)%S(isubcolumn, isubcolumn, iTrace))
            columnS(icolumn)%subColumn(isubcolumn)%errorColAP(iTrace)    = &
                  sqrt(columnS(icolumn)%Sa(isubcolumn, isubcolumn, iTrace))
            columnS(icolumn)%subColumn(isubcolumn)%noiseErrorCol(iTrace) = &
                  sqrt(columnS(icolumn)%Snoise(isubcolumn, isubcolumn, iTrace))
          end do  ! isubcolumn

          ! we have x = xa + A(x_true - xa)
          ! and WT x  = WT xa + WT A (x_true - xa) so that WT A is the averaging kernel pertaining to WT x
          ! In order to interpret the results the Gaussian weights are removed
          WTAK = matmul( WT, ATr )
          do isubcolumn = 1, columnS(icolumn)%nsubColumn
            columnS(icolumn)%subColumn(isubcolumn)%AK (:, iTrace) = WTAK(isubcolumn,:) * removeWeight(:,isubcolumn)
            columnS(icolumn)%subColumn(isubcolumn)%AKx(:, iTrace) = &
                 WTAK(isubcolumn,:) * optPropRTMGridS%Colndens(:,iTrace) * removeWeight(:,isubcolumn) &
               / columnS(icolumn)%subColumn(isubcolumn)%column(iTrace) &
               * ( columnS(icolumn)%altBound(isubcolumn) - columnS(icolumn)%altBound(isubcolumn - 1) ) * 1.0d5  
          end do

        end do ! iTrace

        if ( verbose ) then
          write(intermediateFileUnit,*)
          write(intermediateFileUnit,'(A,100F15.5)') 'alt =', &
                  (optPropRTMGridS%Colaltitude(iGauss), iGauss = 1, optPropRTMGridS%nGaussCol)
          write(intermediateFileUnit,'(A, I4)') 'WT for subcolumns; icolumn = ', icolumn
          do isubcolumn = 1, columnS(icolumn)%nsubColumn
            write(intermediateFileUnit,'(5X,100E15.5)') (WT(isubcolumn,iGauss), iGauss = 1, optPropRTMGridS%nGaussCol)
          end do ! isubcolumn
        end if ! verbose

        deallocStatus    = 0
        sumdeAllocStatus = 0

        deallocate( W,            STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus

        deallocate( WT,           STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus

        deallocate( WTAK,         STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus

        deallocate( removeWeight, STAT = deallocStatus )
        sumdeAllocStatus = sumdeAllocStatus + deallocStatus

        if ( sumdeAllocStatus /= 0 ) then
          call logDebug('FATAL ERROR: deallocation failed')
          call logDebug('for W, WT, WTAK, or removeWeight')
          call logDebug('in subroutine calculateColumnPropertiesRetr')
          call logDebug('in module subcolumnModule')
          call mystop(errS, 'stopped because deallocation failed')
          if (errorCheck(errS)) return
        end if

      end do ! icolumn

      ! give the errors in percent
      do icolumn = 1, ncolumn
        do isubcolumn = 1, columnS(icolumn)%nsubColumn
          do iTrace = 1, nTrace

! JdH debug
! The code fails if for some trace gases the profile is retrieved, and for
! others the column or nothing at all (e.g. O2 for cloud properties).
! As temporary solution assume that that the profile is fitted only for the first trace gas.

            if ( iTrace > 1 ) cycle
            columnS(icolumn)%subColumn(isubcolumn)%errorCol(iTrace) = &
              1.0d2 * columnS(icolumn)%subColumn(isubcolumn)%errorCol(iTrace) &
                    / columnS(icolumn)%subColumn(isubcolumn)%column(iTrace)
            columnS(icolumn)%subColumn(isubcolumn)%errorColAP(iTrace) = &
              1.0d2 * columnS(icolumn)%subColumn(isubcolumn)%errorColAP(iTrace) &
                    / columnS(icolumn)%subColumn(isubcolumn)%columnAP(iTrace)
            columnS(icolumn)%subColumn(isubcolumn)%noiseErrorCol(iTrace) = &
              1.0d2 * columnS(icolumn)%subColumn(isubcolumn)%noiseErrorCol(iTrace) &
                    / columnS(icolumn)%subColumn(isubcolumn)%column(iTrace)
          end do
        end do
      end do

    end subroutine calculateColumnPropertiesRetr


    subroutine calculateColumnPropertiesSim(errS, ncolumn, nTrace, optPropRTMGridS, columnS)

      ! calculate the properties of the (sub)columns for simulation stored in columnS
      !
      ! As the values are given at Gaussian division points the (sub)column can be
      ! calculated using Gaussian integration, i.e. multiply wih the Gaussian weights.
      ! For a subcolumn we use only the altitudes that lie within the altitude range of the subcolum
      ! The average vmr is ontained by dividing the column of the trace gas by the column of air
      ! and multiplying it with 1.0d6 to obtain the value in ppmv.

      implicit none

      type(errorType), intent(inout) :: errS
      integer,                       intent(in)    :: ncolumn
      integer,                       intent(in)    :: nTrace
      type(optPropRTMGridType),      intent(in)    :: optPropRTMGridS
      type(columnType),              intent(inout) :: columnS(ncolumn)

      ! local

      integer     :: iGauss, iTrace, icolumn, isubcolumn
      real(8)     :: column, columnAP, columnAir

      do icolumn = 1, ncolumn
        do isubcolumn = 1, columnS(icolumn)%nsubColumn
          do iTrace = 1, nTrace
            column    = 0.0d0
            columnAP  = 0.0d0
            columnAir = 0.0d0
            do iGauss = 1, optPropRTMGridS%nGaussCol
              if (   (optPropRTMGridS%Colaltitude(iGauss) >  columnS(icolumn)%altBound(isubcolumn - 1)) &
               .and. (optPropRTMGridS%Colaltitude(iGauss) <  columnS(icolumn)%altBound(isubcolumn    )) ) then
                column    = column &
                          + optPropRTMGridS%Colweight(iGauss) * optPropRTMGridS%Colndens(iGauss, iTrace) * 1.0d5
                columnAP  = columnAP &
                          + optPropRTMGridS%Colweight(iGauss) * optPropRTMGridS%ColndensAP(iGauss, iTrace) * 1.0d5
                columnAir = columnAir &
                          + optPropRTMGridS%Colweight(iGauss) * optPropRTMGridS%ColndensAir(iGauss) * 1.0d5
              end if
            end do
            columnS(icolumn)%subColumn(isubcolumn)%column   (iTrace) = column
            columnS(icolumn)%subColumn(isubcolumn)%columnAP (iTrace) = columnAP
            columnS(icolumn)%subColumn(isubcolumn)%columnAir(iTrace) = columnAir
            columnS(icolumn)%subColumn(isubcolumn)%meanVmr  (iTrace) = 1.0d6 * column / columnAir
            columnS(icolumn)%subColumn(isubcolumn)%meanVmrAP(iTrace) = 1.0d6 * columnAP / columnAir
          end do
        end do
      end do


      ! set the errors for the subcolumns to zero
      
      do icolumn = 1, ncolumn
        do isubcolumn = 1, columnS(icolumn)%nsubColumn
          columnS(icolumn)%subColumn(isubcolumn)%errorCol(:)      = 0.0d0
          columnS(icolumn)%subColumn(isubcolumn)%errorColAP(:)    = 0.0d0
          columnS(icolumn)%subColumn(isubcolumn)%noiseErrorCol(:) = 0.0d0
          columnS(icolumn)%subColumn(isubcolumn)%AK(:,:)          = 0.0d0
        end do
      end do

    end subroutine calculateColumnPropertiesSim


end module subcolumnModule

