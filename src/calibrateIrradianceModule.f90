!
! This file is part of the DISAMAR radiative transfer code.
! 
! See the LICENSE.txt file for details on licensing.
! See the CONTRIBUTORS.txt file for a list of contributing authors.
! 
! Further information can be found in the doc directory.
!

  module calibrateIrradianceSlitFunction

    ! This module contains subroutines to calibrate the instrument slit function for the
    ! irradiance. After convolution of the high resolution solar irradiance with the
    ! (modified) slit function the simulated irradiance should agree with the 
    ! measured irradiance. For this purpose three parameters are fitted:
    ! - a multiplicative scale factor for the slit function
    ! - the width of the slit function
    ! - the nominal wavelength of the slit function
    ! each of which is a low degree polynomial in the wavelength.
    ! Note that we account for the Doppler shift and the distance sun-earth
    ! implicitly in this manner. Hence we do not need to know the velocity
    ! component of the instrument in the direction of the sun. 
    ! The multiplicative scale factor found for the irradiance is also used for the
    ! radiance, but the width of the slit function and the wavelength shift is fitted
    ! separately for the radiance. Here we demans that the integral over the slit function
    ! it sels is the same for irradiance and radiance.

  use dataStructures
  use inputModule
  use readModule,               only: getHRSolarIrradiance
  use mathTools,                only: spline, splint, splintLin, gaussDivPoints, GaussDivPoints, polyInt
  use radianceIrradianceModule, only: integrateSlitFunctionIrr

  ! default is private
  private 
  public  :: readIrrRadFromFile

  contains

    subroutine readIrrRadFromFile(errS, numSpectrBands, controlSimS, wavelInstrIrrSimS, wavelInstrRadSimS, &
                                  solarIrradianceSimS, earthRadianceSimS)

    ! Read ASCII file with irradiance and radiance data
    ! values read are wavelength (nm), SNR, and irradiance or radiance.
    ! Header lines starting with '# ' in the first and second column are skipped.
    ! Example:
    ! # Header 1
    ! # Header 2
    ! start_fit_window_irr
    ! irr  460.1500     3000.88   3.402296E+14
    ! irr  460.3000     2901.97   3.202296E+14
    ! end_fit_window_irr
    ! start_fit_window_rad
    ! rad  460.1500     1485.88   1.116153E+13
    ! rad  460.3000     1445.97   1.116153E+13
    ! end_fit_window_rad
    ! start fit window_irr
    ! irr  560.1500     3285.88   3.402296E+14
    ! irr  560.3000     3245.97   3.472296E+14
    ! end_fit_window_irr
    ! start fit window_rad
    ! rad  560.1500     1285.88   2.402296E+13
    ! rad  560.3000     1245.97   2.472296E+13
    ! end_fit_window_rad
    ! end_file
    !
    ! An older version of the file where the sun-normalized radiance
    ! is read can still be used.
    !
    ! NOTE: the module inputModule is used (parser)
    ! Remarks: The wavelength intervals are called fit windows because they are fit windows
    !          when the file read is the result of a simulation. However, they are (parts of) 
    !          spectral channels when the file read is a measured spectrum. Note that the spectral
    !          channels and the fit windows can (partly) overlap. It is demanded that
    !          each fit window specified in the configuration file falls within a spectral
    !          channel that is read here. That means that in the configuration file
    !          two fit windows have to be specified if the original window contains a channel
    !          boundary. This is done because in general the FWHM of the slit function will
    !          differ for different channels.
    

      implicit none

      integer,                 intent(in)    :: numSpectrBands                      ! number of spectral bands / fit windows
      type(controlType),       intent(in)    :: controlSimS                         ! control parameters (simulation)
      type(wavelInstrType),    intent(inout) :: wavelInstrIrrSimS(numSpectrBands)   ! wavelength grid for measured irradiance
      type(wavelInstrType),    intent(inout) :: wavelInstrRadSimS(numSpectrBands)   ! wavelength grid for measured radiance
      type(solarIrrType),      intent(inout) :: solarIrradianceSimS(numSpectrBands) ! solar irr read from file
      type(earthRadianceType), intent(inout) :: earthRadianceSimS(numSpectrBands)   ! earth radiance read from file

      ! local
      integer, parameter :: maxNumWavelengths = 1200
      integer, parameter :: maxNumChannels = 10

      integer            :: OpenError
      integer            :: iband, iwave, iwave_irr, iwave_rad
      integer            :: ichannel, ichannel_irr, ichannel_rad 
      integer            :: nchannel, nchannel_irr, nchannel_rad
      integer            :: nwavel(maxNumChannels), nwavel_irr(maxNumChannels), nwavel_rad(maxNumChannels)
      integer            :: numChannelsForFitWindow, numChannelsForFitWindow_irr, numChannelsForFitWindow_rad


      real(8)            :: wavelength    (maxNumWavelengths, maxNumChannels)
      real(8)            :: wavelength_irr(maxNumWavelengths, maxNumChannels)
      real(8)            :: wavelength_rad(maxNumWavelengths, maxNumChannels)
      real(8)            :: SNR           (maxNumWavelengths, maxNumChannels)
      real(8)            :: SNR_irr       (maxNumWavelengths, maxNumChannels)
      real(8)            :: SNR_rad       (maxNumWavelengths, maxNumChannels)
      real(8)            :: reflectance   (maxNumWavelengths, maxNumChannels)
      real(8)            :: irradiance    (maxNumWavelengths, maxNumChannels)
      real(8)            :: radiance      (maxNumWavelengths, maxNumChannels)
      integer            :: indexArray    (maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_irr(maxNumWavelengths, maxNumChannels)
      integer            :: indexArray_rad(maxNumWavelengths, maxNumChannels)

      integer            :: counter, ipair
      real(8)            :: wavel
      logical            :: addWavel
      logical            :: old_version

      integer            :: channelNumber    (numSpectrBands)  ! channel number for each fit window
      integer            :: channelNumber_irr(numSpectrBands)  ! channel number for each fit window
      integer            :: channelNumber_rad(numSpectrBands)  ! channel number for each fit window

      logical            :: eof

      character(LEN = 40)   :: identifier
      real(8)               :: value       ! dummy value

      logical, parameter    :: verbose = .true.


      open(UNIT= readIrrRadUnit, ACTION= 'READ', STATUS = 'old', IOSTAT= OpenError, &
           FILE = controlSimS%ASCIIinputIrrRadFileName)
      if (OpenError /= 0) then
        call logDebug('ERROR in subroutine readIrrRadFromFile: failed to open file')
        call logDebug('in module readModule')
        call logDebugF('controlS%ASCIIinputIrrRadFileName: ', controlSimS%ASCIIinputIrrRadFileName)
         call mystop(errS, 'stopped because file could not be opened')
         if (errorCheck(errS)) return
      end if

      ! initialize

      wavelength    (:,:)  = 0.0d0
      wavelength_irr(:,:)  = 0.0d0
      wavelength_rad(:,:)  = 0.0d0
      SNR(:,:)             = 0.0d0
      SNR_irr(:,:)         = 0.0d0
      SNR_rad(:,:)         = 0.0d0
      reflectance(:,:)     = 0.0d0
      irradiance(:,:)      = 0.0d0
      radiance(:,:)        = 0.0d0
      nwavel(:)            = 0
      nwavel_irr(:)        = 0
      nwavel_rad(:)        = 0

      ! set options for the parser
!      call input_options(errS, echo_lines=.false.,skip_blank_lines=.true.,error_flag=1, &
!                         concat_string='&')
!      if (errorCheck(errS)) return

      ! Read data from file and store it in the arrays wavelength, SNR and reflectance

      ichannel     = 1
      ichannel_irr = 1
      ichannel_rad = 1

      do
        call read_line(errS, eof, readIrrRadUnit)
        if (errorCheck(errS)) return
        if (eof) exit

        call reada(errS, identifier)
        if (errorCheck(errS)) return

        select case (identifier)

           case('start_fit_window')

             old_version = .true.
             iwave = 1

           case('start_channel_irr')

             old_version = .false.
             iwave_irr = 1

           case('start_channel_rad')

             old_version = .false.
             iwave_rad = 1

           case('end_fit_window')

             old_version = .true.
             nchannel = ichannel
             ichannel = ichannel + 1

             if ( ichannel > maxNumChannels + 1) then
               call logDebug('ERROR subroutine readIrrRadFromFile: too many channels specified')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebugF('maximum number = ', maxNumChannels)
               call logDebug('increase maxNumChannels')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because too many channels are specified')
               if (errorCheck(errS)) return
             end if

           case('end_channel_irr')

             old_version = .false.
             nchannel_irr = ichannel_irr
             ichannel_irr = ichannel_irr + 1

             if ( ichannel_irr > maxNumChannels + 1) then
               call logDebug('ERROR subroutine readIrrRadFromFile: too many channels specified')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebugF('maximum number = ', maxNumChannels)
               call logDebug('increase maxNumChannels')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because too many channels are specified')
               if (errorCheck(errS)) return
             end if

           case('end_channel_rad')

             old_version = .false.
             nchannel_rad = ichannel_rad
             ichannel_rad = ichannel_rad + 1

             if ( ichannel_rad > maxNumChannels + 1) then
               call logDebug('ERROR subroutine readIrrRadFromFile: too many channels specified')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebugF('maximum number = ', maxNumChannels)
               call logDebug('increase maxNumChannels')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because too many channels are specified')
               if (errorCheck(errS)) return
             end if

           case('data')

             old_version = .true.

             if ( nitems /= 4 ) then
               call logDebug('ERROR in external irradiance - radiance file: incorect number of items on a line')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               write(errS%temp,*) 'for channel and wavelength numbers', ichannel, iwave
               call errorAddLine(errS, errS%temp)
               call logDebug('detected by subroutine readIrrRadFromFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because of error in external reflectance file')
               if (errorCheck(errS)) return
             end if

             call readf(errS, value)
             if (errorCheck(errS)) return
             wavelength(iwave, ichannel)  = value

             call readf(errS, value)
             if (errorCheck(errS)) return
             SNR(iwave, ichannel)         = value

             call readf(errS, value)
             if (errorCheck(errS)) return
             reflectance(iwave, ichannel) = value

             nwavel(ichannel) = iwave
             iwave            = iwave + 1

             if ( iwave > maxNumWavelengths ) then
               call logDebug('ERROR in reading reflectance data from file')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebug('too many wavelengths specified')
               call logDebugF('maximum number = ', maxNumWavelengths)
               call logDebug('increase maxNumWavelengths')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because too many wavelengths are specified')
               if (errorCheck(errS)) return
             end if

           case('irr')

             old_version = .false.

             if ( nitems /= 4 ) then
               call logDebug('ERROR in external irradiance - radiance file: incorect number of items on a line')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               write(errS%temp,*) 'for channel and wavelength numbers', ichannel_irr, iwave_irr
               call errorAddLine(errS, errS%temp)
               call logDebug('detected by subroutine readIrrRadFromFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because of error in external irradiance - radiance file')
               if (errorCheck(errS)) return
             end if

             call readf(errS, value)
             if (errorCheck(errS)) return
             wavelength_irr(iwave_irr, ichannel_irr)  = value

             call readf(errS, value)
             if (errorCheck(errS)) return
             SNR_irr(iwave_irr, ichannel_irr)         = value

             call readf(errS, value)
             if (errorCheck(errS)) return
             irradiance(iwave_irr, ichannel_irr)      = value

             nwavel_irr(ichannel_irr) = iwave_irr
             iwave_irr                = iwave_irr + 1

             if ( iwave_irr > maxNumWavelengths ) then
               call logDebug('ERROR in reading radiance - irradiance data from file')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebug('too many wavelengths specified')
               call logDebugF('maximum number = ', maxNumWavelengths)
               call logDebug('increase maxNumWavelengths')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because too many wavelengths are specified')
               if (errorCheck(errS)) return
             end if

           case('rad')

             old_version = .false.

             if ( nitems /= 4 ) then
               call logDebug('ERROR in external irradiance - radiance file: incorect number of items on a line')
               call logDebug('when reading radiance data')
               write(errS%temp,*) 'for channel and wavelength numbers', nchannel_rad, iwave_rad
               call errorAddLine(errS, errS%temp)
               call logDebug('detected by subroutine readIrrRadFromFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because of error in reflectance file')
               if (errorCheck(errS)) return
             end if

             call readf(errS, value)
             if (errorCheck(errS)) return
             wavelength_rad(iwave_rad, ichannel_rad)  = value

             call readf(errS, value)
             if (errorCheck(errS)) return
             SNR_rad(iwave_rad, ichannel_rad)         = value

             call readf(errS, value)
             if (errorCheck(errS)) return
             radiance(iwave_rad, ichannel_rad)        = value

             nwavel_rad(ichannel_rad) = iwave_rad
             iwave_rad                = iwave_rad + 1

             if ( iwave_rad > maxNumWavelengths ) then
               call logDebug('ERROR in reading radiance - irradiance data from file')
               call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
               call logDebug('too many wavelengths specified')
               call logDebugF('maximum number = ', maxNumWavelengths)
               call logDebug('increase maxNumWavelengths')
               call logDebug('in subroutine readIrrRadFromFile')
               call logDebug('in module readModule')
               call mystop(errS, 'stopped because too many wavelengths are specified')
               if (errorCheck(errS)) return
             end if
              
           case('end_file')

             exit ! exit do loop

           case default

             call logDebug("ERROR in file with irradiance - radiance data")
             call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
             call logDebug("identifier is not recognized: "//trim(identifier))
             call logDebug("possible identifiers are:")
             call logDebug('start_fit_window  (old version only)')
             call logDebug('data              (old version only)')
             call logDebug('end_fit_window    (old version only)')
             call logDebug('start_channel_irr' )
             call logDebug('irr')
             call logDebug('end_channel_irr')
             call logDebug('start_fit_window_rad')
             call logDebug('rad')
             call logDebug('end_channel_rad')
             call logDebug('end_file')
             call mystop(errS, 'stopped because identifier is not recognized')
             if (errorCheck(errS)) return

        end select

      end do

      close(UNIT = readIrrRadUnit)

      if ( old_version ) then

        ! code for the old version of the external data file
        ! to make the code backward compatable

        ! Select the channel for each fit window for the Earth radiance

        do iband = 1, numSpectrBands
          numChannelsForFitWindow = 0
          do ichannel = 1, nchannel
            if ( ( wavelInstrIrrSimS(iband)%startWavel >= wavelength(                1, ichannel) - 0.1d0) .and. &
                 ( wavelInstrIrrSimS(iband)%endWavel   <= wavelength( nwavel(ichannel), ichannel) + 0.1d0 ) ) then
              channelNumber(iband) = ichannel
              numChannelsForFitWindow = numChannelsForFitWindow + 1
            end if
          end do ! ichannel
          if ( numChannelsForFitWindow > 1 ) then
            call logDebug("ERROR in configuration file")
            call logDebug("spectral band / fit window in overlap region of spectral channels")
            call logDebug("can not select which channel to use")
            call logDebug("change fit window in configuration file")
            call logDebug("so that spectral channel can be selected")
            call logDebugI("error ocurred for fit window = ", iband)
            call mystop(errS, 'stopped because spectral channel can not be selected')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          if ( numChannelsForFitWindow < 1 ) then
            call logDebug("ERROR in configuration file")
            call logDebug("fit window lies (partly) outside the wavelength range covered by the channels")
            call logDebug("either make the fit window smaller or split it into two or more fit windows")
            call logDebugI("error ocurred for fit window = ", iband)
            call mystop(errS, 'stopped because fit window lies (partly) outside the channels')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          
        end do ! iband

        ! determine the number of wavelengths in each fit window, taking into account excluded parts

        do iband = 1, numSpectrBands
          counter = 0
          do iwave = 1, nwavel(channelNumber(iband))
            wavel = wavelength(iwave, channelNumber(iband))
            addWavel = .false.
            if ( (wavel >= wavelInstrIrrSimS(iband)%startWavel - 0.1d0) .and. &
                 (wavel <= wavelInstrIrrSimS(iband)%endWavel   + 0.1d0) ) addWavel = .true.
            do ipair = 1, wavelInstrIrrSimS(iband)%nExclude
              if ( (wavel >= wavelInstrIrrSimS(iband)%excludeStart(ipair)) .and. &
                   (wavel <= wavelInstrIrrSimS(iband)%excludeEnd(ipair)) )       &
              addWavel = .false.
            end do ! ipair
            if ( addWavel ) counter = counter + 1
          end do ! iwave
          wavelInstrIrrSimS(iband)%nwavel = counter
          wavelInstrRadSimS(iband)%nwavel = counter
          call claimMemWavelInstrS(errS, wavelInstrIrrSimS (iband))   ! claim memory for irradiance
          call claimMemWavelInstrS(errS, wavelInstrRadSimS(iband))    ! claim memory for radiance
        end do ! iband

        ! fill wavelengths and indexArray

        do iband = 1, numSpectrBands
          counter = 0
          do iwave = 1, nwavel(channelNumber(iband))
            addWavel = .false.
            wavel = wavelength(iwave, channelNumber(iband))
            if ( (wavel >= wavelInstrIrrSimS(iband)%startWavel - 0.1d0) .and. &
                 (wavel <= wavelInstrIrrSimS(iband)%endWavel   + 0.1d0) )   addWavel = .true.
            do ipair = 1, wavelInstrIrrSimS(iband)%nExclude
              if ( (wavel >= wavelInstrIrrSimS(iband)%excludeStart(ipair) ) .and. &
                   (wavel <= wavelInstrIrrSimS(iband)%excludeEnd  (ipair) ) ) addWavel = .false.
            end do ! ipair
            if ( addWavel ) then 
              counter = counter + 1
              wavelInstrIrrSimS(iband)%wavel(counter) = wavel
              wavelInstrRadSimS(iband)%wavel(counter) = wavel
              indexArray(counter,iband) = iwave
            end if
          end do ! iwave
          if ( counter < 2 ) then
            call logDebug('ERROR when reading reflectance data from file')
            call logDebug('values for at least 2 wavelengths per spectral band are expected')
            call logDebug('but results for less wavelengths were read')
            call logDebugI('for band number = ', iband)
            call logDebug('error produced by subroutine readReflFromFile')
            call logDebug('in module readModule')
            call mystop(errS, 'stopped because not enough data was read')
            if (errorCheck(errS)) return
          end if
        end do ! iband

        ! Fill values for irradiance and radiance and signal to noise ratio
        ! note that we have the old version of the configuration file here
        ! where the reflectance is listed and not the irradiance and radiance separately.
        ! Therefore we set the solar irradiance to 1.0 and fill the radiance with the
        ! reflectance.

        do iband = 1, numSpectrBands
          do iwave = 1, wavelInstrRadSimS(iband)%nwavel
            solarIrradianceSimS(iband)%solIrrMeas(iwave)    = 1.0d0
            solarIrradianceSimS(iband)%SN(iwave)            = 1.0d8
            solarIrradianceSimS(iband)%solIrrError(iwave)   = 1.0d-8
            earthRadianceSimS(iband)%radiance_Meas(1,iwave) = reflectance(indexArray(iwave,iband), channelNumber(iband))
            earthRadianceSimS(iband)%SN(iwave)              = SNR(indexArray(iwave,iband), channelNumber(iband))
            earthRadianceSimS(iband)%SNrefspec(iwave)       = 0.0d0  ! value is not known
            earthRadianceSimS(iband)%radianceError(iwave)   = earthRadianceSimS(iband)%radiance_Meas(1,iwave) &
                                                            / earthRadianceSimS(iband)%SN(iwave)   
          end do ! iwave            
        end do ! iband


      else

        ! new version of the sim file
        ! Select the channel for each fit window for the Earth radiance based on the
        ! specifications in the configuration file
        ! Note that we will eventually fit the sun-normalized radiance and not the irradiance

        do iband = 1, numSpectrBands
          numChannelsForFitWindow_rad = 0
          do ichannel_rad = 1, nchannel_rad
            if (( wavelInstrIrrSimS(iband)%startWavel >= wavelength_rad(                        1, ichannel_rad) - 0.1d0) .and. &
                ( wavelInstrIrrSimS(iband)%endWavel   <= wavelength_rad( nwavel_rad(ichannel_rad), ichannel_rad) + 0.1d0) ) then
              channelNumber_rad(iband) = ichannel_rad
              numChannelsForFitWindow_rad = numChannelsForFitWindow_rad + 1
            end if
          end do ! ichannel_rad
          if ( numChannelsForFitWindow_rad > 1 ) then
            call logDebug("ERROR in configuration file")
            call logDebug("spectral band / fit window in overlap region of spectral channels")
            call logDebug("can not select which channel to use")
            call logDebug("change fit window in configuration file")
            call logDebug("so that spectral channel can be selected")
            call logDebugI("error ocurred for fit window = ", iband)
            call logDebug("error detected by subroutine readIrrRadFromFile")
            call logDebug("in module readModule")
            call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
            call mystop(errS, 'stopped because spectral channel can not be selected')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          if ( numChannelsForFitWindow_rad < 1 ) then
            call logDebug("ERROR in configuration file: fit window lies (partly)")
            call logDebug("outside the wavelength range covered by the channels")
            call logDebug("either make the fit window smaller or ")
            call logDebug("split it into two or more fit windows located in different channels")
            call logDebugI("error ocurred for fit window = ", iband)
            call logDebug("error detected by subroutine readIrrRadFromFile")
            call logDebug("in module readModule")
            call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
            call mystop(errS, 'stopped because fit window lies (partly) outside the channels')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          
        end do ! iband

        ! Select the channel for each fit window for the irradiance based on the
        ! specifications in the configuration file

        do iband = 1, numSpectrBands
          numChannelsForFitWindow_irr = 0
          do ichannel_irr = 1, nchannel_irr
            if (( wavelInstrIrrSimS(iband)%startWavel >= wavelength_irr(                        1, ichannel_irr) - 0.1d0) .and. &
                ( wavelInstrIrrSimS(iband)%endWavel   <= wavelength_irr( nwavel_irr(ichannel_irr), ichannel_irr) + 0.1d0) ) then
              channelNumber_irr(iband) = ichannel_irr
              numChannelsForFitWindow_irr = numChannelsForFitWindow_irr + 1
            end if
          end do ! ichannel_rad
          if ( numChannelsForFitWindow_irr > 1 ) then
            call logDebug("ERROR in configuration file")
            call logDebug("spectral band / fit window in overlap region of spectral channels")
            call logDebug("can not select which channel to use")
            call logDebug("change fit window in configuration file")
            call logDebug("so that spectral channel can be selected")
            call logDebugI("error ocurred for fit window = ", iband)
            call logDebug("error detected by subroutine readIrrRadFromFile")
            call logDebug("in module readModule")
            call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
            call mystop(errS, 'stopped because spectral channel can not be selected')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          if ( numChannelsForFitWindow_irr < 1 ) then
            call logDebug("ERROR in configuration file: fit window lies (partly)")
            call logDebug("outside the wavelength range covered by the channels")
            call logDebug("either make the fit window smaller or ")
            call logDebug("split it into two or more fit windows located in different channels")
            call logDebugI("error ocurred for fit window = ", iband)
            call logDebug("error detected by subroutine readIrrRadFromFile")
            call logDebug("in module readModule")
            call logDebug('file name: '//trim(controlSimS%ASCIIinputIrrRadFileName))
            call mystop(errS, 'stopped because fit window lies (partly) outside the channels')
            if (errorCheck(errS)) return
          end if ! numChannelsForFitWindow > 1
          
        end do ! iband

        ! determine the number of wavelengths in each fit window, taking into account excluded parts

        do iband = 1, numSpectrBands
          counter = 0
          do iwave = 1, nwavel_irr(channelNumber_irr(iband))
            wavel = wavelength_irr(iwave, channelNumber_irr(iband))
            addWavel = .false.
            if ( (wavel >= wavelInstrIrrSimS(iband)%startWavel - 0.1d0) .and. &
                 (wavel <= wavelInstrIrrSimS(iband)%endWavel   + 0.1d0) ) addWavel = .true.
            do ipair = 1, wavelInstrIrrSimS(iband)%nExclude
              if ( (wavel >= wavelInstrIrrSimS(iband)%excludeStart(ipair)) .and. &
                   (wavel <= wavelInstrIrrSimS(iband)%excludeEnd(ipair)) )       &
              addWavel = .false.
            end do ! ipair
            if ( addWavel ) then
              counter = counter + 1
              if ( counter <= wavelInstrIrrSimS(iband)%nwavel ) then
                wavelInstrIrrSimS(iband)%wavel(counter) = wavelength_irr(iwave, channelNumber_irr(iband))
                indexArray_irr(counter,iband) = iwave
              end if
            end if
          end do ! iwave
          if ( wavelInstrIrrSimS (iband)%nwavel /= counter ) then
            call logDebug('WARNING: then number of wavelengths read does not')
            call logDebug('correspond to the number specified in the configuration file')
            call logDebug('for the solar irradiance')
            write(errS%temp,'(A,I2,I4)') 'in config file for band ', iband, wavelInstrIrrSimS (iband)%nwavel
            call errorAddLine(errS, errS%temp)
            write(errS%temp,'(A,I2,I4)') 'read from file for band ', iband, counter
            call errorAddLine(errS, errS%temp)
            call logDebug('you might have to change the wavelength step')
            call logDebug('in the configuration file')
            if ( wavelInstrIrrSimS (iband)%nwavel > counter ) then
              call logDebug('will stop because not enough wavelengths have been read')
              call mystop(errS, 'stopped in subroutine readIrrRadFromFile')
              if (errorCheck(errS)) return
            else
              call logDebug('continuing with less wavelengths')
            end if
          end if
        end do ! iband

        ! fill wavelengths for simulation and retrieval based on the radiance grid

        do iband = 1, numSpectrBands
          counter = 0
          do iwave = 1, nwavel_rad(channelNumber_rad(iband))
            addWavel = .false.
            wavel = wavelength_rad(iwave, channelNumber_rad(iband))
            if ( (wavel >= wavelInstrRadSimS(iband)%startWavel - 0.1d0) .and. &
                 (wavel <= wavelInstrRadSimS(iband)%endWavel   + 0.1d0) ) addWavel = .true.
            do ipair = 1, wavelInstrRadSimS(iband)%nExclude
              if ( (wavel >= wavelInstrRadSimS(iband)%excludeStart(ipair)) .and. &
                   (wavel <= wavelInstrRadSimS(iband)%excludeEnd  (ipair)) )   addWavel = .false.
            end do ! ipair
            if ( addWavel ) then 
              counter = counter + 1
              if ( counter <= wavelInstrRadSimS(iband)%nwavel ) then
                wavelInstrRadSimS(iband)%wavel(counter) = wavelength_rad(iwave, channelNumber_rad(iband))
                indexArray_rad(counter,iband) = iwave
              end if
            end if ! addWavel
          end do ! iwave
          if ( counter < 3 ) then
            call logDebug('ERROR when reading irradiance and radiance data from file')
            call logDebug('values for at least 3 wavelengths per spectral band are expected')
            call logDebug('but results for less wavelengths were read')
            call logDebugI('for band number = ', iband)
            call logDebug('error produced by subroutine readIrrRadFromFile')
            call logDebug('in module readModule')
            call mystop(errS, 'stopped because not enough data was read')
            if (errorCheck(errS)) return
          end if
          if ( wavelInstrRadSimS (iband)%nwavel /= counter ) then
            call logDebug('WARNING: then number of wavelengths read does not')
            call logDebug('correspond to the number specified in the configuration file')
            call logDebug('for the earth radiance')
            write(errS%temp,'(A,I2,I4)') 'in config file for band ', iband, wavelInstrRadSimS (iband)%nwavel
            call errorAddLine(errS, errS%temp)
            write(errS%temp,'(A,I2,I4)') 'read from file for band ', iband, counter
            call errorAddLine(errS, errS%temp)
            call logDebug('you might have to change the wavelength step')
            call logDebug('in the configuration file')
            if ( wavelInstrRadSimS (iband)%nwavel > counter ) then
              call logDebug('will stop because not enough wavelengths have been read')
              call mystop(errS, 'stopped in subroutine readIrrRadFromFile')
              if (errorCheck(errS)) return
            else
              call logDebug('continuing with less wavelengths')
            end if
          end if

        end do ! iband

        ! fill solar irradiance and earth radiance values

        do iband = 1, numSpectrBands
          do iwave = 1, wavelInstrRadSimS(iband)%nwavel
            solarIrradianceSimS(iband)%solIrrMeas(iwave)    = irradiance(indexArray_irr(iwave,iband), channelNumber_irr(iband))
            solarIrradianceSimS(iband)%SN(iwave)            = SNR_irr(indexArray_irr(iwave,iband), channelNumber_irr(iband))
            solarIrradianceSimS(iband)%solIrrError(iwave)   = solarIrradianceSimS(iband)%solIrrMeas(iwave) &
                                                            /  solarIrradianceSimS(iband)%SN(iwave)
            earthRadianceSimS(iband)%radiance_Meas(1,iwave) = radiance(indexArray_rad(iwave,iband), channelNumber_rad(iband))
            earthRadianceSimS(iband)%SN(iwave)              = SNR_rad(indexArray_rad(iwave,iband), channelNumber_rad(iband))
            earthRadianceSimS(iband)%SNrefspec(iwave)       = 0.0d0  ! value is not known
            earthRadianceSimS(iband)%radianceError(iwave)   = earthRadianceSimS(iband)%radiance_Meas(1,iwave) &
                                                            / earthRadianceSimS(iband)%SN(iwave)   
          end do ! iwave            
        end do ! iband

      end if ! old_version

      if ( verbose ) then
        write(intermediateFileUnit,*)
        do iband = 1, numSpectrBands
          write(intermediateFileUnit,'(A)') &
          ' wavelength and solar irrandiance read and wavelength and solar irradiance on radiance grid'
          do iwave = 1, wavelInstrRadSimS(iband)%nwavel
            write(intermediateFileUnit,'(F12.6, ES20.10, F12.6, ES20.10 )') wavelInstrIrrSimS(iband)%wavel(iwave),         &
                                                                            solarIrradianceSimS(iband)%solIrrMeas(iwave),  &
                                                                            wavelInstrRadSimS(iband)%wavel(iwave),         &
                                                                            earthRadianceSimS(iband)%radiance_Meas(1,iwave)
          end do ! iwave
        end do ! iband
      end if ! verbose

    end subroutine readIrrRadFromFile


    subroutine setupHRWavelengthGridIrr(errS, wavelInstrS, wavelHRS,  FWHM)

      ! We assume that we can use spline interpolation on the solar spectrum
      ! to get the solar irradiance at the high resolution (HR) wavelength grids
      ! for simulation and retrieval, i.e. the high resolution solar spectrum
      ! is not undersampled.

      ! This subroutine allocates and fills the array wavel in the structures wavelHRS

       implicit none

       type(wavelInstrType), intent(in)    :: wavelInstrS
       type(wavelHRType),    intent(inout) :: wavelHRS
       real(8),              intent(in)    :: FWHM

       real(8)    :: waveStart, waveEnd
       type boundariesType
         integer          :: numBoundaries
         real(8), pointer :: boundaries(:)  => null()
       end type boundariesType

       real(8), allocatable :: intervalBoundaries(:)
       real(8)              :: maxInterval
       !  x0(i,j) are the division points (x0(i), i = 1:j) and w0 the corresponding weights
       real(8), allocatable :: x0(:,:)
       real(8), allocatable :: w0(:,:)
       real(8)              :: dw, sw, wavel, newWavel

       logical              :: addWavel
       integer              :: nwavel
       real(8), allocatable :: wavelBand(:)        ! (nwavel) full band - not accounting for excluded parts
       real(8), allocatable :: wavelBandWeight(:)  ! (nwavel) weights for full band - not accounting for excluded parts

       integer              :: iwave, nlines, iinterval, index, ipair
       integer              :: iGauss, nGauss, nGaussMax, nGaussMin

       integer              :: allocStatus
       integer              :: deallocStatus

       logical, parameter   :: verbose = .false.

       ! initialize
       waveStart = wavelInstrS%startWavel - 2.0d0 * FWHM
       waveEnd   = wavelInstrS%endWavel   + 2.0d0 * FWHM

       ! starting at waveStart we add a boundary that lies one FWHM further
       nlines = 0
       wavel = waveStart
       do 
         nlines = nlines + 1
         newWavel = wavel + FWHM
         wavel = newWavel
         if ( wavel > waveEnd ) exit
       end do

       allocStatus = 0
       allocate ( intervalBoundaries(0:nlines), STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('FATAL ERROR: allocation failed')
         call logDebug('for intervalBoundaries')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       ! fill values for the array intervalBoundaries
       nlines = 0
       wavel = waveStart
       intervalBoundaries(0) = wavel
       do 
         nlines = nlines + 1
         newWavel = wavel + FWHM
         wavel = newWavel
         intervalBoundaries(nlines) = wavel
         if ( wavel > waveEnd ) exit
       end do

       ! fill Gausspoints and weights arrays
       nGaussMax = wavelHRS%nGaussFWHM
       nGaussMin = wavelHRS%nGaussFWHM

       ! allocate arrays for gausspoints and weight on (0,1)
       ! they are deallocated just before leaving this subroutine

       allocStatus = 0
       allocate ( x0(nGaussMax,nGaussMax), w0(nGaussMax,nGaussMax), STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('FATAL ERROR: allocation failed')
         call logDebug('for x0 or w0')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       do iGauss = nGaussMin, nGaussMax
         call GaussDivPoints(errS, 0.0d0, 1.0d0, x0(:,iGauss), w0(:,iGauss), iGauss)
         if (errorCheck(errS)) return
       end do

       ! determine the number of wavelengths for the high resolution simulation grid
       maxInterval = 0.0d0
       do iinterval = 2, size(intervalBoundaries) - 2
         dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)
         if ( dw >  maxInterval )  maxInterval = dw    
       end do

       index = 0
       do iinterval = 1, size(intervalBoundaries) - 1
         dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)      ! width of interval
         sw = intervalBoundaries(iinterval-1)                                      ! start wavelength for interval
         nGauss = max( nGaussMin, nint( nGaussMax * dw / maxInterval ) )
         if (nGauss >  nGaussMax ) nGauss =  nGaussMax
         do iGauss = 1, nGauss
           index = index + 1
         end do
       end do

       nwavel = index 

       allocStatus = 0
       allocate ( wavelBand(nwavel), wavelBandWeight(nwavel), STAT = allocStatus )
       if ( allocStatus /= 0 ) then
         call logDebug('FATAL ERROR: allocation failed')
         call logDebug('for wavelBand or wavelBandWeight')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because allocation failed')
         if (errorCheck(errS)) return
       end if

       ! fill wavelength grid for the entire spectral band
       index = 1
       do iinterval = 1, size(intervalBoundaries) - 1
         dw = intervalBoundaries(iinterval) - intervalBoundaries(iinterval-1)
         sw = intervalBoundaries(iinterval-1)
         nGauss = max(  nGaussMin, nint( nGaussMax * dw / maxInterval ) )
         if (nGauss >  nGaussMax ) nGauss = nGaussMax
         do iGauss = 1, nGauss
           wavelBand(index)       = sw + dw * x0(iGauss, nGauss)
           wavelBandWeight(index) = dw * w0(iGauss, nGauss)
           index = index + 1
         end do
       end do

       ! deal with excluded intervals

       ! copy excluded intervals and reduce them with 4 * FWHM (2 left and 2 right)
       wavelHRS%nExclude        = wavelInstrS%nExclude
       wavelHRS%excludeStart(:) = wavelInstrS%excludeStart(:) + 2 * FWHM
       wavelHRS%excludeEnd(:)   = wavelInstrS%excludeEnd(:)   - 2 * FWHM

       ! determine the lengths of the wavelength array taking into account excluded wavelengths
       if ( wavelHRS%nExclude > 0 ) then
         index = 0
         do iwave = 1, nwavel
           wavel = wavelBand(iwave)
           addWavel = .true.
           do ipair = 1, wavelHRS%nExclude
             if ( (wavel >= wavelHRS%excludeStart(ipair)) .and. (wavel <= wavelHRS%excludeEnd(ipair)) ) &
               addWavel = .false.
           end do ! ipair
           if ( addWavel ) index = index + 1
         end do ! iwave

         wavelHRS%nwavel = index

       else

         wavelHRS%nwavel = nwavel

       end if

       ! claim memory space
       call claimMemWavelHRS(errS, wavelHRS)          ! claim memory for high resolution wavelength grid

       ! fill wavelengths

       if ( wavelHRS%nExclude > 0 ) then

         index = 0
         do iwave = 1, nwavel
           wavel = wavelBand(iwave)
           addWavel = .true.
           do ipair = 1, wavelInstrS%nExclude
             if ( (wavel >= wavelHRS%excludeStart(ipair)) .and. (wavel <= wavelHRS%excludeEnd(ipair)) ) &
               addWavel = .false.
           end do ! ipair
           if ( addWavel ) then
             index = index + 1
             wavelHRS%wavel(index)  = wavelBand(iwave)
             wavelHRS%weight(index) = wavelBandWeight(iwave)
           end if
         end do ! iwave

       else

         do iwave = 1, nwavel
           wavelHRS%wavel(iwave)  = wavelBand(iwave)
           wavelHRS%weight(iwave) = wavelBandWeight(iwave)
         end do

       end if

       if ( verbose ) then

         write(intermediateFileUnit, '(A, 2F10.4)') 'wavelength range = ', waveStart, waveEnd

         write(intermediateFileUnit, *) 
         write( intermediateFileUnit, *) ' interval boundaries'
         do iinterval = 0, size(intervalBoundaries) -1
           write(intermediateFileUnit, '(F15.6)') intervalBoundaries(iinterval)
         end do
         
         write(intermediateFileUnit, *) 
         write(intermediateFileUnit,'(A)') 'wavelengths for the high resolution grid'
         do iwave = 1, wavelHRS%nwavel
           write(intermediateFileUnit,'(F12.5)') wavelHRS%wavel(iwave)
         end do

       end if ! verbose

       ! clean up
       deallocStatus  = 0
       deallocate( intervalBoundaries, x0, w0, wavelBand, wavelBandWeight, STAT = deallocStatus )
       if ( deallocStatus /= 0 ) then
         call logDebug('FATAL ERROR: deallocation failed')
         call logDebug('for intervalBoundaries, x0, w0, wavelBand, or wavelBandWeight')
         call logDebug('in subroutine setupHRWavelengthGrid')
         call logDebug('in program DISAMAR - file main_DISAMAR.f90')
         call mystop(errS, 'stopped because deallocation failed')
         if (errorCheck(errS)) return
       end if

    end subroutine setupHRWavelengthGridIrr

  end module calibrateIrradianceSlitFunction


