Temperature:
Columns: Umkehr Layer means (layers 0 to 10); Rows: variation with latitude
(10� lat bands, 85S to 85N); blocks: variation with month


Ozone:
Columns: total ozone, Umkehr Layer column O3 (layers 0 to 10) in DU;
Rows: variation with total ozone; blocks: variation with lat and month


Note:
1) Pressure at the bottom of Umkehr layer L: 2^(-L)*1013.25 hPa,
   L=0,1,2...10, Layer 10 extends to 0 pressure.
2) There are no O3 profiles for some total ozone amounts (given as 999).
3) Charlie Wellemeyer & P. K. Bhartia produced these climatologies.


Temperature climatology is a traditional 3-dmensional climatology
(press-lat-month) produced from NCEP data.  Ozone climatology is a
novel 4-D climatology (press-total ozone-lat-month) produced by
combining the traditional 3-D O3 climatology (press-lat-month)
produced by McPeters and Labow with another 3-D (press-total ozone-lat)
climatology that we have used in the previous TOMS versions.


In creating the 4-D climatology, Charlie and I tried to capture the
following behavior of atmospheric ozone that is well supported by data.
Mean ozone density between 30-300 hPa is highly correlated with lat and
total ozone but has a relatively weak seasonal variation. By contrast,
the mean ozone density above and below these altitudes varies primarily
with season and latitude, with no discernable correlation with total ozone.
(This is by no means obvious since typically more than 50% of the total
ozone is outside the 30-300 hPa altitude range, and the ozone density
peaks around 25 hPa.)  This also explains why one cannot create realistic
ozone profiles by simply scaling the Labow/McPeters climatology (or other
similar climatologies) with total ozone, as some algorithms do implicitly
or explicitly - one gets absurd ozone densities outside 30-300 hPa when
the total ozone amounts are either too small or too large compared to the mean.


There seems to be lot of confusion on this issue. Many people are calling
the 3-D Labow/McPeters climatology as the TOMS V8 climatology. This is not correct.


4) Comparison of TOMS V8 O3 climatology with SAGE and ozonesonde profiles
indicate that the rms differences (in Umkehr layers) between actual profiles
and climatology are typically lees than 25% below 30 hPa and 15% above, and
less than 10% for the mean density between 30-300 hPa.


5) In the SBUV V8 profile algorithm we use the 3-D O3 climatology produced
by Labow & McPeters, i.e., SBUV V8 a priori profiles do not vary with total
ozone as they did in the previous versions. Indeed, in SBUV V8 there is no
total ozone constraint at all, as it did in the previous versions. The SBUV
radiances have very good (direct) information to capture the variability
of the mean ozone density between 30-300 hPa (contrary to what some people
mistakenly believe). The mean density can be retrieved with rms error of
less than 5%, possibly less than 2%, though there is no way to validate it.
However, there is little or no vertical information.


PK
