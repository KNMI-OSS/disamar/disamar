program create_slit_function_file

! This program reads slit function data for the GOME 2 instrument
! and translates it to a file that can be read by DISAMAR

  implicit none

  integer(4), parameter :: nheader = 185                ! number of header lines
  integer(4), parameter :: nslitfunction_values = 142   ! number of slit function values

  integer(4)  :: pixel_number, band_number, order, i, iwavelSF, status
  integer(4)  :: band_number_prev
  integer(4)  :: errorInOpeningFile
  real(8)     :: RAL_nomWavelength      ! (in nm) nominal wavelength given by RAL
  real(8)     :: offset                 ! (in nm) wavelength offset given by RAL (first point)
  real(8)     :: step                   ! (in nm) step size given by RAL
  real(8)     :: nominalWavelength      ! (in nm) called center of mass by RAL
  real(8)     :: nominalWavelengthPrev  ! (in nm) previous nominal wavelength
  real(8)     :: center_Gaussian_fit    ! (in nm) center wavelength for Gaussian slit function (fitted)
  real(8)     :: FWHM                   ! (in nm) FWHM for Gaussian slit function (fitted)
  real(8)     :: deltaWavelength        ! (in nm) distance from nominalWavelength
  real(8)     :: wavelengthGridOrig         (nslitfunction_values)  ! (in nm) original wavelength for slit function points
  real(8)     :: wavelengthGridOrigReordered(nslitfunction_values)  ! (in nm) reordered wavelengths 
  real(8)     :: wavelengthGridShifted      (nslitfunction_values)  ! (in nm) shifted wavelength
  real(8)     :: slitfunctionOrig           (nslitfunction_values)  ! (in nm-1) original values for the slit function
  real(8)     :: slitfunctionOrigReordered  (nslitfunction_values)  ! (in nm-1) reoordered values for the slit function
  real(8)     :: slitfunctionShifted        (nslitfunction_values)  ! (in nm-1) shifted values for the slit function

  real(8)     :: offset_grid
  character(LEN = 50)     :: inputFilename, outputFilename
  integer(4)  :: minlocation(1)

  inputFilename  = 'SFS_SLIT_MAIN.203.edited_not_interpolated'
  outputFilename = 'GOME-2_slit-function_file.txt'

  open(UNIT=2, ACTION= 'READ', STATUS = 'OLD', IOSTAT= errorInOpeningFile, FILE = inputFilename )

  if (errorInOpeningFile /= 0) then
     write (*,*) 'create_slit_function_file: Error in opening input file: ', inputFilename
     stop 'stopped: failed to open input file'
  end if

  open(UNIT=3, ACTION= 'WRITE', STATUS = 'REPLACE', FILE = outputFilename )

  ! read header lines
  do i = 1, nheader
    read(2,*)
  end do

! initialize
  nominalWavelengthPrev = 0.0d0
  band_number_prev = 0

  ! read data
  do
    read (2,'(I3,I4,I4,6F12.7,142E12.5)' , end = 40) band_number, pixel_number, order, RAL_nomWavelength,        &
                                                     offset, step, nominalWavelength, center_Gaussian_fit, FWHM, &
                                                     (slitfunctionOrig(iwavelSF), iwavelSF = 1, nslitfunction_values)
    ! write(3,'(I3,I4,I4,6F12.7,142ES12.5)')         band_number, pixel_number, order, RAL_nomWavelength,        &
    !                                                offset, step, nominalWavelength, center_Gaussian_fit, FWHM, &
    !                                                (slitfunctionOrig(iwavelSF), iwavelSF = 1, nslitfunction_values)

    ! calculate the original wavelengths where the slit function is given
    do iwavelSF = 1, nslitfunction_values
      wavelengthGridOrig(iwavelSF) = RAL_nomWavelength - offset - (iwavelSF - 1) * step
    end do

    ! reorder the wavelengths and the slit function values
    do iwavelSF = 1, nslitfunction_values
      wavelengthGridOrigReordered(nslitfunction_values - iwavelSF + 1) = wavelengthGridOrig(iwavelSF)
      slitfunctionOrigReordered  (nslitfunction_values - iwavelSF + 1) = slitfunctionOrig  (iwavelSF)
    end do

    ! calculate the shifted wavelength grid
    minlocation = minloc( abs(wavelengthGridOrigReordered - nominalWavelength) )
    offset_grid = wavelengthGridOrigReordered( minlocation(1) ) - nominalWavelength
    do iwavelSF = 1, nslitfunction_values
      wavelengthGridShifted(iwavelSF) = wavelengthGridOrigReordered(iwavelSF) - offset_grid
    end do

    ! linear interpolation to get the slit function values on the shifted wavelength grid
    do iwavelSF = 1, nslitfunction_values
      slitfunctionShifted(iwavelSF) =  splintLin(wavelengthGridOrigReordered, slitfunctionOrigReordered, &
	                                             wavelengthGridShifted(iwavelSF), status)
    end do

    if ( band_number_prev == band_number ) then
      ! use only measurements that differ more than 1 nm
      if ( abs( nominalWavelength - nominalWavelengthPrev ) > 1.0d0 ) then

        write(3,'(F12.7, 2I4, A)') nominalWavelength, band_number, pixel_number,  &
                                '  nominal wavelength   band number  pixel number '
        write(3,'(A)') '#  wavelength   delta wavelength  slit function'
        do iwavelSF = 2, nslitfunction_values ! do not use first entry (seems in error; too large)
          deltaWavelength = wavelengthGridShifted(iwavelSF) - nominalWavelength 
          if ( (band_number < 3) .and. (deltaWavelength < 0.65d0) .and. ( deltaWavelength > - 0.65d0 ) ) &
            write(3,'(2F14.7,ES18.5)')  wavelengthGridShifted(iwavelSF), deltaWavelength, slitfunctionShifted(iwavelSF)
          if ( (band_number > 2) .and. (deltaWavelength < 1.30d0) .and. ( deltaWavelength > - 1.30d0 ) ) &
            write(3,'(2F14.7,ES18.5)')  wavelengthGridShifted(iwavelSF), deltaWavelength, slitfunctionShifted(iwavelSF)
        end do
        write(3,*)
      end if
    else
      write(3,'(F12.7, 2I4, A)') nominalWavelength, band_number, pixel_number,  &
                              '  nominal wavelength   band number  pixel number'
      write(3,'(A)') '#  wavelength   delta wavelength  slit function'
      do iwavelSF = 2, nslitfunction_values ! do not use first entry (seems in error; too large)
        deltaWavelength = wavelengthGridShifted(iwavelSF) - nominalWavelength
        if ( (band_number < 3) .and. (deltaWavelength < 0.65d0) .and. ( deltaWavelength > - 0.65d0 ) ) &
          write(3,'(2F14.7,ES18.5)')  wavelengthGridShifted(iwavelSF), deltaWavelength, slitfunctionShifted(iwavelSF)
        if ( (band_number > 2) .and. (deltaWavelength < 1.30d0) .and. ( deltaWavelength > - 1.30d0 ) ) &
          write(3,'(2F14.7,ES18.5)')  wavelengthGridShifted(iwavelSF), deltaWavelength, slitfunctionShifted(iwavelSF)
      end do
      write(3,*)
    end if
    nominalWavelengthPrev = nominalWavelength
    band_number_prev      = band_number
  end do
  40 continue

  close(UNIT=2)
  close(UNIT=3)

  CONTAINS

  function splintLin(xa, ya, x, statusSplint)

    ! splintLin returns the linear interpolated value, i.e. y(x)
    ! when y(x) is specified by x values in xa and y values in ya.
    ! It is assumed that the values in xa are increasing, and bisection
    ! is used to locate the nodes close to x.

    implicit none

    REAL(8), DIMENSION(:), INTENT(IN) :: xa,ya
    REAL(8), INTENT(IN) :: x
    INTEGER(4), INTENT(OUT) :: statusSplint
    REAL(8) :: splintLin
    INTEGER(4) :: khi,klo,n
    REAL(8) :: a,b,h

    ! assume correct processing
    statusSplint = 0

    ! if status = -1 the input is incorrect

    n = size(xa)
    
    klo=max(min(locate(xa,x,n),n-1),1)
    khi=klo+1
    h=xa(khi)-xa(klo)
    if (h == 0.0d0) then
      statusSplint = -1
    end if
    a=(xa(khi)-x)/h
    b=(x-xa(klo))/h
    splintLin=a*ya(klo)+b*ya(khi)
  end function splintLin

  
  INTEGER FUNCTION locate(axis, x, dim)
      !
      ! Purpose: Find the node (location) of the value X in an array 
      ! 
      ! Author: Roeland van Oss, 2003
      ! Modifications: Olaf Tuinder, 20050117
      ! Modifications: Johan de Haan, 2015 08 24
      !
      ! Input: axis, dim, x
      ! InOut: -
      ! Output (as a function value): findNode
      ! Dependencies: -
      !
      ! Find the nearest point near X
      ! Check upper boundary condition
      ! modified from findNode to replace old locate
      ! added descending order of x   
      ! P. Wang 13 Jan. 2022
     
      implicit none
      integer, intent(in) :: dim
      real(8), dimension(dim), intent(in) :: axis
      real(8), intent(in) :: x
      integer, dimension(1) :: nearestLocation

      ! deal with two exceptions:
      ! (1) x = axis(1)
      ! (2) x = axis(dim)
      
      if ( x == axis(1) ) then
          locate = 1
          return
      end if
          
      if ( x == axis(dim) ) then
          locate = dim 
          return
      end if
      
      ! find the nearest grid point near x
      nearestLocation = minloc (abs (axis-x)) 

      ! Check upper boundary condition 
      if (axis(1) < axis(dim)) then   !for ascending 
          if (axis(nearestLocation(1)) > x ) then
              locate = nearestLocation(1) - 1
          else
              locate = nearestLocation(1)
          end if
      else
      !  for descending   P. Wang
          if (axis(nearestLocation(1)) < x ) then
              locate = nearestLocation(1) - 1
          else
              locate = nearestLocation(1)
          end if
      end if    
  
      return

    END FUNCTION locate   


end program  create_slit_function_file
