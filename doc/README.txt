DISAMAR Gitlab OSS version https://gitlab.com/KNMI-OSS/.

See LICENSE.txt for license details.

See further documentation in the doc directory.

See CONTRIBUTORS.txt for a list of contributing authors.

DISAMAR version 4.1.5
DISAMAR release date 2022-02-28
DISAMAR version control identifier 5c5a453f1811

