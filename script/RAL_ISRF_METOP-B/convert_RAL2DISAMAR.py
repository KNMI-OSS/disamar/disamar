#!/usr/bin/env python3

import sys
import argparse
import datetime
import numpy as np

def main():
    parser = argparse.ArgumentParser(description='Convert RAL GOME-2 slitfunction into DISAMAR format')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'), 
                        default=sys.stdin, help="Input file (RAL format)")
    parser.add_argument('-o', '--output', type=argparse.FileType('w'), 
                        default=sys.stdout, help="Input file (RAL format)")
    parser.add_argument('-c', '--comment', default="", 
                        help="Comment to add to the output file")
    args = parser.parse_args()
    
    try:
        input_file_name = "Input file was {0}".format(args.input.name)
    except AttributeError:
        input_file_name = "input read from stdin"
    
    header = """# A parser is used to read this file:
# lines stating with "# " are comments and are skipped
# blank lines are also skipped

# GOME-2 slitfunctions

# {0}
# {1}
# Conversion started at {2}

# Based on the analysis by RAL and set up so that DISAMAR can read this file.
# Only measured slit functions are used (not interpolated ones).
# Only if the nominal wavelength differs more than one nm the functions are listed.

# Each block is headed by the nominal wavelength (RAL calls this center of mass), which is followed
# by the spectral band number, the pixel number, and the number of wavelengths used to specify the
# slit function (141 in all cases). Note that one entry (shortest wavelength) has been
# omitted from the RAL data set because it is large and might be in error.

# Linear interpolation has been used so that entries are listed for
#     wavelength = nominal wavelength  + delta wavelength
#     and the slit functions are listed for this grid
# The delta wavelength grid is the same within a spectral band, but
# bands 3 an4 4 have a grid that differs from bands 1 and 2

# Listed values are wavelength, wavelegth - nominal wavelength (called delta wavelength),
# and the value of the slit function. One should use linear interpolation to calculate
#  values of the slit function at wavelengths between the entries listed here

# Because the delta wavelength grid is the same within a spectral band
# interpolation to a nominal wavelength not listed here is straightforward
#   Let Lambda be the nominal wavelength between Lambda1 and Lambda2 where
#   Lambda1 < Lambda2.
#   let a = (Lambda  - Lambda1) / (Lambda2 - Lambda1)
#   let b = (Lambda2 - Lambda ) / (Lambda2 - Lambda1)
# then
#   S(Lambda) = a * S(Lambda2) + b * S(Lambda1) for all delta wavelength values
#
# Here S(Lambda) is the interpolated tabulated slit function at Lambda.
# If Lambda > Lambda2 or Lambda < Lambda1 extrapolation is required
# because we do not interpolate between different spectral bands.
# The band number and pixel number can be used to identify that
# extrapolation is required.

""".format(args.comment, input_file_name, datetime.datetime.utcnow())
    
    args.output.write(header)
    
    hdr = "{0:12.7f} {1:3d} {2:3d}  nominal wavelength   band number  pixel number\n#  wavelength   delta wavelength  slit function\n"
    
    for line in args.input:
        if line.startswith('!') or line.startswith('#'):
            continue
        
        items = line.split()
        if int(items[2]) == -1:
            continue
        
        nom_wvl = float(items[3])
        band_idx = int(items[0])
        pixel_idx = int(items[1])
        dwvl_start, dvwl_step = float(items[4])+nom_wvl, float(items[5])
        
        # need 141 wavelengths
        wvl = np.arange(dwvl_start, dwvl_start+141*dvwl_step, dvwl_step)
        dwvl = wvl - nom_wvl
        isrf = np.asarray([float(v) for v in items[9:-1]])
        
        args.output.write(hdr.format(nom_wvl, band_idx, pixel_idx))
        for l, dl, sf in zip(wvl, dwvl, isrf):
            args.output.write("{0:14.7f} {1:13.7f}       {2:11.5E}\n".format(l, dl, sf))
        args.output.write("\n")
    
    args.output.close()
    args.input.close()

if __name__ == "__main__": 
    main()

