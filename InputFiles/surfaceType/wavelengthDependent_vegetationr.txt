# Wavelength dependent surface albedo for vegetation - source not known.
# Taken from DAK v 2.5.1
# wavelength range 240.0 - 800.0 nm
# use '&' as concatenation sign if a line is longer than 128 characters.

SECTION SURFACE

subsection pressure
surfPressureSim     1013.00   (surface pressure denotes the lower boundary of the atmosphere - in hPa)
surfPressureRetr    1013.00   (surface pressure denotes the lower boundary of the atmosphere - in hPa)
varSurfPressureRetr   25.00   (variance of the surface pressure in hPa**2)

subsection surfaceType (wavelength independent or polynomial in the wavelength for each spectral band)
surfaceTypeSim    wavelDependent     ( choose between wavelIndependent and wavelDependent )
surfaceTypeRetr   wavelDependent     ( choose between wavelIndependent and wavelDependent )

subsection wavelDependentSim ( repeat for successive wavelength bands )
wavelSurfAlbedo        240.0  300.0  400.0  490.0  510.0  530.0  550.0  570.0  590.0  610.0  630.0  650.0 &
                       670.0  690.0  710.0  730.0  750.0  770.0  800.0
surfAlbedo             0.059  0.059  0.059  0.059  0.067  0.095  0.133  0.127  0.107  0.096  0.088  0.084 &
                       0.092  0.137  0.227  0.308  0.379  0.429  0.471
wavelSurfEmission      500.0  ( wavelengths where surface emission is specified )
surfEmission             0.0  ( emission in 10^12 ph/cm2/s/nm/sr; fluorescence )

subsection wavelDependentRetr        ( repeat for successive wavelength bands )
wavelSurfAlbedo        240.0  300.0  400.0  490.0  510.0  530.0  550.0  570.0  590.0  610.0  630.0  650.0 &
                       670.0  690.0  710.0  730.0  750.0  770.0  800.0
surfAlbedo             0.059  0.059  0.059  0.059  0.067  0.095  0.133  0.127  0.107  0.096  0.088  0.084 &
                       0.092  0.137  0.227  0.308  0.379  0.429  0.471
varSurfAlbedo          0.010  0.010  0.010  0.010  0.010  0.010  0.010  0.010  0.010  0.010  0.010  0.010 &
                       0.010  0.010  0.010  0.010  0.010  0.010  0.010
wavelSurfEmission      500.0  ( wavelengths where surface emission is specified )
surfEmission             0.0  ( emission in 10^12 ph/cm2/s/nm/sr; fluorescence )
varSurfEmission        100.0  ( variance of surface emission in [10^12 ph/cm2/s/nm/sr]**2 )
