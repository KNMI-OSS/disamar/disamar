# Wavelength dependent surface albedo for Sahara sand.
# based on sacspectra of an area in the Sahara desert
# Corners used (lat/lon) = (30.0, 12.0), (28.0, 22.0), (26.0, 22.0), (24.0, 12.0)
# wavelength range = 335 - 772 nm

SECTION SURFACE

subsection pressure
surfPressureSim     1013.00   (surface pressure denotes the lower boundary of the atmosphere - in hPa)
surfPressureRetr    1013.00   (surface pressure denotes the lower boundary of the atmosphere - in hPa)
varSurfPressureRetr   25.00   (variance of the surface pressure in hPa**2)

subsection surfaceType (wavelength independent or polynomial in the wavelength for each spectral band)
surfaceTypeSim    wavelDependent     ( choose between wavelIndependent and wavelDependent )
surfaceTypeRetr   wavelDependent     ( choose between wavelIndependent and wavelDependent )

subsection wavelDependentSim ( repeat for successive wavelength bands )
wavelSurfAlbedo        335.00  380.00  416.00  440.00  463.00  550.00  610.00  670.00  758.00  772.00
surfAlbedo             0.0988  0.0896  0.1080  0.1237  0.1369  0.2107  0.2807  0.3234  0.3745  0.3810
wavelSurfEmission      500.0  ( wavelengths where surface emission is specified )
surfEmission             0.0  ( emission in 10^12 ph/cm2/s/nm/sr; fluorescence )

subsection wavelDependentRetr        ( repeat for successive wavelength bands )
wavelSurfAlbedo        335.00  380.00  416.00  440.00  463.00  550.00  610.00  670.00  758.00  772.00
surfAlbedo             0.0988  0.0896  0.1080  0.1237  0.1369  0.2107  0.2807  0.3234  0.3745  0.3810
varSurfAlbedo          0.0100  0.0100  0.0100  0.0100  0.0100  0.0100  0.0100  0.0100  0.0100  0.0100  
wavelSurfEmission      500.0  ( wavelengths where surface emission is specified )
surfEmission             0.0  ( emission in 10^12 ph/cm2/s/nm/sr; fluorescence )
varSurfEmission        100.0  ( variance of surface emission in [10^12 ph/cm2/s/nm/sr]**2 )
