# if none is selected there is no aerosol present in the entire atmosphere 

SECTION AEROSOL

subsection aerosolType
aerosolTypeSim     none     ( choose between HGscattering, MieScattering, Lambertian, LambWavelIndep, or none)
aerosolTypeRetr    none     ( choose between HGscattering, MieScattering, Lambertian, LambWavelIndep, or none)
