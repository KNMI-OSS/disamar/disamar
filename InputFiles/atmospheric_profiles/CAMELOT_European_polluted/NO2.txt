
SECTION NO2
# the following absorbing gases are recognized as section name
# O3, NO2, trop_NO2, strat_NO2, O2, O2-O2, SO2, HCHO, CHOCHO, BrO, H2O, CH4, CO, CO2

subsection filenames
XsectionFileNameSim      RefSpec/NO2T_VD.dat (file name with absorption cross section data used for simulation)
XsectionFileNameRetr     RefSpec/NO2T_VD.dat (file name with absorption cross section data used for retrieval)

subsection specifyFitting
# profile and column can not be fitted for O2 and O2-O2
fitProfile           0   (1 = fit profile ; 0 = do not fit profile)
fitColumn            1   (1 = fit column  ; 0 = do not fit column)  (it is an error to fit both column and profile)

subsection  profile
# NO2 Trace gas profile for European_polluted based on TM4
# not for TM4+Chimere but only TM4
# Latitude   =       51.0000
# Longitude  =       7.00000
# Month      =        6
# TM4 Latitude index    =       70
# TM4 Longitude index   =       61
# TM4 Lat (min-max)     =       50.0000      52.0000
# TM4 Lon (min-max)     =       3.00000      6.00000
# 
P_vmr_ppmv_sim   1008.900    0.0150600
P_vmr_ppmv_sim    895.617    0.0003910
P_vmr_ppmv_sim    793.521    0.0001584
P_vmr_ppmv_sim    701.221    0.0000841
P_vmr_ppmv_sim    618.594    0.0000534
P_vmr_ppmv_sim    543.953    0.0000376
P_vmr_ppmv_sim    476.430    0.0000317
P_vmr_ppmv_sim    416.582    0.0000290
P_vmr_ppmv_sim    363.078    0.0000301
P_vmr_ppmv_sim    314.561    0.0000441
P_vmr_ppmv_sim    270.302    0.0000712
P_vmr_ppmv_sim    233.133    0.0000978
P_vmr_ppmv_sim    199.909    0.0001114
P_vmr_ppmv_sim    170.513    0.0001365
P_vmr_ppmv_sim    146.953    0.0001677
P_vmr_ppmv_sim    124.751    0.0002437
P_vmr_ppmv_sim    107.833    0.0003273
P_vmr_ppmv_sim     91.496    0.0004276
P_vmr_ppmv_sim     78.890    0.0005312
P_vmr_ppmv_sim     67.421    0.0006520
P_vmr_ppmv_sim     57.347    0.0007862
P_vmr_ppmv_sim     49.699    0.0009210
P_vmr_ppmv_sim     42.050    0.0010830
P_vmr_ppmv_sim     36.830    0.0012450
P_vmr_ppmv_sim     32.779    0.0014070
P_vmr_ppmv_sim     28.728    0.0015690
P_vmr_ppmv_sim     18.600    0.0019890
P_vmr_ppmv_sim     13.431    0.0024100
P_vmr_ppmv_sim      9.344    0.0027310
P_vmr_ppmv_sim      6.382    0.0029950
P_vmr_ppmv_sim      4.762    0.0032310
P_vmr_ppmv_sim      3.142    0.0033330
P_vmr_ppmv_sim      2.466    0.0034350
P_vmr_ppmv_sim      1.899    0.0035030
P_vmr_ppmv_sim      1.331    0.0033040
P_vmr_ppmv_sim      0.772    0.0031050
P_vmr_ppmv_sim      0.399    0.0016060
P_vmr_ppmv_sim      0.206    0.0008289
P_vmr_ppmv_sim      0.106    0.0004272
P_vmr_ppmv_sim      0.053    0.0002146

# European_polluted profile from CAMELOT based on TM4
P_vmr_ppmv_error_percent_retr   1008.900    0.0150600  20.00000E+00
P_vmr_ppmv_error_percent_retr    895.617    0.0003910  20.00000E+00
P_vmr_ppmv_error_percent_retr    793.521    0.0001584  20.00000E+00
P_vmr_ppmv_error_percent_retr    701.221    0.0000841  20.00000E+00
P_vmr_ppmv_error_percent_retr    618.594    0.0000534  20.00000E+00
P_vmr_ppmv_error_percent_retr    543.953    0.0000376  20.00000E+00
P_vmr_ppmv_error_percent_retr    476.430    0.0000317  20.00000E+00
P_vmr_ppmv_error_percent_retr    416.582    0.0000290  20.00000E+00
P_vmr_ppmv_error_percent_retr    363.078    0.0000301  20.00000E+00
P_vmr_ppmv_error_percent_retr    314.561    0.0000441  20.00000E+00
P_vmr_ppmv_error_percent_retr    270.302    0.0000712  20.00000E+00
P_vmr_ppmv_error_percent_retr    233.133    0.0000978  20.00000E+00
P_vmr_ppmv_error_percent_retr    199.909    0.0001114  20.00000E+00
P_vmr_ppmv_error_percent_retr    170.513    0.0001365  20.00000E+00
P_vmr_ppmv_error_percent_retr    146.953    0.0001677  20.00000E+00
P_vmr_ppmv_error_percent_retr    124.751    0.0002437  20.00000E+00
P_vmr_ppmv_error_percent_retr    107.833    0.0003273  20.00000E+00
P_vmr_ppmv_error_percent_retr     91.496    0.0004276  20.00000E+00
P_vmr_ppmv_error_percent_retr     78.890    0.0005312  20.00000E+00
P_vmr_ppmv_error_percent_retr     67.421    0.0006520  20.00000E+00
P_vmr_ppmv_error_percent_retr     57.347    0.0007862  20.00000E+00
P_vmr_ppmv_error_percent_retr     49.699    0.0009210  20.00000E+00
P_vmr_ppmv_error_percent_retr     42.050    0.0010830  20.00000E+00
P_vmr_ppmv_error_percent_retr     36.830    0.0012450  20.00000E+00
P_vmr_ppmv_error_percent_retr     32.779    0.0014070  20.00000E+00
P_vmr_ppmv_error_percent_retr     28.728    0.0015690  20.00000E+00
P_vmr_ppmv_error_percent_retr     18.600    0.0019890  20.00000E+00
P_vmr_ppmv_error_percent_retr     13.431    0.0024100  20.00000E+00
P_vmr_ppmv_error_percent_retr      9.344    0.0027310  20.00000E+00
P_vmr_ppmv_error_percent_retr      6.382    0.0029950  20.00000E+00
P_vmr_ppmv_error_percent_retr      4.762    0.0032310  20.00000E+00
P_vmr_ppmv_error_percent_retr      3.142    0.0033330  20.00000E+00
P_vmr_ppmv_error_percent_retr      2.466    0.0034350  20.00000E+00
P_vmr_ppmv_error_percent_retr      1.899    0.0035030  20.00000E+00
P_vmr_ppmv_error_percent_retr      1.331    0.0033040  20.00000E+00
P_vmr_ppmv_error_percent_retr      0.772    0.0031050  20.00000E+00
P_vmr_ppmv_error_percent_retr      0.399    0.0016060  20.00000E+00
P_vmr_ppmv_error_percent_retr      0.206    0.0008289  20.00000E+00
P_vmr_ppmv_error_percent_retr      0.106    0.0004272  20.00000E+00
P_vmr_ppmv_error_percent_retr      0.053    0.0002146  20.00000E+00

# the profile is defined through linear interpolation on the logarithm of the volume mixing ratio
# or through cubic spline interpolation on the logarithm of the volume mixing ratio.
# For strong absorption (ozone at wavelengths < 305 nm) and in the oxygen A band spline intrepolation can become
# inaccurate and linear interpolation is preferred. If spline interpolation is used there can be the
# side efect that d2R/dkabs/dz > 0 for some parts of the atmosphere and strong absorption. This has negative
# consequences if DISMAS is used as retrieval method because we can not fit a low order polynomial in the wavelength 
# for ln(d2R/dkabs/dz), but have to use  a polynomial for d2R/dkabs/dz. Hence for ozone profile retrieval
# combined with DISMAS linear interpolation should be used.
useLinInterpSim          0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )
useLinInterpRetr         0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )

subsection column
# specify colum either in molecules cm-2 or in Dobson Units
columnSim_molcm2       6.0E15  ( in molecules cm-2; use columnSim_DU for Dobson Units )
columnRetr_molcm2      6.0E15  ( in molecules cm-2; use columnRetr_DU for Dobson Units )
# columnSim_DU         300.0   ( in DU; use columnSim_molcm2 for molecules cm-2)
# columnRetr_DU        300.0   ( in DU; use columnRetr_molcm2 for molecules cm-2)
APerrorColumn_percent    500.0   ( a-priori error for the column in percent)

subsection scaling
# If scaleProfileToColumn = 1 the profile shape is kept constant but the profile is scaled so that
# the column agrees with the column specified in subsection column. If scaleProfileToColumn = 0 no
# scaling is performed and the column values specified in subsection column are ignored. In fact,
# internally the column values read are overwritten with the column value calculated from the profile.
# The a priori error APerrorColumn_percent and the errors specified for the profile are expressed
# in percent and these are therefore not affected by scaling.
scaleProfileToColumnSim  0      ( 1 = scale profile ; 0 = do not scale profile)
scaleProfileToColumnRetr 0      ( 1 = scale profile ; 0 = do not scale profile)
scaleFactorXsecSim       1.0    ( absorption cross section is multiplied with this factor ) 
scaleFactorXsecRetr      1.0    ( absorption cross section is multiplied with this factor ) 

subsection errorCovariancesSpecs ( only required if the profile is fitted )
useAPCorrLength          1       (if true a correlation length is used to fill the non-diagonal elements of Sa)
APCorrLength             6.0     (in km; only used if useAPCorrLength /= 0 )
removeAPcorrTropStrat    1 (1= no correlation between levels in stratosphere and troposphere)
useSaFromFile            0 (if true it reads the a-priori covariance matrix from file, replacing 'aPrioriError_percent')
useSaDiagFromFile        0 (if useSaFromFile and useSaDiagFromFile are both true only the diagnal id used)
APcovarianceFileName     RefSpec/mozart_error_covariance_matrix.dat  (name of file containing Sa; used if useSaFromFile /= 0)

