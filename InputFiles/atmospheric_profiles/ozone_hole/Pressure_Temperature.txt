# sub arctic winter atmosphere SAW, AFGL(1986) (from Lowtran7)
# used as model for ozone hole conditions

SECTION PRESSURE_TEMPERATURE

subsection PT_sim (pressure P in hPa and temperature T in K)
PT    0.1050E+04     257.20
PT    0.1013E+04     257.20
PT    0.8878E+03     259.10
PT    0.7775E+03     255.90
PT    0.6798E+03     252.70
PT    0.5932E+03     247.70
PT    0.5158E+03     240.90
PT    0.4467E+03     234.10
PT    0.3853E+03     227.30
PT    0.3308E+03     220.60
PT    0.2829E+03     217.20
PT    0.2418E+03     217.20
PT    0.2067E+03     217.20
PT    0.1766E+03     217.20
PT    0.1510E+03     217.20
PT    0.1291E+03     217.20
PT    0.1103E+03     217.20
PT    0.9431E+02     216.60
PT    0.8058E+02     216.00
PT    0.6882E+02     215.40
PT    0.5875E+02     214.80
PT    0.5014E+02     214.20
PT    0.4277E+02     213.60
PT    0.3647E+02     213.00
PT    0.3109E+02     212.40
PT    0.2649E+02     211.80
PT    0.2256E+02     211.20
PT    0.1513E+02     213.60
PT    0.1020E+02     216.00
PT    0.6910E+01     218.50
PT    0.4701E+01     222.30
PT    0.3230E+01     228.50
PT    0.2243E+01     234.70
PT    0.1570E+01     240.80
PT    0.1113E+01     247.00
PT    0.7900E+00     253.20
PT    0.5719E+00     259.30
PT    0.2990E+00     259.10
PT    0.1550E+00     250.90
PT    0.7900E-01     248.40
PT    0.4000E-01     245.40
PT    0.2000E-01     234.70
PT    0.9660E-02     223.90
PT    0.4500E-02     213.10
PT    0.2022E-02     202.30
PT    0.9070E-03     211.00
PT    0.4230E-03     218.50

subsection PT_retr (pressure P in hPa, temperature T in K, and variance T in K**2)
PT    0.1050E+04     257.20     1.00E+01
PT    0.1013E+04     257.20     1.00E+01
PT    0.8878E+03     259.10     1.00E+01
PT    0.7775E+03     255.90     1.00E+01
PT    0.6798E+03     252.70     1.00E+01
PT    0.5932E+03     247.70     1.00E+01
PT    0.5158E+03     240.90     1.00E+01
PT    0.4467E+03     234.10     1.00E+01
PT    0.3853E+03     227.30     1.00E+01
PT    0.3308E+03     220.60     1.00E+01
PT    0.2829E+03     217.20     1.00E+01
PT    0.2418E+03     217.20     1.00E+01
PT    0.2067E+03     217.20     1.00E+01
PT    0.1766E+03     217.20     1.00E+01
PT    0.1510E+03     217.20     1.00E+01
PT    0.1291E+03     217.20     1.00E+01
PT    0.1103E+03     217.20     1.00E+01
PT    0.9431E+02     216.60     1.00E+01
PT    0.8058E+02     216.00     1.00E+01
PT    0.6882E+02     215.40     1.00E+01
PT    0.5875E+02     214.80     1.00E+01
PT    0.5014E+02     214.20     1.00E+01
PT    0.4277E+02     213.60     1.00E+01
PT    0.3647E+02     213.00     1.00E+01
PT    0.3109E+02     212.40     1.00E+01
PT    0.2649E+02     211.80     1.00E+01
PT    0.2256E+02     211.20     1.00E+01
PT    0.1513E+02     213.60     1.00E+01
PT    0.1020E+02     216.00     1.00E+01
PT    0.6910E+01     218.50     1.00E+01
PT    0.4701E+01     222.30     1.00E+01
PT    0.3230E+01     228.50     1.00E+01
PT    0.2243E+01     234.70     1.00E+01
PT    0.1570E+01     240.80     1.00E+01
PT    0.1113E+01     247.00     1.00E+01
PT    0.7900E+00     253.20     1.00E+01
PT    0.5719E+00     259.30     1.00E+01
PT    0.2990E+00     259.10     1.00E+01
PT    0.1550E+00     250.90     1.00E+01
PT    0.7900E-01     248.40     1.00E+01
PT    0.4000E-01     245.40     1.00E+01
PT    0.2000E-01     234.70     1.00E+01
PT    0.9660E-02     223.90     1.00E+01
PT    0.4500E-02     213.10     1.00E+01
PT    0.2022E-02     202.30     1.00E+01
PT    0.9070E-03     211.00     1.00E+01
PT    0.4230E-03     218.50     1.00E+01

subsection temperature_offset
temperatureOffsetSim    0.0d0    ( change the temperature for simulation: the same offset for all altitudes )
varianceTempOffsetAP  400.0d0    ( a-priori variance of the offset for the temperature in Kelvin**2 )

subsection specificationsT  ( specifications for the temperature)
useLinInterpSim    0        ( 0: spline interpolation for T(ln_p), 1: linear interpolation for T(ln_p) )
useLinInterpRetr   0        ( 0: spline interpolation for T(ln_p), 1: linear interpolation for T(ln_p) )
useAPCorrLength    0        ( 0: non-diagonal elements of Sa are 0, /= 0: non-diagonal elements of Sa are filled)
APCorrLength       0.3      ( correlation length for temperature in km; only used if useAPCorrLength /= 0 )

