# ozone profile for tropical atmosphere - hardly any ozone in stratosphere

SECTION O3 (troposphric)
# the following absorbing gases are recognized as section name
# O3, NO2, trop_NO2, strat_NO2, O2, O2-O2, SO2, HCHO, CHOCHO, BrO, H2O, CH4, CO, CO2

subsection filenames
XsectionFileNameSim      RefSpec/O3_Brion_coeff_4Temp.txt (file name with absorption cross section for simulation)
XsectionFileNameRetr     RefSpec/O3_Brion_coeff_4Temp.txt (file name with absorption cross section for retrieval)

subsection climatology     (the subsection climatology is required only for ozone; it must be omitted for other gases)
useO3ClimSim         0     (1= use TOMS V8 climatology to replace vmr for key P_vmr_ppmv_sim; 0= do not replace vmr)
useO3ClimRetr        0     (1= use TOMS V8 climatology to replace vmr for key P_vmr_ppmv_error_percent_retr; 0= do not replace vmr)
latitude             0.0   (specify latitude in degrees (-90, 90) )
month                3     (specify month 1 = Jan; 2 = Febr; ....; 12 = Dec )
day_of_month        12     (specify day of month [1,31]; tabulated values in climatology are for day_of_month = 15)
relError_vmr        9.0    (replace the error in percent for key P_vmr_ppmv_error_percent_retr by the value listed here)

subsection specifyFitting
# profile and column can not be fitted for O2 and O2-O2
fitProfile           0   (1 = fit profile ; 0 = do not fit profile)
fitColumn            1   (1 = fit column  ; 0 = do not fit column)  (it is an error to fit both column and profile)

subsection  profile (tropical atmosphere model)
# note that for a collision complex the parent gas has to be specified here, 
# e.g. for O2-O2 the volume mixing ratio of O2 has to be specified

P_vmr_ppmv_sim     0.1050E+04      0.287E-01
P_vmr_ppmv_sim     0.1013E+04      0.287E-01
P_vmr_ppmv_sim     0.9040E+03      0.315E-01
P_vmr_ppmv_sim     0.8050E+03      0.334E-01
P_vmr_ppmv_sim     0.7150E+03      0.350E-01
P_vmr_ppmv_sim     0.6330E+03      0.356E-01
P_vmr_ppmv_sim     0.5590E+03      0.377E-01
P_vmr_ppmv_sim     0.4920E+03      0.399E-01
P_vmr_ppmv_sim     0.4320E+03      0.422E-01
P_vmr_ppmv_sim     0.3780E+03      0.447E-01
P_vmr_ppmv_sim     0.3290E+03      0.500E-01
P_vmr_ppmv_sim     0.2860E+03      0.560E-01
P_vmr_ppmv_sim     0.2470E+03      0.661E-01
P_vmr_ppmv_sim     0.2130E+03      0.781E-01
P_vmr_ppmv_sim      0.2050E+03      0.781E-01
P_vmr_ppmv_sim      0.2040E+03      0.600E-01
P_vmr_ppmv_sim      0.2030E+03      0.300E-01
P_vmr_ppmv_sim      0.2020E+03      0.100E-01
P_vmr_ppmv_sim      0.2010E+03      0.500E-02
P_vmr_ppmv_sim      0.2000E+03      0.100E-02
P_vmr_ppmv_sim      0.1990E+03      0.500E-03
P_vmr_ppmv_sim      0.1980E+03      0.100E-03
P_vmr_ppmv_sim      0.1970E+03      0.500E-04
P_vmr_ppmv_sim      0.1960E+03      0.100E-04
P_vmr_ppmv_sim      0.1950E+03      0.800E-05
P_vmr_ppmv_sim      0.1940E+03      0.700E-05
P_vmr_ppmv_sim      0.1930E+03      0.600E-05
P_vmr_ppmv_sim      0.1920E+03      0.500E-05
P_vmr_ppmv_sim      0.1910E+03      0.500E-05
P_vmr_ppmv_sim      0.1900E+03      0.500E-05
P_vmr_ppmv_sim     0.1820E+03      0.500E-05
P_vmr_ppmv_sim     0.1560E+03      0.500E-05
P_vmr_ppmv_sim     0.1320E+03      0.500E-05
P_vmr_ppmv_sim     0.1110E+03      0.500E-05
P_vmr_ppmv_sim     0.9370E+02      0.500E-05
P_vmr_ppmv_sim     0.7890E+02      0.500E-05
P_vmr_ppmv_sim     0.6660E+02      0.500E-05
P_vmr_ppmv_sim     0.5650E+02      0.500E-05
P_vmr_ppmv_sim     0.4800E+02      0.500E-05
P_vmr_ppmv_sim     0.4090E+02      0.500E-05
P_vmr_ppmv_sim     0.3500E+02      0.500E-05
P_vmr_ppmv_sim     0.3000E+02      0.500E-05
P_vmr_ppmv_sim     0.2570E+02      0.500E-05
P_vmr_ppmv_sim     0.1763E+02      0.500E-05
P_vmr_ppmv_sim     0.1220E+02      0.500E-05
P_vmr_ppmv_sim     0.8520E+01      0.500E-05
P_vmr_ppmv_sim     0.6000E+01      0.500E-05
P_vmr_ppmv_sim     0.4260E+01      0.500E-05
P_vmr_ppmv_sim     0.3050E+01      0.500E-05
P_vmr_ppmv_sim     0.2200E+01      0.500E-05
P_vmr_ppmv_sim     0.1590E+01      0.500E-05
P_vmr_ppmv_sim     0.1160E+01      0.500E-05
P_vmr_ppmv_sim     0.8540E+00      0.500E-05
P_vmr_ppmv_sim     0.4560E+00      0.500E-05
P_vmr_ppmv_sim     0.2390E+00      0.500E-05
P_vmr_ppmv_sim     0.1210E+00      0.500E-05
P_vmr_ppmv_sim     0.5800E-01      0.500E-05
P_vmr_ppmv_sim     0.2600E-01      0.500E-05
P_vmr_ppmv_sim     0.1100E-01      0.500E-05
P_vmr_ppmv_sim     0.4400E-02      0.500E-05
P_vmr_ppmv_sim     0.1720E-02      0.500E-05
P_vmr_ppmv_sim     0.6880E-03      0.500E-05
P_vmr_ppmv_sim     0.2890E-03      0.500E-05

P_vmr_ppmv_error_percent_retr    0.1050E+04      0.287E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1013E+04      0.287E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.9040E+03      0.315E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.8050E+03      0.334E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.7150E+03      0.350E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.6330E+03      0.356E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.5590E+03      0.377E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.4920E+03      0.399E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.4320E+03      0.422E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.3780E+03      0.447E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.3290E+03      0.500E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.2860E+03      0.560E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.2470E+03      0.661E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.2130E+03      0.781E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.2050E+03      0.781E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.2040E+03      0.600E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.2030E+03      0.300E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.2020E+03      0.100E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.2010E+03      0.500E-02    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.2000E+03      0.100E-02    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1990E+03      0.500E-03    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1980E+03      0.100E-03    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1970E+03      0.500E-04    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1960E+03      0.100E-04    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1950E+03      0.800E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1940E+03      0.700E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1930E+03      0.600E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1920E+03      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1910E+03      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr     0.1900E+03      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1820E+03      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1560E+03      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1320E+03      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1110E+03      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.9370E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.7890E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.6660E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.5650E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.4800E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.4090E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.3500E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.3000E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.2570E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1763E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1220E+02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.8520E+01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.6000E+01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.4260E+01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.3050E+01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.2200E+01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1590E+01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1160E+01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.8540E+00      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.4560E+00      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.2390E+00      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1210E+00      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.5800E-01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.2600E-01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1100E-01      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.4400E-02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.1720E-02      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.6880E-03      0.500E-05    20.00000E+00
P_vmr_ppmv_error_percent_retr    0.2890E-03      0.500E-05    20.00000E+00

# the profile is defined through linear interpolation on the logarithm of the volume mixing ratio
# or through cubic spline interpolation on the logarithm of the volume mixing ratio.
# For strong absorption (ozone at wavelengths < 305 nm) and in the oxygen A band spline intrepolation can become
# inaccurate and linear interpolation is preferred. If spline interpolation is used there can be the
# side efect that d2R/dkabs/dz > 0 for some parts of the atmosphere and strong absorption. This has negative
# consequences if DISMAS is used as retrieval method because we can not fit a low order polynomial in the wavelength 
# for ln(d2R/dkabs/dz), but have to use  a polynomial for d2R/dkabs/dz. Hence for ozone profile retrieval
# combined with DISMAS linear interpolation should be used.
useLinInterpSim          0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )
useLinInterpRetr         0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )

subsection column
# specify colum either in molecules cm-2 or in Dobson Units
# columnSim_molcm2       6.0E15  ( in molecules cm-2; use columnSim_DU for Dobson Units )
# columnRetr_molcm2      6.0E15  ( in molecules cm-2; use columnRetr_DU for Dobson Units )
columnSim_DU             300.0   ( in DU; use columnSim_molcm2 for molecules cm-2)
columnRetr_DU            300.0   ( in DU; use columnRetr_molcm2 for molecules cm-2)
APerrorColumn_percent    200.0   ( a-priori error for the column in percent)

subsection scaling
# If scaleProfileToColumn = 1 the profile shape is kept constant but the profile is scaled so that
# the column agrees with the column specified in subsection column. If scaleProfileToColumn = 0 no
# scaling is performed and the column values specified in subsection column are ignored. In fact,
# internally the column values read are overwritten with the column value calculated from the profile.
# The a priori error APerrorColumn_percent and the errors specified for the profile are expressed
# in percent and these are therefore not affected by scaling.
scaleProfileToColumnSim  0      ( 1 = scale profile ; 0 = do not scale profile) (the shape of the profile remains the same)
scaleProfileToColumnRetr 0      ( 1 = scale profile ; 0 = do not scale profile) (the shape of the profile remains the same)
scaleFactorXsecSim       1.0    ( absorption cross section is multiplied with this factor ) 
scaleFactorXsecRetr      1.0    ( absorption cross section is multiplied with this factor ) 

subsection errorCovariancesSpecs ( only required if the profile is fitted )
useAPCorrLength          1       (if true a correlation length is used to fill the non-diagonal elements of Sa)
APCorrLength             6.0     (in km; only used if useAPCorrLength /= 0 )
removeAPcorrTropStrat    1 (1= no correlation between levels in stratosphere and troposphere)
useSaFromFile            0 (if true it reads the a-priori covariance matrix from file, replacing 'aPrioriError_percent')
useSaDiagFromFile        0 (if useSaFromFile and useSaDiagFromFile are both true only the diagnal id used)
APcovarianceFileName     RefSpec/mozart_error_covariance_matrix.dat  (name of file containing Sa; used if useSaFromFile /= 0)

