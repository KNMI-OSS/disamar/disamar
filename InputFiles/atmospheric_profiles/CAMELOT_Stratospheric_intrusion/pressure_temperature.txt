# Temperature profile for Stratospheric Intrusion based on TM4
# Latitude   =       40.0000
# Longitude  =      -73.0000
# Month      =        1
# TM4 Latitude index    =       70
# TM4 Longitude index   =       61
# TM4 Lat (min-max)     =       38.0000      40.0000
# TM4 Lon (min-max)     =      -75.0000     -72.0000
# surface pressure      =       1025.330 hPa (profile extended to 1050 hPa)

SECTION PRESSURE_TEMPERATURE

subsection PT_sim (pressure P in hPa and temperature T in K - Stratospheric Intrusion based on TM4)
PT  1050.0000     267.8710
PT  1025.3300     267.8710
PT   900.1870     258.0420
PT   787.6930     255.5540
PT   689.8300     254.6030
PT   603.1420     251.6390
PT   526.2810     247.3650
PT   458.0290     242.9900
PT   397.2240     239.0940
PT   343.7650     235.8590
PT   298.2950     233.3330
PT   257.5870     232.1800
PT   221.8420     230.6940
PT   192.3060     229.0060
PT   164.3780     225.2530
PT   142.4200     221.4220
PT   121.0070     218.7680
PT   104.6090     216.1460
PT    88.2210     214.3630
PT    76.3530     212.5830
PT    64.6460     212.1520
PT    55.2940     212.0760
PT    47.4140     211.8840
PT    39.5770     211.4340
PT    35.2570     210.9830
PT    30.9660     210.5320
PT    26.6750     210.4600
PT    16.4370     212.0490
PT    11.9690     213.6550
PT     7.5020     221.1690
PT     5.5280     228.7230
PT     3.7890     237.6530
PT     2.6650     247.1090
PT     2.0810     256.5650
PT     1.4980     256.6580
PT     0.9240     254.3720
PT     0.3420     252.0860
PT     0.1740     248.2390
PT     0.0890     245.0610
PT     0.0460     240.1000
PT     0.0230     218.1000

subsection PT_retr (pressure P in hPa, temperature T in K, and variance T in K**2, Stratospheric Intrusion based on TM4)
PT  1050.0000     267.8710     1.00E+01
PT  1025.3300     267.8710     1.00E+01
PT   900.1870     258.0420     1.00E+01
PT   787.6930     255.5540     1.00E+01
PT   689.8300     254.6030     1.00E+01
PT   603.1420     251.6390     1.00E+01
PT   526.2810     247.3650     1.00E+01
PT   458.0290     242.9900     1.00E+01
PT   397.2240     239.0940     1.00E+01
PT   343.7650     235.8590     1.00E+01
PT   298.2950     233.3330     1.00E+01
PT   257.5870     232.1800     1.00E+01
PT   221.8420     230.6940     1.00E+01
PT   192.3060     229.0060     1.00E+01
PT   164.3780     225.2530     1.00E+01
PT   142.4200     221.4220     1.00E+01
PT   121.0070     218.7680     1.00E+01
PT   104.6090     216.1460     1.00E+01
PT    88.2210     214.3630     1.00E+01
PT    76.3530     212.5830     1.00E+01
PT    64.6460     212.1520     1.00E+01
PT    55.2940     212.0760     1.00E+01
PT    47.4140     211.8840     1.00E+01
PT    39.5770     211.4340     1.00E+01
PT    35.2570     210.9830     1.00E+01
PT    30.9660     210.5320     1.00E+01
PT    26.6750     210.4600     1.00E+01
PT    16.4370     212.0490     1.00E+01
PT    11.9690     213.6550     1.00E+01
PT     7.5020     221.1690     1.00E+01
PT     5.5280     228.7230     1.00E+01
PT     3.7890     237.6530     1.00E+01
PT     2.6650     247.1090     1.00E+01
PT     2.0810     256.5650     1.00E+01
PT     1.4980     256.6580     1.00E+01
PT     0.9240     254.3720     1.00E+01
PT     0.3420     252.0860     1.00E+01
PT     0.1740     248.2390     1.00E+01
PT     0.0890     245.0610     1.00E+01
PT     0.0460     240.1000     1.00E+01
PT     0.0230     218.1000     1.00E+01

subsection temperature_offset
temperatureOffsetSim    0.0d0    ( change the temperature for simulation: the same offset for all altitudes )
varianceTempOffsetAP  400.0d0    ( a-priori variance of the offset for the temperature in Kelvin**2 )

subsection specificationsT  ( specifications for the temperature)
useLinInterpSim    0        ( 0: spline interpolation for T(ln_p), 1: linear interpolation for T(ln_p) )
useLinInterpRetr   0        ( 0: spline interpolation for T(ln_p), 1: linear interpolation for T(ln_p) )
useAPCorrLength    0        ( 0: non-diagonal elements of Sa are 0, /= 0: non-diagonal elements of Sa are filled)
APCorrLength       3.0      ( correlation length for temperature in km; only used if useAPCorrLength /= 0 )
