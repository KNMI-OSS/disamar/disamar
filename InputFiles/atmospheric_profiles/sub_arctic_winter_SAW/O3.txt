# ozone profile for sub arctic winter atmosphere SAW, AFGL(1986) (from Lowtran7)

SECTION O3
# the following absorbing gases are recognized as section name
# O3, NO2, trop_NO2, strat_NO2, O2, O2-O2, SO2, HCHO, CHOCHO, BrO, H2O, CH4, CO, CO2

subsection filenames
XsectionFileNameSim      RefSpec/O3_Brion_coeff_4Temp.txt (file name with absorption cross section for simulation)
XsectionFileNameRetr     RefSpec/O3_Brion_coeff_4Temp.txt (file name with absorption cross section for retrieval)

subsection climatology     (the subsection climatology is required only for ozone; it must be omitted for other gases)
useO3ClimSim         0     (1= use TOMS V8 climatology to replace vmr for key P_vmr_ppmv_sim; 0= do not replace vmr)
useO3ClimRetr        0     (1= use TOMS V8 climatology to replace vmr for key P_vmr_ppmv_error_percent_retr; 0= do not replace vmr)
latitude             0.0   (specify latitude in degrees (-90, 90) )
month                3     (specify month 1 = Jan; 2 = Febr; ....; 12 = Dec )
day_of_month        12     (specify day of month [1,31]; tabulated values in climatology are for day_of_month = 15)
relError_vmr        9.0    (replace the error in percent for key P_vmr_ppmv_error_percent_retr by the value listed here)

subsection specifyFitting
# profile and column can not be fitted for O2 and O2-O2
fitProfile           0   (1 = fit profile ; 0 = do not fit profile)
fitColumn            1   (1 = fit column  ; 0 = do not fit column)  (it is an error to fit both column and profile)

subsection  profile
# note that for a collision complex the parent gas has to be specified here, 
# e.g. for O2-O2 the volume mixing ratio of O2 has to be specified

P_vmr_ppmv_sim     0.1050E+04     0.180E-01
P_vmr_ppmv_sim     0.1013E+04     0.180E-01
P_vmr_ppmv_sim     0.8878E+03     0.207E-01
P_vmr_ppmv_sim     0.7775E+03     0.234E-01
P_vmr_ppmv_sim     0.6798E+03     0.277E-01
P_vmr_ppmv_sim     0.5932E+03     0.325E-01
P_vmr_ppmv_sim     0.5158E+03     0.380E-01
P_vmr_ppmv_sim     0.4467E+03     0.445E-01
P_vmr_ppmv_sim     0.3853E+03     0.725E-01
P_vmr_ppmv_sim     0.3308E+03     0.104E+00
P_vmr_ppmv_sim     0.2829E+03     0.210E+00
P_vmr_ppmv_sim     0.2418E+03     0.300E+00
P_vmr_ppmv_sim     0.2067E+03     0.350E+00
P_vmr_ppmv_sim     0.1766E+03     0.400E+00
P_vmr_ppmv_sim     0.1510E+03     0.650E+00
P_vmr_ppmv_sim     0.1291E+03     0.900E+00
P_vmr_ppmv_sim     0.1103E+03     0.120E+01
P_vmr_ppmv_sim     0.9431E+02     0.150E+01
P_vmr_ppmv_sim     0.8058E+02     0.190E+01
P_vmr_ppmv_sim     0.6882E+02     0.245E+01
P_vmr_ppmv_sim     0.5875E+02     0.310E+01
P_vmr_ppmv_sim     0.5014E+02     0.370E+01
P_vmr_ppmv_sim     0.4277E+02     0.400E+01
P_vmr_ppmv_sim     0.3647E+02     0.420E+01
P_vmr_ppmv_sim     0.3109E+02     0.450E+01
P_vmr_ppmv_sim     0.2649E+02     0.460E+01
P_vmr_ppmv_sim     0.2256E+02     0.470E+01
P_vmr_ppmv_sim     0.1513E+02     0.490E+01
P_vmr_ppmv_sim     0.1020E+02     0.540E+01
P_vmr_ppmv_sim     0.6910E+01     0.590E+01
P_vmr_ppmv_sim     0.4701E+01     0.620E+01
P_vmr_ppmv_sim     0.3230E+01     0.625E+01
P_vmr_ppmv_sim     0.2243E+01     0.590E+01
P_vmr_ppmv_sim     0.1570E+01     0.510E+01
P_vmr_ppmv_sim     0.1113E+01     0.410E+01
P_vmr_ppmv_sim     0.7900E+00     0.300E+01
P_vmr_ppmv_sim     0.5719E+00     0.260E+01
P_vmr_ppmv_sim     0.2990E+00     0.160E+01
P_vmr_ppmv_sim     0.1550E+00     0.950E+00
P_vmr_ppmv_sim     0.7900E-01     0.650E+00
P_vmr_ppmv_sim     0.4000E-01     0.500E+00
P_vmr_ppmv_sim     0.2000E-01     0.330E+00
P_vmr_ppmv_sim     0.9660E-02     0.130E+00
P_vmr_ppmv_sim     0.4500E-02     0.750E+00
P_vmr_ppmv_sim     0.2022E-02     0.800E+00
P_vmr_ppmv_sim     0.9070E-03     0.800E+00
P_vmr_ppmv_sim     0.4230E-03     0.400E+00

P_vmr_ppmv_error_percent_retr   0.1050E+04      0.180E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1013E+04      0.180E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.8878E+03      0.207E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.7775E+03      0.234E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.6798E+03      0.277E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.5932E+03      0.325E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.5158E+03      0.380E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.4467E+03      0.445E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.3853E+03      0.725E-01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.3308E+03      0.104E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.2829E+03      0.210E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.2418E+03      0.300E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.2067E+03      0.350E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1766E+03      0.400E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1510E+03      0.650E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1291E+03      0.900E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1103E+03      0.120E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.9431E+02      0.150E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.8058E+02      0.190E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.6882E+02      0.245E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.5875E+02      0.310E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.5014E+02      0.370E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.4277E+02      0.400E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.3647E+02      0.420E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.3109E+02      0.450E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.2649E+02      0.460E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.2256E+02      0.470E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1513E+02      0.490E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1020E+02      0.540E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.6910E+01      0.590E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.4701E+01      0.620E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.3230E+01      0.625E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.2243E+01      0.590E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1570E+01      0.510E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1113E+01      0.410E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.7900E+00      0.300E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.5719E+00      0.260E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.2990E+00      0.160E+01    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.1550E+00      0.950E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.7900E-01      0.650E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.4000E-01      0.500E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.2000E-01      0.330E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.9660E-02      0.130E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.4500E-02      0.750E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.2022E-02      0.800E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.9070E-03      0.800E+00    20.00000E+00
P_vmr_ppmv_error_percent_retr   0.4230E-03      0.400E+00    20.00000E+00

# the profile is defined through linear interpolation on the logarithm of the volume mixing ratio
# or through cubic spline interpolation on the logarithm of the volume mixing ratio.
# For strong absorption (ozone at wavelengths < 305 nm) and in the oxygen A band spline intrepolation can become
# inaccurate and linear interpolation is preferred. If spline interpolation is used there can be the
# side efect that d2R/dkabs/dz > 0 for some parts of the atmosphere and strong absorption. This has negative
# consequences if DISMAS is used as retrieval method because we can not fit a low order polynomial in the wavelength 
# for ln(d2R/dkabs/dz), but have to use  a polynomial for d2R/dkabs/dz. Hence for ozone profile retrieval
# combined with DISMAS linear interpolation should be used.
useLinInterpSim          0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )
useLinInterpRetr         0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )

subsection column
# specify colum either in molecules cm-2 or in Dobson Units
# columnSim_molcm2       6.0E15  ( in molecules cm-2; use columnSim_DU for Dobson Units )
# columnRetr_molcm2      6.0E15  ( in molecules cm-2; use columnRetr_DU for Dobson Units )
columnSim_DU             300.0   ( in DU; use columnSim_molcm2 for molecules cm-2)
columnRetr_DU            300.0   ( in DU; use columnRetr_molcm2 for molecules cm-2)
APerrorColumn_percent    200.0   ( a-priori error for the column in percent)

subsection scaling
# If scaleProfileToColumn = 1 the profile shape is kept constant but the profile is scaled so that
# the column agrees with the column specified in subsection column. If scaleProfileToColumn = 0 no
# scaling is performed and the column values specified in subsection column are ignored. In fact,
# internally the column values read are overwritten with the column value calculated from the profile.
# The a priori error APerrorColumn_percent and the errors specified for the profile are expressed
# in percent and these are therefore not affected by scaling.
scaleProfileToColumnSim  0      ( 1 = scale profile ; 0 = do not scale profile) (the shape of the profile remains the same)
scaleProfileToColumnRetr 0      ( 1 = scale profile ; 0 = do not scale profile) (the shape of the profile remains the same)
scaleFactorXsecSim       1.0    ( absorption cross section is multiplied with this factor ) 
scaleFactorXsecRetr      1.0    ( absorption cross section is multiplied with this factor ) 

subsection errorCovariancesSpecs ( only required if the profile is fitted )
useAPCorrLength          1       (if true a correlation length is used to fill the non-diagonal elements of Sa)
APCorrLength             6.0     (in km; only used if useAPCorrLength /= 0 )
removeAPcorrTropStrat    1 (1= no correlation between levels in stratosphere and troposphere)
useSaFromFile            0 (if true it reads the a-priori covariance matrix from file, replacing 'aPrioriError_percent')
useSaDiagFromFile        0 (if useSaFromFile and useSaDiagFromFile are both true only the diagnal id used)
APcovarianceFileName     RefSpec/mozart_error_covariance_matrix.dat  (name of file containing Sa; used if useSaFromFile /= 0)

