
SECTION O3
# the following absorbing gases are recognized as section name
# O3, NO2, trop_NO2, strat_NO2, O2, O2-O2, SO2, HCHO, CHOCHO, BrO, H2O, CH4, CO, CO2

subsection filenames
XsectionFileNameSim      RefSpec/O3_Brion_coeff_4Temp.txt (file name with absorption cross section for simulation)
XsectionFileNameRetr     RefSpec/O3_Brion_coeff_4Temp.txt (file name with absorption cross section for retrieval)

subsection climatology     (the subsection climatology is required only for ozone; it must be omitted for other gases)
useO3ClimSim         0     (1= use TOMS V8 climatology to replace vmr for key P_vmr_ppmv_sim; 0= do not replace vmr)
useO3ClimRetr        0     (1= use TOMS V8 climatology to replace vmr for key P_vmr_ppmv_error_percent_retr; 0= do not replace vmr)
latitude             0.0   (specify latitude in degrees (-90, 90) )
month                3     (specify month 1 = Jan; 2 = Febr; ....; 12 = Dec )
day_of_month        12     (specify day of month [1,31]; tabulated values in climatology are for day_of_month = 15)
relError_vmr        9.0    (replace the error in percent for key P_vmr_ppmv_error_percent_retr by the value listed here)

subsection specifyFitting
# profile and column can not be fitted for O2 and O2-O2
fitProfile           0   (1 = fit profile ; 0 = do not fit profile)
fitColumn            1   (1 = fit column  ; 0 = do not fit column)  (it is an error to fit both column and profile)

subsection  profile
# O3 Trace gas profile for European_Background based on TM4
# Latitude   =       45.0000
# Longitude  =       2.00000
# Month      =        6
# TM4 Latitude index    =       67
# TM4 Longitude index   =       60
# TM4 Lat (min-max)     =       44.0000      46.0000
# TM4 Lon (min-max)     =       0.00000      3.00000
# surface pressure      =       977.520 hPa (profile extended to 1050 hPa)

P_vmr_ppmv_sim   1050.0000     0.05034
P_vmr_ppmv_sim    977.5200     0.05034
P_vmr_ppmv_sim    868.5980     0.05662
P_vmr_ppmv_sim    770.3700     0.05675
P_vmr_ppmv_sim    681.5660     0.05795
P_vmr_ppmv_sim    601.8100     0.05983
P_vmr_ppmv_sim    529.6150     0.06212
P_vmr_ppmv_sim    464.2380     0.06492
P_vmr_ppmv_sim    405.7080     0.06832
P_vmr_ppmv_sim    353.7480     0.07266
P_vmr_ppmv_sim    306.6830     0.07890
P_vmr_ppmv_sim    263.8090     0.09734
P_vmr_ppmv_sim    226.8600     0.1326
P_vmr_ppmv_sim    194.6650     0.1903
P_vmr_ppmv_sim    165.1240     0.2649
P_vmr_ppmv_sim    142.5480     0.3453
P_vmr_ppmv_sim    120.7390     0.4653
P_vmr_ppmv_sim    104.1690     0.5910
P_vmr_ppmv_sim     87.6600     0.8120
P_vmr_ppmv_sim     75.9550     1.0330
P_vmr_ppmv_sim     64.2900     1.4040
P_vmr_ppmv_sim     55.1550     1.8080
P_vmr_ppmv_sim     47.4170     2.2700
P_vmr_ppmv_sim     39.6970     2.8630
P_vmr_ppmv_sim     35.5530     3.4560
P_vmr_ppmv_sim     31.4830     4.0490
P_vmr_ppmv_sim     27.4120     4.6170
P_vmr_ppmv_sim     17.2360     5.7660
P_vmr_ppmv_sim     12.8630     6.9160
P_vmr_ppmv_sim      8.7580     7.2150
P_vmr_ppmv_sim      6.1390     7.2710
P_vmr_ppmv_sim      4.5080     7.0160
P_vmr_ppmv_sim      2.9410     6.1290
P_vmr_ppmv_sim      2.3710     5.2420
P_vmr_ppmv_sim      1.8010     4.3620
P_vmr_ppmv_sim      1.2320     3.5000
P_vmr_ppmv_sim      0.6710     2.6390
P_vmr_ppmv_sim      0.3450     1.3560
P_vmr_ppmv_sim      0.1770     0.6967
P_vmr_ppmv_sim      0.0910     0.3586
P_vmr_ppmv_sim      0.0460     0.1802

P_vmr_ppmv_error_percent_retr   1050.0000     0.05034  20.00000E+00
P_vmr_ppmv_error_percent_retr    977.5200     0.05034  20.00000E+00
P_vmr_ppmv_error_percent_retr    868.5980     0.05662  20.00000E+00
P_vmr_ppmv_error_percent_retr    770.3700     0.05675  20.00000E+00
P_vmr_ppmv_error_percent_retr    681.5660     0.05795  20.00000E+00
P_vmr_ppmv_error_percent_retr    601.8100     0.05983  20.00000E+00
P_vmr_ppmv_error_percent_retr    529.6150     0.06212  20.00000E+00
P_vmr_ppmv_error_percent_retr    464.2380     0.06492  20.00000E+00
P_vmr_ppmv_error_percent_retr    405.7080     0.06832  20.00000E+00
P_vmr_ppmv_error_percent_retr    353.7480     0.07266  20.00000E+00
P_vmr_ppmv_error_percent_retr    306.6830     0.07890  20.00000E+00
P_vmr_ppmv_error_percent_retr    263.8090     0.09734  20.00000E+00
P_vmr_ppmv_error_percent_retr    226.8600     0.1326   20.00000E+00
P_vmr_ppmv_error_percent_retr    194.6650     0.1903   20.00000E+00
P_vmr_ppmv_error_percent_retr    165.1240     0.2649   20.00000E+00
P_vmr_ppmv_error_percent_retr    142.5480     0.3453   20.00000E+00
P_vmr_ppmv_error_percent_retr    120.7390     0.4653   20.00000E+00
P_vmr_ppmv_error_percent_retr    104.1690     0.5910   20.00000E+00
P_vmr_ppmv_error_percent_retr     87.6600     0.8120   20.00000E+00
P_vmr_ppmv_error_percent_retr     75.9550     1.0330   20.00000E+00
P_vmr_ppmv_error_percent_retr     64.2900     1.4040   20.00000E+00
P_vmr_ppmv_error_percent_retr     55.1550     1.8080   20.00000E+00
P_vmr_ppmv_error_percent_retr     47.4170     2.2700   20.00000E+00
P_vmr_ppmv_error_percent_retr     39.6970     2.8630   20.00000E+00
P_vmr_ppmv_error_percent_retr     35.5530     3.4560   20.00000E+00
P_vmr_ppmv_error_percent_retr     31.4830     4.0490   20.00000E+00
P_vmr_ppmv_error_percent_retr     27.4120     4.6170   20.00000E+00
P_vmr_ppmv_error_percent_retr     17.2360     5.7660   20.00000E+00
P_vmr_ppmv_error_percent_retr     12.8630     6.9160   20.00000E+00
P_vmr_ppmv_error_percent_retr      8.7580     7.2150   20.00000E+00
P_vmr_ppmv_error_percent_retr      6.1390     7.2710   20.00000E+00
P_vmr_ppmv_error_percent_retr      4.5080     7.0160   20.00000E+00
P_vmr_ppmv_error_percent_retr      2.9410     6.1290   20.00000E+00
P_vmr_ppmv_error_percent_retr      2.3710     5.2420   20.00000E+00
P_vmr_ppmv_error_percent_retr      1.8010     4.3620   20.00000E+00
P_vmr_ppmv_error_percent_retr      1.2320     3.5000   20.00000E+00
P_vmr_ppmv_error_percent_retr      0.6710     2.6390   20.00000E+00
P_vmr_ppmv_error_percent_retr      0.3450     1.3560   20.00000E+00
P_vmr_ppmv_error_percent_retr      0.1770     0.6967   20.00000E+00
P_vmr_ppmv_error_percent_retr      0.0910     0.3586   20.00000E+00
P_vmr_ppmv_error_percent_retr      0.0460     0.1802   20.00000E+00

# the profile is defined through linear interpolation on the logarithm of the volume mixing ratio
# or through cubic spline interpolation on the logarithm of the volume mixing ratio.
# For strong absorption (ozone at wavelengths < 305 nm) and in the oxygen A band spline intrepolation can become
# inaccurate and linear interpolation is preferred. If spline interpolation is used there can be the
# side efect that d2R/dkabs/dz > 0 for some parts of the atmosphere and strong absorption. This has negative
# consequences if DISMAS is used as retrieval method because we can not fit a low order polynomial in the wavelength 
# for ln(d2R/dkabs/dz), but have to use  a polynomial for d2R/dkabs/dz. Hence for ozone profile retrieval
# combined with DISMAS linear interpolation should be used.
useLinInterpSim          0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )
useLinInterpRetr         0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )

subsection column
# specify colum either in molecules cm-2 or in Dobson Units
# columnSim_molcm2       6.0E15  ( in molecules cm-2; use columnSim_DU for Dobson Units )
# columnRetr_molcm2      6.0E15  ( in molecules cm-2; use columnRetr_DU for Dobson Units )
columnSim_DU             300.0   ( in DU; use columnSim_molcm2 for molecules cm-2)
columnRetr_DU            300.0   ( in DU; use columnRetr_molcm2 for molecules cm-2)
APerrorColumn_percent    200.0   ( a-priori error for the column in percent)

subsection scaling
# If scaleProfileToColumn = 1 the profile shape is kept constant but the profile is scaled so that
# the column agrees with the column specified in subsection column. If scaleProfileToColumn = 0 no
# scaling is performed and the column values specified in subsection column are ignored. In fact,
# internally the column values read are overwritten with the column value calculated from the profile.
# The a priori error APerrorColumn_percent and the errors specified for the profile are expressed
# in percent and these are therefore not affected by scaling.
scaleProfileToColumnSim  0      ( 1 = scale profile ; 0 = do not scale profile)
scaleProfileToColumnRetr 0      ( 1 = scale profile ; 0 = do not scale profile)
scaleFactorXsecSim       1.0    ( absorption cross section is multiplied with this factor ) 
scaleFactorXsecRetr      1.0    ( absorption cross section is multiplied with this factor ) 

subsection errorCovariancesSpecs ( only required if the profile is fitted )
useAPCorrLength          1       (if true a correlation length is used to fill the non-diagonal elements of Sa)
APCorrLength             6.0     (in km; only used if useAPCorrLength /= 0 )
removeAPcorrTropStrat    1 (1= no correlation between levels in stratosphere and troposphere)
useSaFromFile            0 (if true it reads the a-priori covariance matrix from file, replacing 'aPrioriError_percent')
useSaDiagFromFile        0 (if useSaFromFile and useSaDiagFromFile are both true only the diagnal id used)
APcovarianceFileName     RefSpec/mozart_error_covariance_matrix.dat  (name of file containing Sa; used if useSaFromFile /= 0)

