# O2 profile for Tropical_Biomass_Burning_Land based on TM4
# Latitude   =       5.00000
# Longitude  =       20.0000
# Month      =        1
# TM4 Latitude index    =       47
# TM4 Longitude index   =       66
# TM4 Lat (min-max)     =       4.00000      6.00000
# TM4 Lon (min-max)     =       18.0000      21.0000
# TM4 surface pressure  =      951.320 (hPa)  (extended to 1500 hPa)

SECTION O2
# the following absorbing gases are recognized as section name
# O3, NO2, trop_NO2, strat_NO2, O2, O2-O2, SO2, HCHO, CHOCHO, BrO, H2O, CH4, CO, CO2

subsection filenames
# use the file     RefSpec/07_HIT08_TROPOMI.par          for the HITRAN database
# use the file     RefSpec/07_HIT08_O2AJPL_TROPOMI.par   for the JPL database
XsectionFileNameSim      RefSpec/07_HIT08_O2A_TROPOMI.par (file name with absorption cross section data used for simulation)
XsectionFileNameRetr     RefSpec/07_HIT08_O2A_TROPOMI.par (file name with absorption cross section data used for retrieval)

subsection specifyFitting
# profile and column can not be fitted for O2 and O2-O2
fitProfile           0   (1 = fit profile ; 0 = do not fit profile)
fitColumn            0   (1 = fit column  ; 0 = do not fit column)  (it is an error to fit both column and profile)

subsection  profile
# note that for a collision complex the parent gas has to be specified here, 
# e.g. for O2-O2 the volume mixing ratio of O2 has to be specified

# mixing ratio of O2 is 0.20946 taken from R. Goody, Principles of atmospheric physics and chemistry,
# Table 1.2, Oxford University Press, New York, 1995. [DAK uses 0.209476  (US Stand. Atm., 1976)]

P_vmr_ppmv_sim      1050.600    20.94600E+04
P_vmr_ppmv_sim       951.320    20.94600E+04
P_vmr_ppmv_sim       848.733    20.94600E+04
P_vmr_ppmv_sim       755.161    20.94600E+04
P_vmr_ppmv_sim       669.370    20.94600E+04
P_vmr_ppmv_sim       592.272    20.94600E+04
P_vmr_ppmv_sim       522.716    20.94600E+04
P_vmr_ppmv_sim       459.804    20.94600E+04
P_vmr_ppmv_sim       403.816    20.94600E+04
P_vmr_ppmv_sim       353.635    20.94600E+04
P_vmr_ppmv_sim       307.981    20.94600E+04
P_vmr_ppmv_sim       266.144    20.94600E+04
P_vmr_ppmv_sim       230.476    20.94600E+04
P_vmr_ppmv_sim       197.896    20.94600E+04
P_vmr_ppmv_sim       168.324    20.94600E+04
P_vmr_ppmv_sim       143.715    20.94600E+04
P_vmr_ppmv_sim       120.462    20.94600E+04
P_vmr_ppmv_sim       102.047    20.94600E+04
P_vmr_ppmv_sim        84.890    20.94600E+04
P_vmr_ppmv_sim        72.222    20.94600E+04
P_vmr_ppmv_sim        59.767    20.94600E+04
P_vmr_ppmv_sim        51.649    20.94600E+04
P_vmr_ppmv_sim        43.534    20.94600E+04
P_vmr_ppmv_sim        37.409    20.94600E+04
P_vmr_ppmv_sim        33.191    20.94600E+04
P_vmr_ppmv_sim        28.972    20.94600E+04
P_vmr_ppmv_sim        24.754    20.94600E+04
P_vmr_ppmv_sim        15.748    20.94600E+04
P_vmr_ppmv_sim        11.504    20.94600E+04
P_vmr_ppmv_sim         7.260    20.94600E+04
P_vmr_ppmv_sim         5.474    20.94600E+04
P_vmr_ppmv_sim         3.773    20.94600E+04
P_vmr_ppmv_sim         2.658    20.94600E+04
P_vmr_ppmv_sim         2.060    20.94600E+04
P_vmr_ppmv_sim         1.462    20.94600E+04
P_vmr_ppmv_sim         0.873    20.94600E+04
P_vmr_ppmv_sim         0.276    20.94600E+04
P_vmr_ppmv_sim         0.141    20.94600E+04
P_vmr_ppmv_sim         0.072    20.94600E+04
P_vmr_ppmv_sim         0.037    20.94600E+04
P_vmr_ppmv_sim         0.019    20.94600E+04
P_vmr_ppmv_sim         0.010    20.94600E+04

P_vmr_ppmv_error_percent_retr     1050.600    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      951.320    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      848.733    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      755.161    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      669.370    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      592.272    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      522.716    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      459.804    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      403.816    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      353.635    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      307.981    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      266.144    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      230.476    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      197.896    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      168.324    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      143.715    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      120.462    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr      102.047    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       84.890    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       72.222    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       59.767    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       51.649    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       43.534    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       37.409    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       33.191    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       28.972    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       24.754    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       15.748    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr       11.504    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        7.260    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        5.474    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        3.773    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        2.658    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        2.060    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        1.462    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        0.873    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        0.276    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        0.141    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        0.072    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        0.037    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        0.019    20.94600E+04    10.00000E+00
P_vmr_ppmv_error_percent_retr        0.010    20.94600E+04    10.00000E+00

# the profile is defined through linear interpolation on the logarithm of the volume mixing ratio
# or through cubic spline interpolation on the logarithm of the volume mixing ratio.
# For strong absorption (ozone at wavelengths < 305 nm) and in the oxygen A band spline intrepolation can become
# inaccurate and linear interpolation is preferred. If spline interpolation is used there can be the
# side efect that d2R/dkabs/dz > 0 for some parts of the atmosphere and strong absorption. This has negative
# consequences if DISMAS is used as retrieval method because we can not fit a low order polynomial in the wavelength 
# for ln(d2R/dkabs/dz), but have to use  a polynomial for d2R/dkabs/dz. Hence for ozone profile retrieval
# combined with DISMAS linear interpolation should be used.
useLinInterpSim          0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )
useLinInterpRetr         0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )

subsection column
# specify colum either in molecules cm-2 or in Dobson Units
# columnSim_molcm2       6.0E15  ( in molecules cm-2; use columnSim_DU for Dobson Units )
# columnRetr_molcm2      6.0E15  ( in molecules cm-2; use columnRetr_DU for Dobson Units )
columnSim_DU             0.5     ( in DU; use columnSim_molcm2 for molecules cm-2)
columnRetr_DU            0.5     ( in DU; use columnRetr_molcm2 for molecules cm-2)
APerrorColumn_percent    200.0   ( a-priori error for the column in percent)

subsection scaling
# If scaleProfileToColumn = 1 the profile shape is kept constant but the profile is scaled so that
# the column agrees with the column specified in subsection column. If scaleProfileToColumn = 0 no
# scaling is performed and the column values specified in subsection column are ignored. In fact,
# internally the column values read are overwritten with the column value calculated from the profile.
# The a priori error APerrorColumn_percent and the errors specified for the profile are expressed
# in percent and these are therefore not affected by scaling.
scaleProfileToColumnSim  0      ( 1 = scale profile ; 0 = do not scale profile) (the shape of the profile remains the same)
scaleProfileToColumnRetr 0      ( 1 = scale profile ; 0 = do not scale profile) (the shape of the profile remains the same)
scaleFactorXsecSim       1.0    ( absorption cross section is multiplied with this factor ) 
scaleFactorXsecRetr      1.0    ( absorption cross section is multiplied with this factor ) 

subsection errorCovariancesSpecs ( only required if the profile is fitted )
useAPCorrLength          0       (if true a correlation length is used to fill the non-diagonal elements of Sa)
APCorrLength             3.0     (in km; only used if useAPCorrLength /= 0 )
removeAPcorrTropStrat    1 (1= no correlation between levels in stratosphere and troposphere)
useSaFromFile            0 (if true it reads the a-priori covariance matrix from file, replacing 'aPrioriError_percent')
useSaDiagFromFile        0 (if useSaFromFile and useSaDiagFromFile are both true only the diagnal id used)
APcovarianceFileName     RefSpec/mozart_error_covariance_matrix.dat  (name of file containing Sa; used if useSaFromFile /= 0)

subsection HITRAN  ( required for line absorbing species H2O, O2, CH4, CO, CO2, may be omitted for other gases )
# default HITRAN is used and not code from LISA. The LISA code can only be used for O2 A-band and includes 
# corrections for line mixing (LM) and collision induced absorption (CIA). These correction can be turned off.
# Further it uses JPL data instead of the HITRAN data (but uses approximately the HITRAN format. 
# The flags useLISAsim, useLISAretr, useLMsim, useLMretr, useCIAsim, and useCIAretr are all optional.
# To correct for line mixing and collision induced absorption in the O2 A band set 
# useLISA = 1, useLM = 1 and useCIA = 1. These flags are ignored when:
#    - O2 is not specified as absorbing gas
#    - the wavelength range for a spectral band is not limited to the O2 A-band. That is, the band contains
#      wavelengths outside the window (755. 775) nm.
factorLMSim              1.0d0     (line mixing is multiplied with this factor  - only for O2 A-band: 758 - 775 nm)
factorLMRetr             1.0d0     (line mixing is multiplied with this factor  - only for O2 A-band: 758 - 775 nm)
ISOsim                   1 2 3     (HITRAN: isotope info; 1 = most abundant isotope, 2 = second most abundant, etc.)
ISOretr                  1 2 3     (HITRAN: isotope info; 1 = most abundant isotope, 2 = second most abundant, etc.)
thresholdLineSim         1.0E-7    (lines weaker than thresholdLine * Max(lineStrength) are ignored for wavelength grid)
thresholdLineRetr        1.0E-7    (lines weaker than thresholdLine * Max(lineStrength) are ignored for wavelength grid)
cutoffSim                300.0     (in cm-1; lines at distances larger than cutoff are ignored)
cutoffRetr               300.0     (in cm-1; lines at distances larger than cutoff are ignored)

