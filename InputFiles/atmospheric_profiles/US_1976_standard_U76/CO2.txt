# CO2 with 390 instead of 360 ppmv used for ESA CO2 project (360 ppmv is the values for the standard atm)

SECTION CO2
# the following absorbing gases are recognized as section name
# O3, NO2, trop_NO2, strat_NO2, O2, O2-O2, SO2, HCHO, CHOCHO, BrO, H2O, CH4, CO, CO2

subsection filenames
XsectionFileNameSim      RefSpec/02_HIT08_TROPOMI.par (file name with absorption cross section data used for simulation)
XsectionFileNameRetr     RefSpec/02_HIT08_TROPOMI.par (file name with absorption cross section data used for retrieval)

subsection specifyFitting
# profile and column can not be fitted for O2 and O2-O2
fitProfile           0   (1 = fit profile ; 0 = do not fit profile)
fitColumn            1   (1 = fit column  ; 0 = do not fit column)  (it is an error to fit both column and profile)

subsection  profile
# note that for a collision complex the parent gas has to be specified here, 
# e.g. for O2-O2 the volume mixing ratio of O2 has to be specified
# US standard atmosphere but adjusted CO2 profile (390 ppmv instead of 360 ppmv)

P_vmr_ppmv_sim      1.0500E+03    3.9000E+02
P_vmr_ppmv_sim      1.0130E+03    3.9000E+02
P_vmr_ppmv_sim      9.5590E+02    3.9000E+02
P_vmr_ppmv_sim      8.9880E+02    3.9000E+02
P_vmr_ppmv_sim      8.4690E+02    3.9000E+02
P_vmr_ppmv_sim      7.9500E+02    3.9000E+02
P_vmr_ppmv_sim      7.0120E+02    3.9000E+02
P_vmr_ppmv_sim      6.1660E+02    3.9000E+02
P_vmr_ppmv_sim      5.4050E+02    3.9000E+02
P_vmr_ppmv_sim      4.7220E+02    3.9000E+02
P_vmr_ppmv_sim      4.1100E+02    3.9000E+02
P_vmr_ppmv_sim      3.5650E+02    3.9000E+02
P_vmr_ppmv_sim      3.0800E+02    3.9000E+02
P_vmr_ppmv_sim      2.6500E+02    3.9000E+02
P_vmr_ppmv_sim      2.2700E+02    3.9000E+02
P_vmr_ppmv_sim      1.9400E+02    3.9000E+02
P_vmr_ppmv_sim      1.6580E+02    3.9000E+02
P_vmr_ppmv_sim      1.4170E+02    3.9000E+02
P_vmr_ppmv_sim      1.2110E+02    3.9000E+02
P_vmr_ppmv_sim      5.5290E+01    3.9000E+02
P_vmr_ppmv_sim      2.5490E+01    3.9000E+02
P_vmr_ppmv_sim      1.1970E+01    3.9000E+02
P_vmr_ppmv_sim      5.7460E+00    3.9000E+02
P_vmr_ppmv_sim      2.8710E+00    3.9000E+02
P_vmr_ppmv_sim      1.4910E+00    3.9000E+02
P_vmr_ppmv_sim      7.9780E-01    3.9000E+02
P_vmr_ppmv_sim      4.2520E-01    3.9000E+02
P_vmr_ppmv_sim      2.1960E-01    3.8500E+02

P_vmr_ppmv_error_percent_retr     1.0500E+03     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     1.0130E+03     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     9.5590E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     8.9880E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     8.4690E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     7.9500E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     7.0120E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     6.1660E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     5.4050E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     4.7220E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     4.1100E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     3.5650E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     3.0800E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     2.6500E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     2.2700E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     1.9400E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     1.6580E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     1.4170E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     1.2110E+02     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     5.5290E+01     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     2.5490E+01     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     1.1970E+01     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     5.7460E+00     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     2.8710E+00     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     1.4910E+00     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     7.9780E-01     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     4.2520E-01     3.9000E+02    10.00000E+00
P_vmr_ppmv_error_percent_retr     2.1960E-01     3.8500E+02    10.00000E+00

# the profile is defined through linear interpolation on the logarithm of the volume mixing ratio
# or through cubic spline interpolation on the logarithm of the volume mixing ratio.
# For strong absorption (ozone at wavelengths < 305 nm) and in the oxygen A band spline intrepolation can become
# inaccurate and linear interpolation is preferred. If spline interpolation is used there can be the
# side efect that d2R/dkabs/dz > 0 for some parts of the atmosphere and strong absorption. This has negative
# consequences if DISMAS is used as retrieval method because we can not fit a low order polynomial in the wavelength 
# for ln(d2R/dkabs/dz), but have to use  a polynomial for d2R/dkabs/dz. Hence for ozone profile retrieval
# combined with DISMAS linear interpolation should be used.
useLinInterpSim          0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )
useLinInterpRetr         0    (1 = linear interpolation for ln(vmr(z)); 0 = cubic spline interpolation for ln(vmr(z)) )

subsection column
# specify colum either in molecules cm-2 or in Dobson Units
columnSim_molcm2       0.8396E+22  ( in molecules cm-2; use columnSim_DU for Dobson Units )
columnRetr_molcm2      0.8396E+22  ( in molecules cm-2; use columnRetr_DU for Dobson Units )
# columnSim_DU             0.5         ( in DU; use columnSim_molcm2 for molecules cm-2)
# columnRetr_DU            0.5         ( in DU; use columnRetr_molcm2 for molecules cm-2)
APerrorColumn_percent    100.0       ( a-priori error for the column in percent)

subsection scaling
# If scaleProfileToColumn = 1 the profile shape is kept constant but the profile is scaled so that
# the column agrees with the column specified in subsection column. If scaleProfileToColumn = 0 no
# scaling is performed and the column values specified in subsection column are ignored. In fact,
# internally the column values read are overwritten with the column value calculated from the profile.
# The a priori error APerrorColumn_percent and the errors specified for the profile are expressed
# in percent and these are therefore not affected by scaling.
scaleProfileToColumnSim  0      ( 1 = scale profile ; 0 = do not scale profile) (the shape of the profile remains the same)
scaleProfileToColumnRetr 0      ( 1 = scale profile ; 0 = do not scale profile) (the shape of the profile remains the same)
scaleFactorXsecSim       1.0    ( absorption cross section is multiplied with this factor ) 
scaleFactorXsecRetr      1.0    ( absorption cross section is multiplied with this factor ) 

subsection errorCovariancesSpecs ( only required if the profile is fitted )
useAPCorrLength          1       (if true a correlation length is used to fill the non-diagonal elements of Sa)
APCorrLength             3.0     (in km; only used if useAPCorrLength /= 0 )
removeAPcorrTropStrat    0 (1= no correlation between levels in stratosphere and troposphere)
useSaFromFile            0 (if true it reads the a-priori covariance matrix from file, replacing 'aPrioriError_percent')
useSaDiagFromFile        0 (if useSaFromFile and useSaDiagFromFile are both true only the diagnal id used)
APcovarianceFileName     RefSpec/mozart_error_covariance_matrix.dat  (name of file containing Sa; used if useSaFromFile /= 0)

subsection HITRAN  ( required for line absorbing species H2O, O2, CH4, CO, CO2, may be omitted for other gases )
# default HITRAN is used and not code from LISA. The LISA code can only be used for O2 A-band and includes 
# corrections for line mixing (LM) and collision induced absorption (CIA). These correction can be turned off.
# Further it uses JPL data instead of the HITRAN data (but uses approximately the HITRAN format. 
# The flags useLISAsim, useLISAretr, useLMsim, useLMretr, useCIAsim, and useCIAretr are all optional.
# To correct for line mixing and collision induced absorption in the O2 A band set 
# useLISA = 1, useLM = 1 and useCIA = 1. These flags are ignored when:
#    - O2 is not specified as absorbing gas
#    - the wavelength range for a spectral band is not limited to the O2 A-band. That is, the band contains
#      wavelengths outside the window (755. 775) nm.
ISOsim                   1       (HITRAN: isotope info; 1 = most abundant isotope, 2 = second most abundant, etc.)
ISOretr                  1       (HITRAN: isotope info; 1 = most abundant isotope, 2 = second most abundant, etc.)
thresholdLineSim         3.0E-5  (lines weaker than thresholdLine * Max(lineStrength) are ignored for wavelength grid)
thresholdLineRetr        3.0E-5  (lines weaker than thresholdLine * Max(lineStrength) are ignored for wavelength grid)
cutoffSim                10.0    (in cm-1; lines at distances larger than cutoff are ignored)
cutoffRetr               10.0    (in cm-1; lines at distances larger than cutoff are ignored)
